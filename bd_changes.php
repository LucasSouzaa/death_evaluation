Transformar em uma única tabela as seguintes tabelas existentes:  'state_regulation' e 'municipal_regulation'.
Ambas possuem a mesma estrutura. Sugestão: criar um campo de origem que permita identificar se é municipal ou estadual.

Transformar as tabelas trauma_drugs e demais que estejam trauma_xxx para que possam ser utilizadas tanto no contexto clínico quanto traumático.
    trauma_drugs [principal alvo. hoje existe clinical_drugs e trauma_drugs, por exemplo]
    trauma_laboratorial_exams
    trauma_mechanism  (esta logicamente nao dá... porque mecanismos de trauma sao especificos)	
    trauma_other_related_informations (esta também nao)	
    trauma_procedures (também é possível, visto que existe uma table de configuração... mas nao prioridade)
    trauma_radiological_exams		
    trauma_severity_indicators		
    trauma_vital_signs (principal alvo, já que sao os mesmos sinais vitais)
    
Fazer o mesmo com os sinais vitais. Criar uma única tabela para trauma e não-trauma.
Isso faz sentido pois não há diferença entre os sinais vitais nos casos de trauma e clínicos.
    
Criar campo na tabela use_service para identificar se o caso é clínico ou traumático.


