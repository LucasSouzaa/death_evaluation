
function view_patient(id) {
    jQuery.ajax({
        url: jQuery("body").data("baseurl") + "home/view_patient",
        type: "post",
        dataType: 'json',
        data: {
            id: id
        },
        success: function (response) {
            if (response.status == 'OK') {
                // person data
                jQuery('#nome_paciente').val(response.people.name);
                jQuery('#cartao_sus').val(response.people.sus_card);
                jQuery('#sexo').val(response.people.gender);
                jQuery('#dta_nascimento').val(response.people.birthdate);
                jQuery('#cor').val(response.people.skin_color);
                jQuery('#estado_civil').val(response.people.marital_status);
                jQuery('#nome_mae').val(response.people.mother_name);
                jQuery('#naturalidade').val(response.people.nationality);
                jQuery('#residencia_cidade').val(response.people.home_town);
                jQuery('#residencia_uf').val(response.people.home_town_state);
                jQuery('#patient_id').val(id);
                jQuery('#cpf').val(response.people.cpf);
                jQuery('#nom_profissao').val(response.people.occupation);
                jQuery('#grau_instrucao').val(response.people.instruction);
                jQuery('#residencia_logradouro').val(response.people.street);
                jQuery('#residencia_numero').val(response.people.number);
                jQuery('#residencia_bairro').val(response.people.neighborhood);
                jQuery('#residencia_cep').val(response.people.cep);
                jQuery('#telefone').val(response.people.telephone);
                jQuery('#email').val(response.people.email);
                jQuery('#registro').val(response.people.hc_number);
                jQuery('#plano_saude_paciente').val(response.people.health_plan);
                //open person page
                jQuery('#div-patient-data').show();
            } else {
                var notice = new PNotify({
                    title: 'Erro',
                    text: 'Tente novamente mais tarde',
                    type: 'error',
                    addclass: 'click-2-close',
                    hide: false,
                    buttons: {
                        closer: false,
                        sticker: false
                    }
                });
                notice.get().click(function () {
                    notice.remove();
                });
            }
        }
    });
}


function delete_patient(id) {
    jQuery.ajax({
        url: jQuery("body").data("baseurl") + "home/delete_patient",
        type: "post",
        dataType: 'json',
        data: {
            id: id
        },
        success: function (response) {
            if (response.status == 'OK') {
                window.location = jQuery("body").data('baseurl') + 'home/patients';
            } else {
                var notice = new PNotify({
                    title: 'Erro',
                    text: 'Tente novamente mais tarde',
                    type: 'error',
                    addclass: 'click-2-close',
                    hide: false,
                    buttons: {
                        closer: false,
                        sticker: false
                    }
                });
                notice.get().click(function () {
                    notice.remove();
                });
            }
        }
    });
}


function save_patient(){
     jQuery.ajax({
        url: jQuery("body").data("baseurl") + "home/save_patient",
        type: "post",
        dataType: 'json',
        data: {
            id: jQuery('#patient_id').val(),
            name: jQuery('#nome_paciente').val(),
            cpf: jQuery('#cpf').val(),
            birthdate: jQuery('#dta_nascimento').val(),
            mother_name: jQuery('#nome_mae').val(),
            skin_color: jQuery('#cor').val(),
            gender: jQuery('#sexo').val(),
            marital_status: jQuery('#estado_civil').val(),
            nationality: jQuery('#naturalidade').val(),
            home_town_state: jQuery('#residencia_uf').val(),
            home_town: jQuery('#residencia_cidade').val(),
            sus_card: jQuery('#cartao_sus').val(),
            occupation: jQuery('#nom_profissao').val(),
            instruction: jQuery('#grau_instrucao').val(),
            street: jQuery('#residencia_logradouro').val(),
            number: jQuery('#residencia_numero').val(),
            neighborhood: jQuery('#residencia_bairro').val(),
            cep: jQuery('#residencia_cep').val(),
            telephone: jQuery('#telefone').val(),
            email: jQuery('#email').val(),
            hc_number: jQuery('#registro').val(),
            health_plan: jQuery('#plano_saude_paciente').val()
        },
        success: function (response) {
            if (response.status == 'OK') {
                    location.reload();
            } else {
                var notice = new PNotify({
                    title: 'Erro',
                    text: 'Tente novamente mais tarde',
                    type: 'error',
                    addclass: 'click-2-close',
                    hide: false,
                    buttons: {
                        closer: false,
                        sticker: false
                    }
                });
                notice.get().click(function () {
                    notice.remove();
                });
            }
        }
    });
}