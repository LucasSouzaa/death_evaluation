document.getElementById('capec').innerHTML = "<INPUT TYPE='hidden' VALUE='V01-Y98' name='ec_chapter_cid_field'>Causas externas de morbidade e de mortalidade";
popgpec('V01-Y98', 'gpec');
document.getElementById('capi').innerHTML = "<INPUT TYPE='hidden' VALUE='S00-T98' name='i_chapter_cid_field'>Lesões, envenenamento e algumas outras consequências de causas externas";
popgpi('S00-T98', 'gpi');

//subcategoria lesoes
function popscati(valcap, valgp, valcat, elemento) {
    if (window.XMLHttpRequest) {
        xmlHttp = new XMLHttpRequest();
    }else {
        xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    var url = "public/cid10.php?cap=" + valcap + "&&gp=" + valgp + "&&cat=" + valcat;// + str;
    //url=url+"&amp;sid="+Math.random();
    xmlHttp.open("GET", url, true);
    xmlHttp.send();
    xmlHttp.onreadystatechange = function () {
        if (xmlHttp.readyState === 4 && xmlHttp.status === 200) {
            var str = xmlHttp.responseText;
            var res = str.split("-n-");
            var index;
            var tmp = "<select style=width:100% id='capitulo' name='i_subcategory_cid_field' class='form-control'>";
            var res2 = "";
            for (index = 0; index < res.length - 1; ++index) {
                res2 = res[index];
                res2 = res2.split("--");
                tmp = tmp + "<option data-othervalue='" + res2['0'] + "' value='" + res2['1'] + "'>" + res2['1'] + "</option>";
            }
            tmp = tmp + "</select>";
            document.getElementById(elemento).innerHTML = tmp;
        }
    }
}
//subcategoria causas externas
function popscatec(valcap, valgp, valcat, elemento) {
    if (window.XMLHttpRequest) {
        xmlHttp = new XMLHttpRequest();
    }else {
        xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    var url = "public/cid10.php?cap=" + valcap + "&&gp=" + valgp + "&&cat=" + valcat;// + str;
    //url=url+"&amp;sid="+Math.random();
    xmlHttp.open("GET", url, true);
    xmlHttp.send();
    xmlHttp.onreadystatechange = function () {
        if (xmlHttp.readyState === 4 && xmlHttp.status === 200) {
            var str = xmlHttp.responseText;
            var res = str.split("-n-");
            var index;
            var tmp = "<select style=width:100% id='capitulo' name='ec_subcategory_cid_field' class='form-control'>";
            var res2 = "";
            for (index = 0; index < res.length - 1; ++index) {
                res2 = res[index];
                res2 = res2.split("--");
                tmp = tmp + "<option value='" + res2['1'] + "'>" + res2['1'] + "</option>";
            }
            tmp = tmp + "</select>";
            document.getElementById(elemento).innerHTML = tmp;
        }
    }
}
//categoria lesoes
function popcati(valcap, valgp, elemento) {
    if (window.XMLHttpRequest) {
        xmlHttp = new XMLHttpRequest();
    }
    else {
        xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    var url = "public/cid10.php?cap=" + valcap + "&&gp=" + valgp;// + str;
    //url=url+"&amp;sid="+Math.random();
    xmlHttp.open("GET", url, true);
    xmlHttp.send();
    xmlHttp.onreadystatechange = function () {
        if (xmlHttp.readyState === 4 && xmlHttp.status === 200) {
            var str = xmlHttp.responseText;
            var res = str.split("-n-");
            var index;
            var tmp = "<select style=width:100% id='categoria' name='i_category_cid_field' class='form-control' onchange=popscati('" + valcap + "','" + valgp + "',this.value,'scati')>";
            var res2 = "";
            for (index = 0; index < res.length - 1; ++index) {
                res2 = res[index];
                res2 = res2.split("--");
                tmp = tmp + "<option value='" + res2['0'] + "'>" + res2['1'] + "</option>";
            }
            tmp = tmp + "</select>";
            document.getElementById(elemento).innerHTML = tmp;
        }
    }
}
//categoria causas externas
function popcatec(valcap, valgp, elemento) {
    if (window.XMLHttpRequest) {
        xmlHttp = new XMLHttpRequest();
    }else {
        xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    var url = "public/cid10.php?cap=" + valcap + "&&gp=" + valgp;// + str;
    //url=url+"&amp;sid="+Math.random();
    xmlHttp.open("GET", url, true);
    xmlHttp.send();
    xmlHttp.onreadystatechange = function () {
        if (xmlHttp.readyState === 4 && xmlHttp.status === 200) {
            var str = xmlHttp.responseText;
            var res = str.split("-n-");
            var index;
            var tmp = "<select style=width:100% id='categoria' name='ec_category_cid_field' class='form-control' onchange=popscatec('" + valcap + "','" + valgp + "',this.value,'scatec')>";
            var res2 = "";
            for (index = 0; index < res.length - 1; ++index) {
                res2 = res[index];
                res2 = res2.split("--");
                tmp = tmp + "<option value='" + res2['0'] + "'>" + res2['1'] + "</option>";
            }
            tmp = tmp + "</select>";
            document.getElementById(elemento).innerHTML = tmp;
            popscatec(valcap, valgp, jQuery("#categoria[name=ec_category_cid_field] option:first").val(), 'scatec');
        }                        
    }
}
//grupo lesoes
function popgpi(valcap, elemento) {
    if (window.XMLHttpRequest) {
        xmlHttp = new XMLHttpRequest();
    }else {
        xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    var url = "public/cid10.php?cap=" + valcap;
    xmlHttp.open("GET", url, true);
    xmlHttp.send();
    xmlHttp.onreadystatechange = function () {
        if (xmlHttp.readyState === 4 && xmlHttp.status === 200) {
            var str = xmlHttp.responseText;
            var res = str.split("-n-");
            var index;
            var tmp = "<select style=width:100% id='grupo' name='i_group_cid_field' class='form-control' onchange=popcati('" + valcap + "',this.value,'cati')>";
            var res2 = "";
            for (index = 0; index < res.length - 1; ++index) {
                res2 = res[index];
                res2 = res2.split("--");
                tmp = tmp + "<option value='" + res2['0'] + "'>" + res2['1'] + "</option>";
            }
            tmp = tmp + "</select>";
            document.getElementById(elemento).innerHTML = tmp;
            tmp = "";
        }
    }
}
//grupo causas externas
function popgpec(valcap, elemento) {
    if (window.XMLHttpRequest) {
        xmlHttp2 = new XMLHttpRequest();
    }else {
        xmlHttp2 = new ActiveXObject("Microsoft.XMLHTTP");
    }
    var url = "public/cid10.php?cap=" + valcap;// + str;
    //url=url+"&amp;sid="+Math.random();
    xmlHttp2.open("GET", url, true);
    xmlHttp2.send();
    xmlHttp2.onreadystatechange = function () {
        if (xmlHttp2.readyState === 4 && xmlHttp2.status === 200) {
            var str = xmlHttp2.responseText;
            var res = str.split("-n-");
            var index;
            var tmp = "<select style=width:100% id='grupo' name='ec_group_cid_field' class='form-control' onchange=popcatec('" + valcap + "',this.value,'catec')>";
            var res2 = "";
            for (index = 0; index < res.length - 1; ++index) {
                res2 = res[index];
                res2 = res2.split("--");
                tmp = tmp + "<option value='" + res2['0'] + "'>" + res2['1'] + "</option>";
            }
            tmp = tmp + "</select>";
            document.getElementById(elemento).innerHTML = tmp;
            tmp = "";
        }
    }
}

/* 	document.getElementById('capec').innerHTML = "<INPUT TYPE='hidden' VALUE='V01-Y98' name='data[FinalDiagnosi][ec_system]'>Causas externas de morbidade e de mortalidade";
 popgpec('V01-Y98','gpec');
 document.getElementById('capi').innerHTML = "<INPUT TYPE='hidden' VALUE='S00-T98' name='data[FinalDiagnosi][l_system]'>Lesões, envenenamento e algumas outras consequências de causas externas";
 popgpi('S00-T98','gpi');
 
 //subcategoria lesoes
 function popscati(valcap,valgp,valcat,elemento){
 if (window.XMLHttpRequest){xmlHttp=new XMLHttpRequest();}
 else {xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");}
 var url="public/cid10.php?cap="+valcap+"&&gp="+valgp+"&&cat="+valcat;// + str;
 //url=url+"&amp;sid="+Math.random();
 xmlHttp.open("GET",url,true);
 xmlHttp.send();
 xmlHttp.onreadystatechange=function() {
 if (xmlHttp.readyState==4 && xmlHttp.status==200) {
 
 var str = xmlHttp.responseText;
 var res = str.split("-n-");
 var index;
 var tmp = "<select id='capitulo' name='data[FinalDiagnosi][l_cid]' class='form-control'>";
 var res2 = "";
 for (index = 0; index < res.length-1; ++index) {
 res2 = res[index];
 res2 = res2.split("--");
 tmp = tmp+"<option value='"+res2['1']+"'>"+res2['1']+"</option>";
 }
 tmp = tmp+"</select>";
 document.getElementById(elemento).innerHTML = tmp;
 }
 }
 
 }
 //subcategoria causas externas
 function popscatec(valcap,valgp,valcat,elemento){
 if (window.XMLHttpRequest){xmlHttp=new XMLHttpRequest();}
 else {xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");}
 var url="public/cid10.php?cap="+valcap+"&&gp="+valgp+"&&cat="+valcat;// + str;
 //url=url+"&amp;sid="+Math.random();
 xmlHttp.open("GET",url,true);
 xmlHttp.send();
 xmlHttp.onreadystatechange=function() {
 if (xmlHttp.readyState==4 && xmlHttp.status==200) {
 
 var str = xmlHttp.responseText;
 var res = str.split("-n-");
 var index;
 var tmp = "<select id='capitulo' name='data[FinalDiagnosi][ec_cid]' class='form-control'>";
 var res2 = "";
 for (index = 0; index < res.length-1; ++index) {
 res2 = res[index];
 res2 = res2.split("--");
 tmp = tmp+"<option value='"+res2['1']+"'>"+res2['1']+"</option>";
 }
 tmp = tmp+"</select>";
 document.getElementById(elemento).innerHTML = tmp;
 }
 }
 
 }
 //categoria lesoes
 function popcati(valcap,valgp,elemento){
 if (window.XMLHttpRequest){xmlHttp=new XMLHttpRequest();}
 else {xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");}
 var url="public/cid10.php?cap="+valcap+"&&gp="+valgp;// + str;
 //url=url+"&amp;sid="+Math.random();
 xmlHttp.open("GET",url,true);
 xmlHttp.send();
 xmlHttp.onreadystatechange=function() {
 if (xmlHttp.readyState==4 && xmlHttp.status==200) {
 
 var str = xmlHttp.responseText;
 var res = str.split("-n-");
 var index;
 var tmp = "<select id='categoria' name='data[FinalDiagnosi][l_description]' class='form-control' onchange=popscati('"+valcap+"','"+valgp+"',this.value,'scati')>";
 var res2 = "";
 for (index = 0; index < res.length-1; ++index) {
 res2 = res[index];
 res2 = res2.split("--");
 tmp = tmp+"<option value='"+res2['0']+"'>"+res2['1']+"</option>";
 }
 tmp = tmp+"</select>";
 document.getElementById(elemento).innerHTML = tmp;
 }
 }
 
 }
 //categoria causas externas
 function popcatec(valcap,valgp,elemento){
 if (window.XMLHttpRequest){xmlHttp=new XMLHttpRequest();}
 else {xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");}
 var url="public/cid10.php?cap="+valcap+"&&gp="+valgp;// + str;
 //url=url+"&amp;sid="+Math.random();
 xmlHttp.open("GET",url,true);
 xmlHttp.send();
 xmlHttp.onreadystatechange=function() {
 if (xmlHttp.readyState==4 && xmlHttp.status==200) {
 
 var str = xmlHttp.responseText;
 var res = str.split("-n-");
 var index;
 var tmp = "<select id='categoria' name='data[FinalDiagnosi][ec_description]' class='form-control' onchange=popscatec('"+valcap+"','"+valgp+"',this.value,'scatec')>";
 var res2 = "";
 for (index = 0; index < res.length-1; ++index) {
 res2 = res[index];
 res2 = res2.split("--");
 tmp = tmp+"<option value='"+res2['0']+"'>"+res2['1']+"</option>";
 }
 tmp = tmp+"</select>";
 document.getElementById(elemento).innerHTML = tmp;
 }
 }
 
 }
 //capitulo lesoes
 function popgpi(valcap , elemento){
 if (window.XMLHttpRequest){xmlHttp=new XMLHttpRequest();}
 else {xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");}
 var url="public/cid10.php?cap="+valcap;
 xmlHttp.open("GET",url,true);
 xmlHttp.send();
 xmlHttp.onreadystatechange=function() {
 if (xmlHttp.readyState==4 && xmlHttp.status==200) {
 
 var str = xmlHttp.responseText;
 var res = str.split("-n-");
 var index;
 var tmp = "<select id='grupo' name='data[FinalDiagnosi][l_group]' class='form-control' onchange=popcati('"+valcap+"',this.value,'cati')>";
 var res2 = "";
 for (index = 0; index < res.length-1; ++index) {
 res2 = res[index];
 res2 = res2.split("--");
 tmp = tmp+"<option value='"+res2['0']+"'>"+res2['1']+"</option>";
 }
 tmp = tmp+"</select>";
 document.getElementById(elemento).innerHTML = tmp;
 tmp = "";
 }
 }
 
 }
 //capitulo causas externas
 function popgpec(valcap , elemento){
 if (window.XMLHttpRequest){xmlHttp2=new XMLHttpRequest();}
 else {xmlHttp2=new ActiveXObject("Microsoft.XMLHTTP");}
 var url="public/cid10.php?cap="+valcap;// + str;
 //url=url+"&amp;sid="+Math.random();
 xmlHttp2.open("GET",url,true);
 xmlHttp2.send();
 xmlHttp2.onreadystatechange=function() {
 if (xmlHttp2.readyState==4 && xmlHttp2.status==200) {
 
 var str = xmlHttp2.responseText;
 var res = str.split("-n-");
 var index;
 var tmp = "<select id='grupo' name='data[FinalDiagnosi][ec_group]' class='form-control' onchange=popcatec('"+valcap+"',this.value,'catec')>";
 var res2 = "";
 for (index = 0; index < res.length-1; ++index) {
 res2 = res[index];
 res2 = res2.split("--");
 tmp = tmp+"<option value='"+res2['0']+"'>"+res2['1']+"</option>";
 }
 tmp = tmp+"</select>";
 document.getElementById(elemento).innerHTML = tmp;
 tmp = "";
 }
 }
 
 }*/