/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function login_enter(e) {
    if (e.keyCode == 13) {
        login();
    }
}

function login() {

    jQuery('#enter_button').html('<i class="fa fa-spin fa-spinner"></i> Entrando...');
    var check = false;

    if (jQuery("#checkbox").is(":checked")) {
        check = true;
    }

    jQuery.ajax({
        url: jQuery("body").data("baseurl") + "login/check_login",
        type: "post",
        dataType: 'json',
        data: {
            username: jQuery("#username").val(),
            password: jQuery("#password").val(),
            checkbox: check
        },
        success: function (response) {
            if (response.status == "OK") {
                window.location = jQuery("body").data('baseurl') + 'home';
            } else {
                jQuery.gritter.add({
                    title: '<i class="fa fa-times-circle"></i> Dados de acesso',
                    text: response.message,
                    sticky: false,
                    time: '',
                    class_name: 'gritter-danger'
                });
                jQuery('#enter_button').html('Entrar');
                return false;
            }
        }
    });
}