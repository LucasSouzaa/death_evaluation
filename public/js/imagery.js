function delete_imagery(id) {
    jQuery.ajax({
        url: jQuery("body").data("baseurl") + "hospitalization/delete_imagery",
        type: "post",
        dataType: 'json',
        data: {
            id: id
        },
        success: function (response) {
            jQuery('#hospitalization_steps').load(jQuery("body").data("baseurl") + "hospitalization/imagery/edit");
        }
    });
}

function view_imagery(id) {
    jQuery('#view_image').html('');
    jQuery('#view_image').html('<img src="' + jQuery("body").data("baseurl") + 'hospitalization/view_imagery/' + id + '" class="img-responsive" style="max-width: 720px;"/>');
    jQuery('#modalShowImageButton').click();
}