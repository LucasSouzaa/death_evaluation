// INSTITUIÇÕES

function view_institution(id) {
    jQuery.ajax({
        url: jQuery("body").data("baseurl") + "home/view_institution",
        type: "post",
        dataType: 'json',
        data: {
            id: id
        },
        success: function (response) {
            if (response.status == 'OK') {
                // institution data
                jQuery('#complement').val(response.institution.complement);
                jQuery('#number').val(response.institution.number);
                jQuery('#street').val(response.institution.street);
                jQuery('#zipcode').val(response.institution.zipcode);
                jQuery('#neighborhood').val(response.institution.neighborhood);
                jQuery('#state').val(response.institution.state);
                jQuery('#city').val(response.institution.city);
                jQuery('#ie').val(response.institution.ie);
                jQuery('#cnpj').val(response.institution.cnpj);
                jQuery('#ibge_code').val(response.institution.ibge_code);
                jQuery('#cnae_code').val(response.institution.cnae_code);
                jQuery('#identification_code').val(response.institution.identification_code);
                jQuery('#name').val(response.institution.name);
                jQuery('#status').val(response.institution.status);
                jQuery('#type').val(response.institution.type);
                jQuery('#institution_id').val(id);
                jQuery('#div-institution-data').show();
            } else {
                var notice = new PNotify({
                    title: 'Erro',
                    text: 'Tente novamente mais tarde',
                    type: 'error',
                    addclass: 'click-2-close',
                    hide: false,
                    buttons: {
                        closer: false,
                        sticker: false
                    }
                });
                notice.get().click(function () {
                    notice.remove();
                });
            }
        }
    });
}

function delete_institution(id) {
    jQuery.ajax({
        url: jQuery("body").data("baseurl") + "home/delete_institution",
        type: "post",
        dataType: 'json',
        data: {
            id: id
        },
        success: function (response) {
            if (response.status == 'OK') {
                window.location = jQuery("body").data('baseurl') + 'home/institutions';
            } else {
                var notice = new PNotify({
                    title: 'Erro',
                    text: 'Tente novamente mais tarde',
                    type: 'error',
                    addclass: 'click-2-close',
                    hide: false,
                    buttons: {
                        closer: false,
                        sticker: false
                    }
                });
                notice.get().click(function () {
                    notice.remove();
                });
            }
        }
    });
}

function save_institution() {
    jQuery.ajax({
        url: jQuery("body").data("baseurl") + "home/save_institution",
        type: "post",
        dataType: 'json',
        data: {
            status: jQuery('#status').val(),
            id: jQuery('#institution_id').val(),
            complement: jQuery('#complement').val(),
            number: jQuery('#number').val(),
            street: jQuery('#street').val(),
            zipcode: jQuery('#zipcode').val(),
            neighborhood: jQuery('#neighborhood').val(),
            state: jQuery('#state').val(),
            city: jQuery('#city').val(),
            ie: jQuery('#ie').val(),
            cnpj: jQuery('#cnpj').val(),
            ibge_code: jQuery('#ibge_code').val(),
            cnae_code: jQuery('#cnae_code').val(),
            name: jQuery('#name').val(),
            identification_code: jQuery('#identification_code').val(),
            type: jQuery('#type').val()
        },
        success: function (response) {
            if (response.status == 'OK') {
                window.location = jQuery("body").data('baseurl') + 'home/institutions';
            } else {
                var notice = new PNotify({
                    title: 'Erro',
                    text: 'Tente novamente mais tarde',
                    type: 'error',
                    addclass: 'click-2-close',
                    hide: false,
                    buttons: {
                        closer: false,
                        sticker: false
                    }
                });
                notice.get().click(function () {
                    notice.remove();
                });
            }
        }
    });
}

// SECTORS

function view_sector(id) {
    jQuery.ajax({
        url: jQuery("body").data("baseurl") + "home/view_sector",
        type: "post",
        dataType: 'json',
        data: {
            id: id
        },
        success: function (response) {
            if (response.status == 'OK') {
                // institution data
                jQuery('#complement').val(response.sector.complement);
                jQuery('#name').val(response.sector.name);
                jQuery('#status').val(response.sector.status);
                jQuery('#institution_id').val(response.sector.institution_id);
                jQuery('#sector_id').val(id);
                jQuery('#div-sector-data').show();
            } else {
                var notice = new PNotify({
                    title: 'Erro',
                    text: 'Tente novamente mais tarde',
                    type: 'error',
                    addclass: 'click-2-close',
                    hide: false,
                    buttons: {
                        closer: false,
                        sticker: false
                    }
                });
                notice.get().click(function () {
                    notice.remove();
                });
            }
        }
    });
}

function delete_sector(id) {
    jQuery.ajax({
        url: jQuery("body").data("baseurl") + "home/delete_sector",
        type: "post",
        dataType: 'json',
        data: {
            id: id
        },
        success: function (response) {
            if (response.status == 'OK') {
                window.location = jQuery("body").data('baseurl') + 'home/sectors';
            } else {
                var notice = new PNotify({
                    title: 'Erro',
                    text: 'Tente novamente mais tarde',
                    type: 'error',
                    addclass: 'click-2-close',
                    hide: false,
                    buttons: {
                        closer: false,
                        sticker: false
                    }
                });
                notice.get().click(function () {
                    notice.remove();
                });
            }
        }
    });
}

function save_sector() {
    jQuery.ajax({
        url: jQuery("body").data("baseurl") + "home/save_sector",
        type: "post",
        dataType: 'json',
        data: {
            status: jQuery('#status').val(),
            institution_id: jQuery('#institution_id').val(),
            id: jQuery('#sector_id').val(),
            complement: jQuery('#complement').val(),
            name: jQuery('#name').val()
        },
        success: function (response) {
            if (response.status == 'OK') {
                window.location = jQuery("body").data('baseurl') + 'home/sectors';
            } else {
                var notice = new PNotify({
                    title: 'Erro',
                    text: 'Tente novamente mais tarde',
                    type: 'error',
                    addclass: 'click-2-close',
                    hide: false,
                    buttons: {
                        closer: false,
                        sticker: false
                    }
                });
                notice.get().click(function () {
                    notice.remove();
                });
            }
        }
    });
}

// BEDROOMS

function view_bedroom(id) {
    jQuery.ajax({
        url: jQuery("body").data("baseurl") + "home/view_bedroom",
        type: "post",
        dataType: 'json',
        data: {
            id: id
        },
        success: function (response) {
            if (response.status == 'OK') {
                // institution data
                jQuery('#complement').val(response.bedroom.complement);
                jQuery('#name').val(response.bedroom.name);
                jQuery('#status').val(response.bedroom.status);
                jQuery('#sector_id').val(response.bedroom.sector_id);
                jQuery('#bedroom_id').val(id);
                jQuery('#div-bedroom-data').show();
            } else {
                var notice = new PNotify({
                    title: 'Erro',
                    text: 'Tente novamente mais tarde',
                    type: 'error',
                    addclass: 'click-2-close',
                    hide: false,
                    buttons: {
                        closer: false,
                        sticker: false
                    }
                });
                notice.get().click(function () {
                    notice.remove();
                });
            }
        }
    });
}

function delete_bedroom(id) {
    jQuery.ajax({
        url: jQuery("body").data("baseurl") + "home/delete_bedroom",
        type: "post",
        dataType: 'json',
        data: {
            id: id
        },
        success: function (response) {
            if (response.status == 'OK') {
                window.location = jQuery("body").data('baseurl') + 'home/bedrooms';
            } else {
                var notice = new PNotify({
                    title: 'Erro',
                    text: 'Tente novamente mais tarde',
                    type: 'error',
                    addclass: 'click-2-close',
                    hide: false,
                    buttons: {
                        closer: false,
                        sticker: false
                    }
                });
                notice.get().click(function () {
                    notice.remove();
                });
            }
        }
    });
}

function save_bedroom() {
    jQuery.ajax({
        url: jQuery("body").data("baseurl") + "home/save_bedroom",
        type: "post",
        dataType: 'json',
        data: {
            status: jQuery('#status').val(),
            sector_id: jQuery('#sector_id').val(),
            id: jQuery('#bedroom_id').val(),
            complement: jQuery('#complement').val(),
            name: jQuery('#name').val()
        },
        success: function (response) {
            if (response.status == 'OK') {
                window.location = jQuery("body").data('baseurl') + 'home/bedrooms';
            } else {
                var notice = new PNotify({
                    title: 'Erro',
                    text: 'Tente novamente mais tarde',
                    type: 'error',
                    addclass: 'click-2-close',
                    hide: false,
                    buttons: {
                        closer: false,
                        sticker: false
                    }
                });
                notice.get().click(function () {
                    notice.remove();
                });
            }
        }
    });
}

// BEDS

function view_bed(id) {
    jQuery.ajax({
        url: jQuery("body").data("baseurl") + "home/view_bed",
        type: "post",
        dataType: 'json',
        data: {
            id: id
        },
        success: function (response) {
            if (response.status == 'OK') {
                // institution data
                jQuery('#complement').val(response.bed.complement);
                jQuery('#name').val(response.bed.name);
                jQuery('#status').val(response.bed.status);
                jQuery('#bedroom_id').val(response.bed.bedroom_id);
                jQuery('#bed_id').val(id);
                jQuery('#div-bed-data').show();
            } else {
                var notice = new PNotify({
                    title: 'Erro',
                    text: 'Tente novamente mais tarde',
                    type: 'error',
                    addclass: 'click-2-close',
                    hide: false,
                    buttons: {
                        closer: false,
                        sticker: false
                    }
                });
                notice.get().click(function () {
                    notice.remove();
                });
            }
        }
    });
}

function delete_bed(id) {
    jQuery.ajax({
        url: jQuery("body").data("baseurl") + "home/delete_bed",
        type: "post",
        dataType: 'json',
        data: {
            id: id
        },
        success: function (response) {
            if (response.status == 'OK') {
                window.location = jQuery("body").data('baseurl') + 'home/beds';
            } else {
                var notice = new PNotify({
                    title: 'Erro',
                    text: 'Tente novamente mais tarde',
                    type: 'error',
                    addclass: 'click-2-close',
                    hide: false,
                    buttons: {
                        closer: false,
                        sticker: false
                    }
                });
                notice.get().click(function () {
                    notice.remove();
                });
            }
        }
    });
}

function save_bed() {
    jQuery.ajax({
        url: jQuery("body").data("baseurl") + "home/save_bed",
        type: "post",
        dataType: 'json',
        data: {
            status: jQuery('#status').val(),
            bedroom_id: jQuery('#bedroom_id').val(),
            id: jQuery('#bed_id').val(),
            complement: jQuery('#complement').val(),
            name: jQuery('#name').val()
        },
        success: function (response) {
            if (response.status == 'OK') {
                window.location = jQuery("body").data('baseurl') + 'home/beds';
            } else {
                var notice = new PNotify({
                    title: 'Erro',
                    text: 'Tente novamente mais tarde',
                    type: 'error',
                    addclass: 'click-2-close',
                    hide: false,
                    buttons: {
                        closer: false,
                        sticker: false
                    }
                });
                notice.get().click(function () {
                    notice.remove();
                });
            }
        }
    });
}


// TEAMS

function view_team(id) {
    jQuery.ajax({
        url: jQuery("body").data("baseurl") + "home/view_team",
        type: "post",
        dataType: 'json',
        data: {
            id: id
        },
        success: function (response) {
            if (response.status == 'OK') {
                // institution data
                jQuery('#complement').val(response.team.complement);
                jQuery('#name').val(response.team.name);
                jQuery('#status').val(response.team.status);
                jQuery('#institution_id').val(response.team.institution_id);
                jQuery('#team_id').val(id);
                jQuery('#div-team-data').show();
            } else {
                var notice = new PNotify({
                    title: 'Erro',
                    text: 'Tente novamente mais tarde',
                    type: 'error',
                    addclass: 'click-2-close',
                    hide: false,
                    buttons: {
                        closer: false,
                        sticker: false
                    }
                });
                notice.get().click(function () {
                    notice.remove();
                });
            }
        }
    });
}

function delete_team(id) {
    jQuery.ajax({
        url: jQuery("body").data("baseurl") + "home/delete_team",
        type: "post",
        dataType: 'json',
        data: {
            id: id
        },
        success: function (response) {
            if (response.status == 'OK') {
                window.location = jQuery("body").data('baseurl') + 'home/teams';
            } else {
                var notice = new PNotify({
                    title: 'Erro',
                    text: 'Tente novamente mais tarde',
                    type: 'error',
                    addclass: 'click-2-close',
                    hide: false,
                    buttons: {
                        closer: false,
                        sticker: false
                    }
                });
                notice.get().click(function () {
                    notice.remove();
                });
            }
        }
    });
}

function save_team() {
    jQuery.ajax({
        url: jQuery("body").data("baseurl") + "home/save_team",
        type: "post",
        dataType: 'json',
        data: {
            status: jQuery('#status').val(),
            institution_id: jQuery('#institution_id').val(),
            id: jQuery('#team_id').val(),
            complement: jQuery('#complement').val(),
            name: jQuery('#name').val()
        },
        success: function (response) {
            if (response.status == 'OK') {
                window.location = jQuery("body").data('baseurl') + 'home/teams';
            } else {
                var notice = new PNotify({
                    title: 'Erro',
                    text: 'Tente novamente mais tarde',
                    type: 'error',
                    addclass: 'click-2-close',
                    hide: false,
                    buttons: {
                        closer: false,
                        sticker: false
                    }
                });
                notice.get().click(function () {
                    notice.remove();
                });
            }
        }
    });
}

// PROFESSIONALS

function view_professional(id) {
    jQuery.ajax({
        url: jQuery("body").data("baseurl") + "home/view_professional",
        type: "post",
        dataType: 'json',
        data: {
            id: id
        },
        success: function (response) {
            if (response.status == 'OK') {
                // institution data
                jQuery('#name').val(response.professional.name);
                jQuery('#status').val(response.professional.status);
                jQuery('#team_id').val(response.professional.team_id);
                jQuery('#professional_id').val(id);
                jQuery('#person_id').val(response.professional.person_id);
                jQuery('#user_id').val(response.professional.user_id);
                jQuery('#sus_card').val(response.professional.sus_card);
                jQuery('#birthdate').val(response.professional.birthdate);
                jQuery('#gender').val(response.professional.gender);
                jQuery('#skin_color').val(response.professional.skin_color);
                jQuery('#mother_name').val(response.professional.mother_name);
                jQuery('#marital_status').val(response.professional.marital_status);
                jQuery('#nationality').val(response.professional.nationality);
                jQuery('#home_town_state').val(response.professional.home_town_state);
                jQuery('#home_town').val(response.professional.home_town);
                jQuery('#specialty').val(response.professional.specialty);
                jQuery('#hierarchy').val(response.professional.hierarchy);
                jQuery('#class_document_type').val(response.professional.class_document_type);
                jQuery('#class_document_number').val(response.professional.class_document_number);
                jQuery('#username').val(response.professional.username);
                jQuery('#email').val(response.professional.email);
                jQuery('#password').val(response.professional.password);
                jQuery('#repeat_password').val(response.professional.password);

                jQuery('#div-professional-data').show();
            } else {
                var notice = new PNotify({
                    title: 'Erro',
                    text: 'Tente novamente mais tarde',
                    type: 'error',
                    addclass: 'click-2-close',
                    hide: false,
                    buttons: {
                        closer: false,
                        sticker: false
                    }
                });
                notice.get().click(function () {
                    notice.remove();
                });
            }
        }
    });
}

function delete_professional(id) {
    jQuery.ajax({
        url: jQuery("body").data("baseurl") + "home/delete_professional",
        type: "post",
        dataType: 'json',
        data: {
            id: id
        },
        success: function (response) {
            if (response.status == 'OK') {
                window.location = jQuery("body").data('baseurl') + 'home/professionals';
            } else {
                var notice = new PNotify({
                    title: 'Erro',
                    text: 'Tente novamente mais tarde',
                    type: 'error',
                    addclass: 'click-2-close',
                    hide: false,
                    buttons: {
                        closer: false,
                        sticker: false
                    }
                });
                notice.get().click(function () {
                    notice.remove();
                });
            }
        }
    });
}

function save_professional() {

    if (!jQuery('#password').val()) {
        var notice = new PNotify({
            title: 'Erro',
            text: 'Campo de senha precisa ser preenchido',
            type: 'error',
            addclass: 'click-2-close',
            hide: false,
            buttons: {
                closer: false,
                sticker: false
            }
        });
        notice.get().click(function () {
            notice.remove();
        });
        return false;
        exit;
    }
    if (jQuery('#password').val().length < 6 ) {
        var notice = new PNotify({
            title: 'Erro',
            text: 'Campo de senha precisa ter ao menos 6 caracteres',
            type: 'error',
            addclass: 'click-2-close',
            hide: false,
            buttons: {
                closer: false,
                sticker: false
            }
        });
        notice.get().click(function () {
            notice.remove();
        });
        return false;
        exit;
    }
    if (!jQuery('#email').val()) {
        var notice = new PNotify({
            title: 'Erro',
            text: 'Campo de email precisa ser preenchido',
            type: 'error',
            addclass: 'click-2-close',
            hide: false,
            buttons: {
                closer: false,
                sticker: false
            }
        });
        notice.get().click(function () {
            notice.remove();
        });
        return false;
        exit;
    }
    if (!jQuery('#username').val()) {
        var notice = new PNotify({
            title: 'Erro',
            text: 'Campo de usuário precisa ser preenchido',
            type: 'error',
            addclass: 'click-2-close',
            hide: false,
            buttons: {
                closer: false,
                sticker: false
            }
        });
        notice.get().click(function () {
            notice.remove();
        });
        return false;
        exit;
    }
    if (!jQuery('#name').val()) {
        var notice = new PNotify({
            title: 'Erro',
            text: 'Campo de nome precisa ser preenchido',
            type: 'error',
            addclass: 'click-2-close',
            hide: false,
            buttons: {
                closer: false,
                sticker: false
            }
        });
        notice.get().click(function () {
            notice.remove();
        });
        return false;
        exit;
    }
    if (!jQuery('#birthdate').val()) {
        var notice = new PNotify({
            title: 'Erro',
            text: 'Campo de data de nascimento precisa ser preenchido',
            type: 'error',
            addclass: 'click-2-close',
            hide: false,
            buttons: {
                closer: false,
                sticker: false
            }
        });
        notice.get().click(function () {
            notice.remove();
        });
        return false;
        exit;
    }
    if (jQuery('#password').val() !== jQuery('#repeat_password').val()) {
        var notice = new PNotify({
            title: 'Erro',
            text: 'As senhas não conferem, por favor digite a mesma.',
            type: 'error',
            addclass: 'click-2-close',
            hide: false,
            buttons: {
                closer: false,
                sticker: false
            }
        });
        notice.get().click(function () {
            notice.remove();
        });
        return false;
        exit;
    }

    jQuery.ajax({
        url: jQuery("body").data("baseurl") + "home/save_professional",
        type: "post",
        dataType: 'json',
        data: {
            status: jQuery('#status').val(),
            team_id: jQuery('#team_id').val(),
            id: jQuery('#professional_id').val(),
            person_id: jQuery('#person_id').val(),
            user_id: jQuery('#user_id').val(),
            sus_card: jQuery('#sus_card').val(),
            birthdate: jQuery('#birthdate').val(),
            gender: jQuery('#gender').val(),
            skin_color: jQuery('#skin_color').val(),
            mother_name: jQuery('#mother_name').val(),
            name: jQuery('#name').val(),
            marital_status: jQuery('#marital_status').val(),
            nationality: jQuery('#nationality').val(),
            home_town_state: jQuery('#home_town_state').val(),
            home_town: jQuery('#home_town').val(),
            specialty: jQuery('#specialty').val(),
            hierarchy: jQuery('#hierarchy').val(),
            class_document_type: jQuery('#class_document_type').val(),
            class_document_number: jQuery('#class_document_number').val(),
            username: jQuery('#username').val(),
            email: jQuery('#email').val(),
            password: jQuery('#password').val()
        },
        success: function (response) {
            if (response.status == 'OK') {
                window.location = jQuery("body").data('baseurl') + 'home/professionals';
            } else {
                var notice = new PNotify({
                    title: 'Erro',
                    text: 'Tente novamente mais tarde',
                    type: 'error',
                    addclass: 'click-2-close',
                    hide: false,
                    buttons: {
                        closer: false,
                        sticker: false
                    }
                });
                notice.get().click(function () {
                    notice.remove();
                });
            }
        }
    });
}