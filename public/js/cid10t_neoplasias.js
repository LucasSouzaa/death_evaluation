document.getElementById('capec').innerHTML = "<INPUT TYPE='hidden' VALUE='V01-Y98' name='ec_chapter_cid_field'>Causas externas de morbidade e de mortalidade";
popgpec('C00-D48', 'gpec');

//subcategoria causas externas
function popscatec(valcap, valgp, valcat, elemento) {
    if (window.XMLHttpRequest) {
        xmlHttp = new XMLHttpRequest();
    }else {
        xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    var url = "public/cid10.php?cap=" + valcap + "&&gp=" + valgp + "&&cat=" + valcat;// + str;
    //url=url+"&amp;sid="+Math.random();
    xmlHttp.open("GET", url, true);
    xmlHttp.send();
    xmlHttp.onreadystatechange = function () {
        if (xmlHttp.readyState === 4 && xmlHttp.status === 200) {
            var str = xmlHttp.responseText;
            var res = str.split("-n-");
            var index;
            var tmp = "<select style=width:100% id='capitulo' name='ec_subcategory_cid_field' class='form-control'>";
            var res2 = "";
            for (index = 0; index < res.length - 1; ++index) {
                res2 = res[index];
                res2 = res2.split("--");
                tmp = tmp + "<option value='" + res2['1'] + "'>" + res2['1'] + "</option>";
            }
            tmp = tmp + "</select>";
            document.getElementById(elemento).innerHTML = tmp;
        }
    }
}

//categoria causas externas
function popcatec(valcap, valgp, elemento) {
    if (window.XMLHttpRequest) {
        xmlHttp = new XMLHttpRequest();
    }else {
        xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    var url = "public/cid10.php?cap=" + valcap + "&&gp=" + valgp;// + str;
    //url=url+"&amp;sid="+Math.random();
    xmlHttp.open("GET", url, true);
    xmlHttp.send();
    xmlHttp.onreadystatechange = function () {
        if (xmlHttp.readyState === 4 && xmlHttp.status === 200) {
            var str = xmlHttp.responseText;
            var res = str.split("-n-");
            var index;
            var tmp = "<select style=width:100% id='categoria' name='ec_category_cid_field' class='form-control' onchange=popscatec('" + valcap + "','" + valgp + "',this.value,'scatec')>";
            var res2 = "";
            for (index = 0; index < res.length - 1; ++index) {
                res2 = res[index];
                res2 = res2.split("--");
                tmp = tmp + "<option value='" + res2['0'] + "'>" + res2['1'] + "</option>";
            }
            tmp = tmp + "</select>";
            document.getElementById(elemento).innerHTML = tmp;
            popscatec(valcap, valgp, jQuery("#categoria[name=ec_category_cid_field] option:first").val(), 'scatec');
        }                        
    }
}

//grupo causas externas
function popgpec(valcap, elemento) {
    if (window.XMLHttpRequest) {
        xmlHttp2 = new XMLHttpRequest();
    }else {
        xmlHttp2 = new ActiveXObject("Microsoft.XMLHTTP");
    }
    var url = "public/cid10.php?cap=" + valcap;// + str;
    //url=url+"&amp;sid="+Math.random();
    xmlHttp2.open("GET", url, true);
    xmlHttp2.send();
    xmlHttp2.onreadystatechange = function () {
        if (xmlHttp2.readyState === 4 && xmlHttp2.status === 200) {
            var str = xmlHttp2.responseText;
            var res = str.split("-n-");
            var index;
            var tmp = "<select style=width:100% id='grupo' name='ec_group_cid_field' class='form-control' onchange=popcatec('" + valcap + "',this.value,'catec')>";
            var res2 = "";
            for (index = 0; index < res.length - 1; ++index) {
                res2 = res[index];
                res2 = res2.split("--");
                tmp = tmp + "<option value='" + res2['0'] + "'>" + res2['1'] + "</option>";
            }
            tmp = tmp + "</select>";
            document.getElementById(elemento).innerHTML = tmp;
            tmp = "";
        }
    }
}