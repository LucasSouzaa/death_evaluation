function togglePanels(content, options) {
    jQuery(content).toggle();
    jQuery(content + '_chev').toggle();
    jQuery(content + '_chevback').toggle();
    jQuery(options).toggle();
}

function loadContent(controller) {
    jQuery('#content').load(jQuery("body").data("baseurl") + 'hospitalization/' + controller);
}

// adicionando condicao do paciente no banco e atualizando lista
function add_patient_condition() {

    if (!jQuery('#patient_condition_date_hour').val()) {
        alert('É preciso adicionar a data e hora da conduta');
        return false;
    }

    jQuery.ajax({
        url: jQuery("body").data("baseurl") + "attendance_hospital/save_add_patient_condition",
        type: "post",
        dataType: 'json',
        data: {
            exit_type: jQuery('#patient_condition_type').val(),
            sent_to: jQuery('#patient_condition_local').val(),
            moment_admission: jQuery('#patient_condition_date_hour').val(),
            other_info: jQuery('#patient_condition_other_info').val()
        },
        success: function (response) {
            if (response.status == 'OK') {
                // recarrega atendimento hospitalar
                location.reload();
            } else {
                var notice = new PNotify({
                    title: 'Erro',
                    text: 'Tente novamente mais tarde',
                    type: 'error',
                    addclass: 'click-2-close',
                    hide: false,
                    buttons: {
                        closer: false,
                        sticker: false
                    }
                });
                notice.get().click(function () {
                    notice.remove();
                });
            }
        }
    });
}