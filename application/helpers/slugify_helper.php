<?php
if(!function_exists('slugify')){
    
  // https://gist.github.com/anonymous/2912227
  function slugify($str) {
      $search = array('Ș', 'Ț', 'ş', 'ţ', 'Ş', 'Ţ', 'ș', 'ț', 'î', 'â', 'ă', 'Î', 'Â', 'Ă', 'ë', 'Ë');
      $replace = array('s', 't', 's', 't', 's', 't', 's', 't', 'i', 'a', 'a', 'i', 'a', 'a', 'e', 'E');
      $str = str_ireplace($search, $replace, strtolower(trim($str)));
      $str = preg_replace('/[^\w\d\-\ ]/', '', $str);
      $str = str_replace(' ', '_', $str);
      return preg_replace('/\-{2,}/', '-', $str);
  }
}