<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html>
    <head>
        <link rel="shortcut icon" href="<?= $this->config->base_url(IMGPATH . 'favicon.png') ?>">
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <title> <?= isset($title) ? $title : lang('title') ?></title>
        <link rel="stylesheet" href="<?= $this->config->base_url(CSSPATH . 'bootstrap-responsive.min.css') ?>" />
        <link rel="stylesheet" href="<?= $this->config->base_url(CSSPATH . 'bootstrap.min.css') ?>" />
        <link rel="stylesheet" href="<?= $this->config->base_url(CSSPATH . 'bootstrap-datetimepicker.min.css') ?>" />
        <link rel="stylesheet" href="<?= $this->config->base_url(JSPATH . 'jquery.select2/select2.css') ?>" />
        <link rel="stylesheet" href="<?= $this->config->base_url(PLUGINPATH . 'magnific-popup/magnific-popup.css') ?> " />
        <link rel="stylesheet" href="<?= $this->config->base_url(FONTPATH . 'font-awesome.min.css') ?>" />
        <link rel="stylesheet" href="<?= $this->config->base_url(PLUGINPATH . 'pnotify/pnotify.custom.css') ?> " />
        <link rel="stylesheet" href="<?= $this->config->base_url(CSSPATH . 'my-theme.css') ?>" />
        <link href="<?= $this->config->base_url(CSSPATH . 'gritter/jquery.gritter.css') ?>" rel="stylesheet">
        <script type="text/javascript" src="<?= $this->config->base_url(JSPATH . "jquery.min.js") ?>"></script>
        <script type="text/javascript" src="<?= $this->config->base_url(PLUGINPATH . 'modernizr/modernizr.js') ?>"></script>
        <script type="text/javascript" src="<?= $this->config->base_url(JSPATH . "bootstrap.min.js") ?>"></script>
        <script type="text/javascript" src="<?= $this->config->base_url(JSPATH . "bootstrap-datetimepicker.min.js") ?>"></script>
        <script type="text/javascript" src="<?= $this->config->base_url(JSPATH . "jquery.select2/select2.min.js") ?>"></script>
        <script type="text/javascript" src="<?= $this->config->base_url(PLUGINPATH . 'magnific-popup/magnific-popup.js') ?>"></script>
        <script type="text/javascript" src="<?= $this->config->base_url(JSPATH . "jquery.imagemapster.min.js") ?>"></script>        
        <script type="text/javascript" src="<?= $this->config->base_url(PLUGINPATH . 'pnotify/pnotify.custom.js') ?>"></script>
        <script type="text/javascript" src="<?= $this->config->base_url(JSPATH . "home.js?") . time() ?>"></script>
        <script src="<?= $this->config->base_url(JSPATH . 'jquery.gritter.min.js') ?>"></script>
        <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.2/summernote.css" rel="stylesheet">
        <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.2/summernote.js"></script>
    </head>

    <body data-baseurl="<?= $this->config->base_url(); ?>" style="padding: 0px;">
        <nav class="navbar navbar-default navbar-fixed-top" style="margin-left: 0px; margin-right: 0px;">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-6">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?= $this->config->base_url('home') ?>">RebratsUE</a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-6">
                <ul class="nav navbar-nav">
                    <li id="home_nav"><a href="<?= $this->config->base_url('home') ?>"><i class="fa fa-home"></i> <?= lang('home') ?></a></li>
                    <li id="patients_nav"><a href="<?= $this->config->base_url('home/patients') ?>"><i class="fa fa-file"></i> <?= lang('patients') ?></a></li>
                    <li id="beds_nav"><a href="<?= $this->config->base_url('attendance') ?>"><i class="fa fa-th"></i> <?= lang('attendances') ?></a></li>
                    <li id="configuration_nav">
                        <a href="javascript:void(0)" class="dropdown-toggle" id="dropdownConfig" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            <i class="fa fa-wrench"></i> <?= lang('configurations') ?>
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="dropdownConfig">
                            <li><a href="<?= $this->config->base_url('home/institutions') ?>"><?= lang('institutions') ?></a></li>
                            <li><a href="<?= $this->config->base_url('home/teams') ?>"><?= lang('teams') ?></a></li>
                            <li><a href="<?= $this->config->base_url('home/professionals') ?>"><?= lang('professionals') ?></a></li>
                            <li><a href="<?= $this->config->base_url('home/sectors') ?>"><?= lang('sectors') ?></a></li>
                            <li><a href="<?= $this->config->base_url('home/bedrooms') ?>"><?= lang('bedrooms') ?></a></li>
                            <li><a href="<?= $this->config->base_url('home/beds') ?>"><?= lang('beds') ?></a></li>
                        </ul>
                    </li>
                    <li id="faq_nav"><a href="<?= $this->config->base_url('home/faq') ?>"><i class="fa fa-question-circle"></i> <?= lang('faq') ?></a></li>
                </ul>

                <ul class="nav navbar-nav pull-right">
                    <li id="profile_nav">
                        <a href="javascript:void(0)" class="dropdown-toggle" id="dropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            <i class="fa fa-user"></i> <?= $this->user['name'] ?>
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="dropdownProfile">
                            <li><a href="<?= $this->config->base_url('home/profile') ?>"><?= lang('my_profile') ?></a></li>
                            <li><a href="<?= $this->config->base_url('home/logout') ?>"><?= lang('logout') ?></a></li>
                        </ul>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
        <!-- fim do header -->

        <?php $this->patient = $this->session->userdata('patient'); ?>
        <!-- corpo -->
        <div class="container-fluid " style="padding-top: 30px;">
            <div class="row">
                <div class="well">
                    <h3>
                        <a href="javascript:void(0)"><?= $this->patient['name'] ?></a> 
                        <span class="badge badge-info"><?php
                            if ($this->patient['is_trauma'] === 1): echo 'Trauma';
                            else: echo 'Não Trauma';
                            endif;
                            ?></span>
                    </h3>
                    <p>
                    </p><h4><?= lang('bed') ?>: <?= $this->patient['bed'] ?><span class="badge badge-info" style=" margin-left: 10px"> <?= $this->patient['hospitalization_days'] ?> <?= lang('hospitalization_days') ?></span></h4>
                    <p></p>
                    <p>
                        <?= $this->patient['gender'] ?> - <?= $this->patient['age'] ?> - <?= lang('origin') ?>: <?= $this->patient['home_town'] ?>	</p>
                    <p>
<?= lang('admission_date') ?>: <?= $this->patient['admission_date'] ?> - <?= lang('regulation') ?>: <?= $this->patient['regulation'] ?>	</p>
                    <p>
                    <p></p>
                </div>
            </div>

            <div class="row">
                <ul class="nav nav-pills">
                    <li id="li_demo_data"><a href="<?= $this->config->base_url('attendance/demo_data') ?>"><?= lang('demographics') ?></a></li>
                    <li id="li_hospital"><a href="<?= $this->config->base_url('attendance_hospital') ?>"><?= lang('hospital_care') ?></a></li>
                    <?php if ($this->patient['hospitalization']): ?>
                        <li id="li_hospitalization"><a href="<?= $this->config->base_url('hospitalization/all_hospitalizations') ?>"><?= lang('hospitalization') ?></a></li>
                    <?php endif; ?>
                    <?php if ($this->patient['out']): ?>
                        <li id="li_outpatients"><a href="<?= $this->config->base_url('outpatients') ?>"><?= lang('outpatients') ?></a></li>
                    <?php endif; ?>
                    <?php if ($this->patient['death']): ?>
                        <li id="li_death_evaluation"><a href="<?= $this->config->base_url('death_evaluation') ?>"><?= lang('death_evaluation') ?></a></li>
<?php endif; ?>

                </ul>    
            </div>
            <div class="row">
                        <?php isset($content_hospitalization) ? $this->load->view($content_hospitalization) : '' ?>
                <div class="<?php isset($content_hospitalization) ? 'col-sm-10' : 'col-sm-12' ?>">
                    <div id="content" class="row-fluid">
<?php $this->load->view($content) ?>
                    </div>
                </div>
            </div>
        </div>

        <div class=" footer container-fluid">
            <hr>
            <div class="row-fluid">
                <div class="span12">
                    <p>Faculdade de Medicina de Ribeirão Preto - 2015</p>
                </div>
            </div>
        </div>
    </body>
</html>
