<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html>
    <head>
        <link rel="shortcut icon" href="<?= $this->config->base_url(IMGPATH . 'favicon.png') ?>">
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <title> <?= isset($title) ? $title : lang('title') ?></title>
        <link rel="stylesheet" href="<?= $this->config->base_url(CSSPATH . 'bootstrap-responsive.min.css') ?>" />
        <link rel="stylesheet" href="<?= $this->config->base_url(CSSPATH . 'bootstrap.min.css') ?>" />
        <link rel="stylesheet" href="<?= $this->config->base_url(CSSPATH . 'bootstrap-datetimepicker.min.css') ?>" />
        <link rel="stylesheet" href="<?= $this->config->base_url(JSPATH . 'jquery.select2/select2.css') ?>" />
        <link rel="stylesheet" href="<?= $this->config->base_url(PLUGINPATH . 'magnific-popup/magnific-popup.css') ?> " />
        <link rel="stylesheet" href="<?= $this->config->base_url(FONTPATH . 'font-awesome.min.css') ?>" />
        <link rel="stylesheet" href="<?= $this->config->base_url(PLUGINPATH . 'pnotify/pnotify.custom.css') ?> " />
        <link rel="stylesheet" href="<?= $this->config->base_url(CSSPATH . 'my-theme.css') ?>" />
        <link href="<?= $this->config->base_url(CSSPATH . 'gritter/jquery.gritter.css') ?>" rel="stylesheet">
        <link rel="stylesheet" href="<?= $this->config->base_url(VENDORPATH . 'jquery-datatables-bs3/assets/css/datatables.css') ?>" />
        <script type="text/javascript" src="<?= $this->config->base_url(JSPATH . "jquery.min.js") ?>"></script>
        <script type="text/javascript" src="<?= $this->config->base_url(PLUGINPATH . 'modernizr/modernizr.js') ?>"></script>
        <script type="text/javascript" src="<?= $this->config->base_url(JSPATH . "bootstrap.min.js") ?>"></script>
        <script type="text/javascript" src="<?= $this->config->base_url(JSPATH . "bootstrap-datetimepicker.min.js") ?>"></script>
        <script type="text/javascript" src="<?= $this->config->base_url(JSPATH . "jquery.select2/select2.min.js") ?>"></script>
        <script type="text/javascript" src="<?= $this->config->base_url(PLUGINPATH . 'magnific-popup/magnific-popup.js') ?>"></script>
        <script type="text/javascript" src="<?= $this->config->base_url(JSPATH . "jquery.imagemapster.min.js") ?>"></script>        
        <script type="text/javascript" src="<?= $this->config->base_url(PLUGINPATH . 'pnotify/pnotify.custom.js') ?>"></script>
        <script type="text/javascript" src="<?= $this->config->base_url(JSPATH . "home.js?") . time() ?>"></script>
        <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.2/summernote.css" rel="stylesheet">
        <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.2/summernote.js"></script>
        <script src="<?= $this->config->base_url(JSPATH . 'jquery.gritter.min.js') ?>"></script>
        <script src="<?= $this->config->base_url(VENDORPATH . 'jquery-datatables/media/js/jquery.dataTables.js') ?>"></script>
        <script src="<?= $this->config->base_url(VENDORPATH . 'jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js') ?>"></script>
        <script src="<?= $this->config->base_url(VENDORPATH . 'jquery-datatables-bs3/assets/js/datatables.js') ?>"></script>
        <script src="<?= $this->config->base_url(JSPATH . 'tables/examples.datatables.tabletools.js') ?>"></script>  
    </head>

    <body data-baseurl="<?= $this->config->base_url(); ?>" style="padding: 0px;">
        <nav class="navbar navbar-default navbar-fixed-top" style="margin-left: 0px; margin-right: 0px;">

            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-6">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?= $this->config->base_url('home') ?>">RebratsUE</a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-6">
                <ul class="nav navbar-nav">
                    <li id="home_nav"><a href="<?= $this->config->base_url('home') ?>"><i class="fa fa-home"></i> <?= lang('home') ?></a></li>
                    <li id="patients_nav"><a href="<?= $this->config->base_url('home/patients') ?>"><i class="fa fa-file"></i> <?= lang('patients') ?></a></li>
                    <li id="beds_nav"><a href="<?= $this->config->base_url('attendance') ?>"><i class="fa fa-th"></i> <?= lang('attendances') ?></a></li>
                    <li id="configuration_nav">
                        <a href="javascript:void(0)" class="dropdown-toggle" id="dropdownConfig" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            <i class="fa fa-wrench"></i> <?= lang('configurations') ?>
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="dropdownConfig">
                            <li><a href="<?= $this->config->base_url('home/institutions') ?>"><?= lang('institutions') ?></a></li>
                            <li><a href="<?= $this->config->base_url('home/teams') ?>"><?= lang('teams') ?></a></li>
                            <li><a href="<?= $this->config->base_url('home/professionals') ?>"><?= lang('professionals') ?></a></li>
                            <li><a href="<?= $this->config->base_url('home/sectors') ?>"><?= lang('sectors') ?></a></li>
                            <li><a href="<?= $this->config->base_url('home/bedrooms') ?>"><?= lang('bedrooms') ?></a></li>
                            <li><a href="<?= $this->config->base_url('home/beds') ?>"><?= lang('beds') ?></a></li>
                        </ul>
                    </li>
                    <li id="faq_nav"><a href="<?= $this->config->base_url('home/faq') ?>"><i class="fa fa-question-circle"></i> <?= lang('faq') ?></a></li>
                </ul>

                <ul class="nav navbar-nav pull-right">
                    <li id="profile_nav">
                        <a href="javascript:void(0)" class="dropdown-toggle" id="dropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            <i class="fa fa-user"></i> <?= $this->user['name'] ?>
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="dropdownProfile">
                            <li><a href="<?= $this->config->base_url('user/profile') ?>"><?= lang('my_profile') ?></a></li>
                            <li><a href="<?= $this->config->base_url('home/logout') ?>"><?= lang('logout') ?></a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
        <div class="container-fluid " style="padding-top: 30px;">
            <?php $this->load->view($content) ?>
        </div>
        <div class=" footer container-fluid">
            <hr>
            <div class="row-fluid">
                <div class="span12">
                    <p>Faculdade de Medicina de Ribeirão Preto - 2015</p>
                </div>
            </div>
        </div>
    </body>
    <script src="<?= $this->config->base_url(JSPATH . 'examples.modals.js') ?>"></script>
</html>