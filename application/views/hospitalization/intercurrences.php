<div id="new_intercurrence" hidden="">
    <div class="container-fluid">
        <legend><?= lang('add_intercurrence') ?></legend>

        <div id="use_service_beds"></div>

        <div class="well">
            <h5><label class="control-label"><?= lang('resume') ?></label></h5>
            <div class="form-group">
                <div style="height: 100px" class="summernote" id="resume"><?php
                    if (isset($resume)): echo $resume;
                    endif;
                    ?></div>
            </div>

            <h5><label class="control-label"><?= lang('conduct') ?></label></h5>
            <div class="form-group">
                <select class="form-control" id="conduct">
                    <option value="avaliacao" selected>Em atendimento/avaliação</option>
                    <option value="observacao">Em observação clínica</option>
                    <option value="aguarda_leito">Aguardando leito para internação</option>
                    <option value="alta">Alta</option>
                    <option value="alta_a_pedido">Alta a pedido</option>
                    <option value="transferencia">Transferência</option>
                    <option value="internado">Internado</option>
                    <option value="reinternado">Re-Internado</option>
                    <option value="obito">Óbito</option>
                </select>
            </div>

            <div class="row-fluid">
                <div class="btn-group pull-right" style=" margin-top:20px;">
                    <a class="btn btn-danger btn-sm" href="javascript:void(0)" onclick="jQuery('#new_intercurrence').toggle();
                            jQuery('.summernote').summernote('code', '');"><?= lang('close') ?></a>
                    <a class="btn btn-success btn-sm" href="javascript:void(0)" onclick="save_intercurrence()" > <?= lang('save') ?></a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="search-filter">
    <legend><?= lang('intercurrences') ?>
        <a class="mb-xs mt-xs mr-xs btn btn-success" href="javascript:void(0)" onclick="jQuery('#new_intercurrence').toggle()" style="float: right"> <i class="fa fa-plus-square-o"></i> <?= lang('add_intercurrence') ?></a>
    </legend>        
</div>

<div class="well">
    <?php if (!empty($passages)): ?>
        <table class="table table-bordered table-striped mb-none" >
            <thead>
                <tr>
                    <th><?= lang('datetime_realization') ?></th>
                    <th><?= lang('author') ?></th>
                    <th><?= lang('resume') ?></th>
                    <th><?= lang('conduct') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($passages as $e): ?>
                    <tr>
                        <th><?= $e['created'] ?></th>
                        <th><?= $e['name'] ?></th>
                        <th><?= $e['resume'] ?></th>
                        <th><?= $e['conduct'] ?></th>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php else: ?>
        <h3>Nenhuma intercorrência cadastrada</h3>
    <?php endif; ?>
</div>

<input hidden="" id="passage_id" value=""/>

<script src="<?= $this->config->base_url(JSPATH . 'examples.modals.js') ?>"></script>

<script>

                        jQuery(document).ready(function () {
                            jQuery('.summernote').summernote({
                                height: 150
                            });
                            jQuery('#use_service_beds').load(jQuery("body").data("baseurl") + "hospitalization/use_service_beds");
                        });
                        function save_intercurrence() {
                            jQuery.ajax({
                                url: jQuery("body").data("baseurl") + "hospitalization/save_intercurrence",
                                type: "post",
                                dataType: 'json',
                                data: {
                                    conduct: jQuery('#conduct').val(),
                                    resume: jQuery('#resume').summernote('code')
                                },
                                success: function (response) {
                                    jQuery('#modal_close_button').click();
                                    if (response.status === 'NOK') {
                                        var notice = new PNotify({
                                            title: 'Erro',
                                            text: 'Tente novamente mais tarde',
                                            type: 'error',
                                            addclass: 'click-2-close',
                                            hide: false,
                                            buttons: {
                                                closer: false,
                                                sticker: false
                                            }
                                        });
                                        notice.get().click(function () {
                                            notice.remove();
                                        });
                                    } else {
                                        // recarrega lista de intercorrências
                                        jQuery('#hospitalization_steps').load(jQuery("body").data("baseurl") + "hospitalization/intercurrences");
                                    }

                                }
                            });
                        }
</script>
