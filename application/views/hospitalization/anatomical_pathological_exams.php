<style>
    .bootstrap-datetimepicker-widget{z-index:11510 !important;}
</style>

<div class="search-filter">
    <legend><?= lang('anatomical_pathological_exams') ?>
        <a class="mb-xs mt-xs mr-xs modal-basic btn btn-success" href="#modalAddAnatomicalPathologicalExams" style="float: right"> <i class="fa fa-plus-square-o"></i> <?= lang('add') ?></a>
    </legend>        
</div>

<div class="well">
    <?php if (!empty($anatomical_pathological_exams)): ?>
        <table class="table table-bordered table-striped mb-none" >
            <thead>
                <tr>
                    <th><?= lang('nature_material') ?></th>
                    <th><?= lang('reception_date') ?></th>
                    <th><?= lang('liberation_date') ?></th>
                    <th><?= lang('diagnosys') ?></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($anatomical_pathological_exams as $e): ?>
                    <tr>
                        <th><?= $e['nature_material'] ?></th>
                        <th><?= $e['reception_date'] ?></th>
                        <th><?= $e['liberation_date'] ?></th>
                        <th><?= $e['diagnosys'] ?></th>
                        <th><a href="javascript:void(0)" onclick="delete_anatomical_pathological_exam('<?= $this->encrypt->encode($e['id']) ?>')" class="on-default remove-row" style=" margin-right: 5px; margin-left: 5px;"><i class="fa fa-trash-o"></i></a></th>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php else: ?>
        <h3>Nenhum exame cadastrado</h3>
    <?php endif; ?>
</div>


<div id="modalAddAnatomicalPathologicalExams" class="modal-block mfp-hide" style="width: 80%;">
    <section class="panel">
        <header class="panel-heading">
            <h2 class="panel-title"><?= lang('add_anatomical_pathological_exam') ?></h2>
        </header>
        <div class="panel-body">
            <div class="modal-wrapper">
                <div class="well">
                    <div class="row-fluid">
                        <label><h4>Dados do exame</h4></label>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <h5><label class="control-label"><?= lang('nature_material') ?></label></h5>
                            <input id="nature_material" type="text" class="form-control"/> 
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span6">
                            <h5><label class="control-label"><?= lang('reception_date') ?></label></h5>
                            <input id="reception_date" type="datetime-local" class="form-control"/> 
                        </div>
                        <div class="span6">
                            <h5><label class="control-label"><?= lang('liberation_date') ?></label></h5>
                            <input id="liberation_date" type="datetime-local" class="form-control"/> 
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <h5><label class="control-label"><?= lang('diagnosys') ?></label></h5>
                            <textarea id="diagnosys" class="form-control"></textarea> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer class="panel-footer">
            <div class="row">
                <div class="col-md-12 text-right">
                    <button class="btn btn-primary" onclick="save_anatomical_pathological_exam()"><?= lang('save') ?></button>
                    <button class="btn btn-default modal-dismiss" id="modal_close_button"><?= lang('close') ?></button>
                </div>
            </div>
        </footer>
    </section>
</div>

<script src="<?= $this->config->base_url(JSPATH . 'examples.modals.js') ?>"></script>

<script>
                        function save_anatomical_pathological_exam() {

                            if (!jQuery('#nature_material').val()) {
                                var notice = new PNotify({
                                    title: 'Erro',
                                    text: 'O material precisa ser preenchido',
                                    type: 'error',
                                    addclass: 'click-2-close',
                                    hide: false,
                                    buttons: {
                                        closer: false,
                                        sticker: false
                                    }
                                });
                                notice.get().click(function () {
                                    notice.remove();
                                });
                                return false;
                            }

                            if (!jQuery('#reception_date').val()) {
                                var notice = new PNotify({
                                    title: 'Erro',
                                    text: 'A data de recepção precisa ser preenchida',
                                    type: 'error',
                                    addclass: 'click-2-close',
                                    hide: false,
                                    buttons: {
                                        closer: false,
                                        sticker: false
                                    }
                                });
                                notice.get().click(function () {
                                    notice.remove();
                                });
                                return false;
                            }

                            jQuery.ajax({
                                url: jQuery("body").data("baseurl") + "hospitalization/save_anatomical_pathological_exam",
                                type: "post",
                                dataType: 'json',
                                data: {
                                    nature_material: jQuery('#nature_material').val(),
                                    reception_date: jQuery('#reception_date').val(),
                                    liberation_date: jQuery('#liberation_date').val(),
                                    diagnosys: jQuery('#diagnosys').val()
                                },
                                success: function (response) {
                                    jQuery('#modal_close_button').click();
                                    if (response.status === 'NOK') {
                                        var notice = new PNotify({
                                            title: 'Erro',
                                            text: 'Tente novamente mais tarde',
                                            type: 'error',
                                            addclass: 'click-2-close',
                                            hide: false,
                                            buttons: {
                                                closer: false,
                                                sticker: false
                                            }
                                        });
                                        notice.get().click(function () {
                                            notice.remove();
                                        });
                                    } else {
                                        // recarrega lista de exames
                                        jQuery('#hospitalization_steps').load(jQuery("body").data("baseurl") + "hospitalization/anatomical_pathological_exams");
                                    }
                                }
                            });
                        }

                        function delete_anatomical_pathological_exam(id) {
                            jQuery.ajax({
                                url: jQuery("body").data("baseurl") + "hospitalization/delete_anatomical_pathological_exam",
                                type: "post",
                                dataType: 'json',
                                data: {
                                    id: id
                                },
                                success: function (response) {
                                    if (response.status === 'NOK') {
                                        var notice = new PNotify({
                                            title: 'Erro',
                                            text: 'Tente novamente mais tarde',
                                            type: 'error',
                                            addclass: 'click-2-close',
                                            hide: false,
                                            buttons: {
                                                closer: false,
                                                sticker: false
                                            }
                                        });
                                        notice.get().click(function () {
                                            notice.remove();
                                        });
                                    } else {
                                        // recarrega lista de exames
                                        jQuery('#hospitalization_steps').load(jQuery("body").data("baseurl") + "hospitalization/anatomical_pathological_exams");
                                    }
                                }
                            });
                        }

</script>
