<!-- Condicao de alta -->
<legend>Condição de alta</legend>
<div id="attend_sit_options" class="btn-group" style="width: 100%">
    <div class="well">

        <div class="row-fluid">
            <div class="col-sm-6">
                <h5><label class="control-label">Condicão atual do paciente</label></h5>
                <label class="control-label" style="font-weight: 500;"><?= $final_condition['exit_type'] ?></label>
            </div>
            <div class="col-sm-6">
                <h5><label class="control-label">Data/Hora de saída</label></h5>
                <label class="control-label" style="font-weight: 500;"><?= $final_condition['moment_admission'] ?></label>
            </div>
        </div>

        <?php if ($final_condition['exit_type'] != 'alta' && $final_condition['exit_type'] != 'alta_a_pedido'): ?>        
            <div class="row-fluid">
                <div class="col-sm-4">
                    <h5><label class="control-label">Condição do paciente</label></h5>
                    <select class="form-control" id="condition">
                        <option value="alta">Alta hospitalar</option>
                        <option value="alta_a_pedido">Alta a pedido</option>
                        <option value="transferencia">Transferência</option>
                        <option value="obito">Óbito</option>
                    </select>
                </div>
                <div class="col-sm-4">
                    <h5><label class="control-label">Data/Hora</label></h5>
                    <div class='input-group date input-append datetimepicker1'>
                        <input type='text' class="form-control" data-format="yyyy-MM-dd hh:mm:ss" id="moment_admission" />
                        <span class="input-group-addon add-on">
                            <span><i class="fa fa-calendar" data-time-icon="icon-time" data-date-icon="icon-calendar">
                                </i></span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="row-fluid">
                <div class="col-sm-4 orientations">
                    <h5><label class="control-label">Orientação para o paciente</label></h5>
                    <textarea id="patient_orientation"class="form-control"></textarea>
                </div>
                <div class="col-sm-4 orientations">
                    <h5><label class="control-label">Orientação para a família</label></h5>
                    <textarea id="family_orientation"class="form-control"></textarea>
                </div>
                <div class="col-sm-4 orientations">
                    <h5><label class="control-label">Orientação de retorno</label></h5>
                    <textarea id="return_orientation"class="form-control"></textarea>
                </div>
            </div>
            <div class="row-fluid">
                <div class="col-sm-4 orientations">
                    <h5><label class="control-label">Agendamento Data/Hora</label></h5>
                    <div class='input-group date input-append datetimepicker1'>
                        <input type='text' class="form-control" data-format="yyyy-MM-dd hh:mm:ss" id="return_date" />
                        <span class="input-group-addon add-on">
                            <span><i class="fa fa-calendar" data-time-icon="icon-time" data-date-icon="icon-calendar">
                                </i></span>
                        </span>
                    </div>
                </div>
                <div class="col-sm-4 orientations">
                    <h5><label class="control-label">Ambulatório</label></h5>
                    <select class="form-control" id="institution_id" >
                        <?php foreach ($institutions as $i): ?>
                            <option value="<?= $this->encrypt->encode($i['id']) ?>"><?= $i['name'] ?></option>
                        <?php endforeach; ?>    
                    </select>
                </div>
            </div>
            <div class="row-fluid">
                <div class="col-sm-12">
                    </br>
                    <a class="btn btn-success btn-block" href="javascript:void(0)" onclick="add_patient_condition_final()" > <?= lang('save') ?> nova condição</a>
                </div>
            </div>
        <?php endif; ?>
        
        <?php if ($final_condition['exit_type'] === 'alta' || $final_condition['exit_type'] === 'alta_a_pedido'): ?>
            <div class="row-fluid">
                <div class="col-sm-12">
                    <h5><label class="control-label">Orientação para o paciente</label></h5>
                    <label class="control-label" style="font-weight: 500;"><?= $final_condition['patient_orientation'] ?></label>
                </div>
            </div>
            <div class="row-fluid">
                <div class="col-sm-12">
                    <h5><label class="control-label">Orientação para a família</label></h5>
                    <label class="control-label" style="font-weight: 500;"><?= $final_condition['family_orientation'] ?></label>
                </div>
            </div>
            <div class="row-fluid">
                <div class="col-sm-12">
                    <h5><label class="control-label">Orientação de retorno</label></h5>
                    <label class="control-label" style="font-weight: 500;"><?= $final_condition['return_orientation'] ?></label>
                </div>
            </div>
            <div class="row-fluid">
                <div class="col-sm-6">
                    <h5><label class="control-label">Data de retorno</label></h5>
                    <label class="control-label" style="font-weight: 500;"><?= $final_condition['return_date'] ?></label>
                </div>
                <div class="col-sm-6">
                    <h5><label class="control-label">Ambulatório</label></h5>
                    <label class="control-label" style="font-weight: 500;"><?= $final_condition['name'] ?></label>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>

<script type="text/javascript">
    $(function () {
        $('.datetimepicker1').datetimepicker({
            language: 'pt-BR'
        });
    });
</script>
<script>
    $(document).on('change', '#condition', function () {
        if ($('#condition').val() === 'alta') {
            jQuery('.orientations').show();
        } else if ($('#condition').val() === 'alta_a_pedido') {
            jQuery('.orientations').show();
        } else {
            jQuery('.orientations').hide();
        }
    });

    // adicionando condicao do paciente no banco
    function add_patient_condition_final() {

        jQuery.ajax({
            url: jQuery("body").data("baseurl") + "hospitalization/add_patient_condition_final",
            type: "post",
            dataType: 'json',
            data: {
                exit_type: jQuery('#condition').val(),
                moment_admission: jQuery('#moment_admission').val(),
                patient_orientation: jQuery('#patient_orientation').val(),
                family_orientation: jQuery('#family_orientation').val(),
                return_orientation: jQuery('#return_orientation').val(),
                return_date: jQuery('#return_date').val(),
                institution_id: jQuery('#institution_id').val()
            },
            success: function (response) {
                if (response.status === 'NOK') {
                    var notice = new PNotify({
                        title: 'Erro',
                        text: 'Tente novamente mais tarde',
                        type: 'error',
                        addclass: 'click-2-close',
                        hide: false,
                        buttons: {
                            closer: false,
                            sticker: false
                        }
                    });
                    notice.get().click(function () {
                        notice.remove();
                    });
                } else {
                    // recarrega tela de condicao do paciente
                    jQuery('#hospitalization_steps').load(jQuery("body").data("baseurl") + "hospitalization/outpatient_condition");

                    // se mudar a sessao recarrega a pagina para aparecer as abas
                    if (jQuery('#condition').val() === 'obito') {
                        location.reload();
                    } else if (jQuery('#condition').val() === 'alta' || jQuery('#condition').val() === 'alta_a_pedido') {
                        location.reload();
                    }
                }
            }
        });
    }
</script>