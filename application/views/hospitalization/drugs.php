<div class="search-filter">
    <legend>Medicamentos administrados</legend>        
</div>   

<div class="row-fluid">
    <div class="col-sm-6">
        <h5><label class="control-label"><?= lang('') ?> Nome do medicamento</label></h5>
        <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="attendance_drug_id">
            <option value="0" selected > - </option>
            <?php foreach ($drugs as $dr): ?>
                <option value="<?= $dr['id'] ?>"><?= $dr['drug_name'] ?></option>
            <?php endforeach; ?>
        </select>
    </div>

    <div class="col-sm-2">
        <h5><label class="control-label"><?= lang('') ?> Dose</label></h5>
        <input id="attendance_drug_dose" type="text" class="form-control"/>
    </div>

    <h5><label class="control-label"><?= lang('') ?> Unidade</label></h5>
    <div class="col-sm-2">
        <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="attendance_drug_unit">
            <option value="none" selected > - </option>
            <option value="ml"> ml</option>
            <option value="pilula"> Pilulas</option>
            <option value="ampola"> Ampolas</option>
        </select>
    </div>
    <div class="col-sm-2">
        <a onclick="add_drug_to_patient()" class="btn btn-primary btn-block"><i class="fa fa-plus"></i></a>
    </div>
</div>
</br>

<div class="well">
    <?php if (!empty($admindrugs)): ?>
        <table id="drugstable" class="table table-bordered table-striped mb-none">
            <thead>
                <tr>
                    <th><?= lang('drug_name') ?></th>
                    <th><?= lang('') ?> Quantidade</th>
                    <th><?= lang('') ?> Unidade de medida</th>
                    <th><?= lang('') ?> Administrado a:</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($admindrugs as $aad): ?>
                    <tr>
                        <th><?= $aad['drug_name'] ?></th>
                        <th><?= $aad['dose'] ?></th>
                        <th><?= $aad['unit'] ?></th>
                        <th><?= $aad['days']->format('%d dias %h horas e %i minutos') ?></th>
                        <th><a href="javascript:;" onclick="del_administered_drug(<?= $aad['id'] ?>)" class="on-default remove-row" style=" margin-right: 5px; margin-left: 5px;"><i class="fa fa-trash-o"></i></a></th>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php else: ?>
        <h3>Nenhum medicamento administrado</h3>
    <?php endif; ?>
</div>


<script>
    //medicamentos administrados
    function add_drug_to_patient() {
        jQuery.ajax({
            url: jQuery("body").data("baseurl") + "hospitalization/add_administered_drug",
            type: "post",
            dataType: 'json',
            data: {
                drug_id: jQuery('#attendance_drug_id').val(),
                dose: jQuery('#attendance_drug_dose').val(),
                unit: jQuery('#attendance_drug_unit').val(),
                context: 'hospitalization'
            },
            success: function (response) {
                if (response.status === 'OK') {
                    jQuery('#hospitalization_steps').load(jQuery("body").data("baseurl") + "hospitalization/drugs");
                } else {
                    var notice = new PNotify({
                        title: 'Erro',
                        text: 'Tente novamente mais tarde',
                        type: 'error',
                        addclass: 'click-2-close',
                        hide: false,
                        buttons: {
                            closer: false,
                            sticker: false
                        }
                    });
                    notice.get().click(function () {
                        notice.remove();
                    });
                }
            }
        });
    }

    function del_administered_drug(id) {
        jQuery.ajax({
            url: jQuery("body").data("baseurl") + "hospitalization/delete_administered_drug",
            type: "post",
            dataType: 'json',
            data: {
                id: id
            },
            success: function (response) {
                if (response.status === 'OK') {
                    jQuery('#hospitalization_steps').load(jQuery("body").data("baseurl") + "hospitalization/drugs");
                } else {
                    var notice = new PNotify({
                        title: 'Erro',
                        text: 'Tente novamente mais tarde',
                        type: 'error',
                        addclass: 'click-2-close',
                        hide: false,
                        buttons: {
                            closer: false,
                            sticker: false
                        }
                    });
                    notice.get().click(function () {
                        notice.remove();
                    });
                }
            }
        });
    }
</script>
