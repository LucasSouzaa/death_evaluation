<style>
    .bootstrap-datetimepicker-widget{z-index:11510 !important;}
</style>

<div class="search-filter">
    <legend><?= lang('surgeries') ?>
        <a class="mb-xs mt-xs mr-xs modal-basic btn btn-success" href="#modalAddSurgery" style="float: right"> <i class="fa fa-plus-square-o"></i> <?= lang('add') ?></a>
    </legend>        
</div>

<div class="well">
    <?php if (!empty($surgeries)): ?>
        <table class="table table-bordered table-striped mb-none" >
            <thead>
                <tr>
                    <th><?= lang('name') ?></th>
                    <th><?= lang('solicitation_date') ?></th>
                    <th><?= lang('team') ?></th>
                    <th><?= lang('realization_date') ?></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($surgeries as $e): ?>
                    <tr>
                        <th><?= $e['name'] ?></th>
                        <th><?= $e['created'] ?></th>
                        <th><?= $e['team'] ?></th>
                        <th><?= $e['realization_date'] ?></th>
                        <th><a href="javascript:void(0)" onclick="delete_surgery('<?= $this->encrypt->encode($e['id']) ?>')" class="on-default remove-row" style=" margin-right: 5px; margin-left: 5px;"><i class="fa fa-trash-o"></i></a></th>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php else: ?>
        <h3>Nenhuma cirurgia cadastrada</h3>
    <?php endif; ?>
</div>


<div id="modalAddSurgery" class="modal-block mfp-hide" style="width: 80%;">
    <section class="panel">
        <header class="panel-heading">
            <h2 class="panel-title"><?= lang('add_surgery') ?></h2>
        </header>
        <div class="panel-body">
            <div class="modal-wrapper">
                <div class="well">
                    <div class="row-fluid">
                        <label><h4>Dados da cirurgia</h4></label>
                    </div>
                    <div class="row-fluid">
                        <div class="span4">
                            <h5><label class="control-label">Tipos de cirurgia</label></h5>
                            <select class="form-control" id="surgery_id">
                                <option value="-">-</option>
                                <?php foreach ($c_surgeries as $cs): ?>
                                    <option value="<?= $cs['id'] ?>"><?= $cs['name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="span4">
                            <h5><label class="control-label">Data/Hora da realização</label></h5>
                            <div class='input-group date input-append datetimepicker1'>
                                <input type='text' class="form-control" data-format="yyyy-MM-dd hh:mm:ss" id="realization_date" />
                                <span class="input-group-addon add-on">
                                    <span><i class="fa fa-calendar" data-time-icon="icon-time" data-date-icon="icon-calendar">
                                        </i></span>
                                </span>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="row-fluid">
                    <label>
                        <input type="checkbox" onclick="jQuery('#add_professionals').toggle()">
                        Escolher profissionais
                    </label>
                </div>
                <div class="row-fluid" id="add_professionals" style="display: none;">
                    <!-- Regiao para adicionar exames complementares-->
                    <!-- colocar somente os exames que serao mostrados-->
                    <div class="well">
                        <div class="row-fluid">
                            <label><h4>Profissionais</h4></label>
                        </div>
                        <div class="row-fluid profissionais_time">
                            <?php foreach ($professionals as $p): ?>
                                <div class="span4">
                                    <label>
                                        <input type="checkbox" value="<?= $p['id'] ?>" >
                                        <?= $p['name'] ?> 
                                    </label>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>	
                </div>

            </div>
        </div>
        <footer class="panel-footer">
            <div class="row">
                <div class="col-md-12 text-right">
                    <button class="btn btn-primary" onclick="save_surgery()"><?= lang('save') ?></button>
                    <button class="btn btn-default modal-dismiss" id="modal_close_button"><?= lang('close') ?></button>
                </div>
            </div>
        </footer>
    </section>
</div>
<script src="<?= $this->config->base_url(JSPATH . 'examples.modals.js') ?>"></script>
<script>
                        $(function () {
                            $('.datetimepicker1').datetimepicker({
                                language: 'pt-BR'
                            });
                        });
                        function save_surgery() {
                            var team = [];
                            jQuery('.profissionais_time input:checked').each(function (element) {
                                team.push(jQuery(this).val());
                            });

                            jQuery.ajax({
                                url: jQuery("body").data("baseurl") + "hospitalization/save_surgery",
                                type: "post",
                                dataType: 'json',
                                data: {
                                    c_surgeries_id: jQuery('#surgery_id').val(),
                                    realization_date: jQuery('#realization_date').val(),
                                    team: team
                                },
                                success: function (response) {
                                    jQuery('#modal_close_button').click();
                                    if (response.status === 'NOK') {
                                        var notice = new PNotify({
                                            title: 'Erro',
                                            text: 'Tente novamente mais tarde',
                                            type: 'error',
                                            addclass: 'click-2-close',
                                            hide: false,
                                            buttons: {
                                                closer: false,
                                                sticker: false
                                            }
                                        });
                                        notice.get().click(function () {
                                            notice.remove();
                                        });
                                    } else {
                                        // recarrega lista de cirurgias
                                        jQuery('#hospitalization_steps').load(jQuery("body").data("baseurl") + "hospitalization/surgery_anesthesia");
                                    }
                                }
                            });
                        }

                        function delete_surgery(id) {
                            jQuery.ajax({
                                url: jQuery("body").data("baseurl") + "hospitalization/delete_surgery",
                                type: "post",
                                dataType: 'json',
                                data: {
                                    id: id
                                },
                                success: function (response) {
                                    if (response.status === 'NOK') {
                                        var notice = new PNotify({
                                            title: 'Erro',
                                            text: 'Tente novamente mais tarde',
                                            type: 'error',
                                            addclass: 'click-2-close',
                                            hide: false,
                                            buttons: {
                                                closer: false,
                                                sticker: false
                                            }
                                        });
                                        notice.get().click(function () {
                                            notice.remove();
                                        });
                                    } else {
                                        // recarrega lista de cirurgias
                                        jQuery('#hospitalization_steps').load(jQuery("body").data("baseurl") + "hospitalization/surgery_anesthesia");
                                    }
                                }
                            });
                        }

</script>
