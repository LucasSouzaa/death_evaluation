<div class="search-filter">
    <legend><?= lang('biochemistry_exams') ?>
                    <a class="mb-xs mt-xs mr-xs modal-basic btn btn-success" href="#modalAddBiochemistryExam" style="float: right"> <i class="fa fa-plus-square-o"></i> <?= lang('add_exam') ?></a>
    </legend>
</div>

<div class="well">
    <?php if (!empty($biochemistry_exams)): ?>
        <table class="table table-bordered table-striped mb-none" >
            <thead>
                <tr>
                    <th><?= lang('exam') ?></th>
                    <th><?= lang('solicitation_date') ?></th>
                    <th><?= lang('result_date') ?></th>
                    <th><?= lang('result') ?></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($biochemistry_exams as $e): ?>
                    <tr>
                        <th><?= $e['name'] ?></th>
                        <th><?= $e['created'] ?></th>
                        <th><?php
                            if (isset($e['result_date'])): echo $e['result_date'];
                            endif;
                            ?></th>
                        <th style="width: 15%; text-align: center;"><?php
                            if (isset($e['result'])): echo $e['result'];
                            else:
                                ?>
                                <a class="mb-xs mt-xs mr-xs modal-basic btn btn-success" href="#modalAddResult" onclick="jQuery('#exam_id').val(<?= $e['id'] ?>)"> <i class="fa fa-plus-square-o"></i> <?= lang('add_result') ?></a>
                            <?php endif ?></th>
                        <th><a href="javascript:void(0)" onclick="delete_biochemistry_exam('<?= $this->encrypt->encode($e['id']) ?>')" class="on-default remove-row" style=" margin-right: 5px; margin-left: 5px;"><i class="fa fa-trash-o"></i></a></th>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php else: ?>
        <h3>Nenhuma exame cadastrado</h3>
    <?php endif; ?>
</div>

<input id="exam_id" hidden=""/>

<div id="modalAddResult" class="modal-block mfp-hide" style="width: 80%;">
    <section class="panel">
        <header class="panel-heading">
            <h2 class="panel-title"><?= lang('add_result') ?></h2>
        </header>
        <div class="panel-body">
            <div class="modal-wrapper">
                <div class="well">
                    <div class="row-fluid">
                        <div class="span6">
                            <h5><label class="control-label">Valor</label></h5>
                            <input class="form-control" id="result" />
                        </div>
                        <div class="span6">
                            <h5><label class="control-label">Data/Hora da realização</label></h5>
                            <input id="result_date" type="datetime-local" class="form-control"/> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer class="panel-footer">
            <div class="row">
                <div class="col-md-12 text-right">
                    <button class="btn btn-primary" onclick="save_bio_exam_result()"><?= lang('save') ?></button>
                    <button class="btn btn-default modal-dismiss" id="modal_close_button"><?= lang('close') ?></button>
                </div>
            </div>
        </footer>
    </section>
</div>

<div id="modalAddBiochemistryExam" class="modal-block mfp-hide" style="width: 80%;">
    <section class="panel">
        <header class="panel-heading">
            <h2 class="panel-title"><?= lang('add_biochemistry_exam') ?></h2>
        </header>
        <div class="panel-body">
            <div class="modal-wrapper">
                <div class="row-fluid">
                    <label>
                        <input id="habilita_exames" type="checkbox"> Solicitação de exames pela classificação de pacientes por cores:
                    </label>
                </div>
                <div class="row-fluid" style="padding-left: 20px;">
                    <div class="span4">

                        <label class="radio inline">
                            <input type="radio" name="tipo_trauma" value="amarelo" disabled="true">Trauma amarelo
                            <a href="#" class="icon-question" data-toggle="popover" data-placement="bottom" data-html="true" data-title="Trauma amarelo" data-trigger="hover" data-content="
                               <ul>
                               <li><b>Características fisiológicas</b></li>
                               <ul>
                               <li>Escore de coma de Glasgow = 14 e 15</li>
                               <li>Ausência de sinais precoces de choque circulatório</li>
                               <li>Pressão arterial sistolica > 100 mmHg</li>
                               <li>Saturação de O2 >= 94%</li>
                               </ul>
                               <li><b>Características anatômicas</b></li>
                               <ul>
                               <li>Com sinais pouco significativos de trauma na cabeça tórax, abdome ou fratura proximal de ossos longos</li>
                               </ul>
                               </ul>" data-original-title="" title=""></a>
                        </label>

                    </div>
                    <div class="span4">
                        <label for="" class="radio inline">
                            <input type="radio" name="tipo_trauma" value="laranja" disabled="true">Trauma laranja
                            <a href="#" class="icon-question" data-toggle="popover" data-placement="bottom" data-html="true" data-title="Trauma laranja" data-trigger="hover" data-content="
                               <ul>
                               <li><b>Características fisiológicas</b></li>
                               <ul>
                               <li>Escore de coma de Glasgow <= 13</li>
                               <li>Ausência de sinais precoces de choque circulatório</li>
                               <li>Pressão arterial sistolica > 100 mmHg</li>
                               <li>Saturação de O2 >= 94%</li>
                               </ul>
                               <li><b>Características anatômicas</b></li>
                               <ul>
                               <li>Com sinais pouco significativos de trauma na cabeça sem necessidade de intubação traqueal,tórax no máximo, com necessidade de drenagem pleural < 300ml, abdome sem suspeita de lesão ou com lesões ortopédicas que necessitem de procedimento cirúrgico, exceto fratural proximal de ossos longos</li>
                               </ul>
                               </ul>" data-original-title="" title=""></a>
                        </label>

                    </div>
                    <div class="span4">
                        <label for="" class="radio inline"><input type="radio" name="tipo_trauma" value="vermelho" disabled="true">Trauma vermelho
                            <a href="#" class="icon-question" data-toggle="popover" data-placement="bottom" data-html="true" data-title="Trauma vermelho" data-trigger="hover" data-content="
                               <ul>
                               <li><b>Características fisiológicas</b></li>
                               <ul>
                               <li>Escore de coma de Glasgow <= 13</li>
                               <li>Presença de sinais precoces de choque circulatório</li>
                               <li>Pressão arterial sistolica < 100 mmHg (em qualquer momento)</li>
                               <li>Saturação de O2 <= 90%</li>
                               </ul>
                               <li><b>Características anatômicas</b></li>
                               <ul>
                               <li>Com sinais de trauma significativo na cabeça (paciente intubado), tórax com drenagem pleural > 300 ml, abdome com lesões internas (necessidade de FAST ou CT), fratura de bacia instável ou fratura proximal de ossos longos</li>
                               <li>Obs 1: paciente deve ter 2 abocaths, termômetro esofágico, sondagem vesical e gástrica.</li>
                               <li>Obs 2: pacientes >= 60 anos e/ou presença de comorbidades, solicitar glicemia, uréia/creatinina e sódio/potássio,ou qualquer outro exame necessário.</li>
                               <li>Obs 3: pacientes mulher em idade fértil, solicitar teste de gravidez.</li>
                               </ul>
                               </ul>" data-original-title="" title=""></a>
                        </label>
                    </div>
                </div>

                <!-- exames bioquimicos -->
                <!-- mostra os exames bioquimicos que poderao ser realizados de acordo com a cor-->
                <!-- ver2.2 - sera desabilitado todos os exames iniciaamente: sera checado automaticamente conforme a cor-->

                <!-- colocar somente os exames que serao mostrados-->
                <div class="well">
                    <div class="row-fluid">
                        <label><h4>Exames bioquímicos</h4></label>
                    </div>
                    <div class="row-fluid examesBioquimicos">
                        <div class="span4">
                            <label>
                                <input type="checkbox" id="gasometria" value="1" disabled="">
                                Gasometria arterial 
                            </label>
                        </div>
                        <div class="span4">
                            <label>
                                <input type="checkbox" id="tipo_sanguineo" value="2" disabled="">
                                Tipagem sanguínea 
                            </label>
                        </div>
                        <div class="span4">
                            <label>
                                <input type="checkbox" id="contra_prova" value="3" disabled="">
                                Contra prova 
                            </label>
                        </div>
                    </div>
                    <div class="row-fluid examesBioquimicos">
                        <div class="span4">
                            <label>
                                <input type="checkbox" id="hb" value="4" disabled="">
                                Hb 
                            </label>
                        </div>
                        <div class="span4">
                            <label>
                                <input type="checkbox" id="ht" value="16" disabled="">
                                Ht 
                            </label>
                        </div>
                        <div class="span4">
                            <label>
                                <input type="checkbox" id="plaq" value="17" disabled="">
                                Plaquetas
                            </label>
                        </div>
                    </div>
                    <div class="row-fluid examesBioquimicos">
                        <div class="span4">
                            <label>
                                <input type="checkbox" id="lactato" value="5" disabled="">
                                Lactato 
                            </label>
                        </div>
                        <div class="span4">
                            <label>
                                <input type="checkbox" id="tp_inr" value="6" disabled="">
                                TP/INR 
                            </label>
                        </div>
                        <div class="span4">
                            <label>
                                <input type="checkbox" id="ttpa" value="7" disabled="">
                                TTPA 
                            </label>
                        </div>
                    </div>
                    <div class="row-fluid examesBioquimicos">
                        <div class="span4">
                            <label>
                                <input type="checkbox" id="fibrinogenio" value="8" disabled="">
                                Fibrinogênio 
                            </label>
                        </div>
                    </div>
                </div>
                <!-- Caixa de dialogo para verificar se o usuario deseja pedir exames complementares-->
                <div class="row-fluid">
                    <label>
                        <input type="checkbox" onclick="jQuery('#add_addition_laboratory_exams').toggle()">
                        Solicitação de exames adicionais
                    </label>
                </div>
                <div class="row-fluid" id="add_addition_laboratory_exams" style="display: none;">
                    <!-- Regiao para adicionar exames complementares-->
                    <!-- colocar somente os exames que serao mostrados-->
                    <div class="well">
                        <div class="row-fluid">
                            <label><h4>Exames adicionais</h4></label>
                        </div>
                        <div class="row-fluid examesBioquimicos">
                            <div class="span4">
                                <label>
                                    <input type="checkbox" id="ureia" value="10" >
                                    Uréia 
                                </label>
                            </div>
                            <div class="span4">
                                <label>
                                    <input type="checkbox" id="creatinina" value="11" >
                                    Creatinina 
                                </label>
                            </div>
                            <div class="span4">
                                <label>
                                    <input type="checkbox" id="amilase" value="12" >
                                    Amilase 
                                </label>
                            </div>
                        </div>
                        <div class="row-fluid examesBioquimicos">
                            <div class="span4">
                                <label>
                                    <input type="checkbox" id="teste_gestacao" value="13" >
                                    Teste de gestação 
                                </label>
                            </div>
                            <div class="span4">
                                <label>
                                    <input type="checkbox" id="bilirrubina_total" value="14" >
                                    Bilirrubina total 
                                </label>
                            </div>
                            <div class="span4">
                                <label>
                                    <input type="checkbox" id="bilirrubina_direta" value="15" >
                                    Bilirrubina direta 
                                </label>
                            </div>
                        </div>
                    </div>	
                </div>
            </div>
        </div>
        <footer class="panel-footer">
            <div class="row">
                <div class="col-md-12 text-right">
                    <button class="btn btn-primary" onclick="save_biochemistry_exam()"><?= lang('save') ?></button>
                    <button class="btn btn-default modal-dismiss" id="modal_close_button"><?= lang('close') ?></button>
                </div>
            </div>
        </footer>
    </section>
</div>
<script src="<?= $this->config->base_url(JSPATH . 'examples.modals.js') ?>"></script>

<script>

                        //Exames complementares
                        //habilita os tipos de trauma para serem selecionados
                        $(document).on('change', '#habilita_exames', function () {
                            if ($('#habilita_exames').is(':checked')) {
                                $('input[name=tipo_trauma]').prop('disabled', false);
                                $('input[name=tipo_trauma]').prop('checked', false);
                            } else {
                                $('input[name=tipo_trauma]').prop('disabled', true);
                                $('.examesBioquimicos input').prop('checked', false);
                            }
                        });

                        $(document).on('change', 'input[name=tipo_trauma]', function () {
                            if ($(this).val() === 'amarelo') {
                                $('#hb').prop('checked', true);
                                $('#ht').prop('checked', true);
                                $('#plaq').prop('checked', true);
                                $('#gasometria').prop('checked', false);
                                $('#gasometria_venosa').prop('checked', false);
                                $('#tipo_sanguineo').prop('checked', false);
                                $('#contra_prova').prop('checked', false);
                                $('#lactato').prop('checked', false);
                                $('#tp_inr').prop('checked', false);
                                $('#ttpa').prop('checked', false);
                                $('#fibrinogenio').prop('checked', false);
                                $('#ureia').prop('checked', false);
                                $('#creatinina').prop('checked', false);
                                $('#amilase').prop('checked', false);
                                $('#glicemia').prop('checked', false);
                                $('#teste_gestacao').prop('checked', false);
                                $('#lav_peritoneal').prop('checked', false);
                            }
                            if ($(this).val() === 'laranja') {
                                $('#hb').prop('checked', true);
                                $('#ht').prop('checked', true);
                                $('#plaq').prop('checked', true);
                                $('#tipo_sanguineo').prop('checked', true);
                                $('#contra_prova').prop('checked', true);

                                $('#gasometria').prop('checked', false);
                                $('#gasometria_venosa').prop('checked', false);
                                $('#lactato').prop('checked', false);
                                $('#tp_inr').prop('checked', false);
                                $('#ttpa').prop('checked', false);
                                $('#fibrinogenio').prop('checked', false);
                                $('#ureia').prop('checked', false);
                                $('#creatinina').prop('checked', false);
                                $('#amilase').prop('checked', false);
                                $('#glicemia').prop('checked', false);
                                $('#teste_gestacao').prop('checked', false);
                                $('#lav_peritoneal').prop('checked', false);
                            }
                            if ($(this).val() === 'vermelho') {
                                $('#gasometria').prop('checked', true);
                                $('#hb').prop('checked', true);
                                $('#ht').prop('checked', true);
                                $('#plaq').prop('checked', true);
                                $('#tipo_sanguineo').prop('checked', true);
                                $('#contra_prova').prop('checked', true);
                                $('#tp_inr').prop('checked', true);
                                $('#ttpa').prop('checked', true);
                                $('#fibrinogenio').prop('checked', true);
                                $('#lactato').prop('checked', true);

                                $('#gasometria_venosa').prop('checked', false);
                                $('#ureia').prop('checked', false);
                                $('#creatinina').prop('checked', false);
                                $('#amilase').prop('checked', false);
                                $('#glicemia').prop('checked', false);
                                $('#teste_gestacao').prop('checked', false);
                                $('#lav_peritoneal').prop('checked', false);
                            }
                        });

                        function save_bio_exam_result() {

                            if (!jQuery('#result').val()) {
                                var notice = new PNotify({
                                    title: 'Erro',
                                    text: 'Algum valor precisa ser adicionado',
                                    type: 'error',
                                    addclass: 'click-2-close',
                                    hide: false,
                                    buttons: {
                                        closer: false,
                                        sticker: false
                                    }
                                });
                                notice.get().click(function () {
                                    notice.remove();
                                });
                                return false;
                            }

                            if (!jQuery('#result_date').val()) {
                                var notice = new PNotify({
                                    title: 'Erro',
                                    text: 'A data do resultado precisa ser adicionada',
                                    type: 'error',
                                    addclass: 'click-2-close',
                                    hide: false,
                                    buttons: {
                                        closer: false,
                                        sticker: false
                                    }
                                });
                                notice.get().click(function () {
                                    notice.remove();
                                });
                                return false;
                            }

                            jQuery.ajax({
                                url: jQuery("body").data("baseurl") + "attendance_hospital/save_bio_exam_result",
                                type: "post",
                                dataType: 'json',
                                data: {
                                    result: jQuery('#result').val(),
                                    result_date: jQuery('#result_date').val(),
                                    exam_id: jQuery('#exam_id').val()
                                },
                                success: function (response) {
                                    jQuery('#modal_close_button').click();
                                    if (response.status === 'NOK') {
                                        var notice = new PNotify({
                                            title: 'Erro',
                                            text: 'Tente novamente mais tarde',
                                            type: 'error',
                                            addclass: 'click-2-close',
                                            hide: false,
                                            buttons: {
                                                closer: false,
                                                sticker: false
                                            }
                                        });
                                        notice.get().click(function () {
                                            notice.remove();
                                        });
                                    } else {
                                        // recarrega lista de exames
                                        jQuery('#hospitalization_steps').load(jQuery("body").data("baseurl") + "hospitalization/biochemistry_exams");
                                    }
                                }
                            });
                        }

                        function save_biochemistry_exam() {

                            var count = 0;
                            var exams = [];
                            jQuery('.examesBioquimicos input:checked').each(function (element) {
                                exams.push(jQuery(this).val());
                                count++;
                            });

                            if (count < 1) {
                                var notice = new PNotify({
                                    title: 'Erro',
                                    text: 'algum exame precisa ser selecionado',
                                    type: 'error',
                                    addclass: 'click-2-close',
                                    hide: false,
                                    buttons: {
                                        closer: false,
                                        sticker: false
                                    }
                                });
                                notice.get().click(function () {
                                    notice.remove();
                                });
                                return false;
                            }

                            jQuery.ajax({
                                url: jQuery("body").data("baseurl") + "hospitalization/save_biochemistry_exam",
                                type: "post",
                                dataType: 'json',
                                data: {
                                    exams: exams
                                },
                                success: function (response) {
                                    jQuery('#modal_close_button').click();
                                    if (response.status === 'NOK') {
                                        var notice = new PNotify({
                                            title: 'Erro',
                                            text: 'Tente novamente mais tarde',
                                            type: 'error',
                                            addclass: 'click-2-close',
                                            hide: false,
                                            buttons: {
                                                closer: false,
                                                sticker: false
                                            }
                                        });
                                        notice.get().click(function () {
                                            notice.remove();
                                        });
                                    } else {
                                        // recarrega lista de exames
                                        jQuery('#hospitalization_steps').load(jQuery("body").data("baseurl") + "hospitalization/biochemistry_exams");
                                    }
                                }
                            });
                        }

                        function delete_biochemistry_exam(id) {
                            jQuery.ajax({
                                url: jQuery("body").data("baseurl") + "hospitalization/delete_biochemistry_exam",
                                type: "post",
                                dataType: 'json',
                                data: {
                                    id: id
                                },
                                success: function (response) {
                                    if (response.status === 'NOK') {
                                        var notice = new PNotify({
                                            title: 'Erro',
                                            text: 'Tente novamente mais tarde',
                                            type: 'error',
                                            addclass: 'click-2-close',
                                            hide: false,
                                            buttons: {
                                                closer: false,
                                                sticker: false
                                            }
                                        });
                                        notice.get().click(function () {
                                            notice.remove();
                                        });
                                    } else {
                                        // recarrega lista de exames
                                        jQuery('#hospitalization_steps').load(jQuery("body").data("baseurl") + "hospitalization/biochemistry_exams");
                                    }
                                }
                            });
                        }

</script>