<style>
    .bootstrap-datetimepicker-widget{z-index:11510 !important;}
</style>

<div class="search-filter">
    <legend><?= lang('use_of_blood') ?>
        <a class="mb-xs mt-xs mr-xs modal-basic btn btn-success" href="#modalAddUseOfBlood" style="float: right"> <i class="fa fa-plus-square-o"></i> <?= lang('add') ?></a>
    </legend>        
</div>

<div class="well">
    <?php if (!empty($use_of_blood)): ?>
        <table class="table table-bordered table-striped mb-none" >
            <thead>
                <tr>
                    <th><?= lang('type') ?></th>
                    <th><?= lang('volume') ?></th>
                    <th><?= lang('sector') ?></th>
                    <th><?= lang('moment') ?></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($use_of_blood as $e): ?>
                    <tr>
                        <th><?= lang($e['type']) ?></th>
                        <th><?= $e['volume'] ?> <?= $e['volume_type'] ?></th>
                        <th><?= $e['name'] ?></th>
                        <th><?= $e['created'] ?></th>
                        <th><a href="javascript:void(0)" onclick="delete_use_of_blood('<?= $this->encrypt->encode($e['id']) ?>')" class="on-default remove-row" style=" margin-right: 5px; margin-left: 5px;"><i class="fa fa-trash-o"></i></a></th>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php else: ?>
        <h3>Nenhuma transfusão cadastrada</h3>
    <?php endif; ?>
</div>


<div id="modalAddUseOfBlood" class="modal-block mfp-hide" style="width: 80%;">
    <section class="panel">
        <header class="panel-heading">
            <h2 class="panel-title"><?= lang('add_use_of_blood') ?></h2>
        </header>
        <div class="panel-body">
            <div class="modal-wrapper">
                <div class="well">
                    <div class="row-fluid">
                        <label><h4>Dados do exame</h4></label>
                    </div>
                    <div class="row-fluid">
                        <div class="span6">
                            <h5><label class="control-label"><?= lang('sector') ?></label></h5>
                            <select id="sector_id" class="form-control">
                                <?php foreach ($sectors as $s): ?>
                                    <option value="<?= $this->encrypt->encode($s['id']) ?>"><?= $s['name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span6">
                            <h5><label class="control-label"><?= lang('type') ?></label></h5>
                            <select id="type" class="form-control">
                                <option value="concentrated_erythrocytes"><?= lang('concentrated_erythrocytes') ?></option>
                                <option value="cryoprecipitate"><?= lang('cryoprecipitate') ?></option>
                                <option value="fresh_plasma"><?= lang('fresh_plasma') ?></option>
                                <option value="Platelets"><?= lang('Platelets') ?></option>
                            </select>
                        </div>
                        <div class="span3">
                            <h5><label class="control-label"><?= lang('volume') ?></label></h5>
                            <input id="volume" type="int" class="form-control"/> 
                        </div>
                        <div class="span3">
                            <h5><label class="control-label">Unidade</label></h5>
                            <select id="volume_type" class="form-control">
                                <option value="ml">ml</option>
                                <option value="unidades">unidades</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer class="panel-footer">
            <div class="row">
                <div class="col-md-12 text-right">
                    <button class="btn btn-primary" onclick="save_use_of_blood()"><?= lang('save') ?></button>
                    <button class="btn btn-default modal-dismiss" id="modal_close_button"><?= lang('close') ?></button>
                </div>
            </div>
        </footer>
    </section>
</div>

<script src="<?= $this->config->base_url(JSPATH . 'examples.modals.js') ?>"></script>

<script>
                        function save_use_of_blood() {

                            if (!jQuery('#volume').val()) {
                                var notice = new PNotify({
                                    title: 'Erro',
                                    text: 'O volume ser preenchido',
                                    type: 'error',
                                    addclass: 'click-2-close',
                                    hide: false,
                                    buttons: {
                                        closer: false,
                                        sticker: false
                                    }
                                });
                                notice.get().click(function () {
                                    notice.remove();
                                });
                                return false;
                            }

                            jQuery.ajax({
                                url: jQuery("body").data("baseurl") + "hospitalization/save_use_of_blood",
                                type: "post",
                                dataType: 'json',
                                data: {
                                    volume: jQuery('#volume').val(),
                                    type: jQuery('#type').val(),
                                    volume_type: jQuery('#volume_type').val(),
                                    sector_id: jQuery('#sector_id').val()
                                },
                                success: function (response) {
                                    jQuery('#modal_close_button').click();
                                    if (response.status === 'NOK') {
                                        var notice = new PNotify({
                                            title: 'Erro',
                                            text: 'Tente novamente mais tarde',
                                            type: 'error',
                                            addclass: 'click-2-close',
                                            hide: false,
                                            buttons: {
                                                closer: false,
                                                sticker: false
                                            }
                                        });
                                        notice.get().click(function () {
                                            notice.remove();
                                        });
                                    } else {
                                        // recarrega lista de transfusoes
                                        jQuery('#hospitalization_steps').load(jQuery("body").data("baseurl") + "hospitalization/use_of_blood");
                                    }
                                }
                            });
                        }

                        function delete_use_of_blood(id) {
                            jQuery.ajax({
                                url: jQuery("body").data("baseurl") + "hospitalization/delete_use_of_blood",
                                type: "post",
                                dataType: 'json',
                                data: {
                                    id: id
                                },
                                success: function (response) {
                                    if (response.status === 'NOK') {
                                        var notice = new PNotify({
                                            title: 'Erro',
                                            text: 'Tente novamente mais tarde',
                                            type: 'error',
                                            addclass: 'click-2-close',
                                            hide: false,
                                            buttons: {
                                                closer: false,
                                                sticker: false
                                            }
                                        });
                                        notice.get().click(function () {
                                            notice.remove();
                                        });
                                    } else {
                                        // recarrega lista de transfusoes
                                        jQuery('#hospitalization_steps').load(jQuery("body").data("baseurl") + "hospitalization/use_of_blood");
                                    }
                                }
                            });
                        }

</script>