<?php if (isset($resume)): ?>
    <div id="edit_visit_resume" hidden="">
        <div class="container-fluid">
            <legend>Editar resumo</legend>
            <div class="well">
                <h5><label class="control-label"><?= lang('resume') ?></label></h5>
                <div class="form-group">
                    <div style="height: 100px" class="summernote" id="resume_visit_edited"><?php
                        if (isset($resume)): echo $resume;
                        endif;
                        ?></div>
                </div>
                <div class="row-fluid">
                    <div class="btn-group pull-right" style=" margin-top:20px;">
                        <a class="btn btn-danger btn-sm" href="javascript:void(0)" onclick="jQuery('#edit_visit_resume').toggle();"><?= lang('close') ?></a>
                        <a class="btn btn-success btn-sm" href="javascript:void(0)" onclick="save_visit_resume('<?= $id ?>')"> <?= lang('save') ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>

<div class="search-filter">
    <legend>Resumo das visitas
        <?php if (isset($resume)): ?>
            <a class="mb-xs mt-xs mr-xs btn btn-success" href="javascript:void(0)" onclick="jQuery('#edit_visit_resume').toggle()" style="float: right"> <i class="fa fa-plus-square-o"></i> Editar resumo</a>
        <?php endif; ?>
    </legend>        
</div>

<div class="well">
    <?php
    if (isset($resume)):
        echo $resume;
    else:
        ?>
        <h3>Nenhum resumo cadastrado</h3>
    <?php endif; ?>
</div>

<script>
    jQuery(document).ready(function () {
        jQuery('.summernote').summernote({
            height: 150
        });
    });
    function save_visit_resume(id) {
        jQuery.ajax({
            url: jQuery("body").data("baseurl") + "hospitalization/save_visit_resume",
            type: "post",
            dataType: 'json',
            data: {
                resume: jQuery('#resume_visit_edited').summernote('code'),
                id: id
            },
            success: function (response) {
                jQuery('#modal_close_button').click();
                if (response.status === 'NOK') {
                    var notice = new PNotify({
                        title: 'Erro',
                        text: 'Tente novamente mais tarde',
                        type: 'error',
                        addclass: 'click-2-close',
                        hide: false,
                        buttons: {
                            closer: false,
                            sticker: false
                        }
                    });
                    notice.get().click(function () {
                        notice.remove();
                    });
                } else {
                    // recarrega a pagina
                    location.reload();
                }

            }
        });

    }
</script>