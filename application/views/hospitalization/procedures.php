
<div id="new_procedure" hidden="">
    <div class="container-fluid">
        <legend>Adicionar Procedimento</legend>
        <div class="well">

            <h5><label class="control-label"><?= lang('type') ?></label></h5>
            <div class="form-group">
                <select class="form-control" id="type">
                    <?php foreach ($c_procedures as $h): ?>
                        <option value="<?= $h['id'] ?>" selected><?= $h['procedure_name'] ?></option>
                    <?php endforeach; ?>
                </select>
            </div>

            <h5><label class="control-label">Data/Hora instalação</label></h5>
            <div class='input-group date input-append datetimepicker1'>
                <input type='text' class="form-control" data-format="yyyy-MM-dd hh:mm:ss" id="date_hour_in" />
                <span class="input-group-addon add-on">
                    <span><i class="fa fa-calendar" data-time-icon="icon-time" data-date-icon="icon-calendar">
                        </i></span>
                </span>
            </div>

            <h5><label class="control-label">Data/Hora retirada</label></h5>
            <div class='input-group date input-append datetimepicker1'>
                <input type='text' class="form-control" data-format="yyyy-MM-dd hh:mm:ss" id="date_hour_out" />
                <span class="input-group-addon add-on">
                    <span><i class="fa fa-calendar" data-time-icon="icon-time" data-date-icon="icon-calendar">
                        </i></span>
                </span>
            </div>

            <h5><label class="control-label"><?= lang('responsibles') ?></label></h5>
            <div class="form-group">
                <select class="form-control" id="responsibles">
                    <?php foreach ($responsibles as $r): ?>
                        <option value="<?= $r['id'] ?>" selected><?= $r['name'] ?></option>
                    <?php endforeach; ?>
                </select>
            </div>

            <div class="row-fluid">
                <div class="btn-group pull-right" style=" margin-top:20px;">
                    <a class="btn btn-danger btn-sm" href="javascript:void(0)" onclick="jQuery('#new_procedure').toggle();
                            jQuery('textarea').val('');"><?= lang('close') ?></a>
                    <a class="btn btn-success btn-sm" href="javascript:void(0)" onclick="save_procedure()" > <?= lang('save') ?></a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="search-filter">
    <legend><?= lang('procedures') ?>
        <a class="mb-xs mt-xs mr-xs btn btn-success" href="javascript:void(0)" onclick="jQuery('#new_procedure').toggle()" style="float: right"> <i class="fa fa-plus-square-o"></i> Adicionar Procedimento</a>
    </legend>        
</div>

<div class="well">
    <?php if ($procedures): ?>
        <table class="table table-bordered table-striped mb-none" >
            <thead>
                <tr>
                    <th><?= lang('name') ?></th>
                    <th>Data/Hora instalação</th>
                    <th>Data/Hora retirada</th>
                    <th><?= lang('responsible') ?></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($procedures as $e): ?>
                    <tr>
                        <th><?= $e['name'] ?></th>
                        <th><?= $e['date_hour_in'] ?></th>
                        <th><?= $e['date_hour_out'] ?></th>
                        <th><?= $e['responsible'] ?></th>
                        <th><a href="javascript:;" onclick="delete_procedure('<?= $this->encrypt->encode($e['id']) ?>')" class="on-default remove-row" style=" margin-right: 5px; margin-left: 5px;"><i class="fa fa-trash-o"></i></a></th>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php else: ?>
        <h3>Nenhum procedimento cadastrado</h3>
    <?php endif; ?>
</div>

<div id="new_hemo" hidden="">
    <div class="container-fluid">
        <legend>Adicionar Hemodialise</legend>
        <div class="well">

            <h5><label class="control-label"><?= lang('type') ?></label></h5>
            <div class="form-group">
                <select class="form-control" id="type">
                    <?php foreach ($c_hemos as $h): ?>
                        <option value="<?= $h['id'] ?>" selected><?= $h['procedure_name'] ?></option>
                    <?php endforeach; ?>
                </select>
            </div>

            <h5><label class="control-label">Data/Hora</label></h5>
            <div class='input-group date input-append datetimepicker1'>
                <input type='text' class="form-control" data-format="yyyy-MM-dd hh:mm:ss" id="date_hour_in" />
                <span class="input-group-addon add-on">
                    <span><i class="fa fa-calendar" data-time-icon="icon-time" data-date-icon="icon-calendar">
                        </i></span>
                </span>
            </div>

            <h5><label class="control-label"><?= lang('responsibles') ?></label></h5>
            <div class="form-group">
                <select class="form-control" id="responsibles">
                    <?php foreach ($responsibles as $r): ?>
                        <option value="<?= $r['id'] ?>" selected><?= $r['name'] ?></option>
                    <?php endforeach; ?>
                </select>
            </div>

            <div class="row-fluid">
                <div class="btn-group pull-right" style=" margin-top:20px;">
                    <a class="btn btn-danger btn-sm" href="javascript:void(0)" onclick="jQuery('#new_hemo').toggle();
                            jQuery('textarea').val('');"><?= lang('close') ?></a>
                    <a class="btn btn-success btn-sm" href="javascript:void(0)" onclick="save_hemo()" > <?= lang('save') ?></a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="search-filter">
    <legend>Hemodialises
        <a class="mb-xs mt-xs mr-xs btn btn-success" href="javascript:void(0)" onclick="jQuery('#new_hemo').toggle()" style="float: right"> <i class="fa fa-plus-square-o"></i> Adicionar Hemodialise</a>
    </legend>        
</div>

<div class="well">
    <?php if ($hemos): ?>
        <table class="table table-bordered table-striped mb-none" >
            <thead>
                <tr>
                    <th><?= lang('name') ?></th>
                    <th>Data/Hora instalação</th>
                    <th><?= lang('responsible') ?></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($hemos as $e): ?>
                    <tr>
                        <th><?= $e['name'] ?></th>
                        <th><?= $e['date_hour_in'] ?></th>
                        <th><?= $e['responsible'] ?></th>
                        <th><a href="javascript:;" onclick="delete_procedure('<?= $this->encrypt->encode($e['id']) ?>')" class="on-default remove-row" style=" margin-right: 5px; margin-left: 5px;"><i class="fa fa-trash-o"></i></a></th>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php else: ?>
        <h3>Nenhuma hemodialise cadastrada</h3>
    <?php endif; ?>
</div>

<script type="text/javascript">
    $(function () {
        $('.datetimepicker1').datetimepicker({
            language: 'pt-BR'
        });
    });
</script>

<script>

    function save_procedure() {
        jQuery.ajax({
            url: jQuery("body").data("baseurl") + "hospitalization/save_procedure",
            type: "post",
            dataType: 'json',
            data: {
                c_procedures_id: jQuery('#new_procedure #type').val(),
                date_hour_in: jQuery('#new_procedure #date_hour_in').val(),
                date_hour_out: jQuery('#new_procedure #date_hour_out').val(),
                responsible_id: jQuery('#new_procedure #responsibles').val()
            },
            success: function (response) {
                if (response.status === 'NOK') {
                    var notice = new PNotify({
                        title: 'Erro',
                        text: 'Tente novamente mais tarde',
                        type: 'error',
                        addclass: 'click-2-close',
                        hide: false,
                        buttons: {
                            closer: false,
                            sticker: false
                        }
                    });
                    notice.get().click(function () {
                        notice.remove();
                    });
                } else {
                    // recarrega lista de visitas
                    jQuery('#hospitalization_steps').load(jQuery("body").data("baseurl") + "hospitalization/procedures");
                }
            }
        });
    }

    function save_hemo() {
        jQuery.ajax({
            url: jQuery("body").data("baseurl") + "hospitalization/save_hemo",
            type: "post",
            dataType: 'json',
            data: {
                c_procedures_id: jQuery('#new_hemo #type').val(),
                date_hour_in: jQuery('#new_hemo #date_hour_in').val(),
                responsible_id: jQuery('#new_hemo #responsibles').val()
            },
            success: function (response) {
                if (response.status === 'NOK') {
                    var notice = new PNotify({
                        title: 'Erro',
                        text: 'Tente novamente mais tarde',
                        type: 'error',
                        addclass: 'click-2-close',
                        hide: false,
                        buttons: {
                            closer: false,
                            sticker: false
                        }
                    });
                    notice.get().click(function () {
                        notice.remove();
                    });
                } else {
                    // recarrega lista de visitas
                    jQuery('#hospitalization_steps').load(jQuery("body").data("baseurl") + "hospitalization/procedures");
                }
            }
        });
    }

    function delete_procedure(id) {
        jQuery.ajax({
            url: jQuery("body").data("baseurl") + "hospitalization/delete_procedure",
            type: "post",
            dataType: 'json',
            data: {
                id: id
            },
            success: function (response) {
                if (response.status === 'OK') {
                    jQuery('#hospitalization_steps').load(jQuery("body").data("baseurl") + "hospitalization/procedures");
                } else {
                    var notice = new PNotify({
                        title: 'Erro',
                        text: 'Tente novamente mais tarde',
                        type: 'error',
                        addclass: 'click-2-close',
                        hide: false,
                        buttons: {
                            closer: false,
                            sticker: false
                        }
                    });
                    notice.get().click(function () {
                        notice.remove();
                    });
                }
            }
        });

    }
</script>