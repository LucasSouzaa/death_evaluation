<div id="new_visit" hidden="">
    <div class="container-fluid">
        <legend><?= lang('add_visit') ?></legend>

        <div id="use_service_beds"></div>

        <div class="well">
            <h5><label class="control-label"><?= lang('resume') ?></label></h5>
            <div class="form-group">
                <div style="height: 100px" class="summernote" id="resume_visit"><?php
                    if (isset($resume)): echo $resume;
                    endif;
                    ?></div>
            </div>

            <h5><label class="control-label"><?= lang('conduct') ?></label></h5>
            <div class="form-group">
                <select class="form-control" id="conduct">
                    <option value="avaliacao" selected>Em atendimento/avaliação</option>
                    <option value="observacao">Em observação clínica</option>
                    <option value="aguarda_leito">Aguardando leito para internação</option>
                    <option value="alta">Alta</option>
                    <option value="alta_a_pedido">Alta a pedido</option>
                    <option value="transferencia">Transferência</option>
                    <option value="internado">Internado</option>
                    <option value="reinternado">Re-Internado</option>
                    <option value="obito">Óbito</option>
                </select>
            </div>

            <h5><label class="control-label"><?= lang('conduct_justification') ?></label></h5>
            <div class="form-group">
                <div style="height: 100px" class="summernote" id="conduct_justification"></div>
            </div>

            <div class="row-fluid">
                <div class="btn-group pull-right" style=" margin-top:20px;">
                    <a class="btn btn-danger btn-sm" href="javascript:void(0)" onclick="jQuery('#new_visit').toggle();
                            jQuery('.summernote').summernote('code', '');"><?= lang('close') ?></a>
                    <a class="btn btn-success btn-sm" href="javascript:void(0)" onclick="save_visit()" > <?= lang('save') ?></a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="search-filter">
    <legend><?= lang('visits') ?>
        <a class="mb-xs mt-xs mr-xs btn btn-success" href="javascript:void(0)" onclick="jQuery('#new_visit').toggle()" style="float: right"> <i class="fa fa-plus-square-o"></i> <?= lang('add_visit') ?></a>
    </legend>        
</div>

<div class="well">
    <?php if (!empty($passages)): ?>
        <table class="table table-bordered table-striped mb-none" >
            <thead>
                <tr>
                    <th><?= lang('datetime_realization') ?></th>
                    <th><?= lang('author') ?></th>
                    <th><?= lang('resume') ?></th>
                    <th><?= lang('conduct') ?></th>
                    <th><?= lang('conduct_justification') ?></th>
                    <th><?= lang('responsables') ?></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($passages as $e): ?>
                    <tr>
                        <th><?= $e['created'] ?></th>
                        <th><?= $e['name'] ?></th>
                        <th><?= $e['resume'] ?></th>
                        <th><?= $e['conduct'] ?></th>
                        <th><?= $e['conduct_justification'] ?></th>
                        <th>
                            <?php foreach ($e['responsables'] as $r): ?>
                                <?= $r['name'] ?></br>
                            <?php endforeach; ?>
                        </th>
                        <th style="text-align: center">
                            <a class="modal-basic btn btn-primary" style="padding: 0px 10px 0px 10px" href="#modalAddResponsable" onclick="jQuery('#passage_id').val(<?= $e['id'] ?>)"> Adicionar responsável</a>
                        </th>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php else: ?>
        <h3>Nenhuma visita cadastrada</h3>
    <?php endif; ?>
</div>

<div id="modalAddResponsable" class="modal-block mfp-hide" style="width: 40%;">
    <section class="panel">
        <header class="panel-heading">
            <h2 class="panel-title">Profissionais</h2>
        </header>
        <div class="panel-body">
            <div class="modal-wrapper">
                <select id="person_id" class="form-control">
                    <?php foreach ($professionals as $p): ?>
                        <option value="<?= $p['person_id'] ?>"><?= $p['name'] ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
        <footer class="panel-footer">
            <div class="row">
                <div class="col-md-12 text-right">
                    <button class="btn btn-primary" onclick="insert_responsable_in_visit()"><?= lang('add') ?></button>
                    <button class="btn btn-default modal-dismiss" id="modal_close_button"><?= lang('close') ?></button>
                </div>
            </div>
        </footer>
    </section>
</div>

<input hidden="" id="passage_id" value=""/>
<script src="<?= $this->config->base_url(JSPATH . 'examples.modals.js') ?>"></script>
<script>
                        jQuery(document).ready(function () {
                            jQuery('.summernote').summernote({
                                height: 150
                            });
                            jQuery('#use_service_beds').load(jQuery("body").data("baseurl") + "hospitalization/use_service_beds");
                        });
                        function save_visit() {
                            jQuery.ajax({
                                url: jQuery("body").data("baseurl") + "hospitalization/save_visit",
                                type: "post",
                                dataType: 'json',
                                data: {
                                    conduct_justification: jQuery('#conduct_justification').summernote('code'),
                                    conduct: jQuery('#conduct').val(),
                                    resume: jQuery('#resume_visit').summernote('code')
                                },
                                success: function (response) {
                                    jQuery('#modal_close_button').click();
                                    if (response.status === 'NOK') {
                                        var notice = new PNotify({
                                            title: 'Erro',
                                            text: 'Tente novamente mais tarde',
                                            type: 'error',
                                            addclass: 'click-2-close',
                                            hide: false,
                                            buttons: {
                                                closer: false,
                                                sticker: false
                                            }
                                        });
                                        notice.get().click(function () {
                                            notice.remove();
                                        });
                                    } else {
                                        // recarrega lista de visitas
                                        jQuery('#hospitalization_steps').load(jQuery("body").data("baseurl") + "hospitalization/visits");
                                    }

                                }
                            });
                        }

                        function insert_responsable_in_visit() {
                            jQuery.ajax({
                                url: jQuery("body").data("baseurl") + "hospitalization/insert_responsable_in_visit",
                                type: "post",
                                dataType: 'json',
                                data: {
                                    person_id: jQuery('#person_id').val(),
                                    passage_id: jQuery('#passage_id').val()
                                },
                                success: function (response) {
                                    jQuery('#modal_close_button').click();
                                    if (response.status === 'NOK') {
                                        var notice = new PNotify({
                                            title: 'Erro',
                                            text: 'Tente novamente mais tarde',
                                            type: 'error',
                                            addclass: 'click-2-close',
                                            hide: false,
                                            buttons: {
                                                closer: false,
                                                sticker: false
                                            }
                                        });
                                        notice.get().click(function () {
                                            notice.remove();
                                        });
                                    } else {
                                        // recarrega lista de visitas
                                        jQuery('#hospitalization_steps').load(jQuery("body").data("baseurl") + "hospitalization/visits");
                                    }

                                }
                            });
                        }
</script>
