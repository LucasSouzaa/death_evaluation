<div id="visits_resume"></div>
<div id="use_service_beds"></div>
<div id="visits_evolution"></div>
<div id="intercurrences"></div>
<div id="biochemistry_exams_evolution"></div>
<div id="image_exams_evolution"></div>
<div id="microbiology_exams_evolution"></div>
<div id="anatomical_pathological_exams_evolution"></div>
<div id="get_ec_diagnosys_list_evolution"></div>
<div id="get_i_diagnosys_list_evolution"></div>
<div id="drugs_evolution"></div>
<div id="surgery_anesthesia_evolution"></div>
<div id="procedures_evolution"></div>
<div id="use_of_blood_evolution"></div>
<div id="medical_management_evolution"></div>
<div id="prognostic_indicators_evolution"></div>
<div id="imagery_evolution"></div>
<div id="outpatient_condition_evolution"></div>

<script>
    jQuery('li').removeClass('active');
    jQuery('#li_clinical_evolution_layout').addClass('active');

    jQuery('#visits_resume').load(jQuery("body").data("baseurl") + "hospitalization/visits_resume");
    jQuery('#use_service_beds').load(jQuery("body").data("baseurl") + "hospitalization/use_service_beds");
    jQuery('#visits_evolution').load(jQuery("body").data("baseurl") + "hospitalization/visits");
    jQuery('#intercurrences').load(jQuery("body").data("baseurl") + "hospitalization/intercurrences");
    //jQuery('#biochemistry_exams_evolution').load(jQuery("body").data("baseurl") + "hospitalization/biochemistry_exams");
    //jQuery('#image_exams_evolution').load(jQuery("body").data("baseurl") + "hospitalization/image_exams");
    //jQuery('#microbiology_exams_evolution').load(jQuery("body").data("baseurl") + "hospitalization/microbiology_exams");
    //jQuery('#anatomical_pathological_exams_evolution').load(jQuery("body").data("baseurl") + "hospitalization/anatomical_pathological_exams");
    //jQuery('#get_ec_diagnosys_list_evolution').load(jQuery("body").data("baseurl") + "attendance_hospital/get_ec_diagnosys_list");
    //jQuery('#get_i_diagnosys_list_evolution').load(jQuery("body").data("baseurl") + "attendance_hospital/get_i_diagnosys_list");
    //jQuery('#drugs_evolution').load(jQuery("body").data("baseurl") + "hospitalization/drugs");
    //jQuery('#surgery_anesthesia_evolution').load(jQuery("body").data("baseurl") + "hospitalization/surgery_anesthesia");
    //jQuery('#procedures_evolution').load(jQuery("body").data("baseurl") + "hospitalization/procedures");
    //jQuery('#use_of_blood_evolution').load(jQuery("body").data("baseurl") + "hospitalization/use_of_blood");
    //jQuery('#medical_management_evolution').load(jQuery("body").data("baseurl") + "hospitalization/medical_management");
    //jQuery('#prognostic_indicators_evolution').load(jQuery("body").data("baseurl") + "hospitalization/prognostic_indicators");
    //jQuery('#imagery_evolution').load(jQuery("body").data("baseurl") + "hospitalization/imagery");
    jQuery('#outpatient_condition_evolution').load(jQuery("body").data("baseurl") + "hospitalization/outpatient_condition");
</script>