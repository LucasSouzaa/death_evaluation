
<div id="new_use_service_bed" hidden="">
    <div class="container-fluid">
        <legend>Trocar paciente de leito</legend>
        <div class="well">
            <h5><label class="control-label"><?= lang('beds') ?></label></h5>
            <div class="form-group">
                <select class="form-control" id="bed_id">
                    <?php foreach ($beds as $b): ?>
                        <option value="<?= $b['id'] ?>" selected><?= $b['name'] ?> (Quarto: <?= $b['bedroom'] ?>) (Setor: <?= $b['sector'] ?>)</option>
                    <?php endforeach; ?>
                </select>
            </div>

            <div class="row-fluid">
                <div class="btn-group pull-right" style=" margin-top:20px;">
                    <a class="btn btn-danger btn-sm" href="javascript:void(0)" onclick="jQuery('#new_use_service_bed').toggle();"><?= lang('close') ?></a>
                    <a class="btn btn-success btn-sm" href="javascript:void(0)" onclick="save_use_service_bed()" > <?= lang('save') ?></a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="search-filter">
    <legend>Leitos por onde o paciente passou
        <a class="mb-xs mt-xs mr-xs btn btn-success" href="javascript:void(0)" onclick="jQuery('#new_use_service_bed').toggle()" style="float: right"> <i class="fa fa-plus-square-o"></i> Trocar paciente de leito</a>
    </legend>        
</div>

<div class="well">
    <?php if (!empty($passages_beds)): ?>
        <table class="table table-bordered table-striped mb-none" >
            <thead>
                <tr>
                    <th>Data de entrada</th>
                    <th><?= lang('sector') ?></th>
                    <th><?= lang('bedroom') ?></th>
                    <th><?= lang('bed') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($passages_beds as $e): ?>
                    <tr>
                        <th><?= $e['created'] ?></th>
                        <th><?= $e['sector'] ?></th>
                        <th><?= $e['bedroom'] ?></th>
                        <th><?= $e['bed'] ?></th>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php else: ?>
        <h3>Nenhum leito cadastrado</h3>
    <?php endif; ?>
</div>

<script>
    function save_use_service_bed() {
        jQuery.ajax({
            url: jQuery("body").data("baseurl") + "hospitalization/save_use_service_bed",
            type: "post",
            dataType: 'json',
            data: {
                bed_id: jQuery('#bed_id').val()
            },
            success: function (response) {
                if (response.status === 'NOK') {
                    var notice = new PNotify({
                        title: 'Erro',
                        text: 'Tente novamente mais tarde',
                        type: 'error',
                        addclass: 'click-2-close',
                        hide: false,
                        buttons: {
                            closer: false,
                            sticker: false
                        }
                    });
                    notice.get().click(function () {
                        notice.remove();
                    });
                } else {
                    // recarrega lista de leitos por onde o paciente passou
                   // jQuery('#use_service_beds').load(jQuery("body").data("baseurl") + "hospitalization/use_service_beds");
                    location.reload();
                }

            }
        });
    }
</script>
