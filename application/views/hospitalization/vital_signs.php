
<div id="new_vital_sign" hidden="">
    <div class="container-fluid">
        <legend>Adicionar sinal vital</legend>
        <div class="well">
            <div class="row-fluid">
                <div class="col-sm-2">
                    <h5><label class="control-label"><?= lang('blood_pressure') ?></label></h5>

                    <input id="blood_pressure" type="text" class="form-control" /> 
                </div>

                <div class="col-sm-2">
                    <h5><label class="control-label"><?= lang('heart_rate') ?></label></h5>

                    <input id="heart_rate" type="text" class="form-control"/> 
                </div>

                <div class="col-sm-2">
                    <h5><label class="control-label"><?= lang('respiratory_rate') ?></label></h5>

                    <input id="respiratory_rate" type="text" class="form-control" /> 
                </div>

                <div class="col-sm-2">
                    <h5><label class="control-label">Sat02</label></h5>

                    <input id="sato2" type="text" class="form-control" /> 
                </div>

                <div class="col-sm-2">
                    <h5><label class="control-label"><?= lang('temperature') ?></label></h5>

                    <input id="temperature" type="text" class="form-control"/> 
                </div>

                <div class="col-sm-2">
                    <h5><label class="control-label"><?= lang('glicosimetria') ?></label></h5>

                    <input id="glicosimetria" type="text" class="form-control"/> 
                </div>
            </div>

            <div class="row-fluid">
                <div class="col-sm-3">
                    <h5><label class="control-label"><?= lang('ocular_response') ?></label></h5>
                    <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="aphm_ocular_response">
                        <option value="0" selected> - </option>
                        <option value="4">
                            4 - <?= lang('he_opens_eyes_spontaneously') ?>
                        </option>
                        <option value="3">
                            3 - <?= lang('open_your_eyes_response_call') ?>
                        </option>
                        <option value="2">
                            2 - <?= lang('open_your_eyes_response_pain_stimulus') ?>
                        </option>
                        <option value="1">
                            1 - <?= lang('do_not_open_your_eyes') ?>
                        </option>
                    </select>
                </div>

                <div class="col-sm-3">
                    <h5><label class="control-label"><?= lang('verbal_response') ?></label></h5>
                    <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="aphm_verbal_response">
                        <option value="0" selected> - </option>
                        <option value="5">
                            5 - <?= lang('oriented_talk_normally') ?>
                        </option>
                        <option value="4">
                            4 - <?= lang('confused_disoriented') ?>
                        </option>
                        <option value="3">
                            3 - <?= lang('pronounces_disconnected_words') ?>
                        </option>
                        <option value="2">
                            2 - <?= lang('emits_incomprehensible_sounds') ?>
                        </option>
                        <option value="1">
                            1 - <?= lang('muted') ?>
                        </option>
                    </select>
                </div>

                <div class="col-sm-4">
                    <h5><label class="control-label"><?= lang('motor_response') ?></label></h5>
                    <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="aphm_motor_response">
                        <option value="0" selected> - </option>
                        <option value="6">
                            6 - <?= lang('obeys_commands') ?>
                        </option>
                        <option value="5">
                            5 - <?= lang('locate_painful_stimuli') ?>
                        </option>
                        <option value="4">
                            4 - <?= lang('nonspecific_flexion_reflex_withdrawal_painful_stimuli') ?>
                        </option>
                        <option value="3">
                            3 - <?= lang('abnormal_flexion_painful_stimuli') ?>
                        </option>
                        <option value="2">
                            2 - <?= lang('extension_painful_stimuli') ?>
                        </option>
                        <option value="1">
                            1 - <?= lang('it_not_moves') ?>
                        </option>
                    </select>
                </div>
                <div class="col-sm-2">
                    <h5><label class="control-label"><?= lang('glasgow_scale') ?></label></h5>
                    <input id="glasgow_scale" type="number" class="form-control" value="0" disabled=""/> 
                </div>
            </div>

            <div class="row-fluid">
                <div class="btn-group pull-right" style=" margin-top:20px;">
                    <a class="btn btn-danger btn-sm" href="javascript:void(0)" onclick="jQuery('#new_vital_sign').toggle();
                            jQuery('textarea').val('');"><?= lang('close') ?></a>
                    <a class="btn btn-success btn-sm" href="javascript:void(0)" onclick="save_vital_signs()" > <?= lang('save') ?></a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="search-filter">
    <legend>Sinais vitais
        <a class="mb-xs mt-xs mr-xs btn btn-success" href="javascript:void(0)" onclick="jQuery('#new_vital_sign').toggle()" style="float: right"> <i class="fa fa-plus-square-o"></i> Adicionar sinal vital</a>
    </legend>        
</div>

<div class="well">
    <?php if (!empty($vital_signs)): ?>
        <table class="table table-bordered table-striped mb-none" >
            <thead>
                <tr>
                    <th><?= lang('datetime_realization') ?></th>
                    <th><?= lang('blood_pressure') ?></th>
                    <th><?= lang('heart_rate') ?></th>
                    <th><?= lang('respiratory_rate') ?></th>
                    <th>Sat02</th>
                    <th><?= lang('temperature') ?></th>
                    <th><?= lang('glicosimetria') ?></th>
                    <th><?= lang('ocular_response') ?></th>
                    <th><?= lang('verbal_response') ?></th>
                    <th><?= lang('motor_response') ?></th>
                    <th><?= lang('glasgow_scale') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($vital_signs as $e): ?>
                    <tr>
                        <th><?= $e['created'] ?></th>
                        <th><?= $e['pa'] ?></th>
                        <th><?= $e['fc'] ?></th>
                        <th><?= $e['fr'] ?></th>
                        <th><?= $e['sato2'] ?></th>
                        <th><?= $e['axillary_temperature'] ?></th>
                        <th><?= $e['glicosimetria'] ?></th>
                        <th><?= $e['glasgow_visual_response'] ?></th>
                        <th><?= $e['glasgow_verbal_response'] ?></th>
                        <th><?= $e['glasgow_motor_response'] ?></th>
                        <th><?= $e['glasgow_score'] ?></th>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php else: ?>
        <h3>Nenhum sinal vital cadastrado</h3>
    <?php endif; ?>
</div>

<script>

    function save_vital_signs() {
        jQuery.ajax({
            url: jQuery("body").data("baseurl") + "hospitalization/save_vital_signs",
            type: "post",
            dataType: 'json',
            data: {
                pa: jQuery('#blood_pressure').val(),
                fc: jQuery('#heart_rate').val(),
                fr: jQuery('#respiratory_rate').val(),
                sato2: jQuery('#sato2').val(),
                axillary_temperature: jQuery('#temperature').val(),
                glicosimetria: jQuery('#glicosimetria').val(),
                glasgow_visual_response: jQuery('#aphm_ocular_response').val(),
                glasgow_verbal_response: jQuery('#aphm_verbal_response').val(),
                glasgow_motor_response: jQuery('#aphm_motor_response').val(),
                glasgow_score: jQuery('#glasgow_scale').val()
            },
            success: function (response) {
                if (response.status === 'NOK') {
                    var notice = new PNotify({
                        title: 'Erro',
                        text: 'Tente novamente mais tarde',
                        type: 'error',
                        addclass: 'click-2-close',
                        hide: false,
                        buttons: {
                            closer: false,
                            sticker: false
                        }
                    });
                    notice.get().click(function () {
                        notice.remove();
                    });
                } else {
                    // recarrega lista de sinais vitais
                    jQuery('#hospitalization_steps').load(jQuery("body").data("baseurl") + "hospitalization/vital_signs");
                }

            }
        });
    }
</script>
<script>
    //sinais vitais
    jQuery('#aphm_ocular_response').change(function () {
        var a = parseInt(jQuery('#aphm_ocular_response').val());
        var b = parseInt(jQuery('#aphm_verbal_response').val());
        var c = parseInt(jQuery('#aphm_motor_response').val());
        jQuery('#glasgow_scale').val(a + b + c);
    });


    jQuery('#aphm_verbal_response').change(function () {
        var a = parseInt(jQuery('#aphm_ocular_response').val());
        var b = parseInt(jQuery('#aphm_verbal_response').val());
        var c = parseInt(jQuery('#aphm_motor_response').val());
        jQuery('#glasgow_scale').val(a + b + c);
    });

    jQuery('#aphm_motor_response').change(function () {
        var a = parseInt(jQuery('#aphm_ocular_response').val());
        var b = parseInt(jQuery('#aphm_verbal_response').val());
        var c = parseInt(jQuery('#aphm_motor_response').val());
        jQuery('#glasgow_scale').val(a + b + c);
    }); //fim sinais vitais
</script>