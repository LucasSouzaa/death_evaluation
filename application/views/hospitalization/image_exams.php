<div class="search-filter">
    <legend><?= lang('image_exams') ?>
        <a class="mb-xs mt-xs mr-xs modal-basic btn btn-success" href="#modalAddImageExam" style="float: right"> <i class="fa fa-plus-square-o"></i> <?= lang('add_exam') ?></a>
    </legend>
</div>

<div class="well">
    <?php if (!empty($image_exams)): ?>
        <table class="table table-bordered table-striped mb-none" >
            <thead>
                <tr>
                    <th><?= lang('exam') ?></th>
                    <th><?= lang('solicitation_date') ?></th>
                    <th><?= lang('result_date') ?></th>
                    <th><?= lang('result') ?></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($image_exams as $e): ?>
                    <tr>
                        <th><?= $e['name'] ?></th>
                        <th><?= $e['created'] ?></th>
                        <th><?php
                            if (isset($e['report_date'])): echo $e['report_date'];
                            endif;
                            ?></th>
                        <th style="width: 15%; text-align: center;"><?php
                            if (isset($e['report'])): echo $e['report'];
                            else:
                                ?>
                                <a class="mb-xs mt-xs mr-xs modal-basic btn btn-success" href="#modalAddResult" onclick="jQuery('#exam_id').val(<?= $e['id'] ?>)"> <i class="fa fa-plus-square-o"></i> <?= lang('add_result') ?></a>
                            <?php endif ?></th>
                        <th><a href="javascript:void(0)" onclick="delete_image_exam('<?= $this->encrypt->encode($e['id']) ?>')" class="on-default remove-row" style=" margin-right: 5px; margin-left: 5px;"><i class="fa fa-trash-o"></i></a></th>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php else: ?>
        <h3>Nenhum exame cadastrado</h3>
    <?php endif; ?>
</div>

<input id="exam_id" hidden=""/>

<div id="modalAddResult" class="modal-block mfp-hide" style="width: 80%;">
    <section class="panel">
        <header class="panel-heading">
            <h2 class="panel-title"><?= lang('add_result') ?></h2>
        </header>
        <div class="panel-body">
            <div class="modal-wrapper">
                <div class="well">
                    <div class="row-fluid">
                        <div class="span6">
                            <h5><label class="control-label">Laudo</label></h5>
                            <textarea class="form-control" id="report"></textarea>
                        </div>
                        <div class="span6">
                            <h5><label class="control-label">Data/Hora da realização</label></h5>
                            <input id="report_date" type="datetime-local" class="form-control"/> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer class="panel-footer">
            <div class="row">
                <div class="col-md-12 text-right">
                    <button class="btn btn-primary" onclick="save_image_exam_result()"><?= lang('save') ?></button>
                    <button class="btn btn-default modal-dismiss" id="modal_close_button"><?= lang('close') ?></button>
                </div>
            </div>
        </footer>
    </section>
</div>

<div id="modalAddImageExam" class="modal-block mfp-hide" style="width: 80%;">
    <section class="panel">
        <header class="panel-heading">
            <h2 class="panel-title"><?= lang('add_image_exam') ?></h2>
        </header>
        <div class="panel-body">
            <div class="modal-wrapper">

                <div class="well">
                    <div class="row-fluid">
                        <label><h4>Exames de imagens</h4></label>
                    </div>	
                    <!-- percorre o vetor com os tipos de exames radiologicos que veio do BD->model -->
                    <div class="row-fluid">
                        <h5>Radiografia simples</h5>
                        <!-- percorre o vetor de partes do corpo setado no model -->
                    </div>
                    <div class="row-fluid examesImagem">
                        <div class="span4">
                            <label>
                                <input type="checkbox" value="1"> Crânio	
                            </label>
                        </div>

                        <div class="span4">
                            <label>
                                <input type="checkbox" value="2"> Coluna cervical	
                            </label>
                        </div>

                        <div class="span4">
                            <label>
                                <input type="checkbox" value="3"> Coluna vertebral
                            </label>
                        </div>
                    </div>
                    <div class="row-fluid examesImagem">
                        <div class="span4">
                            <label>
                                <input type="checkbox" value="4"> Tórax	
                            </label>
                        </div>
                        <div class="span4">
                            <label>
                                <input type="checkbox" value="5"> Pelve				
                            </label>
                        </div>

                        <div class="span4">
                            <label>
                                <input type="checkbox" value="6"> Membros Superiores
                            </label>
                        </div>
                    </div>
                    <div class="row-fluid examesImagem">
                        <div class="span4">
                            <label>
                                <input type="checkbox" value="7"> Membros Inferiores
                            </label>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <h5>Ultrassonografia</h5>
                        <!-- percorre o vetor de partes do corpo setado no model -->
                    </div>
                    <div class="row-fluid examesImagem">
                        <div class="span4">
                            <label>
                                <input type="checkbox" value="8"> Fast	
                            </label>
                        </div>
                        <div class="span4">
                            <label>
                                <input type="checkbox" value="9"> Abdome completo
                            </label>
                        </div>
                        <div class="span4">
                            <label>
                                <input type="checkbox" value="10"> Doppler cervical
                            </label>
                        </div>
                    </div>
                    <div class="row-fluid examesImagem">
                        <div class="span4">
                            <label>
                                <input type="checkbox" value="11"> Doppler Scan dos membros inferiores
                            </label>
                        </div>
                        <div class="span4">
                            <label>
                                <input type="checkbox" value="12"> Doppler Scan dos membros superiores
                            </label>
                        </div>
                        <div class="span4">
                            <label>
                                <input type="checkbox" value="13"> Ecocardiografia	
                            </label>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <h5>Tomografia computadorizada</h5>
                        <!-- percorre o vetor de partes do corpo setado no model -->
                    </div>
                    <div class="row-fluid examesImagem">
                        <div class="span4">
                            <label>
                                <input type="checkbox" value="14"> Crânio				
                            </label>
                        </div>
                        <div class="span4">
                            <label>
                                <input type="checkbox" value="15"> Face						
                            </label>
                        </div>
                    </div>
                    <div class="row-fluid examesImagem">
                        <div class="span4">
                            <label>
                                <input type="checkbox" value="16"> Pescoço			
                            </label>
                        </div>
                        <div class="span4">
                            <label>
                                <input type="checkbox" value="17"> Coluna cervical
                            </label>
                        </div>
                        <div class="span4">
                            <label>
                                <input type="checkbox" value="18"> Coluna torácica	
                            </label>
                        </div>
                    </div>
                    <div class="row-fluid examesImagem">
                        <div class="span4">
                            <label>
                                <input type="checkbox" value="19"> Coluna lombar	
                            </label>
                        </div>
                        <div class="span4">
                            <label>
                                <input type="checkbox" value="20"> Tórax	
                            </label>
                        </div>
                        <div class="span4">
                            <label>
                                <input type="checkbox" value="21"> Abdome		
                            </label>
                        </div>
                    </div>
                    <div class="row-fluid examesImagem">
                        <div class="span4">
                            <label>
                                <input type="checkbox" value="22"> Pelve				
                            </label>
                        </div>
                        <div class="span4">
                            <label>
                                <input type="checkbox" value="23"> Membros Superiores
                            </label>
                        </div>
                        <div class="span4">
                            <label>
                                <input type="checkbox" value="24"> Membros Inferiores		
                            </label>
                        </div>
                    </div>
                    <div class="row-fluid examesImagem">
                        <div class="span4">
                            <label>
                                <input type="checkbox" value="25"> Angio TC - Coluna cervical	
                            </label>
                        </div>
                        <div class="span4">
                            <label>
                                <input type="checkbox" value="26"> Angio TC - Coluna torácica	
                            </label>
                        </div>
                        <div class="span4">
                            <label>
                                <input type="checkbox" value="27"> Angio TC - Coluna lombar
                            </label>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <h5>Arteriografia</h5>
                        <!-- percorre o vetor de partes do corpo setado no model -->
                    </div>
                    <div class="row-fluid examesImagem">
                        <div class="span4">
                            <label>
                                <input type="checkbox" value="28"> Crânio
                            </label>
                        </div>
                        <div class="span4">
                            <label>
                                <input type="checkbox" value="29"> Tórax	
                            </label>
                        </div>
                        <div class="span4">
                            <label>
                                <input type="checkbox" value="30"> Abdome		
                            </label>
                        </div>
                    </div>
                    <div class="row-fluid examesImagem">
                        <div class="span4">
                            <label>
                                <input type="checkbox" value="31"> Membros Superiores
                            </label>
                        </div>

                        <div class="span4">
                            <label>
                                <input type="checkbox" value="32"> Membros Inferiores
                            </label>
                        </div>

                    </div>
                    <div class="row-fluid">
                        <h5>Ressonância nuclear magnética</h5>
                        <!-- percorre o vetor de partes do corpo setado no model -->
                    </div>
                    <div class="row-fluid examesImagem">
                        <div class="span4">
                            <label>
                                <input type="checkbox" value="33"> Crânio	
                            </label>
                        </div>
                        <div class="span4">
                            <label>
                                <input type="checkbox" value="34"> Coluna vertebral
                            </label>
                        </div>
                        <div class="span4">
                            <label>
                                <input type="checkbox" value="35"> Abdome
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer class="panel-footer">
            <div class="row">
                <div class="col-md-12 text-right">
                    <button class="btn btn-primary" onclick="save_image_exam()"><?= lang('save') ?></button>
                    <button class="btn btn-default modal-dismiss" id="modal_close_button"><?= lang('close') ?></button>
                </div>
            </div>
        </footer>
    </section>
</div>
<script src="<?= $this->config->base_url(JSPATH . 'examples.modals.js') ?>"></script>

<script>
                        function save_image_exam() {

                            var count = 0;
                            var exams = [];
                            jQuery('.examesImagem input:checked').each(function (element) {
                                exams.push(jQuery(this).val());
                                count++;
                            });

                            if (count < 1) {
                                var notice = new PNotify({
                                    title: 'Erro',
                                    text: 'algum exame precisa ser selecionado',
                                    type: 'error',
                                    addclass: 'click-2-close',
                                    hide: false,
                                    buttons: {
                                        closer: false,
                                        sticker: false
                                    }
                                });
                                notice.get().click(function () {
                                    notice.remove();
                                });
                                return false;
                            }

                            jQuery.ajax({
                                url: jQuery("body").data("baseurl") + "hospitalization/save_image_exam",
                                type: "post",
                                dataType: 'json',
                                data: {
                                    exams: exams
                                },
                                success: function (response) {
                                    jQuery('#modal_close_button').click();
                                    if (response.status === 'NOK') {
                                        var notice = new PNotify({
                                            title: 'Erro',
                                            text: 'Tente novamente mais tarde',
                                            type: 'error',
                                            addclass: 'click-2-close',
                                            hide: false,
                                            buttons: {
                                                closer: false,
                                                sticker: false
                                            }
                                        });
                                        notice.get().click(function () {
                                            notice.remove();
                                        });
                                    } else {
                                        // recarrega lista de exames
                                        jQuery('#hospitalization_steps').load(jQuery("body").data("baseurl") + "hospitalization/image_exams");
                                    }
                                }
                            });
                        }

                        function save_image_exam_result() {

                            if (!jQuery('#report').val()) {
                                var notice = new PNotify({
                                    title: 'Erro',
                                    text: 'Algum valor precisa ser adicionado',
                                    type: 'error',
                                    addclass: 'click-2-close',
                                    hide: false,
                                    buttons: {
                                        closer: false,
                                        sticker: false
                                    }
                                });
                                notice.get().click(function () {
                                    notice.remove();
                                });
                                return false;
                            }

                            if (!jQuery('#report_date').val()) {
                                var notice = new PNotify({
                                    title: 'Erro',
                                    text: 'A data do resultado precisa ser adicionada',
                                    type: 'error',
                                    addclass: 'click-2-close',
                                    hide: false,
                                    buttons: {
                                        closer: false,
                                        sticker: false
                                    }
                                });
                                notice.get().click(function () {
                                    notice.remove();
                                });
                                return false;
                            }

                            jQuery.ajax({
                                url: jQuery("body").data("baseurl") + "attendance_hospital/save_image_exam_result",
                                type: "post",
                                dataType: 'json',
                                data: {
                                    report: jQuery('#report').val(),
                                    report_date: jQuery('#report_date').val(),
                                    exam_id: jQuery('#exam_id').val()
                                },
                                success: function (response) {
                                    jQuery('#modal_close_button').click();
                                    if (response.status === 'NOK') {
                                        var notice = new PNotify({
                                            title: 'Erro',
                                            text: 'Tente novamente mais tarde',
                                            type: 'error',
                                            addclass: 'click-2-close',
                                            hide: false,
                                            buttons: {
                                                closer: false,
                                                sticker: false
                                            }
                                        });
                                        notice.get().click(function () {
                                            notice.remove();
                                        });
                                    } else {
                                        // recarrega lista de exames
                                        jQuery('#hospitalization_steps').load(jQuery("body").data("baseurl") + "hospitalization/image_exams");
                                    }
                                }
                            });
                        }

                        function delete_image_exam(id) {
                            jQuery.ajax({
                                url: jQuery("body").data("baseurl") + "hospitalization/delete_image_exam",
                                type: "post",
                                dataType: 'json',
                                data: {
                                    id: id
                                },
                                success: function (response) {
                                    if (response.status === 'NOK') {
                                        var notice = new PNotify({
                                            title: 'Erro',
                                            text: 'Tente novamente mais tarde',
                                            type: 'error',
                                            addclass: 'click-2-close',
                                            hide: false,
                                            buttons: {
                                                closer: false,
                                                sticker: false
                                            }
                                        });
                                        notice.get().click(function () {
                                            notice.remove();
                                        });
                                    } else {
                                        // recarrega lista de exames
                                        jQuery('#hospitalization_steps').load(jQuery("body").data("baseurl") + "hospitalization/image_exams");
                                    }
                                }
                            });
                        }

</script>
