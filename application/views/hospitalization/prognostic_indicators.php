
    <div class="search-filter">
        <legend>
            Indicadores de Prognóstico
        </legend>        
    </div>
    <div class="well">
        <div class="row">
            <table class="table table-bordered table-hover table-condensed">
                    <thead>
                        <tr>
                            <th>
                                RTS     
                            </th>
                            <th>
                                ISS
                            </th>
                            <th>
                                TRISS
                            </th>
                            <th>
                                NISS
                            </th>
                            <th>
                                NTRISS
                            </th>
                            <th>
                                T-RTS
                            </th>
                        </tr>
                    </thead>
                     <tbody>
                        <?php foreach ($indices as $i): ?>
                            <tr>
                                <td>
                                    <?= $i['rts_resultado']; ?>
                                </td>
                                <td>
                                    <?= $i['iss_resultado']; ?>
                                </td>
                                <td>
                                    <?= $i['triss_resultado']; ?>
                                </td>
                                <td>
                                    <?= $i['niss_resultado']; ?>
                                </td>
                                <td>
                                    <?= $i['ntriss_resultado']; ?>
                                </td>
                                <td>
                                    <?php 
                                    $trts=0;
                                    if($i['glasgow_resultado']<=5&&$i['glasgow_resultado']>=4)
                                        $trts=$trts+1;
                                    else if($i['glasgow_resultado']<=8&&$i['glasgow_resultado']>=6)
                                        $trts=$trts+2;
                                    else if($i['glasgow_resultado']<=9&&$i['glasgow_resultado']>=12)
                                        $trts=$trts+3;
                                    else if($i['glasgow_resultado']<=13&&$i['glasgow_resultado']>=15)
                                        $trts=$trts+4;
                                    
                                    if($i['pas']<=49&&$i['pas']>=1)
                                        $trts=$trts+1;
                                    else if($i['pas']<=75&&$i['pas']>=50)
                                        $trts=$trts+2;
                                    else if($i['pas']<=89&&$i['pas']>=76)
                                        $trts=$trts+3;
                                    else if($i['pas']>89)
                                        $trts=$trts+4;
                                    
                                    if($i['fr']<=5&&$i['fr']>=1)
                                        $trts=$trts+1;
                                    else if($i['fr']<=9&&$i['fr']>=6)
                                        $trts=$trts+2;
                                    else if($i['fr']<=29&&$i['fr']>=10)
                                        $trts=$trts+4;
                                    else if($i['fr']>89)
                                        $trts=$trts+3;
                                    echo $trts;
                                    ?>
                                </td>
                            </tr>		
                        <?php endforeach; ?>
                    </tbody>
        </div>
    </div>


