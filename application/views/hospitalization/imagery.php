<link rel="stylesheet" href="<?= $this->config->base_url(PLUGINPATH . 'dropzone/css/basic.css') ?>" />		
<link rel="stylesheet" href="<?= $this->config->base_url(PLUGINPATH . 'dropzone/css/dropzone.css') ?>" />		
<script type="text/javascript" src="<?= $this->config->base_url(JSPATH . "imagery.js") ?>"></script>

<div class="search-filter">
    <legend><?= lang('images') ?>
        <a class="mb-xs mt-xs mr-xs modal-basic btn btn-success" href="#modalAddImagery" style="float: right"> <i class="fa fa-plus-square-o"></i> <?= lang('add_image') ?></a>
    </legend>        
</div>

<div class="well">
    <?php if (!empty($images)): ?>
        <table class="table table-bordered table-striped mb-none" >
            <thead>
                <tr>
                    <th><?= lang('description') ?></th>
                    <th><?= lang('date') ?></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($images as $i): ?>
                    <tr>
                        <td><?= $i['description'] ?></td>
                        <td><?= $i['created'] ?></td>
                        <td>
                            <a href="javascript:void(0)" onclick="view_imagery(<?= $i['id'] ?>)" style="margin-left: 5px;"><i class="fa fa-eye"></i></a>
                            <a href="javascript:void(0)" onclick="delete_imagery(<?= $i['id'] ?>)" style="margin-left: 5px;"><i class="fa fa-trash-o"></i></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php else: ?>
        <h3>Nenhuma imagem cadastrada</h3>
    <?php endif; ?>
</div>

<!--ADICIONANDO IMAGEM-->
<div id="modalAddImagery" class="modal-block mfp-hide" style="width: 80%;">
    <section class="panel">
        <header class="panel-heading">
            <h2 class="panel-title"><?= lang('add_image') ?></h2>
        </header>
        <div class="panel-body">
            <div class="modal-wrapper">
                <div class="well">
                    <div class="row-fluid">
                        <form action="<?= $this->config->base_url('hospitalization/receive_imagery') ?>" class="dropzone dz-square" id="dropzone-image"></form>
                    </div>
                </div>
            </div>
        </div>
        <footer class="panel-footer">
            <div class="row">
                <div class="col-md-12 text-right">
                    <button class="btn btn-default modal-dismiss" id="modal_close_button"><?= lang('close') ?></button>
                </div>
            </div>
        </footer>
    </section>
</div>

<a class="mb-xs mt-xs mr-xs modal-basic" hidden="" id="modalShowImageButton" href="#modalShowImage"></a>

<!--VISUALIZANDO IMAGEM-->
<div id="modalShowImage" class="modal-block mfp-hide">
    <section class="panel">
        <header class="panel-heading">
            <h2 class="panel-title"><?= lang('image') ?></h2>
        </header>
        <div class="panel-body">
            <div class="modal-wrapper" id="view_image" style="text-align: -webkit-center;"></div>
        </div>
        <footer class="panel-footer">
            <div class="row">
                <div class="col-md-12 text-right">
                    <button class="btn btn-default modal-dismiss"><?= lang('close') ?></button>
                </div>
            </div>
        </footer>
    </section>
</div>
<script src="<?= $this->config->base_url(JSPATH . 'examples.modals.js') ?>"></script>
<script src="<?= $this->config->base_url(PLUGINPATH . 'dropzone/dropzone.js') ?>"></script>		
<script>
                        Dropzone.autoDiscover = false; // otherwise will be initialized twice
                        var myDropzoneOptionsImage = {
                            maxFiles: 1,
                            maxFilesize: 500000000000000,
                            acceptedFiles: 'image/*',
                            init: function () {
                                this.on("complete", function (file) {
                                    jQuery('#hospitalization_steps').load(jQuery("body").data("baseurl") + "hospitalization/imagery");
                                    jQuery('.modal-dismiss').click();
                                });
                            }
                        };
                        var myDropzoneImage = new Dropzone('#dropzone-image', myDropzoneOptionsImage);
</script>

