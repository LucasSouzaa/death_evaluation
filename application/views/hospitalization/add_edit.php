<div class="row" style="padding-top: 20px;">
    <div class="col-sm-2" style=" padding-left: 0px">
        <div id="MainMenu">
            <div class="list-group panel">
                <a id="li_clinical_evolution_layout" href="javascript:void(0);" class="list-group-item" onclick="load_hospitalization_steps('clinical_evolution')"><?= lang('clinical_evolution') ?></a>

                <a href="#passagesCollapse" class="list-group-item " data-toggle="collapse" data-parent="#MainMenu"> <i class="glyphicon glyphicon-plus"></i> Passagens</a>
                <div class="collapse" id="passagesCollapse">
                    <a id="li_visits_layout" href="javascript:void(0);" class="list-group-item" onclick="load_hospitalization_steps('visits')"><?= lang('visits') ?></a>
                    <a id="li_intercurrences_layout" href="javascript:void(0);" class="list-group-item" onclick="load_hospitalization_steps('intercurrences')"><?= lang('intercurrences') ?></a>
                </div>

                <a href="#examsCollapse" class="list-group-item " data-toggle="collapse" data-parent="#MainMenu"> <i class="glyphicon glyphicon-plus"></i> <?= lang('exams') ?></a>
                <div class="collapse" id="examsCollapse">
                    <a id="li_biochemistry_exams_layout" href="javascript:void(0);" class="list-group-item list-group-submenu " onclick="load_hospitalization_steps('biochemistry_exams')"><?= lang('biochemical') ?></a>
                    <a id="li_image_exams_layout" href="javascript:void(0);" class="list-group-item list-group-submenu" onclick="load_hospitalization_steps('image_exams')"><?= lang('image') ?></a>
<!--                    <a id="li_microbiology_exams_layout" href="javascript:void(0);" class="list-group-item list-group-submenu" onclick="load_hospitalization_steps('microbiology_exams')"><?= lang('microbiological') ?></a>-->
                    <a id="li_anatomical_pathological_exams_layout" href="javascript:void(0);" class="list-group-item list-group-submenu" onclick="load_hospitalization_steps('anatomical_pathological_exams')"><?= lang('anatomic_pathology') ?></a>
                </div>
                <a id="li_diagnostics_layout" href="javascript:void(0);" class="list-group-item " onclick="load_hospitalization_steps('diagnostics')"><?= lang('diagnostics') ?></a>
                <a id="li_drugs_layout" href="javascript:void(0);" class="list-group-item " onclick="load_hospitalization_steps('drugs')"><?= lang('drugs') ?></a>
                <a id="li_surgery_anesthesia_layout" href="javascript:void(0);" class="list-group-item " onclick="load_hospitalization_steps('surgery_anesthesia')"><?= lang('surgery') ?> / <?= lang('anesthesia') ?></a>
                <a id="li_procedures_layout" href="javascript:void(0);" class="list-group-item " onclick="load_hospitalization_steps('procedures')"><?= lang('procedures') ?></a>
                <a id="li_vital_signs_layout" href="javascript:void(0);" class="list-group-item " onclick="load_hospitalization_steps('vital_signs')"><?= lang('vital_signs') ?></a>
                <a id="li_use_of_blood_layout" href="javascript:void(0);" class="list-group-item " onclick="load_hospitalization_steps('use_of_blood')"><?= lang('use_of_blood') ?></a>
                <a id="li_medical_management_layout" href="javascript:void(0);" class="list-group-item " onclick="load_hospitalization_steps('medical_management')"><?= lang('medical_management') ?></a>
<!--                <a id="li_prognostic_indicators_layout" href="javascript:void(0);" class="list-group-item " onclick="load_hospitalization_steps('prognostic_indicators')"><?= lang('prognostic_indicators') ?></a>-->
                <a id="li_imagery_layout" href="javascript:void(0);" class="list-group-item " onclick="load_hospitalization_steps('imagery')"><?= lang('imagery') ?></a>
                <a id="li_outpatients_layout" href="javascript:void(0);" class="list-group-item " onclick="load_hospitalization_steps('outpatient_condition')"><?= lang('outpatient_condition') ?></a>
            </div>
        </div>
    </div>
    <div class="col-sm-10">
        <div id="content" class="row-fluid">
            <div id="hospitalization_steps" style="margin-top:20px;"></div>

        </div>
    </div>
</div>

<script>
    jQuery('li').removeClass('active');
    jQuery('#li_hospitalization').addClass('active');
    jQuery('#hospitalization_steps').load(jQuery("body").data("baseurl") + "hospitalization/clinical_evolution");

    function load_hospitalization_steps(type) {
        if (type === 'clinical_evolution') {
            jQuery('#hospitalization_steps').load(jQuery("body").data("baseurl") + "hospitalization/clinical_evolution");
            jQuery('li').removeClass('active');
            jQuery('a').removeClass('active');
            jQuery('#li_hospitalization').addClass('active');
            jQuery('#li_clinical_evolution_layout').addClass('active');
        } else if (type === 'visits') {
            jQuery('#hospitalization_steps').load(jQuery("body").data("baseurl") + "hospitalization/visits");
            jQuery('li').removeClass('active');
            jQuery('a').removeClass('active');
            jQuery('#li_hospitalization').addClass('active');
            jQuery('#li_visits_layout').addClass('active');
        } else if (type === 'intercurrences') {
            jQuery('#hospitalization_steps').load(jQuery("body").data("baseurl") + "hospitalization/intercurrences");
            jQuery('li').removeClass('active');
            jQuery('a').removeClass('active');
            jQuery('#li_hospitalization').addClass('active');
            jQuery('#li_intercurrences_layout').addClass('active');
        } else if (type === 'biochemistry_exams') {
            jQuery('#hospitalization_steps').load(jQuery("body").data("baseurl") + "hospitalization/biochemistry_exams");
            jQuery('li').removeClass('active');
            jQuery('a').removeClass('active');
            jQuery('#li_hospitalization').addClass('active');
            jQuery('#li_biochemistry_exams_layout').addClass('active');
        } else if (type === 'image_exams') {
            jQuery('#hospitalization_steps').load(jQuery("body").data("baseurl") + "hospitalization/image_exams");
            jQuery('li').removeClass('active');
            jQuery('a').removeClass('active');
            jQuery('#li_hospitalization').addClass('active');
            jQuery('#li_image_exams_layout').addClass('active');
//        } else if (type === 'microbiology_exams') {
//            jQuery('#hospitalization_steps').load(jQuery("body").data("baseurl") + "hospitalization/microbiology_exams");
//            jQuery('li').removeClass('active');
//            jQuery('a').removeClass('active');
//            jQuery('#li_hospitalization').addClass('active');
//            jQuery('#li_microbiology_exams_layout').addClass('active');
        } else if (type === 'anatomical_pathological_exams') {
            jQuery('#hospitalization_steps').load(jQuery("body").data("baseurl") + "hospitalization/anatomical_pathological_exams");
            jQuery('li').removeClass('active');
            jQuery('a').removeClass('active');
            jQuery('#li_hospitalization').addClass('active');
            jQuery('#li_anatomical_pathological_exams_layout').addClass('active');
        } else if (type === 'diagnostics') {
            jQuery('#hospitalization_steps').load(jQuery("body").data("baseurl") + "attendance_hospital/diagnosys");
            jQuery('li').removeClass('active');
            jQuery('a').removeClass('active');
            jQuery('#li_hospitalization').addClass('active');
            jQuery('#li_diagnostics_layout').addClass('active');
        } else if (type === 'drugs') {
            jQuery('#hospitalization_steps').load(jQuery("body").data("baseurl") + "hospitalization/drugs");
            jQuery('li').removeClass('active');
            jQuery('a').removeClass('active');
            jQuery('#li_hospitalization').addClass('active');
            jQuery('#li_drugs_layout').addClass('active');
        } else if (type === 'surgery_anesthesia') {
            jQuery('#hospitalization_steps').load(jQuery("body").data("baseurl") + "hospitalization/surgery_anesthesia");
            jQuery('li').removeClass('active');
            jQuery('a').removeClass('active');
            jQuery('#li_hospitalization').addClass('active');
            jQuery('#li_surgery_anesthesia_layout').addClass('active');
        } else if (type === 'procedures') {
            jQuery('#hospitalization_steps').load(jQuery("body").data("baseurl") + "hospitalization/procedures");
            jQuery('li').removeClass('active');
            jQuery('a').removeClass('active');
            jQuery('#li_hospitalization').addClass('active');
            jQuery('#li_procedures_layout').addClass('active');
        } else if (type === 'vital_signs') {
            jQuery('#hospitalization_steps').load(jQuery("body").data("baseurl") + "hospitalization/vital_signs");
            jQuery('li').removeClass('active');
            jQuery('a').removeClass('active');
            jQuery('#li_hospitalization').addClass('active');
            jQuery('#li_vital_signs_layout').addClass('active');
        } else if (type === 'use_of_blood') {
            jQuery('#hospitalization_steps').load(jQuery("body").data("baseurl") + "hospitalization/use_of_blood");
            jQuery('li').removeClass('active');
            jQuery('a').removeClass('active');
            jQuery('#li_hospitalization').addClass('active');
            jQuery('#li_use_of_blood_layout').addClass('active');
        } else if (type === 'medical_management') {
            jQuery('#hospitalization_steps').load(jQuery("body").data("baseurl") + "hospitalization/medical_management");
            jQuery('li').removeClass('active');
            jQuery('a').removeClass('active');
            jQuery('#li_hospitalization').addClass('active');
            jQuery('#li_medical_management_layout').addClass('active');
        } else if (type === 'prognostic_indicators') {
            jQuery('#hospitalization_steps').load(jQuery("body").data("baseurl") + "hospitalization/prognostic_indicators");
            jQuery('li').removeClass('active');
            jQuery('a').removeClass('active');
            jQuery('#li_hospitalization').addClass('active');
            jQuery('#li_prognostic_indicators_layout').addClass('active');
        } else if (type === 'imagery') {
            jQuery('#hospitalization_steps').load(jQuery("body").data("baseurl") + "hospitalization/imagery");
            jQuery('li').removeClass('active');
            jQuery('a').removeClass('active');
            jQuery('#li_hospitalization').addClass('active');
            jQuery('#li_imagery_layout').addClass('active');
        } else if (type === 'outpatient_condition') {
            jQuery('#hospitalization_steps').load(jQuery("body").data("baseurl") + "hospitalization/outpatient_condition");
            jQuery('li').removeClass('active');
            jQuery('a').removeClass('active');
            jQuery('#li_hospitalization').addClass('active');
            jQuery('#li_outpatient_condition_layout').addClass('active');
        }
    }
</script>