<div style="padding: 30px;">
    <div class="search-filter">
        <legend>Internações</legend>        
    </div>

    <div class="well">
        <?php if (!empty($hospitalizations)): ?>
            <table class="table table-bordered table-striped mb-none" >
                <thead>
                    <tr>
                        <th>Entrada</th>
                        <th>Saída</th>
                        <th>Tipo</th>
                        <th>Enviado para</th>
                        <th>Informações adicionais</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($hospitalizations as $e): ?>

                        <tr>
                            <th><?= $e['moment_admission'] ?>
                            </th>
                            <th><?= $e['moment_exit'] ?></th>
                            <th><?= $e['exit_type'] ?></th>
                            <th><?= $e['sent_to'] ?></th>
                            <th><?= $e['other_info'] ?></th>
                            <th style="text-align: -webkit-center;"><a style="padding: 0px 5px 0px 5px;" class="btn btn-success" href="javascript:void(0)" onclick="open_hospitalization('<?= $this->encrypt->encode($e['id']) ?>', '<?= $e['moment_admission'] ?>', '<?= $e['moment_exit'] ?>')">Ver internação</a></th>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        <?php else: ?>
            <h3>Nenhuma internação cadastrada</h3>
        <?php endif; ?>
    </div>
</div>

<script>
    jQuery('li').removeClass('active');
    jQuery('#li_hospitalization').addClass('active');
    function open_hospitalization(hosp_id, moment_admission, moment_exit) {
        jQuery.ajax({
            url: jQuery("body").data("baseurl") + "hospitalization/open_hospitalization",
            type: "post",
            dataType: 'json',
            data: {
                hosp_id: hosp_id,
                moment_admission: moment_admission,
                moment_exit: moment_exit
            },
            success: function (response) {
                window.location = jQuery("body").data("baseurl") + "hospitalization";
            }
        });
    }
</script>
