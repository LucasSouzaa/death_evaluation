</br>
<div id="new_return" hidden="">
    <div class="container-fluid">
        <legend><?= lang('add_return') ?></legend>
        <div class="well">

            <h3><label class="control-label">Condicao final</label></h3>
            <div class="row-fluid">
                <div class="col-sm-6">
                    <h5><label class="control-label">Condicão do paciente</label></h5>
                    <label class="control-label" style="font-weight: 500;"><?= $final_condition['exit_type'] ?></label>
                </div>
                <div class="col-sm-6">
                    <h5><label class="control-label">Data/Hora de saída</label></h5>
                    <label class="control-label" style="font-weight: 500;"><?= $final_condition['moment_admission'] ?></label>
                </div>
            </div>
            <div class="row-fluid">
                <div class="col-sm-12">
                    <h5><label class="control-label">Orientação para o paciente</label></h5>
                    <label class="control-label" style="font-weight: 500;"><?= $final_condition['patient_orientation'] ?></label>
                </div>
            </div>
            <div class="row-fluid">
                <div class="col-sm-12">
                    <h5><label class="control-label">Orientação para a família</label></h5>
                    <label class="control-label" style="font-weight: 500;"><?= $final_condition['family_orientation'] ?></label>
                </div>
            </div>
            <div class="row-fluid">
                <div class="col-sm-12">
                    <h5><label class="control-label">Orientação de retorno</label></h5>
                    <label class="control-label" style="font-weight: 500;"><?= $final_condition['return_orientation'] ?></label>
                </div>
            </div>
            <div class="row-fluid">
                <div class="col-sm-6">
                    <h5><label class="control-label">Data de retorno</label></h5>
                    <label class="control-label" style="font-weight: 500;"><?= $final_condition['return_date'] ?></label>
                </div>
                <div class="col-sm-6">
                    <h5><label class="control-label">Ambulatório</label></h5>
                    <label class="control-label" style="font-weight: 500;"><?= $final_condition['name'] ?></label>
                </div>
            </div>

            <h3><label class="control-label">Retorno</label></h3>
            <div class="row">
                <div class="col-lg-6">
                    <h5><label class="control-label"><?= lang('attended') ?></label></h5>
                    <div class="form-group">
                        <select class="form-control" id="attended">
                            <option value="1" selected="">Sim</option>
                            <option value="0">Não</option>
                        </select>
                    </div>
                </div>
            </div>

            <h5><label class="control-label go_return"><?= lang('resume') ?></label></h5>
            <div class="form-group go_return">
                <textarea style="height: 100px" class="form-control" id="resume"></textarea>
            </div>

            <div class="row go_return">
                <div class="col-lg-6">
                    <h5><label class="control-label"><?= lang('conduct') ?></label></h5>
                    <div class="form-group">
                        <select class="form-control" id="conduct">
                            <option value="return">Voltar para novo acompanhamento</option>
                            <option value="alta">Alta</option>
                            <option value="alta_a_pedido">Alta a pedido</option>
                            <option value="reinternado">Re-internação</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-6">
                    <h5><label class="control-label">Agendamento Data/Hora</label></h5>
                    <div class='input-group date input-append datetimepicker1'>
                        <input type='text' class="form-control" data-format="yyyy-MM-dd hh:mm:ss" id="return_date" />
                        <span class="input-group-addon add-on">
                            <span><i class="fa fa-calendar" data-time-icon="icon-time" data-date-icon="icon-calendar">
                                </i></span>
                        </span>
                    </div>
                </div>
            </div>

            <div class="row-fluid">
                <div class="btn-group pull-right" style=" margin-top:20px;">
                    <a class="btn btn-success btn-sm" href="javascript:void(0)" onclick="save_return()" > <?= lang('save') ?></a>
                    <a class="btn btn-danger btn-sm" href="javascript:void(0)" onclick="jQuery('#new_return').toggle();
                            jQuery('textarea').val('');"><?= lang('close') ?></a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="search-filter">
    <legend><?= lang('returns') ?>
        <a class="mb-xs mt-xs mr-xs btn btn-success" href="javascript:void(0)" onclick="jQuery('#new_return').toggle()" style="float: right"> <i class="fa fa-plus-square-o"></i> <?= lang('add_return') ?></a>
    </legend>        
</div>

<div class="well">
    <?php if (!empty($returns)): ?>
        <table class="table table-bordered table-striped mb-none" >
            <thead>
                <tr>
                    <th><?= lang('attended') ?></th>
                    <th><?= lang('datetime_realization') ?></th>
                    <th><?= lang('author') ?></th>
                    <th><?= lang('resume') ?></th>
                    <th><?= lang('conduct') ?></th>
                    <th><?= lang('return_date') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($returns as $e): ?>
                    <tr>
                        <th><?= ($e['attended'] === '1') ? 'Sim' : 'Não' ?></th>
                        <th><?= $e['created'] ?></th>
                        <th><?= $e['name'] ?></th>
                        <th><?= $e['resume'] ?></th>
                        <th><?= lang($e['conduct']) ?></th>
                        <th><?= $e['return_date'] ?></th>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php else: ?>
        <h3>Nenhum returno cadastrado</h3>
    <?php endif; ?>
</div>

<script type="text/javascript">
    jQuery(function () {
        jQuery('.datetimepicker1').datetimepicker({
            language: 'pt-BR'
        });
    });
    jQuery('#attended').on('change', function () {
        if (jQuery('#attended').val() === '1')
            jQuery('.go_return').show();
        else
            jQuery('.go_return').hide();
    });

    function save_return() {
        jQuery.ajax({
            url: jQuery("body").data("baseurl") + "outpatients/save_return",
            type: "post",
            dataType: 'json',
            data: {
                attended: jQuery('#attended').val(),
                return_date: jQuery('#return_date').val(),
                resume: jQuery('#resume').val(),
                conduct: jQuery('#conduct').val()
            },
            success: function (response) {
                if (response.status === 'NOK') {
                    var notice = new PNotify({
                        title: 'Erro',
                        text: 'Tente novamente mais tarde',
                        type: 'error',
                        addclass: 'click-2-close',
                        hide: false,
                        buttons: {
                            closer: false,
                            sticker: false
                        }
                    });
                    notice.get().click(function () {
                        notice.remove();
                    });
                } else {
                    // recarrega lista de retornos ambulatoriais
                    window.location = jQuery("body").data("baseurl") + "outpatients/index";
                }
            }
        });
    }
    
    jQuery('li').removeClass('active');
    jQuery('#li_outpatients').addClass('active');
</script>