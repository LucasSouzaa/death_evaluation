<legend> 
    <a href="javascript:void(0)" onclick="togglePanels('#ind_prog', '#ind_prog_options');">Indices prognósticos</a>
    <div id="ind_prog_chev" class="pull-right">
        <a href="javascript:void(0)" onclick="togglePanels('#ind_prog', '#ind_prog_options');"><i class="fa fa-plus-square-o"></i></a>
    </div>
    <div id="ind_prog_chevback" style="display:none;" class="pull-right">
        <a href="javascript:void(0)" onclick="togglePanels('#ind_prog', '#ind_prog_options');"><i class="fa fa-minus-square-o"></i></a>
    </div>
</legend>
<div id="ind_prog_options" class="btn-group" style="display:none; width: 100%">
    <div class="well" id="c">
        <div class="content">
            <div class="row-fluid">
                <div class="span3">
                    <div class="control-group">
                        <label class="control-label">APACHE II</label>
                        <div class="controls">
                            <input class="form-control" type="text" id="apache2">
                        </div>
                    </div>
                </div>
                <div class="span3">
                    <div class="control-group">
                        <label class="control-label">Estimativa do risco de morte (%)</label>
                        <div class="controls">
                            <input class="form-control" type="text" id="risk_of_death_apache">
                        </div>
                    </div>
                </div>
                <div class="span3">
                    <div class="control-group">
                        <label class="control-label">SAPS III</label>
                        <div class="controls">
                            <input class="form-control" type="text" id="saps3">
                        </div>
                    </div>
                </div>
                <div class="span3">
                    <div class="control-group">
                        <label class="control-label">Estimativa do risco de morte (%)</label>
                        <div class="controls">
                            <input class="form-control" type="text" id="risk_of_death_saps"> 
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
