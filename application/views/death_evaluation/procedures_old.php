<legend>
    Procedimentos realizados
</legend>
<?php if($procedures): ?>
<div id="procedures_options" class="btn-group" style="width: 100%">
    <div class="well">
        <div class="row-fluid">
            <div class="col-sm-3">
                <div class="row-fluid">
                    <div class="col-sm-1 pull-left">
                        <a href="javascript:void(0);" onclick="jQuery('#acontent').toggle();" class="btn btn-default">A</a>
                    </div>
                </div>
                <div class="row-fluid">
                    <div id="acontent" class="col-sm-12" >
                        </br>
                        <?php if ($procedures['oxign_dispo'] === 1): ?>
                            <label>
                                Dispositivo de oxigênio
                            </label>
                            <div id="oxign_dispo_c" style="margin-left: 15px ;"> 
                                <h6><label class="control-label">Tipo de dispositivo</label></h6>
                                <?= lang($procedures['o2_disp_type']) ?>
                            </div>
                        <?php endif; ?>

                        <?php if ($procedures['breaf_vias'] === 1): ?>
                            <label>Via aérea</label>
                        <?php endif; ?>

                        <div id="aer_via_c" style="margin-left: 15px "> 

                            <?php if ($procedures['maintenance_tec'] === 1): ?>
                                <label>Técnicas de manutenção</label>
                            <?php endif; ?>


                            <div id="manut_tec_c" style="margin-left: 40px;">
                                <?php if ($procedures['mt_oro'] === 1): ?>
                                    <label>Orofaríngea</label>
                                <?php endif; ?>
                                <?php if ($procedures['mt_naso'] === 1): ?>
                                    <label>Nasofaríngea</label>
                                <?php endif; ?>
                                <?php if ($procedures['mt_lar_mask'] === 1): ?>
                                    <label>Máscara laríngea</label>
                                <?php endif; ?>
                                <?php if ($procedures['mt_comb'] === 1): ?>
                                    <label>Combitube</label>
                                <?php endif; ?>

                            </div>

                            <?php if ($procedures['brief_def_via'] === 1): ?>
                                <label>Uso de Via aérea definitiva</label>
                            <?php endif; ?>


                            <div id="ae_via_def_c" style="margin-left: 40px;">
                                <?php if ($procedures['bf_fst_int_seq'] === 1): ?>
                                    <label>Sequência rápida de intubação</label>
                                <?php endif; ?>
                                <?php if ($procedures['bf_oro'] === 1): ?>
                                    <label>Orotraqueal</label>
                                <?php endif; ?>
                                <?php if ($procedures['bf_naso'] === 1): ?>
                                    <label>Nasotraqueal</label>
                                <?php endif; ?>
                                <?php if ($procedures['bf_cir'] === 1): ?>
                                    <label>Cirúrgico</label>
                                <?php endif; ?>

                                <div id="defin-aer-vie-cirurg" style="margin-left: 40px;">
                                    <?php if ($procedures['bf_cir_cripun'] === 1): ?>
                                        <label>Cricotireoidostomia por punção</label>
                                    <?php endif; ?>
                                    <?php if ($procedures['bf_cir_cricir'] === 1): ?>
                                        <label>Cricotireoidostomia cirúrgica</label>
                                    <?php endif; ?>
                                    <?php if ($procedures['bf_cir_critraq'] === 1): ?>
                                        <label>Traqueostomia</label>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <?php if ($procedures['cervical_col'] === 1): ?>
                                <label>Colar cervical</label>
                            <?php endif; ?>

                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-3">
                <div class="row-fluid">
                    <div class="col-sm-1 pull-left">
                        <a href="javascript:void(0);" onclick="jQuery('#bcontent').toggle();" class="btn btn-default">B</a>
                    </div>
                </div>
                <div class="row-fluid">
                    <div id="bcontent" class="col-sm-12" >
                        </br>
                        <?php if ($procedures['mec_vent'] === 1): ?>
                            <label>Ventilação</label>
                            <div id="mec_vent_c" style="margin-left: 15px"> 
                                <h6><label class="control-label">Tipo de ventilação</label></h6>
                                <?= lang($procedures['mec_vent_type']) ?>
                            </div>
                        <?php endif; ?>

                        <?php if ($procedures['pun_pleu'] === 1): ?>
                            <label>Punção pleural</label>
                        <?php endif; ?>

                        <div id="pler-punc" style="margin-left: 15px ;">
                            <?php if ($procedures['pun_pleu_l'] === 1): ?>
                                <label>Esquerda</label>
                            <?php endif; ?>
                            <?php if ($procedures['pun_pleu_r'] === 1): ?>
                                <label>Direita</label>
                            <?php endif; ?>

                        </div>

                        <?php if ($procedures['pleu_dren'] === 1): ?>
                            <label>Uso de dreno pleural</label>
                        <?php endif; ?>

                        <div id="pler-dren" style="margin-left: 15px ;">
                            <?php if ($procedures['pleu_dren_l'] === 1): ?>
                                <label>Esquerda</label>
                            <?php endif; ?>

                            <?php if ($procedures['pleu_dren_l'] === 1): ?>
                                <div id="pler-dren-left" > 
                                    <h6><label class="control-label">Quantidade (ml)</label></h6>
                                    <?= $procedures['pleu_dren_lv']; ?>
                                </div>
                            <?php endif; ?>
                            <?php if ($procedures['pleu_dren_r'] === 1): ?>
                                <label>Direita</label>
                            <?php endif; ?>

                            <?php if ($procedures['pleu_dren_r'] === 1): ?>
                                <div id="pler-dren-rig" > 
                                    <h6><label class="control-label">Quantidade (ml)</label></h6>
                                    <?= $procedures['pleu_dren_rv']; ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-3">
                <div class="row-fluid">
                    <div class="col-sm-1 pull-left">
                        <a href="javascript:void(0);" onclick="jQuery('#ccontent').toggle();" class="btn btn-default">C</a>
                    </div>
                </div>
                <div class="row-fluid">
                    <div id="ccontent" class="col-sm-12" >
                        </br>
                        <?php if ($procedures['extern_hem'] === 1): ?>
                            <label>Presença de hemorragia externa</label>
                        <?php endif; ?>

                        <div id="extern_hem_c" style="margin-left: 15px ;">
                            <?php if ($procedures['extern_hem_head_face'] === 1): ?>
                                <label> Cabeça e face</label>
                            <?php endif; ?>
                            <?php if ($procedures['extern_hem_neck'] === 1): ?>
                                <label> Pescoço</label>
                            <?php endif; ?>
                            <?php if ($procedures['extern_hem_chest'] === 1): ?>
                                <label> Tórax</label>
                            <?php endif; ?>
                            <?php if ($procedures['extern_hem_ab'] === 1): ?>
                                <label> Abdome</label>
                            <?php endif; ?>
                            <?php if ($procedures['extern_hem_pelvper'] === 1): ?>
                                <label> Pelve/Períneo</label>
                            <?php endif; ?>
                            <?php if ($procedures['extern_hem_mi'] === 1): ?>
                                <label> Membros inferiores</label>
                            <?php endif; ?>
                            <?php if ($procedures['extern_hem_ms'] === 1): ?>
                                <label> Membros superiores</label>
                            <?php endif; ?>

                        </div>

                        <?php if ($procedures['vasc_access_per'] === 1): ?>
                            <label> Acesso vascular periférico</label>
                            <div id="vasc_access_per_c" style="margin-left: 15px ;">
                                <h6><label class="control-label">Tipo</label></h6>
                                <?= lang($procedures['vasc_access_per_disptype']) ?>

                                <h6><label class="control-label">Região</label></h6>
                                <?= lang($procedures['vasc_access_per_reg']) ?>
                            </div>
                        <?php endif; ?>

                        <?php if ($procedures['volemic_infus'] === 1): ?>
                            <label> Infusão volêmica</label>
                            <div id="volemic_infus_c" style="margin-left: 15px;">
                                <?= lang($procedures['volemic_infus_type']) ?>
                                <h6><label class="control-label">Quantidade (ml)</label></h6>
                                <?= $procedures['volemic_infus_qt']; ?>
                            </div>
                        <?php endif; ?>

                        <?php if ($procedures['ressus_cardio'] === 1): ?>
                            <label> Ressuscitação cardiopulmonar</label>

                            <div id="cardiopulm-ressuc" style="margin-left: 15px ;">
                                <h6><label class="control-label">Número de paradas cardíacas</label></h6>
                                <?= $procedures['ressus_cardio_v']; ?>

                                <h5><label class="control-label"><?= lang('duration') ?></label></h5>
                                <?= $procedures['ressus_cardio_d']; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <?php if ($attendance_type === 1): ?>
                <div class="col-sm-3">
                    <div class="row-fluid">
                        <div class="col-sm-1 pull-left">
                            <a href="javascript:void(0);" onclick="jQuery('#dcontent').toggle();" class="btn btn-default">D</a>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div id="dcontent" class="col-sm-12" >
                            </br>
                            <?php if ($procedures['board_imo'] === 1): ?>
                                <label> Imobilização em prancha longa</label>
                            <?php endif; ?>
                            <?php if ($procedures['memb_imob'] === 1): ?>
                                <label> Imobilização dos membros</label>
                            <?php endif; ?>

                            <div id="mem_imob_c" style="margin-left: 15px ;">
                                <?php if ($procedures['memb_imob_sd'] === 1): ?>
                                    <label> Membro superior direito</label>
                                <?php endif; ?>
                                <?php if ($procedures['memb_imob_se'] === 1): ?>
                                    <label> Membro superior esquerdo</label>
                                <?php endif; ?>
                                <?php if ($procedures['memb_imob_id'] === 1): ?>
                                    <label> Membro inferior direito</label>
                                <?php endif; ?>
                                <?php if ($procedures['memb_imob_ie'] === 1): ?>
                                    <label> Membro inferior esquerdo</label>
                                <?php endif; ?>
                                <?php if ($procedures['memb_imob_pelv'] === 1): ?>
                                    <label> Pelve</label>
                                <?php endif; ?>

                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>   
    </div>
</div>
<?php else:?>
<h4>Nenhum procedimento prévio</h4>
<?php endif; ?>
</br>