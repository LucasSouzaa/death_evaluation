<legend>
    <a href="javascript:void(0)" onclick="togglePanels('#acoes_sugeridas', '#acoes_sugeridas_options');">Ações corretivas sugeridas</a>
    <div id="acoes_sugeridas_chev" class="pull-right">
        <a href="javascript:void(0)" onclick="togglePanels('#acoes_sugeridas', '#acoes_sugeridas_options');"><i class="fa fa-plus-square-o"></i></a>
    </div>
    <div id="acoes_sugeridas_chevback" style="display:none;" class="pull-right">
        <a href="javascript:void(0)" onclick="togglePanels('#acoes_sugeridas', '#acoes_sugeridas_options');"><i class="fa fa-minus-square-o"></i></a>
    </div>
</legend>
<div id="acoes_sugeridas_options" class="btn-group" style="display:none; width: 100%">
    <div class="well" id="e">
        <div class="content">
            <div class="row-fluid content">
                <div class="span6">
                    <h4>Educação continuada:</h4>
                    <div class="control-group">
                        <label class="control-label">Temas teóricos</label>
                        <div class="controls">
                            <input class="form-control" type="text" id="theoretical_issues">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Temas práticos</label>
                        <div class="controls">
                            <input class="form-control" type="text" id="practical_issues">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Para todos os funcionários do setor</label>
                        <div class="controls">
                            <input class="form-control" type="text" id="employees_sector">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Para funcionários específicos</label>
                        <div class="controls">
                            <input class="form-control" type="text" id="specific_employees">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Outros</label>
                        <div class="controls">
                            <input class="form-control" type="text" id="other_continuing_education">
                        </div>
                    </div>
                </div>
                <div class="span6">
                    <h4>Melhoria no sistema:</h4>
                    <div class="control-group">
                        <label class="control-label">Equipamentos</label>
                        <div class="controls">
                            <input class="form-control" type="text" id="improvement_equipaments">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Desenvolvimento de práticas</label>
                        <div class="controls">
                            <input class="form-control" type="text" id="development_practical">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Desenvolvimento de protocolos</label>
                        <div class="controls">
                            <input class="form-control" type="text" id="developing_protocols">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Comunicação entre setores</label>
                        <div class="controls">
                            <input class="form-control" type="text" id="communication_sectors">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Outros</label>
                        <div class="controls">
                            <input class="form-control" type="text" id="other_improvement">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row-fluid content">
                <div class="span6">

                    <h4>Treinamento de tópicos específicos:</h4>
                    <div class="control-group">
                        <label class="control-label">Trauma por sistema</label>
                        <div class="controls">
                            <input class="form-control" type="text" id="trauma_system">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Procedimento técnico</label>
                        <div class="controls">
                            <input class="form-control" type="text" id="technical_procedure">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Transporte e acompanhamento do paciente</label>
                        <div class="controls">
                            <input class="form-control" type="text" id="transport_followup">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Outros</label>
                        <div class="controls">
                            <input class="form-control" type="text" id="other_training">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Revisor</label>
                        <div class="controls">
                            <input class="form-control" type="text" id="reviser">
                        </div>
                    </div>
                </div>
                <div class="span6">

                </div>
            </div>
        </div>
    </div>
</div>