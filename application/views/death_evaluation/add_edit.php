<div class="search-filter">
    <legend><i class="fa fa-ambulance"> Atendimento Pré Hospitalar</i></legend>        
</div>
<!-- VIEW MECANISMO 
    -- trauma_mechanism_pre ok
-->
   <?php $this->load->view('death_evaluation/mechanism'); ?>
<!-- Fim MECANISMO -->

<!-- VIEW sinais vitais 
    -- vital_signs (passar context prehospitalar)
-->
   <?php $this->load->view('death_evaluation/vital_signs'); ?>
<!-- Fim MECANISMO -->


<!-- VIEW REGULACAO 
    -- regulation
-->
   <?php $this->load->view('death_evaluation/regulation'); ?>
<!-- Fim REGULACAO -->

<!-- VIEW procedimentos
    -- trauma_procedures + context prehospitalar
 -->
   <?php $this->load->view('death_evaluation/procedures_aph'); ?>
<!-- Fim PROCEDIMENTOS -->
</div></div></div>
<!--- ----------OK pra cima-----------  -->


<div class="search-filter">
    <legend><i class="fa fa-building-o"> Admissao Hospitalar</i></legend>        
</div>

<!-- VIEW sinais vitais na admissao
    
 -->
<?php $this->load->view('death_evaluation/vital_signs_admission'); ?>
<!-- FIM SINAIS VITAIS ADMISSAo -->

<!-- VIEW Procedimentos Admissao
    
 -->
<?php $this->load->view('death_evaluation/procedures_admission'); ?>
<!-- FIM Procedimentos ADMISSAo -->

<!-- VIEW Exames de imagens
    
 -->
<div id="image_exams_evolution"></div>
<script>
jQuery('#image_exams_evolution').load(jQuery("body").data("baseurl") + "death_evaluation/image_exams/view");
</script>
<!-- FIM Exames de imagem -->

<!-- VIEW Exames laboratoriais
    
 -->
<div id="biochemistry_exams_evolution"></div>
<script>
jQuery('#biochemistry_exams_evolution').load(jQuery("body").data("baseurl") + "death_evaluation/hospital_biochemistry_exams/hospital/view");</script>
<!-- FIM Exames laboratoriais -->
</div>
</div>
<!-- FIM ADMISSAO -->

<div id="surgery_anesthesia_evolution"></div>
<script>
jQuery('#surgery_anesthesia_evolution').load(jQuery("body").data("baseurl") + "death_evaluation/surgery_anesthesia/view");
</script>

<!-- VIEW Lesoes encontradas -->
<div id="injury_ais"></div>
<script>
jQuery('#injury_ais').load(jQuery("body").data("baseurl") + "attendance_hospital/get_ais_diagnosys_list");
</script>
<!-- FIM VIEW Lesoes encontradas -->

<!-- VIEW CTI -->
<?php $this->load->view('death_evaluation/cti'); ?>
<div id="visits_resume"></div>
<script>
jQuery('#visits_resume').load(jQuery("body").data("baseurl") + "hospitalization/visits_resume");
</script>
<div id="cti_biochemistry_exams_evolution"></div>
<script>
jQuery('#cti_biochemistry_exams_evolution').load(jQuery("body").data("baseurl") + "death_evaluation/hospital_biochemistry_exams/hospitalization/view");
</script>

<div id="procedures_evolution"></div>
<script>
jQuery('#procedures_evolution').load(jQuery("body").data("baseurl") + "death_evaluation/procedures/view");
</script>

    <div class="search-filter">
        <legend>Tempos</legend>        
    </div>
    <div class="row-fluid"><br>  
        <div class="col-md-3">
            <h5><label class="control-label">Tempo entre admissao e internação CTI</label></h5>
            <input id="pa" type="text" class="form-control"> 
        </div>
        <div class="col-md-3">
            <h5><label class="control-label">Tempo de alta e transferencia</label></h5>
            <input id="pa" type="text" class="form-control"> 
        </div>
        <div class="col-md-3">
            <h5><label class="control-label">Tempo total de internação no CTI:</label></h5>
            <input id="pa" type="text" class="form-control"> 
        </div>
    </div>
</div>

<!-- FIM VIEW CTI -->

<!-- HEMOTRANSFUSAO -->
<div id="use_of_blood_evolution"></div>
<script>
jQuery('#use_of_blood_evolution').load(jQuery("body").data("baseurl") + "hospitalization/use_of_blood/view");
</script>
<!-- FIM HEMOTRANSFUSAO -->

<!-- Antecedentes -->
<div class="search-filter">
    <legend>Antecedentes</legend>        
</div>
<div id="antecedentes" >
    <div class="panel-body">
        <div class="well">
            <?php $this->load->view('death_evaluation/antecedentes/neoplasias'); ?>
            <?php $this->load->view('death_evaluation/antecedentes/alergias'); ?>
            <?php $this->load->view('death_evaluation/antecedentes/cirurgias'); ?>
            <?php $this->load->view('death_evaluation/antecedentes/medicamentos'); ?>
            <?php $this->load->view('death_evaluation/antecedentes/doencas'); ?>
        </div>
    </div>
</div>
<!-- FIM Antecedentes -->


<?php $this->load->view('death_evaluation/death'); ?>
<div class="pill-content">
    <div class="pill-pane active">
        <div class="container-fluid">
        <!-- VIEW CAMPOS IML -->
        <?php $this->load->view('death_evaluation/iml'); ?>
        <!-- Fim IML -->

        <!-- Indices de trauma  -->
        <?php $this->load->view('death_evaluation/indices_trauma'); ?>
        <!-- Fim Indices -->


        <!--  Indices prognósticos -->
        <!-- <?php $this->load->view('death_evaluation/indices_prog'); ?>     -->
        <!-- FIM indices prog -->
        
        <!-- Resultado da avaliação -->
        <?php $this->load->view('death_evaluation/evaluation_result'); ?>
        <!-- Fim Indices -->

        <!-- Ações corretivas sugeridas -->
        <?php $this->load->view('death_evaluation/acoes_corretivas'); ?>
        <!-- Fim Indices -->
            
            <div class="row-fluid">
                <a class="btn btn-success pull-right" href="javascript:void(0)" onclick="save_death_evaluation()"> Salvar</a>
            </div>
        </div>
    </div>
</div>


<script>
    jQuery('li').removeClass('active');
    jQuery('#li_death_evaluation').addClass('active');
</script>

