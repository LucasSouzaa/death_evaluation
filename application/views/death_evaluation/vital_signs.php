<!-- SINAIS VITAIS -->
<div class="search-filter">
    <legend>Sinais Vitais</legend>        
</div>
	
                <div class="row-fluid">
                    <div class="col-sm-2">
                        <h5><label class="control-label"><?= lang('') ?> Pressão arterial</label></h5>

                        <input id="pa" type="text" class="form-control" value="<?php
                        if (isset($vital_signs['pa'])): echo $vital_signs['pa'];
                        endif;
                        ?>"/> 
                    </div>

                    <div class="col-sm-2">
                        <h5><label class="control-label"><?= lang('') ?> Frequencia cardíaca</label></h5>

                        <input id="fc" type="text" class="form-control" value="<?php
                        if (isset($vital_signs['fc'])): echo $vital_signs['fc'];
                        endif;
                        ?>"/> 
                    </div>

                    <div class="col-sm-2">
                        <h5><label class="control-label"><?= lang('') ?> Frequencia respiratória</label></h5>

                        <input id="fr" type="text" class="form-control" value="<?php
                        if (isset($vital_signs['fr'])): echo $vital_signs['fr'];
                        endif;
                        ?>"/> 
                    </div>

                    <div class="col-sm-2">
                        <h5><label class="control-label"><?= lang('') ?> Temperatura</label></h5>

                        <input id="axillary_temperature" type="text" class="form-control" value="<?php
                        if (isset($vital_signs['axillary_temperature'])): echo $vital_signs['axillary_temperature'];
                        endif;
                        ?>"/> 
                    </div>

                    <div class="col-sm-2">
                        <h5><label class="control-label"><?= lang('') ?> Glicosimetria</label></h5>

                        <input id="glicosimetria" type="text" class="form-control" value="<?php
                        if (isset($vital_signs['glicosimetria'])): echo $vital_signs['glicosimetria'];
                        endif;
                        ?>"/> 
                    </div>
                </div>

                <div class="row-fluid">
                    <div class="col-sm-3">
                        <h5><label class="control-label"><?= lang('ocular_response') ?></label></h5>
                        <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="glasgow_visual_response">
                            <option value="0" selected> - </option>
                            <option value="4" <?php
                            if (isset($vital_signs['glasgow_visual_response']) && $vital_signs['glasgow_visual_response'] == '4'): echo 'selected';
                            endif;
                            ?>>
                                4 - <?= lang('he_opens_eyes_spontaneously') ?>
                            </option>
                            <option value="3" <?php
                            if (isset($vital_signs['glasgow_visual_response']) && $vital_signs['glasgow_visual_response'] == '3'): echo 'selected';
                            endif;
                            ?>>
                                3 - <?= lang('open_your_eyes_response_call') ?>
                            </option>
                            <option value="2" <?php
                            if (isset($vital_signs['glasgow_visual_response']) && $vital_signs['glasgow_visual_response'] == '2'): echo 'selected';
                            endif;
                            ?>>
                                2 - <?= lang('open_your_eyes_response_pain_stimulus') ?>
                            </option>
                            <option value="1" <?php
                            if (isset($vital_signs['glasgow_visual_response']) && $vital_signs['glasgow_visual_response'] == '1'): echo 'selected';
                            endif;
                            ?>>
                                1 - <?= lang('do_not_open_your_eyes') ?>
                            </option>
                        </select>
                    </div>

                    <div class="col-sm-3">
                        <h5><label class="control-label"><?= lang('verbal_response') ?></label></h5>
                        <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="glasgow_verbal_response">
                            <option value="0" selected> - </option>
                            <option value="5" <?php
                            if (isset($vital_signs['glasgow_verbal_response']) && $vital_signs['glasgow_verbal_response'] == '5'): echo 'selected';
                            endif;
                            ?>>
                                5 - <?= lang('oriented_talk_normally') ?>
                            </option>
                            <option value="4" <?php
                            if (isset($vital_signs['glasgow_verbal_response']) && $vital_signs['glasgow_verbal_response'] == '4'): echo 'selected';
                            endif;
                            ?>>
                                4 - <?= lang('confused_disoriented') ?>
                            </option>
                            <option value="3" <?php
                            if (isset($vital_signs['glasgow_verbal_response']) && $vital_signs['glasgow_verbal_response'] == '3'): echo 'selected';
                            endif;
                            ?>>
                                3 - <?= lang('pronounces_disconnected_words') ?>
                            </option>
                            <option value="2" <?php
                            if (isset($vital_signs['glasgow_verbal_response']) && $vital_signs['glasgow_verbal_response'] == '2'): echo 'selected';
                            endif;
                            ?>>
                                2 - <?= lang('emits_incomprehensible_sounds') ?>
                            </option>
                            <option value="1" <?php
                            if (isset($vital_signs['glasgow_verbal_response']) && $vital_signs['glasgow_verbal_response'] == '1'): echo 'selected';
                            endif;
                            ?>>
                                1 - <?= lang('muted') ?>
                            </option>
                        </select>
                    </div>

                    <div class="col-sm-4">
                        <h5><label class="control-label"><?= lang('motor_response') ?></label></h5>
                        <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="glasgow_motor_response">
                            <option value="0" selected> - </option>
                            <option value="6" <?php
                            if (isset($vital_signs['glasgow_motor_response']) && $vital_signs['glasgow_motor_response'] == '6'): echo 'selected';
                            endif;
                            ?>>
                                6 - <?= lang('obeys_commands') ?>
                            </option>
                            <option value="5" <?php
                            if (isset($vital_signs['glasgow_motor_response']) && $vital_signs['glasgow_motor_response'] == '5'): echo 'selected';
                            endif;
                            ?>>
                                5 - <?= lang('locate_painful_stimuli') ?>
                            </option>
                            <option value="4" <?php
                            if (isset($vital_signs['glasgow_motor_response']) && $vital_signs['glasgow_motor_response'] == '4'): echo 'selected';
                            endif;
                            ?>>
                                4 - <?= lang('nonspecific_flexion_reflex_withdrawal_painful_stimuli') ?>
                            </option>
                            <option value="3" <?php
                            if (isset($vital_signs['glasgow_motor_response']) && $vital_signs['glasgow_motor_response'] == '3'): echo 'selected';
                            endif;
                            ?>>
                                3 - <?= lang('abnormal_flexion_painful_stimuli') ?>
                            </option>
                            <option value="2" <?php
                            if (isset($vital_signs['glasgow_motor_response']) && $vital_signs['glasgow_motor_response'] == '2'): echo 'selected';
                            endif;
                            ?>>
                                2 - <?= lang('extension_painful_stimuli') ?>
                            </option>
                            <option value="1" <?php
                            if (isset($vital_signs['glasgow_motor_response']) && $vital_signs['glasgow_motor_response'] == '1'): echo 'selected';
                            endif;
                            ?>>
                                1 - <?= lang('it_not_moves') ?>
                            </option>
                        </select>
                    </div>
                    <div class="col-sm-2">
                        <h5><label class="control-label"> Escala de Glasgow</label></h5>
                        <input id="glasgow_score" type="number" class="form-control" value="0"
                         disabled=""/> 
                    </div>
                </div>

                <div class="row-fluid">

                    <div id="pupil_reflex" style="display:none; margin-left: 15px;">
                        <div class="row-fluid">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" id="pup_reflex_isocoria"
                                    <?php
                                    if (isset($vital_signs['isocoria']) && $vital_signs['isocoria'] == 1): echo 'checked';
                                    endif;
                                    ?>> <?= lang('') ?> Isocorico
                                </label>
                                <label>
                                    <input type="checkbox" id="pup_reflex_anisiocoria" name="pup_reflex_opt" class="show-div" data-target="pup_reflex_anisio" <?php
                                    if (isset($vital_signs['anisiocoria']) && $vital_signs['anisiocoria'] == 1): echo 'checked';
                                    endif;
                                    ?>> <?= lang('') ?>Ansiocorico
                                </label>
                            </div>
                        </div>
                        <div id="pup_reflex_anisio" class="row-fluid" style="display:none; margin-left: 25px;">
                            <h5><label class="control-label">Lado maior</label></h5>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" id="right_anisocoria" 
                                    <?php
                                    if (isset($vital_signs['right_anisiocoria']) && $vital_signs['right_anisiocoria'] == 1): echo 'checked';
                                    endif;
                                    ?>> <?= lang('') ?> Direita
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" id="left_anisocoria" 
                                    <?php
                                    if (isset($vital_signs['left_anisiocoria']) && $vital_signs['left_anisiocoria'] == 1): echo 'checked';
                                    endif;
                                    ?>> <?= lang('') ?> Esquerda
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                
				<div class="row-fluid" style="margin-top:10px;">
					<div id="vital_signs_btn" class="btn-group pull-right">
						<a href="javascript:;" class="btn btn-success btn-sm" onclick="save_prehospital_mobile_vital_signs();"><?= lang('save') ?></a>
					</div>
				</div>
			</div>


<script>
    //sinais vitais
    jQuery('#glasgow_visual_response').change(function () {
        var a = parseInt(jQuery('#glasgow_visual_response').val());
        var b = parseInt(jQuery('#glasgow_verbal_response').val());
        var c = parseInt(jQuery('#glasgow_motor_response').val());
        jQuery('#glasgow_score').val(a + b + c);
    });


    jQuery('#glasgow_verbal_response').change(function () {
        var a = parseInt(jQuery('#glasgow_visual_response').val());
        var b = parseInt(jQuery('#glasgow_verbal_response').val());
        var c = parseInt(jQuery('#glasgow_motor_response').val());
        jQuery('#glasgow_score').val(a + b + c);
    });

    jQuery('#glasgow_motor_response').change(function () {
        var a = parseInt(jQuery('#glasgow_visual_response').val());
        var b = parseInt(jQuery('#glasgow_verbal_response').val());
        var c = parseInt(jQuery('#glasgow_motor_response').val());
        jQuery('#glasgow_score').val(a + b + c);
    }); 

    $(document).ready(function(){
        var a = parseInt(jQuery('#glasgow_visual_response').val());
        var b = parseInt(jQuery('#glasgow_verbal_response').val());
        var c = parseInt(jQuery('#glasgow_motor_response').val());
        jQuery('#glasgow_score').val(a + b + c);
    })
</script>