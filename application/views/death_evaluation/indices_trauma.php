<legend>
    <a href="javascript:void(0)" onclick="togglePanels('#ind_trauma', '#ind_trauma_options');">Indices de trauma </a>
    <div id="ind_trauma_chev" class="pull-right">
        <a href="javascript:void(0)" onclick="togglePanels('#ind_trauma', '#ind_trauma_options');"><i class="fa fa-plus-square-o"></i></a>
    </div>
    <div id="ind_trauma_chevback" style="display:none;" class="pull-right">
        <a href="javascript:void(0)" onclick="togglePanels('#ind_trauma', '#ind_trauma_options');"><i class="fa fa-minus-square-o"></i></a>
    </div>
</legend>
<div id="ind_trauma_options" class="btn-group" style="display:none; width: 100%">
    <div class="well" id="b">
        <div class="content">
            <div class="row-fluid">
                <div class="span2">
                    <div class="control-group">
                        <label class="control-label">RTS</label>
                        <div class="controls">
                            <input disabled="" class="form-control" type="text" id="rts">
                        </div>
                    </div>
                </div>
                <div class="span2">
                    <div class="control-group">
                        <label class="control-label">ISS</label>
                        <div class="controls">
                            <input disabled="" class="form-control" type="text" id="iss">
                        </div>
                    </div>
                </div>
                <div class="span2">
                    <div class="control-group">
                        <label class="control-label">NISS</label>
                        <div class="controls">
                            <input disabled="" class="form-control" type="text" id="niss">
                        </div>
                    </div>
                </div>
                <div class="span2">
                    <div class="control-group">
                        <label class="control-label">TRISS (%)</label>
                        <div class="controls">
                            <input disabled="" class="form-control" type="text" id="triss">
                        </div>
                    </div>
                </div>
                <div class="span2">
                    <div class="control-group">
                        <label class="control-label">NTRISS (%)</label>
                        <div class="controls">
                            <input disabled="" class="form-control" type="text" id="ntriss">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
