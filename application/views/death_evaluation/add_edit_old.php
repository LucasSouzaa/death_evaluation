<div id="visits_resume"></div>
<div id="use_service_beds"></div>
<div id="visits_evolution"></div>
<div id="biochemistry_exams_evolution"></div>
<div id="image_exams_evolution"></div>
<div id="microbiology_exams_evolution"></div>
<div id="anatomical_pathological_exams_evolution"></div>
<div id="get_ec_diagnosys_list_evolution"></div>
<div id="get_i_diagnosys_list_evolution"></div>
<div id="drugs_evolution"></div>
<div id="surgery_anesthesia_evolution"></div>
<div id="procedures_evolution"></div>
<div id="use_of_blood_evolution"></div>
<div class="search-filter">
    <legend>Gerenciamento médico</legend>
</div>
<div id="medical_management_evolution"></div>
<div id="prognostic_indicators_evolution"></div>
<div id="imagery_evolution"></div>
<div id="outpatient_condition_evolution"></div>

<!-- Se houve internação -->
<?php if ($hospitalized == 1): ?>
    <script>
        // jQuery('#visits_resume').load(jQuery("body").data("baseurl") + "hospitalization/visits_resume");
        // jQuery('#use_service_beds').load(jQuery("body").data("baseurl") + "hospitalization/use_service_beds");
        // jQuery('#visits_evolution').load(jQuery("body").data("baseurl") + "hospitalization/visits/view");
        // jQuery('#image_exams_evolution').load(jQuery("body").data("baseurl") + "death_evaluation/image_exams/view");
        // jQuery('#anatomical_pathological_exams_evolution').load(jQuery("body").data("baseurl") + "death_evaluation/anatomical_pathological_exams/view");
        // jQuery('#use_of_blood_evolution').load(jQuery("body").data("baseurl") + "hospitalization/use_of_blood/view");
        // //jQuery('#medical_management_evolution').load(jQuery("body").data("baseurl") + "hospitalization/medical_management/view");
        // //jQuery('#prognostic_indicators_evolution').load(jQuery("body").data("baseurl") + "hospitalization/prognostic_indicators/view");
        // jQuery('#imagery_evolution').load(jQuery("body").data("baseurl") + "hospitalization/imagery/view");
        // jQuery('#outpatient_condition_evolution').load(jQuery("body").data("baseurl") + "hospitalization/outpatient_condition");
    </script>
<?php else: ?>
    <script>
        jQuery('.search-filter').hide()
    </script>
<?php endif; ?>

<script>
    // jQuery('#biochemistry_exams_evolution').load(jQuery("body").data("baseurl") + "hospitalization/all_biochemistry_exams/view");
    // jQuery('#get_ec_diagnosys_list_evolution').load(jQuery("body").data("baseurl") + "attendance_hospital/get_ec_diagnosys_list/view");
    // jQuery('#get_i_diagnosys_list_evolution').load(jQuery("body").data("baseurl") + "attendance_hospital/get_i_diagnosys_list/view");
    // jQuery('#drugs_evolution').load(jQuery("body").data("baseurl") + "death_evaluation/drugs/view");
    // jQuery('#surgery_anesthesia_evolution').load(jQuery("body").data("baseurl") + "death_evaluation/surgery_anesthesia/view");
    // jQuery('#procedures_evolution').load(jQuery("body").data("baseurl") + "death_evaluation/procedures/view");
</script>

<div class="pill-content">
    <div class="pill-pane active">
        <div class="container-fluid">
        <!-- VIEW CAMPOS IML -->
        <?php $this->load->view('death_evaluation/iml'); ?>
        <!-- Fim IML -->

        <!-- Indices de trauma  -->
        <?php $this->load->view('death_evaluation/indices_trauma'); ?>
        <!-- Fim Indices -->


        <!--  Indices prognósticos -->
        <!-- <?php $this->load->view('death_evaluation/indices_prog'); ?>     -->
        <!-- FIM indices prog -->
        
        <!-- Resultado da avaliação -->
        <?php $this->load->view('death_evaluation/evaluation_result'); ?>
        <!-- Fim Indices -->

        <!-- Ações corretivas sugeridas -->
        <?php $this->load->view('death_evaluation/acoes_corretivas'); ?>
        <!-- Fim Indices -->
            
            <div class="row-fluid">
                <a class="btn btn-success pull-right" href="javascript:void(0)" onclick="save_death_evaluation()"> Salvar</a>
            </div>
        </div>
    </div>
</div>

<input id="indicators_head" hidden="" value="<?= $trisscalc['head'] ?>"/>
<input id="indicators_face" hidden="" value="<?= $trisscalc['face'] ?>"/>
<input id="indicators_ches" hidden="" value="<?= $trisscalc['ches'] ?>"/>
<input id="indicators_abdo" hidden="" value="<?= $trisscalc['abdo'] ?>"/>
<input id="indicators_limb" hidden="" value="<?= $trisscalc['limb'] ?>"/>
<input id="indicators_skin" hidden="" value="<?= $trisscalc['skin'] ?>"/>
<input id="indicators_resprate" hidden="" value="<?= $trisscalc['resprate'] ?>"/>
<input id="indicators_sysbp" hidden="" value="<?= $trisscalc['sysbp'] ?>"/>
<input id="indicators_coma" hidden="" value="<?= $trisscalc['coma'] ?>"/>
<input id="indicators_age" hidden="" value="<?= $trisscalc['age'] ?>"/>
<input id="indicators_head" hidden="" value="<?= $trisscalc['head'] ?>"/>

<script>

    jQuery('#found_gravity').change(function () {
        if (jQuery('#found_gravity').is(':checked')) {
            jQuery('.found_gravity').show();
        } else {
            jQuery('.found_gravity').hide();
        }
    });

    jQuery('#bac').change(function () {
        if (jQuery('#bac').is(':checked')) {
            jQuery('.bac').show();
        } else {
            jQuery('.bac').hide();
        }
    });

    jQuery('#necroscopy').change(function () {
        if (jQuery('#necroscopy').is(':checked')) {
            jQuery('.necroscopy').show();
        } else {
            jQuery('.necroscopy').hide();
        }
    });

    jQuery('#glasgow_visual_response').change(function () {
        var a = parseInt(jQuery('#glasgow_visual_response').val());
        var b = parseInt(jQuery('#glasgow_verbal_response').val());
        var c = parseInt(jQuery('#glasgow_motor_response').val());
        jQuery('#glasgow_score').val(a + b + c);
    });

    jQuery('#glasgow_verbal_response').change(function () {
        var a = parseInt(jQuery('#glasgow_visual_response').val());
        var b = parseInt(jQuery('#glasgow_verbal_response').val());
        var c = parseInt(jQuery('#glasgow_motor_response').val());
        jQuery('#glasgow_score').val(a + b + c);
    });

    jQuery('#glasgow_motor_response').change(function () {
        var a = parseInt(jQuery('#glasgow_visual_response').val());
        var b = parseInt(jQuery('#glasgow_verbal_response').val());
        var c = parseInt(jQuery('#glasgow_motor_response').val());
        jQuery('#glasgow_score').val(a + b + c);
    });



    function togglePanels(content, options) {
        jQuery(content).toggle();
        jQuery(content + '_chev').toggle();
        jQuery(content + '_chevback').toggle();
        jQuery(options).toggle();
    }

</script>

<script>

    function save_death_evaluation() {

        jQuery.ajax({
            url: jQuery("body").data("baseurl") + "death_evaluation/save_death_evaluation",
            type: "post",
            dataType: 'json',
            data: {
                autopsy: jQuery('#autopsy').is(':checked'),
                conclusive_iml: jQuery('#conclusive_iml').is(':checked'),
                found: jQuery('#found').val(),
                found_gravity: jQuery('#found_gravity').is(':checked'),
                found_changed_gravity: jQuery('#found_changed_gravity').val(),
                bac: jQuery('#bac').is(':checked'),
                dosage: jQuery('#dosage').val(),
                normal_value: jQuery('#normal_value').val(),
                rts: jQuery('#rts').val(),
                iss: jQuery('#iss').val(),
                niss: jQuery('#niss').val(),
                triss: jQuery('#triss').val(),
                ntriss: jQuery('#ntriss').val(),
                apache2: jQuery('#apache2').val(),
                risk_of_death_apache: jQuery('#risk_of_death_apache').val(),
                saps3: jQuery('#saps3').val(),
                risk_of_death_saps: jQuery('#risk_of_death_saps').val(),
                resume: jQuery('#resume').val(),
                death_decision: jQuery('input[name=death_decision]:checked').val(),
                case_problems: jQuery('#case_problems').val(),
                conclusive_death_evaluation: jQuery('#conclusive_death_evaluation').is(':checked'),
                conclusive_death_evaluation_reason: jQuery('#conclusive_death_evaluation_reason').val(),
                deficiencies_none: jQuery('#deficiencies_none').is(':checked'),
                approach_airways: jQuery('#approach_airways').is(':checked'),
                bleeding_control: jQuery('#bleeding_control').is(':checked'),
                volemic_reposition: jQuery('#volemic_reposition').is(':checked'),
                trauma_approach_system: jQuery('#trauma_approach_system').is(':checked'),
                cti_approach: jQuery('#cti_approach').is(':checked'),
                delay_treatment: jQuery('#delay_treatment').is(':checked'),
                infection_related_problems: jQuery('#infection_related_problems').is(':checked'),
                deficiencies_documentation: jQuery('#deficiencies_documentation').is(':checked'),
                other_deficiencies: jQuery('#other_deficiencies').is(':checked'),
                delay_decision: jQuery('#delay_decision').is(':checked'),
                technical_difficulties: jQuery('#technical_difficulties').is(':checked'),
                equipments: jQuery('#equipments').is(':checked'),
                diagnosis_st: jQuery('#diagnosis_st').is(':checked'),
                insufficient_surgical_technique: jQuery('#insufficient_surgical_technique').is(':checked'),
                excess_crystalloid: jQuery('#excess_crystalloid').is(':checked'),
                severe_acidosis: jQuery('#severe_acidosis').is(':checked'),
                delay_blood_transfusion: jQuery('#delay_blood_transfusion').is(':checked'),
                skull: jQuery('#skull').is(':checked'),
                column: jQuery('#column').is(':checked'),
                torax: jQuery('#torax').is(':checked'),
                abdome: jQuery('#abdome').is(':checked'),
                pelve: jQuery('#pelve').is(':checked'),
                members: jQuery('#members').is(':checked'),
                monitoring: jQuery('#monitoring').is(':checked'),
                mechanical_ventilation_cti: jQuery('#mechanical_ventilation_cti').is(':checked'),
                shock_reversal: jQuery('#shock_reversal').is(':checked'),
                mechanical_ventilation: jQuery('input[name=mechanical_ventilation]:checked').val(),
                drug_use: jQuery('#drug_use').is(':checked'),
                surgical_decision: jQuery('#surgical_decision').is(':checked'),
                cti_hospitalization: jQuery('#cti_hospitalization').is(':checked'),
                delay_diagnosis_infectious_focus: jQuery('#delay_diagnosis_infectious_focus').is(':checked'),
                delay_surgical_management_infectious_source: jQuery('#delay_surgical_management_infectious_source').is(':checked'),
                delayed_antibiotic_therapy: jQuery('#delayed_antibiotic_therapy').is(':checked'),
                Incorrect_empirical_antibiotic_therapy: jQuery('#Incorrect_empirical_antibiotic_therapy').is(':checked'),
                lack_microbiological_examination: jQuery('#lack_microbiological_examination').is(':checked'),
                vital_signs: jQuery('#vital_signs').is(':checked'),
                exams: jQuery('#exams').is(':checked'),
                conducts: jQuery('#conducts').is(':checked'),
                procedures: jQuery('#procedures').is(':checked'),
                other_documentations: jQuery('#other_documentations').val(),
                pre_hospital: jQuery('#pre_hospital').is(':checked'),
                trauma_room: jQuery('#trauma_room').is(':checked'),
                operating_room: jQuery('#operating_room').is(':checked'),
                recovery: jQuery('#recovery').is(':checked'),
                cti: jQuery('#cti').is(':checked'),
                semi_intensive: jQuery('#semi_intensive').is(':checked'),
                ward: jQuery('#ward').is(':checked'),
                inadequacy_system: jQuery('#inadequacy_system').is(':checked'),
                theoretical_issues: jQuery('#theoretical_issues').val(),
                practical_issues: jQuery('#practical_issues').val(),
                employees_sector: jQuery('#employees_sector').val(),
                specific_employees: jQuery('#specific_employees').val(),
                other_continuing_education: jQuery('#other_continuing_education').val(),
                improvement_equipaments: jQuery('#improvement_equipaments').val(),
                development_practical: jQuery('#development_practical').val(),
                developing_protocols: jQuery('#developing_protocols').val(),
                communication_sectors: jQuery('#communication_sectors').val(),
                other_improvement: jQuery('#other_improvement').val(),
                trauma_system: jQuery('#trauma_system').val(),
                technical_procedure: jQuery('#technical_procedure').val(),
                transport_followup: jQuery('#transport_followup').val(),
                other_training: jQuery('#other_training').val(),
                reviser: jQuery('#reviser').val()
            },
            success: function (response) {
                if (response.status == 'NOK') {
                    var notice = new PNotify({
                        title: 'Erro',
                        text: 'Tente novamente mais tarde',
                        type: 'error',
                        addclass: 'click-2-close',
                        hide: false,
                        buttons: {
                            closer: false,
                            sticker: false
                        }
                    });
                    notice.get().click(function () {
                        notice.remove();
                    });
                } else {
                    // recarrega avaliacao de obito
                    jQuery('#hospital_attendance_steps').load(jQuery("body").data("baseurl") + "attendance_hospital/primary_evaluation/view");
                }
            }
        });
    }

    function calcule_ind_trauma_options() {

//        // PRIMEIRO INSTANCIAMOS AS VARIAVEIS
//        var glasgow = 0;
//        var sist_pressure = 0;
//        var resp_freq = 0;
//        var age = 0;
//        var ais_fci = [1, 2];
//
//        // CALCULANDO RTS
//        if (glasgow <= 3) {
//            var ecg = 0;
//        } else if (glasgow > 3 && glasgow <= 5) {
//            var ecg = 1;
//        } else if (glasgow > 5 && glasgow <= 8) {
//            var ecg = 2;
//        } else if (glasgow > 8 && glasgow <= 12) {
//            var ecg = 3;
//        } else if (glasgow > 12 && glasgow <= 15) {
//            var ecg = 4;
//        }
//
//        if (sist_pressure = 0) {
//            var pas = 0;
//        } else if (sist_pressure > 0 && sist_pressure <= 49) {
//            var pas = 1;
//        } else if (sist_pressure > 49 && sist_pressure <= 75) {
//            var pas = 2;
//        } else if (sist_pressure > 75 && sist_pressure <= 89) {
//            var pas = 3;
//        } else if (sist_pressure > 89) {
//            var pas = 4;
//        }
//
//        if (resp_freq = 0) {
//            var fr = 0;
//        } else if (resp_freq > 0 && resp_freq <= 5) {
//            var fr = 1;
//        } else if (resp_freq > 5 && resp_freq <= 9) {
//            var fr = 2;
//        } else if (resp_freq > 9 && resp_freq <= 29) {
//            var fr = 4;
//        } else if (resp_freq > 29) {
//            var fr = 3;
//        }
//
//        var rts = parseFloat('0,9368') * ecg + parseFloat('0,7326') * pas + parseFloat('0,2908') * fr;
//
//        if (rts == 0) {
//            jQuery('.ind_trauma_options #rts').val('0,027');
//        } else if (rts == 1) {
//            jQuery('.ind_trauma_options #rts').val('0,071');
//        } else if (rts == 2) {
//            jQuery('.ind_trauma_options #rts').val('0,172');
//        } else if (rts == 3) {
//            jQuery('.ind_trauma_options #rts').val('0,361');
//        } else if (rts == 4) {
//            jQuery('.ind_trauma_options #rts').val('0,605');
//        } else if (rts == 5) {
//            jQuery('.ind_trauma_options #rts').val('0,807');
//        } else if (rts == 6) {
//            jQuery('.ind_trauma_options #rts').val('0,919');
//        } else if (rts == 7) {
//            jQuery('.ind_trauma_options #rts').val('0,969');
//        } else if (rts == 8) {
//            jQuery('.ind_trauma_options #rts').val('0,988');
//        }
//
//        var iss = 0;
//        jQuery.each(fci, function (index, value) {
//            iss = iss + (value * value);
//        });
//        jQuery('.ind_trauma_options #iss').val(iss);
//
//
//        var j = j1 + j2 * rts + j3 * iss + j4 * age;
//        var triss = 1 / (1 + Math.exp(-j));
//        jQuery('.ind_trauma_options #triss').val(triss);

    }

</script>

<script language="javascript">
    function ValidateInput(checkVal) {
        len = checkVal.length;
        for (i = 0; i < len; i++) {
            if (!((checkVal.substring(i, i + 1) == "0") ||
                    (checkVal.substring(i, i + 1) == "1") ||
                    (checkVal.substring(i, i + 1) == "2") ||
                    (checkVal.substring(i, i + 1) == "3") ||
                    (checkVal.substring(i, i + 1) == "4") ||
                    (checkVal.substring(i, i + 1) == "5") ||
                    (checkVal.substring(i, i + 1) == "6") ||
                    (checkVal.substring(i, i + 1) == "7") ||
                    (checkVal.substring(i, i + 1) == "8") ||
                    (checkVal.substring(i, i + 1) == "9"))) {
                errorMsg = "You have entered an invalid number. Please enter a new amount and try again.";
                return true;
            }
        }
        if (!((checkVal >= 0) & (checkVal < 300))) {
            errorMsg = "Numbers way out of range";
            return true;
        } else {
            return false;
        }
    }

    function isGreater(a, b) {
        if (Math.abs(a) < Math.abs(b))
            return -1;
        else
        if (Math.abs(b) == Math.abs(a))
            return 0;
        return 1;
    }

    function doISSStuff(head, face, ches, abdo, limb, skin) {

        var badnum = 0;
        var iss = 0;
        var issA = 0;
        var issB = 0;
        var issC = 0;

        ais = new Array(6);
        sortais = new Array(6);
        ais[0] = head;
        ais[1] = face;
        ais[2] = ches;
        ais[3] = abdo;
        ais[4] = limb;
        ais[5] = skin;

        for (j = 0; j < 6; j++) {
            if (ValidateInput(ais[j])) {
                window.alert(errorMsg);
                badnum = -1;
            }
            if ((ais[j] < 0) || (ais[j] > 6)) {
                alert('AIS out of range (0 - 6)');
                badnum = -1;
            }
        }

        aissort = ais.sort(isGreater);


        issA = aissort[3] * aissort[3];
        issB = aissort[4] * aissort[4];
        issC = aissort[5] * aissort[5];

        if (badnum == 0) {
            if (issC == 36)
                iss = 75;
            else
                iss = issA + issB + issC;
        } else
            iss = 0;

        //document.trisscalc.issscore.value = iss;
        jQuery('#iss').val(iss);
    }

    function doRTSStuff(resprate, sysbp, coma) {

        var rts = 0;
        var rr = resprate;
        var sbp = sysbp;
        var gcs = coma;
        var rri = 0;
        var sbpi = 0;
        var gcsi = 0;

        if (ValidateInput(rr)) {
            window.alert(errorMsg);
            distance = 0;
        }

        if (ValidateInput(sbp)) {
            window.alert(errorMsg);
            distance = 0;
        }

        if (ValidateInput(gcs)) {
            window.alert(errorMsg);
            distance = 0;
        }

        if (gcs == 3)
            gcsi = 0;
        else if (gcs >= 4 && gcs <= 5)
            gcsi = 1;
        else if (gcs >= 6 && gcs <= 8)
            gcsi = 2;
        else if (gcs >= 9 && gcs <= 12)
            gcsi = 3;
        else if (gcs >= 13 && gcs <= 15)
            gcsi = 4;
        else
            alert('Glasgow Coma Score has values 3-15');

        if (rr >= 10 && rr <= 29)
            rri = 4;
        else if (rr >= 30 && rr <= 80)
            rri = 3;
        else if (rr >= 6 && rr <= 9)
            rri = 2;
        else if (rr >= 1 && rr <= 5)
            rri = 1;
        else if (rr == 0)
            rri = 0;
        else
            alert('Respiratory rate invalid');

        if (sbp >= 90 && sbp <= 300)
            sbpi = 4;
        else if (sbp >= 76 && sbp <= 89)
            sbpi = 3;
        else if (sbp >= 50 && sbp <= 75)
            sbpi = 2;
        else if (sbp >= 1 && sbp <= 49)
            sbpi = 1;
        else if (sbp == 0)
            sbpi = 0;
        else
            alert('Systolic Blood Pressure Invalid');

        rts = sbpi * 0.7326 + rri * 0.2908 + gcsi * 0.9368;

        document.trisscalc.rtsscore.value = (Math.round(1000 * rts) / 1000);
        jQuery('#rts').val((Math.round(1000 * rts) / 1000));

    }

    function doTRISSStuff(age, rts, iss) {

        var trissblunt = 0;
        var trisssharp = 0;

        var trissblunthold = 0;
        var trisssharphold = 0;

        var agei = 0;

        var sharpb0 = -2.5355
        var sharpb1 = 0.9934
        var sharpb2 = -0.0651
        var sharpb3 = -1.1360

        var bluntb0 = -0.4499
        var bluntb1 = 0.8085
        var bluntb2 = -0.0835
        var bluntb3 = -1.7430

        if (ValidateInput(age)) {
            window.alert(errorMsg);
            distance = 0;
        }

        if (age >= 0 && age <= 54)
            agei = 0;
        else
        if (age >= 55 && age <= 120)
            agei = 1;
        else
            alert("Enter Patient's Age");

        if (age < 15) {
            trissblunthold = 0 - (bluntb0 + bluntb1 * rts + bluntb2 * iss + bluntb3 * agei);
            trisssharphold = 0 - (bluntb0 + bluntb1 * rts + bluntb2 * iss + bluntb3 * agei);
        } else {
            trissblunthold = 0 - (bluntb0 + bluntb1 * rts + bluntb2 * iss + bluntb3 * agei);
            trisssharphold = 0 - (sharpb0 + sharpb1 * rts + sharpb2 * iss + sharpb3 * agei);
        }

        trissblunt = 1 / (1 + Math.exp(trissblunthold));
        trisssharp = 1 / (1 + Math.exp(trisssharphold));

        if (iss == "") {
            alert('No ISS score');
            trissblunt = 0;
            trisssharp = 0;
        }
        if (rts == "") {
            alert('No RTS score');
            trissblunt = 0;
            trisssharp = 0;
        }
        if (age == "") {
            alert("Enter Patient's Age");
            trissblunt = 0;
            trisssharp = 0;
        }

        document.trisscalc.trissblunt.value = (Math.round(1000 * trissblunt) / 10) + "%";
        document.trisscalc.trisssharp.value = (Math.round(1000 * trisssharp) / 10) + "%";
    }

    var head = jQuery('#indicators_head').val();
    var face = jQuery('#indicators_face').val();
    var ches = jQuery('#indicators_ches').val();
    var abdo = jQuery('#indicators_abdo').val();
    var limb = jQuery('#indicators_limb').val();
    var skin = jQuery('#indicators_skin').val();
    var resprate = jQuery('#indicators_resprate').val();
    var sysbp = jQuery('#indicators_sysbp').val();
    var coma = jQuery('#indicators_coma').val();
    var age = jQuery('#indicators_age').val();

    doISSStuff(head, face, ches, abdo, limb, skin);
    doRTSStuff(resprate, sysbp, coma);

    var rts = jQuery('#rts').val();
    var iss = jQuery('#iss').val();

    doTRISSStuff(age, rts, iss);

</script>


<script>
    jQuery('li').removeClass('active');
    jQuery('#li_death_evaluation').addClass('active');
</script>