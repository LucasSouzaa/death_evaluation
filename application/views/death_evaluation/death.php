<div class="search-filter">
    <legend>Óbito</legend>        
</div>
<div id="death" >
    <div class="panel-body">
        <div class="well">
		    <div class="row-fluid"><br>  
		        <div class="col-md-3">
		            <h5><label class="control-label">Local</label></h5>
		            <input id="death_local" type="text" value="<?= $local_obito; ?>" class="form-control"> 
		        </div>
		        <div class="col-md-3">
		            <h5><label class="control-label">Data/hora</label></h5>
		            <input id="death_date"  value="<?= $data_obito; ?>" class="form-control"> 
		        </div>
		    </div>

		    <div class="row-fluid"><br>  
		        <div class="col-md-3">
		            <h5><label class="control-label">Tempo total de internação</label></h5>
		            <input id="death_time_total" value="<?= $total_dias; ?> dias" type="text" class="form-control"> 
		        </div>
		        <div class="col-md-3">
		            <h5><label class="control-label">Classificação</label></h5>

		            <input id="death_classification" type="text" value="<?= $classification; ?>" class="form-control"> 
		        </div>
		    </div>

		    <div class="row-fluid"><br>  
		        <div class="col-md-3">
		            <h5><label class="control-label">Principal causa de morte</label></h5>
		            <label> <input type="checkbox" id="choque"> Choque</label><br/>
		            <label><input type="checkbox" id="tce">	 TCE</label><br/>
		           	<label><input type="checkbox" id="sepse"> Sepse</label><br/>
		           	<label><input type="checkbox" id="cardiaca"> Cardiaca</label><br/>
		           	<label><input type="checkbox" id="ira"> Ira</label><br/>
		           	<label><input type="checkbox" id="sdmos"> SDMOS</label><br/>
		           	<label><input type="checkbox" id="outra"> Outra</label><br/>
		        </div>
		        <div class="col-md-3">
		            <h5><label class="control-label">Segunda causa de morte</label></h5>
		            <label><input type="checkbox" id="choque"> Choque</label><br/>
		            <label><input type="checkbox" id="tce"> TCE</label><br/>
		           	<label><input type="checkbox" id="sepse"> Sepse</label><br/>
		           	<label><input type="checkbox" id="cardiaca"> Cardiaca</label><br/>
		           	<label><input type="checkbox" id="ira"> Ira</label><br/>
		           	<label><input type="checkbox" id="sdmos"> SDMOS</label><br/>
		           	<label><input type="checkbox" id="outra"> Outra</label><br/>
		        </div>
		    </div>

		    <div class="row-fluid"><br>  
		        
		    </div>
        </div>
    </div>
</div>