<!-- REGULACAO -->
<div class="search-filter">
    <legend>Regulação</legend>        
</div>
            <?php if(isset($regulation)){
                ?>
                <script>
                $(document).ready(function(){
                    $(".aphmobile_regulation_type_detail").css('display', '')
                    $(".reg_units").css('display', '')
                    $(".aph_regulation_transport_type_other").css('display', '')
                })
                </script>
            <?php } ?>
            <div class="row-fluid">
                <div class="col-sm-3">
                    <h5><label class="control-label">Tipo de regulação</label></h5>
                    <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="aphmobile_regulation_type">
                        <option value="not" selected> Não regulado </option>
                        <option value="primary" <?php if (isset($regulation) && $regulation['type'] == 'primary'): echo 'selected'; endif; ?>>Primária (Atendimento em cena)</option>
                        <option value="secondary" <?php if (isset($regulation) && $regulation['type'] == 'secondary'): echo 'selected'; endif; ?>>Secundária (Transporte interserviço de saúde)</option>
                    </select>
                </div>
                <div class="aphmobile_regulation_type_detail" style="display:none;">
                    <div class="col-sm-2">
                        <h5><label class="control-label">Nível de regulação</label></h5>
                        <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="aphmobile_regulation_level">
                            <option value="municipal" <?php if (isset($regulation) && $regulation['level'] == 'municipal'): echo 'selected'; endif; ?>>Regulação municipal</option>
                            <option value="state" <?php if (isset($regulation) && $regulation['level'] == 'state'): echo 'selected'; endif; ?>>Regulação estadual</option>
                        </select>
                    </div>
                    <div class="col-sm-2">
                        <h5><label class="control-label">Data / Hora</label></h5>
                        <div class='input-group date input-append datetimepicker1'>
                            <input type='text' class="form-control" data-format="yyyy-MM-dd hh:mm:ss" id="aphmobile_regulation_datetime" />
                            <span class="input-group-addon add-on">
                                <span><i class="fa fa-calendar" data-time-icon="icon-time" data-date-icon="icon-calendar"></i></span>
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <h5><label class="control-label">Local da ocorrência</label></h5>
                        <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="aphmobile_regulation_oc_local">
                            <option value="none" selected> - </option>
                            <option value="urban" <?php if (isset($regulation) && $regulation['oc_local'] == 'urban'): echo 'selected'; endif; ?>>Urbano</option>
                            <option value="rural" <?php if (isset($regulation) && $regulation['oc_local'] == 'rural'): echo 'selected'; endif; ?>>Rural</option>
                            <option value="highway" <?php if (isset($regulation) && $regulation['oc_local'] == 'highway'): echo 'selected'; endif; ?>>Rodovia</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row-fluid" style="margin-top:10px;">
                <div class="col-sm-3 reg_units" style="display:none;">
                    <h5><label class="control-label">Unidade de origem</label></h5>
                    <input id="aphmobile_regulation_unit_origin" type="text" class="form-control" value=" <?php if (isset($regulation)): echo $regulation['unit_origin']; endif; ?>" /> 
                </div>
                <div class="col-sm-3 reg_units" style="display:none;">
                    <h5><label class="control-label">Unidade de destino</label></h5>
                    <input id="aphmobile_regulation_unit_destiny" type="text" class="form-control" value=" <?php if (isset($regulation)): echo $regulation['unit_destiny']; endif; ?>" /> 
                </div>

                <div class="col-sm-3 aphmobile_regulation_type_detail" style="display:none;">
                    <h5><label class="control-label">Tipo de transporte</label></h5>
                    <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="aph_regulation_transport_type">
                        <option value="none"> - </option>
                        <option value="basic_samu" <?php if (isset($regulation) && $regulation['transport_type'] == 'basic_samu'): echo 'selected'; endif;?>> USA (Unidade de suporte avançado)</option>
                        <option value="advanced_samu" <?php if (isset($regulation) && $regulation['transport_type'] == 'advanced_samu'): echo 'selected'; endif;?>> UBS (Unidade básica de saúde)</option>
                        <option value="basic_rescue" <?php if (isset($regulation) && $regulation['transport_type'] == 'basic_rescue'): echo 'selected'; endif;?>> Bombeiros</option>
                        <option value="advanced_rescue" <?php if (isset($regulation) && $regulation['transport_type'] == 'advanced_rescue'): echo 'selected'; endif;?>> Policiais</option>
                        <option value="dealership" <?php if (isset($regulation) && $regulation['transport_type'] == 'dealership'): echo 'selected'; endif;?>> Leigos</option>
                        <option value="other" <?php if (isset($regulation) && $regulation['transport_type'] == 'other'): echo 'selected'; endif;?>> Outros</option>
                    </select>
                </div>
                <div id="aph_regulation_transport_type_other" class="col-sm-3" style="display:none;">
                    <h5><label class="control-label"><?= lang('transport_specify') ?></label></h5>
                    <input id="aph_regulation_transport_type_other_v" type="text" class="form-control" value="<?php
                    if (isset($regulation)): echo $regulation['transport_type_other'];
                    endif;
                    ?>"/>
                </div>
            </div>
            <br/>
            <div class="search-filter">
                <legend>Tempos</legend>        
            </div>
            <div class="row-fluid"><br>  
                <div class="col-md-3">
                    <h5><label class="control-label">Tempo entre trauma e APH</label></h5>
                    <input id="ttaph" type="text" class="form-control"> 
                </div>
                <div class="col-md-3">
                    <h5><label class="control-label">Tempo de atendimento em cena</label></h5>
                    <input id="tac" type="text" class="form-control"> 
                </div>
                <div class="col-md-3">
                    <h5><label class="control-label">Tempo de transporte até o hospital</label></h5>
                    <input id="tth" type="text" class="form-control"> 
                </div>
                <div class="col-md-3">
                    <h5><label class="control-label">Tempo entre trauma e admissão</label></h5>
                    <input id="tta" type="text" class="form-control"> 
                </div>
            </div>
            <br/>
            <div class="row-fluid" style="margin-top:10px;">
                <div id="regulation_btn" class="btn-group pull-right">
                    <a href="javascript:;" class="btn btn-success btn-sm" onclick="save_prehospital_mobile_regulation();"><?= lang('save') ?></a>
                </div>
        

<script>
    //regulação
    jQuery('#aphmobile_regulation_type').change(function () {
        jQuery('.aphmobile_regulation_type_detail').hide();
        jQuery('.reg_units').hide();
        jQuery('#aphmobile_regulation_level').val('municipal');
        jQuery('#aph_regulation_transport_type_other').hide();
        jQuery('#aph_regulation_transport_type').val('none');
        jQuery('#aph_regulation_transport_type_other').val('');
        jQuery('#aphmobile_regulation_datetime').val('');
        jQuery('#aphmobile_regulation_oc_local').val('none');
        jQuery('#aphmobile_regulation_unit_origin').val('');
        jQuery('#aphmobile_regulation_unit_destiny').val('');
        if (jQuery('#aphmobile_regulation_type').val() === 'primary') {
            jQuery('#aphmobile_regulation_level').val('municipal');
            jQuery('#aphmobile_regulation_level').prop('disabled', true);
            jQuery('.aphmobile_regulation_type_detail').show();
        } else if (jQuery('#aphmobile_regulation_type').val() === 'secondary') {
            jQuery('#aphmobile_regulation_level').val('municipal');
            jQuery('#aphmobile_regulation_level').prop('disabled', false);
            jQuery('.aphmobile_regulation_type_detail').show();
            jQuery('.reg_units').show();
        }
    });

    jQuery('#aph_regulation_transport_type').change(function () {
        jQuery('#aph_regulation_transport_type_other').val('');
        if (jQuery('#aph_regulation_transport_type').val() === 'other') {
            jQuery('#aph_regulation_transport_type_other').show();
        } 
    }); 
</script>