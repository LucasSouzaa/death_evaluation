<!-- IML -->
<legend> 
    <a href="javascript:void(0)" onclick="togglePanels('#iml', '#iml_options');">IML</a>
    <div id="iml_chev" class="pull-right">
        <a href="javascript:void(0)" onclick="togglePanels('#iml', '#iml_options');"><i class="fa fa-plus-square-o"></i></a>
    </div>
    <div id="iml_chevback" style="display:none;" class="pull-right">
        <a href="javascript:void(0)" onclick="togglePanels('#iml', '#iml_options');"><i class="fa fa-minus-square-o"></i></a>
    </div>
</legend>
<div id="iml_options" class="btn-group" style="display:none; width: 100%">
    <div class="well" id="a">
        <div class="row-fluid content">
            <div class="span6">
                <div class="control-group">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" id="autopsy" > Fez autopsia
                        </label>
                    </div>
                </div>	
                <div class="control-group">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" id="conclusive_iml" > Foi conclusiva
                        </label>
                    </div>
                </div>
                <div class="control-group">
                    <div class="checkbox"><!--EDITADOAQUI -->
                        <label>
                            <input type="checkbox" id="necroscopy" > Foi realizado exame necroscópico?
                        </label>
                    </div>
                    <div class="control-group necroscopy" hidden="" style="margin-left: 15px;">
                        <label class="control-label">Data</label>
                        <div class="controls">
                            <input class="form-control" type="date" id="date_necroscopy">
                        </div>
                    </div>
                    <div class="control-group necroscopy" hidden="" style="margin-left: 15px;">
                        <label class="control-label">hora</label>
                        <div class="controls">
                            <input class="form-control" type="text" id="tie_necroscopy">
                        </div>
                    </div>
                </div>
            </div>	
            <div class="span6">
                <div class="control-group">
                    <label class="control-label">Achado</label>
                    <div class="controls">
                        <input class="form-control" type="text" id="found">
                    </div>
                </div>
                <div class="control-group">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" id="found_gravity" > O achado da autopsia alterou a gravidade do caso
                        </label>
                    </div>
                </div>
                <div class="control-group found_gravity" hidden="" style="margin-left: 15px;">
                    <label class="control-label">Achado alterou gravidade do caso</label>
                    <div class="controls">
                        <input class="form-control" type="text" id="found_changed_gravity">
                    </div>
                </div>

                <div class="control-group">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" id="bac" > Alcoolemia
                        </label>
                    </div>
                </div>
                <div class="control-group bac" hidden="" style="margin-left: 15px;">
                    <label class="control-label">Dosagem</label>
                    <div class="controls">
                        <input class="form-control" type="text" id="dosage">
                    </div>
                </div>
                <div class="control-group bac" hidden="" style="margin-left: 15px;">
                    <label class="control-label">Valor normal</label>
                    <div class="controls">
                        <input class="form-control" type="text" id="normal_value">
                    </div>
                </div>
            </div>
        </div>
        <!--EDITADOAQUI \/-->
            <div class="row-fluid">
                <h4>Lesões encontradas</h4>
            </div>
            <div class="row-fluid">
                <div class="col-sm-2">
                    <h5><label class="control-label">Região</label></h5>
                    <select class="form-control" id="regiao">
                        <option value="-">-</option>
                        <option value="cabeca">Cabeça/Pescoço</option>
                        <option value="face">Face</option>
                        <option value="torax">Tórax</option>
                        <option value="abdome">Abdome</option>
                        <option value="extremidade">Extremidades/Pelve</option>
                        <option value="geral">Geral/Externo</option>
                    </select>
                </div>
                <div class="col-sm-3">
                    <h5><label class="control-label">Descrição</label></h5>
                    <input class="form-control" type="text" id="descricao" >
                </div>
                <div class="col-sm-1">
                    <h5><label class="control-label">Valor</label></h5>
                    <input class="form-control" type="number" id="valor" >
                </div>
                <div class="col-sm-3">
                    <h5><label class="control-label">&nbsp;&nbsp;</label></h5>
                    <a onclick="add_ais()" class="btn btn-primary btn-block"><i class="fa fa-plus"></i> Adicionar</a>
                </div>
            </div>
            <div class="row-fluid">
                <div id="ais_iml_list">

                </div>
            </div>


            <div class="row-fluid">
                <h4>Causas de morte</h4>
            </div>
            <div class="row-fluid">
                <div class="col-sm-3">
                    <h5><label class="control-label">Causa da morte</label></h5>
                    <input class="form-control" type="text" id="causa" >
                </div>
                <div class="col-sm-3">
                    <h5><label class="control-label"> &nbsp;&nbsp;</label></h5>
                    <a onclick="add_death_iml()" class="btn btn-primary btn-block"><i class="fa fa-plus"></i> Adicionar</a>
                </div>
            </div>
            <div class="row-fluid">
                <div id="death_iml_list">

                </div>
            </div>
        
    </div>
</div>

<script>
    // adicionando cristaloide no banco e atualizando lista
    function add_ais() {

        if (jQuery('#regiao').val() == '-') {
            var notice = new PNotify({
                title: 'Erro',
                text: 'É preciso selecionar uma região lesionada',
                type: 'error',
                addclass: 'click-2-close',
                hide: false,
                buttons: {
                    closer: false,
                    sticker: false
                }
            });
            notice.get().click(function () {
                notice.remove();
            });
            return false;
        }

        if (!jQuery('#descricao').val()) {
            var notice = new PNotify({
                title: 'Erro',
                text: 'É preciso digitar a descricao da lesao',
                type: 'error',
                addclass: 'click-2-close',
                hide: false,
                buttons: {
                    closer: false,
                    sticker: false
                }
            });
            notice.get().click(function () {
                notice.remove();
            });
            return false;
        }

        if (!jQuery('#valor').val()) {
            var notice = new PNotify({
                title: 'Erro',
                text: 'É preciso digitar o valor do AIS',
                type: 'error',
                addclass: 'click-2-close',
                hide: false,
                buttons: {
                    closer: false,
                    sticker: false
                }
            });
            notice.get().click(function () {
                notice.remove();
            });
            return false;
        }else if(jQuery('#valor').val() < 0 && jQuery('#valor').val() > 6){
            var notice = new PNotify({
                title: 'Erro',
                text: 'O valor do AIS fica entre 0 e 6',
                type: 'error',
                addclass: 'click-2-close',
                hide: false,
                buttons: {
                    closer: false,
                    sticker: false
                }
            });
            notice.get().click(function () {
                notice.remove();
            });
            return false;
        }

        jQuery.ajax({
            url: jQuery("body").data("baseurl") + "death_evaluation/add_iml_ais",
            type: "post",
            dataType: 'json',
            data: {
                regiao: jQuery('#regiao').val(),
                descricao: jQuery('#descricao').val(),
                valor: jQuery('#valor').val(),
            },
            success: function (response) {
                jQuery('#modal_close_button').click();
                if (response.status == 'NOK') {
                    var notice = new PNotify({
                        title: 'Erro',
                        text: 'Tente novamente mais tarde',
                        type: 'error',
                        addclass: 'click-2-close',
                        hide: false,
                        buttons: {
                            closer: false,
                            sticker: false
                        }
                    });
                    notice.get().click(function () {
                        notice.remove();
                    });
                } else {
                    // recarrega lista de cristaloides
                    jQuery('#ais_iml_list').load(jQuery("body").data("baseurl") + "death_evaluation/get_iml_list/ais_iml");
                }
            }
        });
    }

    jQuery('#ais_iml_list').load(jQuery("body").data("baseurl") + "death_evaluation/get_iml_list/ais_iml");
</script>

<script>
    // adicionando cristaloide no banco e atualizando lista
    function add_death_iml() {

        if (!jQuery('#causa').val()) {
            var notice = new PNotify({
                title: 'Erro',
                text: 'É preciso descrever a causa da morte',
                type: 'error',
                addclass: 'click-2-close',
                hide: false,
                buttons: {
                    closer: false,
                    sticker: false
                }
            });
            notice.get().click(function () {
                notice.remove();
            });
            return false;
        }

        jQuery.ajax({
            url: jQuery("body").data("baseurl") + "death_evaluation/add_death_iml",
            type: "post",
            dataType: 'json',
            data: {
                causa: jQuery('#causa').val(),
            },
            success: function (response) {
                jQuery('#modal_close_button').click();
                if (response.status == 'NOK') {
                    var notice = new PNotify({
                        title: 'Erro',
                        text: 'Tente novamente mais tarde',
                        type: 'error',
                        addclass: 'click-2-close',
                        hide: false,
                        buttons: {
                            closer: false,
                            sticker: false
                        }
                    });
                    notice.get().click(function () {
                        notice.remove();
                    });
                } else {
                    // recarrega lista de cristaloides
                    jQuery('#death_iml_list').load(jQuery("body").data("baseurl") + "death_evaluation/get_death_iml/death_iml");
                }
            }
        });
    }

    jQuery('#death_iml_list').load(jQuery("body").data("baseurl") + "death_evaluation/get_death_iml/death_iml");
</script>