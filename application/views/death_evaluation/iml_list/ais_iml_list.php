<?php if (!empty($iml_list)): ?>
    <div class="search-filter">
        <legend><?= lang($iml_type) ?></legend>
    </div>

    <div class="well">
        <table class="table table-bordered table-striped mb-none" >
            <thead>
                <tr>
                    <th>Região</th>
                    <th>Descrição</th>
                    <th>Valor</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($iml_list as $vrl): ?>
                    <tr>
                        <th><?= $vrl['regiao'] ?></th>
                        <th><?= $vrl['descricao'] ?></th>
                        <th><?= $vrl['valor'] ?></th>
                        <th><a href="javascript:void(0)" onclick="delete_ais_iml('<?= $iml_type ?>', '<?= $this->encrypt->encode($vrl['id_ais_iml']) ?>')" class="on-default remove-row" style=" margin-right: 5px; margin-left: 5px;"><i class="fa fa-trash-o"></i></a></th>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<?php else: ?>
    <h4>Nenhuma dado adicionado por enquanto</h4>
<?php endif; ?>

<script>

    function delete_ais_iml(iml_type, id) {
        jQuery.ajax({
            url: jQuery("body").data("baseurl") + "death_evaluation/delete_ais_iml",
            type: "post",
            dataType: 'json',
            data: {
                id_ais_iml: id
            },
            success: function (response) {
                jQuery('#modal_close_button').click();
                if (response.status === 'NOK') {
                    var notice = new PNotify({
                        title: 'Erro',
                        text: 'Tente novamente mais tarde',
                        type: 'error',
                        addclass: 'click-2-close',
                        hide: false,
                        buttons: {
                            closer: false,
                            sticker: false
                        }
                    });
                    notice.get().click(function () {
                        notice.remove();
                    });
                } else {
                    // recarrega lista
                    jQuery('#' + iml_type + '_list').load(jQuery("body").data("baseurl") + "death_evaluation/get_iml_list/" + iml_type);
                }
            }
        });
    }
</script>