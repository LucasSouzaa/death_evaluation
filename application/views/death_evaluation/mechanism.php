<!-- MECANISMOS (apenas trauma) -->

<div id="mechanisms" >
	<div class="panel-body">
		<div class="well">
            <div class="search-filter">
                <legend>Mecanismo</legend>        
            </div>
			<div class="row-fluid">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" id="penetrating_trauma" name="group[]" class="show-div" data-target="penetrating_trauma_c"
                        <?php if (isset($mechanisms) && $mechanisms['penetrating_trauma'] == 1): echo 'checked'; endif; ?>>
                        Trauma penetrante
                    </label>
                </div>
                

                <div class="checkbox">
                    <label>
                        <input type="checkbox" id="blunt_trauma" name="group[]" class="show-div" data-target="blunt_trauma_c"
                        <?php if (isset($mechanisms) && $mechanisms['blunt_trauma'] == 1): echo 'checked'; endif; ?>>
                        Trauma contuso
                    </label>
                </div>
                 
                <!-- QUEIMADURA -->
                <div class="checkbox">
                    <label>
                        <input type="checkbox" id="burn_trauma" name="group[]" class="show-div" data-target="burn_trauma_c"
                        <?php if (isset($mechanisms) && $mechanisms['burn_trauma'] == 1): echo 'checked'; endif; ?>>
                        Queimaduras
                    </label>
                </div>
                
                <div class="checkbox">
                    <label>
                        <input type="checkbox" id="other_infos" name="group[]" class="show-div" data-target="other_infos_c" 
                        <?php if (isset($mechanisms) && $mechanisms['other_infos'] == 1): echo 'checked'; endif; ?>>
                        Outras informações sobre o trauma
                    </label>
                </div>
                <div id="other_infos_c" <?php if (isset($mechanisms) && $mechanisms['other_infos'] == 0): echo 'style="display:none;"'; endif; ?>>
                    <div class="row-fluid">
                        <div class="col-sm-3">
                            <h5><label class="control-label"><?= lang('') ?>Intenção</label></h5>
                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="other_infos_intent">
                                <option value="none" selected> - </option>
                                <option value="ivi" <?php
                                if (isset($mechanisms) && $mechanisms['other_infos_intent'] == 'ivi'): echo 'selected';
                                endif;
                                ?>><?= lang('') ?>Intencional - Violência interpessoal</option>
                                <option value="iva" <?php
                                if (isset($mechanisms) && $mechanisms['other_infos_intent'] == 'iva'): echo 'selected';
                                endif;
                                ?>><?= lang('') ?>Intencional - Violência autoprovocada</option>
                                <option value="ni" <?php
                                if (isset($mechanisms) && $mechanisms['other_infos_intent'] == 'ni'): echo 'selected';
                                endif;
                                ?>><?= lang('') ?>Não intencional</option>
                                <option value="ot" <?php
                                if (isset($mechanisms) && $mechanisms['other_infos_intent'] == 'ot'): echo 'selected';
                                endif;
                                ?>><?= lang('') ?>Não informado pelo paciente</option>
                                <option value="ig" <?php
                                if (isset($mechanisms) && $mechanisms['other_infos_intent'] == 'ig'): echo 'selected';
                                endif;
                                ?>><?= lang('') ?>Ignorado</option>
                            </select>
                        </div>
                        <br><br>
                        <div class="col-sm-6">
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="other_infos_rt" <?php
                                    if (isset($mechanisms) && $mechanisms['other_infos_rt'] == 1): echo 'checked';
                                    endif;
                                    ?>> <?= lang('') ?> Relacionado ao trabalho
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="other_infos_a" <?php
                                    if (isset($mechanisms) && $mechanisms['other_infos_a'] == 1): echo 'checked';
                                    endif;
                                    ?>> <?= lang('') ?> Álcool
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="other_infos_di" <?php
                                    if (isset($mechanisms) && $mechanisms['other_infos_di'] == 1): echo 'checked';
                                    endif;
                                    ?>> <?= lang('') ?> Drogas ilícitas
                                </label>
                        </div>
                    </div>
                </div>
			</div>

		<div class="row-fluid" style="margin-top:10px;">
			<div id="mechanisms_btn" class="btn-group pull-right">
				<a href="javascript:;" class="btn btn-success btn-sm" onclick="save_prehospital_mobile_mechanisms();"><?= lang('save') ?></a>
			</div>


<script>
    $('input.show-div').on('change', function () {
        var source = $(this);
        var target = $('#' + source.attr('data-target'));
        if ($('input[data-target=' + source.attr('data-target') + ']:checked').length)
            target.show();
        else
            target.hide();
    });
    //mecanismos
    jQuery('#blunt_type').change(function () {
        jQuery('.blunt_selects').hide();

        if (jQuery('#blunt_type').val() == 'four_wheel_vehicle') {
            jQuery('.blunt_vehicle_type').show();
            jQuery('.blunt_accupied_position_4').show();
            jQuery('.blunt_colision_type').show();
            jQuery('.blunt_four_ind').show();
        } else if (jQuery('#blunt_type').val() == 'motorcyclist') {
            jQuery('.blunt_accupied_position_2').show();
            jQuery('.blunt_description_position_2').show();
            jQuery('.blunt_motorcyclist_ind').show();
        } else if (jQuery('#blunt_type').val() == 'cyclist') {
            jQuery('.blunt_accupied_position_2').show();
            jQuery('.blunt_description_position_2').show();
            jQuery('.blunt_motorcyclist_ind').show();
        } else if (jQuery('#blunt_type').val() == 'hit') {
            jQuery('.blunt_hit_by').show();
        } else if(jQuery('#blunt_type').val() == 'drowning') {
            jQuery('.blunt_drowning_detail').show();
            jQuery('.blunt_drowning_ind').show();
        } else if(jQuery('#blunt_type').val() == 'ees') {
            jQuery('.blunt_ees_detail').show();
            jQuery('.blunt_ees_ind').show();
        } else if (jQuery('#blunt_type').val() == 'fall') {
            jQuery('.blunt_fall_type').show();
            jQuery('.blunt_fall_height').show();
            jQuery('.blunt_fall_ind').show();
        } else if (jQuery('#blunt_type').val() == 'explosion') {
            jQuery('.blunt_explos_detail').show();
            jQuery('.blunt_explos_ind').show();
        } else if (jQuery('#blunt_type').val() == 'fis_agress') {
            jQuery('.blunt_fisagress_detail').show();
        }
    });

    jQuery('#burn_type').change(function () {
        jQuery('#burn_type_other').hide();
        if (jQuery('#burn_type').val() === 'other') {
            jQuery('#burn_type_other').show();
        }
    });

    jQuery('.burn_area').change(function () {
        var total_area = 0;
        total_area = Number(jQuery('#burn_1').val()) + Number(jQuery('#burn_2').val()) + Number(jQuery('#burn_3').val());
        if (total_area > 100)
            total_area = 100;
        jQuery('#total_area_burn').val(total_area);
    });
</script>