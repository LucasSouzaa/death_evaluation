<div id="visits_resume"></div>
<div id="use_service_beds"></div>
<div id="visits_evolution"></div>
<div id="biochemistry_exams_evolution"></div>
<div id="image_exams_evolution"></div>
<div id="microbiology_exams_evolution"></div>
<div id="anatomical_pathological_exams_evolution"></div>
<div id="get_ec_diagnosys_list_evolution"></div>
<div id="get_i_diagnosys_list_evolution"></div>
<div id="drugs_evolution"></div>
<div id="surgery_anesthesia_evolution"></div>
<div id="procedures_evolution"></div>
<div id="use_of_blood_evolution"></div>
<div class="search-filter">
    <legend>Gerenciamento médico</legend>
</div>
<div id="medical_management_evolution"></div>

<div id="prognostic_indicators_evolution"></div>

<div id="imagery_evolution"></div>
<div id="outpatient_condition_evolution"></div>

<?php if ($hospitalized === 1): ?>
    <script>
        jQuery('#visits_resume').load(jQuery("body").data("baseurl") + "hospitalization/visits_resume");
        jQuery('#use_service_beds').load(jQuery("body").data("baseurl") + "hospitalization/use_service_beds");
        jQuery('#visits_evolution').load(jQuery("body").data("baseurl") + "hospitalization/visits/view");
        jQuery('#image_exams_evolution').load(jQuery("body").data("baseurl") + "death_evaluation/image_exams/view");
        jQuery('#anatomical_pathological_exams_evolution').load(jQuery("body").data("baseurl") + "death_evaluation/anatomical_pathological_exams/view");
        jQuery('#use_of_blood_evolution').load(jQuery("body").data("baseurl") + "hospitalization/use_of_blood/view");
        //jQuery('#medical_management_evolution').load(jQuery("body").data("baseurl") + "hospitalization/medical_management/view");
        //jQuery('#prognostic_indicators_evolution').load(jQuery("body").data("baseurl") + "hospitalization/prognostic_indicators/view");
        jQuery('#imagery_evolution').load(jQuery("body").data("baseurl") + "hospitalization/imagery/view");
        jQuery('#outpatient_condition_evolution').load(jQuery("body").data("baseurl") + "hospitalization/outpatient_condition");
    </script>
<?php endif; ?>

<script>
    jQuery('#biochemistry_exams_evolution').load(jQuery("body").data("baseurl") + "hospitalization/all_biochemistry_exams/view");
    jQuery('#get_ec_diagnosys_list_evolution').load(jQuery("body").data("baseurl") + "attendance_hospital/get_ec_diagnosys_list/view");
    jQuery('#get_i_diagnosys_list_evolution').load(jQuery("body").data("baseurl") + "attendance_hospital/get_i_diagnosys_list/view");
    jQuery('#drugs_evolution').load(jQuery("body").data("baseurl") + "death_evaluation/drugs/view");
    jQuery('#surgery_anesthesia_evolution').load(jQuery("body").data("baseurl") + "death_evaluation/surgery_anesthesia/view");
    jQuery('#procedures_evolution').load(jQuery("body").data("baseurl") + "death_evaluation/procedures/view");
</script>

<div class="pill-content">
    <div class="pill-pane active">
        <div class="container-fluid">
            <!-- IML -->
            <legend> 
                <a href="javascript:void(0)" onclick="togglePanels('#iml', '#iml_options');">IML</a>
                <div id="iml_chev" class="pull-right">
                    <a href="javascript:void(0)" onclick="togglePanels('#iml', '#iml_options');"><i class="fa fa-plus-square-o"></i></a>
                </div>
                <div id="iml_chevback" style="display:none;" class="pull-right">
                    <a href="javascript:void(0)" onclick="togglePanels('#iml', '#iml_options');"><i class="fa fa-minus-square-o"></i></a>
                </div>
            </legend>
            <div id="iml_options" class="btn-group" style="display:none; width: 100%">
                <div class="well" id="a">
                    <div class="row-fluid content">
                        <div class="span6">
                            <div class="control-group">
                                <div class="checkbox">
                                    <label>
                                        Fez autopsia - <?php
                                        if ($de['autopsy'] === 1): echo 'Sim';
                                        else: echo 'Não';
                                        endif;
                                        ?>
                                    </label>
                                </div>
                            </div>	
                            <div class="control-group">
                                <div class="checkbox">
                                    <label>
                                        Foi conclusiva - <?php
                                        if ($de['conclusive_iml'] === 1): echo 'Sim';
                                        else: echo 'Não';
                                        endif;
                                        ?>
                                    </label>
                                </div>
                            </div>
                        </div>	
                        <div class="span6">
                            <div class="control-group">
                                <label class="control-label">Achado</label>
                                <div class="controls">
                                    <?= $de['found'] ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="checkbox">
                                    <label>
                                        O achado da autopsia alterou a gravidade do caso - <?php
                                        if ($de['found_gravity'] === 1): echo 'Sim';
                                        else: echo 'Não';
                                        endif;
                                        ?>
                                    </label>
                                </div>
                            </div>
                            <div class="control-group found_gravity" hidden="" style="margin-left: 15px;">
                                <label class="control-label">Achado alterou gravidade do caso</label>
                                <div class="controls">
                                    <input class="form-control" type="text" id="found_changed_gravity">
                                </div>
                            </div>

                            <div class="control-group">
                                <div class="checkbox">
                                    <label>
                                        Alcoolemia - <?php
                                        if ($de['bac'] === 1): echo 'Sim';
                                        else: echo 'Não';
                                        endif;
                                        ?>
                                    </label>
                                </div>
                            </div>
                            <div class="control-group bac" hidden="" style="margin-left: 15px;">
                                <label class="control-label">Dosagem</label>
                                <div class="controls">
                                    <?= $de['dosage'] ?>
                                </div>
                            </div>
                            <div class="control-group bac" hidden="" style="margin-left: 15px;">
                                <label class="control-label">Valor normal</label>
                                <div class="controls">
                                    <?= $de['normal_value'] ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Indices de trauma  -->
            <legend> 
                <a href="javascript:void(0)" onclick="togglePanels('#ind_trauma', '#ind_trauma_options');">Indices de trauma</a>
                <div id="ind_trauma_chev" class="pull-right">
                    <a href="javascript:void(0)" onclick="togglePanels('#ind_trauma', '#ind_trauma_options');"><i class="fa fa-plus-square-o"></i></a>
                </div>
                <div id="ind_trauma_chevback" style="display:none;" class="pull-right">
                    <a href="javascript:void(0)" onclick="togglePanels('#ind_trauma', '#ind_trauma_options');"><i class="fa fa-minus-square-o"></i></a>
                </div>
            </legend>
            <div id="ind_trauma_options" class="btn-group" style="display:none; width: 100%">
                <div class="well" id="b">
                    <div class="content">
                        <div class="row-fluid">
                            <div class="span4">
                                <div class="control-group">
                                    <label class="control-label">RTS</label>
                                    <div class="controls">
                                        <?= $de['rts'] ?>
                                    </div>
                                </div>
                            </div>
                            <div class="span4">
                                <div class="control-group">
                                    <label class="control-label">ISS</label>
                                    <div class="controls">
                                        <?= $de['iss'] ?>
                                    </div>
                                </div>
                            </div>
                            <div class="span4">
                                <div class="control-group">
                                    <label class="control-label">NISS</label>
                                    <div class="controls">
                                        <?= $de['niss'] ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row-fluid">
                            <div class="span4">
                                <div class="control-group">
                                    <label class="control-label">TRISS (%)</label>
                                    <div class="controls">
                                        <?= $de['triss'] ?>
                                    </div>
                                </div>
                            </div>
                            <div class="span4">
                                <div class="control-group">
                                    <label class="control-label">NTRISS (%)</label>
                                    <div class="controls">
                                        <?= $de['ntriss'] ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--  Indices prognósticos -->
            <legend> 
                <a href="javascript:void(0)" onclick="togglePanels('#ind_prog', '#ind_prog_options');">Indices prognósticos</a>
                <div id="ind_prog_chev" class="pull-right">
                    <a href="javascript:void(0)" onclick="togglePanels('#ind_prog', '#ind_prog_options');"><i class="fa fa-plus-square-o"></i></a>
                </div>
                <div id="ind_prog_chevback" style="display:none;" class="pull-right">
                    <a href="javascript:void(0)" onclick="togglePanels('#ind_prog', '#ind_prog_options');"><i class="fa fa-minus-square-o"></i></a>
                </div>
            </legend>
            <div id="ind_prog_options" class="btn-group" style="display:none; width: 100%">
                <div class="well" id="c">
                    <div class="content">
                        <div class="row-fluid">
                            <div class="span4">
                                <div class="control-group">
                                    <label class="control-label">APACHE II</label>
                                    <div class="controls">
                                        <?= $de['apache2'] ?>
                                    </div>
                                </div>
                            </div>
                            <div class="span4">
                                <div class="control-group">
                                    <label class="control-label">Estimativa do risco de morte (%)</label>
                                    <div class="controls">
                                        <?= $de['risk_of_death_apache'] ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row-fluid">
                            <div class="span4">
                                <div class="control-group">
                                    <label class="control-label">SAPS III</label>
                                    <div class="controls">
                                        <?= $de['saps3'] ?>
                                    </div>
                                </div>
                            </div>
                            <div class="span4">
                                <div class="control-group">
                                    <label class="control-label">Estimativa do risco de morte (%)</label>
                                    <div class="controls">
                                        <?= $de['risk_of_death_saps'] ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <!-- Resultado da avaliação do óbito -->
            <legend>
                <a href="javascript:void(0)" onclick="togglePanels('#avaliacao_obito', '#avaliacao_obito_options');">Resultado da avaliação do óbito</a>
                <div id="avaliacao_obito_chev" class="pull-right">
                    <a href="javascript:void(0)" onclick="togglePanels('#avaliacao_obito', '#avaliacao_obito_options');"><i class="fa fa-plus-square-o"></i></a>
                </div>
                <div id="avaliacao_obito_chevback" style="display:none;" class="pull-right">
                    <a href="javascript:void(0)" onclick="togglePanels('#avaliacao_obito', '#avaliacao_obito_options');"><i class="fa fa-minus-square-o"></i></a>
                </div>
            </legend>
            <div id="avaliacao_obito_options" class="btn-group" style="display:none; width: 100%">
                <div class="well" id="d">
                    <div class="content">
                        <div class="row-fluid">
                            <div class="span6">
                                <div class="control-group">
                                    <h4>Resumo do caso</h4>
                                    <div class="controls">
                                        <?= $de['resume'] ?>
                                    </div>
                                </div>
                                </br>
                                <h4>Decisão sobre a morte</h4>
                                <label for="" class="radio control-label" style="margin-left: 15px">
                                    <?= $de['death_decision'] ?>
                                </label>
                                </br>

                                <div class="control-group">
                                    <h4>Problemas encontrados na condução do caso</h4>
                                    <div class="controls">
                                        <?= $de['case_problems'] ?>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <div class="checkbox">
                                        <label>
                                            Foi conclusivo - <?php
                                            if ($de['conclusive_death_evaluation'] === 1): echo 'Sim';
                                            else: echo 'Não';
                                            endif;
                                            ?>
                                        </label>
                                    </div>
                                </div>
                                </br>
                                <div class="control-group">
                                    <h4>Motivo</h4>
                                    <div class="controls">
                                        <?= $de['conclusive_death_evaluation_reason'] ?>
                                    </div>
                                </div>
                                </br>
                                <h4>Deficiências no tratamento</h4>
                                <div class="control-group">
                                    <div class="checkbox">
                                        <label>
                                            Nenhuma - <?php
                                            if ($de['deficiencies_none'] === 1): echo 'Sim';
                                            else: echo 'Não';
                                            endif;
                                            ?>
                                        </label>
                                    </div>
                                </div>	
                                <div class="control-group">
                                    <div class="checkbox">
                                        <label>
                                            Abordagem das vias aéreas - <?php
                                            if ($de['approach_airways'] === 1): echo 'Sim';
                                            else: echo 'Não';
                                            endif;
                                            ?>
                                        </label>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <div class="checkbox">
                                        <label>
                                            Controle da hemorragia - <?php
                                            if ($de['bleeding_control'] === 1): echo 'Sim';
                                            else: echo 'Não';
                                            endif;
                                            ?>
                                        </label>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <div class="checkbox">
                                        <label>
                                            Reposição volêmica - <?php
                                            if ($de['volemic_reposition'] === 1): echo 'Sim';
                                            else: echo 'Não';
                                            endif;
                                            ?>
                                        </label>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <div class="checkbox">
                                        <label>
                                            Abordagem do trauma por sistema - <?php
                                            if ($de['trauma_approach_system'] === 1): echo 'Sim';
                                            else: echo 'Não';
                                            endif;
                                            ?>
                                        </label>
                                    </div>
                                </div>	
                                <div class="control-group">
                                    <div class="checkbox">
                                        <label>
                                            Abordagem no CTI - <?php
                                            if ($de['cti_approach'] === 1): echo 'Sim';
                                            else: echo 'Não';
                                            endif;
                                            ?>
                                        </label>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <div class="checkbox">
                                        <label>
                                            Demora no tratamento - <?php
                                            if ($de['delay_treatment'] === 1): echo 'Sim';
                                            else: echo 'Não';
                                            endif;
                                            ?>
                                        </label>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <div class="checkbox">
                                        <label>
                                            Problemas relacionados à infecção - <?php
                                            if ($de['infection_related_problems'] === 1): echo 'Sim';
                                            else: echo 'Não';
                                            endif;
                                            ?>
                                        </label>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <div class="checkbox">
                                        <label>
                                            Deficiências na documentação - <?php
                                            if ($de['deficiencies_documentation'] === 1): echo 'Sim';
                                            else: echo 'Não';
                                            endif;
                                            ?>
                                        </label>
                                    </div>
                                </div>	
                                <div class="control-group">
                                    <div class="checkbox">
                                        <label>
                                            Outras - <?php
                                            if ($de['other_deficiencies'] === 1): echo 'Sim';
                                            else: echo 'Não';
                                            endif;
                                            ?>
                                        </label>
                                    </div>
                                </div>
                                </br>
                                <h4>Abordagem das vias aéreas</h4>
                                <div class="control-group">
                                    <div class="checkbox">
                                        <label>
                                            Demora na decisão - <?php
                                            if ($de['delay_decision'] === 1): echo 'Sim';
                                            else: echo 'Não';
                                            endif;
                                            ?>
                                        </label>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <div class="checkbox">
                                        <label>
                                            Dificuldades técnicas - <?php
                                            if ($de['technical_difficulties'] === 1): echo 'Sim';
                                            else: echo 'Não';
                                            endif;
                                            ?>
                                        </label>
                                    </div>
                                </div>	
                                <div class="control-group">
                                    <div class="checkbox">
                                        <label>
                                            Equipamentos - <?php
                                            if ($de['equipments'] === 1): echo 'Sim';
                                            else: echo 'Não';
                                            endif;
                                            ?>
                                        </label>
                                    </div>
                                </div>
                                </br>
                                <h4>Controle da hemorragia</h4>
                                <div class="control-group">
                                    <div class="checkbox">
                                        <label>
                                            Diagnóstico na ST - <?php
                                            if ($de['diagnosis_st'] === 1): echo 'Sim';
                                            else: echo 'Não';
                                            endif;
                                            ?>
                                        </label>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <div class="checkbox">
                                        <label>
                                            Técnica cirúrgica insuficiente - <?php
                                            if ($de['insufficient_surgical_technique'] === 1): echo 'Sim';
                                            else: echo 'Não';
                                            endif;
                                            ?>
                                        </label>
                                    </div>
                                </div>	
                                </br>
                                <h4>Reposição Volêmica</h4>
                                <div class="control-group">
                                    <div class="checkbox">
                                        <label>
                                            Excesso de cristaloides - <?php
                                            if ($de['excess_crystalloid'] === 1): echo 'Sim';
                                            else: echo 'Não';
                                            endif;
                                            ?>
                                        </label>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <div class="checkbox">
                                        <label>
                                            Acidose grave - <?php
                                            if ($de['severe_acidosis'] === 1): echo 'Sim';
                                            else: echo 'Não';
                                            endif;
                                            ?>
                                        </label>
                                    </div>
                                </div>	
                                <div class="control-group">
                                    <div class="checkbox">
                                        <label>
                                            Demora na hemotransfusão - <?php
                                            if ($de['delay_blood_transfusion'] === 1): echo 'Sim';
                                            else: echo 'Não';
                                            endif;
                                            ?>
                                        </label>
                                    </div>
                                </div>                                
                            </div>
                            <div class="span6">
                                <h4>Abordagem do trauma por sistema</h4>
                                <div class="control-group">
                                    <div class="checkbox">
                                        <label>
                                            Crânio - <?php
                                            if ($de['skull'] === 1): echo 'Sim';
                                            else: echo 'Não';
                                            endif;
                                            ?>
                                        </label>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <div class="checkbox">
                                        <label>
                                            Coluna - <?php
                                            if ($de['column'] === 1): echo 'Sim';
                                            else: echo 'Não';
                                            endif;
                                            ?>
                                        </label>
                                    </div>
                                </div>	
                                <div class="control-group">
                                    <div class="checkbox">
                                        <label>
                                            Tórax - <?php
                                            if ($de['torax'] === 1): echo 'Sim';
                                            else: echo 'Não';
                                            endif;
                                            ?>
                                        </label>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <div class="checkbox">
                                        <label>
                                            Abdome - <?php
                                            if ($de['abdome'] === 1): echo 'Sim';
                                            else: echo 'Não';
                                            endif;
                                            ?>
                                        </label>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <div class="checkbox">
                                        <label>
                                            Pelve - <?php
                                            if ($de['pelve'] === 1): echo 'Sim';
                                            else: echo 'Não';
                                            endif;
                                            ?>
                                        </label>
                                    </div>
                                </div>	
                                <div class="control-group">
                                    <div class="checkbox">
                                        <label>
                                            Membros - <?php
                                            if ($de['members'] === 1): echo 'Sim';
                                            else: echo 'Não';
                                            endif;
                                            ?>
                                        </label>
                                    </div>
                                </div>
                                </br>
                                <h4>Abordagem no CTI</h4>
                                <div class="control-group">
                                    <div class="checkbox">
                                        <label>
                                            Monitorização - <?php
                                            if ($de['monitoring'] === 1): echo 'Sim';
                                            else: echo 'Não';
                                            endif;
                                            ?>
                                        </label>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <div class="checkbox">
                                        <label>
                                            Ventilação mecânica - <?php
                                            if ($de['mechanical_ventilation_cti'] === 1): echo 'Sim';
                                            else: echo 'Não';
                                            endif;
                                            ?>
                                        </label>
                                    </div>
                                </div>	
                                <div class="control-group">
                                    <div class="checkbox">
                                        <label>
                                            Reversão de choque - <?php
                                            if ($de['shock_reversal'] === 1): echo 'Sim';
                                            else: echo 'Não';
                                            endif;
                                            ?>
                                        </label>
                                    </div>
                                </div>
                                </br>
                                <h4>Ventilação mecânica</h4>
                                <label for="" class="radio control-label" style="margin-left: 15px">
                                    <?= $de["mechanical_ventilation"] ?>
                                </label>
                                </br>
                                <h4>Demora no tratamento</h4>
                                <div class="control-group">
                                    <div class="checkbox">
                                        <label>
                                            Uso de drogas - <?php
                                            if ($de['drug_use'] === 1): echo 'Sim';
                                            else: echo 'Não';
                                            endif;
                                            ?>
                                        </label>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <div class="checkbox">
                                        <label>
                                            Decisão cirúrgica - <?php
                                            if ($de['surgical_decision'] === 1): echo 'Sim';
                                            else: echo 'Não';
                                            endif;
                                            ?>
                                        </label>
                                    </div>
                                </div>	
                                <div class="control-group">
                                    <div class="checkbox">
                                        <label>
                                            Internação na UTI - <?php
                                            if ($de['cti_hospitalization'] === 1): echo 'Sim';
                                            else: echo 'Não';
                                            endif;
                                            ?>
                                        </label>
                                    </div>
                                </div>
                                </br>
                                <h4>Problemas relacionados à infecção</h4>
                                <div class="control-group">
                                    <div class="checkbox">
                                        <label>
                                            Atraso no diagnóstico do foco infeccioso - <?php
                                            if ($de['delay_diagnosis_infectious_focus'] === 1): echo 'Sim';
                                            else: echo 'Não';
                                            endif;
                                            ?>
                                        </label>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <div class="checkbox">
                                        <label>
                                            Atraso no controle cirúrgico do foco infeccioso - <?php
                                            if ($de['delay_surgical_management_infectious_source'] === 1): echo 'Sim';
                                            else: echo 'Não';
                                            endif;
                                            ?>
                                        </label>
                                    </div>
                                </div>	
                                <div class="control-group">
                                    <div class="checkbox">
                                        <label>
                                            Atraso no início da antibioticoterapia - <?php
                                            if ($de['delayed_antibiotic_therapy'] === 1): echo 'Sim';
                                            else: echo 'Não';
                                            endif;
                                            ?>
                                        </label>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <div class="checkbox">
                                        <label>
                                            Antibioticoterapia empírica incorreta - <?php
                                            if ($de['Incorrect_empirical_antibiotic_therapy'] === 1): echo 'Sim';
                                            else: echo 'Não';
                                            endif;
                                            ?>
                                        </label>
                                    </div>
                                </div>	
                                <div class="control-group">
                                    <div class="checkbox">
                                        <label>
                                            Falta de exame microbiológico - <?php
                                            if ($de['lack_microbiological_examination'] === 1): echo 'Sim';
                                            else: echo 'Não';
                                            endif;
                                            ?>
                                        </label>
                                    </div>
                                </div>
                                </br>
                                <h4>Deficiência na documentação</h4>
                                <div class="control-group">
                                    <div class="checkbox">
                                        <label>
                                            Sinais vitais - <?php
                                            if ($de['vital_signs'] === 1): echo 'Sim';
                                            else: echo 'Não';
                                            endif;
                                            ?>
                                        </label>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <div class="checkbox">
                                        <label>
                                            Exames - <?php
                                            if ($de['exams'] === 1): echo 'Sim';
                                            else: echo 'Não';
                                            endif;
                                            ?>
                                        </label>
                                    </div>
                                </div>	
                                <div class="control-group">
                                    <div class="checkbox">
                                        <label>
                                            Condutas - <?php
                                            if ($de['conducts'] === 1): echo 'Sim';
                                            else: echo 'Não';
                                            endif;
                                            ?>
                                        </label>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <div class="checkbox">
                                        <label>
                                            Procedimentos - <?php
                                            if ($de['procedures'] === 1): echo 'Sim';
                                            else: echo 'Não';
                                            endif;
                                            ?>
                                        </label>
                                    </div>
                                </div>
                                </br>
                                <div class="control-group">
                                    <h4>Quais outros ?</h4>
                                    <div class="controls">
                                        <?= $de["other_documentations"] ?>
                                    </div>
                                </div>
                                </br>
                                <h4>LOCALIZAÇÃO DAS DEFICIÊNCIAS</h4>
                                <div class="control-group">
                                    <div class="checkbox">
                                        <label>
                                            Pré-hospitalar - <?php
                                            if ($de['pre_hospital'] === 1): echo 'Sim';
                                            else: echo 'Não';
                                            endif;
                                            ?>
                                        </label>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <div class="checkbox">
                                        <label>
                                            Sala de trauma - <?php
                                            if ($de['trauma_room'] === 1): echo 'Sim';
                                            else: echo 'Não';
                                            endif;
                                            ?>
                                        </label>
                                    </div>
                                </div>	
                                <div class="control-group">
                                    <div class="checkbox">
                                        <label>
                                            Sala cirúrgica - <?php
                                            if ($de['operating_room'] === 1): echo 'Sim';
                                            else: echo 'Não';
                                            endif;
                                            ?>
                                        </label>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <div class="checkbox">
                                        <label>
                                            Recuperação - <?php
                                            if ($de['recovery'] === 1): echo 'Sim';
                                            else: echo 'Não';
                                            endif;
                                            ?>
                                        </label>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <div class="checkbox">
                                        <label>
                                            CTI - <?php
                                            if ($de['cti'] === 1): echo 'Sim';
                                            else: echo 'Não';
                                            endif;
                                            ?>
                                        </label>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <div class="checkbox">
                                        <label>
                                            Semi-intensivo - <?php
                                            if ($de['semi_intensive'] === 1): echo 'Sim';
                                            else: echo 'Não';
                                            endif;
                                            ?>
                                        </label>
                                    </div>
                                </div>	
                                <div class="control-group">
                                    <div class="checkbox">
                                        <label>
                                            Enfermaria - <?php
                                            if ($de['ward'] === 1): echo 'Sim';
                                            else: echo 'Não';
                                            endif;
                                            ?>
                                        </label>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <div class="checkbox">
                                        <label>
                                            Inadequação do sistema - <?php
                                            if ($de['inadequacy_system'] === 1): echo 'Sim';
                                            else: echo 'Não';
                                            endif;
                                            ?>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Ações corretivas sugeridas -->
            <legend>
                <a href="javascript:void(0)" onclick="togglePanels('#acoes_sugeridas', '#acoes_sugeridas_options');">Ações corretivas sugeridas</a>
                <div id="acoes_sugeridas_chev" class="pull-right">
                    <a href="javascript:void(0)" onclick="togglePanels('#acoes_sugeridas', '#acoes_sugeridas_options');"><i class="fa fa-plus-square-o"></i></a>
                </div>
                <div id="acoes_sugeridas_chevback" style="display:none;" class="pull-right">
                    <a href="javascript:void(0)" onclick="togglePanels('#acoes_sugeridas', '#acoes_sugeridas_options');"><i class="fa fa-minus-square-o"></i></a>
                </div>
            </legend>
            <div id="acoes_sugeridas_options" class="btn-group" style="display:none; width: 100%">
                <div class="well" id="e">
                    <div class="content">
                        <div class="row-fluid content">
                            <div class="span6">
                                <h4>Educação continuada:</h4>
                                <div class="control-group">
                                    <label class="control-label">Temas teóricos</label>
                                    <div class="controls">
                                        <?= $de['theoretical_issues']; ?>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Temas práticos</label>
                                    <div class="controls">
                                        <?= $de['practical_issues'] ?>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Para todos os funcionários do setor</label>
                                    <div class="controls">
                                        <?= $de['employees_sector'] ?>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Para funcionários específicos</label>
                                    <div class="controls">
                                        <?= $de['specific_employees'] ?>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Outros</label>
                                    <div class="controls">
                                        <?= $de['other_continuing_education'] ?>
                                    </div>
                                </div>
                            </div>
                            <div class="span6">
                                <h4>Melhoria no sistema:</h4>
                                <div class="control-group">
                                    <label class="control-label">Equipamentos</label>
                                    <div class="controls">
                                        <?= $de['improvement_equipaments'] ?>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Desenvolvimento de práticas</label>
                                    <div class="controls">
                                        <?= $de['development_practical'] ?>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Desenvolvimento de protocolos</label>
                                    <div class="controls">
                                        <?= $de['developing_protocols'] ?>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Comunicação entre setores</label>
                                    <div class="controls">
                                        <?= $de['communication_sectors'] ?>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Outros</label>
                                    <div class="controls">
                                        <?= $de['other_improvement'] ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row-fluid content">
                            <div class="span6">

                                <h4>Treinamento de tópicos específicos:</h4>
                                <div class="control-group">
                                    <label class="control-label">Trauma por sistema</label>
                                    <div class="controls">
                                        <?= $de['trauma_system'] ?>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Procedimento técnico</label>
                                    <div class="controls">
                                        <?= $de['technical_procedure'] ?>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Transporte e acompanhamento do paciente</label>
                                    <div class="controls">
                                        <?= $de['transport_followup'] ?>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Outros</label>
                                    <div class="controls">
                                        <?= $de['other_training'] ?>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Revisor</label>
                                    <div class="controls">
                                        <?= $de['reviser'] ?>
                                    </div>
                                </div>
                            </div>
                            <div class="span6">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row-fluid">
                <a class="btn btn-success pull-right" href="javascript:void(0)" onclick="save_death_evaluation()"> Salvar</a>
            </div>
        </div>
    </div>
</div>


<script>

    jQuery('#found_gravity').change(function () {
        if (jQuery('#found_gravity').is(':checked')) {
            jQuery('.found_gravity').show();
        } else {
            jQuery('.found_gravity').hide();
        }
    });

    jQuery('#bac').change(function () {
        if (jQuery('#bac').is(':checked')) {
            jQuery('.bac').show();
        } else {
            jQuery('.bac').hide();
        }
    });

    jQuery('#d_VitalSign3EyeResponse').change(function () {
        var a = parseInt(jQuery('#d_VitalSign3EyeResponse').val());
        var b = parseInt(jQuery('#d_VitalSign3VerbalResponse').val());
        var c = parseInt(jQuery('#d_VitalSign3MotorResponse').val());
        jQuery('#d_VitalSign3Glasgow').val(a + b + c);
    });

    jQuery('#d_VitalSign3VerbalResponse').change(function () {
        var a = parseInt(jQuery('#d_VitalSign3EyeResponse').val());
        var b = parseInt(jQuery('#d_VitalSign3VerbalResponse').val());
        var c = parseInt(jQuery('#d_VitalSign3MotorResponse').val());
        jQuery('#d_VitalSign3Glasgow').val(a + b + c);
    });

    jQuery('#d_VitalSign3MotorResponse').change(function () {
        var a = parseInt(jQuery('#d_VitalSign3EyeResponse').val());
        var b = parseInt(jQuery('#d_VitalSign3VerbalResponse').val());
        var c = parseInt(jQuery('#d_VitalSign3MotorResponse').val());
        jQuery('#d_VitalSign3Glasgow').val(a + b + c);
    });



    function togglePanels(content, options) {
        jQuery(content).toggle();
        jQuery(content + '_chev').toggle();
        jQuery(content + '_chevback').toggle();
        jQuery(options).toggle();
    }

</script>

<script>

    function save_death_evaluation() {

        jQuery.ajax({
            url: jQuery("body").data("baseurl") + "death_evaluation/save_death_evaluation",
            type: "post",
            dataType: 'json',
            data: {
                autopsy: jQuery('#autopsy').is(':checked'),
                conclusive_iml: jQuery('#conclusive_iml').is(':checked'),
                found: jQuery('#found').val(),
                found_gravity: jQuery('#found_gravity').is(':checked'),
                found_changed_gravity: jQuery('#found_changed_gravity').val(),
                bac: jQuery('#bac').is(':checked'),
                dosage: jQuery('#dosage').val(),
                normal_value: jQuery('#normal_value').val(),
                rts: jQuery('#rts').val(),
                iss: jQuery('#iss').val(),
                niss: jQuery('#niss').val(),
                triss: jQuery('#triss').val(),
                ntriss: jQuery('#ntriss').val(),
                apache2: jQuery('#apache2').val(),
                risk_of_death_apache: jQuery('#risk_of_death_apache').val(),
                saps3: jQuery('#saps3').val(),
                risk_of_death_saps: jQuery('#risk_of_death_saps').val(),
                resume: jQuery('#resume').val(),
                death_decision: jQuery('input[name=death_decision]:checked').val(),
                case_problems: jQuery('#case_problems').val(),
                conclusive_death_evaluation: jQuery('#conclusive_death_evaluation').is(':checked'),
                conclusive_death_evaluation_reason: jQuery('#conclusive_death_evaluation_reason').val(),
                deficiencies_none: jQuery('#deficiencies_none').is(':checked'),
                approach_airways: jQuery('#approach_airways').is(':checked'),
                bleeding_control: jQuery('#bleeding_control').is(':checked'),
                volemic_reposition: jQuery('#volemic_reposition').is(':checked'),
                trauma_approach_system: jQuery('#trauma_approach_system').is(':checked'),
                cti_approach: jQuery('#cti_approach').is(':checked'),
                delay_treatment: jQuery('#delay_treatment').is(':checked'),
                infection_related_problems: jQuery('#infection_related_problems').is(':checked'),
                deficiencies_documentation: jQuery('#deficiencies_documentation').is(':checked'),
                other_deficiencies: jQuery('#other_deficiencies').is(':checked'),
                delay_decision: jQuery('#delay_decision').is(':checked'),
                technical_difficulties: jQuery('#technical_difficulties').is(':checked'),
                equipments: jQuery('#equipments').is(':checked'),
                diagnosis_st: jQuery('#diagnosis_st').is(':checked'),
                insufficient_surgical_technique: jQuery('#insufficient_surgical_technique').is(':checked'),
                excess_crystalloid: jQuery('#excess_crystalloid').is(':checked'),
                severe_acidosis: jQuery('#severe_acidosis').is(':checked'),
                delay_blood_transfusion: jQuery('#delay_blood_transfusion').is(':checked'),
                skull: jQuery('#skull').is(':checked'),
                column: jQuery('#column').is(':checked'),
                torax: jQuery('#torax').is(':checked'),
                abdome: jQuery('#abdome').is(':checked'),
                pelve: jQuery('#pelve').is(':checked'),
                members: jQuery('#members').is(':checked'),
                monitoring: jQuery('#monitoring').is(':checked'),
                mechanical_ventilation_cti: jQuery('#mechanical_ventilation_cti').is(':checked'),
                shock_reversal: jQuery('#shock_reversal').is(':checked'),
                mechanical_ventilation: jQuery('input[name=mechanical_ventilation]:checked').val(),
                drug_use: jQuery('#drug_use').is(':checked'),
                surgical_decision: jQuery('#surgical_decision').is(':checked'),
                cti_hospitalization: jQuery('#cti_hospitalization').is(':checked'),
                delay_diagnosis_infectious_focus: jQuery('#delay_diagnosis_infectious_focus').is(':checked'),
                delay_surgical_management_infectious_source: jQuery('#delay_surgical_management_infectious_source').is(':checked'),
                delayed_antibiotic_therapy: jQuery('#delayed_antibiotic_therapy').is(':checked'),
                Incorrect_empirical_antibiotic_therapy: jQuery('#Incorrect_empirical_antibiotic_therapy').is(':checked'),
                lack_microbiological_examination: jQuery('#lack_microbiological_examination').is(':checked'),
                vital_signs: jQuery('#vital_signs').is(':checked'),
                exams: jQuery('#exams').is(':checked'),
                conducts: jQuery('#conducts').is(':checked'),
                procedures: jQuery('#procedures').is(':checked'),
                other_documentations: jQuery('#other_documentations').val(),
                pre_hospital: jQuery('#pre_hospital').is(':checked'),
                trauma_room: jQuery('#trauma_room').is(':checked'),
                operating_room: jQuery('#operating_room').is(':checked'),
                recovery: jQuery('#recovery').is(':checked'),
                cti: jQuery('#cti').is(':checked'),
                semi_intensive: jQuery('#semi_intensive').is(':checked'),
                ward: jQuery('#ward').is(':checked'),
                inadequacy_system: jQuery('#inadequacy_system').is(':checked'),
                theoretical_issues: jQuery('#theoretical_issues').val(),
                practical_issues: jQuery('#practical_issues').val(),
                employees_sector: jQuery('#employees_sector').val(),
                specific_employees: jQuery('#specific_employees').val(),
                other_continuing_education: jQuery('#other_continuing_education').val(),
                improvement_equipaments: jQuery('#improvement_equipaments').val(),
                development_practical: jQuery('#development_practical').val(),
                developing_protocols: jQuery('#developing_protocols').val(),
                communication_sectors: jQuery('#communication_sectors').val(),
                other_improvement: jQuery('#other_improvement').val(),
                trauma_system: jQuery('#trauma_system').val(),
                technical_procedure: jQuery('#technical_procedure').val(),
                transport_followup: jQuery('#transport_followup').val(),
                other_training: jQuery('#other_training').val(),
                reviser: jQuery('#reviser').val()
            },
            success: function (response) {
                if (response.status === 'NOK') {
                    var notice = new PNotify({
                        title: 'Erro',
                        text: 'Tente novamente mais tarde',
                        type: 'error',
                        addclass: 'click-2-close',
                        hide: false,
                        buttons: {
                            closer: false,
                            sticker: false
                        }
                    });
                    notice.get().click(function () {
                        notice.remove();
                    });
                } else {
                    // recarrega avaliacao de obito
                    jQuery('#hospital_attendance_steps').load(jQuery("body").data("baseurl") + "attendance_hospital/primary_evaluation/view");
                }
            }
        });
    }
</script>
<script>
    jQuery('li').removeClass('active');
    jQuery('#li_death_evaluation').addClass('active');
</script>