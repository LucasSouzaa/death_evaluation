<div class="search-filter">
    <legend>Doenças preexistentes</legend>        
</div>
<div class="row-fluid">
                        <div class="checkbox">
                            <div class="col-md-3">
                                <label>
                                    <input type="checkbox" id="hiv"  <?php
                                    if ($history_complements['hiv']): echo 'checked';
                                    endif;
                                    ?>> HIV+
                                </label>
                            </div>
                            <div class="col-md-3">
                                <label>
                                    <input type="checkbox" id="hep"  <?php
                                    if ($history_complements['hep']): echo 'checked';
                                    endif;
                                    ?>> Hepatite
                                </label>
                            </div>
                            <div class="col-md-3">
                                <label>
                                    <input type="checkbox" id="has"  <?php
                                    if ($history_complements['has']): echo 'checked';
                                    endif;
                                    ?>> HAS
                                </label>
                            </div>
                            <div class="col-md-3">
                                <label>
                                    <input type="checkbox" id="diab"  <?php
                                    if ($history_complements['diab']): echo 'checked';
                                    endif;
                                    ?>> Diabetes
                                </label>
                            </div>
                            <div class="col-md-3">
                                <label>
                                    <input type="checkbox" id="dpoc"  <?php
                                    if ($history_complements['dpoc']): echo 'checked';
                                    endif;
                                    ?>> DPOC
                                </label>
                            </div>
                            <div class="col-md-3">
                                <label>
                                    <input type="checkbox" id="avc"  <?php
                                    if ($history_complements['avc']): echo 'checked';
                                    endif;
                                    ?>> AVC
                                </label>
                            </div>
                            <div class="col-md-3">
                                <label>
                                    <input type="checkbox" id="sci"  <?php
                                    if ($history_complements['sci']): echo 'checked';
                                    endif;
                                    ?>> Síndrome coronariana isquêmica
                                </label>
                            </div>
                            <div class="col-md-3">
                                <label>
                                    <input type="checkbox" id="psic_transt"  <?php
                                    if ($history_complements['psic_transt']): echo 'checked';
                                    endif;
                                    ?>> Transtorno psiquiátrico
                                </label>
                            </div>
                            <div class="col-md-3">
                                <label>
                                    <input type="checkbox" id="elit" <?php
                                    if ($history_complements['elit']): echo 'checked';
                                    endif;
                                    ?> > Elitismo
                                </label>
                            </div>
                            <div class="col-md-3">
                                <label>
                                    <input type="checkbox" id="udi" <?php
                                    if ($history_complements['udi']): echo 'checked';
                                    endif;
                                    ?>> Uso de drogas ilícitas
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row-fluid" style="margin-top:15px; margin-left:15px;">
                        <div class="col-md-12">
                            <label>Outras condições</label>
                        </div>
                        <div class="checkbox">
                            <div class="col-md-3">
                                <label>
                                    <input type="checkbox" id="ant_gestation" <?php
                                    if ($history_complements['ant_gestation']) : echo 'checked';
                                    endif;
                                    ?>> Gestação
                                </label>
                            </div>
                            <div class="col-md-3">
                                <label>
                                    <input type="checkbox" id="ant_tetanus_vaccination" <?php
                                    if ($history_complements['ant_tetanus_vaccination']) : echo 'checked';
                                    endif;
                                    ?>> Vacinação Antitetânica
                                </label>
                            </div>
                            <div class="col-md-3">
                                <label>
                                    <input type="checkbox" id="ant_acute_intoxication" <?php
                                    if ($history_complements['ant_acute_intoxication']): echo 'checked';
                                    endif;
                                    ?>> Intoxicação aguda
                                </label>
                            </div>
                            <div class="col-md-3">
                                <label>
                                    <input type="checkbox" id="ant_fluid_intake" <?php
                                    if ($history_complements['ant_fluid_intake']): echo 'checked';
                                    endif;
                                    ?>> Ingestão de líquidos/alimentos
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row-fluid" style="margin-top:15px; margin-left:15px;">
                        <div class="col-md-12">
                            <label>Deficiências</label>
                        </div>
                        <div class="checkbox">

                            <div class="col-md-3">
                                <label>
                                    <input type="checkbox" id="physical_disability" <?php
                                    if ($history_complements['physical_disability']): echo 'checked';
                                    endif;
                                    ?>> Deficência física
                                </label>
                            </div>
                            <div class="col-md-3">
                                <label>
                                    <input type="checkbox" id="mental_disability" <?php
                                    if ($history_complements['mental_disability']): echo 'checked';
                                    endif;
                                    ?>> Deficência mental
                                </label>
                            </div>
                            <div class="col-md-3">
                                <label>
                                    <input type="checkbox" id="hearing_deficiency" <?php
                                    if ($history_complements['hearing_deficiency']): echo 'checked';
                                    endif;
                                    ?>> Deficência auditiva
                                </label>
                            </div>
                            <div class="col-md-3">
                                <label>
                                    <input type="checkbox" id="visual_impairment" <?php
                                    if ($history_complements['visual_impairment']): echo 'checked';
                                    endif;
                                    ?>> Deficência visual
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>