<div class="search-filter">
    <legend>Medicamentos</legend>        
</div>
<div class="row-fluid">
    <div id="history_medicines_list">
        <?php if (isset($medicines)): ?>
            <table class="table table-striped table-bordered table-condensed table-hover">
                <thead>
                    <tr>
                        <th><?= lang('drug_name') ?></th>
                        <th> Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($medicines as $hm): ?>
                        <tr>
                            <th><?= $hm['name'] ?></th>
                            <th><a href="javascript:;" onclick="delete_history_item('medicines', <?= $hm['id'] ?>)" class="on-default remove-row" style=" margin-right: 5px; margin-left: 5px;"><i class="fa fa-trash-o"></i></a></th>
                        </tr>
<?php endforeach; ?>
        <?php
        else:?>
                        <tr>
                            <th>Lista Vazia</th>
                            <th></th>
                        </tr>
        <?php endif;
        ?>
                </tbody>
            </table>

    </div>
</div>