<div class="search-filter">
    <legend>Cirurgias</legend>        
</div>
<div class="row-fluid">
<div id="history_surgeries_list">
    <?php if (isset($surgeries)): ?>
        <table class="table table-striped table-bordered table-condensed table-hover">
            <thead>
                <tr>
                    <th>Cirurgias</th>
                    <th> Action</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($surgeries as $hc): ?>
                    <tr>
                        <th><?= $hc['name'] ?></th>
                        <th><a href="javascript:;" onclick="delete_history_item('surgeries', <?= $hc['id'] ?>)" class="on-default remove-row" style=" margin-right: 5px; margin-left: 5px;"><i class="fa fa-trash-o"></i></a></th>
                    </tr>
<?php endforeach; ?>
        <?php
        else:?>
                        <tr>
                            <th>Lista Vazia</th>
                            <th></th>
                        </tr>
        <?php endif;
        ?>
                </tbody>
            </table>

    </div>
</div>
