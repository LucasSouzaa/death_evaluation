<div class="search-filter">
    <legend>Alergias</legend>        
</div>
<div class="row-fluid">
    <div id="history_allergies_list">
        <?php if (isset($allergies)): ?>
            <table class="table table-striped table-bordered table-condensed table-hover">
                <thead>
                    <tr>
                        <th>Alergias</th>
                        <th> Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($allergies as $ha): ?>
                        <tr>
                            <th><?= $ha['name'] ?></th>
                            <th><a href="javascript:;" onclick="delete_history_item('allergies', <?= $ha['id'] ?>)" class="on-default remove-row" style=" margin-right: 5px; margin-left: 5px;"><i class="fa fa-trash-o"></i></a></th>
                        </tr>
<?php endforeach; ?>
        <?php
        else:?>
                        <tr>
                            <th>Lista Vazia</th>
                            <th></th>
                        </tr>
        <?php endif;
        ?>
                </tbody>
            </table>

    </div>
</div>