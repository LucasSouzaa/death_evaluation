<!-- Resultado da avaliação do óbito -->
<legend>
    <a href="javascript:void(0)" onclick="togglePanels('#avaliacao_obito', '#avaliacao_obito_options');">Resultado da avaliação do óbito</a>
    <div id="avaliacao_obito_chev" class="pull-right">
        <a href="javascript:void(0)" onclick="togglePanels('#avaliacao_obito', '#avaliacao_obito_options');"><i class="fa fa-plus-square-o"></i></a>
    </div>
    <div id="avaliacao_obito_chevback" style="display:none;" class="pull-right">
        <a href="javascript:void(0)" onclick="togglePanels('#avaliacao_obito', '#avaliacao_obito_options');"><i class="fa fa-minus-square-o"></i></a>
    </div>
</legend>
<div id="avaliacao_obito_options" class="btn-group" style="display:none; width: 100%">
    <div class="well" id="d">
        <div class="content">
            <div class="row-fluid">
                <div class="span6">
                    <div class="control-group">
                        <h4>Resumo do caso</h4>
                        <div class="controls">
                            <textarea class="form-control" id="resume"></textarea>
                        </div>
                    </div>
                    </br>
                    <h4>Decisão sobre a morte</h4>
                    <label for="" class="radio control-label" style="margin-left: 15px">
                        <input type="radio" name="death_decision" value="1" > Evitável
                    </label>
                    <label for="" class="radio control-label" style="margin-left: 15px">
                        <input type="radio" name="death_decision" value="2" > Potencialmente evitável
                    </label>
                    <label for="" class="radio control-label" style="margin-left: 15px">
                        <input type="radio" name="death_decision" value="3" > Inevitável, sem possibilidade de melhora
                    </label>
                    <label for="" class="radio control-label" style="margin-left: 15px">
                        <input type="radio" name="death_decision" value="4" > Não evitável, mas com cuidado que pode ser melhorado
                    </label>
                    </br>

                    <div class="control-group">
                        <h4>Problemas encontrados na condução do caso</h4>
                        <div class="controls">
                            <input class="form-control" type="text" id="case_problems">
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="conclusive_death_evaluation" > Foi conclusivo
                            </label>
                        </div>
                    </div>
                    </br>
                    <div class="control-group">
                        <h4>Motivo</h4>
                        <div class="controls">
                            <input class="form-control" type="text" id="conclusive_death_evaluation_reason">
                        </div>
                    </div>
                    </br>
                    <h4>Deficiências no tratamento</h4>
                    <div class="control-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="deficiencies_none" > Nenhuma
                            </label>
                        </div>
                    </div>	
                    <div class="control-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="approach_airways" > Abordagem das vias aéreas
                            </label>
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="bleeding_control" > Controle da hemorragia
                            </label>
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="volemic_reposition" > Reposição volêmica
                            </label>
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="trauma_approach_system" > Abordagem do trauma por sistema
                            </label>
                        </div>
                    </div>	
                    <div class="control-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="cti_approach" > Abordagem no CTI
                            </label>
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="delay_treatment" > Demora no tratamento
                            </label>
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="infection_related_problems" > Problemas relacionados à infecção
                            </label>
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="deficiencies_documentation" > Deficiências na documentação
                            </label>
                        </div>
                    </div>	
                    <div class="control-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="other_deficiencies" > Outras
                            </label>
                        </div>
                    </div>
                    </br>
                    <h4>Abordagem das vias aéreas</h4>
                    <div class="control-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="delay_decision" > Demora na decisão
                            </label>
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="technical_difficulties" > Dificuldades técnicas
                            </label>
                        </div>
                    </div>	
                    <div class="control-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="equipments" > Equipamentos
                            </label>
                        </div>
                    </div>
                    </br>
                    <h4>Controle da hemorragia</h4>
                    <div class="control-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="diagnosis_st" > Diagnóstico na ST
                            </label>
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="insufficient_surgical_technique" > Técnica cirúrgica insuficiente
                            </label>
                        </div>
                    </div>	
                    </br>
                    <h4>Reposição Volêmica</h4>
                    <div class="control-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="excess_crystalloid" > Excesso de cristaloides
                            </label>
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="severe_acidosis" > Acidose grave
                            </label>
                        </div>
                    </div>	
                    <div class="control-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="delay_blood_transfusion" > Demora na hemotransfusão
                            </label>
                        </div>
                    </div>                                
                </div>
                <div class="span6">
                    <h4>Abordagem do trauma por sistema</h4>
                    <div class="control-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="skull" > crânio
                            </label>
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="column" > coluna
                            </label>
                        </div>
                    </div>	
                    <div class="control-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="torax" > tórax
                            </label>
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="abdome" > abdome
                            </label>
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="pelve" > pelve
                            </label>
                        </div>
                    </div>	
                    <div class="control-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="members" > membros
                            </label>
                        </div>
                    </div>
                    </br>
                    <h4>Abordagem no CTI</h4>
                    <div class="control-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="monitoring" > monitorização
                            </label>
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="mechanical_ventilation_cti" > ventilação mecânica
                            </label>
                        </div>
                    </div>	
                    <div class="control-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="shock_reversal" > reversão de choque
                            </label>
                        </div>
                    </div>
                    </br>
                    <h4>Ventilação mecânica</h4>
                    <label for="" class="radio control-label" style="margin-left: 15px">
                        <input type="radio" name="mechanical_ventilation" value="Invasivo" > Invasivo
                    </label>
                    <label for="" class="radio control-label" style="margin-left: 15px">
                        <input type="radio" name="mechanical_ventilation" value="Não invasivo" > Não invasivo
                    </label>
                    </br>
                    <h4>Demora no tratamento</h4>
                    <div class="control-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="drug_use" > Uso de drogas
                            </label>
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="surgical_decision" > Decisão cirúrgica
                            </label>
                        </div>
                    </div>	
                    <div class="control-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="cti_hospitalization" > Internação na UTI
                            </label>
                        </div>
                    </div>
                    </br>
                    <h4>Problemas relacionados à infecção</h4>
                    <div class="control-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="delay_diagnosis_infectious_focus" > Atraso no diagnóstico do foco infeccioso
                            </label>
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="delay_surgical_management_infectious_source" > Atraso no controle cirúrgico do foco infeccioso
                            </label>
                        </div>
                    </div>	
                    <div class="control-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="delayed_antibiotic_therapy" > Atraso no início da antibioticoterapia                                        </label>
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="Incorrect_empirical_antibiotic_therapy" > Antibioticoterapia empírica incorreta
                            </label>
                        </div>
                    </div>	
                    <div class="control-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="lack_microbiological_examination" > Falta de exame microbiológico
                            </label>
                        </div>
                    </div>
                    </br>
                    <h4>Deficiência na documentação</h4>
                    <div class="control-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="vital_signs" > sinais vitais
                            </label>
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="exams" > Exames
                            </label>
                        </div>
                    </div>	
                    <div class="control-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="conducts" > Condutas
                            </label>
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="procedures" > Procedimentos
                            </label>
                        </div>
                    </div>
                    </br>
                    <div class="control-group">
                        <h4>Quais outros ?</h4>
                        <div class="controls">
                            <input class="form-control" type="text" id="other_documentations">
                        </div>
                    </div>
                    </br>
                    <h4>LOCALIZAÇÃO DAS DEFICIÊNCIAS</h4>
                    <div class="control-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="pre_hospital" > Pré-hospitalar
                            </label>
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="trauma_room" > Sala de trauma
                            </label>
                        </div>
                    </div>	
                    <div class="control-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="operating_room" > Sala cirúrgica
                            </label>
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="recovery" > Recuperação
                            </label>
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="cti" > CTI
                            </label>
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="semi_intensive" > Semi-intensivo
                            </label>
                        </div>
                    </div>	
                    <div class="control-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="ward" > Enfermaria
                            </label>
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="inadequacy_system" > Inadequação do sistema
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>