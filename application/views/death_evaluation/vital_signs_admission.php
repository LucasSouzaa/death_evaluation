<div id="c_circulacao_options" class="btn-group" >
    <div class="well" id="c">
        <div class="search-filter">
            <legend>Sinais Vitais</legend>        
        </div>
        <div class="content">
            <!-- sinais vitais -->
            <div class="row-fluid">
                <div class="span2">
                    <div class="control-group">
                        <label for="VitalSign3Pa" class="control-label">PA</label>
                        <div class="controls">
                            <input class="form-control" type="text" id="pa" value="<?= isset($vital_signs_admissao['pa']) ? $vital_signs_admissao['pa'] : '0' ?>">
                        </div>
                    </div>
                </div>
                <div class="span2">
                    <div class="control-group">
                        <label for="VitalSign3Fc" class="control-label">FC</label>
                        <div class="controls">
                            <input class="form-control" type="text" id="fc" value="<?= isset($vital_signs_admissao['fc']) ? $vital_signs_admissao['fc'] : '0' ?>">
                        </div>
                    </div>
                </div>
                <div class="span2">
                    <div class="control-group">
                        <label for="VitalSign3Fr" class="control-label">FR</label>
                        <div class="controls">
                            <input class="form-control" type="text" id="fr" value="<?= isset($vital_signs_admissao['fr']) ? $vital_signs_admissao['fr'] : '0' ?>">
                        </div>
                    </div>
                </div>
                <div class="span2">
                    <div class="control-group">
                        <label for="VitalSign3SatO2" class="control-label">Sat O2</label>
                        <div class="controls">
                            <input class="form-control" type="text" id="sato2" value="<?= isset($vital_signs_admissao['sato2']) ? $vital_signs_admissao['sato2'] : '0' ?>">
                        </div>
                    </div>
                </div>
                <div class="span2">
                    <div class="control-group">
                        <label for="VitalSign3AxillaryTemperature" class="control-label">Temperatura axilar</label>
                        <div class="controls">
                            <input class="form-control" type="text" id="axillary_temperature" value="<?= isset($vital_signs_admissao['axillary_temperature']) ? $vital_signs_admissao['axillary_temperature'] : '0' ?>">
                        </div>
                    </div>
                </div>
                <div class="span2">
                    <div class="control-group">
                        <label for="VitalSign3EsophagealTemperature" class="control-label">Temp. esofágica</label>
                        <div class="controls">
                            <input class="form-control" type="text" id="esophageal_temperature" value="<?= isset($vital_signs_admissao['esophageal_temperature']) ? $vital_signs_admissao['esophageal_temperature'] : '0' ?>">
                        </div>
                    </div>
                </div>
            </div>
            </br>

            <div class="row-fluid">
                    <div class="col-sm-3">
                        <h5><label class="control-label"><?= lang('ocular_response') ?></label></h5>
                        <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="glasgow_visual_response_admissao">
                            <option value="0" selected> - </option>
                            <option value="4" <?php
                            if (isset($vital_signs_admissao['glasgow_visual_response']) && $vital_signs_admissao['glasgow_visual_response'] == '4'): echo 'selected';
                            endif;
                            ?>>
                                4 - <?= lang('he_opens_eyes_spontaneously') ?>
                            </option>
                            <option value="3" <?php
                            if (isset($vital_signs_admissao['glasgow_visual_response']) && $vital_signs_admissao['glasgow_visual_response'] == '3'): echo 'selected';
                            endif;
                            ?>>
                                3 - <?= lang('open_your_eyes_response_call') ?>
                            </option>
                            <option value="2" <?php
                            if (isset($vital_signs_admissao['glasgow_visual_response']) && $vital_signs_admissao['glasgow_visual_response'] == '2'): echo 'selected';
                            endif;
                            ?>>
                                2 - <?= lang('open_your_eyes_response_pain_stimulus') ?>
                            </option>
                            <option value="1" <?php
                            if (isset($vital_signs_admissao['glasgow_visual_response']) && $vital_signs_admissao['glasgow_visual_response'] == '1'): echo 'selected';
                            endif;
                            ?>>
                                1 - <?= lang('do_not_open_your_eyes') ?>
                            </option>
                        </select>
                    </div>

                    <div class="col-sm-3">
                        <h5><label class="control-label"><?= lang('verbal_response') ?></label></h5>
                        <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="glasgow_verbal_response_admissao">
                            <option value="0" selected> - </option>
                            <option value="5" <?php
                            if (isset($vital_signs_admissao['glasgow_verbal_response']) && $vital_signs_admissao['glasgow_verbal_response'] == '5'): echo 'selected';
                            endif;
                            ?>>
                                5 - <?= lang('oriented_talk_normally') ?>
                            </option>
                            <option value="4" <?php
                            if (isset($vital_signs_admissao['glasgow_verbal_response']) && $vital_signs_admissao['glasgow_verbal_response'] == '4'): echo 'selected';
                            endif;
                            ?>>
                                4 - <?= lang('confused_disoriented') ?>
                            </option>
                            <option value="3" <?php
                            if (isset($vital_signs_admissao['glasgow_verbal_response']) && $vital_signs_admissao['glasgow_verbal_response'] == '3'): echo 'selected';
                            endif;
                            ?>>
                                3 - <?= lang('pronounces_disconnected_words') ?>
                            </option>
                            <option value="2" <?php
                            if (isset($vital_signs_admissao['glasgow_verbal_response']) && $vital_signs_admissao['glasgow_verbal_response'] == '2'): echo 'selected';
                            endif;
                            ?>>
                                2 - <?= lang('emits_incomprehensible_sounds') ?>
                            </option>
                            <option value="1" <?php
                            if (isset($vital_signs_admissao['glasgow_verbal_response']) && $vital_signs_admissao['glasgow_verbal_response'] == '1'): echo 'selected';
                            endif;
                            ?>>
                                1 - <?= lang('muted') ?>
                            </option>
                        </select>
                    </div>

                    <div class="col-sm-4">
                        <h5><label class="control-label"><?= lang('motor_response') ?></label></h5>
                        <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="glasgow_motor_response_admissao">
                            <option value="0" selected> - </option>
                            <option value="6" <?php
                            if (isset($vital_signs_admissao['glasgow_motor_response']) && $vital_signs_admissao['glasgow_motor_response'] == '6'): echo 'selected';
                            endif;
                            ?>>
                                6 - <?= lang('obeys_commands') ?>
                            </option>
                            <option value="5" <?php
                            if (isset($vital_signs_admissao['glasgow_motor_response']) && $vital_signs_admissao['glasgow_motor_response'] == '5'): echo 'selected';
                            endif;
                            ?>>
                                5 - <?= lang('locate_painful_stimuli') ?>
                            </option>
                            <option value="4" <?php
                            if (isset($vital_signs_admissao['glasgow_motor_response']) && $vital_signs_admissao['glasgow_motor_response'] == '4'): echo 'selected';
                            endif;
                            ?>>
                                4 - <?= lang('nonspecific_flexion_reflex_withdrawal_painful_stimuli') ?>
                            </option>
                            <option value="3" <?php
                            if (isset($vital_signs_admissao['glasgow_motor_response']) && $vital_signs_admissao['glasgow_motor_response'] == '3'): echo 'selected';
                            endif;
                            ?>>
                                3 - <?= lang('abnormal_flexion_painful_stimuli') ?>
                            </option>
                            <option value="2" <?php
                            if (isset($vital_signs_admissao['glasgow_motor_response']) && $vital_signs_admissao['glasgow_motor_response'] == '2'): echo 'selected';
                            endif;
                            ?>>
                                2 - <?= lang('extension_painful_stimuli') ?>
                            </option>
                            <option value="1" <?php
                            if (isset($vital_signs_admissao['glasgow_motor_response']) && $vital_signs_admissao['glasgow_motor_response'] == '1'): echo 'selected';
                            endif;
                            ?>>
                                1 - <?= lang('it_not_moves') ?>
                            </option>
                        </select>
                    </div>
                    <div class="col-sm-2">
                        <h5><label class="control-label"><?= lang('') ?> Escala de Glasgow</label></h5>
                        <input id="glasgow_score_admissao" type="number" class="form-control" value="0" disabled=""/> 
                    </div>
                </div>
        </div>


<script>
    //sinais vitais
    jQuery('#glasgow_visual_response_admissao').change(function () {
        var a = parseInt(jQuery('#glasgow_visual_response_admissao').val());
        var b = parseInt(jQuery('#glasgow_verbal_response_admissao').val());
        var c = parseInt(jQuery('#glasgow_motor_response_admissao').val());
        jQuery('#glasgow_score_admissao').val(a + b + c);
    });


    jQuery('#glasgow_verbal_response_admissao').change(function () {
        var a = parseInt(jQuery('#glasgow_visual_response_admissao').val());
        var b = parseInt(jQuery('#glasgow_verbal_response_admissao').val());
        var c = parseInt(jQuery('#glasgow_motor_response_admissao').val());
        jQuery('#glasgow_score_admissao').val(a + b + c);
    });

    jQuery('#glasgow_motor_response_admissao').change(function () {
        var a = parseInt(jQuery('#glasgow_visual_response_admissao').val());
        var b = parseInt(jQuery('#glasgow_verbal_response_admissao').val());
        var c = parseInt(jQuery('#glasgow_motor_response_admissao').val());
        jQuery('#glasgow_score_admissao').val(a + b + c);
    }); 

    $(document).ready(function(){
        var a = parseInt(jQuery('#glasgow_visual_response_admissao').val());
        var b = parseInt(jQuery('#glasgow_verbal_response_admissao').val());
        var c = parseInt(jQuery('#glasgow_motor_response_admissao').val());
        jQuery('#glasgow_score_admissao').val(a + b + c);
    })
</script>