<br/>
<div class="search-filter">
    <legend>Procedimentos do atendimento hospitalar</legend>        
</div>

<div class="row-fluid procedure_h">
    <div class="col-sm-1 pull-left">
        <a href="javascript:void(0)" onclick="jQuery('.procedure_h #acontent').toggle();" class="btn btn-default">A</a>
    </div>
    <div id="acontent" class="col-sm-11">
        <div class="checkbox">
            <label>
                <input type="checkbox" id="oxign_dispo" name="group[]" class="show-div" data-target="oxign_dispo_c" <?php
                if (isset($procedures_hospital) && $procedures_hospital['oxign_dispo'] == 1): echo 'checked';
                endif;
                ?>> Dispositivo de oxigênio
            </label>
        </div>

        <div id="oxign_dispo_c" style="margin-left: 30px; <?php
        if (!isset($procedures_hospital) || $procedures_hospital['oxign_dispo'] == 0): echo 'display:none;';
        endif;
        ?>">
            <h6><label class="control-label">Tipo de dispositivo</label></h6>
            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="oxdisptype">
                <option value="none" selected> - </option>
                <option value="cat" <?php
                if (isset($procedures_hospital) && $procedures_hospital['oxdisptype'] == 'cat'): echo 'selected';
                endif;
                ?>>Catéter</option>
                <option value="mascreserv" <?php
                if (isset($procedures_hospital) && $procedures_hospital['oxdisptype'] == 'mascreserv'): echo 'selected';
                endif;
                ?>>Máscara com reservatório</option>
                <option value="mascnonreserv" <?php
                if (isset($procedures_hospital) && $procedures_hospital['oxdisptype'] == 'mascnonreserv'): echo 'selected';
                endif;
                ?>>Máscara sem reservatório</option>
            </select>
        </div>

        <div class="checkbox">
            <label>
                <input type="checkbox" id="breaf_vias" name="group[]" class="show-div" data-target="aer_via_c" <?php
                if (isset($procedures_hospital) && $procedures_hospital['breaf_vias'] == 1): echo 'checked';
                endif;
                ?>>  Via aérea
            </label>
        </div>

        <div id="aer_via_c" style="margin-left: 30px;  <?php
        if (!isset($procedures_hospital) || $procedures_hospital['breaf_vias'] == 0): echo 'display:none;';
        endif;
        ?>"> 
            <div class="checkbox">
                <label>
                    <input type="checkbox" id="maintenance_tec" name="group[]" class="show-div" data-target="manut_tec_c"<?php
                    if (isset($procedures_hospital) && $procedures_hospital['maintenance_tec'] == 1): echo 'checked';
                    endif;
                    ?>>  Técnicas de manutenção
                </label>
            </div>

            <div id="manut_tec_c" style="margin-left: 40px; <?php
            if (!isset($procedures_hospital) || $procedures_hospital['maintenance_tec'] == 0): echo 'display:none;';
            endif;
            ?>">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" id="mt_oro" <?php
                        if (isset($procedures_hospital) && $procedures_hospital['mt_oro'] == 1): echo 'checked';
                        endif;
                        ?>>  Orofaríngea
                    </label>
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" id="mt_naso" <?php
                        if (isset($procedures_hospital) && $procedures_hospital['mt_naso'] == 1): echo 'checked';
                        endif;
                        ?>>  Nasofaríngea
                    </label>
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" id="mt_lar_mask" <?php
                        if (isset($procedures_hospital) && $procedures_hospital['mt_lar_mask'] == 1): echo 'checked';
                        endif;
                        ?>>  Máscara laríngea
                    </label>
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" id="mt_comb" <?php
                        if (isset($procedures_hospital) && $procedures_hospital['mt_comb'] == 1): echo 'checked';
                        endif;
                        ?>>  Combitube
                    </label>
                </div>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" id="brief_def_via" name="group[]" class="show-div" data-target="ae_via_def_c" <?php
                    if (isset($procedures_hospital) && $procedures_hospital['brief_def_via'] == 1): echo 'checked';
                    endif;
                    ?>>  Uso de Via aérea definitiva
                </label>
            </div>

            <div id="ae_via_def_c" style="margin-left: 40px;  <?php
            if (!isset($procedures_hospital) || $procedures_hospital['brief_def_via'] == 0): echo 'display:none;';
            endif;
            ?>">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" id="bf_fst_int_seq" <?php
                        if (isset($procedures_hospital) && $procedures_hospital['bf_fst_int_seq'] == 1): echo 'checked';
                        endif;
                        ?>>  Sequência rápida de intubação
                    </label>
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" id="bf_oro" <?php
                        if (isset($procedures_hospital) && $procedures_hospital['bf_oro'] == 1): echo 'checked';
                        endif;
                        ?>>  Orotraqueal
                    </label>
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" id="bf_naso" <?php
                        if (isset($procedures_hospital) && $procedures_hospital['bf_naso'] == 1): echo 'checked';
                        endif;
                        ?>>  Nasotraqueal
                    </label>
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" id="bf_cir" name="group[]" class="show-div" data-target="defin-aer-vie-cirurg" <?php
                        if (isset($procedures_hospital) && $procedures_hospital['bf_cir'] == 1): echo 'checked';
                        endif;
                        ?>>  Cirúrgico
                    </label>
                </div>

                <div id="defin-aer-vie-cirurg" style="margin-left: 40px;  <?php
                if (!isset($procedures_hospital) || $procedures_hospital['bf_cir'] == 0): echo 'display:none;';
                endif;
                ?>">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" id="bf_cir_cripun" <?php
                            if (isset($procedures_hospital) && $procedures_hospital['bf_cir_cripun'] == 1): echo 'checked';
                            endif;
                            ?>>  Cricotireoidostomia por punção
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" id="bf_cir_cricir" <?php
                            if (isset($procedures_hospital) && $procedures_hospital['bf_cir_cricir'] == 1): echo 'checked';
                            endif;
                            ?>>  Cricotireoidostomia cirúrgica
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" id="bf_cir_critraq" <?php
                            if (isset($procedures_hospital) && $procedures_hospital['bf_cir_critraq'] == 1): echo 'checked';
                            endif;
                            ?>>  Traqueostomia
                        </label>
                    </div>
                </div>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" id="cervical_col" <?php
                    if (isset($procedures_hospital) && $procedures_hospital['cervical_col'] == 1): echo 'checked';
                    endif;
                    ?>>  Colar cervical
                </label>
            </div>

        </div>
    </div>
</div>

<div class="row-fluid procedure_h">
    <div class="col-sm-1 pull-left">
        <a href="javascript:void(0)" onclick="jQuery('.procedure_h #bcontent').toggle();" class="btn btn-default">B</a>
    </div>
    <div id="bcontent" class="col-sm-11">
        <div class="checkbox">
            <label>
                <input type="checkbox" id="mec_vent" name="group[]" class="show-div" data-target="mec_vent_c" <?php
                if (isset($procedures_hospital) && $procedures_hospital['mec_vent'] == 1): echo 'checked';
                endif;
                ?>> Ventilação
            </label>
        </div>

        <div id="mec_vent_c" style="margin-left: 30px; <?php
        if (!isset($procedures_hospital) || $procedures_hospital['mec_vent'] == 0): echo 'display:none;';
        endif;
        ?>"> 
            <h6><label class="control-label">Tipo de ventilação</label></h6>
            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="mec_vent_type">
                <option value="none" selected> - </option>
                <option value="manual" <?php
                if (isset($procedures_hospital) && $procedures_hospital['mec_vent_type'] == 'manual'): echo 'selected';
                endif;
                ?>>Ventilação manual (Ambu)</option>
                <option value="mecanic" <?php
                if (isset($procedures_hospital) && $procedures_hospital['mec_vent_type'] == 'mecanic'): echo 'selected';
                endif;
                ?>>Ventilação mecânica</option>
            </select>
        </div>

        <div id="mecanic_vent_c" style="margin-left: 40px; <?php
        if (!isset($procedures_hospital) || $procedures_hospital['mec_vent_type'] !== 'mecanic'): echo 'display:none;';
        endif;
        ?>"> 
            <h6><label class="control-label">Tipo de ventilação mecânica</label></h6>
            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="mecanic_vent_type">
                <option value="none" selected> - </option>
                <option value="invasiv" <?php
                if (isset($procedures_hospital) && $procedures_hospital['mecanic_vent_type'] == 'invasiv'): echo 'selected';
                endif;
                ?>>Invasiva</option>
                <option value="n_invasiv" <?php
                if (isset($procedures_hospital) && $procedures_hospital['mecanic_vent_type'] == 'n_invasiv'): echo 'selected';
                endif;
                ?>>Não Invasiva</option>
            </select>
        </div>

        <div id="mecanic_vent_c_inv" style="margin-left: 40px; <?php
        if (!isset($procedures_hospital) || $procedures_hospital['mecanic_vent_type'] !== 'invasiv'): echo 'display:none;';
        endif;
        ?>"> 
            <h6><label class="control-label">Modo ventilatório</label></h6>
            <input id="inv_mec_vent_mode" type="text" class="form-control" value="<?php
            if (isset($procedures_hospital)): echo $procedures_hospital['inv_mec_vent_mode'];
            endif;
            ?>"/>

            <h6><label class="control-label">Volume</label></h6>
            <input id="inv_mec_vent_volume"  placeholder="ml/kg" maxlength="45" type="number" class="form-control" value="<?php
            if (isset($procedures_hospital)): echo $procedures_hospital['inv_mec_vent_volume'];
            endif;
            ?>"/>

            <h6><label class="control-label">FiO2</label></h6>
            <input id="inv_mec_vent_fio2"  placeholder="21-100%" min="21" max="100" type="number" class="form-control" value="<?php
            if (isset($procedures_hospital)): echo $procedures_hospital['inv_mec_vent_fio2'];
            endif;
            ?>"/>

            <h6><label class="control-label">PEEP</label></h6>
            <input id="inv_mec_vent_peep"  placeholder="5-60" min="5" max="60" type="number" class="form-control" value="<?php
            if (isset($procedures_hospital)): echo $procedures_hospital['inv_mec_vent_peep'];
            endif;
            ?>"/>

            <h6><label class="control-label">Freq. Resp.</label></h6>
            <input id="inv_mec_vent_freq_resp"  placeholder="5-60" min="5" max="60" type="number" class="form-control" value="<?php
            if (isset($procedures_hospital)): echo $procedures_hospital['inv_mec_vent_freq_resp'];
            endif;
            ?>"/>

            <h6><label class="control-label">Pressão lim.</label></h6>
            <input id="inv_mec_vent_press_lim"  placeholder="21-100'" min="21" max="100" type="number" class="form-control" value="<?php
            if (isset($procedures_hospital)): echo $procedures_hospital['inv_mec_vent_press_lim'];
            endif;
            ?>"/>
        </div>

        <div id="mecanic_vent_c_n_inv" style="margin-left: 40px; <?php
        if (!isset($procedures_hospital) || $procedures_hospital['mecanic_vent_type'] !== 'n_invasiv'): echo 'display:none;';
        endif;
        ?>"> 
            <h6><label class="control-label">IPAP</label></h6>
            <input id="n_inv_mec_vent_ipap" type="number" class="form-control" value="<?php
            if (isset($procedures_hospital)): echo $procedures_hospital['n_inv_mec_vent_ipap'];
            endif;
            ?>"/>

            <h6><label class="control-label">EPAP</label></h6>
            <input id="n_inv_mec_vent_epap" type="number" class="form-control" value="<?php
            if (isset($procedures_hospital)): echo $procedures_hospital['n_inv_mec_vent_epap'];
            endif;
            ?>"/>

            <h6><label class="control-label">FiO2</label></h6>
            <input id="n_inv_mec_vent_fio2" type="number" class="form-control" value="<?php
            if (isset($procedures_hospital)): echo $procedures_hospital['n_inv_mec_vent_fio2'];
            endif;
            ?>"/>

            <h6><label class="control-label">Freq. Resp.</label></h6>
            <input id="n_inv_mec_vent_freq_resp" type="number" class="form-control" value="<?php
            if (isset($procedures_hospital)): echo $procedures_hospital['n_inv_mec_vent_freq_resp'];
            endif;
            ?>"/>
        </div>

        <div class="checkbox">
            <label>
                <input type="checkbox" id="pun_pleu" name="group[]" class="show-div" data-target="pler-punc" <?php
                if (isset($procedures_hospital) && $procedures_hospital['pun_pleu'] == 1): echo 'checked';
                endif;
                ?>>  Punção pleural
            </label>
        </div>

        <div id="pler-punc" style="margin-left: 30px; <?php
        if (!isset($procedures_hospital) || $procedures_hospital['pun_pleu'] == 0): echo 'display:none;';
        endif;
        ?>">
            <div class="checkbox">
                <label>
                    <input type="checkbox" id="pun_pleu_l" <?php
                    if (isset($procedures_hospital) && $procedures_hospital['pun_pleu_l'] == 1): echo 'checked';
                    endif;
                    ?>>  Esquerda
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" id="pun_pleu_r" <?php
                    if (isset($procedures_hospital) && $procedures_hospital['pun_pleu_r'] == 1): echo 'checked';
                    endif;
                    ?>>  Direita
                </label>
            </div>
        </div>

        <div class="checkbox">
            <label>
                <input type="checkbox" id="pleu_dren" name="group[]" class="show-div" data-target="pler-dren" <?php
                if (isset($procedures_hospital) && $procedures_hospital['pleu_dren'] == 1): echo 'checked';
                endif;
                ?>>  Uso de dreno pleural
            </label>
        </div>

        <div id="pler-dren" style="margin-left: 30px; <?php
        if (!isset($procedures_hospital) || $procedures_hospital['pleu_dren'] == 0): echo 'display:none;';
        endif;
        ?>">
            <div class="checkbox">
                <label>
                    <input type="checkbox" id="pleu_dren_l" name="group[]" class="show-div" data-target="pler-dren-left" <?php
                    if (isset($procedures_hospital) && $procedures_hospital['pleu_dren_l'] == 1): echo 'checked';
                    endif;
                    ?>>  Esquerda
                </label>
            </div>
            <div id="pler-dren-left" style=" <?php
            if (!isset($procedures_hospital) || $procedures_hospital['pleu_dren_l'] == 0): echo 'display:none;';
            endif;
            ?>">
                <h6><label class="control-label">Quantidade (ml)</label></h6>
                <input id="pleu_dren_lv" type="text" class="form-control" value="<?php
                if (isset($procedures_hospital)): echo $procedures_hospital['pleu_dren_lv'];
                endif;
                ?>"/>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" id="pleu_dren_r" name="group[]" class="show-div" data-target="pler-dren-rig" <?php
                    if (isset($procedures_hospital) && $procedures_hospital['pleu_dren_r'] == 1): echo 'checked';
                    endif;
                    ?>>  Direita
                </label>
            </div>
            <div id="pler-dren-rig" style=" <?php
            if (!isset($procedures_hospital) || $procedures_hospital['pleu_dren_r'] == 0): echo 'display:none;';
            endif;
            ?>">
                <h6><label class="control-label">Quantidade (ml)</label></h6>
                <input id="pleu_dren_rv" type="text" class="form-control" value="<?php
                if (isset($procedures_hospital)): echo $procedures_hospital['pleu_dren_rv'];
                endif;
                ?>"/>
            </div>
        </div>
    </div>
</div>

<div class="row-fluid procedure_h">
    <div class="col-sm-1 pull-left">
        <a href="javascript:void(0)" onclick="jQuery('.procedure_h #ccontent').toggle();" class="btn btn-default">C</a>
    </div>
    <div id="ccontent" class="col-sm-11">
        <div class="checkbox">
            <label>
                <input type="checkbox" id="extern_hem" name="group[]" class="show-div" data-target="extern_hem_c" <?php
                if (isset($procedures_hospital) && $procedures_hospital['extern_hem'] == 1): echo 'checked';
                endif;
                ?>>  Presença de hemorragia externa
            </label>
        </div>
        <div id="extern_hem_c" style="margin-left: 30px; <?php
        if (!isset($procedures_hospital) || $procedures_hospital['extern_hem'] == 0): echo 'display:none;';
        endif;
        ?>">
            <div class="checkbox">
                <label>
                    <input type="checkbox" id="extern_hem_head_face" <?php
                    if (isset($procedures_hospital) && $procedures_hospital['extern_hem_head_face'] == 1): echo 'checked';
                    endif;
                    ?>>  Cabeça e face
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" id="extern_hem_neck" <?php
                    if (isset($procedures_hospital) && $procedures_hospital['extern_hem_neck'] == 1): echo 'checked';
                    endif;
                    ?>>  Pescoço
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" id="extern_hem_chest" <?php
                    if (isset($procedures_hospital) && $procedures_hospital['extern_hem_chest'] == 1): echo 'checked';
                    endif;
                    ?>>  Tórax
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" id="extern_hem_ab" <?php
                    if (isset($procedures_hospital) && $procedures_hospital['extern_hem_ab'] == 1): echo 'checked';
                    endif;
                    ?>>  Abdome
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" id="extern_hem_pelvper" <?php
                    if (isset($procedures_hospital) && $procedures_hospital['extern_hem_pelvper'] == 1): echo 'checked';
                    endif;
                    ?>>  Pelve/Períneo
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" id="extern_hem_mi" <?php
                    if (isset($procedures_hospital) && $procedures_hospital['extern_hem_mi'] == 1): echo 'checked';
                    endif;
                    ?>>  Membros inferiores
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" id="extern_hem_ms" <?php
                    if (isset($procedures_hospital) && $procedures_hospital['extern_hem_ms'] == 1): echo 'checked';
                    endif;
                    ?>>  Membros superiores
                </label>
            </div>
        </div>

        <div class="checkbox">
            <label>
                <input type="checkbox" id="intern_hem" name="group[]" class="show-div" data-target="intern_hem_c" <?php
                if (isset($procedures_hospital) && $procedures_hospital['intern_hem'] == 1): echo 'checked';
                endif;
                ?>>  Pesquisa de hemorragia interna
            </label>
        </div>
        <div id="intern_hem_c" style="margin-left: 30px; <?php
        if (!isset($procedures_hospital) || $procedures_hospital['intern_hem'] == 0): echo 'display:none;';
        endif;
        ?>">
            <div class="checkbox">
                <label>
                    <input type="checkbox" id="intern_hem_rx_sp_torax" <?php
                    if (isset($procedures_hospital) && $procedures_hospital['intern_hem_rx_sp_torax'] == 1): echo 'checked';
                    endif;
                    ?>>  Radiografia simples de tórax
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" id="intern_hem_rx_sp_pelve" <?php
                    if (isset($procedures_hospital) && $procedures_hospital['intern_hem_rx_sp_pelve'] == 1): echo 'checked';
                    endif;
                    ?>>  Radiografia simples de pelve
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" id="intern_hem_ux_fast" <?php
                    if (isset($procedures_hospital) && $procedures_hospital['intern_hem_ux_fast'] == 1): echo 'checked';
                    endif;
                    ?>>  Ultrasonografia FAST
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" id="intern_hem_lav_perit_diag" <?php
                    if (isset($procedures_hospital) && $procedures_hospital['intern_hem_lav_perit_diag'] == 1): echo 'checked';
                    endif;
                    ?>>  Lavado peritoneal diagnóstico
                </label>
            </div>
        </div>

        <div class="checkbox">
            <label>
                <input type="checkbox" id="vasc_access_per" name="group[]" class="show-div" data-target="vasc_access_per_c" <?php
                if (isset($procedures_hospital) && $procedures_hospital['vasc_access_per'] == 1): echo 'checked';
                endif;
                ?>>  Acesso vascular periférico
            </label>
        </div>
        <div id="vasc_access_per_c" style="margin-left: 30px;  <?php
        if (!isset($procedures_hospital) || $procedures_hospital['vasc_access_per'] == 0): echo 'display:none;';
        endif;
        ?>">
            <h6><label class="control-label">Tipo</label></h6>
            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" name="parent_selection" id="vasc_access_per_disptype">
                <option value="none" selected> - </option>
                <option value="ven_pun" <?php
                if (isset($procedures_hospital) && $procedures_hospital['vasc_access_per_disptype'] == 'ven_pun'): echo 'selected';
                endif;
                ?>>Punção venosa</option>
                <option value="intraoss_pun" <?php
                if (isset($procedures_hospital) && $procedures_hospital['vasc_access_per_disptype'] == 'intraoss_pun'): echo 'selected';
                endif;
                ?>>Punção intraóssea</option>
                <option value="dissec" <?php
                if (isset($procedures_hospital) && $procedures_hospital['vasc_access_per_disptype'] == 'dissec'): echo 'selected';
                endif;
                ?>>Dissecção</option>
            </select>

            <h6><label class="control-label">Região</label></h6>
            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" name="parent_selection" id="vasc_access_per_reg">
                <option value="none" selected> - </option>
                <option value="msd" <?php
                if (isset($procedures_hospital) && $procedures_hospital['vasc_access_per_reg'] == 'msd'): echo 'selected';
                endif;
                ?>>Membro superior direito</option>
                <option value="mse" <?php
                if (isset($procedures_hospital) && $procedures_hospital['vasc_access_per_reg'] == 'mse'): echo 'selected';
                endif;
                ?>>Membro superior esquerdo</option>
                <option value="mid" <?php
                if (isset($procedures_hospital) && $procedures_hospital['vasc_access_per_reg'] == 'mid'): echo 'selected';
                endif;
                ?>>Membro inferior direito</option>
                <option value="mie" <?php
                if (isset($procedures_hospital) && $procedures_hospital['vasc_access_per_reg'] == 'mie'): echo 'selected';
                endif;
                ?>>Membro inferior esquerdo</option>
            </select>
        </div>

        <div class="checkbox">
            <label>
                <input type="checkbox" id="volemic_infus" name="group[]" class="show-div" data-target="volemic_infus_c" <?php
                if (isset($procedures_hospital) && $procedures_hospital['volemic_infus'] == 1): echo 'checked';
                endif;
                ?>>  Infusão volêmica
            </label>
        </div>
        <div id="volemic_infus_c" style="margin-left: 30px; <?php
        if (!isset($procedures_hospital) || $procedures_hospital['volemic_infus'] == 0): echo 'display:none;';
        endif;
        ?>">
            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="aphm-regulation-ntrauma">
                <option value="ringer" <?php
                if (isset($procedures_hospital) && $procedures_hospital['volemic_infus_type'] == 'ringer'): echo 'selected';
                endif;
                ?>> Ringer lactato</option>
                <option value="soro" <?php
                if (isset($procedures_hospital) && $procedures_hospital['volemic_infus_type'] == 'soro'): echo 'selected';
                endif;
                ?>>Soro lactato</option>
            </select>

            <h6><label class="control-label">Quantidade (ml)</label></h6>
            <input id="volemic_infus_qt" type="text" class="form-control" value="<?php
            if (isset($procedures_hospital)): echo $procedures_hospital['volemic_infus_qt'];
            endif;
            ?>"/>

        </div>

        <div class="checkbox">
            <label>
                <input type="checkbox" id="ressus_cardio" name="group[]" class="show-div" data-target="cardiopulm-ressuc" <?php
                if (isset($procedures_hospital) && $procedures_hospital['ressus_cardio'] == 1): echo 'checked';
                endif;
                ?>>  Ressuscitação cardiopulmonar
            </label>
        </div>
        <div id="cardiopulm-ressuc" style="margin-left: 30px;  <?php
        if (!isset($procedures_hospital) || $procedures_hospital['ressus_cardio'] == 0): echo 'display:none;';
        endif;
        ?>">
            <h6><label class="control-label">Número de paradas cardíacas</label></h6>
            <input id="ressus_cardio_v" type="text" class="form-control" value="<?php
            if (isset($procedures_hospital)): echo $procedures_hospital['ressus_cardio_v'];
            endif;
            ?>"/>

            <h5><label class="control-label"><?= lang('duration') ?></label></h5>
            <input id="ressus_cardio_d" type="text" class="form-control" value="<?php
            if (isset($procedures_hospital)): echo $procedures_hospital['ressus_cardio_d'];
            endif;
            ?>"/> 
        </div>
    </div>
</div>
    <div class="row-fluid procedure_h">
        <div class="col-sm-1 pull-left">
            <a href="javascript:void(0)" onclick="jQuery('.procedure_h #dcontent').toggle();" class="btn btn-default">D</a>
        </div>
        <div id="dcontent" class="col-sm-11">
            <div class="checkbox">
                <label>
                    <input type="checkbox" id="board_imo" <?php
                    if (isset($procedures_hospital) && $procedures_hospital['board_imo'] == 1): echo 'checked';
                    endif;
                    ?>>  Imobilização em prancha longa
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" id="memb_imob" name="group[]" class="show-div" data-target="mem_imob_c" <?php
                    if (isset($procedures_hospital) && $procedures_hospital['memb_imob'] == 1): echo 'checked';
                    endif;
                    ?>>  Imobilização dos membros
                </label>
            </div>
            <div id="mem_imob_c" style="margin-left: 30px; <?php
            if (!isset($procedures_hospital) || $procedures_hospital['memb_imob'] == 0): echo 'display:none;';
            endif;
            ?>">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" id="memb_imob_sd" <?php
                        if (isset($procedures_hospital) && $procedures_hospital['memb_imob_sd'] == 1): echo 'checked';
                        endif;
                        ?>>  Membro superior direito
                    </label>
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" id="memb_imob_se" <?php
                        if (isset($procedures_hospital) && $procedures_hospital['memb_imob_se'] == 1): echo 'checked';
                        endif;
                        ?>>  Membro superior esquerdo
                    </label>
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" id="memb_imob_id" <?php
                        if (isset($procedures_hospital) && $procedures_hospital['memb_imob_id'] == 1): echo 'checked';
                        endif;
                        ?>>  Membro inferior direito
                    </label>
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" id="memb_imob_ie" <?php
                        if (isset($procedures_hospital) && $procedures_hospital['memb_imob_ie'] == 1): echo 'checked';
                        endif;
                        ?>>  Membro inferior esquerdo
                    </label>
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" id="memb_imob_pelv" <?php
                        if (isset($procedures_hospital) && $procedures_hospital['memb_imob_pelv'] == 1): echo 'checked';
                        endif;
                        ?>>  Pelve
                    </label>
                </div>
            </div>
        </div>
    </div>

</br>
<script>
		jQuery('input.show-div').on('change', function () {
            var source = $(this);
            var target = $('#' + source.attr('data-target'));
            if ($('input[data-target=' + source.attr('data-target') + ']:checked').length)
                target.show();
            else
                target.hide();
        });

        jQuery('#mec_vent_type').change(function () {
            if (jQuery('#mec_vent_type').val() == 'mecanic') {
                jQuery('#mecanic_vent_c').css('display', 'block');
            } else {
                jQuery('#mecanic_vent_c').css('display', 'none');
            }
        })

        jQuery('#mecanic_vent_type').change(function () {
            if (jQuery('#mecanic_vent_type').val() == 'invasiv') {
                jQuery('#mecanic_vent_c_inv').css('display', 'block');
                jQuery('#mecanic_vent_c_n_inv').css('display', 'none');
            } else if (jQuery('#mecanic_vent_type').val() == 'n_invasiv') {
                jQuery('#mecanic_vent_c_inv').css('display', 'none');
                jQuery('#mecanic_vent_c_n_inv').css('display', 'block');
            } else {
                jQuery('#mecanic_vent_c_inv').css('display', 'none');
                jQuery('#mecanic_vent_c_n_inv').css('display', 'none');
            }
        })
        function togglePanels(content, options) {
            jQuery(content).toggle();
            jQuery(content + '_chev').toggle();
            jQuery(content + '_chevback').toggle();
            jQuery(options).toggle();
        }
</script>