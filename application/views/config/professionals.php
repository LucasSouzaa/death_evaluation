<script type="text/javascript" src="<?= $this->config->base_url(JSPATH . "config.js") ?>"></script>

<!--DADOS DO PACIENTE-->
<div class="add-divs" id="div-professional-data" hidden="" style=" padding: 15px;">
    <div class="search-filter">
        <ul>
            <h1><?= lang('add') . ' / ' . lang('edit') . ' ' . lang('professional') ?></h1>
        </ul>
    </div>
    <section class="panel">
        <div class="content">
            <form class="form-horizontal group-border-dashed" id="professional_form" action="#">

                <div class="form-group">
                    <h3><label class="col-sm-2 control-label"><?= lang('personal_info') ?></label></h3>
                </div>

                <div class="form-group">
                    <h4><label class="col-sm-2 control-label"><?= lang('name') ?></label></h4>
                    <div class="col-sm-4">
                        <input id="name" type="text" class="form-control"/> 
                    </div>

                    <h4><label class="col-sm-2 control-label"><?= lang('sus_card') ?></label></h4>
                    <div class="col-sm-4">
                        <input id="sus_card" type="text" class="form-control"/> 
                    </div>
                </div>

                <div class="form-group">
                    <h4><label class="col-sm-2 control-label"><?= lang('birthdate') ?></label></h4>
                    <div class="col-sm-4">
                        <input id="birthdate" type="date" class="form-control"/> 
                    </div>

                    <h4><label class="col-sm-2 control-label"><?= lang('gender') ?></label></h4>
                    <div class="col-sm-4">
                        <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="gender">
                            <option value="m" selected><?= lang('male') ?></option>
                            <option value="f"><?= lang('female') ?></option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <h4><label class="col-sm-2 control-label"><?= lang('skin_color') ?></label></h4>
                    <div class="col-sm-4">
                        <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="skin_color">
                            <option value="white" selected><?= lang('white') ?></option>
                            <option value="black"><?= lang('black') ?></option>
                            <option value="brown"><?= lang('brown') ?></option>
                            <option value="mulattos"><?= lang('mulattos') ?></option>
                            <option value="caboclos"><?= lang('caboclos') ?></option>
                            <option value="cafuzos"><?= lang('cafuzos') ?></option>
                            <option value="na"><?= lang('na') ?></option>
                        </select>
                    </div>

                    <h4><label class="col-sm-2 control-label"><?= lang('mother_name') ?></label></h4>
                    <div class="col-sm-4">
                        <input id="mother_name" type="text" class="form-control"/> 
                    </div>
                </div>
                <div class="form-group">
                    <h4><label class="col-sm-2 control-label"><?= lang('marital_status') ?></label></h4>
                    <div class="col-sm-4">
                        <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="marital_status">
                            <option value="single" selected><?= lang('single') ?></option>
                            <option value="married"><?= lang('married') ?></option>
                            <option value="widower"><?= lang('widower') ?></option>
                            <option value="other"><?= lang('other') ?></option>
                        </select>
                    </div>

                    <h4><label class="col-sm-2 control-label"><?= lang('nationality') ?></label></h4>
                    <div class="col-sm-4">
                        <input id="nationality" type="text" class="form-control"/> 
                    </div>
                </div>

                <div class="form-group">
                    <h4><label class="col-sm-2 control-label"><?= lang('home_town_state') ?></label></h4>
                    <div class="col-sm-4">
                        <input id="home_town_state" type="text" class="form-control" maxlength="2"/> 
                    </div>

                    <h4><label class="col-sm-2 control-label"><?= lang('home_town') ?></label></h4>
                    <div class="col-sm-4">
                        <input id="home_town" type="text" class="form-control"/> 
                    </div>
                </div>

                <div class="form-group">
                    <h4><label class="col-sm-2 control-label"><?= lang('status') ?></label></h4>
                    <div class="col-sm-4">
                        <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="status">
                            <option value="1" selected><?= lang('activated') ?></option>
                            <option value="0"><?= lang('inactivated') ?></option>
                            <option value="2"><?= lang('deleted') ?></option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <h3><label class="col-sm-2 control-label"><?= lang('professional_info') ?></label></h3>
                </div>

                <div class="form-group">
                    <h4><label class="col-sm-2 control-label"><?= lang('teams') ?></label></h4>
                    <div class="col-sm-4">
                        <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="team_id">
                            <?php foreach ($teams as $t): ?>
                                <option value="<?= $t['id'] ?>" selected><?= $t['name'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>  

                <div class="form-group">
                    <h4><label class="col-sm-2 control-label"><?= lang('specialty') ?></label></h4>
                    <div class="col-sm-4">
                        <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="specialty">
                            <option value="neurology" selected><?= lang('neurology') ?></option>
                            <option value="surgery" ><?= lang('surgery') ?></option>
                            <option value="orthopedics" ><?= lang('orthopedics') ?></option>
                            <option value="head_and_neck" ><?= lang('head_and_neck') ?></option>
                            <option value="neurosurgery" ><?= lang('neurosurgery') ?></option>
                            <option value="ophthalmology" ><?= lang('ophthalmology') ?></option>
                            <option value="otorhinolaringology" ><?= lang('otorhinolaringology') ?></option>
                            <option value="pediatrics" ><?= lang('pediatrics') ?></option>
                            <option value="burned" ><?= lang('burned') ?></option>
                            <option value="chest" ><?= lang('chest') ?></option>
                            <option value="urology" ><?= lang('urology') ?></option>
                            <option value="vascular" ><?= lang('vascular') ?></option>
                        </select>
                    </div>

                    <h4><label class="col-sm-2 control-label"><?= lang('hierarchy') ?></label></h4>
                    <div class="col-sm-4">
                        <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="hierarchy">
                            <option value="r1" selected><?= lang('r1') ?></option>
                            <option value="r2" ><?= lang('r2') ?></option>
                            <option value="r3" ><?= lang('r3') ?></option>
                            <option value="attending_physician" ><?= lang('attending_physician') ?></option>
                            <option value="teaching_medical" ><?= lang('teaching_medical') ?></option>
                            <option value="nurse" ><?= lang('nurse') ?></option>
                            <option value="nursing_technician" selected><?= lang('nursing_technician') ?></option>
                        </select>
                    </div>
                </div>  

                <div class="form-group">
                    <h4><label class="col-sm-2 control-label"><?= lang('class_document_type') ?></label></h4>
                    <div class="col-sm-4">
                        <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="class_document_type">
                            <option value="crm" selected><?= lang('crm') ?></option>
                            <option value="coren" ><?= lang('coren') ?></option>
                        </select>
                    </div>

                    <h4><label class="col-sm-2 control-label"><?= lang('class_document_number') ?></label></h4>
                    <div class="col-sm-4">
                        <input id="class_document_number" type="text" class="form-control"/> 
                    </div>
                </div>

                <div class="form-group">
                    <h3><label class="col-sm-2 control-label"><?= lang('access_info') ?></label></h3>
                </div>

                <div class="form-group">
                    <h4><label class="col-sm-2 control-label"><?= lang('username') ?></label></h4>
                    <div class="col-sm-4">
                        <input id="username" type="text" class="form-control"/> 
                    </div>

                    <h4><label class="col-sm-2 control-label"><?= lang('email') ?></label></h4>
                    <div class="col-sm-4">
                        <input id="email" type="text" class="form-control"/> 
                    </div>
                </div>

                <div class="form-group">
                    <h4><label class="col-sm-2 control-label"><?= lang('password') ?></label></h4>
                    <div class="col-sm-4">
                        <input id="password" type="password" class="form-control"/> 
                    </div>

                    <h4><label class="col-sm-2 control-label"><?= lang('repeat_password') ?></label></h4>
                    <div class="col-sm-4">
                        <input id="repeat_password" type="password" class="form-control"/> 
                    </div>
                </div>

                <input id="professional_id" value="" hidden="" />
                <input id="person_id" value="" hidden="" />
                <input id="user_id" value="" hidden="" />
            </form>
        </div>
        <div class="form-group">
            <div class="col-sm-12" style="text-align: center">
                <a onclick="save_professional()" class="btn btn-success"><?= lang('save') ?></a>
                <a onclick="jQuery('#div-professional-data').toggle()" class="btn btn-danger"><?= lang('close') ?></a>
            </div>
        </div>
    </section>
    </br>
</div>


<div class="padding-md">
    <div class="search-filter">
        <a href="javascript:void(0)" class="btn btn-primary" onclick=" jQuery('input').val('');
                jQuery('#professional_id').val('');
                jQuery('#person_id').val('');
                jQuery('#user_id').val('');
                jQuery('#div-professional-data').show();" style="float: right"> <i class="fa fa-plus-square"></i> Adicionar</a>
        <ul>
            <h1><?= lang('professionals') ?></h1>  
        </ul>
    </div>

    <section class="panel">
        <table class="table table-bordered table-striped mb-none" id="datatable-tabletools" data-swf-path="<?= $this->config->base_url(VENDORPATH . 'jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf') ?>">
            <thead>
                <tr>
                    <th><?= lang('name') ?></th>
                    <th><?= lang('team') ?></th>
                    <th><?= lang('specialty') ?></th>
                    <th><?= lang('class_document_type') ?></th>
                    <th><?= lang('class_document_number') ?></th>
                    <th></th>
                </tr>
            </thead>
            <?php if (!empty($professionals)): ?>
                <tbody>
                    <?php foreach ($professionals as $p): ?>
                        <tr>
                            <td>
                                <a href="javascript:void(0)" onclick="view_professional(<?= $p['id'] ?>)" style="display: block"><?= $p['name'] ?></a>
                            </td>
                            <td><?= $p['team'] ?></td>
                            <td><?= lang($p['specialty']) ?></td>
                            <td><?= lang($p['class_document_type']) ?></td>
                            <td><?= $p['class_document_number'] ?></td>
                            <td style="text-align: -webkit-center;">
                                <a href="javascript:void(0)" onclick="delete_professional(<?= $p['id'] ?>)" style="margin-left: 5px;"><i class="fa fa-trash-o"></i></a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            <?php endif; ?>
        </table>
    </section>
</div>