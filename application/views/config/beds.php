<script type="text/javascript" src="<?= $this->config->base_url(JSPATH . "config.js") ?>"></script>

<!--DADOS DO PACIENTE-->
<div class="add-divs" id="div-bed-data" hidden="" style=" padding: 15px;">
    <div class="search-filter">
        <ul>
            <h1><?= lang('add') . ' / ' . lang('edit') . ' ' . lang('bed') ?></h1>
        </ul>
    </div>
    <section class="panel">
        <div class="content">
            <form class="form-horizontal group-border-dashed" id="bed_form" action="#">
                <div class="form-group">
                    <h4><label class="col-sm-2 control-label"><?= lang('bedrooms') ?></label></h4>
                    <div class="col-sm-4">
                        <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="bedroom_id">
                            <?php foreach ($bedrooms as $b): ?>
                                <option value="<?= $b['id'] ?>" selected><?= $b['name'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>

                    <h4><label class="col-sm-2 control-label"><?= lang('name') ?></label></h4>
                    <div class="col-sm-4">
                        <input id="name" type="text" class="form-control"/> 
                    </div>
                </div>


                <div class="form-group">
                    <h4><label class="col-sm-2 control-label"><?= lang('complement') ?></label></h4>
                    <div class="col-sm-4">
                        <input id="complement" type="text" class="form-control"/> 
                    </div>

                    <h4><label class="col-sm-2 control-label"><?= lang('status') ?></label></h4>
                    <div class="col-sm-4">
                        <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="status">
                            <option value="1" selected><?= lang('activated') ?></option>
                            <option value="0"><?= lang('inactivated') ?></option>
                            <option value="2"><?= lang('deleted') ?></option>
                        </select>
                    </div>
                </div>

                <input id="bed_id" value="" hidden="" />
            </form>
        </div>
        <div class="form-group">
            <div class="col-sm-12" style="text-align: center">
                <a onclick="save_bed()" class="btn btn-success"><?= lang('save') ?></a>
                <a onclick="jQuery('#div-bed-data').toggle()" class="btn btn-danger"><?= lang('close') ?></a>
            </div>
        </div>
    </section>
    </br>
</div>


<div class="padding-md">
    <div class="search-filter">
        <a href="javascript:void(0)" class="btn btn-primary" onclick=" jQuery('input').val('');
                jQuery('#bed_id').val('');
                jQuery('#div-bed-data').show();" style="float: right"> <i class="fa fa-plus-square"></i> Adicionar</a>
        <ul>
            <h1><?= lang('beds') ?></h1>  
        </ul>
    </div>

    <section class="panel">
        <table class="table table-bordered table-striped mb-none" id="datatable-tabletools" data-swf-path="<?= $this->config->base_url(VENDORPATH . 'jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf') ?>">
            <thead>
                <tr>
                    <th><?= lang('name') ?></th>
                    <th><?= lang('bedroom') ?></th>
                    <th><?= lang('sector') ?></th>
                    <th></th>
                </tr>
            </thead>
            <?php if (!empty($beds)): ?>
                <tbody>
                    <?php foreach ($beds as $s): ?>
                        <tr>
                            <td>
                                <a href="javascript:void(0)" onclick="view_bed(<?= $s['id'] ?>)" style="display: block"><?= $s['name'] ?></a>
                            </td>
                            <td><?= $s['bedroom'] ?></td>
                            <td><?= $s['sector'] ?></td>
                            <td style="text-align: -webkit-center;">
                                <a href="javascript:void(0)" onclick="delete_bed(<?= $s['id'] ?>)" style="margin-left: 5px;"><i class="fa fa-trash-o"></i></a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            <?php endif; ?>
        </table>
    </section>
</div>