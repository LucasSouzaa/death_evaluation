<script type="text/javascript" src="<?= $this->config->base_url(JSPATH . "config.js") ?>"></script>

<!--DADOS DO PACIENTE-->
<div class="add-divs" id="div-institution-data" hidden="" style=" padding: 15px;">

    <div class="search-filter">
        <ul>
            <h1><?= lang('add') . ' / ' . lang('edit') . ' ' . lang('institution') ?></h1>
        </ul>
    </div>

    <section class="panel">
        <div class="content">
            <form class="form-horizontal group-border-dashed" id="institution_form" action="#">
                <div class="form-group">
                    <h4><label class="col-sm-2 control-label"><?= lang('identification_code') ?></label></h4>
                    <div class="col-sm-4">
                        <input id="identification_code" type="text" class="form-control"/> 
                    </div>

                    <h4><label class="col-sm-2 control-label"><?= lang('name') ?></label></h4>
                    <div class="col-sm-4">
                        <input id="name" type="text" class="form-control"/> 
                    </div>
                </div>

                <div class="form-group">
                    <h4><label class="col-sm-2 control-label"><?= lang('cnae_code') ?></label></h4>
                    <div class="col-sm-4">
                        <input id="cnae_code" type="text" class="form-control"/> 
                    </div>

                    <h4><label class="col-sm-2 control-label"><?= lang('ibge_code') ?></label></h4>
                    <div class="col-sm-4">
                        <input id="ibge_code" type="text" class="form-control"/> 
                    </div>
                </div>

                <div class="form-group">
                    <h4><label class="col-sm-2 control-label"><?= lang('cnpj') ?></label></h4>
                    <div class="col-sm-4">
                        <input id="cnpj" type="text" class="form-control"/> 
                    </div>

                    <h4><label class="col-sm-2 control-label"><?= lang('ie') ?></label></h4>
                    <div class="col-sm-4">
                        <input id="ie" type="text" class="form-control"/> 
                    </div>
                </div>

                <div class="form-group">
                    <h4><label class="col-sm-2 control-label"><?= lang('city') ?></label></h4>
                    <div class="col-sm-4">
                        <input id="city" type="text" class="form-control"/> 
                    </div>

                    <h4><label class="col-sm-2 control-label"><?= lang('state') ?></label></h4>
                    <div class="col-sm-4">
                        <input id="state" type="text" class="form-control"/> 
                    </div>
                </div>

                <div class="form-group">
                    <h4><label class="col-sm-2 control-label"><?= lang('neighborhood') ?></label></h4>
                    <div class="col-sm-4">
                        <input id="neighborhood" type="text" class="form-control"/> 
                    </div>

                    <h4><label class="col-sm-2 control-label"><?= lang('zipcode') ?></label></h4>
                    <div class="col-sm-4">
                        <input id="zipcode" type="text" class="form-control"/> 
                    </div>
                </div>

                <div class="form-group">
                    <h4><label class="col-sm-2 control-label"><?= lang('street') ?></label></h4>
                    <div class="col-sm-4">
                        <input id="street" type="text" class="form-control"/> 
                    </div>

                    <h4><label class="col-sm-2 control-label"><?= lang('number') ?></label></h4>
                    <div class="col-sm-4">
                        <input id="number" type="number" class="form-control" /> 
                    </div>
                </div>

                <div class="form-group">
                    <h4><label class="col-sm-2 control-label"><?= lang('complement') ?></label></h4>
                    <div class="col-sm-4">
                        <input id="complement" type="text" class="form-control"/> 
                    </div>

                    <h4><label class="col-sm-2 control-label"><?= lang('status') ?></label></h4>
                    <div class="col-sm-4">
                        <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="status">
                            <option value="1" selected><?= lang('activated') ?></option>
                            <option value="0"><?= lang('inactivated') ?></option>
                            <option value="2"><?= lang('deleted') ?></option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <h4><label class="col-sm-2 control-label"><?= lang('type') ?></label></h4>
                    <div class="col-sm-4">
                        <input id="type" type="text" class="form-control"/> 
                    </div>
                </div>

                <input id="institution_id" value="" hidden="" />
            </form>
        </div>
        <div class="form-group">
            <div class="col-sm-12" style="text-align: center">
                <a onclick="save_institution()" class="btn btn-success"><?= lang('save') ?></a>
                <a onclick="jQuery('#div-institution-data').toggle()" class="btn btn-danger"><?= lang('close') ?></a>
            </div>
        </div>
    </section>
    </br>
</div>


<div class="padding-md">
    <div class="search-filter">
        <a href="javascript:void(0)" class="btn btn-primary" onclick=" jQuery('input').val('');
                jQuery('#institution_id').val('');
                jQuery('#div-institution-data').show();" style="float: right"> <i class="fa fa-plus-square"></i> Adicionar</a>
        <ul>
            <h1><?= lang('institutions') ?></h1>
        </ul>
    </div>

    <section class="panel">
        <table class="table table-bordered table-striped mb-none" id="datatable-tabletools" data-swf-path="<?= $this->config->base_url(VENDORPATH . 'jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf') ?>">
            <thead>
                <tr>
                    <th><?= lang('identification_code') ?></th>
                    <th><?= lang('name') ?></th>
                    <th><?= lang('type') ?></th>
                    <th><?= lang('localization') ?></th>
                    <th></th>
                </tr>
            </thead>
            <?php if (isset($institutions)): ?>
                <tbody>
                    <?php foreach ($institutions as $i): ?>
                        <tr>
                            <td>
                                <a href="javascript:void(0)" onclick="view_institution(<?= $i['id'] ?>)" style="display: block"><?= $i['identification_code'] ?></a>
                            </td>
                            <td>
                                <a href="javascript:void(0)" onclick="view_institution(<?= $i['id'] ?>)" style="display: block"><?= $i['name'] ?></a>
                            </td>
                            <td><?= $i['type'] ?></td>
                            <td><?= $i['city'] . '-' . $i['state'] ?></td>
                            <td style="text-align: -webkit-center;">
                                <a href="javascript:void(0)" onclick="delete_institution(<?= $i['id'] ?>)" style="margin-left: 5px;"><i class="fa fa-trash-o"></i></a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            <?php endif; ?>
        </table>
    </section>
</div>