<div class="col-md-2"></div>
<div class="container-fluid well col-md-8">

    <h3>Dados de Acesso</h3>
    <div class="row-fluid" style="padding-top: 10px">
        <div class="col-md-4">
            <div class="control-group">
                <label class="control-label">Email</label>
                <div class="controls">
                    <input class="form-control" name="email" id="email" style="width:100%" type="text" value="">
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="control-group">
                <label class="control-label">Usuário</label>
                <div class="controls">
                    <input class="form-control" name="username" id="username" type="text" value="">
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="control-group">
                <label class="control-label">Senha</label>
                <div class='controls'>
                    <input type='password' class="form-control" name="password" id="password" />
                </div>
            </div>	
        </div>
    </div>
    
    <div class="row-fluid">
                <div class="col-sm-12" style="text-align: center">
                    <a onclick="save_patient()" class="btn btn-success"><?= lang('save') ?></a>
                </div>
            </div>
    
    
    

</div>
<div class="col-md-2"></div>