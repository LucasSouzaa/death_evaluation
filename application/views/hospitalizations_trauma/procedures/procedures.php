<script type="text/javascript" src="<?= $this->config->base_url(JSPATH . "jquery.dataTables.min.js") ?>"></script>
<script type="text/javascript" src="<?= $this->config->base_url(JSPATH . "dataTables.bootstrap.js") ?>"></script>

<div class="padding-md">
    <div class="search-filter">
        <a class="mb-xs mt-xs mr-xs modal-basic" href="#modalAddGeneralProcedure" style="padding: 12px; float: left"> <i class="fa fa-plus-square-o fa-2x"></i></a>
        <ul>
            <h1><?= lang('procedures') ?> <?= lang('general') ?></h1>
        </ul>
    </div>

    <section class="panel">
        <div class="panel-body">
            <table id="general-procedures-table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th><?= lang('procedure') ?></th>
                        <th><?= lang('executor') ?></th>
                        <th><?= lang('responsible') ?></th>
                        <th><?= lang('datetime_installation') ?></th>
                        <th><?= lang('datetime_withdrawal') ?></th>
                    </tr>
                </thead>
                <?php if (isset($general_procedures)): ?>
                    <tbody>
                        <?php foreach ($general_procedures as $gp): ?>
                            <tr>
                                <td><?= $gp['procedure'] ?></td>
                                <td><?= $gp['executor'] ?></td>
                                <td><?= $gp['responsible'] ?></td>
                                <td><?= $gp['datetime_installation'] ?></td>
                                <td><?= $gp['datetime_withdrawal'] ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                <?php endif; ?>
            </table>
        </div>
    </section>
</div>

<div class="padding-md">
    <div class="search-filter">
        <a class="mb-xs mt-xs mr-xs modal-basic" href="#modalAddHemodialysis" style="padding: 12px; float: left"> <i class="fa fa-plus-square-o fa-2x"></i></a>
        <ul>
            <h1><?= lang('hemodialysis') ?></h1>
        </ul>
    </div>

    <section class="panel">
        <div class="panel-body">
            <table id="hemodialysis-procedures-table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th><?= lang('type') ?></th>
                        <th><?= lang('volume') ?></th>
                        <th><?= lang('region') ?></th>
                        <th><?= lang('pcr') ?></th>
                        <th><?= lang('reversion_time') ?></th>
                    </tr>
                </thead>
                <?php if (isset($hemodialysis_procedures)): ?>
                    <tbody>
                        <?php foreach ($hemodialysis_procedures as $hp): ?>
                            <tr>
                                <td><?= $hp['type'] ?></td>
                                <td><?= $hp['volume'] ?></td>
                                <td><?= $hp['region'] ?></td>
                                <td><?= $hp['pcr'] ?></td>
                                <td><?= $hp['reversion_time'] ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                <?php endif; ?>
            </table>
        </div>
    </section>
</div>


<div id="modalAddGeneralProcedure" class="modal-block mfp-hide">
    <section class="panel">
        <header class="panel-heading">
            <h2 class="panel-title"><?= lang('add_general_procedure') ?></h2>
        </header>
        <div class="panel-body">
            <div class="modal-wrapper">
                <div class="form-group">
                    <label class="control-label"><?= lang('procedure_type') ?> </label>
                    <select id="general_procedure_type" class="form-control selectpicker" style=" width: 100%">

                        <optgroup label="<?= $procedures[0]['type'] ?>">
                            <?php $type = $procedures[0]['type']; ?>
                            <?php
                            for ($i = 0; $i < count($procedures); $i++):
                                ?>
                                <?php if ($procedures[$i]['type'] !== $type): ?>
                                </optgroup>
                                <optgroup label="<?= $procedures[$i]['type'] ?>">
                                <?php endif; ?>
                                <?php $type = $procedures[$i]['type']; ?>
                                <option value="<?= $procedures[$i]['id'] ?>"><?= $procedures[$i]['display_name'] ?></option>
                            <?php endfor; ?>
                        </optgroup>
                    </select>
                </div>	
                <div class="form-group">   
                    <div class="col-md-6">
                        <label class="control-label"><?= lang('datetime_installation') ?> </label>
                        <div class="input-group date datetime" data-date-format="dd-mm-yyyy - HH:ii">
                            <input class="form-control" type="text" value="" readonly id="datetime_installation_procedure">
                            <span class="input-group-addon btn btn-primary"><span class="glyphicon glyphicon-th"></span></span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label class="control-label"><?= lang('datetime_withdrawal') ?> </label>
                        <div class="input-group date datetime" data-date-format="dd-mm-yyyy - HH:ii">
                            <input class="form-control" type="text" value="" readonly id="datetime_withdrawal_procedures">
                            <span class="input-group-addon btn btn-primary"><span class="glyphicon glyphicon-th"></span></span>
                        </div>
                    </div>
                </div>	
                <div class="form-group">   
                    <div class="col-md-6">
                        <label class="control-label"><?= lang('datetime_installation') ?> </label>
                        <div class="input-group date datetime" data-date-format="dd-mm-yyyy - HH:ii">
                            <input class="form-control" type="text" value="" readonly id="datetime_installation_procedure">
                            <span class="input-group-addon btn btn-primary"><span class="glyphicon glyphicon-th"></span></span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label class="control-label"><?= lang('datetime_withdrawal') ?> </label>
                        <div class="input-group date datetime" data-date-format="dd-mm-yyyy - HH:ii">
                            <input class="form-control" type="text" value="" readonly id="datetime_withdrawal_procedures">
                            <span class="input-group-addon btn btn-primary"><span class="glyphicon glyphicon-th"></span></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer class="panel-footer">
            <div class="row">
                <div class="col-md-12 text-right">
                    <button class="btn btn-primary modal-confirm" onclick="save_general_procedure()"><?= lang('save') ?></button>
                    <button class="btn btn-default modal-dismiss"><?= lang('close') ?></button>
                </div>
            </div>
        </footer>
    </section>
</div>

<div id="modalAddHemodialysis" class="modal-block mfp-hide">
    <section class="panel">
        <header class="panel-heading">
            <h2 class="panel-title"><?= lang('add_hemodialysis') ?></h2>
        </header>
        <div class="panel-body">
            <div class="modal-wrapper">
                <div class="form-group">
                    <label class="control-label"><?= lang('procedure_type') ?> </label>
                    <select id="hemodialysis_type" class="form-control selectpicker" style=" width: 100%">
                        <optgroup label="<?= $procedures[0]['type'] ?>">
                            <?php $type = $procedures[0]['type']; ?>
                            <?php
                            for ($i = 0; $i < count($procedures); $i++):
                                ?>
                                <?php if ($procedures[$i]['type'] !== $type): ?>
                                </optgroup>
                                <optgroup label="<?= $procedures[$i]['type'] ?>">
                                <?php endif; ?>
                                <?php $type = $procedures[$i]['type']; ?>
                                <option value="<?= $procedures[$i]['id'] ?>"><?= $procedures[$i]['display_name'] ?></option>
                            <?php endfor; ?>
                        </optgroup>
                    </select>
                </div>	
                <div class="form-group">   
                    <div class="col-md-6">
                        <label class="control-label"><?= lang('volume') ?> </label>
                        <div class="input-group" >
                            <input class="form-control" type="text" value="" id="volume">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label class="control-label"><?= lang('region') ?> </label>
                        <div class="input-group" >
                            <input class="form-control" type="text" value="" id="region">
                        </div>
                    </div>
                </div>	
                <div class="form-group">   
                    <div class="col-md-6">
                        <label class="control-label"><?= lang('pcr') ?> </label>
                        <div class="input-group" >
                            <input class="form-control" type="text" value="" id="pcr">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label class="control-label"><?= lang('reversion_time') ?> </label>
                        <div class="input-group" >
                            <input class="form-control" type="text" value="" id="reversion_time">
                        </div>
                    </div>
                </div>	
               
            </div>
        </div>
        <footer class="panel-footer">
            <div class="row">
                <div class="col-md-12 text-right">
                    <button class="btn btn-primary modal-confirm" onclick="save_hemodialysis()"><?= lang('save') ?></button>
                    <button class="btn btn-default modal-dismiss"><?= lang('close') ?></button>
                </div>
            </div>
        </footer>
    </section>
</div>

<script>
    jQuery('#MainMenu div a').removeClass('list-group-item-success');
    jQuery('#procedures-layout').addClass('list-group-item-success');

    jQuery(document).ready(function (){
        jQuery('#general-procedures-table').dataTable({
            "language": {
                "lengthMenu": "Exibindo _MENU_ registros por página",
                "zeroRecords": "Nenhum registro encontrado",
                "info": "Exibindo página _PAGE_ de _PAGES_",
                "infoEmpty": "Tabela vazia",
                "infoFiltered": "(Filtrado de _MAX_ registros)",
                "sSearchPlaceholder": "Buscar ",
                "sSearch": "",
                "oPaginate": {
                    "sFirst": "Primeira",
                    "sLast": "Última",
                    "sNext": "Próxima",
                    "sPrevious": "Anterior"
                }
            }
        });
        
        jQuery('#hemodialysis-procedures-table').dataTable({
            "language": {
                "lengthMenu": "Exibindo _MENU_ registros por página",
                "zeroRecords": "Nenhum registro encontrado",
                "info": "Exibindo página _PAGE_ de _PAGES_",
                "infoEmpty": "Tabela vazia",
                "infoFiltered": "(Filtrado de _MAX_ registros)",
                "sSearchPlaceholder": "Buscar ",
                "sSearch": "",
                "oPaginate": {
                    "sFirst": "Primeira",
                    "sLast": "Última",
                    "sNext": "Próxima",
                    "sPrevious": "Anterior"
                }
            }
        });
    });

</script>

<script src="<?= $this->config->base_url(JSPATH . 'examples.modals.js') ?>"></script>