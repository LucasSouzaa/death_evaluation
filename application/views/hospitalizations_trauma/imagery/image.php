<link rel="stylesheet" href="<?= $this->config->base_url(PLUGINPATH . 'dropzone/css/basic.css') ?>" />		
<link rel="stylesheet" href="<?= $this->config->base_url(PLUGINPATH . 'dropzone/css/dropzone.css') ?>" />		
<script type="text/javascript" src="<?= $this->config->base_url(JSPATH . "jquery.dataTables.min.js") ?>"></script>
<script type="text/javascript" src="<?= $this->config->base_url(JSPATH . "dataTables.bootstrap.js") ?>"></script>
<script type="text/javascript" src="<?= $this->config->base_url(JSPATH . "imagery.js") ?>"></script>
<!--ADICIONANDO IMAGEM-->
<div class="add-divs" id="div-image" hidden="" style=" padding: 15px;">
    <section class="panel">
        <header class="panel-heading">

            <h2 class="panel-title"><?= lang('add') ?> <?= lang('image') ?></h2>
            <p class="panel-subtitle">
            </p>
        </header>
        <div class="panel-body">
            <form action="<?= $this->config->base_url('imagery/receive') ?>" class="dropzone dz-square" id="dropzone-image"></form>
        </div>
    </section>
    </br>
</div>

<div class="padding-md">
    <div class="search-filter">
        <a href="javascript:void(0)" onclick="jQuery('#div-image').toggle()" style="padding: 12px; float: left"> <i class="fa fa-plus-square-o fa-2x"></i></a>
        <a class="mb-xs mt-xs mr-xs modal-basic" hidden="" id="modalShowImageButton" href="#modalShowImage" style="padding: 12px; float: left"> <i class="fa fa-plus-square-o fa-2x"></i></a>
        <ul>
            <h1><?= lang('images') ?></h1>
        </ul>

    </div>

    <section class="panel">
        <div class="panel-body">
            <table id="general-procedures-table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th><?= lang('patient') ?></th>
                        <th><?= lang('description') ?></th>
                        <th><?= lang('date') ?></th>
                        <th></th>
                    </tr>
                </thead>
                <?php if (isset($images)): ?>
                    <tbody>
                        <?php foreach ($images as $i): ?>
                            <tr>
                                <td><?= $i['patient'] ?></td>
                                <td><?= $i['description'] ?></td>
                                <td><?= $i['created'] ?></td>
                                <td>
                                    <a href="javascript:void(0)" onclick="view_image(<?= $i['id'] ?>)" style="margin-left: 5px;"><i class="fa fa-eye"></i></a>
                                    <a href="javascript:void(0)" onclick="delete_image(<?= $i['id'] ?>)" style="margin-left: 5px;"><i class="fa fa-trash-o"></i></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                <?php endif; ?>
            </table>
        </div>
    </section>
</div>

<div id="modalShowImage" class="modal-block mfp-hide">
    <section class="panel">
        <header class="panel-heading">
            <h2 class="panel-title"><?= lang('add_general_procedure') ?></h2>
        </header>
        <div class="panel-body">
            <div class="modal-wrapper" id="view_image">



            </div>
        </div>
        <footer class="panel-footer">
            <div class="row">
                <div class="col-md-12 text-right">
                    <button class="btn btn-default modal-dismiss"><?= lang('close') ?></button>
                </div>
            </div>
        </footer>
    </section>
</div>

<script src="<?= $this->config->base_url(PLUGINPATH . 'dropzone/dropzone.js') ?>"></script>		
<script src="<?= $this->config->base_url(JSPATH . 'examples.modals.js') ?>"></script>
<script>
                                jQuery('#MainMenu div a').removeClass('list-group-item-success');
                                jQuery('#imagery-layout').addClass('list-group-item-success');

                                Dropzone.autoDiscover = false; // otherwise will be initialized twice
                                var myDropzoneOptionsImage = {
                                    maxFiles: 1,
                                    maxFilesize: 500000000000000,
                                    acceptedFiles: 'image/*',
                                    init: function () {
                                        this.on("complete", function (file) {
                                            jQuery('#imagery-layout').click();
                                        });
                                    }
                                };
                                var myDropzoneImage = new Dropzone('#dropzone-image', myDropzoneOptionsImage);

                                jQuery(document).ready(function () {
                                    jQuery('#general-procedures-table').dataTable({
                                        "language": {
                                            "lengthMenu": "Exibindo _MENU_ registros por página",
                                            "zeroRecords": "Nenhum registro encontrado",
                                            "info": "Exibindo página _PAGE_ de _PAGES_",
                                            "infoEmpty": "Tabela vazia",
                                            "infoFiltered": "(Filtrado de _MAX_ registros)",
                                            "sSearchPlaceholder": "Buscar ",
                                            "sSearch": "",
                                            "oPaginate": {
                                                "sFirst": "Primeira",
                                                "sLast": "Última",
                                                "sNext": "Próxima",
                                                "sPrevious": "Anterior"
                                            }
                                        }
                                    });
                                });
</script>

