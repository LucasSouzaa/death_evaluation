<script type="text/javascript" src="<?= $this->config->base_url(JSPATH . "patient.js") ?>"></script>

<!--DADOS DO PACIENTE-->
<div class="add-divs" id="div-patient-data" hidden="" style=" padding: 15px;">
    <div class="search-filter">
        <ul>
            <h1><?= lang('add') . ' / ' . lang('edit') . ' ' . lang('patient') ?></h1>
        </ul>
    </div>
    <section class="panel">        
        <ul class="nav nav-tabs">
            <li class="active"><a href="#patient_data" data-toggle="tab"><?= lang('patient_data') ?></a></li>
            <li><a href="#patient_contacts" data-toggle="tab"><?= lang('contacts') ?></a></li>
            <li><a href="#patient_addresses" data-toggle="tab"><?= lang('addresses') ?></a></li>
        </ul>
        <div class="tab-content">
            <div id="patient_data" class="tab-pane active">
                <div class="content">
                    <form class="form-horizontal group-border-dashed" action="#">
                        <div class="form-group">
                            <h4><label class="col-sm-2 control-label"><?= lang('name') ?></label></h4>
                            <div class="col-sm-4">
                                <input id="name" type="text" class="form-control"/> 
                            </div>

                            <h4><label class="col-sm-2 control-label"><?= lang('sus_card') ?></label></h4>
                            <div class="col-sm-4">
                                <input id="sus_card" type="text" class="form-control"/> 
                            </div>
                        </div>

                        <div class="form-group">
                            <h4><label class="col-sm-2 control-label"><?= lang('birthdate') ?></label></h4>
                            <div class="col-sm-4">
                                <input id="birthdate" type="date" class="form-control"/> 
                            </div>

                            <h4><label class="col-sm-2 control-label"><?= lang('gender') ?></label></h4>
                            <div class="col-sm-4">
                                <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="gender">
                                    <option value="m" selected><?= lang('male') ?></option>
                                    <option value="f"><?= lang('female') ?></option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <h4><label class="col-sm-2 control-label"><?= lang('skin_color') ?></label></h4>
                            <div class="col-sm-4">
                                <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="skin_color">
                                    <option value="white" selected><?= lang('white') ?></option>
                                    <option value="black"><?= lang('black') ?></option>
                                    <option value="brown"><?= lang('brown') ?></option>
                                    <option value="mulattos"><?= lang('mulattos') ?></option>
                                    <option value="caboclos"><?= lang('caboclos') ?></option>
                                    <option value="cafuzos"><?= lang('cafuzos') ?></option>
                                    <option value="na"><?= lang('na') ?></option>
                                </select>
                            </div>

                            <h4><label class="col-sm-2 control-label"><?= lang('mother_name') ?></label></h4>
                            <div class="col-sm-4">
                                <input id="mother_name" type="text" class="form-control"/> 
                            </div>
                        </div>
                        <div class="form-group">
                            <h4><label class="col-sm-2 control-label"><?= lang('marital_status') ?></label></h4>
                            <div class="col-sm-4">
                                <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="marital_status">
                                    <option value="single" selected><?= lang('single') ?></option>
                                    <option value="married"><?= lang('married') ?></option>
                                    <option value="widower"><?= lang('widower') ?></option>
                                    <option value="other"><?= lang('other') ?></option>
                                </select>
                            </div>

                            <h4><label class="col-sm-2 control-label"><?= lang('nationality') ?></label></h4>
                            <div class="col-sm-4">
                                <input id="nationality" type="text" class="form-control"/> 
                            </div>
                        </div>

                        <div class="form-group">
                            <h4><label class="col-sm-2 control-label"><?= lang('home_town_state') ?></label></h4>
                            <div class="col-sm-4">
                                <input id="home_town_state" type="text" class="form-control" maxlength="2"/> 
                            </div>

                            <h4><label class="col-sm-2 control-label"><?= lang('home_town') ?></label></h4>
                            <div class="col-sm-4">
                                <input id="home_town" type="text" class="form-control"/> 
                            </div>
                        </div>

                        <div class="form-group">
                            <h4><label class="col-sm-2 control-label"><?= lang('status') ?></label></h4>
                            <div class="col-sm-4">
                                <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="status">
                                    <option value="1" selected><?= lang('activated') ?></option>
                                    <option value="0"><?= lang('inactivated') ?></option>
                                    <option value="2"><?= lang('deleted') ?></option>
                                </select>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div id="patient_contacts" class="tab-pane">
                <div class="content">
                    <form class="form-horizontal group-border-dashed" action="#">
                        <div class="form-group">

                        </div>
                    </form>
                    <div id="contacts_table">
                    </div>
                </div>
            </div>
            <div id="patient_addresses" class="tab-pane">
                <div class="content">
                    <form class="form-horizontal group-border-dashed" action="#">
                        <div class="form-group">

                        </div>
                    </form>
                    <div id="addresses_table">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12" style="text-align: center">
                    <a onclick="save_patient()" class="btn btn-success"><?= lang('save') ?></a>
                    <a onclick="jQuery('#div-patient-data').toggle()" class="btn btn-danger"><?= lang('close') ?></a>
                </div>
            </div>
        </div>
    </section>
    </br>
</div>

<input id="patient_id" value="" hidden="" />

<div class="padding-md">
    <div class="search-filter">
        <a href="javascript:void(0)" class="btn btn-primary" onclick=" jQuery('input').val('');
                jQuery('#contacts_table').html('<h3>Sem dados de contato cadastrados</h3>');
                jQuery('#addresses_table').html('<h3>Sem endereços cadastrados</h3>');
                jQuery('#patient_id').val('');
                jQuery('#div-patient-data').show();" style="float: right"> <i class="fa fa-plus-square"></i> Adicionar</a>
        <ul>
            <h1><?= lang('patients') ?></h1>
        </ul>
    </div>

    <section class="panel">
        <div class="col-xs-4">
            <form action="filter_patients" method="POST">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Procurar paciente" name="filter">
                    <input type="hidden" name="page" value="1">
                    <span class="input-group-btn">
                        <input type="submit" class="btn btn-default" type="button" value="procurar">
                    </span>
                </div><!-- /input-group -->
            </form>
        </div><!-- /.col-lg-6 -->
        <table class="table table-bordered table-striped mb-none" id="datatable-tabletools" data-paging="false" data-swf-path="<?= $this->config->base_url(VENDORPATH . 'jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf') ?>">
            <thead>
                <tr>
                    <th><?= lang('sus_card') ?></th>
                    <th><?= lang('name') ?></th>
                    <th><?= lang('birthdate') ?></th>
                    <th>Passagens</th>
                    <th></th>
                </tr>
            </thead>
            <?php if (isset($patients)): ?>
                <tbody>
                    <?php foreach ($patients as $p): ?>
                        <tr>
                            <td>
                                <a href="javascript:void(0)" onclick="view_patient('<?= $this->encrypt->encode($p['id']) ?>')" style="display: block"><?= $p['sus_card'] ?></a>
                            </td>
                            <td>
                                <a href="javascript:void(0)" onclick="view_patient('<?= $this->encrypt->encode($p['id']) ?>')" style="display: block"><?= $p['name'] ?></a>
                            </td>
                            <td><?= $p['birthdate'] ?></td>
                            <td>
                                <?php foreach ($p['use_services'] as $usi): ?>
                                    <a href="<?= $this->config->base_url('attendance/open_attendance') . '/' . $this->encrypt->encode($usi['id']) ?>" style="margin-left: 5px;" title="<?= lang('open') ?>"><i class="fa fa-file" <?php if ($usi['type'] === 1): ?> style="color:green" <?php else: ?> style="color:red" <?php endif; ?> ></i></a>
                                <?php endforeach; ?>
                            </td>
                            <td style="text-align: center">
                                <a class="mb-xs mt-xs mr-xs modal-basic" onclick="jQuery('#patient_is_selected').val('<?= $this->encrypt->encode($p["id"]) ?>')" href="#modalChooseTrauma" style="margin-left: 5px;" title="<?= lang('new_attendance') ?>"> <i class="fa fa-plus-square-o"></i></a>
                                <a href="javascript:void(0)" onclick="delete_patient('<?= $this->encrypt->encode($p['id']) ?>')" style="margin-left: 5px;" title="<?= lang('delete') ?>"><i class="fa fa-trash-o"></i></a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>

            <?php endif; ?>
        </table>
        <div class="btn-group" role="group" aria-label="...">
            <?php for ($i = 0; $i < $num_rows; $i++) { ?>
                <form action="filter_patients" method="POST" class="pagination">  
                    <input type="hidden" value="<?php echo $i + 1; ?>" name="page">
                    <button type="submit" class="btn btn-default"><?php echo $i + 1; ?></button>
                </form>    
            <?php } ?>
        </div>
    </section>     
</div>
<div class="col-md-6">

</div>

<input id="patient_is_selected" value="" hidden=""  />


<div id="modalChooseTrauma" class="modal-block mfp-hide">
    <section class="panel">
        <header class="panel-heading">
            <h2 class="panel-title"><?= lang('attendance_type') ?></h2>
        </header>
        <div class="panel-body">
            <div class="row" >
                <div class="col-md-6">
                    <a class="btn btn-danger" href="javascript:void(0)" onclick="window.location = jQuery('body').data('baseurl') + 'attendance/new_attendance/' + jQuery('#patient_is_selected').val() + '/trauma'" title="<?= lang('trauma') ?>"><?= lang('trauma') ?></a>
                </div>
                <div class="col-md-6">
                    <a class="btn btn-success" href="javascript:void(0)" onclick="window.location = jQuery('body').data('baseurl') + 'attendance/new_attendance/' + jQuery('#patient_is_selected').val() + '/n_trauma'" title="<?= lang('n_trauma') ?>"><?= lang('n_trauma') ?></a>
                </div>
            </div>
        </div>
        <footer class="panel-footer">
            <div class="row">
                <div class="col-md-12 text-right">
                    <button class="btn btn-default modal-dismiss"><?= lang('close') ?></button>
                </div>
            </div>
        </footer>
    </section>
</div>

<!--<script>
    $(document).ready(function() {
    $('#datatable').DataTable( {
        dom: 'T<"clear">lfrtip',
        tableTools: {
            "sSwfPath": "<?= $this->config->base_url(VENDORPATH . 'jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf') ?>"
        },
        paging: false,
        
    } );
} );

</script>-->
