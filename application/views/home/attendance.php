<div class="padding-md">
    <div class="search-filter">
        <ul>
            <h1>Selecione o Atendimento</h1>
        </ul>
    </div>

    <table class="table table-bordered table-striped mb-none" id="datatable-tabletools" data-paging="false" data-swf-path="<?= $this->config->base_url(VENDORPATH . 'jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf') ?>">
        <thead>
            <tr>
                <th><?= lang('sus_card') ?></th>
                <th><?= lang('bed') ?></th>
                <th><?= lang('name') ?></th>
                <th><?= lang('birthdate') ?></th>
                <th>Entrada</th>
            </tr>
        </thead>
        <?php if (isset($attendance)): ?>
            <tbody>
                <?php foreach ($attendance as $p): ?>
                    <tr>
                        <td>
                            <a href="<?= $this->config->base_url('attendance/open_attendance') . '/' . $this->encrypt->encode($p['id']) ?>" style="display: block"><?= $p['sus_card'] ?></a>
                        </td>
                        <td><?= $p['bed'] ?></td>
                        <td>
                            <a href="<?= $this->config->base_url('attendance/open_attendance') . '/' . $this->encrypt->encode($p['id']) ?>" style="display: block"><?= $p['name'] ?></a>
                        </td>
                        <td><?= $p['birthdate'] ?></td>
                        <td><?= $p['moment_admission'] ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        <?php endif; ?>
    </table>
</div>