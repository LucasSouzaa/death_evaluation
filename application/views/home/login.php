<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html>
    <head>
        <link rel="shortcut icon" href="<?= $this->config->base_url(IMGPATH . 'favicon.png') ?>">
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <title> <?= isset($title) ? $title : lang('title') ?></title>
        <link rel="stylesheet" href="<?= $this->config->base_url(CSSPATH . 'bootstrap-responsive.min.css') ?>" />
        <link rel="stylesheet" href="<?= $this->config->base_url(CSSPATH . 'bootstrap.min.css') ?>" />
        <link rel="stylesheet" href="<?= $this->config->base_url(CSSPATH . 'bootstrap-datetimepicker.min.css') ?>" />
        <link rel="stylesheet" href="<?= $this->config->base_url(JSPATH . 'jquery.select2/select2.css') ?>" />
        <link rel="stylesheet" href="<?= $this->config->base_url(PLUGINPATH . 'magnific-popup/magnific-popup.css') ?> " />
        <link rel="stylesheet" href="<?= $this->config->base_url(FONTPATH . 'font-awesome.min.css') ?>" />
        <link rel="stylesheet" href="<?= $this->config->base_url(PLUGINPATH . 'pnotify/pnotify.custom.css') ?> " />
        <link rel="stylesheet" href="<?= $this->config->base_url(CSSPATH . 'my-theme.css') ?>" />
        <link href="<?= $this->config->base_url(CSSPATH . 'gritter/jquery.gritter.css') ?>" rel="stylesheet">
        <link rel="stylesheet" href="<?= $this->config->base_url(VENDORPATH . 'jquery-datatables-bs3/assets/css/datatables.css') ?>" />
        <script type="text/javascript" src="<?= $this->config->base_url(JSPATH . "jquery.min.js") ?>"></script>
        <script type="text/javascript" src="<?= $this->config->base_url(PLUGINPATH . 'modernizr/modernizr.js') ?>"></script>
        <script type="text/javascript" src="<?= $this->config->base_url(JSPATH . "bootstrap.min.js") ?>"></script>
        <script type="text/javascript" src="<?= $this->config->base_url(JSPATH . "jquery.imagemapster.min.js") ?>"></script>        
        <script type="text/javascript" src="<?= $this->config->base_url(PLUGINPATH . 'pnotify/pnotify.custom.js') ?>"></script>
        <script src="<?= $this->config->base_url(JSPATH . 'jquery.gritter.min.js') ?>"></script>
        <link rel="stylesheet" href="<?= $this->config->base_url(CSSPATH . 'loginstyle.css') ?>" />	
    </head>
    <body data-baseurl="<?= $this->config->base_url(); ?>" style="background: url(<?= $this->config->base_url(IMGPATH . 'background_login.jpg') ?>) no-repeat center center fixed; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;">
        <div class="container">
            <div class="row" id="pwd-container">
                <div class="col-md-4"></div>
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <section class="login-form">
                        <form method="post" action="#" role="login">
                            <h2 style="  text-align: -webkit-center;"><i class="fa fa-plus-square-o"></i> RebratsUE</h2>

                            <input type="text" name="username" id="username" onkeyup="login_enter(event);" placeholder="<?= lang('username') ?>" required class="form-control input-lg" />
                            <input type="password" class="form-control input-lg" onkeyup="login_enter(event);" id="password" placeholder="<?= lang('password') ?>" required="" />
                            <p>
                                <input type="checkbox" id="checkbox" name="remember_password" <?php
                                if (isset($_COOKIE["check"])) {
                                    if ($_COOKIE["check"] === 'true') {
                                        ?> checked="checked" <?php
                                           }
                                       }
                                       ?> /> <?= lang('remember_password') ?>
                            </p> 
                            <div class="pwstrength_viewport_progress"></div>
                            <a href="javascript:void(0)" id="enter_button" onclick="login()" class="btn btn-lg btn-primary btn-block"><?= lang('enter') ?></a>
                        </form>
                    </section>  
                </div>
            </div>
        </div>
    </body>
    <script src="<?= $this->config->base_url(JSPATH . 'login.js') ?>"></script>
</html>