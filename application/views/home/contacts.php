<table class="table table-bordered table-striped mb-none" >
    <thead>
        <tr>
            <th><?= lang('type') ?></th>
            <th><?= lang('contact') ?></th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($contacts as $c): ?>
            <tr>
                <th><?= $c['type'] ?></th>
                <th><?= $c['contact'] ?></th>
                <th>
                    <a href="javascript:void(0)" onclick="delete_contact(<?= $c['id'] ?>)" class="on-default remove-row" style=" margin-right: 5px; margin-left: 5px;"><i class="fa fa-trash-o"></i></a>
                    <a href="javascript:void(0)" onclick="$('#div-patient-contact').toggle();
                       $('#type').val(<?= $c['type'] ?>);
                       $('#contact').val(<?= $c['contact'] ?>);
                       " class="on-default remove-row" style=" margin-right: 5px; margin-left: 5px;"><i class="fa fa-edit"></i></a>
                </th>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>