<div class="search-filter">
    <a href="javascript:void(0)" class="btn btn-primary" onclick="
            jQuery('#div-faq-data').show();
            $('#btn_edit').hide();
            $('#btn_submit').show();
            $('#question').val('');
            $('#answer').val('');
       " style="float: right"> <i class="fa fa-plus-square"></i> Adicionar</a>
    <ul>
        <h1><?= lang('faq') ?></h1>
    </ul>
</div>

<div id="patient_data" class="tab-pane active">
    <div class="content add-divs" id="div-faq-data" hidden="">
        <form method="POST" class="form-horizontal group-border-dashed" id="form_faq" action="">
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-6">
                        <h5><?= lang('question') ?></h5>
                        <input id="question" type="text" name="form-question" class="form-control"/> 
                        <div class="alert alert-danger" id="alert_question-err">
                            <strong>Atenção</strong> Este campo deve ser preenchido.
                        </div>
                    </div>
                    <div class="col-sm-3"></div>
                </div>
                <div class="row">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-6">
                        <h5><?= lang('answer') ?></h5>
                        <textarea id="answer" name="form-answer" type="text" class="form-control" > </textarea>
                        <div class="alert alert-danger" id="alert_answer-err">
                            <strong>Atenção</strong> Este campo deve ser preenchido.
                        </div>
                    </div>
                    <div class="col-sm-3"></div>
                    <input type="hidden" val="" id="hidden_id" name="form-id_question">
                </div><br/>
                <div class="row">
                    <div class="form-group">
                        <div class="col-sm-12" style="text-align: center">
                            <a id="btn_submit" class="btn btn-success"><?= lang('save') ?></a>
                            <a id="btn_edit" class="btn btn-success"><?= lang('edit') ?></a>
                            <a onclick="jQuery('#div-faq-data').toggle()" class="btn btn-danger"><?= lang('close') ?></a>
                        </div>
                    </div>
                </div>    
            </div>
        </form>
    </div>
</div>

<!-- FORMULARIO INVISIVEL PARA REMOCAO DE QUESTOES -->
<form id="hidden_form" method="POST" action="rem_question">
    <input type="hidden" val="" id="hidden_remove" name="form-id_question">
</form>
<?php if (!empty($perguntas)): ?>
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        <?php foreach ($perguntas as $count => $item): ?> 
            <div class="panel panel-default" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $count ?>">

                <div class="panel-heading" role="tab" id="heading<?php echo $count ?>">
                    <h4 class="panel-title">
                        <a class="collapsed" role="button"  aria-expanded="false" aria-controls="collapse<?php echo $count ?>">
                            <?php echo $item['question']; ?> 
                        </a>
                        <a id="btn_rem" class="btn pull-right" style="padding: 0px 7px;" href="javascript:void(0)" onclick="
                                $('#hidden_remove').val('<?php echo $item['id_question'] ?>');
                                result = confirm('tem certeza?');
                                if (result === true) {
                                    $('#hidden_form').submit();
                                }
                           ">
                            <i class="fa fa-trash" data-toggle="tooltip" title="Remover"></i>
                        </a>
                        <a id="edit_faq<?php echo $item['id_question'] ?>" class="btn pull-right" style="padding: 0px 7px;" href="javascript:void(0)" onclick="
                                jQuery('#div-faq-data').show();
                                $('#btn_submit').hide();
                                $('#btn_edit').show();
                                $('#question').val('<?php echo $item['question'] ?>');
                                $('#answer').val('<?php echo $item['answer'] ?>');
                                $('#hidden_id').val('<?php echo $item['id_question'] ?>');
                                $('html, body').animate({scrollTop: 0}, 0);
                           ">
                            <i data-toggle="tooltip" title="Editar" class="fa fa-pencil"></i>
                        </a>
                    </h4>
                </div>
                <div id="collapse<?php echo $count ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo $count ?>">
                    <div class="panel-body">
                        <?php echo $item['answer']; ?>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
<?php else: ?>
    <h4>Nenhuma questão cadastrada por enquanto.</h4>
<?php endif; ?>

<script>
    $(document).ready(function () {
        $('#alert_question-err').hide();
        $('#alert_answer-err').hide();
        $('#btn_edit').hide();
        $('#btn_submit').hide();
        $('#question').focusout(function () {
            if ($('#question').val() === '') {
                $('#alert_question-err').show();
            } else {
                $('#alert_question-err').hide();
            }
        });

        $('#btn_submit').click(function () {
            $('#alert_question-err').hide();
            $('#alert_answer-err').hide();
            if ($('#answer').val() === '') {
                $('#alert_answer-err').show();
            } else if ($('#question').val() === '') {
                $('#alert_question-err').show();
            } else {
                $('#form_faq').attr("action", "new_question");
                $('#form_faq').submit();
            }
        });

        $('#btn_edit').click(function () {
            $('#alert_question-err').hide();
            $('#alert_answer-err').hide();
            if ($('#answer').val() === '') {
                $('#alert_answer-err').show();
            } else if ($('#question').val() === '') {
                $('#alert_question-err').show();
            } else {
                $('#form_faq').attr("action", "edit_question");
                $('#form_faq').submit();
            }
        });

        $('#edit_faq').click(function () {
            alert('Tente novamente mais tarde (faq01)');
        });

    });
</script>