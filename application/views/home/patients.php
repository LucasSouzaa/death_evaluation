<script type="text/javascript" src="<?= $this->config->base_url(JSPATH . "patient.js") ?>"></script>

<!--DADOS DO PACIENTE-->
<div class="add-divs" id="div-patient-data" hidden="" style=" padding: 15px;">
    <section class="panel">
            <div class="tab-content">
                <div id="patient_data" class="tab-pane active">
                    <form id="form_add" class="form-horizontal" style="width:100%" method="POST" onsubmit="return verificaInformacaoPaciente()">
                        <input type="hidden" name="paciente_id" value="">
                        <input type="hidden" name="aba" value="informacoes_paciente">
                        <h2>Identificação</h2>
                        <div class="row-fluid" style="padding-top: 10px">
                            <div class="span4">
                                <div class="control-group">
                                    <label for="nome_paciente" class="control-label">Nome</label>
                                    <div class="controls">
                                        <input class="form-control" name="nome_paciente" id="nome_paciente" style="width:100%" type="text" value="">
                                    </div>
                                </div>
                            </div>
                            <div class="span4">
                                <div class="control-group">
                                    <label for="cpf" class="control-label">CPF</label>
                                    <div class="controls">
                                        <input class="form-control" name="cpf" id="cpf" type="text" value="">
                                    </div>
                                </div>
                            </div>

                            <div class="span4">
                                <div class="control-group">
                                    <label for="dta_nascimento" class="control-label">Data de Nascimento</label>
                                    <div class='controls'>
                                        <input type='date' class="form-control" id="dta_nascimento" />
                                    </div>
                                </div>	
                            </div>
                        </div>
                        <div class="row-fluid">
                            <div class="span4">
                                <div class="control-group">
                                    <label for="nome_mae" class="control-label">Nome da mãe</label>
                                    <div class="controls">
                                        <input class="form-control" name="nome_mae" id="nome_mae" style="width:100%" type="text" value="">
                                    </div>
                                </div>
                            </div>

                            <div class="span4">
                                <div class="control-group">
                                    <label for="cor" class="control-label">Cor</label>
                                    <div class="controls">
                                        <select name="cor" id="cor" data-plugin-selectTwo class="form-control populate input-medium mb-md">
                                            <option value=" ">Selecione uma opção</option>
                                            <option value="Amarela">Amarela</option>
                                            <option value="Branca">Branca</option>
                                            <option value="Mulata">Mulata</option>
                                            <option value="Negra">Negra</option>
                                            <option value="Parda">Parda</option>
                                            <option value="Vermelha">Vermelha</option>
                                            <option value="Putro">Outra</option>
                                            <option value="Não especificada">Não Especificada</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="span4">
                                <div class="control-group">
                                    <label for="sexo" class="control-label"> Sexo</label>
                                    <div class="controls">
                                        <select name="sexo" id="sexo" data-plugin-selectTwo class="form-control populate input-medium mb-md">
                                            <option value="Feminino">Feminino</option>
                                            <option value="Masculino">Masculino</option>
                                            <option value="Outro">Outro</option>
                                            <option value="Não especificado">Não Especificado</option>
                                        </select>
                                    </div>
                                </div>	
                            </div>
                        </div>
                        <div class="row-fluid">
                            <div class="span4">
                                <div class="control-group">
                                    <label for="estado_civil" class="control-label">Estado Civil</label>
                                    <div class="controls">
                                        <select name="estado_civil" id="estado_civil" data-plugin-selectTwo class="form-control populate input-medium mb-md">
                                            <option value=" ">Selecione uma opção</option>
                                            <option value="Amasiado">Amasiado</option>
                                            <option value="Casado">Casado</option>
                                            <option value="Divorciado">Divorciado</option>
                                            <option value="Separado">Separado</option>
                                            <option value="Solteiro">Solteiro</option>
                                            <option value="Viúvo">Viuvo</option>
                                            <option value="Outro">Outro</option>
                                            <option value="Não especificado">Não Especificado</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="span4">
                                <div class="control-group">
                                    <label for="naturalidade" class="control-label">Naturalidade</label>
                                    <div class="controls">
                                        <input class="form-control" name="naturalidade" id="naturalidade" type="text" value="">
                                    </div>
                                </div>
                            </div>

                            <div class="span4">
                                <div class="control-group">
                                    <label for="nom_profissao" class="control-label">Ocupação</label>
                                    <div class="controls">
                                        <input class="form-control" name="nom_profissao" id="nom_profissao" type="text" value="">
                                    </div>
                                </div>	
                            </div>
                        </div>
                        <div class="row-fluid">
                            <div class="span4">
                                <div class="control-group">
                                    <label for="grau_instrucao" class="control-label">Escolaridade</label>
                                    <div class="controls">
                                        <select name="grau_instrucao" id="grau_instrucao" data-plugin-selectTwo class="form-control populate input-medium mb-md">
                                            <option value="0">Selecione uma opção</option>
                                            <option value="1 Grau Fundamental Incompleto">1 Grau-Fundamental incompleto</option>
                                            <option value="1 Grau Fundamental Completo">1 Grau-Fundamentao completo</option>
                                            <option value="2 Grau Fundamental Incompleto">2 Grau incompleto</option>
                                            <option value="2 Grau Fundamental Completo">2 Grau completo</option>
                                            <option value="Superior Incompleto">Superior Incompleto</option>
                                            <option value="Superior Completo">Superior Completo</option>
                                            <option value="Pós-Graduado">Pós-Graduação</option>
                                            <option value="Deconhecido">Desconhecido</option>
                                            <option value="Nenhum">Nenhum</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h2>Endereço</h2>
                        <div class="row-fluid">
                            <div class="span4">
                                <div class="control-group">
                                    <label for="residencia_logradouro" class="control-label">Endereço</label>
                                    <div class="controls">
                                        <input class="form-control" name="residencia_logradouro" style="width:100%" type="text" value="">
                                    </div>
                                </div>	
                            </div>
                            <div class="span4">
                                <div class="control-group">
                                    <label for="residencia_numero" class="control-label">Número</label>
                                    <div class="controls">
                                        <input class="form-control" name="residencia_numero" type="text" value="">
                                    </div>
                                </div>	
                            </div>

                            <div class="span4">
                                <div class="control-group">
                                    <label for="residencia_bairro" class="control-label">Bairro</label>
                                    <div class="controls">
                                        <input class="form-control" name="residencia_bairro" type="text" value="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row-fluid">
                            <div class="span4">
                                <div class="control-group">
                                    <label for="residencia_cep" class="control-label">CEP</label>
                                    <div class="controls">
                                        <input class="form-control" name="residencia_cep" id="residencia_cep" type="text" value="">
                                    </div>
                                </div>
                            </div>

                            <div class="span4">
                                <div class="control-group">
                                    <label for="residencia_cidade" class="control-label">Cidade</label>
                                    <div class="controls">
                                        <input class="form-control" name="residencia_cidade" id="residencia_cidade" type="text" value="">
                                    </div>
                                </div>
                            </div>
                            <div class="span4">
                                <div class="control-group">
                                    <label for="residencia_uf" class="control-label">UF</label>
                                    <div class="controls">
                                        <select name="residencia_uf" id="residencia_uf" data-plugin-selectTwo class="form-control populate input-medium mb-md" style="float:left">
                                            <option value=" ">-</option>
                                            <option value="AC">AC</option>
                                            <option value="AL">AL</option>
                                            <option value="AM">AM</option>
                                            <option value="AP">AP</option>
                                            <option value="BA">BA</option>
                                            <option value="CE">CE</option>
                                            <option value="DF">DF</option>
                                            <option value="ES">ES</option>
                                            <option value="GO">GO</option>
                                            <option value="MA">MA</option>
                                            <option value="MT">MT</option>
                                            <option value="MS">MS</option>
                                            <option value="MG">MG</option>
                                            <option value="PA">PA</option>
                                            <option value="PB">PB</option>
                                            <option value="PR">PR</option>
                                            <option value="PE">PE</option>
                                            <option value="PI">PI</option>
                                            <option value="RJ">RJ</option>
                                            <option value="RN">RN</option>
                                            <option value="RO">RO</option>
                                            <option value="RS">RS</option>
                                            <option value="RR">RR</option>
                                            <option value="SC">SC</option>
                                            <option value="SE">SE</option>
                                            <option value="SP">SP</option>
                                            <option value="TO">TO</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h2>Contato</h2>
                        <div class="row-fluid">
                            <div class="span4">
                                <div class="control-group">
                                    <label for="telefone" class="control-label">Telefone</label>
                                    <div class="controls">
                                        <input class="form-control" name="telefone" id="telefone" type="text" value="">
                                    </div>
                                </div>
                            </div>
                            <div class="span4">
                                <div class="control-group">
                                    <label for="email" class="control-label">Email</label>
                                    <div class="controls">
                                        <input class="form-control" name="email" id="email" type="text" value="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h2>Registros de Saúde</h2>
                        <div class="row-fluid">
                            <div class="span4">
                                <div class="control-group">
                                    <label for="registro" class="control-label">Registro</label>
                                    <div class="controls">
                                        <input class="form-control" name="registro" id="registro" type="text" value="">
                                    </div>
                                </div>
                            </div>
                            <div class="span4">
                                <div class="control-group">
                                    <label for="cartao_sus" class="control-label">Número do Cartão SUS</label>
                                    <div class="controls">
                                        <input class="form-control" name="cartao_sus" id="cartao_sus" type="text" value="">
                                    </div>
                                </div>	
                            </div>
                        </div>
                        <h2>Outras Informações</h2>
                        <div class="row-fluid">
                            <div class="span4">
                                <div class="control-group">
                                    <label for="plano_saude" class="control-label">Plano de Saúde</label>
                                    <div class="controls">
                                        <select name="plano_saude" id="plano_saude_paciente" data-plugin-selectTwo class="form-control populate input-medium mb-md" style="float:left">
                                            <option value=" ">Selecione uma opção</option>
                                            <option value="SUS">SUS</option>
                                            <option value="São Francisco">São Francisco</option>
                                            <option value="UNIMED">UNIMED</option>
                                            <option value="Santa Casa">Santa Casa</option>
                                            <option value="Sermed">Sermed</option>
                                            <option value="Cabesp">Cabesp</option>
                                            <option value="Bradesco Saúde">Bradesco Saúde</option>
                                            <option value="Caixa Econômica Federal">Caixa Econômica Federal</option>
                                            <option value="CASSI">CASSI</option>
                                            <option value="Blue Life">Blue Life</option>
                                            <option value="Iamspe">Iamspe</option>
                                            <option value="Medial Saúde">Medial Saúde</option>
                                            <option value="Omint">Omint</option>
                                            <option value="SABESPREV">SABESPREV</option>
                                            <option value="SAMSP">SAMSP</option>
                                            <option value="Golden Cross">Golden Cross</option>
                                            <option value="Outros">Outros</option>
                                        </select>
                                    </div>
                                </div>	
                            </div>
                        </div>
                    </form>
                </div>

                <div class="form-group">
                    <div class="col-sm-12" style="text-align: center">
                        <a onclick="save_patient()" class="btn btn-success"><?= lang('save') ?></a>
                        <a onclick="jQuery('#div-patient-data').toggle()" class="btn btn-danger"><?= lang('close') ?></a>
                    </div>
                </div>
            </div>
    </section>
    </br>
</div>

<input id="patient_id" value="" hidden="" />

<div class="padding-md">
    <div class="search-filter">
        <form id="form_filter" method="POST">
            <div class="input-group" style="width: 50%; float: right;">
                <input type="text" class="form-control" placeholder="Procurar paciente" name="filter">
                <input type="hidden" name="page" value="1">
                <span class="input-group-btn">
                    <button  id="btn_filter" class="btn btn-default" type="button" value="procurar">Buscar</button>
                    <a href="javascript:void(0)" class="btn btn-primary" onclick=" jQuery('input').val('');
                            jQuery('#patient_id').val('');
                            jQuery('#div-patient-data').show();" value="procurar">Adicionar</a>
                </span>
            </div>
        </form>
        <ul>
            <h1><?= lang('patients') ?></h1>
        </ul>
    </div>

    <section class="panel">
        <table class="table table-bordered table-striped mb-none" id="datatable-tabletools" data-paging="false" data-swf-path="<?= $this->config->base_url(VENDORPATH . 'jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf') ?>">
            <thead>
                <tr>
                    <th><?= lang('sus_card') ?></th>
                    <th><?= lang('name') ?></th>
                    <th><?= lang('birthdate') ?></th>
                    <th>Passagens</th>
                    <th></th>
                </tr>
            </thead>
            <?php if (isset($patients)): ?>
                <tbody>
                    <?php foreach ($patients as $p): ?>
                        <tr>
                            <td>
                                <a href="javascript:void(0)" onclick="view_patient('<?= $this->encrypt->encode($p['id']) ?>')" style="display: block"><?= $p['sus_card'] ?></a>
                            </td>
                            <td>
                                <a href="javascript:void(0)" onclick="view_patient('<?= $this->encrypt->encode($p['id']) ?>')" style="display: block"><?= $p['name'] ?></a>
                            </td>
                            <td><?= $p['birthdate'] ?></td>
                            <td>
                                <?php foreach ($p['use_services'] as $usi): ?>
                                    <a href="<?= $this->config->base_url('attendance/open_attendance') . '/' . $this->encrypt->encode($usi['id']) ?>" style="margin-left: 5px;" title="<?= lang('open') ?>"><i class="fa fa-file" <?php if($usi['type'] == 1): ?> style="color:green" <?php else: ?> style="color:red" <?php endif; ?> ></i></a>
                                <?php endforeach; ?>
                            </td>
                            <td style="text-align: center">
                                <a class="mb-xs mt-xs mr-xs modal-basic" onclick="jQuery('#patient_is_selected').val('<?= $this->encrypt->encode($p["id"]) ?>')" href="#modalChooseTrauma" style="margin-left: 5px;" title="<?= lang('new_attendance') ?>"> <i class="fa fa-plus-square-o"></i></a>
                                <a href="javascript:void(0)" onclick="delete_patient('<?= $this->encrypt->encode($p['id']) ?>')" style="margin-left: 5px;" title="<?= lang('delete') ?>"><i class="fa fa-trash-o"></i></a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            <?php endif; ?>
        </table>
    </section>
</div>

<input id="patient_is_selected" value="" hidden=""  />


<div id="modalChooseTrauma" class="modal-block mfp-hide" style="width: 300px">
    <section class="panel">
        <header class="panel-heading">
            <h2 class="panel-title"><?= lang('attendance_type') ?></h2>
        </header>
        <div class="panel-body">
            <div class="row" >
                <div class="col-md-6">
                    <a class="btn btn-success" href="javascript:void(0)" onclick="window.location = jQuery('body').data('baseurl') + 'attendance/new_attendance/' + jQuery('#patient_is_selected').val() + '/1'" title="<?= lang('trauma') ?>"><i class="fa fa-file-o"></i> <?= lang('trauma') ?></a>
                </div>
                <div class="col-md-6">
                    <a class="btn btn-danger" href="javascript:void(0)" onclick="window.location = jQuery('body').data('baseurl') + 'attendance/new_attendance/' + jQuery('#patient_is_selected').val() + '/0'" title="<?= lang('n_trauma') ?>"><i class="fa fa-file-o"></i> <?= lang('n_trauma') ?></a>
                </div>
            </div>
        </div>
        <footer class="panel-footer">
            <div class="row">
                <div class="col-md-12 text-right">
                    <button class="btn btn-default modal-dismiss"><?= lang('close') ?></button>
                </div>
            </div>
        </footer>
    </section>
</div>
<script>
    $('#btn_filter').click(function () {
        if ($(location).attr('href').indexOf('patients') !== -1) {
            $("#form_filter").attr("action", "filter_patients");
        } else {
            $("#form_filter").attr("action", "home/filter_patients");
        }
        $("#form_filter").submit();
    })
</script>

<script>
    $(document).ready(function () {
        $("#cpf").mask("999.999.999-99");
        $("#residencia_cep").mask("99.999-999");
    });
</script>
