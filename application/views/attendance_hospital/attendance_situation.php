<!-- Situação de Atendimento -->
<legend>
    <a href="javascript:void(0)" onclick="togglePanels('#attend_sit', '#attend_sit_options');">Situação de Atendimento </a>
    <div id="attend_sit_chev" class="pull-right">
        <a href="javascript:void(0)" onclick="togglePanels('#attend_sit', '#attend_sit_options');"><i class="fa fa-plus-square-o"></i></a>
    </div>
    <div id="attend_sit_chevback" style="display:none;" class="pull-right">
        <a href="javascript:void(0)" onclick="togglePanels('#attend_sit', '#attend_sit_options');"><i class="fa fa-minus-square-o"></i></a>
    </div>
</legend>
<div id="attend_sit_options" class="btn-group" style="display:none; width: 100%">
    <div class="well">
        <div class="row-fluid">
            <div class="col-sm-3">
                <h5><label class="control-label">Condição do paciente</label></h5>
                <select class="form-control" id="patient_condition_type">
                    <option value="atendimento_avaliacao" selected><?= lang('atendimento_avaliacao') ?></option>
                    <option value="observacao"><?= lang('observacao') ?></option>
                    <option value="aguarda_leito"><?= lang('aguarda_leito') ?></option>
                    <option value="alta"><?= lang('alta') ?></option>
                    <option value="alta_a_pedido"><?= lang('alta_a_pedido') ?></option>
                    <option value="transferencia"><?= lang('transferencia') ?></option>
                    <option value="internado"><?= lang('internado') ?></option>
                    <option value="reinternado"><?= lang('reinternado') ?></option>
                    <option value="obito"><?= lang('obito') ?></option>
                </select>
            </div>
            <div class="col-sm-3">
                <h5><label class="control-label">Data/Hora</label></h5>
                <div class='input-group date input-append datetimepicker1'>
                    <input type='text' class="form-control" data-format="yyyy-MM-dd hh:mm:ss" id="patient_condition_date_hour" />
                    <span class="input-group-addon add-on">
                        <span><i class="fa fa-calendar" data-time-icon="icon-time" data-date-icon="icon-calendar">
                            </i></span>
                    </span>
                </div>
            </div>
            <div class="col-sm-3 patient_condition_local">
                <h5><label class="control-label">Local</label></h5>
                <select class="form-control" id="patient_condition_local">
                    <option value="centro_cirurgico"><?= lang('centro_cirurgico') ?></option>
                    <option value="centro_terapia_intensiva"><?= lang('centro_terapia_intensiva') ?></option>
                    <option value="enfermaria"><?= lang('enfermaria') ?></option>
                    <option value="sala_trauma"><?= lang('sala_trauma') ?></option>
                    <option value="sala_estabilizacao_clinica"><?= lang('sala_estabilizacao_clinica') ?></option>
                    <option value="unidade_semiintensiva"><?= lang('unidade_semiintensiva') ?></option>
                    <option value="outro"><?= lang('outro') ?></option>
                </select>
            </div>
            <div class="col-sm-3">
                <h5><label class="control-label">Outras informações</label></h5>
                <input class="form-control" type="text" id="patient_condition_other_info" >
            </div>

            <div class="col-sm-12">
                <h5><label class="control-label"></label></h5>
                <a onclick="add_patient_condition()" class="btn btn-primary btn-block"><i class="fa fa-plus"></i> Adicionar</a>
            </div>
        </div>
        </br>
        <div class="row-fluid">
            <div id="patient_condition_list">

            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function () {
        $('.datetimepicker1').datetimepicker({
            language: 'pt-BR'
        });
    });
</script>