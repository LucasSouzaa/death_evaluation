<?php if (!empty($volemic_reposition_list)): ?>
    <div class="search-filter">
        <legend><?= lang($volemic_reposition_type) ?></legend>
    </div>

    <div class="well">
        <table class="table table-bordered table-striped mb-none" >
            <thead>
                <tr>
                    <th><?= lang('solution_type') ?></th>
                    <th><?= lang('solution_detail') ?></th>
                    <th><?= lang('moment') ?></th>
                    <?php if ($volemic_reposition_type === 'hemo'): ?>
                        <th>APTM</th>
                    <?php endif; ?>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($volemic_reposition_list as $vrl): ?>
                    <tr>
                        <th><?= $vrl['solution_type'] ?></th>
                        <th><?= $vrl['solution_detail'] ?><?php if ($volemic_reposition_type !== 'endo_drugs'): ?> ml<?php endif; ?></th>
                        <th><?= $vrl['moment'] ?></th>
                        <?php if ($volemic_reposition_type === 'hemo'): ?>
                            <th><?= $vrl['aptm'] ?></th>
                        <?php endif; ?>
                        <th><a href="javascript:void(0)" onclick="delete_volemic_reposition('<?= $volemic_reposition_type ?>', '<?= $this->encrypt->encode($vrl['id']) ?>')" class="on-default remove-row" style=" margin-right: 5px; margin-left: 5px;"><i class="fa fa-trash-o"></i></a></th>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<?php else: ?>
    <h4>Nenhuma dado adicionado por enquanto</h4>
<?php endif; ?>

<script>

    if ('<?= $volemic_reposition_type ?>' === 'cristaloides') {
        jQuery('#cristaloides_volume_in').val(<?= $volemic_reposition_sum ?>);
    } else if ('<?= $volemic_reposition_type ?>' === 'hemo') {
        jQuery('#hemoderivados_volume_in').val(<?= $volemic_reposition_sum ?>);
    }

    function delete_volemic_reposition(volemic_reposition_type, id) {
        jQuery.ajax({
            url: jQuery("body").data("baseurl") + "attendance_hospital/delete_volemic_reposition",
            type: "post",
            dataType: 'json',
            data: {
                id: id
            },
            success: function (response) {
                jQuery('#modal_close_button').click();
                if (response.status === 'NOK') {
                    var notice = new PNotify({
                        title: 'Erro',
                        text: 'Tente novamente mais tarde',
                        type: 'error',
                        addclass: 'click-2-close',
                        hide: false,
                        buttons: {
                            closer: false,
                            sticker: false
                        }
                    });
                    notice.get().click(function () {
                        notice.remove();
                    });
                } else {
                    // recarrega lista
                    jQuery('#' + volemic_reposition_type + '_list').load(jQuery("body").data("baseurl") + "attendance_hospital/get_volemic_reposition_list/" + volemic_reposition_type);
                }
            }
        });
    }
</script>