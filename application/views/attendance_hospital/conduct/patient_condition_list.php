<div class="search-filter">
    <legend>Lista de condições do paciente</legend>
</div>

<?php if (isset($patient_condition_list)): ?>
    <table class="table table-bordered table-striped mb-none" >
        <thead>
            <tr>
                <th>Condição</th>
                <th>Data/Hora Admissão</th>
                <th>Data/Hora Saida</th>
                <th>Local</th>
                <th>Outras informações</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($patient_condition_list as $pcl): ?>
                <tr>
                    <th><?= lang($pcl['exit_type']) ?></th>
                    <th><?= $pcl['moment_admission'] ?></th>
                    <th><?php if($pcl['moment_exit'] === '0000-00-00 00:00:00'): echo 'Sem saída'; else: echo $pcl['moment_exit']; endif; ?></th>
                    <th><?= lang($pcl['sent_to']) ?></th>
                    <th><?= $pcl['other_info'] ?></th>
                    <th><a href="javascript:void(0)" onclick="delete_patient_condition('<?= $this->encrypt->encode($pcl['id']) ?>')" class="on-default remove-row" style=" margin-right: 5px; margin-left: 5px;"><i class="fa fa-trash-o"></i></a></th>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php else: ?>
    <h3>Nenhuma dado cadastrado</h3>
<?php endif; ?>


<script>

    function delete_patient_condition(id) {
        jQuery.ajax({
            url: jQuery("body").data("baseurl") + "attendance_hospital/delete_patient_condition",
            type: "post",
            dataType: 'json',
            data: {
                id: id
            },
            success: function (response) {
                jQuery('#modal_close_button').click();
                if (response.status === 'NOK') {
                    var notice = new PNotify({
                        title: 'Erro',
                        text: 'Tente novamente mais tarde',
                        type: 'error',
                        addclass: 'click-2-close',
                        hide: false,
                        buttons: {
                            closer: false,
                            sticker: false
                        }
                    });
                    notice.get().click(function () {
                        notice.remove();
                    });
                } else {
                    // recarrega lista
                    jQuery('#patient_condition_list').load(jQuery("body").data("baseurl") + "attendance_hospital/get_patient_condition_list");
                }
            }
        });
    }
</script>