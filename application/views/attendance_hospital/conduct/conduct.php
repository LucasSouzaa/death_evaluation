
<div class="pill-content">
    <div class="pill-pane active">
        <div class="container-fluid">
            <div id="external_couses_diagnosis">
                <!-- reposicao volemica -->
                <legend> 
                    <a href="javascript:void(0)" onclick="togglePanels('#vol_rep', '#vol_rep_options');">Reposição Volêmica</a>
                    <div id="vol_rep_chev" class="pull-right">
                        <a href="javascript:void(0)" onclick="togglePanels('#vol_rep', '#vol_rep_options');"><i class="fa fa-plus-square-o"></i></a>
                    </div>
                    <div id="vol_rep_chevback" style="display:none;" class="pull-right">
                        <a href="javascript:void(0)" onclick="togglePanels('#vol_rep', '#vol_rep_options');"><i class="fa fa-minus-square-o"></i></a>
                    </div>
                </legend>
                <div id="vol_rep_options" class="btn-group" style="display:none; width: 100%">
                    <div class="well">
                        <div class="row-fluid">
                            <h4>Cristalóides</h4>
                        </div>

                        <div class="row-fluid">
                            <div class="col-sm-3">
                                <h5><label class="control-label">Tipo</label></h5>
                                <select class="form-control" id="cristaloides_type">
                                    <option value="-">-</option>
                                    <option value="ringer_lactato">Ringer lactato</option>
                                    <option value="soro_fisiologico">Soro fisiológico</option>
                                </select>
                            </div>
                            <div class="col-sm-3">
                                <h5><label class="control-label">Volume</label></h5>
                                <input class="form-control" type="number" id="cristaloides_volume" >
                            </div>
                            <div class="col-sm-3">
                                <h5><label class="control-label">Data/Hora</label></h5>
                                <div class='input-group date input-append datetimepicker1'>
                                    <input type='text' class="form-control" data-format="yyyy-MM-dd hh:mm:ss" id="cristaloides_date_hour" />
                                    <span class="input-group-addon add-on">
                                        <span><i class="fa fa-calendar" data-time-icon="icon-time" data-date-icon="icon-calendar">
                                            </i></span>
                                    </span>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <h5><label class="control-label"></label></h5>
                                <a onclick="add_cristaloides()" class="btn btn-primary btn-block"><i class="fa fa-plus"></i> Adicionar</a>
                            </div>
                        </div>
                        </br>
                        <div class="row-fluid">
                            <div id="cristaloides_list">

                            </div>
                        </div>
                    </div>

                    <div class="well">
                        <div class="row-fluid">
                            <h4>Hemocomponentes</h4>
                        </div>

                        <div class="row-fluid">
                            <div class="col-sm-3">
                                <h5><label class="control-label">Tipo</label></h5>
                                <select class="form-control" id="hemo_type">
                                    <option value="-">-</option>
                                    <option value="concentrado_hemacias">Concentrado de hemácias</option>
                                    <option value="plasma">Plasma</option>
                                    <option value="crioprecipitado">Crioprecipitado</option>
                                    <option value="plaquetas">Plaquetas</option>
                                </select>
                            </div>
                            <div class="col-sm-3">
                                <h5><label class="control-label">Volume</label></h5>
                                <input class="form-control" type="number" id="hemo_volume" >
                            </div>
                            <div class="col-sm-3">
                                <h5><label class="control-label">Data/Hora</label></h5>
                                <div class='input-group date input-append datetimepicker1'>
                                    <input type='text' class="form-control" data-format="yyyy-MM-dd hh:mm:ss" id="hemo_date_hour" />
                                    <span class="input-group-addon add-on">
                                        <span><i class="fa fa-calendar" data-time-icon="icon-time" data-date-icon="icon-calendar">
                                            </i></span>
                                    </span>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" id="aptm" > Acionamento do protocolo de transfusão maciça?
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-1">
                                <h5><label class="control-label"></label></h5>
                                <a onclick="add_hemo()" class="btn btn-primary btn-block"><i class="fa fa-plus"></i></a>
                            </div>
                        </div>
                        </br>
                        <div class="row-fluid">
                            <div id="hemo_list">

                            </div>
                        </div>
                    </div>

                    <div class="well">
                        <div class="row-fluid">
                            <h4>Medicamentos endovenosos</h4>
                        </div>

                        <div class="row-fluid">
                            <div class="col-sm-3">
                                <h5><label class="control-label">Tipo de medicamento</label></h5>
                                <select class="form-control" id="endo_drugs_type">
                                    <option value="-">-</option>
                                    <option value="sedativos">Sedativos</option>
                                    <option value="analgesicos">Analgésicos</option>
                                    <option value="bloqueadores_neuromusculares">Bloqueadores neuromusculares</option>
                                    <option value="antifibrinoliticos">Antifibrinolíticos</option>
                                    <option value="diureticos">Diuréticos</option>
                                    <option value="aminovasoativos">Aminovasoativos</option>
                                    <option value="anticonvulsivantes">Anticonvulsivantes</option>
                                    <option value="antihipertensivos">Antihipertensivos</option>
                                    <option value="ansioliticos">Ansiolíticos</option>
                                </select>
                            </div>
                            <div class="col-sm-3">
                                <h5><label class="control-label">Medicamento</label></h5>
                                <select class="form-control" id="endo_drugs_drug">
                                    <option value="-">-</option>
                                    <option value="diureticos">Diuréticos</option>
                                    <option value="aas">AAS</option>
                                    <option value="bloqueadores_calcio">Bloqueadores de canais de cálcio</option>
                                    <option value="beta_bloqueadores">Beta bloqueadores</option>
                                    <option value="eca_inibidores">Inibidores de ECA</option>
                                    <option value="ansioliticos">Ansiolíticos</option>
                                    <option value="digoxina">Digoxina</option>
                                    <option value="anticoagulantes_orais">Anticoagulantes orais</option>
                                    <option value="anticoncepcionais">Anticoncepcionais</option>
                                    <option value="antipisicoticos">Antipisicóticos</option>
                                    <option value="outros">Outros</option>
                                </select>
                            </div>
                            <div class="col-sm-3">
                                <h5><label class="control-label">Data/Hora</label></h5>
                                <div class='input-group date input-append datetimepicker1'>
                                    <input type='text' class="form-control" data-format="yyyy-MM-dd hh:mm:ss" id="endo_drugs_date_hour" />
                                    <span class="input-group-addon add-on">
                                        <span><i class="fa fa-calendar" data-time-icon="icon-time" data-date-icon="icon-calendar">
                                            </i></span>
                                    </span>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <h5><label class="control-label"></label></h5>
                                <a onclick="add_endo_drugs()" class="btn btn-primary btn-block"><i class="fa fa-plus"></i> Adicionar</a>
                            </div>
                        </div>
                        </br>
                        <div class="row-fluid">
                            <div id="endo_drugs_list">

                            </div>
                        </div>
                    </div>
                </div>


                <!-- balanco hidrico -->
                <legend> 
                    <a href="javascript:void(0)" onclick="togglePanels('#hid_bal', '#hid_bal_options');">Balanço Hídrico</a>
                    <div id="hid_bal_chev" class="pull-right">
                        <a href="javascript:void(0)" onclick="togglePanels('#hid_bal', '#hid_bal_options');"><i class="fa fa-plus-square-o"></i></a>
                    </div>
                    <div id="hid_bal_chevback" style="display:none;" class="pull-right">
                        <a href="javascript:void(0)" onclick="togglePanels('#hid_bal', '#hid_bal_options');"><i class="fa fa-minus-square-o"></i></a>
                    </div>
                </legend>
                <div id="hid_bal_options" class="btn-group" style="display:none; width: 100%">
                    <div class="well">
                        <div class="row-fluid">
                            <h4>Entrada</h4>
                        </div>
                        <div class="row-fluid">
                            <div class="col-sm-6">
                                <h5><label class="control-label">Volume de cristalóides (ml)</label></h5>
                                <input class="form-control" type="number" id="cristaloides_volume_in" disabled="">
                            </div>

                            <div class="col-sm-6">
                                <h5><label class="control-label">Volume de hemodericados (ml)</label></h5>
                                <input class="form-control" type="number" id="hemoderivados_volume_in" disabled="">
                            </div>
                        </div>
                        </br>
                        <div class="row-fluid">
                            <h4>Saída</h4>
                        </div>
                        <div class="row-fluid">
                            <div class="col-sm-6">
                                <h5><label class="control-label">Diurese (ml)</label></h5>
                                <input class="form-control" type="number" id="diurese_out" value="<?= $water_balance['diurese_out'] ?>" > 
                            </div>

                            <div class="col-sm-6">
                                <h5><label class="control-label">Sonda gástrica (ml)</label></h5>
                                <input class="form-control" type="number" id="sonda_gast_out" value="<?= $water_balance['sonda_gast_out'] ?>">
                            </div>
                        </div>
                        <div class="row-fluid">
                            <div class="col-sm-6">
                                <h5><label class="control-label">Dreno direito (ml)</label></h5>
                                <input class="form-control" type="number" id="dreno_dir_out" value="<?= $water_balance['dreno_dir_out'] ?>">
                            </div>

                            <div class="col-sm-6">
                                <h5><label class="control-label">Dreno esquerdo (ml)</label></h5>
                                <input class="form-control" type="number" id="dreno_esq_out" value="<?= $water_balance['dreno_esq_out'] ?>">
                            </div>
                        </div>
                    </div>
                </div>

                <!-- resumo medico evolucao -->
                <legend>
                    <a href="javascript:void(0)" onclick="togglePanels('#res_med_evol', '#res_med_evol_options');">Resumo Médico/Evolução</a>
                    <div id="res_med_evol_chev" class="pull-right">
                        <a href="javascript:void(0)" onclick="togglePanels('#res_med_evol', '#res_med_evol_options');"><i class="fa fa-plus-square-o"></i></a>
                    </div>
                    <div id="res_med_evol_chevback" style="display:none;" class="pull-right">
                        <a href="javascript:void(0)" onclick="togglePanels('#res_med_evol', '#res_med_evol_options');"><i class="fa fa-minus-square-o"></i></a>
                    </div>
                </legend>
                <div id="res_med_evol_options" class="btn-group" style="display:none; width: 100%">
                    <div class="well">
                        <div class="row-fluid">
                            <div class="col-sm-12">
                                <div class="summernote" id="evolution_textarea"><?= $evolution['evolution'] ?></div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Situação de Atendimento -->
                <?php $this->load->view('attendance_hospital/attendance_situation'); ?>
            </div>
        </div>
    </div>
</div>

<a href="javascript:void(0)" class="btn btn-success btn-sm save_button" onclick="save_water_balance(); save_evolution();"><?= lang('save') ?></a>

<script type="text/javascript">
    jQuery('.datetimepicker1').datetimepicker({
        language: 'pt-BR'
    });
    jQuery(document).ready(function () {
        jQuery('.summernote').summernote({
            height: 150
        });
    });

    // carrega as listas que ja tem dados no banco
    jQuery('#patient_condition_list').load(jQuery("body").data("baseurl") + "attendance_hospital/get_patient_condition_list");
    jQuery('#endo_drugs_list').load(jQuery("body").data("baseurl") + "attendance_hospital/get_volemic_reposition_list/endo_drugs");
    jQuery('#hemo_list').load(jQuery("body").data("baseurl") + "attendance_hospital/get_volemic_reposition_list/hemo");
    jQuery('#cristaloides_list').load(jQuery("body").data("baseurl") + "attendance_hospital/get_volemic_reposition_list/cristaloides");

    jQuery(document).on('change', '#patient_condition_type', function () {
        if (jQuery('#patient_condition_type').val() === 'avaliacao') {
            jQuery('.patient_condition_local').show();
        } else if (jQuery('#patient_condition_type').val() === 'observacao') {
            jQuery('.patient_condition_local').show();
        } else if (jQuery('#patient_condition_type').val() === 'aguarda_leito') {
            jQuery('.patient_condition_local').show();
        } else if (jQuery('#patient_condition_type').val() === 'internado') {
            jQuery('.patient_condition_local').show();
        } else if (jQuery('#patient_condition_type').val() === 'reinternado') {
            jQuery('.patient_condition_local').show();
        } else {
            jQuery('.patient_condition_local').hide();
        }
    });

    function save_water_balance() {
        jQuery.ajax({
            url: jQuery("body").data("baseurl") + "attendance_hospital/save_water_balance",
            type: "post",
            dataType: 'json',
            data: {
                diurese_out: jQuery('#diurese_out').val(),
                sonda_gast_out: jQuery('#sonda_gast_out').val(),
                dreno_dir_out: jQuery('#dreno_dir_out').val(),
                dreno_esq_out: jQuery('#dreno_esq_out').val()
            },
            success: function (response) {
                jQuery('#modal_close_button').click();
                if (response.status === 'NOK') {
                    var notice = new PNotify({
                        title: 'Erro',
                        text: 'Tente novamente mais tarde',
                        type: 'error',
                        addclass: 'click-2-close',
                        hide: false,
                        buttons: {
                            closer: false,
                            sticker: false
                        }
                    });
                    notice.get().click(function () {
                        notice.remove();
                    });
                } else {
                    var notice = new PNotify({
                        title: 'Sucesso',
                        text: 'Balanço hídrico Salvo',
                        type: 'success',
                        addclass: 'click-2-close',
                        hide: false,
                        buttons: {
                            closer: false,
                            sticker: false
                        }
                    });
                    notice.get().click(function () {
                        notice.remove();
                    });
                }
            }
        });
    }

    function save_evolution() {
        jQuery.ajax({
            url: jQuery("body").data("baseurl") + "attendance_hospital/save_evolution",
            type: "post",
            dataType: 'json',
            data: {
                evolution: jQuery('#evolution_textarea').summernote('code')
            },
            success: function (response) {
                jQuery('#modal_close_button').click();
                if (response.status === 'NOK') {
                    var notice = new PNotify({
                        title: 'Erro',
                        text: 'Tente novamente mais tarde',
                        type: 'error',
                        addclass: 'click-2-close',
                        hide: false,
                        buttons: {
                            closer: false,
                            sticker: false
                        }
                    });
                    notice.get().click(function () {
                        notice.remove();
                    });
                } else {
                    var notice = new PNotify({
                        title: 'Sucesso',
                        text: 'Resumo Médico/Evolução Salvo',
                        type: 'success',
                        addclass: 'click-2-close',
                        hide: false,
                        buttons: {
                            closer: false,
                            sticker: false
                        }
                    });
                    notice.get().click(function () {
                        notice.remove();
                    });
                }
            }
        });
    }

    // adicionando cristaloide no banco e atualizando lista
    function add_cristaloides() {

        if (jQuery('#cristaloides_type').val() === '-') {
            var notice = new PNotify({
                title: 'Erro',
                text: 'É preciso escolher um tipo de cristalóide',
                type: 'error',
                addclass: 'click-2-close',
                hide: false,
                buttons: {
                    closer: false,
                    sticker: false
                }
            });
            notice.get().click(function () {
                notice.remove();
            });
            return false;
        }

        if (!jQuery('#cristaloides_volume').val()) {
            var notice = new PNotify({
                title: 'Erro',
                text: 'É preciso digitar o volume do cristalóide',
                type: 'error',
                addclass: 'click-2-close',
                hide: false,
                buttons: {
                    closer: false,
                    sticker: false
                }
            });
            notice.get().click(function () {
                notice.remove();
            });
            return false;
        }

        if (!jQuery('#cristaloides_date_hour').val()) {
            var notice = new PNotify({
                title: 'Erro',
                text: 'É preciso digitar a data/hora que o cristalóide foi dado',
                type: 'error',
                addclass: 'click-2-close',
                hide: false,
                buttons: {
                    closer: false,
                    sticker: false
                }
            });
            notice.get().click(function () {
                notice.remove();
            });
            return false;
        }

        jQuery.ajax({
            url: jQuery("body").data("baseurl") + "attendance_hospital/add_volemic_reposition",
            type: "post",
            dataType: 'json',
            data: {
                solution_type: jQuery('#cristaloides_type').val(),
                solution_detail: jQuery('#cristaloides_volume').val(),
                moment: jQuery('#cristaloides_date_hour').val(),
                aptm: false,
                solution: 'cristaloides'
            },
            success: function (response) {
                jQuery('#modal_close_button').click();
                if (response.status === 'NOK') {
                    var notice = new PNotify({
                        title: 'Erro',
                        text: 'Tente novamente mais tarde',
                        type: 'error',
                        addclass: 'click-2-close',
                        hide: false,
                        buttons: {
                            closer: false,
                            sticker: false
                        }
                    });
                    notice.get().click(function () {
                        notice.remove();
                    });
                } else {
                    // recarrega lista de cristaloides
                    jQuery('#cristaloides_list').load(jQuery("body").data("baseurl") + "attendance_hospital/get_volemic_reposition_list/cristaloides");
                }
            }
        });
    }

    // adicionando hemocomponente no banco e atualizando lista
    function add_hemo() {

        if (jQuery('#hemo_type').val() === '-') {
            var notice = new PNotify({
                title: 'Erro',
                text: 'É preciso escolher um tipo de hemocomponente',
                type: 'error',
                addclass: 'click-2-close',
                hide: false,
                buttons: {
                    closer: false,
                    sticker: false
                }
            });
            notice.get().click(function () {
                notice.remove();
            });
            return false;
        }

        if (!jQuery('#hemo_volume').val()) {
            var notice = new PNotify({
                title: 'Erro',
                text: 'É preciso digitar o volume do hemocomponente',
                type: 'error',
                addclass: 'click-2-close',
                hide: false,
                buttons: {
                    closer: false,
                    sticker: false
                }
            });
            notice.get().click(function () {
                notice.remove();
            });
            return false;
        }

        if (!jQuery('#hemo_date_hour').val()) {
            var notice = new PNotify({
                title: 'Erro',
                text: 'É preciso digitar a data/hora que o hemocomponente foi dado',
                type: 'error',
                addclass: 'click-2-close',
                hide: false,
                buttons: {
                    closer: false,
                    sticker: false
                }
            });
            notice.get().click(function () {
                notice.remove();
            });
            return false;
        }

        jQuery.ajax({
            url: jQuery("body").data("baseurl") + "attendance_hospital/add_volemic_reposition",
            type: "post",
            dataType: 'json',
            data: {
                solution_type: jQuery('#hemo_type').val(),
                solution_detail: jQuery('#hemo_volume').val(),
                moment: jQuery('#hemo_date_hour').val(),
                aptm: jQuery('#aptm').is(':checked'),
                solution: 'hemo'
            },
            success: function (response) {
                jQuery('#modal_close_button').click();
                if (response.status === 'NOK') {
                    var notice = new PNotify({
                        title: 'Erro',
                        text: 'Tente novamente mais tarde',
                        type: 'error',
                        addclass: 'click-2-close',
                        hide: false,
                        buttons: {
                            closer: false,
                            sticker: false
                        }
                    });
                    notice.get().click(function () {
                        notice.remove();
                    });
                } else {
                    // recarrega lista de hemo
                    jQuery('#hemo_list').load(jQuery("body").data("baseurl") + "attendance_hospital/get_volemic_reposition_list/hemo");
                }
            }
        });
    }

    // adicionando droga endovenosa no banco e atualizando lista
    function add_endo_drugs() {

        if (jQuery('#endo_drugs_type').val() === '-') {
            var notice = new PNotify({
                title: 'Erro',
                text: 'É preciso escolher um tipo de droga endovenosa',
                type: 'error',
                addclass: 'click-2-close',
                hide: false,
                buttons: {
                    closer: false,
                    sticker: false
                }
            });
            notice.get().click(function () {
                notice.remove();
            });
            return false;
        }

        if (!jQuery('#endo_drugs_drug').val()) {
            var notice = new PNotify({
                title: 'Erro',
                text: 'É preciso digitar o volume do droga endovenosa',
                type: 'error',
                addclass: 'click-2-close',
                hide: false,
                buttons: {
                    closer: false,
                    sticker: false
                }
            });
            notice.get().click(function () {
                notice.remove();
            });
            return false;
        }

        if (!jQuery('#endo_drugs_date_hour').val()) {
            var notice = new PNotify({
                title: 'Erro',
                text: 'É preciso digitar a data/hora que o droga endovenosa foi dado',
                type: 'error',
                addclass: 'click-2-close',
                hide: false,
                buttons: {
                    closer: false,
                    sticker: false
                }
            });
            notice.get().click(function () {
                notice.remove();
            });
            return false;
        }

        jQuery.ajax({
            url: jQuery("body").data("baseurl") + "attendance_hospital/add_volemic_reposition",
            type: "post",
            dataType: 'json',
            data: {
                solution_type: jQuery('#endo_drugs_type').val(),
                solution_detail: jQuery('#endo_drugs_drug').val(),
                moment: jQuery('#endo_drugs_date_hour').val(),
                aptm: false,
                solution: 'endo_drugs'
            },
            success: function (response) {
                jQuery('#modal_close_button').click();
                if (response.status === 'NOK') {
                    var notice = new PNotify({
                        title: 'Erro',
                        text: 'Tente novamente mais tarde',
                        type: 'error',
                        addclass: 'click-2-close',
                        hide: false,
                        buttons: {
                            closer: false,
                            sticker: false
                        }
                    });
                    notice.get().click(function () {
                        notice.remove();
                    });
                } else {
                    // recarrega lista de endo_drugs
                    jQuery('#endo_drugs_list').load(jQuery("body").data("baseurl") + "attendance_hospital/get_volemic_reposition_list/endo_drugs");
                }
            }
        });
    }

    function togglePanels(content, options) {
        jQuery(content).toggle();
        jQuery(content + '_chev').toggle();
        jQuery(content + '_chevback').toggle();
        jQuery(options).toggle();
    }
</script>