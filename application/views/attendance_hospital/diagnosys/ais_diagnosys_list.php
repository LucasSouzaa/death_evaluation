<div class="search-filter">
    <legend>Lesões Encontradas	(AIS)</legend>
</div>

<div class="well">
    <?php if (!empty($ais_diagnosys_list)): ?>
        <table class="table table-bordered table-striped mb-none" >
            <thead>
                <tr>
                    <th>ais 2005</th>
                    <th>Região do corpo</th>
                    <th>Descrição da lesão</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($ais_diagnosys_list as $ec): ?>
                    <tr>
                        <th><?= $ec['ais2005'] ?></th>
                        <th><?= $ec['regiao_corpo'] ?></th>
                        <th><?= $ec['descricao_lesao'] ?></th>
                        <th><a href="javascript:void(0)" onclick="delete_ais('<?= $this->encrypt->encode($ec['id']) ?>')" class="on-default remove-row" style=" margin-right: 5px; margin-left: 5px;"><i class="fa fa-trash-o"></i></a></th>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php else: ?>
        <h3>Nenhuma diagnóstico cadastrado</h3>
    <?php endif; ?>
</div>


<script>

    function delete_ais(id) {
        jQuery.ajax({
            url: jQuery("body").data("baseurl") + "attendance_hospital/delete_ais",
            type: "post",
            dataType: 'json',
            data: {
                id: id
            },
            success: function (response) {
                if (response.status === 'NOK') {
                    var notice = new PNotify({
                        title: 'Erro',
                        text: 'Tente novamente mais tarde',
                        type: 'error',
                        addclass: 'click-2-close',
                        hide: false,
                        buttons: {
                            closer: false,
                            sticker: false
                        }
                    });
                    notice.get().click(function () {
                        notice.remove();
                    });
                } else {
                    // recarrega lista de diagnosticos
                    jQuery('#ais_diagnosys_list').load(jQuery("body").data("baseurl") + "attendance_hospital/get_ais_diagnosys_list");
                }
            }
        });
    }
</script>