<div class="search-filter">
    <legend>CID Lesões</legend>
</div>
<div class="well">
    <?php if (!empty($i_diagnosys_list)): ?>
        <table class="table table-bordered table-striped mb-none" >
            <thead>
                <tr>
                    <th>Região</th>
                    <th><?= lang('chapter') ?></th>
                    <th><?= lang('group') ?></th>
                    <th><?= lang('category') ?></th>
                    <th><?= lang('sub_category') ?></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($i_diagnosys_list as $i): ?>
                    <tr>
                        <th><?= $i['region'] ?></th>
                        <th><?= $i['chapter_cid_field'] ?></th>
                        <th><?= $i['group_cid_field'] ?></th>
                        <th><?= $i['category_cid_field'] ?></th>
                        <th><?= $i['subcategory_cid_field'] ?></th>
                        <th><a href="javascript:void(0)" onclick="delete_i_cid('<?= $this->encrypt->encode($i['id']) ?>')" class="on-default remove-row" style=" margin-right: 5px; margin-left: 5px;"><i class="fa fa-trash-o"></i></a></th>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php else: ?>
        <h3>Nenhuma diagnóstico cadastrado</h3>
    <?php endif; ?>
</div>

<script>

    function delete_i_cid(id) {
        jQuery.ajax({
            url: jQuery("body").data("baseurl") + "attendance_hospital/delete_i_cid",
            type: "post",
            dataType: 'json',
            data: {
                id: id
            },
            success: function (response) {
                jQuery('#modal_close_button').click();
                if (response.status === 'NOK') {
                    var notice = new PNotify({
                        title: 'Erro',
                        text: 'Tente novamente mais tarde',
                        type: 'error',
                        addclass: 'click-2-close',
                        hide: false,
                        buttons: {
                            closer: false,
                            sticker: false
                        }
                    });
                    notice.get().click(function () {
                        notice.remove();
                    });
                } else {
                    // recarrega lista de diagnosticos
                    jQuery('#i_diagnosys_list').load(jQuery("body").data("baseurl") + "attendance_hospital/get_i_diagnosys_list");
                }
            }
        });
    }
</script>