<script src="<?= $this->config->base_url(JSPATH . 'cid10t.js') ?>"></script>

<div class="container-fluid">
    <div id="external_couses_diagnosis">
        <legend>Adicionar CID causas externas	</legend>
        <div class="well">
            <div id="external_couses_cid_diagnosis"> 
                <b>Capítulo:</b><br>
                <span id="capec"><input type="hidden" value="V01-Y98" name="ec_chapter_cid_field">Causas externas de morbidade e de mortalidade</span><br> 
                <b>Grupo:</b><br>
                <span id="gpec"><select style="width:100%" id="grupo" name="ec_group_cid_field" class="form-control" onchange="popcatec('V01-Y98', this.value, 'catec')"><option value="V01-V09">Pedestre traumatizado em um acidente de transporte</option><option value="V10-V19">Ciclista traumatizado em um acidente de transporte</option><option value="V20-V29">Motociclista traumatizado em um acidente de transporte</option><option value="V30-V39">Ocupante de triciclo motorizado traumatizado em um acidente de transporte</option><option value="V40-V49">Ocupante de um automóvel traumatizado em um acidente de transporte</option><option value="V50-V59">Ocupante de uma caminhonete traumatizado em um acidente de transporte</option><option value="V60-V69">Ocupante de um veículo de transporte pesado traumatizado em um acidente de transporte</option><option value="V70-V79">Ocupante de um ônibus traumatizado em um acidente de transporte</option><option value="V80-V89">Outros acidentes de transporte terrestre</option><option value="V90-V94">Acidentes de transporte por água</option><option value="V95-V97">Acidentes de transporte aéreo e espacial</option><option value="V98-V99">Outros acidentes de transporte e os não especificados</option><option value="W00-W19">Quedas</option><option value="W20-W49">Exposição a forças mecânicas inanimadas</option><option value="W50-W64">Exposição a forças mecânicas animadas</option><option value="W65-W74">Afogamento e submersão acidentais</option><option value="W75-W84">Outros riscos acidentais à respiração</option><option value="W85-W99">Exposição à corrente elétrica, à radiação e às temperaturas e pressões extremas do ambiente</option><option value="X00-X09">Exposição à fumaça, ao fogo e às chamas</option><option value="X10-X19">Contato com uma fonte de calor ou com substâncias quentes</option><option value="X20-X29">Contato com animais e plantas venenosos</option><option value="X30-X39">Exposição às forças da natureza</option><option value="X40-X49">Envenenamento [intoxicação] acidental por e exposição à substâncias nocivas</option><option value="X50-X57">Excesso de esforços, viagens e privações</option><option value="X58-X59">Exposição acidental a outros fatores e aos não especificados</option><option value="X60-X84">Lesões autoprovocadas intencionalmente</option><option value="X85-Y09">Agressões</option><option value="Y10-Y34">Eventos (fatos) cuja intenção é indeterminada</option><option value="Y35-Y36">Intervenções legais e operações de guerra</option><option value="Y40-Y59">Efeitos adversos de drogas, medicamentos e substâncias biológicas usadas com finalidade terapêutica</option><option value="Y60-Y69">Acidentes ocorridos em pacientes durante a prestação de cuidados médicos e cirúrgicos</option><option value="Y70-Y82">Incidentes adversos durante atos diagnósticos ou terapêuticos associados ao uso de dispositivos (aparelhos) médicos</option><option value="Y83-Y84">Reação anormal em paciente ou complicação tardia causadas por procedimentos cirúrgicos e outros procedimentos médicos sem menção de acidente ao tempo do procedimento</option><option value="Y85-Y89">Seqüelas de causas externas de morbidade e de mortalidade</option><option value="Y90-Y98">Fatores suplementares relacionados com as causas de morbidade e de mortalidade classificados em outra parte</option></select></span><br>
                <b>Categoria:</b><br>
                <span id="catec"></span></br>
                <b>Subcategoria:</b><br>
                <span id="scatec"></span></br>
                <a onclick="add_ec_cid()" class="btn btn-lg btn-primary btn-block"><i class="fa fa-plus"></i></a>
            </div>
        </div>

        <div id="ec_diagnosys_list">

        </div>
    </div>

    <div id="injury_diagnosis">
        <legend>Adicionar CID Lesões</legend>
        <div class="well" id="injury_cid_diagnosis">
            <b>Capítulo:</b><br>
            <span id="capi"><input type="hidden" value="S00-T98" name="i_chapter_cid_field">Lesões, envenenamento e algumas outras consequências de causas externas</span><br> 
            <b>Grupo:</b><br>
            <span id="gpi"><select style="width:100%" id="grupo" name="i_group_cid_field" class="form-control" onchange="popcati('S00-T98', this.value, 'cati')"><option value="S00-S09">Traumatismos da cabeça</option><option value="S10-S19">Traumatismos do pescoço</option><option value="S20-S29">Traumatismos do tórax</option><option value="S30-S39">Traumatismos do abdome, do dorso, da coluna lombar e da pelve</option><option value="S40-S49">Traumatismos do ombro e do braço</option><option value="S50-S59">Traumatismos do cotovelo e do antebraço</option><option value="S60-S69">Traumatismos do punho e da mão</option><option value="S70-S79">Traumatismos do quadril e da coxa</option><option value="S80-S89">Traumatismos do joelho e da perna</option><option value="S90-S99">Traumatismos do tornozelo e do pé</option><option value="T00-T07">Traumatismos envolvendo múltiplas regiões do corpo</option><option value="T08-T14">Traumatismos de localização não especificada do tronco, membro ou outra região do corpo</option><option value="T15-T19">Efeito da penetração de corpo estranho através de orifício natural</option><option value="T20-T32">Queimaduras e corrosões</option><option value="T33-T35">Geladuras [frostbite]</option><option value="T36-T50">Intoxicação por drogas, medicamentos e substâncias biológicas</option><option value="T51-T65">Efeitos tóxicos de substâncias de origem predominantemente não-medicinal</option><option value="T66-T78">Outros efeitos de causas externas e os não especificados</option><option value="T79-T79">Algumas complicações precoces de traumatismos</option><option value="T80-T88">Complicações de cuidados médicos e cirúrgicos, não classificados em outra parte</option><option value="T90-T98">Seqüelas de traumatismos, de intoxicações e de outras conseqüências das causas externas</option></select></span><br>
            <b>Categoria:</b><br>
            <span id="cati"> </span><br>
            <b>Subcategoria:</b><br>
            <span id="scati"> </span><br>
            <a onclick="add_i_cid()" class="btn btn-lg btn-primary btn-block"><i class="fa fa-plus"></i></a>
        </div>

        <div id="i_diagnosys_list">

        </div>
    </div>

    <div id="ais_diagnosis">
        <legend>Adicionar AIS</legend>
        <div class="well" id="ais_diagnosis">

            <div class="row-fluid">
                <div class="col-sm-12">
                    <h5><label class="control-label"><?= lang('') ?> Descrição da lesão</label></h5>
                    <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="ais_id">
                        <option value="0" selected > - </option>
                        <?php foreach ($aiss as $a): ?>
                            <option value="<?= $a['id'] ?>"><?= $a['descricao_lesao'] ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            </br>

            <a onclick="add_ais()" class="btn btn-lg btn-primary btn-block"><i class="fa fa-plus"></i></a>
        </div>

        <div id="ais_diagnosys_list">

        </div>
    </div>

</div>

<script>
    jQuery('#ec_diagnosys_list').load(jQuery("body").data("baseurl") + "attendance_hospital/get_ec_diagnosys_list");
    jQuery('#i_diagnosys_list').load(jQuery("body").data("baseurl") + "attendance_hospital/get_i_diagnosys_list");
    jQuery('#ais_diagnosys_list').load(jQuery("body").data("baseurl") + "attendance_hospital/get_ais_diagnosys_list");
//    setTimeout(jQuery("#gpec #grupo").change(), 500);
//    setTimeout(jQuery("#gpi #grupo").change(), 500);
//    setTimeout(jQuery("#catec #categoria").change(), 1500);
//    setTimeout(jQuery("#cati #categoria").change(), 1500);

    function add_ec_cid() {
        if (!jQuery('[name=ec_category_cid_field]').val()) {
            var notice = new PNotify({
                title: 'Erro',
                text: 'É preciso escolher uma categoria',
                type: 'error',
                addclass: 'click-2-close',
                hide: false,
                buttons: {
                    closer: false,
                    sticker: false
                }
            });
            notice.get().click(function () {
                notice.remove();
            });
            return false;
        }
        jQuery.ajax({
            url: jQuery("body").data("baseurl") + "attendance_hospital/save_add_ec_cid",
            type: "post",
            dataType: 'json',
            data: {
                chapter_cid_field: jQuery('[name=ec_chapter_cid_field]').val(),
                group_cid_field: jQuery('[name=ec_group_cid_field]').val(),
                category_cid_field: jQuery('[name=ec_category_cid_field]').val(),
                subcategory_cid_field: jQuery('[name=ec_subcategory_cid_field]').val()
            },
            success: function (response) {
                jQuery('#modal_close_button').click();
                if (response.status === 'NOK') {
                    var notice = new PNotify({
                        title: 'Erro',
                        text: 'Tente novamente mais tarde',
                        type: 'error',
                        addclass: 'click-2-close',
                        hide: false,
                        buttons: {
                            closer: false,
                            sticker: false
                        }
                    });
                    notice.get().click(function () {
                        notice.remove();
                    });
                } else {
                    // recarrega lista de diagnosticos
                    jQuery('#ec_diagnosys_list').load(jQuery("body").data("baseurl") + "attendance_hospital/get_ec_diagnosys_list");
                }
            }
        });
    }

    function add_ais() {

        jQuery.ajax({
            url: jQuery("body").data("baseurl") + "attendance_hospital/save_add_ais",
            type: "post",
            dataType: 'json',
            data: {
                ais_id: jQuery('#ais_id').val()
            },
            success: function (response) {
                if (response.status === 'NOK') {
                    var notice = new PNotify({
                        title: 'Erro',
                        text: 'Tente novamente mais tarde',
                        type: 'error',
                        addclass: 'click-2-close',
                        hide: false,
                        buttons: {
                            closer: false,
                            sticker: false
                        }
                    });
                    notice.get().click(function () {
                        notice.remove();
                    });
                } else {
                    // recarrega lista de diagnosticos
                    jQuery('#ais_diagnosys_list').load(jQuery("body").data("baseurl") + "attendance_hospital/get_ais_diagnosys_list");
                }
            }
        });
    }

    function add_i_cid() {

        if (!jQuery('[name=i_category_cid_field]').val()) {
            var notice = new PNotify({
                title: 'Erro',
                text: 'É preciso escolher uma categoria',
                type: 'error',
                addclass: 'click-2-close',
                hide: false,
                buttons: {
                    closer: false,
                    sticker: false
                }
            });
            notice.get().click(function () {
                notice.remove();
            });
            return false;
        }

        jQuery.ajax({
            url: jQuery("body").data("baseurl") + "attendance_hospital/save_add_i_cid",
            type: "post",
            dataType: 'json',
            data: {
                chapter_cid_field: jQuery('[name=i_chapter_cid_field]').val(),
                group_cid_field: jQuery('[name=i_group_cid_field]').val(),
                category_cid_field: jQuery('[name=i_category_cid_field]').val(),
                subcategory_cid_field: jQuery('[name=i_subcategory_cid_field]').val(),
                subcategory_cid_field_code: jQuery('[name=i_subcategory_cid_field]').find(':selected').data('othervalue')
            },
            success: function (response) {
                jQuery('#modal_close_button').click();
                if (response.status === 'NOK') {
                    var notice = new PNotify({
                        title: 'Erro',
                        text: 'Tente novamente mais tarde',
                        type: 'error',
                        addclass: 'click-2-close',
                        hide: false,
                        buttons: {
                            closer: false,
                            sticker: false
                        }
                    });
                    notice.get().click(function () {
                        notice.remove();
                    });
                } else {
                    // recarrega lista de diagnosticos
                    jQuery('#i_diagnosys_list').load(jQuery("body").data("baseurl") + "attendance_hospital/get_i_diagnosys_list");
                }
            }
        });
    }

</script>