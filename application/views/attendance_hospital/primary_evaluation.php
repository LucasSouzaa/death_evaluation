<style>
    .circle{
        border-radius: 50%;
        background-color: black;
        border: 1px solid black;
    }
    .circle:hover{
        background-color: gray;
        cursor: pointer;
        border: 1px solid gray;
        color: gray;
    }
    .circle p{
        font-size:12px;
        color:black;
        text-align:center;
        margin-top: -20px;
    }
    .t1{
        width: 1mm;
        height: 1mm; 
    }
    .t2{width: 2mm; height: 2mm;}
    .t3{width: 3mm; height: 3mm;}
    .t4{width: 4mm; height: 4mm;}
    .t5{width: 5mm; height: 5mm;}
    .t6{width: 6mm; height: 6mm;}
    .t7{width: 7mm; height: 7mm;}
    .t8{width: 8mm; height: 8mm;}
    .t9{width: 9mm; height: 9mm;}

</style>
<div class="pill-content">
    <div class="pill-pane active">
        <div class="container-fluid">
            <!-- A. vias aereas -->
            <legend>
                <a href="javascript:void(0)" onclick="togglePanels('#a_vias_aereas', '#a_vias_aereas_options');">A - Vias Aéreas</a>
                <div id="a_vias_aereas_chev" class="pull-right">
                    <a href="javascript:void(0)" onclick="togglePanels('#a_vias_aereas', '#a_vias_aereas_options');"><i class="fa fa-plus-square-o"></i></a>
                </div>
                <div id="a_vias_aereas_chevback" style="display:none;" class="pull-right">
                    <a href="javascript:void(0)" onclick="togglePanels('#a_vias_aereas', '#a_vias_aereas_options');"><i class="fa fa-minus-square-o"></i></a>
                </div>
            </legend>
            <div id="a_vias_aereas_options" class="btn-group" style="display:none; width: 100%">
                <div class="well" id="a">
                    <div class="row-fluid content">
                        <h3>Estado da Via Aérea</h3>
                        <div class="control-group">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" id="a_pervious" <?php if ($eval['a_pervious'] === 1): echo 'checked'; endif; ?> > Pérvias
                                </label>
                            </div>
                        </div>	

                        <div class="control-group">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" id="a_maintain_techniques" <?php if ($eval['a_maintain_techniques'] === 1): echo 'checked'; endif; ?> > Uso de técnicas de manutenção de via aérea
                                </label>
                            </div>
                        </div>

                        <div class="control-group">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" <?php if ($eval['a_definitive_airway'] === 1): echo 'checked'; endif; ?> onchange="if (jQuery('#a_definitive_airway').prop('checked')) {
                                                jQuery('#a_definitive_airway_label').html('- Checar posição do tubo traqueal');
                                            } else {
                                                jQuery('#a_definitive_airway_label').html('');
                                            }" id="a_definitive_airway" > Uso de via aérea definitiva
                                </label>
                                <label id="a_definitive_airway_label" style="color: red">

                                </label>
                            </div>
                        </div>

                        <div class="control-group">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" id="a_hoarseness_emphysema"  <?php if ($eval['a_hoarseness_emphysema'] === 1): echo 'checked'; endif; ?>> Presença de rouquidão e enfisema cervical subcutâneo
                                </label>
                            </div>
                        </div>

                        <div class="control-group">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" id="a_neck_brace" <?php if ($eval['a_neck_brace'] === 1): echo 'checked'; endif; ?>> Uso do colar cervical
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- B. ventilacao -->
            <legend> 
                <a href="javascript:void(0)" onclick="togglePanels('#b_ventilacao', '#b_ventilacao_options');">B - Ventilação</a>
                <div id="b_ventilacao_chev" class="pull-right">
                    <a href="javascript:void(0)" onclick="togglePanels('#b_ventilacao', '#b_ventilacao_options');"><i class="fa fa-plus-square-o"></i></a>
                </div>
                <div id="b_ventilacao_chevback" style="display:none;" class="pull-right">
                    <a href="javascript:void(0)" onclick="togglePanels('#b_ventilacao', '#b_ventilacao_options');"><i class="fa fa-minus-square-o"></i></a>
                </div>
            </legend>
            <div id="b_ventilacao_options" class="btn-group" style="display:none; width: 100%">
                <div class="well" id="b">
                    <div class="content">
                        <h3>Presença de:</h3>
                        <div class="row-fluid">
                            <div class="span12">
                                <h4>Inspeção</h4>
                            </div>
                        </div>
                        <div class="row-fluid">				
                            <div class="span3">
                                <div class="control-group">
                                    <label for="PrimaryAssessmentBTrachea" class="control-label">Traquéia</label>
                                    <div class="controls">
                                        <select class="form-control" id="b_trachea">
                                            <option value="">-</option>
                                            <option value="centrada" <?php if($eval['b_trachea'] === 'centrada'): echo 'selected'; endif; ?>>Centrada</option>
                                            <option value="com_desvio" <?php if($eval['b_trachea'] === 'com_desvio'): echo 'selected'; endif; ?>>Com desvio</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="span3">
                                <div class="control-group">
                                    </br>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" id="b_jugular_venous_distension" <?php if ($eval['b_jugular_venous_distension'] === 1): echo 'checked'; endif; ?>> Turgência jugular
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row-fluid">
                            <div class="span12">
                                <h4>Palpação</h4>
                            </div>
                        </div>
                        <div class="row-fluid">
                            <div class="span3">
                                <div class="control-group">
                                    <label for="PrimaryAssessmentBCrackle" class="control-label">Crepitação</label>
                                    <div class="controls">
                                        <select class="form-control" id="b_crackle">
                                            <option value="">-</option>
                                            <option value="hemitorax_direito" <?php if($eval['b_crackle'] === 'hemitorax_direito'): echo 'selected'; endif; ?>>Hemitórax direito</option>
                                            <option value="hemitorax_esquerdo" <?php if($eval['b_crackle'] === 'hemitorax_esquerdo'): echo 'selected'; endif; ?>>Hemitórax esquerdo</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="span3">
                                <div class="control-group">
                                    <label for="PrimaryAssessmentBCrackle" class="control-label">Dor</label>
                                    <div class="controls">
                                        <select class="form-control" id="b_pain">
                                            <option value="">-</option>
                                            <option value="hemitorax_direito" <?php if($eval['b_pain'] === 'hemitorax_direito'): echo 'selected'; endif; ?>>Hemitórax direito</option>
                                            <option value="hemitorax_esquerdo" <?php if($eval['b_pain'] === 'hemitorax_esquerdo'): echo 'selected'; endif; ?>>Hemitórax esquerdo</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row-fluid">
                            <div class="span12">
                                <h4>Percussão</h4>
                            </div>
                        </div>
                        <div class="row-fluid">
                            <div class="span3">
                                <div class="control-group">
                                    <label for="PrimaryAssessmentBHypertympanism" class="control-label">Hipertimpanismo</label>
                                    <div class="controls">
                                        <select class="form-control" id="b_hypertympanism">
                                            <option value="">-</option>
                                            <option value="hemitorax_direito" <?php if($eval['b_hypertympanism'] === 'hemitorax_direito'): echo 'selected'; endif; ?>>Hemitórax direito</option>
                                            <option value="hemitorax_esquerdo" <?php if($eval['b_hypertympanism'] === 'hemitorax_esquerdo'): echo 'selected'; endif; ?>>Hemitórax esquerdo</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="span3">
                                <div class="control-group">
                                    <label for="PrimaryAssessmentBUnderlying" class="control-label">Submacicez</label>
                                    <div class="controls">
                                        <select class="form-control" id="b_underlying">
                                            <option value="">-</option>
                                            <option value="hemitorax_direito" <?php if($eval['b_underlying'] === 'hemitorax_direito'): echo 'selected'; endif; ?>>Hemitórax direito</option>
                                            <option value="hemitorax_esquerdo" <?php if($eval['b_underlying'] === 'hemitorax_esquerdo'): echo 'selected'; endif; ?>>Hemitórax esquerdo</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row-fluid">
                            <div class="span12">
                                <h4>Ausculta</h4>
                            </div>
                        </div>
                        <div class="row-fluid">
                            <div class="span3">
                                <div class="control-group">
                                    <label for="PrimaryAssessmentBVesicularLeft" class="control-label">Murmúrio vesicular esquerdo</label>
                                    <div class="controls">
                                        <select class="form-control" id="b_vesicular_left">
                                            <option value="">-</option>
                                                <option value="normal" <?php if($eval['b_vesicular_left'] === 'hemitorax_direito'): echo 'selected'; endif; ?>>Normal</option>
                                            <option value="ausente" <?php if($eval['b_vesicular_left'] === 'hemitorax_esquerdo'): echo 'selected'; endif; ?>>Ausente</option>
                                            <option value="diminuido" <?php if($eval['b_vesicular_left'] === 'hemitorax_esquerdo'): echo 'selected'; endif; ?>>Diminuido</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="span3">
                                <div class="control-group">
                                    <label for="PrimaryAssessmentBVesicularRight" class="control-label">Murmúrio vesicular direito</label>
                                    <div class="controls">
                                        <select class="form-control" id="b_vesicular_right">
                                            <option value="">-</option>
                                                <option value="normal" <?php if($eval['b_vesicular_right'] === 'hemitorax_direito'): echo 'selected'; endif; ?>>Normal</option>
                                            <option value="ausente" <?php if($eval['b_vesicular_right'] === 'hemitorax_esquerdo'): echo 'selected'; endif; ?>>Ausente</option>
                                            <option value="diminuido" <?php if($eval['b_vesicular_right'] === 'hemitorax_esquerdo'): echo 'selected'; endif; ?>>Diminuido</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="span3">
                                <div class="control-group">
                                    <label for="PrimaryAssessmentBEstetores" class="control-label">Ruídos Adversos - Estetores</label>
                                    <div class="controls">
                                        <select class="form-control" id="b_estetores">
                                            <option value="">-</option>
                                            <option value="hemitorax_direito" <?php if($eval['b_estetores'] === 'hemitorax_direito'): echo 'selected'; endif; ?>>Hemitórax direito</option>
                                            <option value="hemitorax_esquerdo" <?php if($eval['b_estetores'] === 'hemitorax_esquerdo'): echo 'selected'; endif; ?>>Hemitórax esquerdo</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="span3">
                                <div class="control-group">
                                    <label for="PrimaryAssessmentBEstetores" class="control-label">Ruídos Adversos - Sibilos</label>
                                    <div class="controls">
                                        <select class="form-control" id="b_sibilos">
                                            <option value="">-</option>
                                            <option value="hemitorax_direito" <?php if($eval['b_sibilos'] === 'hemitorax_direito'): echo 'selected'; endif; ?>>Hemitórax direito</option>
                                            <option value="hemitorax_esquerdo" <?php if($eval['b_sibilos'] === 'hemitorax_esquerdo'): echo 'selected'; endif; ?>>Hemitórax esquerdo</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="span3">
                                <div class="control-group">
                                    <label for="PrimaryAssessmentBEstetores" class="control-label">Ruídos Adversos - Roncos</label>
                                    <div class="controls">
                                        <select class="form-control" id="b_roncos">
                                            <option value="">-</option>
                                            <option value="hemitorax_direito" <?php if($eval['b_roncos'] === 'hemitorax_direito'): echo 'selected'; endif; ?>>Hemitórax direito</option>
                                            <option value="hemitorax_esquerdo" <?php if($eval['b_roncos'] === 'hemitorax_esquerdo'): echo 'selected'; endif; ?>>Hemitórax esquerdo</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- C. circulacao -->
            <legend>
                <a href="javascript:void(0)" onclick="togglePanels('#c_circulacao', '#c_circulacao_options');">C - Circulação</a>
                <div id="c_circulacao_chev" class="pull-right">
                    <a href="javascript:void(0)" onclick="togglePanels('#c_circulacao', '#c_circulacao_options');"><i class="fa fa-plus-square-o"></i></a>
                </div>
                <div id="c_circulacao_chevback" style="display:none;" class="pull-right">
                    <a href="javascript:void(0)" onclick="togglePanels('#c_circulacao', '#c_circulacao_options');"><i class="fa fa-minus-square-o"></i></a>
                </div>
            </legend>
            <div id="c_circulacao_options" class="btn-group" style="display:none; width: 100%">
                <div class="well" id="c">
                    <div class="content">
                        <!-- sinais vitais -->
                        <div class="row-fluid">
                            <div class="span2">
                                <div class="control-group">
                                    <label for="VitalSign3Pa" class="control-label">PA</label>
                                    <div class="controls">
                                        <input class="form-control" type="text" id="pa" value="<?= isset($vit_sig['pa']) ? $vit_sig['pa'] : '0' ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="span2">
                                <div class="control-group">
                                    <label for="VitalSign3Fc" class="control-label">FC</label>
                                    <div class="controls">
                                        <input class="form-control" type="text" id="fc" value="<?= isset($vit_sig['fc']) ? $vit_sig['fc'] : '0' ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="span2">
                                <div class="control-group">
                                    <label for="VitalSign3Fr" class="control-label">FR</label>
                                    <div class="controls">
                                        <input class="form-control" type="text" id="fr" value="<?= isset($vit_sig['fr']) ? $vit_sig['fr'] : '0' ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="span2">
                                <div class="control-group">
                                    <label for="VitalSign3SatO2" class="control-label">Sat O2</label>
                                    <div class="controls">
                                        <input class="form-control" type="text" id="sato2" value="<?= isset($vit_sig['sato2']) ? $vit_sig['sato2'] : '0' ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="span2">
                                <div class="control-group">
                                    <label for="VitalSign3AxillaryTemperature" class="control-label">Temperatura axilar</label>
                                    <div class="controls">
                                        <input class="form-control" type="text" id="axillary_temperature" value="<?= isset($vit_sig['axillary_temperature']) ? $vit_sig['axillary_temperature'] : '0' ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="span2">
                                <div class="control-group">
                                    <label for="VitalSign3EsophagealTemperature" class="control-label">Temp. esofágica</label>
                                    <div class="controls">
                                        <input class="form-control" type="text" id="esophageal_temperature" value="<?= isset($vit_sig['esophageal_temperature']) ? $vit_sig['esophageal_temperature'] : '0' ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        </br>
                        <div class="row-fluid">
                            <div class="span4">
                                <div class="control-group">
                                    <label for="PrimaryAssessmentCSkin" class="control-label">Pele/Mucosas</label>
                                    <div class="controls">
                                        <select class="form-control" id="c_skin">
                                            <option value="">-</option>
                                            <option value="palidas" <?php if($eval['c_skin'] === 'palidas'): echo 'selected'; endif; ?>>Pálidas</option>
                                            <option value="cianoticas" <?php if($eval['c_skin'] === 'cianoticas'): echo 'selected'; endif; ?>>Cianóticas</option>
                                            <option value="coradas" <?php if($eval['c_skin'] === 'coradas'): echo 'selected'; endif; ?>>Coradas</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="span4">
                                <div class="control-group">
                                    <label for="PrimaryAssessmentCCapillaryFilling" class="control-label">Enchimento capilar</label>
                                    <div class="controls">
                                        <select class="form-control" id="c_capillary_refill">
                                            <option value="">-</option>
                                            <option value="normal" <?php if($eval['c_capillary_refill'] === 'normal'): echo 'selected'; endif; ?>>Normal</option>
                                            <option value="lentificado" <?php if($eval['c_capillary_refill'] === 'lentificado'): echo 'selected'; endif; ?>>Lentificado</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="span4">
                                <div class="control-group">
                                    <label for="PrimaryAssessmentCRadialPulse" class="control-label">Características do pulso radial</label>
                                    <div class="controls">
                                        <select class="form-control" id="c_characteristics_radial_pulse">
                                            <option value="">-</option>
                                            <option value="normal" <?php if($eval['c_characteristics_radial_pulse'] === 'normal'): echo 'selected'; endif; ?>>Normal</option>
                                            <option value="filiforme" <?php if($eval['c_characteristics_radial_pulse'] === 'filiforme'): echo 'selected'; endif; ?>>Filiforme</option>
                                            <option value="ausente" <?php if($eval['c_characteristics_radial_pulse'] === 'ausente'): echo 'selected'; endif; ?>>Ausente</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </br>
                        <div class="row-fluid">

                            <div class="span9" id="bleeding_lacation" style="display: block;">
                                <label for="" class=" radio inline">Presença de hemorragia externa? (Localização) </label>
                                <div class="control-group">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" id="c_bleeding_skull" <?php if ($eval['c_bleeding_skull'] === 1): echo 'checked'; endif; ?>> Crânio
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" id="c_bleeding_cervical" <?php if ($eval['c_bleeding_cervical'] === 1): echo 'checked'; endif; ?>> Cervical
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" id="c_bleeding_chest" <?php if ($eval['c_bleeding_chest'] === 1): echo 'checked'; endif; ?>> Tórax
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" id="c_bleeding_pelvis" <?php if ($eval['c_bleeding_pelvis'] === 1): echo 'checked'; endif; ?>> Bacia
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" id="c_bleeding_upper_limbs" <?php if ($eval['c_bleeding_upper_limbs'] === 1): echo 'checked'; endif; ?>> Membros superiores
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" id="c_bleeding_lower_limbs" <?php if ($eval['c_bleeding_lower_limbs'] === 1): echo 'checked'; endif; ?>> Membros inferiores
                                        </label>
                                    </div>
                                </div>	
                            </div>	
                            <div class="span3" style="display: block;">
                            </div>	
                        </div>
                    </div>
                </div>
            </div>
            <!-- D. estado neurologico -->
            <legend>
                <a href="javascript:void(0)" onclick="togglePanels('#d_estudo_neurologico', '#d_estudo_neurologico_options');">D - Estado Neurológico</a>
                <div id="d_estudo_neurologico_chev" class="pull-right">
                    <a href="javascript:void(0)" onclick="togglePanels('#d_estudo_neurologico', '#d_estudo_neurologico_options');"><i class="fa fa-plus-square-o"></i></a>
                </div>
                <div id="d_estudo_neurologico_chevback" style="display:none;" class="pull-right">
                    <a href="javascript:void(0)" onclick="togglePanels('#d_estudo_neurologico', '#d_estudo_neurologico_options');"><i class="fa fa-minus-square-o"></i></a>
                </div>
            </legend>
            <div id="d_estudo_neurologico_options" class="btn-group" style="display:none; width: 100%">
                <div class="well" id="d">
                    <div class="content">
                        <div class="row-fluid">
                            <div class="col-sm-3">
                                <h5><label class="control-label"><?= lang('ocular_response') ?></label></h5>
                                <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="glasgow_visual_response">
                                    <option value="0" selected> - </option>
                                    <option value="4" <?php if($vit_sig['glasgow_visual_response'] === '4'): echo 'selected'; endif; ?>>
                                        4 - <?= lang('he_opens_eyes_spontaneously') ?>
                                    </option>
                                    <option value="3" <?php if($vit_sig['glasgow_visual_response'] === '3'): echo 'selected'; endif; ?>>
                                        3 - <?= lang('open_your_eyes_response_call') ?>
                                    </option>
                                    <option value="2" <?php if($vit_sig['glasgow_visual_response'] === '2'): echo 'selected'; endif; ?>>
                                        2 - <?= lang('open_your_eyes_response_pain_stimulus') ?>
                                    </option>
                                    <option value="1" <?php if($vit_sig['glasgow_visual_response'] === '1'): echo 'selected'; endif; ?>>
                                        1 - <?= lang('do_not_open_your_eyes') ?>
                                    </option>
                                </select>
                            </div>

                            <div class="col-sm-3">
                                <h5><label class="control-label"><?= lang('verbal_response') ?></label></h5>
                                <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="glasgow_verbal_response">
                                    <option value="0" selected> - </option>
                                    <option value="5" <?php if($vit_sig['glasgow_verbal_response'] === '5'): echo 'selected'; endif; ?>>
                                        5 - <?= lang('oriented_talk_normally') ?>
                                    </option>
                                    <option value="4" <?php if($vit_sig['glasgow_verbal_response'] === '4'): echo 'selected'; endif; ?>>
                                        4 - <?= lang('confused_disoriented') ?>
                                    </option>
                                    <option value="3" <?php if($vit_sig['glasgow_verbal_response'] === '3'): echo 'selected'; endif; ?>>
                                        3 - <?= lang('pronounces_disconnected_words') ?>
                                    </option>
                                    <option value="2" <?php if($vit_sig['glasgow_verbal_response'] === '2'): echo 'selected'; endif; ?>>
                                        2 - <?= lang('emits_incomprehensible_sounds') ?>
                                    </option>
                                    <option value="1" <?php if($vit_sig['glasgow_verbal_response'] === '1'): echo 'selected'; endif; ?>>
                                        1 - <?= lang('muted') ?>
                                    </option>
                                </select>
                            </div>

                            <div class="col-sm-3">
                                <h5><label class="control-label"><?= lang('motor_response') ?></label></h5>
                                <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="glasgow_motor_response">
                                    <option value="0" selected> - </option>
                                    <option value="6" <?php if($vit_sig['glasgow_motor_response'] === '6'): echo 'selected'; endif; ?>>
                                        6 - <?= lang('obeys_commands') ?>
                                    </option>
                                    <option value="5" <?php if($vit_sig['glasgow_motor_response'] === '5'): echo 'selected'; endif; ?>>
                                        5 - <?= lang('locate_painful_stimuli') ?>
                                    </option>
                                    <option value="4" <?php if($vit_sig['glasgow_motor_response'] === '4'): echo 'selected'; endif; ?>>
                                        4 - <?= lang('nonspecific_flexion_reflex_withdrawal_painful_stimuli') ?>
                                    </option>
                                    <option value="3" <?php if($vit_sig['glasgow_motor_response'] === '3'): echo 'selected'; endif; ?>>
                                        3 - <?= lang('abnormal_flexion_painful_stimuli') ?>
                                    </option>
                                    <option value="2" <?php if($vit_sig['glasgow_motor_response'] === '2'): echo 'selected'; endif; ?>>
                                        2 - <?= lang('extension_painful_stimuli') ?>
                                    </option>
                                    <option value="1" <?php if($vit_sig['glasgow_motor_response'] === '1'): echo 'selected'; endif; ?>>
                                        1 - <?= lang('it_not_moves') ?>
                                    </option>
                                </select>
                            </div>
                            <div class="col-sm-3">
                                <h5><label class="control-label"> Escala de Glasgow</label></h5>
                                <input id="glasgow_score" type="number" class="form-control" value="<?= isset($vit_sig['glasgow_score']) ? $vit_sig['glasgow_score'] : '0' ?>" disabled=""/> 
                            </div>
                        </div>                
                        </br>
                        <div class="row-fluid">
                            <div class="span6">
                                <label for="">Fator de alteração de consciência:</label>
                                <div class="control-group">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" id="d_drugs" <?php if ($eval['d_drugs'] === 1): echo 'checked'; endif; ?>> Uso de drogas ilícitas
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" id="d_alcohol" <?php if ($eval['d_alcohol'] === 1): echo 'checked'; endif; ?>> Uso de álcool
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"  id="d_intubated" <?php if ($eval['d_intubated'] === 1): echo 'checked'; endif; ?>> Intubado
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" id="d_cns_depressants" <?php if ($eval['d_cns_depressants'] === 1): echo 'checked'; endif; ?>>Uso de medicamentos depressores do SNC
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" id="d_sedated" onchange="if (jQuery('#d_sedated').prop('checked')) {
                                                        jQuery('.ramsay_scale').css('display', 'block');
                                                    } else {
                                                        jQuery('.ramsay_scale').css('display', 'none');
                                                    }"> Sedado
                                        </label>
                                    </div>
                                    <label for="PrimaryAssessmentDRamsay" class="control-label ramsay_scale" style="display: none">Escala de Ramsay</label>
                                    <div class="controls ramsay_scale" style="display: none">
                                        <input min="1" max="6" class="span5 form-control" type="number" id="d_ramsay"  value="<?= isset($eval['d_ramsay']) ? $eval['d_ramsay'] : '0' ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row-fluid">
                            <div class="span6">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" id="anisocoria_present" onchange="if (jQuery('#anisocoria_present').prop('checked')) {
                                                    jQuery('.anisocoria_present_div').css('display', 'block');
                                                } else {
                                                    jQuery('.anisocoria_present_div').css('display', 'none');
                                                }"> Presença de anisocoria
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row-fluid pupil_size anisocoria_present_div" style="display: none">
                            <div class="span6">
                                <div class="control-group">
                                    <label for="PrimaryAssessmentDPupilRightSize" class="control-label">Tamanho da pupila direita</label>
                                    <div class="controls">
                                        <input class="form-control" min="2" max="9" placeholder="2-9" type="number" id="right_anisocoria" value="<?= isset($vit_sig['right_anisocoria']) ? $vit_sig['right_anisocoria'] : '0' ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="span6">
                                <div class="control-group">
                                    <label for="PrimaryAssessmentDPupilLeftSize" class="control-label">Tamanho da pupila esquerda</label>
                                    <div class="controls">
                                        <input class="form-control" min="2" max="9" placeholder="2-9" type="number" id="left_anisocoria" value="<?= isset($vit_sig['left_anisocoria']) ? $vit_sig['left_anisocoria'] : '0' ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        </br>
                        <div class="row-fluid pupil_size anisocoria_present_div" style="display: none">
                            <div class="span6" id="pupil_size_right" style="margin-top:15px;">
                                <div class="span1"><div class="circle t2"><p>2</p></div></div>
                                <div class="span1"><div class="circle t3"><p>3</p></div></div>
                                <div class="span1"><div class="circle t4"><p>4</p></div></div>
                                <div class="span1"><div class="circle t5"><p>5</p></div></div>
                                <div class="span1"><div class="circle t6"><p>6</p></div></div>
                                <div class="span1"><div class="circle t7"><p>7</p></div></div>
                                <div class="span1"><div class="circle t8"><p>8</p></div></div>
                                <div class="span1"><div class="circle t9"><p>9</p></div></div>
                            </div>
                            <div class="span6" id="pupil_size_right" style="margin-top:15px;">
                                <div class="span1"><div class="circle t2"><p>2</p></div></div>
                                <div class="span1"><div class="circle t3"><p>3</p></div></div>
                                <div class="span1"><div class="circle t4"><p>4</p></div></div>
                                <div class="span1"><div class="circle t5"><p>5</p></div></div>
                                <div class="span1"><div class="circle t6"><p>6</p></div></div>
                                <div class="span1"><div class="circle t7"><p>7</p></div></div>
                                <div class="span1"><div class="circle t8"><p>8</p></div></div>
                                <div class="span1"><div class="circle t9"><p>9</p></div></div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row-fluid">
                        <div class="span6">
                            <div class="span6">
                                <h5>Reação Pupilar - Direita</h5>
                                <div class="control-group">
                                    <label for="PrimaryAssessmentDPupilRightReaction" class="control-label">Reflexo fotomotor</label>
                                    <div class="controls">
                                        <select class="form-control" id="pupillary_reflex_right">
                                            <option value="">-</option>
                                            <option value="normal" <?php if($vit_sig['pupillary_reflex_right'] === 'normal'): echo 'selected'; endif; ?>>Normal</option>
                                            <option value="lento" <?php if($vit_sig['pupillary_reflex_right'] === 'lento'): echo 'selected'; endif; ?>>Lento</option>
                                            <option value="arreativo" <?php if($vit_sig['pupillary_reflex_right'] === 'arreativo'): echo 'selected'; endif; ?>>Arreativo</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="span6">
                                <h5>Reação Pupilar - Esquerda</h5>
                                <div class="control-group">
                                    <label for="PrimaryAssessmentDPupilLeftReaction" class="control-label">Reflexo fotomotor</label>
                                    <div class="controls">
                                        <select class="form-control" id="pupillary_reflex_left">
                                            <option value="">-</option>
                                            <option value="normal" <?php if($vit_sig['pupillary_reflex_left'] === 'normal'): echo 'selected'; endif; ?>>Normal</option>
                                            <option value="lento" <?php if($vit_sig['pupillary_reflex_left'] === 'lento'): echo 'selected'; endif; ?>>Lento</option>
                                            <option value="arreativo" <?php if($vit_sig['pupillary_reflex_left'] === 'arreativo'): echo 'selected'; endif; ?>>Arreativo</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="span6">	

                        </div>
                    </div>
                </div>
            </div>
            <!--E. exposição -->
            <legend>
                <a href="javascript:void(0)" onclick="togglePanels('#e_exposicao', '#e_exposicao_options');">E - Exposição</a>
                <div id="e_exposicao_chev" class="pull-right">
                    <a href="javascript:void(0)" onclick="togglePanels('#e_exposicao', '#e_exposicao_options');"><i class="fa fa-plus-square-o"></i></a>
                </div>
                <div id="e_exposicao_chevback" style="display:none;" class="pull-right">
                    <a href="javascript:void(0)" onclick="togglePanels('#e_exposicao', '#e_exposicao_options');"><i class="fa fa-minus-square-o"></i></a>
                </div>
            </legend>
            <div id="e_exposicao_options" class="btn-group" style="display:none; width: 100%">
                <div class="well" id="e">
                    <div class="content">
                        <div class="row-fluid">
                            <h5>Segmento corporal lesionado externamente</h5>
                            <div class="row-fluid">
                                <div class="control-group span6">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" id="e_skull" <?php if ($eval['e_skull'] === 1): echo 'checked'; endif; ?>>Crânio
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" id="e_face" <?php if ($eval['e_face'] === 1): echo 'checked'; endif; ?>>Face
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" id="e_cervical" <?php if ($eval['e_cervical'] === 1): echo 'checked'; endif; ?>>Pescoço
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" id="e_thorax" <?php if ($eval['e_thorax'] === 1): echo 'checked'; endif; ?>>Tórax
                                        </label>
                                    </div>
                                </div>
                                <div class="control-group span6">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" id="e_abdomen" <?php if ($eval['e_abdomen'] === 1): echo 'checked'; endif; ?>>Abdome
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" id="e_pelvis" <?php if ($eval['e_pelvis'] === 1): echo 'checked'; endif; ?>>Bacia
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" id="e_upper_limbs" <?php if ($eval['e_upper_limbs'] === 1): echo 'checked'; endif; ?>>Membros superiores
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" id="e_lower_limbs" <?php if ($eval['e_lower_limbs'] === 1): echo 'checked'; endif; ?>>Membros inferiores
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Procedimentos realizados -->
            <legend>
                <a href="javascript:void(0)" onclick="togglePanels('#procedures', '#procedures_options');">Procedimentos realizados</a>
                <div id="procedures_chev" class="pull-right">
                    <a href="javascript:void(0)" onclick="togglePanels('#procedures', '#procedures_options');"><i class="fa fa-plus-square-o"></i></a>
                </div>
                <div id="procedures_chevback" style="display:none;" class="pull-right">
                    <a href="javascript:void(0)" onclick="togglePanels('#procedures', '#procedures_options');"><i class="fa fa-minus-square-o"></i></a>
                </div>
            </legend>
            <div id="procedures_options" class="btn-group" style="display:none; width: 100%">
                <div class="well">
                    <h4>Procedimentos do atendimento hospitalar</h4>
                    <div class="row-fluid procedure_h">
                        <div class="col-sm-1 pull-left">
                            <a href="javascript:void(0)" onclick="jQuery('.procedure_h #acontent').toggle();" class="btn btn-default">A</a>
                        </div>
                        <div id="acontent" class="col-sm-11">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" id="oxign_dispo" name="group[]" class="show-div" data-target="oxign_dispo_c" <?php
                                    if (isset($procedures) && $procedures['oxign_dispo'] === 1): echo 'checked';
                                    endif;
                                    ?>> Dispositivo de oxigênio
                                </label>
                            </div>

                            <div id="oxign_dispo_c" style="margin-left: 30px; <?php
                            if (!isset($procedures) || $procedures['oxign_dispo'] === 0): echo 'display:none;';
                            endif;
                            ?>">
                                <h6><label class="control-label">Tipo de dispositivo</label></h6>
                                <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="oxdisptype">
                                    <option value="none" selected> - </option>
                                    <option value="cat" <?php
                                    if (isset($procedures) && $procedures['oxdisptype'] === 'cat'): echo 'selected';
                                    endif;
                                    ?>>Catéter</option>
                                    <option value="mascreserv" <?php
                                    if (isset($procedures) && $procedures['oxdisptype'] === 'mascreserv'): echo 'selected';
                                    endif;
                                    ?>>Máscara com reservatório</option>
                                    <option value="mascnonreserv" <?php
                                    if (isset($procedures) && $procedures['oxdisptype'] === 'mascnonreserv'): echo 'selected';
                                    endif;
                                    ?>>Máscara sem reservatório</option>
                                </select>
                            </div>

                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" id="breaf_vias" name="group[]" class="show-div" data-target="aer_via_c" <?php
                                    if (isset($procedures) && $procedures['breaf_vias'] === 1): echo 'checked';
                                    endif;
                                    ?>>  Via aérea
                                </label>
                            </div>

                            <div id="aer_via_c" style="margin-left: 30px;  <?php
                            if (!isset($procedures) || $procedures['breaf_vias'] === 0): echo 'display:none;';
                            endif;
                            ?>"> 
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" id="maintenance_tec" name="group[]" class="show-div" data-target="manut_tec_c"<?php
                                        if (isset($procedures) && $procedures['maintenance_tec'] === 1): echo 'checked';
                                        endif;
                                        ?>>  Técnicas de manutenção
                                    </label>
                                </div>

                                <div id="manut_tec_c" style="margin-left: 40px; <?php
                                if (!isset($procedures) || $procedures['maintenance_tec'] === 0): echo 'display:none;';
                                endif;
                                ?>">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" id="mt_oro" <?php
                                            if (isset($procedures) && $procedures['mt_oro'] === 1): echo 'checked';
                                            endif;
                                            ?>>  Orofaríngea
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" id="mt_naso" <?php
                                            if (isset($procedures) && $procedures['mt_naso'] === 1): echo 'checked';
                                            endif;
                                            ?>>  Nasofaríngea
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" id="mt_lar_mask" <?php
                                            if (isset($procedures) && $procedures['mt_lar_mask'] === 1): echo 'checked';
                                            endif;
                                            ?>>  Máscara laríngea
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" id="mt_comb" <?php
                                            if (isset($procedures) && $procedures['mt_comb'] === 1): echo 'checked';
                                            endif;
                                            ?>>  Combitube
                                        </label>
                                    </div>
                                </div>

                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" id="brief_def_via" name="group[]" class="show-div" data-target="ae_via_def_c" <?php
                                        if (isset($procedures) && $procedures['brief_def_via'] === 1): echo 'checked';
                                        endif;
                                        ?>>  Uso de Via aérea definitiva
                                    </label>
                                </div>

                                <div id="ae_via_def_c" style="margin-left: 40px;  <?php
                                if (!isset($procedures) || $procedures['brief_def_via'] === 0): echo 'display:none;';
                                endif;
                                ?>">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" id="bf_fst_int_seq" <?php
                                            if (isset($procedures) && $procedures['bf_fst_int_seq'] === 1): echo 'checked';
                                            endif;
                                            ?>>  Sequência rápida de intubação
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" id="bf_oro" <?php
                                            if (isset($procedures) && $procedures['bf_oro'] === 1): echo 'checked';
                                            endif;
                                            ?>>  Orotraqueal
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" id="bf_naso" <?php
                                            if (isset($procedures) && $procedures['bf_naso'] === 1): echo 'checked';
                                            endif;
                                            ?>>  Nasotraqueal
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" id="bf_cir" name="group[]" class="show-div" data-target="defin-aer-vie-cirurg" <?php
                                            if (isset($procedures) && $procedures['bf_cir'] === 1): echo 'checked';
                                            endif;
                                            ?>>  Cirúrgico
                                        </label>
                                    </div>

                                    <div id="defin-aer-vie-cirurg" style="margin-left: 40px;  <?php
                                    if (!isset($procedures) || $procedures['bf_cir'] === 0): echo 'display:none;';
                                    endif;
                                    ?>">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" id="bf_cir_cripun" <?php
                                                if (isset($procedures) && $procedures['bf_cir_cripun'] === 1): echo 'checked';
                                                endif;
                                                ?>>  Cricotireoidostomia por punção
                                            </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" id="bf_cir_cricir" <?php
                                                if (isset($procedures) && $procedures['bf_cir_cricir'] === 1): echo 'checked';
                                                endif;
                                                ?>>  Cricotireoidostomia cirúrgica
                                            </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" id="bf_cir_critraq" <?php
                                                if (isset($procedures) && $procedures['bf_cir_critraq'] === 1): echo 'checked';
                                                endif;
                                                ?>>  Traqueostomia
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" id="cervical_col" <?php
                                        if (isset($procedures) && $procedures['cervical_col'] === 1): echo 'checked';
                                        endif;
                                        ?>>  Colar cervical
                                    </label>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="row-fluid procedure_h">
                        <div class="col-sm-1 pull-left">
                            <a href="javascript:void(0)" onclick="jQuery('.procedure_h #bcontent').toggle();" class="btn btn-default">B</a>
                        </div>
                        <div id="bcontent" class="col-sm-11">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" id="mec_vent" name="group[]" class="show-div" data-target="mec_vent_c" <?php
                                    if (isset($procedures) && $procedures['mec_vent'] === 1): echo 'checked';
                                    endif;
                                    ?>> Ventilação
                                </label>
                            </div>

                            <div id="mec_vent_c" style="margin-left: 30px; <?php
                            if (!isset($procedures) || $procedures['mec_vent'] === 0): echo 'display:none;';
                            endif;
                            ?>"> 
                                <h6><label class="control-label">Tipo de ventilação</label></h6>
                                <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="mec_vent_type">
                                    <option value="none" selected> - </option>
                                    <option value="manual" <?php
                                    if (isset($procedures) && $procedures['mec_vent_type'] === 'manual'): echo 'selected';
                                    endif;
                                    ?>>Ventilação manual (Ambu)</option>
                                    <option value="mecanic" <?php
                                    if (isset($procedures) && $procedures['mec_vent_type'] === 'mecanic'): echo 'selected';
                                    endif;
                                    ?>>Ventilação mecânica</option>
                                </select>
                            </div>

                            <div id="mecanic_vent_c" style="margin-left: 40px; <?php
                            if (!isset($procedures) || $procedures['mec_vent_type'] !== 'mecanic'): echo 'display:none;';
                            endif;
                            ?>"> 
                                <h6><label class="control-label">Tipo de ventilação mecânica</label></h6>
                                <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="mecanic_vent_type">
                                    <option value="none" selected> - </option>
                                    <option value="invasiv" <?php
                                    if (isset($procedures) && $procedures['mecanic_vent_type'] === 'invasiv'): echo 'selected';
                                    endif;
                                    ?>>Invasiva</option>
                                    <option value="n_invasiv" <?php
                                    if (isset($procedures) && $procedures['mecanic_vent_type'] === 'n_invasiv'): echo 'selected';
                                    endif;
                                    ?>>Não Invasiva</option>
                                </select>
                            </div>

                            <div id="mecanic_vent_c_inv" style="margin-left: 40px; <?php
                            if (!isset($procedures) || $procedures['mecanic_vent_type'] !== 'invasiv'): echo 'display:none;';
                            endif;
                            ?>"> 
                                <h6><label class="control-label">Modo ventilatório</label></h6>
                                <input id="inv_mec_vent_mode" type="text" class="form-control" value="<?php
                                if (isset($procedures)): echo $procedures['inv_mec_vent_mode'];
                                endif;
                                ?>"/>

                                <h6><label class="control-label">Volume</label></h6>
                                <input id="inv_mec_vent_volume"  placeholder="ml/kg" maxlength="45" type="number" class="form-control" value="<?php
                                if (isset($procedures)): echo $procedures['inv_mec_vent_volume'];
                                endif;
                                ?>"/>

                                <h6><label class="control-label">FiO2</label></h6>
                                <input id="inv_mec_vent_fio2"  placeholder="21-100%" min="21" max="100" type="number" class="form-control" value="<?php
                                if (isset($procedures)): echo $procedures['inv_mec_vent_fio2'];
                                endif;
                                ?>"/>

                                <h6><label class="control-label">PEEP</label></h6>
                                <input id="inv_mec_vent_peep"  placeholder="5-60" min="5" max="60" type="number" class="form-control" value="<?php
                                if (isset($procedures)): echo $procedures['inv_mec_vent_peep'];
                                endif;
                                ?>"/>

                                <h6><label class="control-label">Freq. Resp.</label></h6>
                                <input id="inv_mec_vent_freq_resp"  placeholder="5-60" min="5" max="60" type="number" class="form-control" value="<?php
                                if (isset($procedures)): echo $procedures['inv_mec_vent_freq_resp'];
                                endif;
                                ?>"/>

                                <h6><label class="control-label">Pressão lim.</label></h6>
                                <input id="inv_mec_vent_press_lim"  placeholder="21-100'" min="21" max="100" type="number" class="form-control" value="<?php
                                if (isset($procedures)): echo $procedures['inv_mec_vent_press_lim'];
                                endif;
                                ?>"/>
                            </div>

                            <div id="mecanic_vent_c_n_inv" style="margin-left: 40px; <?php
                            if (!isset($procedures) || $procedures['mecanic_vent_type'] !== 'n_invasiv'): echo 'display:none;';
                            endif;
                            ?>"> 
                                <h6><label class="control-label">IPAP</label></h6>
                                <input id="n_inv_mec_vent_ipap" type="number" class="form-control" value="<?php
                                if (isset($procedures)): echo $procedures['n_inv_mec_vent_ipap'];
                                endif;
                                ?>"/>

                                <h6><label class="control-label">EPAP</label></h6>
                                <input id="n_inv_mec_vent_epap" type="number" class="form-control" value="<?php
                                if (isset($procedures)): echo $procedures['n_inv_mec_vent_epap'];
                                endif;
                                ?>"/>

                                <h6><label class="control-label">FiO2</label></h6>
                                <input id="n_inv_mec_vent_fio2" type="number" class="form-control" value="<?php
                                if (isset($procedures)): echo $procedures['n_inv_mec_vent_fio2'];
                                endif;
                                ?>"/>

                                <h6><label class="control-label">Freq. Resp.</label></h6>
                                <input id="n_inv_mec_vent_freq_resp" type="number" class="form-control" value="<?php
                                if (isset($procedures)): echo $procedures['n_inv_mec_vent_freq_resp'];
                                endif;
                                ?>"/>
                            </div>

                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" id="pun_pleu" name="group[]" class="show-div" data-target="pler-punc" <?php
                                    if (isset($procedures) && $procedures['pun_pleu'] === 1): echo 'checked';
                                    endif;
                                    ?>>  Punção pleural
                                </label>
                            </div>

                            <div id="pler-punc" style="margin-left: 30px; <?php
                            if (!isset($procedures) || $procedures['pun_pleu'] === 0): echo 'display:none;';
                            endif;
                            ?>">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" id="pun_pleu_l" <?php
                                        if (isset($procedures) && $procedures['pun_pleu_l'] === 1): echo 'checked';
                                        endif;
                                        ?>>  Esquerda
                                    </label>
                                </div>

                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" id="pun_pleu_r" <?php
                                        if (isset($procedures) && $procedures['pun_pleu_r'] === 1): echo 'checked';
                                        endif;
                                        ?>>  Direita
                                    </label>
                                </div>
                            </div>

                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" id="pleu_dren" name="group[]" class="show-div" data-target="pler-dren" <?php
                                    if (isset($procedures) && $procedures['pleu_dren'] === 1): echo 'checked';
                                    endif;
                                    ?>>  Uso de dreno pleural
                                </label>
                            </div>

                            <div id="pler-dren" style="margin-left: 30px; <?php
                            if (!isset($procedures) || $procedures['pleu_dren'] === 0): echo 'display:none;';
                            endif;
                            ?>">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" id="pleu_dren_l" name="group[]" class="show-div" data-target="pler-dren-left" <?php
                                        if (isset($procedures) && $procedures['pleu_dren_l'] === 1): echo 'checked';
                                        endif;
                                        ?>>  Esquerda
                                    </label>
                                </div>
                                <div id="pler-dren-left" style=" <?php
                                if (!isset($procedures) || $procedures['pleu_dren_l'] === 0): echo 'display:none;';
                                endif;
                                ?>">
                                    <h6><label class="control-label">Quantidade (ml)</label></h6>
                                    <input id="pleu_dren_lv" type="text" class="form-control" value="<?php
                                    if (isset($procedures)): echo $procedures['pleu_dren_lv'];
                                    endif;
                                    ?>"/>
                                </div>

                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" id="pleu_dren_r" name="group[]" class="show-div" data-target="pler-dren-rig" <?php
                                        if (isset($procedures) && $procedures['pleu_dren_r'] === 1): echo 'checked';
                                        endif;
                                        ?>>  Direita
                                    </label>
                                </div>
                                <div id="pler-dren-rig" style=" <?php
                                if (!isset($procedures) || $procedures['pleu_dren_r'] === 0): echo 'display:none;';
                                endif;
                                ?>">
                                    <h6><label class="control-label">Quantidade (ml)</label></h6>
                                    <input id="pleu_dren_rv" type="text" class="form-control" value="<?php
                                    if (isset($procedures)): echo $procedures['pleu_dren_rv'];
                                    endif;
                                    ?>"/>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row-fluid procedure_h">
                        <div class="col-sm-1 pull-left">
                            <a href="javascript:void(0)" onclick="jQuery('.procedure_h #ccontent').toggle();" class="btn btn-default">C</a>
                        </div>
                        <div id="ccontent" class="col-sm-11">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" id="extern_hem" name="group[]" class="show-div" data-target="extern_hem_c" <?php
                                    if (isset($procedures) && $procedures['extern_hem'] === 1): echo 'checked';
                                    endif;
                                    ?>>  Presença de hemorragia externa
                                </label>
                            </div>
                            <div id="extern_hem_c" style="margin-left: 30px; <?php
                            if (!isset($procedures) || $procedures['extern_hem'] === 0): echo 'display:none;';
                            endif;
                            ?>">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" id="extern_hem_head_face" <?php
                                        if (isset($procedures) && $procedures['extern_hem_head_face'] === 1): echo 'checked';
                                        endif;
                                        ?>>  Cabeça e face
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" id="extern_hem_neck" <?php
                                        if (isset($procedures) && $procedures['extern_hem_neck'] === 1): echo 'checked';
                                        endif;
                                        ?>>  Pescoço
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" id="extern_hem_chest" <?php
                                        if (isset($procedures) && $procedures['extern_hem_chest'] === 1): echo 'checked';
                                        endif;
                                        ?>>  Tórax
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" id="extern_hem_ab" <?php
                                        if (isset($procedures) && $procedures['extern_hem_ab'] === 1): echo 'checked';
                                        endif;
                                        ?>>  Abdome
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" id="extern_hem_pelvper" <?php
                                        if (isset($procedures) && $procedures['extern_hem_pelvper'] === 1): echo 'checked';
                                        endif;
                                        ?>>  Pelve/Períneo
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" id="extern_hem_mi" <?php
                                        if (isset($procedures) && $procedures['extern_hem_mi'] === 1): echo 'checked';
                                        endif;
                                        ?>>  Membros inferiores
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" id="extern_hem_ms" <?php
                                        if (isset($procedures) && $procedures['extern_hem_ms'] === 1): echo 'checked';
                                        endif;
                                        ?>>  Membros superiores
                                    </label>
                                </div>
                            </div>

                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" id="intern_hem" name="group[]" class="show-div" data-target="intern_hem_c" <?php
                                    if (isset($procedures) && $procedures['intern_hem'] === 1): echo 'checked';
                                    endif;
                                    ?>>  Pesquisa de hemorragia interna
                                </label>
                            </div>
                            <div id="intern_hem_c" style="margin-left: 30px; <?php
                            if (!isset($procedures) || $procedures['intern_hem'] === 0): echo 'display:none;';
                            endif;
                            ?>">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" id="intern_hem_rx_sp_torax" <?php
                                        if (isset($procedures) && $procedures['intern_hem_rx_sp_torax'] === 1): echo 'checked';
                                        endif;
                                        ?>>  Radiografia simples de tórax
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" id="intern_hem_rx_sp_pelve" <?php
                                        if (isset($procedures) && $procedures['intern_hem_rx_sp_pelve'] === 1): echo 'checked';
                                        endif;
                                        ?>>  Radiografia simples de pelve
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" id="intern_hem_ux_fast" <?php
                                        if (isset($procedures) && $procedures['intern_hem_ux_fast'] === 1): echo 'checked';
                                        endif;
                                        ?>>  Ultrasonografia FAST
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" id="intern_hem_lav_perit_diag" <?php
                                        if (isset($procedures) && $procedures['intern_hem_lav_perit_diag'] === 1): echo 'checked';
                                        endif;
                                        ?>>  Lavado peritoneal diagnóstico
                                    </label>
                                </div>
                            </div>

                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" id="vasc_access_per" name="group[]" class="show-div" data-target="vasc_access_per_c" <?php
                                    if (isset($procedures) && $procedures['vasc_access_per'] === 1): echo 'checked';
                                    endif;
                                    ?>>  Acesso vascular periférico
                                </label>
                            </div>
                            <div id="vasc_access_per_c" style="margin-left: 30px;  <?php
                            if (!isset($procedures) || $procedures['vasc_access_per'] === 0): echo 'display:none;';
                            endif;
                            ?>">
                                <h6><label class="control-label">Tipo</label></h6>
                                <select data-plugin-selectTwo class="form-control populate input-medium mb-md" name="parent_selection" id="vasc_access_per_disptype">
                                    <option value="none" selected> - </option>
                                    <option value="ven_pun" <?php
                                    if (isset($procedures) && $procedures['vasc_access_per_disptype'] === 'ven_pun'): echo 'selected';
                                    endif;
                                    ?>>Punção venosa</option>
                                    <option value="intraoss_pun" <?php
                                    if (isset($procedures) && $procedures['vasc_access_per_disptype'] === 'intraoss_pun'): echo 'selected';
                                    endif;
                                    ?>>Punção intraóssea</option>
                                    <option value="dissec" <?php
                                    if (isset($procedures) && $procedures['vasc_access_per_disptype'] === 'dissec'): echo 'selected';
                                    endif;
                                    ?>>Dissecção</option>
                                </select>

                                <h6><label class="control-label">Região</label></h6>
                                <select data-plugin-selectTwo class="form-control populate input-medium mb-md" name="parent_selection" id="vasc_access_per_reg">
                                    <option value="none" selected> - </option>
                                    <option value="msd" <?php
                                    if (isset($procedures) && $procedures['vasc_access_per_reg'] === 'msd'): echo 'selected';
                                    endif;
                                    ?>>Membro superior direito</option>
                                    <option value="mse" <?php
                                    if (isset($procedures) && $procedures['vasc_access_per_reg'] === 'mse'): echo 'selected';
                                    endif;
                                    ?>>Membro superior esquerdo</option>
                                    <option value="mid" <?php
                                    if (isset($procedures) && $procedures['vasc_access_per_reg'] === 'mid'): echo 'selected';
                                    endif;
                                    ?>>Membro inferior direito</option>
                                    <option value="mie" <?php
                                    if (isset($procedures) && $procedures['vasc_access_per_reg'] === 'mie'): echo 'selected';
                                    endif;
                                    ?>>Membro inferior esquerdo</option>
                                </select>
                            </div>

                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" id="volemic_infus" name="group[]" class="show-div" data-target="volemic_infus_c" <?php
                                    if (isset($procedures) && $procedures['volemic_infus'] === 1): echo 'checked';
                                    endif;
                                    ?>>  Infusão volêmica
                                </label>
                            </div>
                            <div id="volemic_infus_c" style="margin-left: 30px; <?php
                            if (!isset($procedures) || $procedures['volemic_infus'] === 0): echo 'display:none;';
                            endif;
                            ?>">
                                <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="aphm-regulation-ntrauma">
                                    <option value="ringer" <?php
                                    if (isset($procedures) && $procedures['volemic_infus_type'] === 'ringer'): echo 'selected';
                                    endif;
                                    ?>> Ringer lactato</option>
                                    <option value="soro" <?php
                                    if (isset($procedures) && $procedures['volemic_infus_type'] === 'soro'): echo 'selected';
                                    endif;
                                    ?>>Soro lactato</option>
                                </select>

                                <h6><label class="control-label">Quantidade (ml)</label></h6>
                                <input id="volemic_infus_qt" type="text" class="form-control" value="<?php
                                if (isset($procedures)): echo $procedures['volemic_infus_qt'];
                                endif;
                                ?>"/>

                            </div>

                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" id="ressus_cardio" name="group[]" class="show-div" data-target="cardiopulm-ressuc" <?php
                                    if (isset($procedures) && $procedures['ressus_cardio'] === 1): echo 'checked';
                                    endif;
                                    ?>>  Ressuscitação cardiopulmonar
                                </label>
                            </div>
                            <div id="cardiopulm-ressuc" style="margin-left: 30px;  <?php
                            if (!isset($procedures) || $procedures['ressus_cardio'] === 0): echo 'display:none;';
                            endif;
                            ?>">
                                <h6><label class="control-label">Número de paradas cardíacas</label></h6>
                                <input id="ressus_cardio_v" type="text" class="form-control" value="<?php
                                if (isset($procedures)): echo $procedures['ressus_cardio_v'];
                                endif;
                                ?>"/>

                                <h5><label class="control-label"><?= lang('duration') ?></label></h5>
                                <input id="ressus_cardio_d" type="text" class="form-control" value="<?php
                                if (isset($procedures)): echo $procedures['ressus_cardio_d'];
                                endif;
                                ?>"/> 
                            </div>
                        </div>
                    </div>
                    <?php if ($attendance_type === 1): ?>
                        <div class="row-fluid procedure_h">
                            <div class="col-sm-1 pull-left">
                                <a href="javascript:void(0)" onclick="jQuery('.procedure_h #dcontent').toggle();" class="btn btn-default">D</a>
                            </div>
                            <div id="dcontent" class="col-sm-11">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" id="board_imo" <?php
                                        if (isset($procedures) && $procedures['board_imo'] === 1): echo 'checked';
                                        endif;
                                        ?>>  Imobilização em prancha longa
                                    </label>
                                </div>

                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" id="memb_imob" name="group[]" class="show-div" data-target="mem_imob_c" <?php
                                        if (isset($procedures) && $procedures['memb_imob'] === 1): echo 'checked';
                                        endif;
                                        ?>>  Imobilização dos membros
                                    </label>
                                </div>
                                <div id="mem_imob_c" style="margin-left: 30px; <?php
                                if (!isset($procedures) || $procedures['memb_imob'] === 0): echo 'display:none;';
                                endif;
                                ?>">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" id="memb_imob_sd" <?php
                                            if (isset($procedures) && $procedures['memb_imob_sd'] === 1): echo 'checked';
                                            endif;
                                            ?>>  Membro superior direito
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" id="memb_imob_se" <?php
                                            if (isset($procedures) && $procedures['memb_imob_se'] === 1): echo 'checked';
                                            endif;
                                            ?>>  Membro superior esquerdo
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" id="memb_imob_id" <?php
                                            if (isset($procedures) && $procedures['memb_imob_id'] === 1): echo 'checked';
                                            endif;
                                            ?>>  Membro inferior direito
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" id="memb_imob_ie" <?php
                                            if (isset($procedures) && $procedures['memb_imob_ie'] === 1): echo 'checked';
                                            endif;
                                            ?>>  Membro inferior esquerdo
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" id="memb_imob_pelv" <?php
                                            if (isset($procedures) && $procedures['memb_imob_pelv'] === 1): echo 'checked';
                                            endif;
                                            ?>>  Pelve
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    </br>
                    <h4>Procedimentos do atendimento pré hospitalar fixo</h4>
                    <div class="row-fluid procedure_pre_f">
                        <div class="col-sm-1 pull-left">
                            <a href="javascript:void(0);" onclick="jQuery('.procedure_pre_f #acontent').toggle();" class="btn btn-default">A</a>
                        </div>

                        <div id="acontent" class="col-sm-11" >
                            </br>
                            <?php if ($procedures_unit1['oxign_dispo'] === 1): ?>
                                <label>
                                    Dispositivo de oxigênio
                                </label>
                                <div id="oxign_dispo_c" style="margin-left: 15px ;"> 
                                    <h6><label class="control-label">Tipo de dispositivo</label></h6>
                                    <?= lang($procedures_unit1['o2_disp_type']) ?>
                                </div>
                            <?php endif; ?>

                            <?php if ($procedures_unit1['breaf_vias'] === 1): ?>
                                <label>Via aérea</label>
                            <?php endif; ?>

                            <div id="aer_via_c" style="margin-left: 15px "> 

                                <?php if ($procedures_unit1['maintenance_tec'] === 1): ?>
                                    <label>Técnicas de manutenção</label>
                                <?php endif; ?>


                                <div id="manut_tec_c" style="margin-left: 40px;">
                                    <?php if ($procedures_unit1['mt_oro'] === 1): ?>
                                        <label>Orofaríngea</label>
                                    <?php endif; ?>
                                    <?php if ($procedures_unit1['mt_naso'] === 1): ?>
                                        <label>Nasofaríngea</label>
                                    <?php endif; ?>
                                    <?php if ($procedures_unit1['mt_lar_mask'] === 1): ?>
                                        <label>Máscara laríngea</label>
                                    <?php endif; ?>
                                    <?php if ($procedures_unit1['mt_comb'] === 1): ?>
                                        <label>Combitube</label>
                                    <?php endif; ?>

                                </div>

                                <?php if ($procedures_unit1['brief_def_via'] === 1): ?>
                                    <label>Uso de Via aérea definitiva</label>
                                <?php endif; ?>


                                <div id="ae_via_def_c" style="margin-left: 40px;">
                                    <?php if ($procedures_unit1['bf_fst_int_seq'] === 1): ?>
                                        <label>Sequência rápida de intubação</label>
                                    <?php endif; ?>
                                    <?php if ($procedures_unit1['bf_oro'] === 1): ?>
                                        <label>Orotraqueal</label>
                                    <?php endif; ?>
                                    <?php if ($procedures_unit1['bf_naso'] === 1): ?>
                                        <label>Nasotraqueal</label>
                                    <?php endif; ?>
                                    <?php if ($procedures_unit1['bf_cir'] === 1): ?>
                                        <label>Cirúrgico</label>
                                    <?php endif; ?>

                                    <div id="defin-aer-vie-cirurg" style="margin-left: 40px;">
                                        <?php if ($procedures_unit1['bf_cir_cripun'] === 1): ?>
                                            <label>Cricotireoidostomia por punção</label>
                                        <?php endif; ?>
                                        <?php if ($procedures_unit1['bf_cir_cricir'] === 1): ?>
                                            <label>Cricotireoidostomia cirúrgica</label>
                                        <?php endif; ?>
                                        <?php if ($procedures_unit1['bf_cir_critraq'] === 1): ?>
                                            <label>Traqueostomia</label>
                                        <?php endif; ?>
                                    </div>
                                </div>

                                <?php if ($procedures_unit1['cervical_col'] === 1): ?>
                                    <label>Colar cervical</label>
                                <?php endif; ?>

                            </div>
                        </div>
                    </div>

                    <div class="row-fluid procedure_pre_f">
                        <div class="col-sm-1 pull-left">
                            <a href="javascript:void(0);" onclick="jQuery('.procedure_pre_f #bcontent').toggle();" class="btn btn-default">B</a>
                        </div>

                        <div id="bcontent" class="col-sm-11" >
                            </br>
                            <?php if ($procedures_unit1['mec_vent'] === 1): ?>
                                <label>Ventilação</label>
                                <div id="mec_vent_c" style="margin-left: 15px"> 
                                    <h6><label class="control-label">Tipo de ventilação</label></h6>
                                    <?= lang($procedures_unit1['mec_vent_type']) ?>
                                </div>
                            <?php endif; ?>

                            <?php if ($procedures_unit1['pun_pleu'] === 1): ?>
                                <label>Punção pleural</label>
                            <?php endif; ?>

                            <div id="pler-punc" style="margin-left: 15px ;">
                                <?php if ($procedures_unit1['pun_pleu_l'] === 1): ?>
                                    <label>Esquerda</label>
                                <?php endif; ?>
                                <?php if ($procedures_unit1['pun_pleu_r'] === 1): ?>
                                    <label>Direita</label>
                                <?php endif; ?>

                            </div>

                            <?php if ($procedures_unit1['pleu_dren'] === 1): ?>
                                <label>Uso de dreno pleural</label>
                            <?php endif; ?>

                            <div id="pler-dren" style="margin-left: 15px ;">
                                <?php if ($procedures_unit1['pleu_dren_l'] === 1): ?>
                                    <label>Esquerda</label>
                                <?php endif; ?>

                                <?php if ($procedures_unit1['pleu_dren_l'] === 1): ?>
                                    <div id="pler-dren-left" > 
                                        <h6><label class="control-label">Quantidade (ml)</label></h6>
                                        <?= $procedures_unit1['pleu_dren_lv']; ?>
                                    </div>
                                <?php endif; ?>
                                <?php if ($procedures_unit1['pleu_dren_r'] === 1): ?>
                                    <label>Direita</label>
                                <?php endif; ?>

                                <?php if ($procedures_unit1['pleu_dren_r'] === 1): ?>
                                    <div id="pler-dren-rig" > 
                                        <h6><label class="control-label">Quantidade (ml)</label></h6>
                                        <?= $procedures_unit1['pleu_dren_rv']; ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>

                    <div class="row-fluid procedure_pre_f">
                        <div class="col-sm-1 pull-left">
                            <a href="javascript:void(0);" onclick="jQuery('.procedure_pre_f #ccontent').toggle();" class="btn btn-default">C</a>
                        </div>

                        <div id="ccontent" class="col-sm-11" >
                            </br>
                            <?php if ($procedures_unit1['extern_hem'] === 1): ?>
                                <label>Presença de hemorragia externa</label>
                            <?php endif; ?>

                            <div id="extern_hem_c" style="margin-left: 15px ;">
                                <?php if ($procedures_unit1['extern_hem_head_face'] === 1): ?>
                                    <label> Cabeça e face</label>
                                <?php endif; ?>
                                <?php if ($procedures_unit1['extern_hem_neck'] === 1): ?>
                                    <label> Pescoço</label>
                                <?php endif; ?>
                                <?php if ($procedures_unit1['extern_hem_chest'] === 1): ?>
                                    <label> Tórax</label>
                                <?php endif; ?>
                                <?php if ($procedures_unit1['extern_hem_ab'] === 1): ?>
                                    <label> Abdome</label>
                                <?php endif; ?>
                                <?php if ($procedures_unit1['extern_hem_pelvper'] === 1): ?>
                                    <label> Pelve/Períneo</label>
                                <?php endif; ?>
                                <?php if ($procedures_unit1['extern_hem_mi'] === 1): ?>
                                    <label> Membros inferiores</label>
                                <?php endif; ?>
                                <?php if ($procedures_unit1['extern_hem_ms'] === 1): ?>
                                    <label> Membros superiores</label>
                                <?php endif; ?>

                            </div>

                            <?php if ($procedures_unit1['vasc_access_per'] === 1): ?>
                                <label> Acesso vascular periférico</label>
                                <div id="vasc_access_per_c" style="margin-left: 15px ;">
                                    <h6><label class="control-label">Tipo</label></h6>
                                    <?= lang($procedures_unit1['vasc_access_per_disptype']) ?>

                                    <h6><label class="control-label">Região</label></h6>
                                    <?= lang($procedures_unit1['vasc_access_per_reg']) ?>
                                </div>
                            <?php endif; ?>

                            <?php if ($procedures_unit1['volemic_infus'] === 1): ?>
                                <label> Infusão volêmica</label>
                                <div id="volemic_infus_c" style="margin-left: 15px;">
                                    <?= lang($procedures_unit1['volemic_infus_type']) ?>
                                    <h6><label class="control-label">Quantidade (ml)</label></h6>
                                    <?= $procedures_unit1['volemic_infus_qt']; ?>
                                </div>
                            <?php endif; ?>

                            <?php if ($procedures_unit1['ressus_cardio'] === 1): ?>
                                <label> Ressuscitação cardiopulmonar</label>

                                <div id="cardiopulm-ressuc" style="margin-left: 15px ;">
                                    <h6><label class="control-label">Número de paradas cardíacas</label></h6>
                                    <?= $procedures_unit1['ressus_cardio_v']; ?>

                                    <h5><label class="control-label"><?= lang('duration') ?></label></h5>
                                    <?= $procedures_unit1['ressus_cardio_d']; ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <?php if ($attendance_type === 1): ?>
                        <div class="row-fluid procedure_pre_f">
                            <div class="col-sm-1 pull-left">
                                <a href="javascript:void(0);" onclick="jQuery('.procedure_pre_f #dcontent').toggle();" class="btn btn-default">D</a>
                            </div>

                            <div id="dcontent" class="col-sm-11" >
                                </br>
                                <?php if ($procedures_unit1['board_imo'] === 1): ?>
                                    <label> Imobilização em prancha longa</label>
                                <?php endif; ?>
                                <?php if ($procedures_unit1['memb_imob'] === 1): ?>
                                    <label> Imobilização dos membros</label>
                                <?php endif; ?>

                                <div id="mem_imob_c" style="margin-left: 15px ;">
                                    <?php if ($procedures_unit1['memb_imob_sd'] === 1): ?>
                                        <label> Membro superior direito</label>
                                    <?php endif; ?>
                                    <?php if ($procedures_unit1['memb_imob_se'] === 1): ?>
                                        <label> Membro superior esquerdo</label>
                                    <?php endif; ?>
                                    <?php if ($procedures_unit1['memb_imob_id'] === 1): ?>
                                        <label> Membro inferior direito</label>
                                    <?php endif; ?>
                                    <?php if ($procedures_unit1['memb_imob_ie'] === 1): ?>
                                        <label> Membro inferior esquerdo</label>
                                    <?php endif; ?>
                                    <?php if ($procedures_unit1['memb_imob_pelv'] === 1): ?>
                                        <label> Pelve</label>
                                    <?php endif; ?>

                                </div>
                            </div>
                        </div>
                    <?php endif; ?>

                    </br>
                    <h4>Procedimentos do atendimento pré hospitalar móvel</h4>
                    <div class="row-fluid procedure_pre_m">
                        <div class="col-sm-1 pull-left">
                            <a href="javascript:void(0);" onclick="jQuery('.procedure_pre_m #acontent').toggle();" class="btn btn-default">A</a>
                        </div>
                        <div id="acontent" class="col-sm-11" >
                            </br>
                            <?php if ($procedures_scene['oxign_dispo'] === 1): ?>
                                <label>
                                    Dispositivo de oxigênio
                                </label>
                                <div id="oxign_dispo_c" style="margin-left: 15px ;"> 
                                    <h6><label class="control-label">Tipo de dispositivo</label></h6>
                                    <?= lang($procedures_scene['o2_disp_type']) ?>
                                </div>
                            <?php endif; ?>

                            <?php if ($procedures_scene['breaf_vias'] === 1): ?>
                                <label>Via aérea</label>
                            <?php endif; ?>

                            <div id="aer_via_c" style="margin-left: 15px "> 

                                <?php if ($procedures_scene['maintenance_tec'] === 1): ?>
                                    <label>Técnicas de manutenção</label>
                                <?php endif; ?>


                                <div id="manut_tec_c" style="margin-left: 40px;">
                                    <?php if ($procedures_scene['mt_oro'] === 1): ?>
                                        <label>Orofaríngea</label>
                                    <?php endif; ?>
                                    <?php if ($procedures_scene['mt_naso'] === 1): ?>
                                        <label>Nasofaríngea</label>
                                    <?php endif; ?>
                                    <?php if ($procedures_scene['mt_lar_mask'] === 1): ?>
                                        <label>Máscara laríngea</label>
                                    <?php endif; ?>
                                    <?php if ($procedures_scene['mt_comb'] === 1): ?>
                                        <label>Combitube</label>
                                    <?php endif; ?>

                                </div>

                                <?php if ($procedures_scene['brief_def_via'] === 1): ?>
                                    <label>Uso de Via aérea definitiva</label>
                                <?php endif; ?>


                                <div id="ae_via_def_c" style="margin-left: 40px;">
                                    <?php if ($procedures_scene['bf_fst_int_seq'] === 1): ?>
                                        <label>Sequência rápida de intubação</label>
                                    <?php endif; ?>
                                    <?php if ($procedures_scene['bf_oro'] === 1): ?>
                                        <label>Orotraqueal</label>
                                    <?php endif; ?>
                                    <?php if ($procedures_scene['bf_naso'] === 1): ?>
                                        <label>Nasotraqueal</label>
                                    <?php endif; ?>
                                    <?php if ($procedures_scene['bf_cir'] === 1): ?>
                                        <label>Cirúrgico</label>
                                    <?php endif; ?>

                                    <div id="defin-aer-vie-cirurg" style="margin-left: 40px;">
                                        <?php if ($procedures_scene['bf_cir_cripun'] === 1): ?>
                                            <label>Cricotireoidostomia por punção</label>
                                        <?php endif; ?>
                                        <?php if ($procedures_scene['bf_cir_cricir'] === 1): ?>
                                            <label>Cricotireoidostomia cirúrgica</label>
                                        <?php endif; ?>
                                        <?php if ($procedures_scene['bf_cir_critraq'] === 1): ?>
                                            <label>Traqueostomia</label>
                                        <?php endif; ?>
                                    </div>
                                </div>

                                <?php if ($procedures_scene['cervical_col'] === 1): ?>
                                    <label>Colar cervical</label>
                                <?php endif; ?>

                            </div>
                        </div>
                    </div>

                    <div class="row-fluid procedure_pre_m">
                        <div class="col-sm-1 pull-left">
                            <a href="javascript:void(0);" onclick="jQuery('.procedure_pre_m #bcontent').toggle();" class="btn btn-default">B</a>
                        </div>

                        <div id="bcontent" class="col-sm-11" >
                            </br>
                            <?php if ($procedures_scene['mec_vent'] === 1): ?>
                                <label>Ventilação</label>
                                <div id="mec_vent_c" style="margin-left: 15px"> 
                                    <h6><label class="control-label">Tipo de ventilação</label></h6>
                                    <?= lang($procedures_scene['mec_vent_type']) ?>
                                </div>
                            <?php endif; ?>

                            <?php if ($procedures_scene['pun_pleu'] === 1): ?>
                                <label>Punção pleural</label>
                            <?php endif; ?>

                            <div id="pler-punc" style="margin-left: 15px ;">
                                <?php if ($procedures_scene['pun_pleu_l'] === 1): ?>
                                    <label>Esquerda</label>
                                <?php endif; ?>
                                <?php if ($procedures_scene['pun_pleu_r'] === 1): ?>
                                    <label>Direita</label>
                                <?php endif; ?>

                            </div>

                            <?php if ($procedures_scene['pleu_dren'] === 1): ?>
                                <label>Uso de dreno pleural</label>
                            <?php endif; ?>

                            <div id="pler-dren" style="margin-left: 15px ;">
                                <?php if ($procedures_scene['pleu_dren_l'] === 1): ?>
                                    <label>Esquerda</label>
                                <?php endif; ?>

                                <?php if ($procedures_scene['pleu_dren_l'] === 1): ?>
                                    <div id="pler-dren-left" > 
                                        <h6><label class="control-label">Quantidade (ml)</label></h6>
                                        <?= $procedures_scene['pleu_dren_lv']; ?>
                                    </div>
                                <?php endif; ?>
                                <?php if ($procedures_scene['pleu_dren_r'] === 1): ?>
                                    <label>Direita</label>
                                <?php endif; ?>

                                <?php if ($procedures_scene['pleu_dren_r'] === 1): ?>
                                    <div id="pler-dren-rig" > 
                                        <h6><label class="control-label">Quantidade (ml)</label></h6>
                                        <?= $procedures_scene['pleu_dren_rv']; ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>

                    <div class="row-fluid procedure_pre_m">
                        <div class="col-sm-1 pull-left">
                            <a href="javascript:void(0);" onclick="jQuery('.procedure_pre_m #ccontent').toggle();" class="btn btn-default">C</a>
                        </div>

                        <div id="ccontent" class="col-sm-11" >
                            </br>
                            <?php if ($procedures_scene['extern_hem'] === 1): ?>
                                <label>Presença de hemorragia externa</label>
                            <?php endif; ?>

                            <div id="extern_hem_c" style="margin-left: 15px ;">
                                <?php if ($procedures_scene['extern_hem_head_face'] === 1): ?>
                                    <label> Cabeça e face</label>
                                <?php endif; ?>
                                <?php if ($procedures_scene['extern_hem_neck'] === 1): ?>
                                    <label> Pescoço</label>
                                <?php endif; ?>
                                <?php if ($procedures_scene['extern_hem_chest'] === 1): ?>
                                    <label> Tórax</label>
                                <?php endif; ?>
                                <?php if ($procedures_scene['extern_hem_ab'] === 1): ?>
                                    <label> Abdome</label>
                                <?php endif; ?>
                                <?php if ($procedures_scene['extern_hem_pelvper'] === 1): ?>
                                    <label> Pelve/Períneo</label>
                                <?php endif; ?>
                                <?php if ($procedures_scene['extern_hem_mi'] === 1): ?>
                                    <label> Membros inferiores</label>
                                <?php endif; ?>
                                <?php if ($procedures_scene['extern_hem_ms'] === 1): ?>
                                    <label> Membros superiores</label>
                                <?php endif; ?>

                            </div>

                            <?php if ($procedures_scene['vasc_access_per'] === 1): ?>
                                <label> Acesso vascular periférico</label>
                                <div id="vasc_access_per_c" style="margin-left: 15px ;">
                                    <h6><label class="control-label">Tipo</label></h6>
                                    <?= lang($procedures_scene['vasc_access_per_disptype']) ?>

                                    <h6><label class="control-label">Região</label></h6>
                                    <?= lang($procedures_scene['vasc_access_per_reg']) ?>
                                </div>
                            <?php endif; ?>

                            <?php if ($procedures_scene['volemic_infus'] === 1): ?>
                                <label> Infusão volêmica</label>
                                <div id="volemic_infus_c" style="margin-left: 15px;">
                                    <?= lang($procedures_scene['volemic_infus_type']) ?>
                                    <h6><label class="control-label">Quantidade (ml)</label></h6>
                                    <?= $procedures_scene['volemic_infus_qt']; ?>
                                </div>
                            <?php endif; ?>

                            <?php if ($procedures_scene['ressus_cardio'] === 1): ?>
                                <label> Ressuscitação cardiopulmonar</label>

                                <div id="cardiopulm-ressuc" style="margin-left: 15px ;">
                                    <h6><label class="control-label">Número de paradas cardíacas</label></h6>
                                    <?= $procedures_scene['ressus_cardio_v']; ?>

                                    <h5><label class="control-label"><?= lang('duration') ?></label></h5>
                                    <?= $procedures_scene['ressus_cardio_d']; ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <?php if ($attendance_type === 1): ?>
                        <div class="row-fluid procedure_pre_m">
                            <div class="col-sm-1 pull-left">
                                <a href="javascript:void(0);" onclick="jQuery('.procedure_pre_m #dcontent').toggle();" class="btn btn-default">D</a>
                            </div>

                            <div id="dcontent" class="col-sm-11" >
                                </br>
                                <?php if ($procedures_scene['board_imo'] === 1): ?>
                                    <label> Imobilização em prancha longa</label>
                                <?php endif; ?>
                                <?php if ($procedures_scene['memb_imob'] === 1): ?>
                                    <label> Imobilização dos membros</label>
                                <?php endif; ?>

                                <div id="mem_imob_c" style="margin-left: 15px ;">
                                    <?php if ($procedures_scene['memb_imob_sd'] === 1): ?>
                                        <label> Membro superior direito</label>
                                    <?php endif; ?>
                                    <?php if ($procedures_scene['memb_imob_se'] === 1): ?>
                                        <label> Membro superior esquerdo</label>
                                    <?php endif; ?>
                                    <?php if ($procedures_scene['memb_imob_id'] === 1): ?>
                                        <label> Membro inferior direito</label>
                                    <?php endif; ?>
                                    <?php if ($procedures_scene['memb_imob_ie'] === 1): ?>
                                        <label> Membro inferior esquerdo</label>
                                    <?php endif; ?>
                                    <?php if ($procedures_scene['memb_imob_pelv'] === 1): ?>
                                        <label> Pelve</label>
                                    <?php endif; ?>

                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <a class="btn btn-success save_button" href="javascript:void(0)" onclick="save_primary_evaluation()"> Salvar</a>
    <script>
        jQuery('input.show-div').on('change', function () {
            var source = $(this);
            var target = $('#' + source.attr('data-target'));
            if ($('input[data-target=' + source.attr('data-target') + ']:checked').length)
                target.show();
            else
                target.hide();
        });

        jQuery('#mec_vent_type').change(function () {
            if (jQuery('#mec_vent_type').val() === 'mecanic') {
                jQuery('#mecanic_vent_c').css('display', 'block');
            } else {
                jQuery('#mecanic_vent_c').css('display', 'none');
            }
        })

        jQuery('#mecanic_vent_type').change(function () {
            if (jQuery('#mecanic_vent_type').val() === 'invasiv') {
                jQuery('#mecanic_vent_c_inv').css('display', 'block');
                jQuery('#mecanic_vent_c_n_inv').css('display', 'none');
            } else if (jQuery('#mecanic_vent_type').val() === 'n_invasiv') {
                jQuery('#mecanic_vent_c_inv').css('display', 'none');
                jQuery('#mecanic_vent_c_n_inv').css('display', 'block');
            } else {
                jQuery('#mecanic_vent_c_inv').css('display', 'none');
                jQuery('#mecanic_vent_c_n_inv').css('display', 'none');
            }
        })

        jQuery('#glasgow_visual_response').change(function () {
            var a = parseInt(jQuery('#glasgow_visual_response').val());
            var b = parseInt(jQuery('#glasgow_verbal_response').val());
            var c = parseInt(jQuery('#glasgow_motor_response').val());
            jQuery('#glasgow_score').val(a + b + c);
        });

        jQuery('#glasgow_verbal_response').change(function () {
            var a = parseInt(jQuery('#glasgow_visual_response').val());
            var b = parseInt(jQuery('#glasgow_verbal_response').val());
            var c = parseInt(jQuery('#glasgow_motor_response').val());
            jQuery('#glasgow_score').val(a + b + c);
        });

        jQuery('#glasgow_motor_response').change(function () {
            var a = parseInt(jQuery('#glasgow_visual_response').val());
            var b = parseInt(jQuery('#glasgow_verbal_response').val());
            var c = parseInt(jQuery('#glasgow_motor_response').val());
            jQuery('#glasgow_score').val(a + b + c);
        });

        function save_primary_evaluation() {

            jQuery.ajax({
                url: jQuery("body").data("baseurl") + "attendance_hospital/save_primary_evaluation",
                type: "post",
                dataType: 'json',
                data: {
                    a_pervious: jQuery('#a_pervious').is(':checked'),
                    a_maintain_techniques: jQuery('#a_maintain_techniques').is(':checked'),
                    a_definitive_airway: jQuery('#a_definitive_airway').is(':checked'),
                    a_hoarseness_emphysema: jQuery('#a_hoarseness_emphysema').is(':checked'),
                    a_neck_brace: jQuery('#a_neck_brace').is(':checked'),
                    b_crackle: jQuery('#b_crackle').val(),
                    b_hypertympanism: jQuery('#b_hypertympanism').val(),
                    b_underlying: jQuery('#b_underlying').val(),
                    b_estetores: jQuery('#b_estetores').val(),
                    b_sibilos: jQuery('#b_sibilos').val(),
                    b_roncos: jQuery('#b_roncos').val(),
                    b_pain: jQuery('#b_pain').val(),
                    b_trachea: jQuery('#b_trachea').val(),
                    b_vesicular_left: jQuery('#b_vesicular_left').val(),
                    b_vesicular_right: jQuery('#b_vesicular_right').val(),
                    b_jugular_venous_distension: jQuery('#b_jugular_venous_distension').is(':checked'),
                    pa: jQuery('#pa').val(),
                    fc: jQuery('#fc').val(),
                    fr: jQuery('#fr').val(),
                    sato2: jQuery('#sato2').val(),
                    axillary_temperature: jQuery('#axillary_temperature').val(),
                    esophageal_temperature: jQuery('#esophageal_temperature').val(),
                    c_skin: jQuery('#c_skin').val(),
                    c_capillary_refill: jQuery('#c_capillary_refill').val(),
                    c_characteristics_radial_pulse: jQuery('#c_characteristics_radial_pulse').val(),
                    c_bleeding_skull: jQuery('#c_bleeding_skull').is(':checked'),
                    c_bleeding_cervical: jQuery('#c_bleeding_cervical').is(':checked'),
                    c_bleeding_chest: jQuery('#c_bleeding_chest').is(':checked'),
                    c_bleeding_pelvis: jQuery('#c_bleeding_pelvis').is(':checked'),
                    c_bleeding_upper_limbs: jQuery('#c_bleeding_upper_limbs').is(':checked'),
                    c_bleeding_lower_limbs: jQuery('#c_bleeding_lower_limbs').is(':checked'),
                    d_sedated: jQuery('#d_sedated').is(':checked'),
                    d_drugs: jQuery('#d_drugs').is(':checked'),
                    d_cns_depressants: jQuery('#d_cns_depressants').is(':checked'),
                    d_alcohol: jQuery('#d_alcohol').is(':checked'),
                    d_intubated: jQuery('#d_intubated').is(':checked'),
                    pupillary_reflex_left: jQuery('#pupillary_reflex_left').val(),
                    pupillary_reflex_right: jQuery('#pupillary_reflex_right').val(),
                    left_anisocoria: jQuery('#left_anisocoria').val(),
                    right_anisocoria: jQuery('#right_anisocoria').val(),
                    d_ramsay: jQuery('#d_ramsay').val(),
                    e_skull: jQuery('#e_skull').is(':checked'),
                    e_face: jQuery('#e_face').is(':checked'),
                    e_cervical: jQuery('#e_cervical').is(':checked'),
                    e_thorax: jQuery('#e_thorax').is(':checked'),
                    e_abdomen: jQuery('#e_abdomen').is(':checked'),
                    e_pelvis: jQuery('#e_pelvis').is(':checked'),
                    e_upper_limbs: jQuery('#e_upper_limbs').is(':checked'),
                    e_lower_limbs: jQuery('#e_lower_limbs').is(':checked'),
                    glasgow_visual_response: jQuery('#glasgow_visual_response').val(),
                    glasgow_verbal_response: jQuery('#glasgow_verbal_response').val(),
                    glasgow_motor_response: jQuery('#glasgow_motor_response').val(),
                    glasgow_score: jQuery('#glasgow_score').val()
                },
                success: function (response) {
                    if (response.status === 'NOK') {
                        var notice = new PNotify({
                            title: 'Erro',
                            text: 'Tente novamente mais tarde',
                            type: 'error',
                            addclass: 'click-2-close',
                            hide: false,
                            buttons: {
                                closer: false,
                                sticker: false
                            }
                        });
                        notice.get().click(function () {
                            notice.remove();
                        });
                    } else {
                        jQuery.ajax({
                            url: jQuery("body").data("baseurl") + "attendance_hospital/save_trauma_realized_procedures",
                            type: "post",
                            dataType: 'json',
                            data: {
                                o2_disp_type: jQuery('#oxdisptype').val(),
                                breaf_vias: jQuery('#breaf_vias').prop('checked'),
                                maintenance_tec: jQuery('#maintenance_tec').prop('checked'),
                                brief_def_via: jQuery('#brief_def_via').prop('checked'),
                                cervical_col: jQuery('#cervical_col').prop('checked'),
                                mt_oro: jQuery('#mt_oro').prop('checked'),
                                mt_naso: jQuery('#mt_naso').prop('checked'),
                                mt_lar_mask: jQuery('#mt_lar_mask').prop('checked'),
                                mt_comb: jQuery('#mt_comb').prop('checked'),
                                bf_fst_int_seq: jQuery('#bf_fst_int_seq').prop('checked'),
                                bf_oro: jQuery('#bf_oro').prop('checked'),
                                bf_naso: jQuery('#bf_naso').prop('checked'),
                                bf_cir: jQuery('#bf_cir').prop('checked'),
                                bf_cir_cripun: jQuery('#bf_cir_cripun').prop('checked'),
                                bf_cir_cricir: jQuery('#bf_cir_cricir').prop('checked'),
                                bf_cir_critraq: jQuery('#bf_cir_critraq').prop('checked'),
                                mec_vent: jQuery('#mec_vent').prop('checked'),
                                mec_vent_type: jQuery('#mec_vent_type').val(),
                                mecanic_vent_type: jQuery('#mecanic_vent_type').val(),
                                inv_mec_vent_mode: jQuery('#inv_mec_vent_mode').val(),
                                inv_mec_vent_volume: jQuery('#inv_mec_vent_volume').val(),
                                inv_mec_vent_fio2: jQuery('#inv_mec_vent_fio2').val(),
                                inv_mec_vent_peep: jQuery('#inv_mec_vent_peep').val(),
                                inv_mec_vent_freq_resp: jQuery('#inv_mec_vent_freq_resp').val(),
                                inv_mec_vent_press_lim: jQuery('#inv_mec_vent_press_lim').val(),
                                n_inv_mec_vent_ipap: jQuery('#n_inv_mec_vent_ipap').val(),
                                n_inv_mec_vent_epap: jQuery('#n_inv_mec_vent_epap').val(),
                                n_inv_mec_vent_fio2: jQuery('#n_inv_mec_vent_fio2').val(),
                                n_inv_mec_vent_freq_resp: jQuery('#n_inv_mec_vent_freq_resp').val(),
                                pun_pleu: jQuery('#pun_pleu').prop('checked'),
                                pun_pleu_r: jQuery('#pun_pleu_r').prop('checked'),
                                pun_pleu_l: jQuery('#pun_pleu_l').prop('checked'),
                                pleu_dren: jQuery('#pleu_dren').prop('checked'),
                                pleu_dren_l: jQuery('#pleu_dren_l').prop('checked'),
                                pleu_dren_r: jQuery('#pleu_dren_r').prop('checked'),
                                pleu_dren_rv: jQuery('#pleu_dren_rv').val(),
                                pleu_dren_lv: jQuery('#pleu_dren_lv').val(),
                                extern_hem: jQuery('#extern_hem').prop('checked'),
                                extern_hem_head_face: jQuery('#extern_hem_head_face').prop('checked'),
                                extern_hem_neck: jQuery('#extern_hem_neck').prop('checked'),
                                extern_hem_chest: jQuery('#extern_hem_chest').prop('checked'),
                                extern_hem_ab: jQuery('#extern_hem_ab').prop('checked'),
                                extern_hem_pelvper: jQuery('#extern_hem_pelvper').prop('checked'),
                                extern_hem_mi: jQuery('#extern_hem_mi').prop('checked'),
                                extern_hem_ms: jQuery('#extern_hem_ms').prop('checked'),
                                vasc_access_per: jQuery('#vasc_access_per').prop('checked'),
                                vasc_access_per_disptype: jQuery('#vasc_access_per_disptype').val(),
                                vasc_access_per_reg: jQuery('#vasc_access_per_reg').val(),
                                volemic_infus: jQuery('#volemic_infus').prop('checked'),
                                ressus_cardio_v: jQuery('#ressus_cardio_v').val(),
                                ressus_cardio_d: jQuery('#ressus_cardio_d').val(),
                                board_imo: jQuery('#board_imo').prop('checked'),
                                memb_imob: jQuery('#memb_imob').prop('checked'),
                                memb_imob_sd: jQuery('#memb_imob_sd').prop('checked'),
                                memb_imob_se: jQuery('#memb_imob_se').prop('checked'),
                                memb_imob_id: jQuery('#memb_imob_id').prop('checked'),
                                memb_imob_ie: jQuery('#memb_imob_ie').prop('checked'),
                                memb_imob_pelv: jQuery('#memb_imob_pelv').prop('checked'),
                                context: 'hospital'
                            },
                            success: function (response) {
                                if (response.status === 'OK') {
                                    // recarrega avaliacao primaria
                                    jQuery('#hospital_attendance_steps').load(jQuery("body").data("baseurl") + "attendance_hospital/primary_evaluation/view");
                                } else {
                                    var notice = new PNotify({
                                        title: 'Erro',
                                        text: 'Tente novamente mais tarde',
                                        type: 'error',
                                        addclass: 'click-2-close',
                                        hide: false,
                                        buttons: {
                                            closer: false,
                                            sticker: false
                                        }
                                    });
                                    notice.get().click(function () {
                                        notice.remove();
                                    });
                                }
                            }
                        });
                    }
                }
            });
        }

        function togglePanels(content, options) {
            jQuery(content).toggle();
            jQuery(content + '_chev').toggle();
            jQuery(content + '_chevback').toggle();
            jQuery(options).toggle();
        }

    </script>