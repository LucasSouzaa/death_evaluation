<div class="row" style="padding-top: 20px;">
    <div class="col-sm-2" style=" padding-left: 0px">
        <div id="MainMenu">
            <div class="list-group panel">
                <a id="li_primary_evaluation" href="javascript:void(0);" class="list-group-item" onclick="load_hospital_attendance_steps('primary_evaluation')">Avaliação primária</a>
                <a id="li_secundary_evaluation" href="javascript:void(0);" class="list-group-item" onclick="load_hospital_attendance_steps('secundary_evaluation')">Avaliação secundária</a>
                <a id="li_biochemistry_exams" href="javascript:void(0);" class="list-group-item" onclick="load_hospital_attendance_steps('biochemistry_exams')">Exames bioquímicos</a>
                <a id="li_image_exams" href="javascript:void(0);" class="list-group-item" onclick="load_hospital_attendance_steps('image_exams')">Exames de imagem</a>
                <a id="li_diagnosys" href="javascript:void(0);" class="list-group-item" onclick="load_hospital_attendance_steps('diagnosys')">Diagnóstico</a>
                <a id="li_conduct" href="javascript:void(0);" class="list-group-item" onclick="load_hospital_attendance_steps('conduct')">Conduta</a>
            </div>
        </div>
    </div>
    <div class="col-sm-10">
        <div id="content" class="row-fluid">
            <?php if ($first_condition_empty): ?>
                <?php $this->load->view('attendance_hospital/attendance_situation'); ?>
            <?php else: ?>
                <div id="hospital_attendance_steps" style="margin-top:20px;"></div>
            <?php endif; ?>
        </div>
    </div>
</div>


<script>
    jQuery('li').removeClass('active');
    jQuery('#li_hospital').addClass('active');
    jQuery('#li_primary_evaluation').addClass('active');
    jQuery('#hospital_attendance_steps').load(jQuery("body").data("baseurl") + "attendance_hospital/primary_evaluation");

    function load_hospital_attendance_steps(type) {
        if (type === 'primary_evaluation') {
            jQuery('#hospital_attendance_steps').load(jQuery("body").data("baseurl") + "attendance_hospital/primary_evaluation");
            jQuery('li').removeClass('active');
            jQuery('a').removeClass('active');
            jQuery('#li_hospital').addClass('active');
            jQuery('#li_primary_evaluation').addClass('active');
        } else if (type === 'secundary_evaluation') {
            jQuery('#hospital_attendance_steps').load(jQuery("body").data("baseurl") + "attendance_hospital/secundary_evaluation");
            jQuery('li').removeClass('active');
            jQuery('a').removeClass('active');
            jQuery('#li_hospital').addClass('active');
            jQuery('#li_secundary_evaluation').addClass('active');
        } else if (type === 'biochemistry_exams') {
            jQuery('#hospital_attendance_steps').load(jQuery("body").data("baseurl") + "attendance_hospital/biochemistry_exams");
            jQuery('li').removeClass('active');
            jQuery('a').removeClass('active');
            jQuery('#li_hospital').addClass('active');
            jQuery('#li_biochemistry_exams').addClass('active');
        } else if (type === 'image_exams') {
            jQuery('#hospital_attendance_steps').load(jQuery("body").data("baseurl") + "attendance_hospital/image_exams");
            jQuery('li').removeClass('active');
            jQuery('a').removeClass('active');
            jQuery('#li_hospital').addClass('active');
            jQuery('#li_image_exams').addClass('active');
        } else if (type === 'diagnosys') {
            jQuery('#hospital_attendance_steps').load(jQuery("body").data("baseurl") + "attendance_hospital/diagnosys");
            jQuery('li').removeClass('active');
            jQuery('a').removeClass('active');
            jQuery('#li_hospital').addClass('active');
            jQuery('#li_diagnosys').addClass('active');
        } else if (type === 'conduct') {
            jQuery('#hospital_attendance_steps').load(jQuery("body").data("baseurl") + "attendance_hospital/conduct");
            jQuery('li').removeClass('active');
            jQuery('a').removeClass('active');
            jQuery('#li_hospital').addClass('active');
            jQuery('#li_conduct').addClass('active');
        }
    }
</script>