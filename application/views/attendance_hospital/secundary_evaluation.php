<script src="<?= $this->config->base_url(JSPATH . 'cid10t_neoplasias.js') ?>"></script>

<div class="pill-content">
    <div class="pill-pane active">
        <div class="container-fluid">
            <legend> 
                <a href="javascript:void(0)" onclick="togglePanels('#history', '#history_options');">Antecedentes</a>
                <div id="history_chev" class="pull-right">
                    <a href="javascript:void(0)" onclick="togglePanels('#history', '#history_options');"><i class="fa fa-plus-square-o"></i></a>
                </div>
                <div id="history_chevback" style="display:none;" class="pull-right">
                    <a href="javascript:void(0)" onclick="togglePanels('#history', '#history_options');"><i class="fa fa-minus-square-o"></i></a>
                </div>
            </legend>

            <div id="history" style="display:none;">
                <div class="well">
                    <div class="row-fluid">
                        <div class="col-sm-7">
                            <h5><label class="control-label">Neoplasias</label></h5>

                            <div id="external_couses_diagnosis">
                                <div class="well">
                                    <div id="external_couses_cid_diagnosis"> 
                                        <b>Capítulo:</b><br>
                                        <span id="capec"><input type="hidden" value="V01-Y98" name="ec_chapter_cid_field">Causas externas de morbidade e de mortalidade</span><br> 
                                        <b>Grupo:</b><br>
                                        <span id="gpec"><select style="width:100%" id="grupo" name="ec_group_cid_field" class="form-control" onchange="popcatec('V01-Y98', this.value, 'catec')"><option value="V01-V09">Pedestre traumatizado em um acidente de transporte</option><option value="V10-V19">Ciclista traumatizado em um acidente de transporte</option><option value="V20-V29">Motociclista traumatizado em um acidente de transporte</option><option value="V30-V39">Ocupante de triciclo motorizado traumatizado em um acidente de transporte</option><option value="V40-V49">Ocupante de um automóvel traumatizado em um acidente de transporte</option><option value="V50-V59">Ocupante de uma caminhonete traumatizado em um acidente de transporte</option><option value="V60-V69">Ocupante de um veículo de transporte pesado traumatizado em um acidente de transporte</option><option value="V70-V79">Ocupante de um ônibus traumatizado em um acidente de transporte</option><option value="V80-V89">Outros acidentes de transporte terrestre</option><option value="V90-V94">Acidentes de transporte por água</option><option value="V95-V97">Acidentes de transporte aéreo e espacial</option><option value="V98-V99">Outros acidentes de transporte e os não especificados</option><option value="W00-W19">Quedas</option><option value="W20-W49">Exposição a forças mecânicas inanimadas</option><option value="W50-W64">Exposição a forças mecânicas animadas</option><option value="W65-W74">Afogamento e submersão acidentais</option><option value="W75-W84">Outros riscos acidentais à respiração</option><option value="W85-W99">Exposição à corrente elétrica, à radiação e às temperaturas e pressões extremas do ambiente</option><option value="X00-X09">Exposição à fumaça, ao fogo e às chamas</option><option value="X10-X19">Contato com uma fonte de calor ou com substâncias quentes</option><option value="X20-X29">Contato com animais e plantas venenosos</option><option value="X30-X39">Exposição às forças da natureza</option><option value="X40-X49">Envenenamento [intoxicação] acidental por e exposição à substâncias nocivas</option><option value="X50-X57">Excesso de esforços, viagens e privações</option><option value="X58-X59">Exposição acidental a outros fatores e aos não especificados</option><option value="X60-X84">Lesões autoprovocadas intencionalmente</option><option value="X85-Y09">Agressões</option><option value="Y10-Y34">Eventos (fatos) cuja intenção é indeterminada</option><option value="Y35-Y36">Intervenções legais e operações de guerra</option><option value="Y40-Y59">Efeitos adversos de drogas, medicamentos e substâncias biológicas usadas com finalidade terapêutica</option><option value="Y60-Y69">Acidentes ocorridos em pacientes durante a prestação de cuidados médicos e cirúrgicos</option><option value="Y70-Y82">Incidentes adversos durante atos diagnósticos ou terapêuticos associados ao uso de dispositivos (aparelhos) médicos</option><option value="Y83-Y84">Reação anormal em paciente ou complicação tardia causadas por procedimentos cirúrgicos e outros procedimentos médicos sem menção de acidente ao tempo do procedimento</option><option value="Y85-Y89">Seqüelas de causas externas de morbidade e de mortalidade</option><option value="Y90-Y98">Fatores suplementares relacionados com as causas de morbidade e de mortalidade classificados em outra parte</option></select></span><br>
                                        <b>Categoria:</b><br>
                                        <span id="catec"></span></br>
                                        <b>Subcategoria:</b><br>
                                        <span id="scatec"></span></br>
                                        <a onclick="add_history_item_cid('neoplasms');" class="btn btn-lg btn-primary btn-block"><i class="fa fa-plus"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-5" style=" margin-top: 40px">
                            <div id="history_neoplasms_list">
                                <?php if (isset($neoplasms)): ?>
                                    <table class="table table-striped table-bordered table-condensed table-hover">
                                        <thead>
                                            <tr>
                                                <th>Neoplasias</th>
                                                <th> Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($neoplasms as $hn): ?>
                                                <tr>
                                                    <th><?= $hn['name'] ?></th>
                                                    <th><a href="javascript:;" onclick="delete_history_item('neoplasms', <?= $hn['id'] ?>)" class="on-default remove-row" style=" margin-right: 5px; margin-left: 5px;"><i class="fa fa-trash-o"></i></a></th>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                    <?php
                                else:
                                    echo 'Lista vazia';
                                endif;
                                ?>
                            </div>
                        </div>
                    </div>

                    <div class="row-fluid">
                        <div class="col-sm-7">
                            <h5><label class="control-label">Alergias</label></h5>
                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="history_allergies_c">
                                <?php foreach ($cfg_allergies as $hac): ?>
                                    <option value="<?= $hac['id'] ?>" selected><?= $hac['name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                            </br>
                            <a onclick="add_history_item('allergies');" class="btn btn-lg btn-primary btn-block"><i class="fa fa-plus"></i></a>
                        </div>
                        <div class="col-sm-5" style=" margin-top: 40px">
                            <div id="history_allergies_list">
                                <?php if (isset($allergies)): ?>
                                    <table class="table table-striped table-bordered table-condensed table-hover">
                                        <thead>
                                            <tr>
                                                <th>Alergias</th>
                                                <th> Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($allergies as $ha): ?>
                                                <tr>
                                                    <th><?= $ha['name'] ?></th>
                                                    <th><a href="javascript:;" onclick="delete_history_item('allergies', <?= $ha['id'] ?>)" class="on-default remove-row" style=" margin-right: 5px; margin-left: 5px;"><i class="fa fa-trash-o"></i></a></th>
                                                </tr>
                                            <?php endforeach;
                                            ?>
                                        </tbody>
                                    </table>
                                    <?php
                                else:
                                    echo 'Lista vazia';
                                endif;
                                ?>
                            </div>
                        </div>
                    </div>

                    <div class="row-fluid">
                        <div class="col-sm-7">
                            <h5><label class="control-label">Medicamentos</label></h5>
                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="history_medicines_c">
                                <?php foreach ($cfg_medicines as $hmc): ?>
                                    <option value="<?= $hmc['id'] ?>" selected><?= $hmc['name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                            </br>
                            <a onclick="add_history_item('medicines');" class="btn btn-lg btn-primary btn-block"><i class="fa fa-plus"></i></a>
                        </div>
                        <div class="col-sm-5" style=" margin-top: 40px">
                            <div id="history_medicines_list">
                                <?php if (isset($medicines)): ?>
                                    <table class="table table-striped table-bordered table-condensed table-hover">
                                        <thead>
                                            <tr>
                                                <th><?= lang('drug_name') ?></th>
                                                <th> Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($medicines as $hm): ?>
                                                <tr>
                                                    <th><?= $hm['name'] ?></th>
                                                    <th><a href="javascript:;" onclick="delete_history_item('medicines', <?= $hm['id'] ?>)" class="on-default remove-row" style=" margin-right: 5px; margin-left: 5px;"><i class="fa fa-trash-o"></i></a></th>
                                                </tr>
                                            <?php endforeach;
                                            ?>
                                        </tbody>
                                    </table>
                                    <?php
                                else:
                                    echo 'Lista vazia';
                                endif;
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="row-fluid" style="margin-top:15px;">
                        <div class="col-sm-7">
                            <h5><label class="control-label">Cirurgias</label></h5>
                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="history_surgeries_c">
                                <?php foreach ($cfg_surgeries as $hcc): ?>
                                    <option value="<?= $hcc['id'] ?>" selected><?= $hcc['name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                            </br>
                            <a onclick="add_history_item('surgeries');" class="btn btn-lg btn-primary btn-block"><i class="fa fa-plus"></i></a>
                        </div>
                        <div class="col-sm-5" style=" margin-top: 40px">
                            <div id="history_surgeries_list">
                                <?php if (isset($surgeries)): ?>
                                    <table class="table table-striped table-bordered table-condensed table-hover">
                                        <thead>
                                            <tr>
                                                <th>Cirurgias</th>
                                                <th> Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($surgeries as $hc): ?>
                                                <tr>
                                                    <th><?= $hc['name'] ?></th>
                                                    <th><a href="javascript:;" onclick="delete_history_item('surgeries', <?= $hc['id'] ?>)" class="on-default remove-row" style=" margin-right: 5px; margin-left: 5px;"><i class="fa fa-trash-o"></i></a></th>
                                                </tr>
                                            <?php endforeach;
                                            ?>
                                        </tbody>
                                    </table>
                                    <?php
                                else:
                                    echo 'Lista vazia';
                                endif;
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="row-fluid" style="margin-top:15px; margin-left:15px;">
                        <div class="checkbox">
                            <div class="col-md-3">
                                <label>
                                    <input type="checkbox" id="hiv"  <?php
                                    if ($history_complements['hiv']): echo 'checked';
                                    endif;
                                    ?>> HIV+
                                </label>
                            </div>
                            <div class="col-md-3">
                                <label>
                                    <input type="checkbox" id="hep"  <?php
                                    if ($history_complements['hep']): echo 'checked';
                                    endif;
                                    ?>> Hepatite
                                </label>
                            </div>
                            <div class="col-md-3">
                                <label>
                                    <input type="checkbox" id="has"  <?php
                                    if ($history_complements['has']): echo 'checked';
                                    endif;
                                    ?>> HAS
                                </label>
                            </div>
                            <div class="col-md-3">
                                <label>
                                    <input type="checkbox" id="diab"  <?php
                                    if ($history_complements['diab']): echo 'checked';
                                    endif;
                                    ?>> Diabetes
                                </label>
                            </div>
                            <div class="col-md-3">
                                <label>
                                    <input type="checkbox" id="dpoc"  <?php
                                    if ($history_complements['dpoc']): echo 'checked';
                                    endif;
                                    ?>> DPOC
                                </label>
                            </div>
                            <div class="col-md-3">
                                <label>
                                    <input type="checkbox" id="avc"  <?php
                                    if ($history_complements['avc']): echo 'checked';
                                    endif;
                                    ?>> AVC
                                </label>
                            </div>
                            <div class="col-md-3">
                                <label>
                                    <input type="checkbox" id="sci"  <?php
                                    if ($history_complements['sci']): echo 'checked';
                                    endif;
                                    ?>> Síndrome coronariana isquêmica
                                </label>
                            </div>
                            <div class="col-md-3">
                                <label>
                                    <input type="checkbox" id="psic_transt"  <?php
                                    if ($history_complements['psic_transt']): echo 'checked';
                                    endif;
                                    ?>> Transtorno psiquiátrico
                                </label>
                            </div>
                            <div class="col-md-3">
                                <label>
                                    <input type="checkbox" id="elit" <?php
                                    if ($history_complements['elit']): echo 'checked';
                                    endif;
                                    ?> > Elitismo
                                </label>
                            </div>
                            <div class="col-md-3">
                                <label>
                                    <input type="checkbox" id="udi" <?php
                                    if ($history_complements['udi']): echo 'checked';
                                    endif;
                                    ?>> Uso de drogas ilícitas
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row-fluid" style="margin-top:15px; margin-left:15px;">
                        <div class="col-md-12">
                            <label>Outras condições</label>
                        </div>
                        <div class="checkbox">
                            <div class="col-md-3">
                                <label>
                                    <input type="checkbox" id="ant_gestation" <?php
                                    if ($history_complements['ant_gestation']) : echo 'checked';
                                    endif;
                                    ?>> Gestação
                                </label>
                            </div>
                            <div class="col-md-3">
                                <label>
                                    <input type="checkbox" id="ant_tetanus_vaccination" <?php
                                    if ($history_complements['ant_tetanus_vaccination']) : echo 'checked';
                                    endif;
                                    ?>> Vacinação Antitetânica
                                </label>
                            </div>
                            <div class="col-md-3">
                                <label>
                                    <input type="checkbox" id="ant_acute_intoxication" <?php
                                    if ($history_complements['ant_acute_intoxication']): echo 'checked';
                                    endif;
                                    ?>> Intoxicação aguda
                                </label>
                            </div>
                            <div class="col-md-3">
                                <label>
                                    <input type="checkbox" id="ant_fluid_intake" <?php
                                    if ($history_complements['ant_fluid_intake']): echo 'checked';
                                    endif;
                                    ?>> Ingestão de líquidos/alimentos
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row-fluid" style="margin-top:15px; margin-left:15px;">
                        <div class="col-md-12">
                            <label>Deficiências</label>
                        </div>
                        <div class="checkbox">

                            <div class="col-md-3">
                                <label>
                                    <input type="checkbox" id="physical_disability" <?php
                                    if ($history_complements['physical_disability']): echo 'checked';
                                    endif;
                                    ?>> Deficência física
                                </label>
                            </div>
                            <div class="col-md-3">
                                <label>
                                    <input type="checkbox" id="mental_disability" <?php
                                    if ($history_complements['mental_disability']): echo 'checked';
                                    endif;
                                    ?>> Deficência mental
                                </label>
                            </div>
                            <div class="col-md-3">
                                <label>
                                    <input type="checkbox" id="hearing_deficiency" <?php
                                    if ($history_complements['hearing_deficiency']): echo 'checked';
                                    endif;
                                    ?>> Deficência auditiva
                                </label>
                            </div>
                            <div class="col-md-3">
                                <label>
                                    <input type="checkbox" id="visual_impairment" <?php
                                    if ($history_complements['visual_impairment']): echo 'checked';
                                    endif;
                                    ?>> Deficência visual
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <legend>
                <a href="javascript:void(0)" onclick="togglePanels('#fisexam', '#fisexam_options');">Exame Físico </a>
                <div id="fisexam_chev" class="pull-right">
                    <a href="javascript:void(0)" onclick="togglePanels('#fisexam', '#fisexam_options');"><i class="fa fa-plus-square-o"></i></a>
                </div>
                <div id="fisexam_chevback" style="display:none;" class="pull-right">
                    <a href="javascript:void(0)" onclick="togglePanels('#fisexam', '#fisexam_options');"><i class="fa fa-minus-square-o"></i></a>
                </div>
            </legend>

            <div id="fisexam" style="display:none;">
                <div class="row-fluid">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" id="compfisevaluation"
                            <?php
                            if ($fisexam['compfisevaluation'] === 1): echo 'checked';
                            endif;
                            ?>>
                            Avaliação física completa sem nenhuma alteração (exceto avaliação neurológica e de queimaduras que precisam ser analisados)
                        </label>
                    </div>

                    <!-- CABEÇA -->
                    <div class="well" style="margin-left:25px;">
                        <h3 class="panel-title pull-left">
                            <a href="javascript:void(0)" onclick="togglePanels('#head_evaluation', '#head_evaluation_options');">Avaliação da cabeça</a>
                        </h3>
                        <div class="row-fluid">
                            <div id="head_evaluation_chev">
                                <a href="javascript:void(0)" onclick="togglePanels('#head_evaluation', '#head_evaluation_options');"><i class="fa fa-plus pull-right"></i></a>
                            </div>
                            <div id="head_evaluation_chevback" style="display:none;">
                                <a href="javascript:void(0)" onclick="togglePanels('#head_evaluation', '#head_evaluation_options');"><i class="fa fa-minus pull-right"></i></a>
                            </div>
                        </div>
                        <div id="head_evaluation" style="display:none;">
                            <hr>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" id="normal_head_evaluation"
                                    <?php
                                    if ($fisexam['normal_head_evaluation'] === 1): echo 'checked';
                                    endif;
                                    ?>>
                                    Avaliação da cabeça sem alterações (normal)
                                </label>
                            </div>
                            <div class="row-fluid">
                                <div class="col-sm-5 well">
                                    <h4 class="text-center"> Lado direito </h4>
                                    <div class="row-fluid">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" id="normal_head_evaluation_r"
                                                <?php
                                                if ($fisexam['normal_head_evaluation_r'] === 1): echo 'checked';
                                                endif;
                                                ?>>
                                                Avaliação da cabeça do lado direito sem alterações (normal)
                                            </label>
                                        </div>
                                    </div>
                                    <h4> Couro cabeludo </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="head_cba_r" name="head_cb_r" <?php
                                            if ($fisexam['head_cba_r_inj_nat'] && isset($fisexam['head_cba_r_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#head_cba_r_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="head_cbn_r" name="head_cb_r" <?php
                                            if ($fisexam['head_cba_r_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#head_cba_r_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="head_cba_r_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="head_cba_r_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="excoriation" <?php
                                                if ($fisexam['head_cba_r_inj_nat'] === 'excoriation'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Escoriação
                                                </option>
                                                <option value="superficial_laceration" <?php
                                                if ($fisexam['head_cba_r_inj_nat'] === 'superficial_laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração superficial
                                                </option>
                                                <option value="deep_laceration" <?php
                                                if ($fisexam['head_cba_r_inj_nat'] === 'deep_laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração profundo (>10cm)
                                                </option>
                                                <option value="av_small_tissue_loss" <?php
                                                if ($fisexam['head_cba_r_inj_nat'] === 'av_small_tissue_loss'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Avulsão com perda tecidual pequena (<100cm²)
                                                </option>
                                                <option value="av_big_tissue_loss" <?php
                                                if ($fisexam['head_cba_r_inj_nat'] === 'av_big_tissue_loss'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Avulsão com perda tecidual grande (>100cm²)
                                                </option>
                                                <option value="avul_comp" <?php
                                                if ($fisexam['head_cba_r_inj_nat'] === 'avul_comp'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Avulsão completa
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h4> Calota craniana </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="head_cca_r" name="head_cc_r" <?php
                                            if ($fisexam['head_cca_r_inj_nat'] && isset($fisexam['head_cca_r_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#head_cca_r_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="head_ccn_r" name="head_cc_r" <?php
                                            if ($fisexam['head_cca_r_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#head_cca_r_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="head_cca_r_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="head_cca_r_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="simple_fracture" <?php
                                                if ($fisexam['head_cca_r_inj_nat'] === 'simple_fracture'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Fratura simples
                                                </option>
                                                <option value="comminuted_fracture_without_sinking" <?php
                                                if ($fisexam['head_cca_r_inj_nat'] === 'comminuted_fracture_without_sinking'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Fratura cominutiva sem afundamento (<= 2cm)
                                                </option>
                                                <option value="comminuted_fracture_with_sinking" <?php
                                                if ($fisexam['head_cca_r_inj_nat'] === 'comminuted_fracture_with_sinking'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Fratura cominutiva com afundamento (> 2cm)
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h4> Base do cranio </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="head_bca_r" name="head_bc_r" <?php
                                            if ($fisexam['head_bca_r_inj_nat'] && isset($fisexam['head_bca_r_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#head_bca_r_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="head_bcn_r" name="head_bc_r" <?php
                                            if ($fisexam['head_bca_r_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#head_bca_r_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="head_bca_r_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="head_bca_r_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="fracture_without_cerebrospinal_fluid_leaks" <?php
                                                if ($fisexam['head_bca_r_inj_nat'] === 'fracture_without_cerebrospinal_fluid_leaks'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Fratura sem fístula liquórica
                                                </option>
                                                <option value="fracture_with_cerebrospinal_fluid_leaks" <?php
                                                if ($fisexam['head_bca_r_inj_nat'] === 'fracture_with_cerebrospinal_fluid_leaks'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Fratura com fístula liquórica
                                                </option>
                                                <option value="open_fracture_comminuted" <?php
                                                if ($fisexam['head_bca_r_inj_nat'] === 'open_fracture_comminuted'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Fratura exposta ou cominutiva
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-1"></div>

                                <div class="col-sm-5 well">
                                    <h4 class="text-center"> Lado esquerdo </h4>
                                    <div class="row-fluid">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" id="normal_head_evaluation_l"
                                                <?php
                                                if ($fisexam['normal_head_evaluation_l'] === 1): echo 'checked';
                                                endif;
                                                ?>>
                                                Avaliação da cabeça do lado esquerdo sem alterações (normal)
                                            </label>
                                        </div>
                                    </div>
                                    <h4> Couro cabeludo </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="head_cba_l" name="head_cb_l" <?php
                                            if ($fisexam['head_cba_l_inj_nat'] && isset($fisexam['head_cba_l_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#head_cba_l_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="head_cbn_l" name="head_cb_l" <?php
                                            if ($fisexam['head_cba_l_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#head_cba_l_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="head_cba_l_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="head_cba_l_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="excoriation" <?php
                                                if ($fisexam['head_cba_l_inj_nat'] === 'excoriation'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Escoriação
                                                </option>
                                                <option value="superficial_laceration" <?php
                                                if ($fisexam['head_cba_l_inj_nat'] === 'superficial_laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração superficial
                                                </option>
                                                <option value="deep_laceration" <?php
                                                if ($fisexam['head_cba_l_inj_nat'] === 'deep_laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração profundo (>10cm)
                                                </option>
                                                <option value="av_small_tissue_loss" <?php
                                                if ($fisexam['head_cba_l_inj_nat'] === 'av_small_tissue_loss'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Avulsão com perda tecidual pequena (<100cm²)
                                                </option>
                                                <option value="av_big_tissue_loss" <?php
                                                if ($fisexam['head_cba_l_inj_nat'] === 'av_big_tissue_loss'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Avulsão com perda tecidual grande (>100cm²)
                                                </option>
                                                <option value="avul_comp" <?php
                                                if ($fisexam['head_cba_l_inj_nat'] === 'avul_comp'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Avulsão completa
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h4> Calota craniana </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="head_cca_l" name="head_cc_l" <?php
                                            if ($fisexam['head_cca_l_inj_nat'] && isset($fisexam['head_cca_l_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#head_cca_l_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="head_ccn_l" name="head_cc_l" <?php
                                            if ($fisexam['head_cca_l_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#head_cca_l_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="head_cca_l_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="head_cca_l_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="simple_fracture" <?php
                                                if ($fisexam['head_cca_l_inj_nat'] === 'simple_fracture'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Fratura simples
                                                </option>
                                                <option value="comminuted_fracture_without_sinking" <?php
                                                if ($fisexam['head_cca_l_inj_nat'] === 'comminuted_fracture_without_sinking'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Fratura cominutiva sem afundamento (<= 2cm)
                                                </option>
                                                <option value="comminuted_fracture_with_sinking" <?php
                                                if ($fisexam['head_cca_l_inj_nat'] === 'comminuted_fracture_with_sinking'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Fratura cominutiva com afundamento (> 2cm)
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h4> Base do cranio </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="head_bca_l" name="head_bc_l" <?php
                                            if ($fisexam['head_bca_l_inj_nat'] && isset($fisexam['head_bca_l_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#head_bca_l_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="head_bcn_l" name="head_bc_l" <?php
                                            if ($fisexam['head_bca_l_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#head_bca_l_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="head_bca_l_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="head_bca_l_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="fracture_without_cerebrospinal_fluid_leaks" <?php
                                                if ($fisexam['head_bca_l_inj_nat'] === 'fracture_without_cerebrospinal_fluid_leaks'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Fratura sem fístula liquórica
                                                </option>
                                                <option value="fracture_with_cerebrospinal_fluid_leaks" <?php
                                                if ($fisexam['head_bca_l_inj_nat'] === 'fracture_with_cerebrospinal_fluid_leaks'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Fratura com fístula liquórica
                                                </option>
                                                <option value="open_fracture_comminuted" <?php
                                                if ($fisexam['head_bca_l_inj_nat'] === 'open_fracture_comminuted'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Fratura exposta ou cominutiva
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- FACE -->
                    <div class="well" style="margin-left:25px;">
                        <h3 class="panel-title pull-left">
                            <a href="javascript:void(0)" onclick="togglePanels('#face_evaluation', '#face_evaluation_options');">Avaliação da face</a>
                        </h3>
                        <div class="row-fluid">
                            <div id="face_evaluation_chev">
                                <a href="javascript:void(0)" onclick="togglePanels('#face_evaluation', '#face_evaluation_options');"><i class="fa fa-plus pull-right"></i></a>
                            </div>
                            <div id="face_evaluation_chevback" style="display:none;">
                                <a href="javascript:void(0)" onclick="togglePanels('#face_evaluation', '#face_evaluation_options');"><i class="fa fa-minus pull-right"></i></a>
                            </div>
                        </div>
                        <div id="face_evaluation" style="display:none;">
                            <hr>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" id="normal_face_evaluation"
                                    <?php
                                    if ($fisexam['normal_face_evaluation'] === 1): echo 'checked';
                                    endif;
                                    ?>>
                                    Avaliação da face sem alterações (normal)
                                </label>
                            </div>
                            <div class="row-fluid">
                                <div class="col-sm-5 well">
                                    <h4 class="text-center"> Lado direito </h4>
                                    <div class="row-fluid">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" id="normal_face_evaluation_r"
                                                <?php
                                                if ($fisexam['normal_face_evaluation_r'] === 1): echo 'checked';
                                                endif;
                                                ?>>
                                                Avaliação da face do lado direito sem alterações (normal)
                                            </label>
                                        </div>
                                    </div>
                                    <h4> Pele </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="face_pela_r" name="face_pel_r" <?php
                                            if ($fisexam['face_pela_r_inj_nat'] && isset($fisexam['face_pela_r_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#face_pela_r_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="face_peln_r" name="face_pel_r" <?php
                                            if ($fisexam['face_pela_r_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#face_pela_r_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="face_pela_r_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="face_pela_r_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="excoriation" <?php
                                                if ($fisexam['face_pela_r_inj_nat'] === 'excoriation'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Escoriação
                                                </option>
                                                <option value="superficial_laceration" <?php
                                                if ($fisexam['face_pela_r_inj_nat'] === 'superficial_laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração superficial
                                                </option>
                                                <option value="deep_laceration" <?php
                                                if ($fisexam['face_pela_r_inj_nat'] === 'deep_laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração profundo (>10cm)
                                                </option>
                                                <option value="av_small_tissue_loss" <?php
                                                if ($fisexam['face_pela_r_inj_nat'] === 'av_small_tissue_loss'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Avulsão com perda tecidual pequena (<= 25cm²)
                                                </option>
                                                <option value="av_big_tissue_loss" <?php
                                                if ($fisexam['face_pela_r_inj_nat'] === 'av_big_tissue_loss'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Avulsão com perda tecidual grande (>25cm²)
                                                </option>
                                                <option value="avul_comp" <?php
                                                if ($fisexam['face_pela_r_inj_nat'] === 'avul_comp'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Avulsão completa
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h4> Olhos </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="face_olha_r" name="face_olh_r" <?php
                                            if ($fisexam['face_olha_r_inj_nat'] && isset($fisexam['face_olha_r_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#face_olha_r_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="face_olhn_r" name="face_olh_r" <?php
                                            if ($fisexam['face_olha_r_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#face_olha_r_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="face_olha_r_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="face_olha_r_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="subconjunctival_hemorrhage" <?php
                                                if ($fisexam['face_olha_r_inj_nat'] === 'subconjunctival_hemorrhage'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Hemorragia subconjuntival
                                                </option>
                                                <option value="foreign_bodies_cornea" <?php
                                                if ($fisexam['face_olha_r_inj_nat'] === 'foreign_bodies_cornea'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Presença de corpos estranhos em córnea
                                                </option>
                                                <option value="corneal_brasion" <?php
                                                if ($fisexam['face_olha_r_inj_nat'] === 'corneal_brasion'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Abrasão de córnea
                                                </option>
                                                <option value="perforation_globe" <?php
                                                if ($fisexam['face_olha_r_inj_nat'] === 'perforation_globe'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Perfuração do globo ocular
                                                </option>
                                                <option value="unilateral_enucleation" <?php
                                                if ($fisexam['face_olha_r_inj_nat'] === 'unilateral_enucleation'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Enucleação unilateral
                                                </option>
                                                <option value="bilateral_enucleation" <?php
                                                if ($fisexam['face_olha_r_inj_nat'] === 'bilateral_enucleation'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Enucleação bilateral
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h4> Orelha </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="face_orea_r" name="face_ore_r" <?php
                                            if ($fisexam['face_orea_r_inj_nat'] && isset($fisexam['face_orea_r_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#face_orea_r_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="face_oren_r" name="face_ore_r" <?php
                                            if ($fisexam['face_orea_r_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#face_orea_r_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="face_orea_r_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="face_orea_r_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="middle_outer_ear_lesion" <?php
                                                if ($fisexam['face_orea_r_inj_nat'] === 'middle_outer_ear_lesion'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Lesão na orelha média e externa
                                                </option>
                                                <option value="rupture_tympanic_membrane" <?php
                                                if ($fisexam['face_orea_r_inj_nat'] === 'rupture_tympanic_membrane'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Ruptura da membrana timpânica
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h4> Boca, gengiva, língua e lábios </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="face_bgla_r" name="face_bgl_r" <?php
                                            if ($fisexam['face_bgla_r_inj_nat'] && isset($fisexam['face_bgla_r_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#face_bgla_r_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="face_bgln_r" name="face_bgl_r" <?php
                                            if ($fisexam['face_bgla_r_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#face_bgla_r_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="face_bgla_r_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="face_bgla_r_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="bruise" <?php
                                                if ($fisexam['face_bgla_r_inj_nat'] === 'bruise'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Contusão
                                                </option>
                                                <option value="superficial_laceration" <?php
                                                if ($fisexam['face_bgla_r_inj_nat'] === 'superficial_laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração superficial
                                                </option>
                                                <option value="laceration_deep_extensive" <?php
                                                if ($fisexam['face_bgla_r_inj_nat'] === 'laceration_deep_extensive'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração profunda ou extensa
                                                </option>
                                                <option value="loss_teeth" <?php
                                                if ($fisexam['face_bgla_r_inj_nat'] === 'loss_teeth'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Perda de dentes
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h4> Mandíbula </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="face_manda_r" name="face_mand_r" <?php
                                            if ($fisexam['face_manda_r_inj_nat'] && isset($fisexam['face_manda_r_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#face_manda_r_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="face_mandn_r" name="face_mand_r" <?php
                                            if ($fisexam['face_manda_r_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#face_manda_r_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="face_manda_r_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="face_manda_r_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="closed_fracture" <?php
                                                if ($fisexam['face_manda_r_inj_nat'] === 'closed_fracture'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Fratura fechada
                                                </option>
                                                <option value="open_comminuted_fracture" <?php
                                                if ($fisexam['face_manda_r_inj_nat'] === 'open_comminuted_fracture'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Fratura aberta ou cominutiva
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h4> Maxilar </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="face_maxia_r" name="face_maxi_r" <?php
                                            if ($fisexam['face_maxia_r_inj_nat'] && isset($fisexam['face_maxia_r_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#face_maxia_r_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="face_maxin_r" name="face_maxi_r" <?php
                                            if ($fisexam['face_maxia_r_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#face_maxia_r_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="face_maxia_r_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="face_maxia_r_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="lef1" <?php
                                                if ($fisexam['face_maxia_r_inj_nat'] === 'lef1'): echo 'selected';
                                                endif;
                                                ?>>
                                                    LeFort I
                                                </option>
                                                <option value="lef2" <?php
                                                if ($fisexam['face_maxia_r_inj_nat'] === 'lef2'): echo 'selected';
                                                endif;
                                                ?>>
                                                    LeFort II
                                                </option>
                                                <option value="lef3" <?php
                                                if ($fisexam['face_maxia_r_inj_nat'] === 'lef3'): echo 'selected';
                                                endif;
                                                ?>>
                                                    LeFort III
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h4> Nariz </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="face_nara_r" name="face_nar_r" <?php
                                            if ($fisexam['face_nara_r_inj_nat'] && isset($fisexam['face_nara_r_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#face_nara_r_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="face_narn_r" name="face_nar_r" <?php
                                            if ($fisexam['face_nara_r_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#face_nara_r_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="face_nara_r_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="face_nara_r_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="epistaxis_without_CSF_leak" <?php
                                                if ($fisexam['face_nara_r_inj_nat'] === 'epistaxis_without_CSF_leak'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Epistaxe sem fístula liquórica
                                                </option>
                                                <option value="epistaxis_cerebrospinal_leaks" <?php
                                                if ($fisexam['face_nara_r_inj_nat'] === 'epistaxis_cerebrospinal_leaks'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Epistaxe com fístula liquórica
                                                </option>
                                                <option value="closed_fracture" <?php
                                                if ($fisexam['face_nara_r_inj_nat'] === 'closed_fracture'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Fratura fechada
                                                </option>
                                                <option value="open_comminuted_fracture" <?php
                                                if ($fisexam['face_nara_r_inj_nat'] === 'open_comminuted_fracture'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Fratura aberta ou cominutiva
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h4> Órbita </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="face_orba_r" name="face_orb_r" <?php
                                            if ($fisexam['face_orba_r_inj_nat'] && isset($fisexam['face_orba_r_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#face_orba_r_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="face_orbn_r" name="face_orb_r" <?php
                                            if ($fisexam['face_orba_r_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#face_orba_r_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="face_orba_r_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="face_orba_r_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="closed_fracture" <?php
                                                if ($fisexam['face_orba_r_inj_nat'] === 'closed_fracture'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Fratura fechada
                                                </option>
                                                <option value="open_comminuted_fracture" <?php
                                                if ($fisexam['face_orba_r_inj_nat'] === 'open_comminuted_fracture'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Fratura aberta ou cominutiva
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h4> Zigoma </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="face_ziga_r" name="face_zig_r" <?php
                                            if ($fisexam['face_ziga_r_inj_nat'] && isset($fisexam['face_ziga_r_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#face_ziga_r_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="face_zign_r" name="face_zig_r" <?php
                                            if ($fisexam['face_ziga_r_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#face_ziga_r_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="face_ziga_r_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="face_ziga_r_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="closed_fracture" <?php
                                                if ($fisexam['face_ziga_r_inj_nat'] === 'closed_fracture'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Fratura fechada
                                                </option>
                                                <option value="open_comminuted_fracture" <?php
                                                if ($fisexam['face_ziga_r_inj_nat'] === 'open_comminuted_fracture'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Fratura aberta ou cominutiva
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-1"></div>

                                <div class="col-sm-5 well">
                                    <h4 class="text-center"> Lado esquerdo </h4>
                                    <div class="row-fluid">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" id="normal_face_evaluation_l"
                                                <?php
                                                if ($fisexam['normal_face_evaluation_l'] === 1): echo 'checked';
                                                endif;
                                                ?>>
                                                Avaliação da face do lado esquerdo sem alterações (normal)
                                            </label>
                                        </div>
                                    </div>
                                    <h4> Pele </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="face_pela_l" name="face_pel_l" <?php
                                            if ($fisexam['face_pela_l_inj_nat'] && isset($fisexam['face_pela_l_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#face_pela_l_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="face_peln_l" name="face_pel_l" <?php
                                            if ($fisexam['face_pela_l_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#face_pela_l_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="face_pela_l_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="face_pela_l_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="excoriation" <?php
                                                if ($fisexam['face_pela_l_inj_nat'] === 'excoriation'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Escoriação
                                                </option>
                                                <option value="superficial_laceration" <?php
                                                if ($fisexam['face_pela_l_inj_nat'] === 'superficial_laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração superficial
                                                </option>
                                                <option value="deep_laceration" <?php
                                                if ($fisexam['face_pela_l_inj_nat'] === 'deep_laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração profundo (>10cm)
                                                </option>
                                                <option value="av_small_tissue_loss" <?php
                                                if ($fisexam['face_pela_l_inj_nat'] === 'av_small_tissue_loss'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Avulsão com perda tecidual pequena (<= 25cm²)
                                                </option>
                                                <option value="av_big_tissue_loss" <?php
                                                if ($fisexam['face_pela_l_inj_nat'] === 'av_big_tissue_loss'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Avulsão com perda tecidual grande (>25cm²)
                                                </option>
                                                <option value="avul_comp" <?php
                                                if ($fisexam['face_pela_l_inj_nat'] === 'avul_comp'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Avulsão completa
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h4> Olhos </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="face_olha_l" name="face_olh_l" <?php
                                            if ($fisexam['face_olha_l_inj_nat'] && isset($fisexam['face_olha_l_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#face_olha_l_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="face_olhn_l" name="face_olh_l" <?php
                                            if ($fisexam['face_olha_l_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#face_olha_l_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="face_olha_l_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="face_olha_l_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="subconjunctival_hemorrhage" <?php
                                                if ($fisexam['face_olha_l_inj_nat'] === 'subconjunctival_hemorrhage'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Hemorragia subconjuntival
                                                </option>
                                                <option value="foreign_bodies_cornea" <?php
                                                if ($fisexam['face_olha_l_inj_nat'] === 'foreign_bodies_cornea'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Presença de corpos estranhos em córnea
                                                </option>
                                                <option value="corneal_brasion" <?php
                                                if ($fisexam['face_olha_l_inj_nat'] === 'corneal_brasion'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Abrasão de córnea
                                                </option>
                                                <option value="perforation_globe" <?php
                                                if ($fisexam['face_olha_l_inj_nat'] === 'perforation_globe'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Perfuração do globo ocular
                                                </option>
                                                <option value="unilateral_enucleation" <?php
                                                if ($fisexam['face_olha_l_inj_nat'] === 'unilateral_enucleation'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Enucleação unilateral
                                                </option>
                                                <option value="bilateral_enucleation" <?php
                                                if ($fisexam['face_olha_l_inj_nat'] === 'bilateral_enucleation'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Enucleação bilateral
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h4> Orelha </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="face_orea_l" name="face_ore_l" <?php
                                            if ($fisexam['face_orea_l_inj_nat'] && isset($fisexam['face_orea_l_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#face_orea_l_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="face_oren_l" name="face_ore_l" <?php
                                            if ($fisexam['face_orea_l_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#face_orea_l_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="face_orea_l_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="face_orea_l_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="middle_outer_ear_lesion" <?php
                                                if ($fisexam['face_orea_l_inj_nat'] === 'middle_outer_ear_lesion'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Lesão na orelha média e externa
                                                </option>
                                                <option value="rupture_tympanic_membrane" <?php
                                                if ($fisexam['face_orea_l_inj_nat'] === 'rupture_tympanic_membrane'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Ruptura da membrana timpânica
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h4> Boca, gengiva, língua e lábios </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="face_bgla_l" name="face_bgl_l" <?php
                                            if ($fisexam['face_blga_l_inj_nat'] && isset($fisexam['face_blga_l_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#face_bgla_l_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="face_bgln_l" name="face_bgl_l" <?php
                                            if ($fisexam['face_blga_l_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#face_bgla_l_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="face_bgla_l_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="face_blga_l_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="bruise" <?php
                                                if ($fisexam['face_blga_l_inj_nat'] === 'bruise'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Contusão
                                                </option>
                                                <option value="superficial_laceration" <?php
                                                if ($fisexam['face_blga_l_inj_nat'] === 'superficial_laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração superficial
                                                </option>
                                                <option value="laceration_deep_extensive" <?php
                                                if ($fisexam['face_blga_l_inj_nat'] === 'laceration_deep_extensive'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração profunda ou extensa
                                                </option>
                                                <option value="loss_teeth" <?php
                                                if ($fisexam['face_blga_l_inj_nat'] === 'loss_teeth'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Perda de dentes
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h4> Mandíbula </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="face_manda_l" name="face_mand_l" <?php
                                            if ($fisexam['face_manda_l_inj_nat'] && isset($fisexam['face_manda_l_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#face_manda_l_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="face_mandn_l" name="face_mand_l" <?php
                                            if ($fisexam['face_manda_l_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#face_manda_l_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="face_manda_l_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="face_manda_l_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="closed_fracture" <?php
                                                if ($fisexam['face_manda_l_inj_nat'] === 'closed_fracture'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Fratura fechada
                                                </option>
                                                <option value="open_comminuted_fracture" <?php
                                                if ($fisexam['face_manda_l_inj_nat'] === 'open_comminuted_fracture'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Fratura aberta ou cominutiva
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h4> Maxilar </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="face_maxia_l" name="face_maxi_l" <?php
                                            if ($fisexam['face_maxia_l_inj_nat'] && isset($fisexam['face_maxia_l_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#face_maxia_l_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="face_maxin_l" name="face_maxi_l" <?php
                                            if ($fisexam['face_maxia_l_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#face_maxia_l_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="face_maxia_l_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="face_maxia_l_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="lef1" <?php
                                                if ($fisexam['face_maxia_l_inj_nat'] === 'lef1'): echo 'selected';
                                                endif;
                                                ?>>
                                                    LeFort I
                                                </option>
                                                <option value="lef2" <?php
                                                if ($fisexam['face_maxia_l_inj_nat'] === 'lef2'): echo 'selected';
                                                endif;
                                                ?>>
                                                    LeFort II
                                                </option>
                                                <option value="lef3" <?php
                                                if ($fisexam['face_maxia_l_inj_nat'] === 'lef3'): echo 'selected';
                                                endif;
                                                ?>>
                                                    LeFort III
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h4> Nariz </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="face_nara_l" name="face_nar_l" <?php
                                            if ($fisexam['face_nara_l_inj_nat'] && isset($fisexam['face_nara_l_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#face_nara_l_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="face_narn_l" name="face_nar_l" <?php
                                            if ($fisexam['face_nara_l_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#face_nara_l_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="face_nara_l_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="face_nara_l_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="epistaxis_without_CSF_leak" <?php
                                                if ($fisexam['face_nara_l_inj_nat'] === 'epistaxis_without_CSF_leak'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Epistaxe sem fístula liquórica
                                                </option>
                                                <option value="epistaxis_cerebrospinal_leaks" <?php
                                                if ($fisexam['face_nara_l_inj_nat'] === 'epistaxis_cerebrospinal_leaks'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Epistaxe com fístula liquórica
                                                </option>
                                                <option value="closed_fracture" <?php
                                                if ($fisexam['face_nara_l_inj_nat'] === 'closed_fracture'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Fratura fechada
                                                </option>
                                                <option value="open_comminuted_fracture" <?php
                                                if ($fisexam['face_nara_l_inj_nat'] === 'open_comminuted_fracture'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Fratura aberta ou cominutiva
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h4> Órbita </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="face_orba_l" name="face_orb_l" <?php
                                            if ($fisexam['face_orba_l_inj_nat'] && isset($fisexam['face_orba_l_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#face_orba_l_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="face_orbn_l" name="face_orb_l" <?php
                                            if ($fisexam['face_orba_l_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#face_orba_l_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="face_orba_l_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="face_orba_l_inj_nat">
                                                <option value="closed_fracture" <?php
                                                if ($fisexam['face_orba_l_inj_nat'] === 'closed_fracture'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Fratura fechada
                                                </option>
                                                <option value="open_comminuted_fracture" <?php
                                                if ($fisexam['face_orba_l_inj_nat'] === 'open_comminuted_fracture'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Fratura aberta ou cominutiva
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h4> Zigoma </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="face_ziga_l" name="face_zig_l" <?php
                                            if ($fisexam['face_ziga_l_inj_nat'] && isset($fisexam['face_ziga_l_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#face_ziga_l_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="face_zign_l" name="face_zig_l" <?php
                                            if ($fisexam['face_ziga_l_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#face_ziga_l_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="face_ziga_l_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="face_ziga_l_inj_nat">
                                                <option value="closed_fracture" <?php
                                                if ($fisexam['face_ziga_l_inj_nat'] === 'closed_fracture'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Fratura fechada
                                                </option>
                                                <option value="open_comminuted_fracture" <?php
                                                if ($fisexam['face_ziga_l_inj_nat'] === 'open_comminuted_fracture'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Fratura aberta ou cominutiva
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- PESCOÇO -->
                    <div class="well" style="margin-left:25px;">
                        <h3 class="panel-title pull-left">
                            <a href="javascript:void(0)" onclick="togglePanels('#neck_evaluation', '#neck_evaluation_options');">Avaliação do pescoço</a>
                        </h3>
                        <div class="row-fluid">
                            <div id="neck_evaluation_chev">
                                <a href="javascript:void(0)" onclick="togglePanels('#neck_evaluation', '#neck_evaluation_options');"><i class="fa fa-plus pull-right"></i></a>
                            </div>
                            <div id="neck_evaluation_chevback" style="display:none;">
                                <a href="javascript:void(0)" onclick="togglePanels('#neck_evaluation', '#neck_evaluation_options');"><i class="fa fa-minus pull-right"></i></a>
                            </div>
                        </div>
                        <div id="neck_evaluation" style="display:none;">
                            <hr>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" id="normal_neck_evaluation"
                                    <?php
                                    if ($fisexam['normal_neck_evaluation'] === 1): echo 'checked';
                                    endif;
                                    ?>>
                                    Avaliação do pescoço sem alterações (normal)
                                </label>
                            </div>
                            <div class="row-fluid">
                                <div class="col-sm-5 well">
                                    <h4 class="text-center"> Lado direito </h4>
                                    <div class="row-fluid">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" id="normal_neck_evaluation_r"
                                                <?php
                                                if ($fisexam['normal_neck_evaluation_r'] === 1): echo 'checked';
                                                endif;
                                                ?>>
                                                Avaliação do pescoço do lado direito sem alterações (normal)
                                            </label>
                                        </div>
                                    </div>
                                    <h4> Pele </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="neck_pela_r" name="neck_pel_r" <?php
                                            if ($fisexam['neck_pela_r_inj_nat'] && isset($fisexam['neck_pela_r_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#neck_pela_r_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="neck_peln_r" name="neck_pel_r" <?php
                                            if ($fisexam['neck_pela_r_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#neck_pela_r_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="neck_pela_r_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="neck_pela_r_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="excoriation" <?php
                                                if ($fisexam['neck_pela_r_inj_nat'] === 'excoriation'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Escoriação
                                                </option>
                                                <option value="superficial_laceration" <?php
                                                if ($fisexam['neck_pela_r_inj_nat'] === 'superficial_laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração superficial
                                                </option>
                                                <option value="deep_laceration" <?php
                                                if ($fisexam['neck_pela_r_inj_nat'] === 'deep_laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração profundo (>10cm)
                                                </option>
                                                <option value="av_small_tissue_loss" <?php
                                                if ($fisexam['neck_pela_r_inj_nat'] === 'av_small_tissue_loss'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Avulsão com perda tecidual pequena (<= 25cm²)
                                                </option>
                                                <option value="av_big_tissue_loss" <?php
                                                if ($fisexam['neck_pela_r_inj_nat'] === 'av_big_tissue_loss'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Avulsão com perda tecidual grande (>25cm²)
                                                </option>
                                                <option value="avul_comp" <?php
                                                if ($fisexam['neck_pela_r_inj_nat'] === 'avul_comp'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Avulsão completa
                                                </option>
                                                <option value="avul_comp" <?php
                                                if ($fisexam['neck_pela_r_inj_nat'] === 'avul_comp'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Presença de sangramento venoso ativo
                                                </option>
                                                <option value="avul_comp" <?php
                                                if ($fisexam['neck_pela_r_inj_nat'] === 'avul_comp'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Presença de sangramento arterial ativo
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h4> Pulso carotídeo </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="neck_pca_r" name="neck_pc_r" <?php
                                            if ($fisexam['neck_pca_r_inj_nat'] && isset($fisexam['neck_pca_r_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#neck_pca_r_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="neck_pcn_r" name="neck_pc_r" <?php
                                            if ($fisexam['neck_pca_r_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#neck_pca_r_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="neck_pca_r_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="neck_pca_r_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="asymmetry" <?php
                                                if ($fisexam['neck_pca_r_inj_nat'] === 'asymmetry'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Assimetria
                                                </option>
                                                <option value="presence_tremor" <?php
                                                if ($fisexam['neck_pca_r_inj_nat'] === 'presence_tremor'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Presença de frêmito
                                                </option>
                                                <option value="presence_breath" <?php
                                                if ($fisexam['neck_pca_r_inj_nat'] === 'presence_breath'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Presença de sopro
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-1"></div>

                                <div class="col-sm-5 well">
                                    <h4 class="text-center"> Lado esquerdo </h4>
                                    <div class="row-fluid">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" id="normal_neck_evaluation_l"
                                                <?php
                                                if ($fisexam['normal_neck_evaluation_l'] === 1): echo 'checked';
                                                endif;
                                                ?>>
                                                Avaliação do pescoço do lado esquerdo sem alterações (normal)
                                            </label>
                                        </div>
                                    </div>
                                    <h4> Pele </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="neck_pela_l" name="neck_pel_l" <?php
                                            if ($fisexam['neck_pela_l_inj_nat'] && isset($fisexam['neck_pela_l_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#neck_pela_l_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="neck_peln_l" name="neck_pel_l" <?php
                                            if ($fisexam['neck_pela_l_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#neck_pela_l_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="neck_pela_l_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="neck_pela_l_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="excoriation" <?php
                                                if ($fisexam['neck_pela_l_inj_nat'] === 'excoriation'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Escoriação
                                                </option>
                                                <option value="superficial_laceration" <?php
                                                if ($fisexam['neck_pela_l_inj_nat'] === 'superficial_laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração superficial
                                                </option>
                                                <option value="deep_laceration" <?php
                                                if ($fisexam['neck_pela_l_inj_nat'] === 'deep_laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração profundo (>10cm)
                                                </option>
                                                <option value="av_small_tissue_loss" <?php
                                                if ($fisexam['neck_pela_l_inj_nat'] === 'av_small_tissue_loss'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Avulsão com perda tecidual pequena (<= 25cm²)
                                                </option>
                                                <option value="av_big_tissue_loss" <?php
                                                if ($fisexam['neck_pela_l_inj_nat'] === 'av_big_tissue_loss'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Avulsão com perda tecidual grande (>25cm²)
                                                </option>
                                                <option value="avul_comp" <?php
                                                if ($fisexam['neck_pela_l_inj_nat'] === 'avul_comp'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Avulsão completa
                                                </option>
                                                <option value="avul_comp" <?php
                                                if ($fisexam['neck_pela_l_inj_nat'] === 'avul_comp'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Presença de sangramento venoso ativo
                                                </option>
                                                <option value="avul_comp" <?php
                                                if ($fisexam['neck_pela_l_inj_nat'] === 'avul_comp'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Presença de sangramento arterial ativo
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h4> Pulso carotídeo </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="neck_pca_l" name="neck_pc_l" <?php
                                            if ($fisexam['neck_pca_l_inj_nat'] && isset($fisexam['neck_pca_l_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#neck_pca_l_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="neck_pcn_l" name="neck_pc_l" <?php
                                            if ($fisexam['neck_pca_l_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#neck_pca_l_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="neck_pca_l_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="neck_pca_l_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="asymmetry" <?php
                                                if ($fisexam['neck_pca_l_inj_nat'] === 'asymmetry'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Assimetria
                                                </option>
                                                <option value="presence_tremor" <?php
                                                if ($fisexam['neck_pca_l_inj_nat'] === 'presence_tremor'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Presença de frêmito
                                                </option>
                                                <option value="presence_breath" <?php
                                                if ($fisexam['neck_pca_l_inj_nat'] === 'presence_breath'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Presença de sopro
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- COLUNA -->
                    <div class="well" style="margin-left:25px;">
                        <h3 class="panel-title pull-left">
                            <a href="javascript:void(0)" onclick="togglePanels('#spine_evaluation', '#spine_evaluation_options');">Avaliação da coluna</a>
                        </h3>
                        <div class="row-fluid">
                            <div id="spine_evaluation_chev">
                                <a href="javascript:void(0)" onclick="togglePanels('#spine_evaluation', '#spine_evaluation_options');"><i class="fa fa-plus pull-right"></i></a>
                            </div>
                            <div id="spine_evaluation_chevback" style="display:none;">
                                <a href="javascript:void(0)" onclick="togglePanels('#spine_evaluation', '#spine_evaluation_options');"><i class="fa fa-minus pull-right"></i></a>
                            </div>
                        </div>
                        <div id="spine_evaluation" style="display:none;">
                            <hr>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" id="normal_spine_evaluation"
                                    <?php
                                    if ($fisexam['normal_spine_evaluation'] === 1): echo 'checked';
                                    endif;
                                    ?>>
                                    Avaliação da coluna sem alterações (normal)
                                </label>
                            </div>
                            <div class="row-fluid">
                                <div class="col-sm-5 well">
                                    <h4 class="text-center"> Lado direito </h4>
                                    <div class="row-fluid">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" id="normal_spine_evaluation_r"
                                                <?php
                                                if ($fisexam['normal_spine_evaluation_r'] === 1): echo 'checked';
                                                endif;
                                                ?>>
                                                Avaliação da coluna do lado direito sem alterações (normal)
                                            </label>
                                        </div>
                                    </div>
                                    <h4> Coluna cervical </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="spine_cca_r" name="spine_cc_r" <?php
                                            if ($fisexam['spine_cca_r_inj_nat'] && isset($fisexam['spine_cca_r_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#spine_cca_r_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="spine_ccn_r" name="spine_cc_r" <?php
                                            if ($fisexam['spine_cca_r_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#spine_cca_r_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="spine_cca_r_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="spine_cca_r_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="presence_referred_pain" <?php
                                                if ($fisexam['spine_cca_r_inj_nat'] === 'presence_referred_pain'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Presença de dor referida
                                                </option>
                                                <option value="pain_palpation_later" <?php
                                                if ($fisexam['spine_cca_r_inj_nat'] === 'pain_palpation_later'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Dor à palpação posterior
                                                </option>
                                                <option value="degree_presence_deviation_later_boss" <?php
                                                if ($fisexam['spine_cca_r_inj_nat'] === 'degree_presence_deviation_later_boss'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Presença de degrau, desvio ou saliência posterior
                                                </option>
                                                <option value="pain_active_mobilization_neck" <?php
                                                if ($fisexam['spine_cca_r_inj_nat'] === 'pain_active_mobilization_neck'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Dor à mobilização ativa do pescoço
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h4> Coluna torácica </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="spine_cta_r" name="spine_ct_r" <?php
                                            if ($fisexam['spine_cta_r_inj_nat'] && isset($fisexam['spine_cta_r_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#spine_cta_r_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="spine_ctn_r" name="spine_ct_r" <?php
                                            if ($fisexam['spine_cta_r_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#spine_cta_r_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="spine_cta_r_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="spine_cta_r_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="bruise_hematoma" <?php
                                                if ($fisexam['spine_cta_r_inj_nat'] === 'bruise_hematoma'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Contusão ou hematoma
                                                </option>
                                                <option value="parcial_laceration" <?php
                                                if ($fisexam['spine_cta_r_inj_nat'] === 'parcial_laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração parcial
                                                </option>
                                                <option value="deep_laceration" <?php
                                                if ($fisexam['spine_cta_r_inj_nat'] === 'deep_laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração profunda
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h4> Coluna lombo sacra </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="spine_clsa_r" name="spine_ct_r" <?php
                                            if ($fisexam['spine_clsa_r_inj_nat'] && isset($fisexam['spine_clsa_r_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#spine_clsa_r_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="spine_ctn_r" name="spine_ct_r" <?php
                                            if ($fisexam['spine_clsa_r_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#spine_clsa_r_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="spine_clsa_r_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="spine_clsa_r_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="bruise_hematoma" <?php
                                                if ($fisexam['spine_clsa_r_inj_nat'] === 'bruise_hematoma'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Contusão ou hematoma
                                                </option>
                                                <option value="parcial_laceration" <?php
                                                if ($fisexam['spine_clsa_r_inj_nat'] === 'parcial_laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração parcial
                                                </option>
                                                <option value="deep_laceration" <?php
                                                if ($fisexam['spine_clsa_r_inj_nat'] === 'deep_laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração profunda
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-1"></div>

                                <div class="col-sm-5 well">
                                    <h4 class="text-center"> Lado esquerdo </h4>
                                    <div class="row-fluid">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" id="normal_spine_evaluation_l"
                                                <?php
                                                if ($fisexam['normal_spine_evaluation_l'] === 1): echo 'checked';
                                                endif;
                                                ?>>
                                                Avaliação da coluna do lado esquerdo sem alterações (normal)
                                            </label>
                                        </div>
                                    </div>
                                    <h4> Coluna cervical </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="spine_cca_l" name="spine_cc_l" <?php
                                            if ($fisexam['spine_cca_l_inj_nat'] && isset($fisexam['spine_cca_l_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#spine_cca_l_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="spine_ccn_l" name="spine_cc_l" <?php
                                            if ($fisexam['spine_cca_l_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#spine_cca_l_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="spine_cca_l_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="spine_cca_l_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="presence_referred_pain" <?php
                                                if ($fisexam['spine_cca_l_inj_nat'] === 'presence_referred_pain'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Presença de dor referida
                                                </option>
                                                <option value="pain_palpation_later" <?php
                                                if ($fisexam['spine_cca_l_inj_nat'] === 'pain_palpation_later'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Dor à palpação posterior
                                                </option>
                                                <option value="degree_presence_deviation_later_boss" <?php
                                                if ($fisexam['spine_cca_l_inj_nat'] === 'degree_presence_deviation_later_boss'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Presença de degrau, desvio ou saliência posterior
                                                </option>
                                                <option value="pain_active_mobilization_neck" <?php
                                                if ($fisexam['spine_cca_l_inj_nat'] === 'pain_active_mobilization_neck'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Dor à mobilização ativa do pescoço
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h4> Coluna torácica </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="spine_cta_l" name="spine_ct_l" <?php
                                            if ($fisexam['spine_cta_l_inj_nat'] && isset($fisexam['spine_cta_l_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#spine_cta_l_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="spine_ctn_l" name="spine_ct_l" <?php
                                            if ($fisexam['spine_cta_l_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#spine_cta_l_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="spine_cta_l_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="spine_cta_l_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="bruise_hematoma" <?php
                                                if ($fisexam['spine_cta_l_inj_nat'] === 'bruise_hematoma'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Contusão ou hematoma
                                                </option>
                                                <option value="parcial_laceration" <?php
                                                if ($fisexam['spine_cta_l_inj_nat'] === 'parcial_laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração parcial
                                                </option>
                                                <option value="deep_laceration" <?php
                                                if ($fisexam['spine_cta_l_inj_nat'] === 'deep_laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração profunda
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h4> Coluna lombo sacra </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="spine_clsa_l" name="spine_ct_l" <?php
                                            if ($fisexam['spine_clsa_l_inj_nat'] && isset($fisexam['spine_clsa_l_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#spine_clsa_l_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="spine_ctn_l" name="spine_ct_l" <?php
                                            if ($fisexam['spine_clsa_l_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#spine_clsa_l_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="spine_clsa_l_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="spine_clsa_l_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="bruise_hematoma" <?php
                                                if ($fisexam['spine_clsa_l_inj_nat'] === 'bruise_hematoma'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Contusão ou hematoma
                                                </option>
                                                <option value="parcial_laceration" <?php
                                                if ($fisexam['spine_clsa_l_inj_nat'] === 'parcial_laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração parcial
                                                </option>
                                                <option value="deep_laceration" <?php
                                                if ($fisexam['spine_clsa_l_inj_nat'] === 'deep_laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração profunda
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- TORAX -->
                    <div class="well" style="margin-left:25px;">
                        <h3 class="panel-title pull-left">
                            <a href="javascript:void(0)" onclick="togglePanels('#chest_evaluation', '#chest_evaluation_options');">Avaliação do tórax</a>
                        </h3>
                        <div class="row-fluid">
                            <div id="chest_evaluation_chev">
                                <a href="javascript:void(0)" onclick="togglePanels('#chest_evaluation', '#chest_evaluation_options');"><i class="fa fa-plus pull-right"></i></a>
                            </div>
                            <div id="chest_evaluation_chevback" style="display:none;">
                                <a href="javascript:void(0)" onclick="togglePanels('#chest_evaluation', '#chest_evaluation_options');"><i class="fa fa-minus pull-right"></i></a>
                            </div>
                        </div>
                        <div id="chest_evaluation" style="display:none;">
                            <hr>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" id="normal_chest_evaluation"
                                    <?php
                                    if ($fisexam['normal_chest_evaluation'] === 1): echo 'checked';
                                    endif;
                                    ?>>
                                    Avaliação de tórax sem alterações (normal)
                                </label>
                            </div>
                            <div class="row-fluid">
                                <div class="col-sm-5 well">
                                    <h4 class="text-center"> Lado direito </h4>
                                    <div class="row-fluid">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" id="normal_chest_evaluation_r"
                                                <?php
                                                if ($fisexam['normal_chest_evaluation_r'] === 1): echo 'checked';
                                                endif;
                                                ?>>
                                                Avaliação de tórax do lado direito sem alterações (normal)
                                            </label>
                                        </div>
                                    </div>
                                    <h4> Inspeção estática e dinâmica </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="chest_ieda_r" name="chest_ied_r" <?php
                                            if ($fisexam['chest_ieda_r_inj_nat'] && isset($fisexam['chest_ieda_r_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#chest_ieda_r_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="chest_iedn_r" name="chest_ied_r" <?php
                                            if ($fisexam['chest_ieda_r_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#chest_ieda_r_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="chest_ieda_r_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="chest_ieda_r_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="excoriation" <?php
                                                if ($fisexam['chest_ieda_r_inj_nat'] === 'excoriation'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Escoriação
                                                </option>
                                                <option value="superficial_laceration" <?php
                                                if ($fisexam['chest_ieda_r_inj_nat'] === 'superficial_laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração superficial
                                                </option>
                                                <option value="subcutaneous_emphysema" <?php
                                                if ($fisexam['chest_ieda_r_inj_nat'] === 'subcutaneous_emphysema'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Enfisema subcutâneo
                                                </option>
                                                <option value="crackling_bone" <?php
                                                if ($fisexam['chest_ieda_r_inj_nat'] === 'crackling_bone'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Crepitação óssea
                                                </option>
                                                <option value="thoracic_sinking_with_asymmetry" <?php
                                                if ($fisexam['chest_ieda_r_inj_nat'] === 'thoracic_sinking_with_asymmetry'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Afundamento torácico com assimetria
                                                </option>
                                                <option value="deep_laceration" <?php
                                                if ($fisexam['chest_ieda_r_inj_nat'] === 'secondary'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração profunda (>20cm)
                                                </option>
                                                <option value="av_with_small_tissue_loss" <?php
                                                if ($fisexam['chest_ieda_r_inj_nat'] === 'av_with_small_tissue_loss'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Avulsão com perda tecidual pequena (<= 100cm²)
                                                </option>
                                                <option value="av_with_big_tissue_loss" <?php
                                                if ($fisexam['chest_ieda_r_inj_nat'] === 'av_with_big_tissue_loss'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Avulsão com perda tecidual grande (> 100cm²)
                                                </option>
                                                <option value="av_complete" <?php
                                                if ($fisexam['chest_ieda_r_inj_nat'] === 'av_complete'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Avulsão completa
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h4> Percussão </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="chest_perca_r" name="chest_perc_r" <?php
                                            if ($fisexam['chest_perca_r_inj_nat'] && isset($fisexam['chest_perca_r_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#chest_perca_r_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="chest_percn_r" name="chest_perc_r" <?php
                                            if ($fisexam['chest_perca_r_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#chest_perca_r_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="chest_perca_r_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="chest_perca_r_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="hypertympanism" <?php
                                                if ($fisexam['chest_perca_r_inj_nat'] === 'hypertympanism'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Hipertimpanismo
                                                </option>
                                                <option value="dullness" <?php
                                                if ($fisexam['chest_perca_r_inj_nat'] === 'dullness'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Macicez
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h4> Ausculta </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="chest_ausca_r" name="chest_ausc_r" <?php
                                            if ($fisexam['chest_ausca_r_inj_nat'] && isset($fisexam['chest_ausca_r_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#chest_ausca_r_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="chest_auscn_r" name="chest_ausc_r" <?php
                                            if ($fisexam['chest_ausca_r_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#chest_ausca_r_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="chest_ausca_r_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="chest_ausca_r_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="vesicular_murmur_diminished" <?php
                                                if ($fisexam['chest_ausca_r_inj_nat'] === 'vesicular_murmur_diminished'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Murmúrio vesicular diminuído
                                                </option>
                                                <option value="missing_abolished_breath_sounds" <?php
                                                if ($fisexam['chest_ausca_r_inj_nat'] === 'missing_abolished_breath_sounds'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Murmúrio vesicular ausente ou abolido
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-1"></div>

                                <div class="col-sm-5 well">
                                    <h4 class="text-center"> Lado esquerdo </h4>
                                    <div class="row-fluid">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" id="normal_chest_evaluation_l"
                                                <?php
                                                if ($fisexam['normal_chest_evaluation_l'] === 1): echo 'checked';
                                                endif;
                                                ?>>
                                                Avaliação de tórax do lado esquerdo sem alterações (normal)
                                            </label>
                                        </div>
                                    </div>
                                    <h4> Inspeção estática e dinâmica </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="chest_ieda_l" name="chest_ied_l" <?php
                                            if ($fisexam['chest_ieda_l_inj_nat'] && isset($fisexam['chest_ieda_l_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#chest_ieda_l_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="chest_iedn_l" name="chest_ied_l" <?php
                                            if ($fisexam['chest_ieda_l_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#chest_ieda_l_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="chest_ieda_l_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="chest_ieda_l_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="excoriation" <?php
                                                if ($fisexam['chest_ieda_l_inj_nat'] === 'excoriation'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Escoriação
                                                </option>
                                                <option value="superficial_laceration" <?php
                                                if ($fisexam['chest_ieda_l_inj_nat'] === 'superficial_laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração superficial
                                                </option>
                                                <option value="subcutaneous_emphysema" <?php
                                                if ($fisexam['chest_ieda_l_inj_nat'] === 'subcutaneous_emphysema'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Enfisema subcutâneo
                                                </option>
                                                <option value="crackling_bone" <?php
                                                if ($fisexam['chest_ieda_l_inj_nat'] === 'crackling_bone'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Crepitação óssea
                                                </option>
                                                <option value="thoracic_sinking_with_asymmetry" <?php
                                                if ($fisexam['chest_ieda_l_inj_nat'] === 'thoracic_sinking_with_asymmetry'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Afundamento torácico com assimetria
                                                </option>
                                                <option value="deep_laceration" <?php
                                                if ($fisexam['chest_ieda_l_inj_nat'] === 'secondary'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração profunda (>20cm)
                                                </option>
                                                <option value="av_with_small_tissue_loss" <?php
                                                if ($fisexam['chest_ieda_l_inj_nat'] === 'av_with_small_tissue_loss'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Avulsão com perda tecidual pequena (<= 100cm²)
                                                </option>
                                                <option value="av_with_big_tissue_loss" <?php
                                                if ($fisexam['chest_ieda_l_inj_nat'] === 'av_with_big_tissue_loss'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Avulsão com perda tecidual grande (> 100cm²)
                                                </option>
                                                <option value="av_complete" <?php
                                                if ($fisexam['chest_ieda_l_inj_nat'] === 'av_complete'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Avulsão completa
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h4> Percussão </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="chest_perca_l" name="chest_perc_l" <?php
                                            if ($fisexam['chest_perca_l_inj_nat'] && isset($fisexam['chest_perca_l_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#chest_ieda_l_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="chest_percn_l" name="chest_perc_l" <?php
                                            if ($fisexam['chest_perca_l_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#chest_ieda_l_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="chest_perca_l_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="chest_perca_l_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="hypertympanism" <?php
                                                if ($fisexam['chest_perca_l_inj_nat'] === 'hypertympanism'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Hipertimpanismo
                                                </option>
                                                <option value="dullness" <?php
                                                if ($fisexam['chest_perca_l_inj_nat'] === 'dullness'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Macicez
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h4> Ausculta </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="chest_ausca_l" name="chest_ausc_l" <?php
                                            if ($fisexam['chest_ausca_l_inj_nat'] && isset($fisexam['chest_ausca_l_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#chest_ieda_l_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="chest_auscn_l" name="chest_ausc_l" <?php
                                            if ($fisexam['chest_ausca_l_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#chest_ieda_l_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="chest_ausca_l_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="chest_ausca_l_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="vesicular_murmur_diminished" <?php
                                                if ($fisexam['chest_ausca_l_inj_nat'] === 'vesicular_murmur_diminished'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Murmúrio vesicular diminuído
                                                </option>
                                                <option value="missing_abolished_breath_sounds" <?php
                                                if ($fisexam['chest_ausca_l_inj_nat'] === 'missing_abolished_breath_sounds'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Murmúrio vesicular ausente ou abolido
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- ABDOME -->
                    <div class="well" style="margin-left:25px;">
                        <h3 class="panel-title pull-left">
                            <a href="javascript:void(0)" onclick="togglePanels('#belly_evaluation', '#belly_evaluation_options');">Avaliação do abdome</a>
                        </h3>
                        <div class="row-fluid">
                            <div id="belly_evaluation_chev">
                                <a href="javascript:void(0)" onclick="togglePanels('#belly_evaluation', '#belly_evaluation_options');"><i class="fa fa-plus pull-right"></i></a>
                            </div>
                            <div id="belly_evaluation_chevback" style="display:none;">
                                <a href="javascript:void(0)" onclick="togglePanels('#belly_evaluation', '#belly_evaluation_options');"><i class="fa fa-minus pull-right"></i></a>
                            </div>
                        </div>
                        <div id="belly_evaluation" style="display:none;">
                            <hr>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" id="normal_belly_evaluation"
                                    <?php
                                    if ($fisexam['normal_belly_evaluation'] === 1): echo 'checked';
                                    endif;
                                    ?>>
                                    Avaliação do abdome sem alterações (normal)
                                </label>
                            </div>
                            <div class="row-fluid">
                                <div class="col-sm-5 well">
                                    <h4 class="text-center"> Lado direito </h4>
                                    <div class="row-fluid">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" id="normal_belly_evaluation_r"
                                                <?php
                                                if ($fisexam['normal_belly_evaluation_r'] === 1): echo 'checked';
                                                endif;
                                                ?>>
                                                Avaliação do abdome do lado direito sem alterações (normal)
                                            </label>
                                        </div>
                                    </div>
                                    <h4> Inspeção estática e dinâmica </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="belly_ieda_r" name="belly_ied_r" <?php
                                            if ($fisexam['belly_ieda_r_inj_nat'] && isset($fisexam['belly_ieda_r_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#belly_ieda_r_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="belly_iedn_r" name="belly_ied_r" <?php
                                            if ($fisexam['belly_ieda_r_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#belly_ieda_r_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="belly_ieda_r_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="belly_ieda_r_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="excoriation" <?php
                                                if ($fisexam['belly_ieda_r_inj_nat'] === 'excoriation'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Escoriação
                                                </option>
                                                <option value="superficial_laceration" <?php
                                                if ($fisexam['belly_ieda_r_inj_nat'] === 'superficial_laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração superficial
                                                </option>
                                                <option value="deep_laceration" <?php
                                                if ($fisexam['belly_ieda_r_inj_nat'] === 'secondary'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração profunda (>20cm)
                                                </option>
                                                <option value="av_with_small_tissue_loss" <?php
                                                if ($fisexam['belly_ieda_r_inj_nat'] === 'av_with_small_tissue_loss'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Avulsão com perda tecidual pequena (<= 100cm²)
                                                </option>
                                                <option value="av_with_big_tissue_loss" <?php
                                                if ($fisexam['belly_ieda_r_inj_nat'] === 'av_with_big_tissue_loss'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Avulsão com perda tecidual grande (> 100cm²)
                                                </option>
                                                <option value="paradoxical_breathing" <?php
                                                if ($fisexam['belly_ieda_r_inj_nat'] === 'paradoxical_breathing'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Respiração paradoxal
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h4> Percussão </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="belly_perca_r" name="belly_perc_r" <?php
                                            if ($fisexam['belly_perca_r_inj_nat'] && isset($fisexam['belly_perca_r_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#belly_perca_r_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="belly_percn_r" name="belly_perc_r" <?php
                                            if ($fisexam['belly_perca_r_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#belly_perca_r_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="belly_perca_r_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="belly_perca_r_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="hypertympanism" <?php
                                                if ($fisexam['belly_perca_r_inj_nat'] === 'hypertympanism'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Hipertimpanismo
                                                </option>
                                                <option value="dullness" <?php
                                                if ($fisexam['belly_perca_r_inj_nat'] === 'dullness'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Macicez
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-1"></div>

                                <div class="col-sm-5 well">
                                    <h4 class="text-center"> Lado esquerdo </h4>
                                    <div class="row-fluid">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" id="normal_belly_evaluation_l"
                                                <?php
                                                if ($fisexam['normal_belly_evaluation_l'] === 1): echo 'checked';
                                                endif;
                                                ?>>
                                                Avaliação do abdome do lado esquerdo sem alterações (normal)
                                            </label>
                                        </div>
                                    </div>
                                    <h4> Inspeção estática e dinâmica </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="belly_ieda_l" name="belly_ied_l" <?php
                                            if ($fisexam['belly_ieda_l_inj_nat'] && isset($fisexam['belly_ieda_l_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#belly_ieda_l_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="belly_iedn_l" name="belly_ied_l" <?php
                                            if ($fisexam['belly_ieda_l_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#belly_ieda_l_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="belly_ieda_l_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="belly_ieda_l_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="excoriation" <?php
                                                if ($fisexam['belly_ieda_l_inj_nat'] === 'excoriation'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Escoriação
                                                </option>
                                                <option value="superficial_laceration" <?php
                                                if ($fisexam['belly_ieda_l_inj_nat'] === 'superficial_laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração superficial
                                                </option>
                                                <option value="deep_laceration" <?php
                                                if ($fisexam['belly_ieda_l_inj_nat'] === 'secondary'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração profunda (>20cm)
                                                </option>
                                                <option value="av_with_small_tissue_loss" <?php
                                                if ($fisexam['belly_ieda_l_inj_nat'] === 'av_with_small_tissue_loss'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Avulsão com perda tecidual pequena (<= 100cm²)
                                                </option>
                                                <option value="av_with_big_tissue_loss" <?php
                                                if ($fisexam['belly_ieda_l_inj_nat'] === 'av_with_big_tissue_loss'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Avulsão com perda tecidual grande (> 100cm²)
                                                </option>
                                                <option value="paradoxical_breathing" <?php
                                                if ($fisexam['belly_ieda_l_inj_nat'] === 'paradoxical_breathing'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Respiração paradoxal
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h4> Percussão </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="belly_perca_l" name="belly_perc_l" <?php
                                            if ($fisexam['belly_perca_l_inj_nat'] && isset($fisexam['belly_perca_l_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#belly_perca_l_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="belly_percn_l" name="belly_perc_l" <?php
                                            if ($fisexam['belly_perca_l_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#belly_perca_l_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="belly_perca_l_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="belly_perca_l_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="hypertympanism" <?php
                                                if ($fisexam['belly_perca_l_inj_nat'] === 'hypertympanism'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Hipertimpanismo
                                                </option>
                                                <option value="dullness" <?php
                                                if ($fisexam['belly_perca_l_inj_nat'] === 'dullness'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Macicez
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- PELVE -->
                    <div class="well" style="margin-left:25px;">
                        <h3 class="panel-title pull-left">
                            <a href="javascript:void(0)" onclick="togglePanels('#pelvis_evaluation', '#pelvis_evaluation_options');">Avaliação da pelve</a>
                        </h3>
                        <div class="row-fluid">
                            <div id="pelvis_evaluation_chev">
                                <a href="javascript:void(0)" onclick="togglePanels('#pelvis_evaluation', '#pelvis_evaluation_options');"><i class="fa fa-plus pull-right"></i></a>
                            </div>
                            <div id="pelvis_evaluation_chevback" style="display:none;">
                                <a href="javascript:void(0)" onclick="togglePanels('#pelvis_evaluation', '#pelvis_evaluation_options');"><i class="fa fa-minus pull-right"></i></a>
                            </div>
                        </div>
                        <div id="pelvis_evaluation" style="display:none;">
                            <hr>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" id="normal_pelvis_evaluation"
                                    <?php
                                    if ($fisexam['normal_pelvis_evaluation'] === 1): echo 'checked';
                                    endif;
                                    ?>>
                                    Avaliação da pelve sem alterações (normal)
                                </label>
                            </div>
                            <div class="row-fluid">
                                <div class="col-sm-5 well">
                                    <h4 class="text-center"> Lado direito </h4>
                                    <div class="row-fluid">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" id="normal_pelvis_evaluation_r"
                                                <?php
                                                if ($fisexam['normal_pelvis_evaluation_r'] === 1): echo 'checked';
                                                endif;
                                                ?>>
                                                Avaliação da pelve do lado direito sem alterações (normal)
                                            </label>
                                        </div>
                                    </div>
                                    <h4> Pele </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="pelvis_pela_r" name="pelvis_pel_r" <?php
                                            if ($fisexam['pelvis_pela_r_inj_nat'] && isset($fisexam['pelvis_pela_r_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#pelvis_peln_r_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="pelvis_peln_r" name="pelvis_pel_r" <?php
                                            if ($fisexam['pelvis_pela_r_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#pelvis_peln_r_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="pelvis_pela_r_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="pelvis_pela_r_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="bruise_hematoma" <?php
                                                if ($fisexam['pelvis_pela_r_inj_nat'] === 'bruise_hematoma'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Contusão ou hematoma
                                                </option>
                                                <option value="parcial_laceration" <?php
                                                if ($fisexam['pelvis_pela_r_inj_nat'] === 'parcial_laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração parcial
                                                </option>
                                                <option value="deep_laceration" <?php
                                                if ($fisexam['pelvis_pela_r_inj_nat'] === 'deep_laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração profunda
                                                </option>
                                                <option value="av_tec_loss" <?php
                                                if ($fisexam['pelvis_pela_r_inj_nat'] === 'av_tec_loss'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Avulsão com perda tecidual
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h4> Palpação </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="pelvis_palpa_r" name="pelvis_palp_r"  <?php
                                            if ($fisexam['pelvis_palpa_r_inj_nat'] && isset($fisexam['pelvis_palpa_r_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#pelvis_palpa_r_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="pelvis_palpn_r" name="pelvis_palp_r"  <?php
                                            if ($fisexam['pelvis_palpa_r_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#pelvis_palpa_r_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="pelvis_palpa_r_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="pelvis_palpa_r_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="pain_crep_comp_pelvis" <?php
                                                if ($fisexam['pelvis_palpa_r_inj_nat'] === 'pain_crep_comp_pelvis'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Dor e crepitação à compressão lateral da pelve
                                                </option>
                                                <option value="pain_crep_comp_pubis" <?php
                                                if ($fisexam['pelvis_palpa_r_inj_nat'] === 'pain_crep_comp_pubis'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Dor e crepitação à compressão anterior do púbis
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-1"></div>

                                <div class="col-sm-5 well">
                                    <h4 class="text-center"> Lado esquerdo </h4>
                                    <div class="row-fluid">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" id="normal_pelvis_evaluation_l"
                                                <?php
                                                if ($fisexam['normal_pelvis_evaluation_l'] === 1): echo 'checked';
                                                endif;
                                                ?>>
                                                Avaliação da pelve do lado esquerdo sem alterações (normal)
                                            </label>
                                        </div>
                                    </div>
                                    <h4> Pele </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="pelvis_pela_l" name="pelvis_pel_l"  <?php
                                            if ($fisexam['face_pela_r_inj_nat'] && isset($fisexam['face_pela_r_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#pelvis_pela_l_inj_nat').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="pelvis_peln_l" name="pelvis_pel_l"  <?php
                                            if ($fisexam['face_pela_r_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#pelvis_pela_l_inj_nat').hide();">Exame normal</label>
                                    </div>
                                    <div id="pelvis_pela_l_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="pelvis_pela_l_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="bruise_hematoma" <?php
                                                if ($fisexam['pelvis_pela_l_inj_nat'] === 'bruise_hematoma'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Contusão ou hematoma
                                                </option>
                                                <option value="parcial_laceration" <?php
                                                if ($fisexam['pelvis_pela_l_inj_nat'] === 'parcial_laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração parcial
                                                </option>
                                                <option value="deep_laceration" <?php
                                                if ($fisexam['pelvis_pela_l_inj_nat'] === 'deep_laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração profunda
                                                </option>
                                                <option value="av_tec_loss" <?php
                                                if ($fisexam['pelvis_pela_l_inj_nat'] === 'av_tec_loss'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Avulsão com perda tecidual
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h4> Palpação </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="pelvis_palpa_l" name="pelvis_palp_l"  <?php
                                            if ($fisexam['pelvis_palpa_l_inj_nat'] && isset($fisexam['pelvis_palpa_l_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#pelvis_palpa_l_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="pelvis_palpn_l" name="pelvis_palp_l"  <?php
                                            if ($fisexam['pelvis_palpa_l_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#pelvis_palpa_l_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="pelvis_palpa_l_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="pelvis_palpa_l_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="pain_crep_comp_pelvis" <?php
                                                if ($fisexam['pelvis_palpa_l_inj_nat'] === 'pain_crep_comp_pelvis'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Dor e crepitação à compressão lateral da pelve
                                                </option>
                                                <option value="pain_crep_comp_pubis" <?php
                                                if ($fisexam['pelvis_palpa_l_inj_nat'] === 'pain_crep_comp_pubis'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Dor e crepitação à compressão anterior do púbis
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- PERINEO, RETO, GENITALIA -->
                    <div class="well" style="margin-left:25px;">
                        <h3 class="panel-title pull-left">
                            <a href="javascript:void(0)" onclick="togglePanels('#prg_evaluation', '#prg_evaluation_options');">Avaliação do períneo, reto e genitália</a>
                        </h3>
                        <div class="row-fluid">
                            <div id="prg_evaluation_chev">
                                <a href="javascript:void(0)" onclick="togglePanels('#prg_evaluation', '#prg_evaluation_options');"><i class="fa fa-plus pull-right"></i></a>
                            </div>
                            <div id="prg_evaluation_chevback" style="display:none;">
                                <a href="javascript:void(0)" onclick="togglePanels('#prg_evaluation', '#prg_evaluation_options');"><i class="fa fa-minus pull-right"></i></a>
                            </div>
                        </div>
                        <div id="prg_evaluation" style="display:none;">
                            <hr>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" id="normal_prg_evaluation"
                                    <?php
                                    if ($fisexam['normal_prg_evaluation'] === 1): echo 'checked';
                                    endif;
                                    ?>>
                                    Avaliação do períneo, reto e genitália sem alterações (normal)
                                </label>
                            </div>
                            <div class="row-fluid">
                                <div class="col-sm-5 well">
                                    <h4 class="text-center"> Lado direito </h4>
                                    <div class="row-fluid">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" id="normal_prg_evaluation_r"
                                                <?php
                                                if ($fisexam['normal_prg_evaluation_r'] === 1): echo 'checked';
                                                endif;
                                                ?>>
                                                Avaliação do períneo, reto e genitália do lado direito sem alterações (normal)
                                            </label>
                                        </div>
                                    </div>
                                    <h4> Períneo </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="prg_pera_r" name="prg_per_r"  <?php
                                            if ($fisexam['pelvis_pera_r_inj_nat'] && isset($fisexam['pelvis_pera_r_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#prg_pera_r_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="prg_pern_r" name="prg_per_r"  <?php
                                            if ($fisexam['pelvis_pera_r_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#prg_pera_r_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="prg_pera_r_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="pelvis_pera_r_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="cont_hem" <?php
                                                if ($fisexam['pelvis_pera_r_inj_nat'] === 'cont_hem'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Contusão ou hematoma
                                                </option>
                                                <option value="uret" <?php
                                                if ($fisexam['pelvis_pera_r_inj_nat'] === 'uret'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Uretrorragia
                                                </option>
                                                <option value="parcial_laceration" <?php
                                                if ($fisexam['pelvis_pera_r_inj_nat'] === 'parcial_laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração parcial (em perfuração)
                                                </option>
                                                <option value="deep_laceration" <?php
                                                if ($fisexam['pelvis_pera_r_inj_nat'] === 'deep_laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração profunda (com perfuração)
                                                </option>
                                                <option value="av_tecidual_loss" <?php
                                                if ($fisexam['pelvis_pera_r_inj_nat'] === 'av_tecidual_loss'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Avulsão com perda tecidual
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h4> Toque retal </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="prg_toqret_r" name="prg_per_r"  <?php
                                            if ($fisexam['pelvis_toqret_r_inj_nat'] && isset($fisexam['pelvis_toqret_r_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#prg_toqret_r_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="prg_pern_r" name="prg_per_r"  <?php
                                            if ($fisexam['pelvis_toqret_r_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#prg_toqret_r_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="prg_toqret_r_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="pelvis_toqret_r_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="herbca" <?php
                                                if ($fisexam['pelvis_toqret_r_inj_nat'] === 'herbca'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Hipotonia do esfíncter com reflexo bulbo-cavernoso ausente
                                                </option>
                                                <option value="herbcp" <?php
                                                if ($fisexam['pelvis_toqret_r_inj_nat'] === 'herbcp'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Hipotonia do esfíncter com reflexo bulbo-cavernoso presente
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h4> Pênis </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="prg_pena_r" name="prg_pen_r" <?php
                                            if ($fisexam['pelvis_pena_r_inj_nat'] && isset($fisexam['pelvis_pena_r_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#prg_pena_r_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="prg_penn_r" name="prg_pen_r" <?php
                                            if ($fisexam['pelvis_pena_r_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#prg_pena_r_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="prg_pena_r_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="pelvis_pena_r_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="cont_hem" <?php
                                                if ($fisexam['pelvis_pena_r_inj_nat'] === 'cont_hem'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Contusão ou hematoma
                                                </option>
                                                <option value="parcial_laceration" <?php
                                                if ($fisexam['pelvis_pena_r_inj_nat'] === 'parcial_laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração parcial
                                                </option>
                                                <option value="deep_laceration" <?php
                                                if ($fisexam['pelvis_pena_r_inj_nat'] === 'deep_laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração profunda
                                                </option>
                                                <option value="av_tissue_loss" <?php
                                                if ($fisexam['pelvis_pena_r_inj_nat'] === 'av_tissue_loss'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Avulsão com perda tecidual extensa ou maciça
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h4> Testículos </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="prg_testa_r" name="prg_test_r" <?php
                                            if ($fisexam['pelvis_testa_r_inj_nat'] && isset($fisexam['pelvis_testa_r_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#prg_testa_rdetail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="prg_testn_r" name="prg_test_r"<?php
                                            if ($fisexam['pelvis_testa_r_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#prg_testa_rdetail').hide();">Exame normal</label>
                                    </div>
                                    <div id="prg_testa_r_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="pelvis_testa_r_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="cont_hem" <?php
                                                if ($fisexam['pelvis_testa_r_inj_nat'] === 'cont_hem'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Contusão ou hematoma
                                                </option>
                                                <option value="parcial_laceration" <?php
                                                if ($fisexam['pelvis_testa_r_inj_nat'] === 'parcial_laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração parcial
                                                </option>
                                                <option value="deep_laceration" <?php
                                                if ($fisexam['pelvis_testa_r_inj_nat'] === 'deep_laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração profunda
                                                </option>
                                                <option value="av_tissue_loss" <?php
                                                if ($fisexam['pelvis_testa_r_inj_nat'] === 'av_tissue_loss'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Avulsão com perda tecidual extensa ou maciça
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h4> Vulva </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="prg_vulva_r" name="prg_vulv_r"  <?php
                                            if ($fisexam['pelvis_vulva_r_inj_nat'] && isset($fisexam['pelvis_vulva_r_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#prg_vulva_r_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="prg_vulvn_r" name="prg_vulv_r"  <?php
                                            if ($fisexam['pelvis_vulva_r_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#prg_vulva_r_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="prg_vulva_r_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="pelvis_vulva_r_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="cont_hem" <?php
                                                if ($fisexam['pelvis_vulva_r_inj_nat'] === 'cont_hem'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Contusão ou hematoma
                                                </option>
                                                <option value="parcial_laceration" <?php
                                                if ($fisexam['pelvis_vulva_r_inj_nat'] === 'parcial_laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração parcial
                                                </option>
                                                <option value="deep_laceration" <?php
                                                if ($fisexam['pelvis_vulva_r_inj_nat'] === 'deep_laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração profunda
                                                </option>
                                                <option value="av_tissue_loss" <?php
                                                if ($fisexam['pelvis_vulva_r_inj_nat'] === 'av_tissue_loss'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Avulsão com perda tecidual extensa ou maciça
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-1"></div>

                                <div class="col-sm-5 well">
                                    <h4 class="text-center"> Lado esquerdo </h4>
                                    <div class="row-fluid">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" id="normal_prg_evaluation_l"
                                                <?php
                                                if ($fisexam['normal_prg_evaluation_l'] === 1): echo 'checked';
                                                endif;
                                                ?>>
                                                Avaliação do períneo, reto e genitália do lado esquerdo sem alterações (normal)
                                            </label>
                                        </div>
                                    </div>
                                    <h4> Períneo </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="prg_pera_l" name="prg_per_l"  <?php
                                            if ($fisexam['pelvis_pera_l_inj_nat'] && isset($fisexam['pelvis_pera_l_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#prg_pera_l_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="prg_pern_l" name="prg_per_l"  <?php
                                            if ($fisexam['pelvis_pera_l_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#prg_pera_l_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="prg_pera_l_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="pelvis_pera_l_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="cont_hem" <?php
                                                if ($fisexam['pelvis_pera_l_inj_nat'] === 'cont_hem'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Contusão ou hematoma
                                                </option>
                                                <option value="uret" <?php
                                                if ($fisexam['pelvis_pera_l_inj_nat'] === 'uret'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Uretrorragia
                                                </option>
                                                <option value="parcial_laceration" <?php
                                                if ($fisexam['pelvis_pera_l_inj_nat'] === 'parcial_laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração parcial (em perfuração)
                                                </option>
                                                <option value="deep_laceration" <?php
                                                if ($fisexam['pelvis_pera_l_inj_nat'] === 'deep_laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração profunda (com perfuração)
                                                </option>
                                                <option value="av_tecidual_loss" <?php
                                                if ($fisexam['pelvis_pera_l_inj_nat'] === 'av_tecidual_loss'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Avulsão com perda tecidual
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h4> Toque retal </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="prg_toqret_l" name="prg_per_l"  <?php
                                            if ($fisexam['pelvis_toqret_l_inj_nat'] && isset($fisexam['pelvis_toqret_l_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#prg_toqret_l_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="prg_pern_l" name="prg_per_l"  <?php
                                            if ($fisexam['pelvis_toqret_l_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#prg_toqret_l_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="prg_toqret_l_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="pelvis_toqret_l_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="herbca" <?php
                                                if ($fisexam['pelvis_toqret_l_inj_nat'] === 'herbca'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Hipotonia do esfíncter com reflexo bulbo-cavernoso ausente
                                                </option>
                                                <option value="herbcp" <?php
                                                if ($fisexam['pelvis_toqret_l_inj_nat'] === 'herbcp'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Hipotonia do esfíncter com reflexo bulbo-cavernoso presente
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h4> Pênis </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="prg_pena_l" name="prg_pen_l"  <?php
                                            if ($fisexam['pelvis_pena_l_inj_nat'] && isset($fisexam['pelvis_pena_l_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#prg_pena_l_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="prg_penn_l" name="prg_pen_l"  <?php
                                            if ($fisexam['pelvis_pena_l_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#prg_pena_l_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="prg_pena_l_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="pelvis_pena_l_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="cont_hem" <?php
                                                if ($fisexam['pelvis_pena_l_inj_nat'] === 'cont_hem'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Contusão ou hematoma
                                                </option>
                                                <option value="parcial_laceration" <?php
                                                if ($fisexam['pelvis_pena_l_inj_nat'] === 'parcial_laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração parcial
                                                </option>
                                                <option value="deep_laceration" <?php
                                                if ($fisexam['pelvis_pena_l_inj_nat'] === 'deep_laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração profunda
                                                </option>
                                                <option value="av_tissue_loss" <?php
                                                if ($fisexam['pelvis_pena_l_inj_nat'] === 'av_tissue_loss'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Avulsão com perda tecidual extensa ou maciça
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h4> Testículos </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="prg_testa_l" name="prg_test_l"  <?php
                                            if ($fisexam['pelvis_testa_l_inj_nat'] && isset($fisexam['pelvis_testa_l_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#prg_testa_l_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="prg_testn_l" name="prg_test_l"  <?php
                                            if ($fisexam['pelvis_testa_l_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#prg_testa_l_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="prg_testa_l_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="pelvis_testa_l_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="cont_hem" <?php
                                                if ($fisexam['pelvis_testa_l_inj_nat'] === 'cont_hem'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Contusão ou hematoma
                                                </option>
                                                <option value="parcial_laceration" <?php
                                                if ($fisexam['pelvis_testa_l_inj_nat'] === 'parcial_laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração parcial
                                                </option>
                                                <option value="deep_laceration" <?php
                                                if ($fisexam['pelvis_testa_l_inj_nat'] === 'deep_laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração profunda
                                                </option>
                                                <option value="av_tissue_loss" <?php
                                                if ($fisexam['pelvis_testa_l_inj_nat'] === 'av_tissue_loss'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Avulsão com perda tecidual extensa ou maciça
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h4> Vulva </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="prg_vulva_l" name="prg_vulv_l"  <?php
                                            if ($fisexam['pelvis_vulva_l_inj_nat'] && isset($fisexam['pelvis_vulva_l_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#prg_vulva_l_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="prg_vulvn_l" name="prg_vulv_l"  <?php
                                            if ($fisexam['pelvis_vulva_l_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#prg_vulva_l_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="prg_vulva_l_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="pelvis_vulva_l_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="cont_hem" <?php
                                                if ($fisexam['pelvis_vulva_l_inj_nat'] === 'cont_hem'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Contusão ou hematoma
                                                </option>
                                                <option value="parcial_laceration" <?php
                                                if ($fisexam['pelvis_vulva_l_inj_nat'] === 'parcial_laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração parcial
                                                </option>
                                                <option value="deep_laceration" <?php
                                                if ($fisexam['pelvis_vulva_l_inj_nat'] === 'deep_laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração profunda
                                                </option>
                                                <option value="av_tissue_loss" <?php
                                                if ($fisexam['pelvis_vulva_l_inj_nat'] === 'av_tissue_loss'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Avulsão com perda tecidual extensa ou maciça
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- ORTOPEDICA -->
                    <div class="well" style="margin-left:25px;">
                        <h3 class="panel-title pull-left">
                            <a href="javascript:void(0)" onclick="togglePanels('#ortop_evaluation', '#ortop_evaluation_options');">Avaliação ortopédica</a>
                        </h3>
                        <div class="row-fluid">
                            <div id="ortop_evaluation_chev">
                                <a href="javascript:void(0)" onclick="togglePanels('#ortop_evaluation', '#ortop_evaluation_options');"><i class="fa fa-plus pull-right"></i></a>
                            </div>
                            <div id="ortop_evaluation_chevback" style="display:none;">
                                <a href="javascript:void(0)" onclick="togglePanels('#ortop_evaluation', '#ortop_evaluation_options');"><i class="fa fa-minus pull-right"></i></a>
                            </div>
                        </div>
                        <div id="ortop_evaluation" style="display:none;">
                            <hr>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" id="normal_ortop_evaluation"
                                    <?php
                                    if ($fisexam['normal_ortop_evaluation'] === 1): echo 'checked';
                                    endif;
                                    ?>>
                                    Avaliação ortopédica sem alterações (normal)
                                </label>
                            </div>
                            <div class="row-fluid">
                                <div class="col-sm-5 well">
                                    <h4 class="text-center"> Lado direito (extremidades superiores)</h4>
                                    <div class="row-fluid">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" id="normal_ortop_evaluation_rs"
                                                <?php
                                                if ($fisexam['normal_ortop_evaluation_rs'] === 1): echo 'checked';
                                                endif;
                                                ?>>
                                                Avaliação ortopédica do lado direito sem alterações (normal)
                                            </label>
                                        </div>
                                    </div>
                                    <h4> Clavícula </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="ortop_clava_rs" name="ortop_clav_rs" <?php
                                            if ($fisexam['ortop_clava_rs_inj_nat'] && isset($fisexam['ortop_clava_rs_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#ortop_clava_rs_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="ortop_clavn_rs" name="ortop_clav_rs" <?php
                                            if ($fisexam['ortop_clava_rs_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#ortop_clava_rs_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="ortop_clava_rs_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="ortop_clava_rs_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="contus" <?php
                                                if ($fisexam['ortop_clava_rs_inj_nat'] === 'contus'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Contusão
                                                </option>
                                                <option value="hem" <?php
                                                if ($fisexam['ortop_clava_rs_inj_nat'] === 'hem'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Hematoma
                                                </option>
                                                <option value="volume_plus" <?php
                                                if ($fisexam['ortop_clava_rs_inj_nat'] === 'volume_plus'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Aumento de volume de partes moles
                                                </option>
                                                <option value="pain_moviment" <?php
                                                if ($fisexam['ortop_clava_rs_inj_nat'] === 'pain_moviment'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Dor à movimentação
                                                </option>
                                                <option value="crackling_bone" <?php
                                                if ($fisexam['ortop_clava_rs_inj_nat'] === 'crackling_bone'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Crepitação óssea
                                                </option>
                                                <option value="anormal_mob" <?php
                                                if ($fisexam['ortop_clava_rs_inj_nat'] === 'anormal_mob'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Mobilidade anormal
                                                </option>
                                                <option value="functional_imp" <?php
                                                if ($fisexam['ortop_clava_rs_inj_nat'] === 'functional_imp'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Impotência funcional
                                                </option>
                                                <option value="def" <?php
                                                if ($fisexam['ortop_clava_rs_inj_nat'] === 'def'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Deformidade
                                                </option>
                                                <option value="rotation" <?php
                                                if ($fisexam['ortop_clava_rs_inj_nat'] === 'rotation'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Rotação
                                                </option>
                                                <option value="laceration" <?php
                                                if ($fisexam['ortop_clava_rs_inj_nat'] === 'laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração
                                                </option>
                                                <option value="bleeding" <?php
                                                if ($fisexam['ortop_clava_rs_inj_nat'] === 'bleeding'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Sangramento
                                                </option>
                                                <option value="mot_change" <?php
                                                if ($fisexam['ortop_clava_rs_inj_nat'] === 'mot_change'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Alteração da motricidade
                                                </option>
                                                <option value="infusion_change" <?php
                                                if ($fisexam['ortop_clava_rs_inj_nat'] === 'infusion_change'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Alteração da perfusão
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h4> Ombro </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="ortop_omba_rs" name="ortop_omb_rs" <?php
                                            if ($fisexam['ortop_omba_rs_inj_nat'] && isset($fisexam['ortop_omba_rs_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#ortop_omba_rs_inj_nat').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="ortop_ombn_rs" name="ortop_omb_rs" <?php
                                            if ($fisexam['ortop_omba_rs_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#ortop_omba_rs_inj_nat').hide();">Exame normal</label>
                                    </div>
                                    <div id="ortop_omba_rs_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="ortop_omba_rs_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="contus" <?php
                                                if ($fisexam['ortop_omba_rs_inj_nat'] === 'contus'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Contusão
                                                </option>
                                                <option value="hem" <?php
                                                if ($fisexam['ortop_omba_rs_inj_nat'] === 'hem'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Hematoma
                                                </option>
                                                <option value="volume_plus" <?php
                                                if ($fisexam['ortop_omba_rs_inj_nat'] === 'volume_plus'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Aumento de volume de partes moles
                                                </option>
                                                <option value="pain_moviment" <?php
                                                if ($fisexam['ortop_omba_rs_inj_nat'] === 'pain_moviment'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Dor à movimentação
                                                </option>
                                                <option value="crackling_bone" <?php
                                                if ($fisexam['ortop_omba_rs_inj_nat'] === 'crackling_bone'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Crepitação óssea
                                                </option>
                                                <option value="anormal_mob" <?php
                                                if ($fisexam['ortop_omba_rs_inj_nat'] === 'anormal_mob'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Mobilidade anormal
                                                </option>
                                                <option value="functional_imp" <?php
                                                if ($fisexam['ortop_omba_rs_inj_nat'] === 'functional_imp'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Impotência funcional
                                                </option>
                                                <option value="def" <?php
                                                if ($fisexam['ortop_omba_rs_inj_nat'] === 'def'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Deformidade
                                                </option>
                                                <option value="rotation" <?php
                                                if ($fisexam['ortop_omba_rs_inj_nat'] === 'rotation'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Rotação
                                                </option>
                                                <option value="laceration" <?php
                                                if ($fisexam['ortop_omba_rs_inj_nat'] === 'laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração
                                                </option>
                                                <option value="bleeding" <?php
                                                if ($fisexam['ortop_omba_rs_inj_nat'] === 'bleeding'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Sangramento
                                                </option>
                                                <option value="mot_change" <?php
                                                if ($fisexam['ortop_omba_rs_inj_nat'] === 'mot_change'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Alteração da motricidade
                                                </option>
                                                <option value="infusion_change" <?php
                                                if ($fisexam['ortop_omba_rs_inj_nat'] === 'infusion_change'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Alteração da perfusão
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h4> Braço </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="ortop_brca_rs" name="ortop_brac_rs" <?php
                                            if ($fisexam['ortop_brca_rs_inj_nat'] && isset($fisexam['ortop_brca_rs_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#ortop_brca_rs_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="ortop_brcn_rs" name="ortop_brac_rs" <?php
                                            if ($fisexam['ortop_brca_rs_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#ortop_brca_rs_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="ortop_brca_rs_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="ortop_brca_rs_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="contus" <?php
                                                if ($fisexam['ortop_brca_rs_inj_nat'] === 'contus'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Contusão
                                                </option>
                                                <option value="hem" <?php
                                                if ($fisexam['ortop_brca_rs_inj_nat'] === 'hem'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Hematoma
                                                </option>
                                                <option value="volume_plus" <?php
                                                if ($fisexam['ortop_brca_rs_inj_nat'] === 'volume_plus'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Aumento de volume de partes moles
                                                </option>
                                                <option value="pain_moviment" <?php
                                                if ($fisexam['ortop_brca_rs_inj_nat'] === 'pain_moviment'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Dor à movimentação
                                                </option>
                                                <option value="crackling_bone" <?php
                                                if ($fisexam['ortop_brca_rs_inj_nat'] === 'crackling_bone'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Crepitação óssea
                                                </option>
                                                <option value="anormal_mob" <?php
                                                if ($fisexam['ortop_brca_rs_inj_nat'] === 'anormal_mob'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Mobilidade anormal
                                                </option>
                                                <option value="functional_imp" <?php
                                                if ($fisexam['ortop_brca_rs_inj_nat'] === 'functional_imp'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Impotência funcional
                                                </option>
                                                <option value="def" <?php
                                                if ($fisexam['ortop_brca_rs_inj_nat'] === 'def'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Deformidade
                                                </option>
                                                <option value="rotation" <?php
                                                if ($fisexam['ortop_brca_rs_inj_nat'] === 'rotation'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Rotação
                                                </option>
                                                <option value="laceration" <?php
                                                if ($fisexam['ortop_brca_rs_inj_nat'] === 'laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração
                                                </option>
                                                <option value="bleeding" <?php
                                                if ($fisexam['ortop_brca_rs_inj_nat'] === 'bleeding'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Sangramento
                                                </option>
                                                <option value="mot_change" <?php
                                                if ($fisexam['ortop_brca_rs_inj_nat'] === 'mot_change'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Alteração da motricidade
                                                </option>
                                                <option value="infusion_change" <?php
                                                if ($fisexam['ortop_brca_rs_inj_nat'] === 'infusion_change'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Alteração da perfusão
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h4> Cotovelo </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="ortop_cota_rs" name="ortop_cot_rs" <?php
                                            if ($fisexam['ortop_cota_rs_inj_nat'] && isset($fisexam['ortop_cota_rs_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#ortop_cota_rs_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="ortop_cotn_rs" name="ortop_cot_rs" <?php
                                            if ($fisexam['ortop_cota_rs_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#ortop_cota_rs_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="ortop_cota_rs_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="ortop_cota_rs_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="contus" <?php
                                                if ($fisexam['ortop_cota_rs_inj_nat'] === 'contus'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Contusão
                                                </option>
                                                <option value="hem" <?php
                                                if ($fisexam['ortop_cota_rs_inj_nat'] === 'hem'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Hematoma
                                                </option>
                                                <option value="volume_plus" <?php
                                                if ($fisexam['ortop_cota_rs_inj_nat'] === 'volume_plus'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Aumento de volume de partes moles
                                                </option>
                                                <option value="pain_moviment" <?php
                                                if ($fisexam['ortop_cota_rs_inj_nat'] === 'pain_moviment'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Dor à movimentação
                                                </option>
                                                <option value="crackling_bone" <?php
                                                if ($fisexam['ortop_cota_rs_inj_nat'] === 'crackling_bone'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Crepitação óssea
                                                </option>
                                                <option value="anormal_mob" <?php
                                                if ($fisexam['ortop_cota_rs_inj_nat'] === 'anormal_mob'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Mobilidade anormal
                                                </option>
                                                <option value="functional_imp" <?php
                                                if ($fisexam['ortop_cota_rs_inj_nat'] === 'functional_imp'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Impotência funcional
                                                </option>
                                                <option value="def" <?php
                                                if ($fisexam['ortop_cota_rs_inj_nat'] === 'def'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Deformidade
                                                </option>
                                                <option value="rotation" <?php
                                                if ($fisexam['ortop_cota_rs_inj_nat'] === 'rotation'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Rotação
                                                </option>
                                                <option value="laceration" <?php
                                                if ($fisexam['ortop_cota_rs_inj_nat'] === 'laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração
                                                </option>
                                                <option value="bleeding" <?php
                                                if ($fisexam['ortop_cota_rs_inj_nat'] === 'bleeding'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Sangramento
                                                </option>
                                                <option value="mot_change" <?php
                                                if ($fisexam['ortop_cota_rs_inj_nat'] === 'mot_change'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Alteração da motricidade
                                                </option>
                                                <option value="infusion_change" <?php
                                                if ($fisexam['ortop_cota_rs_inj_nat'] === 'infusion_change'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Alteração da perfusão
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h4> Antebraço </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="ortop_anta_rs" name="ortop_ant_rs" <?php
                                            if ($fisexam['ortop_anta_rs_inj_nat'] && isset($fisexam['ortop_anta_rs_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#ortop_anta_rs_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="ortop_antn_rs" name="ortop_ant_rs" <?php
                                            if ($fisexam['ortop_anta_rs_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#ortop_anta_rs_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="ortop_anta_rs_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="ortop_anta_rs_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="contus" <?php
                                                if ($fisexam['ortop_anta_rs_inj_nat'] === 'contus'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Contusão
                                                </option>
                                                <option value="hem" <?php
                                                if ($fisexam['ortop_anta_rs_inj_nat'] === 'hem'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Hematoma
                                                </option>
                                                <option value="volume_plus" <?php
                                                if ($fisexam['ortop_anta_rs_inj_nat'] === 'volume_plus'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Aumento de volume de partes moles
                                                </option>
                                                <option value="pain_moviment" <?php
                                                if ($fisexam['ortop_anta_rs_inj_nat'] === 'pain_moviment'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Dor à movimentação
                                                </option>
                                                <option value="crackling_bone" <?php
                                                if ($fisexam['ortop_anta_rs_inj_nat'] === 'crackling_bone'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Crepitação óssea
                                                </option>
                                                <option value="anormal_mob" <?php
                                                if ($fisexam['ortop_anta_rs_inj_nat'] === 'anormal_mob'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Mobilidade anormal
                                                </option>
                                                <option value="functional_imp" <?php
                                                if ($fisexam['ortop_anta_rs_inj_nat'] === 'functional_imp'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Impotência funcional
                                                </option>
                                                <option value="def" <?php
                                                if ($fisexam['ortop_anta_rs_inj_nat'] === 'def'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Deformidade
                                                </option>
                                                <option value="rotation" <?php
                                                if ($fisexam['ortop_anta_rs_inj_nat'] === 'rotation'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Rotação
                                                </option>
                                                <option value="laceration" <?php
                                                if ($fisexam['ortop_anta_rs_inj_nat'] === 'laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração
                                                </option>
                                                <option value="bleeding" <?php
                                                if ($fisexam['ortop_anta_rs_inj_nat'] === 'bleeding'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Sangramento
                                                </option>
                                                <option value="mot_change" <?php
                                                if ($fisexam['ortop_anta_rs_inj_nat'] === 'mot_change'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Alteração da motricidade
                                                </option>
                                                <option value="infusion_change" <?php
                                                if ($fisexam['ortop_anta_rs_inj_nat'] === 'infusion_change'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Alteração da perfusão
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h4> Punho </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="ortop_puna_rs" name="ortop_pun_rs" <?php
                                            if ($fisexam['ortop_puna_rs_inj_nat'] && isset($fisexam['ortop_puna_rs_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#ortop_puna_rs_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="ortop_punn_rs" name="ortop_pun_rs" <?php
                                            if ($fisexam['ortop_puna_rs_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#ortop_puna_rs_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="ortop_puna_rs_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="ortop_puna_rs_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="contus" <?php
                                                if ($fisexam['ortop_puna_rs_inj_nat'] === 'contus'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Contusão
                                                </option>
                                                <option value="hem" <?php
                                                if ($fisexam['ortop_puna_rs_inj_nat'] === 'hem'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Hematoma
                                                </option>
                                                <option value="volume_plus" <?php
                                                if ($fisexam['ortop_puna_rs_inj_nat'] === 'volume_plus'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Aumento de volume de partes moles
                                                </option>
                                                <option value="pain_moviment" <?php
                                                if ($fisexam['ortop_puna_rs_inj_nat'] === 'pain_moviment'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Dor à movimentação
                                                </option>
                                                <option value="crackling_bone" <?php
                                                if ($fisexam['ortop_puna_rs_inj_nat'] === 'crackling_bone'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Crepitação óssea
                                                </option>
                                                <option value="anormal_mob" <?php
                                                if ($fisexam['ortop_puna_rs_inj_nat'] === 'anormal_mob'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Mobilidade anormal
                                                </option>
                                                <option value="functional_imp" <?php
                                                if ($fisexam['ortop_puna_rs_inj_nat'] === 'functional_imp'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Impotência funcional
                                                </option>
                                                <option value="def" <?php
                                                if ($fisexam['ortop_puna_rs_inj_nat'] === 'def'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Deformidade
                                                </option>
                                                <option value="rotation" <?php
                                                if ($fisexam['ortop_puna_rs_inj_nat'] === 'rotation'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Rotação
                                                </option>
                                                <option value="laceration" <?php
                                                if ($fisexam['ortop_puna_rs_inj_nat'] === 'laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração
                                                </option>
                                                <option value="bleeding" <?php
                                                if ($fisexam['ortop_puna_rs_inj_nat'] === 'bleeding'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Sangramento
                                                </option>
                                                <option value="mot_change" <?php
                                                if ($fisexam['ortop_puna_rs_inj_nat'] === 'mot_change'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Alteração da motricidade
                                                </option>
                                                <option value="infusion_change" <?php
                                                if ($fisexam['ortop_puna_rs_inj_nat'] === 'infusion_change'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Alteração da perfusão
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h4> Mão </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="ortop_maoa_rs" name="ortop_maoa_rs_inj_nat" <?php
                                            if ($fisexam['ortop_maoa_rs_inj_nat'] && isset($fisexam['ortop_maoa_rs_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#ortop_maoa_rs_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="ortop_maon_rs" name="ortop_maoa_rs_inj_nat" <?php
                                            if ($fisexam['ortop_puna_rs_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> disabled onclick="jQuery('#ortop_maoa_rs_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="ortop_maoa_rs_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="ortop_maoa_rs_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="contus" <?php
                                                if ($fisexam['ortop_maoa_rs_inj_nat'] === 'contus'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Contusão
                                                </option>
                                                <option value="hem" <?php
                                                if ($fisexam['ortop_maoa_rs_inj_nat'] === 'hem'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Hematoma
                                                </option>
                                                <option value="volume_plus" <?php
                                                if ($fisexam['ortop_maoa_rs_inj_nat'] === 'volume_plus'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Aumento de volume de partes moles
                                                </option>
                                                <option value="pain_moviment" <?php
                                                if ($fisexam['ortop_maoa_rs_inj_nat'] === 'pain_moviment'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Dor à movimentação
                                                </option>
                                                <option value="crackling_bone" <?php
                                                if ($fisexam['ortop_maoa_rs_inj_nat'] === 'crackling_bone'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Crepitação óssea
                                                </option>
                                                <option value="anormal_mob" <?php
                                                if ($fisexam['ortop_maoa_rs_inj_nat'] === 'anormal_mob'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Mobilidade anormal
                                                </option>
                                                <option value="functional_imp" <?php
                                                if ($fisexam['ortop_maoa_rs_inj_nat'] === 'functional_imp'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Impotência funcional
                                                </option>
                                                <option value="def" <?php
                                                if ($fisexam['ortop_maoa_rs_inj_nat'] === 'def'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Deformidade
                                                </option>
                                                <option value="rotation" <?php
                                                if ($fisexam['ortop_maoa_rs_inj_nat'] === 'rotation'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Rotação
                                                </option>
                                                <option value="laceration" <?php
                                                if ($fisexam['ortop_maoa_rs_inj_nat'] === 'laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração
                                                </option>
                                                <option value="bleeding" <?php
                                                if ($fisexam['ortop_maoa_rs_inj_nat'] === 'bleeding'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Sangramento
                                                </option>
                                                <option value="mot_change" <?php
                                                if ($fisexam['ortop_maoa_rs_inj_nat'] === 'mot_change'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Alteração da motricidade
                                                </option>
                                                <option value="infusion_change" <?php
                                                if ($fisexam['ortop_puna_ls_inj_nat'] === 'infusion_change'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Alteração da perfusão
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-1"></div>

                                <div class="col-sm-5 well">
                                    <h4 class="text-center"> Lado esquerdo (extremidades superiores)</h4>
                                    <div class="row-fluid">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" id="normal_ortop_evaluation_ls"
                                                <?php
                                                if ($fisexam['normal_ortop_evaluation_ls'] === 1): echo 'checked';
                                                endif;
                                                ?>>
                                                Avaliação ortopédica do lado esquerdo sem alterações (normal)
                                            </label>
                                        </div>
                                    </div>
                                    <h4> Clavícula </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="ortop_clava_ls" name="ortop_clav_ls" <?php
                                            if ($fisexam['ortop_clava_ls_inj_nat'] !== 'none' && isset($fisexam['ortop_clava_ls_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#ortop_clava_ls_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="ortop_clavn_ls" name="ortop_clav_ls" <?php
                                            if ($fisexam['ortop_clava_ls_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?>onclick="jQuery('#ortop_clava_ls_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="ortop_clava_ls_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="ortop_clava_ls_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="contus" <?php
                                                if ($fisexam['ortop_clava_ls_inj_nat'] === 'contus'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Contusão
                                                </option>
                                                <option value="hem" <?php
                                                if ($fisexam['ortop_clava_ls_inj_nat'] === 'hem'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Hematoma
                                                </option>
                                                <option value="volume_plus" <?php
                                                if ($fisexam['ortop_clava_ls_inj_nat'] === 'volume_plus'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Aumento de volume de partes moles
                                                </option>
                                                <option value="pain_moviment" <?php
                                                if ($fisexam['ortop_clava_ls_inj_nat'] === 'pain_moviment'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Dor à movimentação
                                                </option>
                                                <option value="crackling_bone" <?php
                                                if ($fisexam['ortop_clava_ls_inj_nat'] === 'crackling_bone'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Crepitação óssea
                                                </option>
                                                <option value="anormal_mob" <?php
                                                if ($fisexam['ortop_clava_ls_inj_nat'] === 'anormal_mob'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Mobilidade anormal
                                                </option>
                                                <option value="functional_imp" <?php
                                                if ($fisexam['ortop_clava_ls_inj_nat'] === 'functional_imp'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Impotência funcional
                                                </option>
                                                <option value="def" <?php
                                                if ($fisexam['ortop_clava_ls_inj_nat'] === 'def'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Deformidade
                                                </option>
                                                <option value="rotation" <?php
                                                if ($fisexam['ortop_clava_ls_inj_nat'] === 'rotation'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Rotação
                                                </option>
                                                <option value="laceration" <?php
                                                if ($fisexam['ortop_clava_ls_inj_nat'] === 'laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração
                                                </option>
                                                <option value="bleeding" <?php
                                                if ($fisexam['ortop_clava_ls_inj_nat'] === 'bleeding'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Sangramento
                                                </option>
                                                <option value="mot_change" <?php
                                                if ($fisexam['ortop_clava_ls_inj_nat'] === 'mot_change'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Alteração da motricidade
                                                </option>
                                                <option value="infusion_change" <?php
                                                if ($fisexam['ortop_clava_ls_inj_nat'] === 'infusion_change'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Alteração da perfusão
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h4> Ombro </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="ortop_omba_ls" name="ortop_omb_ls" <?php
                                            if ($fisexam['ortop_omba_ls_inj_nat'] && isset($fisexam['ortop_omba_ls_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#ortop_omba_ls_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="ortop_ombn_ls" name="ortop_omb_ls" <?php
                                            if ($fisexam['ortop_omba_ls_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#ortop_omba_ls_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="ortop_omba_ls_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="ortop_omba_ls_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="contus" <?php
                                                if ($fisexam['ortop_omba_ls_inj_nat'] === 'ortop_omba_ls_inj_nat'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Contusão
                                                </option>
                                                <option value="hem" <?php
                                                if ($fisexam['ortop_omba_ls_inj_nat'] === 'hem'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Hematoma
                                                </option>
                                                <option value="volume_plus" <?php
                                                if ($fisexam['ortop_omba_ls_inj_nat'] === 'volume_plus'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Aumento de volume de partes moles
                                                </option>
                                                <option value="pain_moviment" <?php
                                                if ($fisexam['ortop_omba_ls_inj_nat'] === 'pain_moviment'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Dor à movimentação
                                                </option>
                                                <option value="crackling_bone" <?php
                                                if ($fisexam['ortop_omba_ls_inj_nat'] === 'crackling_bone'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Crepitação óssea
                                                </option>
                                                <option value="anormal_mob" <?php
                                                if ($fisexam['ortop_omba_ls_inj_nat'] === 'anormal_mob'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Mobilidade anormal
                                                </option>
                                                <option value="functional_imp" <?php
                                                if ($fisexam['ortop_omba_ls_inj_nat'] === 'functional_imp'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Impotência funcional
                                                </option>
                                                <option value="def" <?php
                                                if ($fisexam['ortop_omba_ls_inj_nat'] === 'def'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Deformidade
                                                </option>
                                                <option value="rotation" <?php
                                                if ($fisexam['ortop_omba_ls_inj_nat'] === 'rotation'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Rotação
                                                </option>
                                                <option value="laceration" <?php
                                                if ($fisexam['ortop_omba_ls_inj_nat'] === 'laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração
                                                </option>
                                                <option value="bleeding" <?php
                                                if ($fisexam['ortop_omba_ls_inj_nat'] === 'bleeding'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Sangramento
                                                </option>
                                                <option value="mot_change" <?php
                                                if ($fisexam['ortop_omba_ls_inj_nat'] === 'mot_change'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Alteração da motricidade
                                                </option>
                                                <option value="infusion_change" <?php
                                                if ($fisexam['ortop_omba_ls_inj_nat'] === 'infusion_change'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Alteração da perfusão
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h4> Braço </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="ortop_brca_ls" name="ortop_brac_ls" <?php
                                            if ($fisexam['ortop_brca_ls_inj_nat'] && isset($fisexam['ortop_brca_ls_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#ortop_brca_ls_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="ortop_brcn_ls" name="ortop_brac_ls" <?php
                                            if ($fisexam['ortop_brca_ls_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#ortop_brca_ls_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="ortop_brca_ls_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="ortop_brca_ls_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="contus" <?php
                                                if ($fisexam['ortop_brca_ls_inj_nat'] === 'contus'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Contusão
                                                </option>
                                                <option value="hem" <?php
                                                if ($fisexam['ortop_brca_ls_inj_nat'] === 'hem'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Hematoma
                                                </option>
                                                <option value="volume_plus" <?php
                                                if ($fisexam['ortop_brca_ls_inj_nat'] === 'volume_plus'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Aumento de volume de partes moles
                                                </option>
                                                <option value="pain_moviment" <?php
                                                if ($fisexam['ortop_brca_ls_inj_nat'] === 'pain_moviment'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Dor à movimentação
                                                </option>
                                                <option value="crackling_bone" <?php
                                                if ($fisexam['ortop_brca_ls_inj_nat'] === 'crackling_bone'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Crepitação óssea
                                                </option>
                                                <option value="anormal_mob" <?php
                                                if ($fisexam['ortop_brca_ls_inj_nat'] === 'anormal_mob'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Mobilidade anormal
                                                </option>
                                                <option value="functional_imp" <?php
                                                if ($fisexam['ortop_brca_ls_inj_nat'] === 'functional_imp'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Impotência funcional
                                                </option>
                                                <option value="def" <?php
                                                if ($fisexam['ortop_brca_ls_inj_nat'] === 'def'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Deformidade
                                                </option>
                                                <option value="rotation" <?php
                                                if ($fisexam['ortop_brca_ls_inj_nat'] === 'rotation'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Rotação
                                                </option>
                                                <option value="laceration" <?php
                                                if ($fisexam['ortop_brca_ls_inj_nat'] === 'laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração
                                                </option>
                                                <option value="bleeding" <?php
                                                if ($fisexam['ortop_brca_ls_inj_nat'] === 'bleeding'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Sangramento
                                                </option>
                                                <option value="mot_change" <?php
                                                if ($fisexam['ortop_brca_ls_inj_nat'] === 'mot_change'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Alteração da motricidade
                                                </option>
                                                <option value="infusion_change" <?php
                                                if ($fisexam['ortop_brca_ls_inj_nat'] === 'infusion_change'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Alteração da perfusão
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h4> Cotovelo </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="ortop_cota_ls" name="ortop_cot_ls" <?php
                                            if ($fisexam['ortop_cota_ls_inj_nat'] && isset($fisexam['ortop_cota_ls_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#ortop_cota_ls_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="ortop_cotn_ls" name="ortop_cot_ls" <?php
                                            if ($fisexam['ortop_cota_ls_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#ortop_cota_ls_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="ortop_cota_ls_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="ortop_cota_ls_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="contus" <?php
                                                if ($fisexam['ortop_cota_ls_inj_nat'] === 'contus'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Contusão
                                                </option>
                                                <option value="hem" <?php
                                                if ($fisexam['ortop_cota_ls_inj_nat'] === 'hem'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Hematoma
                                                </option>
                                                <option value="volume_plus" <?php
                                                if ($fisexam['ortop_cota_ls_inj_nat'] === 'volume_plus'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Aumento de volume de partes moles
                                                </option>
                                                <option value="pain_moviment" <?php
                                                if ($fisexam['ortop_cota_ls_inj_nat'] === 'pain_moviment'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Dor à movimentação
                                                </option>
                                                <option value="crackling_bone" <?php
                                                if ($fisexam['ortop_cota_ls_inj_nat'] === 'crackling_bone'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Crepitação óssea
                                                </option>
                                                <option value="anormal_mob" <?php
                                                if ($fisexam['ortop_cota_ls_inj_nat'] === 'anormal_mob'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Mobilidade anormal
                                                </option>
                                                <option value="functional_imp" <?php
                                                if ($fisexam['ortop_cota_ls_inj_nat'] === 'functional_imp'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Impotência funcional
                                                </option>
                                                <option value="def" <?php
                                                if ($fisexam['ortop_cota_ls_inj_nat'] === 'def'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Deformidade
                                                </option>
                                                <option value="rotation" <?php
                                                if ($fisexam['ortop_cota_ls_inj_nat'] === 'rotation'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Rotação
                                                </option>
                                                <option value="laceration" <?php
                                                if ($fisexam['ortop_cota_ls_inj_nat'] === 'laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração
                                                </option>
                                                <option value="bleeding" <?php
                                                if ($fisexam['ortop_cota_ls_inj_nat'] === 'bleeding'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Sangramento
                                                </option>
                                                <option value="mot_change" <?php
                                                if ($fisexam['ortop_cota_ls_inj_nat'] === 'mot_change'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Alteração da motricidade
                                                </option>
                                                <option value="infusion_change" <?php
                                                if ($fisexam['ortop_cota_ls_inj_nat'] === 'infusion_change'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Alteração da perfusão
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h4> Antebraço </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="ortop_anta_ls" name="ortop_ant_ls" <?php
                                            if ($fisexam['ortop_anta_ls_inj_nat'] && isset($fisexam['ortop_anta_ls_inj_nat'])): echo 'checked';
                                            endif;
                                            ?>  onclick="jQuery('#ortop_anta_ls_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="ortop_antn_ls" name="ortop_ant_ls" <?php
                                            if ($fisexam['ortop_anta_ls_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?>  onclick="jQuery('#ortop_anta_ls_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="ortop_anta_ls_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="ortop_anta_ls_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="contus" <?php
                                                if ($fisexam['ortop_anta_ls_inj_nat'] === 'contus'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Contusão
                                                </option>
                                                <option value="hem" <?php
                                                if ($fisexam['ortop_anta_ls_inj_nat'] === 'hem'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Hematoma
                                                </option>
                                                <option value="volume_plus" <?php
                                                if ($fisexam['ortop_anta_ls_inj_nat'] === 'volume_plus'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Aumento de volume de partes moles
                                                </option>
                                                <option value="pain_moviment" <?php
                                                if ($fisexam['ortop_anta_ls_inj_nat'] === 'pain_moviment'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Dor à movimentação
                                                </option>
                                                <option value="crackling_bone" <?php
                                                if ($fisexam['ortop_anta_ls_inj_nat'] === 'crackling_bone'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Crepitação óssea
                                                </option>
                                                <option value="anormal_mob" <?php
                                                if ($fisexam['ortop_anta_ls_inj_nat'] === 'anormal_mob'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Mobilidade anormal
                                                </option>
                                                <option value="functional_imp" <?php
                                                if ($fisexam['ortop_anta_ls_inj_nat'] === 'functional_imp'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Impotência funcional
                                                </option>
                                                <option value="def" <?php
                                                if ($fisexam['ortop_anta_ls_inj_nat'] === 'def'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Deformidade
                                                </option>
                                                <option value="rotation" <?php
                                                if ($fisexam['ortop_anta_ls_inj_nat'] === 'rotation'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Rotação
                                                </option>
                                                <option value="laceration" <?php
                                                if ($fisexam['ortop_anta_ls_inj_nat'] === 'laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração
                                                </option>
                                                <option value="bleeding" <?php
                                                if ($fisexam['ortop_anta_ls_inj_nat'] === 'bleeding'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Sangramento
                                                </option>
                                                <option value="mot_change" <?php
                                                if ($fisexam['ortop_anta_ls_inj_nat'] === 'mot_change'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Alteração da motricidade
                                                </option>
                                                <option value="infusion_change" <?php
                                                if ($fisexam['ortop_anta_ls_inj_nat'] === 'infusion_change'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Alteração da perfusão
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h4> Punho </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="ortop_puna_ls" name="ortop_pun_ls" <?php
                                            if ($fisexam['ortop_puna_ls_inj_nat'] && isset($fisexam['ortop_puna_ls_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#ortop_puna_ls_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="ortop_punn_ls" name="ortop_pun_ls" <?php
                                            if ($fisexam['ortop_puna_ls_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#ortop_puna_ls_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="ortop_puna_ls_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="ortop_puna_ls_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="contus" <?php
                                                if ($fisexam['ortop_puna_ls_inj_nat'] === 'contus'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Contusão
                                                </option>
                                                <option value="hem" <?php
                                                if ($fisexam['ortop_puna_ls_inj_nat'] === 'hem'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Hematoma
                                                </option>
                                                <option value="volume_plus" <?php
                                                if ($fisexam['ortop_puna_ls_inj_nat'] === 'volume_plus'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Aumento de volume de partes moles
                                                </option>
                                                <option value="pain_moviment" <?php
                                                if ($fisexam['ortop_puna_ls_inj_nat'] === 'pain_moviment'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Dor à movimentação
                                                </option>
                                                <option value="crackling_bone" <?php
                                                if ($fisexam['ortop_puna_ls_inj_nat'] === 'crackling_bone'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Crepitação óssea
                                                </option>
                                                <option value="anormal_mob" <?php
                                                if ($fisexam['ortop_puna_ls_inj_nat'] === 'anormal_mob'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Mobilidade anormal
                                                </option>
                                                <option value="functional_imp" <?php
                                                if ($fisexam['ortop_puna_ls_inj_nat'] === 'functional_imp'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Impotência funcional
                                                </option>
                                                <option value="def" <?php
                                                if ($fisexam['ortop_puna_ls_inj_nat'] === 'def'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Deformidade
                                                </option>
                                                <option value="rotation" <?php
                                                if ($fisexam['ortop_puna_ls_inj_nat'] === 'rotation'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Rotação
                                                </option>
                                                <option value="laceration" <?php
                                                if ($fisexam['ortop_puna_ls_inj_nat'] === 'laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração
                                                </option>
                                                <option value="bleeding" <?php
                                                if ($fisexam['ortop_puna_ls_inj_nat'] === 'bleeding'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Sangramento
                                                </option>
                                                <option value="mot_change" <?php
                                                if ($fisexam['ortop_puna_ls_inj_nat'] === 'mot_change'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Alteração da motricidade
                                                </option>
                                                <option value="infusion_change" <?php
                                                if ($fisexam['ortop_puna_ls_inj_nat'] === 'infusion_change'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Alteração da perfusão
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h4> Mão </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="ortop_maoa_ls" name="ortop_mao_ls" <?php
                                            if ($fisexam['ortop_maoa_ls_inj_nat'] && isset($fisexam['ortop_maoa_ls_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#ortop_maoa_ls_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="ortop_maon_ls" name="ortop_mao_ls" <?php
                                            if ($fisexam['ortop_maoa_ls_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#ortop_maoa_ls_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="ortop_maoa_ls_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="ortop_maoa_ls_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="contus" <?php
                                                if ($fisexam['ortop_maoa_ls_inj_nat'] === 'contus'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Contusão
                                                </option>
                                                <option value="hem" <?php
                                                if ($fisexam['ortop_maoa_ls_inj_nat'] === 'hem'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Hematoma
                                                </option>
                                                <option value="volume_plus" <?php
                                                if ($fisexam['ortop_maoa_ls_inj_nat'] === 'volume_plus'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Aumento de volume de partes moles
                                                </option>
                                                <option value="pain_moviment" <?php
                                                if ($fisexam['ortop_maoa_ls_inj_nat'] === 'pain_moviment'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Dor à movimentação
                                                </option>
                                                <option value="crackling_bone" <?php
                                                if ($fisexam['ortop_maoa_ls_inj_nat'] === 'crackling_bone'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Crepitação óssea
                                                </option>
                                                <option value="anormal_mob" <?php
                                                if ($fisexam['ortop_maoa_ls_inj_nat'] === 'anormal_mob'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Mobilidade anormal
                                                </option>
                                                <option value="functional_imp" <?php
                                                if ($fisexam['ortop_maoa_ls_inj_nat'] === 'functional_imp'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Impotência funcional
                                                </option>
                                                <option value="def" <?php
                                                if ($fisexam['ortop_maoa_ls_inj_nat'] === 'def'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Deformidade
                                                </option>
                                                <option value="rotation" <?php
                                                if ($fisexam['ortop_maoa_ls_inj_nat'] === 'rotation'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Rotação
                                                </option>
                                                <option value="laceration" <?php
                                                if ($fisexam['ortop_maoa_ls_inj_nat'] === 'laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração
                                                </option>
                                                <option value="bleeding" <?php
                                                if ($fisexam['ortop_maoa_ls_inj_nat'] === 'bleeding'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Sangramento
                                                </option>
                                                <option value="mot_change" <?php
                                                if ($fisexam['ortop_maoa_ls_inj_nat'] === 'mot_change'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Alteração da motricidade
                                                </option>
                                                <option value="infusion_change" <?php
                                                if ($fisexam['ortop_maoa_ls_inj_nat'] === 'infusion_change'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Alteração da perfusão
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row-fluid">
                                <div class="col-sm-5 well">
                                    <h4 class="text-center"> Lado direito (extremidades inferiores)</h4>
                                    <div class="row-fluid">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" id="normal_ortop_evaluation_ri"
                                                <?php
                                                if ($fisexam['normal_ortop_evaluation_ri'] === 1): echo 'checked';
                                                endif;
                                                ?>>
                                                Avaliação ortopédica do lado direito sem alterações (normal)
                                            </label>
                                        </div>
                                    </div>
                                    <h4> Quadril </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="ortop_quada_ri" name="ortop_quad_ri" <?php
                                            if ($fisexam['ortop_quada_ri_inj_nat'] && isset($fisexam['ortop_quada_ri_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#ortop_quada_ri_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="ortop_quadn_ri" name="ortop_quad_ri" <?php
                                            if ($fisexam['ortop_quada_ri_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#ortop_quada_ri_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="ortop_quada_ri_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium  mb-md" id="ortop_quada_ri_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="contus" <?php
                                                if ($fisexam['ortop_quada_ri_inj_nat'] === 'contus'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Contusão
                                                </option>
                                                <option value="hem" <?php
                                                if ($fisexam['ortop_quada_ri_inj_nat'] === 'hem'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Hematoma
                                                </option>
                                                <option value="volume_plus" <?php
                                                if ($fisexam['ortop_quada_ri_inj_nat'] === 'volume_plus'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Aumento de volume de partes moles
                                                </option>
                                                <option value="pain_moviment" <?php
                                                if ($fisexam['ortop_quada_ri_inj_nat'] === 'pain_moviment'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Dor à movimentação
                                                </option>
                                                <option value="crackling_bone" <?php
                                                if ($fisexam['ortop_quada_ri_inj_nat'] === 'crackling_bone'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Crepitação óssea
                                                </option>
                                                <option value="anormal_mob" <?php
                                                if ($fisexam['ortop_quada_ri_inj_nat'] === 'anormal_mob'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Mobilidade anormal
                                                </option>
                                                <option value="functional_imp" <?php
                                                if ($fisexam['ortop_quada_ri_inj_nat'] === 'functional_imp'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Impotência funcional
                                                </option>
                                                <option value="def" <?php
                                                if ($fisexam['ortop_quada_ri_inj_nat'] === 'def'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Deformidade
                                                </option>
                                                <option value="rotation" <?php
                                                if ($fisexam['ortop_quada_ri_inj_nat'] === 'rotation'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Rotação
                                                </option>
                                                <option value="laceration" <?php
                                                if ($fisexam['ortop_quada_ri_inj_nat'] === 'laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração
                                                </option>
                                                <option value="bleeding" <?php
                                                if ($fisexam['ortop_quada_ri_inj_nat'] === 'bleeding'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Sangramento
                                                </option>
                                                <option value="mot_change" <?php
                                                if ($fisexam['ortop_quada_ri_inj_nat'] === 'mot_change'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Alteração da motricidade
                                                </option>
                                                <option value="infusion_change" <?php
                                                if ($fisexam['ortop_quada_ri_inj_nat'] === 'infusion_change'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Alteração da perfusão
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h4> Coxa </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="ortop_coxa_ri" name="ortop_cox_ri" <?php
                                            if ($fisexam['ortop_coxa_ri_inj_nat'] && isset($fisexam['ortop_coxa_ri_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#ortop_coxa_ri_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="ortop_coxn_ri" name="ortop_cox_ri" <?php
                                            if ($fisexam['ortop_coxa_ri_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#ortop_coxa_ri_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="ortop_coxa_ri_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium  mb-md" id="ortop_coxa_ri_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="contus" <?php
                                                if ($fisexam['ortop_coxa_ri_inj_nat'] === 'contus'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Contusão
                                                </option>
                                                <option value="hem" <?php
                                                if ($fisexam['ortop_coxa_ri_inj_nat'] === 'hem'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Hematoma
                                                </option>
                                                <option value="volume_plus" <?php
                                                if ($fisexam['ortop_coxa_ri_inj_nat'] === 'volume_plus'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Aumento de volume de partes moles
                                                </option>
                                                <option value="pain_moviment" <?php
                                                if ($fisexam['ortop_coxa_ri_inj_nat'] === 'pain_moviment'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Dor à movimentação
                                                </option>
                                                <option value="crackling_bone" <?php
                                                if ($fisexam['ortop_coxa_ri_inj_nat'] === 'crackling_bone'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Crepitação óssea
                                                </option>
                                                <option value="anormal_mob" <?php
                                                if ($fisexam['ortop_coxa_ri_inj_nat'] === 'anormal_mob'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Mobilidade anormal
                                                </option>
                                                <option value="functional_imp" <?php
                                                if ($fisexam['ortop_coxa_ri_inj_nat'] === 'functional_imp'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Impotência funcional
                                                </option>
                                                <option value="def" <?php
                                                if ($fisexam['ortop_coxa_ri_inj_nat'] === 'def'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Deformidade
                                                </option>
                                                <option value="rotation" <?php
                                                if ($fisexam['ortop_coxa_ri_inj_nat'] === 'rotation'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Rotação
                                                </option>
                                                <option value="laceration" <?php
                                                if ($fisexam['ortop_coxa_ri_inj_nat'] === 'laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração
                                                </option>
                                                <option value="bleeding" <?php
                                                if ($fisexam['ortop_coxa_ri_inj_nat'] === 'bleeding'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Sangramento
                                                </option>
                                                <option value="mot_change" <?php
                                                if ($fisexam['ortop_coxa_ri_inj_nat'] === 'mot_change'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Alteração da motricidade
                                                </option>
                                                <option value="infusion_change" <?php
                                                if ($fisexam['ortop_coxa_ri_inj_nat'] === 'infusion_change'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Alteração da perfusão
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h4> Joelho </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="ortop_joea_ri" name="ortop_joe_ri" <?php
                                            if ($fisexam['ortop_joea_ri_inj_nat'] && isset($fisexam['ortop_joea_ri_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#ortop_joea_ri_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="ortop_joen_ri" name="ortop_joe_ri" <?php
                                            if ($fisexam['ortop_joea_ri_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#ortop_joea_ri_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="ortop_joea_ri_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium  mb-md" id="ortop_joea_ri_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="contus" <?php
                                                if ($fisexam['ortop_joea_ri_inj_nat'] === 'contus'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Contusão
                                                </option>
                                                <option value="hem" <?php
                                                if ($fisexam['ortop_joea_ri_inj_nat'] === 'hem'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Hematoma
                                                </option>
                                                <option value="volume_plus" <?php
                                                if ($fisexam['ortop_joea_ri_inj_nat'] === 'volume_plus'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Aumento de volume de partes moles
                                                </option>
                                                <option value="pain_moviment" <?php
                                                if ($fisexam['ortop_joea_ri_inj_nat'] === 'pain_moviment'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Dor à movimentação
                                                </option>
                                                <option value="crackling_bone" <?php
                                                if ($fisexam['ortop_joea_ri_inj_nat'] === 'crackling_bone'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Crepitação óssea
                                                </option>
                                                <option value="anormal_mob" <?php
                                                if ($fisexam['ortop_joea_ri_inj_nat'] === 'anormal_mob'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Mobilidade anormal
                                                </option>
                                                <option value="functional_imp" <?php
                                                if ($fisexam['ortop_joea_ri_inj_nat'] === 'functional_imp'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Impotência funcional
                                                </option>
                                                <option value="def" <?php
                                                if ($fisexam['ortop_joea_ri_inj_nat'] === 'def'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Deformidade
                                                </option>
                                                <option value="rotation" <?php
                                                if ($fisexam['ortop_joea_ri_inj_nat'] === 'rotation'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Rotação
                                                </option>
                                                <option value="laceration" <?php
                                                if ($fisexam['ortop_joea_ri_inj_nat'] === 'laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração
                                                </option>
                                                <option value="bleeding" <?php
                                                if ($fisexam['ortop_joea_ri_inj_nat'] === 'bleeding'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Sangramento
                                                </option>
                                                <option value="mot_change" <?php
                                                if ($fisexam['ortop_joea_ri_inj_nat'] === 'mot_change'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Alteração da motricidade
                                                </option>
                                                <option value="infusion_change" <?php
                                                if ($fisexam['ortop_joea_ri_inj_nat'] === 'infusion_change'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Alteração da perfusão
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h4> Perna </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="ortop_pera_ri" name="ortop_per_ri" <?php
                                            if ($fisexam['ortop_pera_ri_inj_nat'] && isset($fisexam['ortop_pera_ri_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#ortop_pera_ri_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="ortop_pern_ri" name="ortop_per_ri" <?php
                                            if ($fisexam['ortop_pera_ri_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#ortop_pera_ri_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="ortop_pera_ri_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium  mb-md" id="ortop_pera_ri_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="contus" <?php
                                                if ($fisexam['ortop_pera_ri_inj_nat'] === 'contus'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Contusão
                                                </option>
                                                <option value="hem" <?php
                                                if ($fisexam['ortop_pera_ri_inj_nat'] === 'hem'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Hematoma
                                                </option>
                                                <option value="volume_plus" <?php
                                                if ($fisexam['ortop_pera_ri_inj_nat'] === 'volume_plus'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Aumento de volume de partes moles
                                                </option>
                                                <option value="pain_moviment" <?php
                                                if ($fisexam['ortop_pera_ri_inj_nat'] === 'pain_moviment'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Dor à movimentação
                                                </option>
                                                <option value="crackling_bone" <?php
                                                if ($fisexam['ortop_pera_ri_inj_nat'] === 'crackling_bone'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Crepitação óssea
                                                </option>
                                                <option value="anormal_mob" <?php
                                                if ($fisexam['ortop_pera_ri_inj_nat'] === 'anormal_mob'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Mobilidade anormal
                                                </option>
                                                <option value="functional_imp" <?php
                                                if ($fisexam['ortop_pera_ri_inj_nat'] === 'functional_imp'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Impotência funcional
                                                </option>
                                                <option value="def" <?php
                                                if ($fisexam['ortop_pera_ri_inj_nat'] === 'def'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Deformidade
                                                </option>
                                                <option value="rotation" <?php
                                                if ($fisexam['ortop_pera_ri_inj_nat'] === 'rotation'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Rotação
                                                </option>
                                                <option value="laceration" <?php
                                                if ($fisexam['ortop_pera_ri_inj_nat'] === 'laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração
                                                </option>
                                                <option value="bleeding" <?php
                                                if ($fisexam['ortop_pera_ri_inj_nat'] === 'bleeding'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Sangramento
                                                </option>
                                                <option value="mot_change" <?php
                                                if ($fisexam['ortop_pera_ri_inj_nat'] === 'mot_change'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Alteração da motricidade
                                                </option>
                                                <option value="infusion_change" <?php
                                                if ($fisexam['ortop_pera_ri_inj_nat'] === 'infusion_change'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Alteração da perfusão
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h4> Tornozelo </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="ortop_tora_ri" name="ortop_tor_ri" <?php
                                            if ($fisexam['ortop_tora_ri_inj_nat'] && isset($fisexam['ortop_tora_ri_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#ortop_tora_ri_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="ortop_torn_ri" name="ortop_tor_ri"  <?php
                                            if ($fisexam['ortop_tora_ri_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#ortop_tora_ri_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="ortop_tora_ri_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium  mb-md" id="ortop_tora_ri_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="contus" <?php
                                                if ($fisexam['ortop_tora_ri_inj_nat'] === 'contus'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Contusão
                                                </option>
                                                <option value="hem" <?php
                                                if ($fisexam['ortop_tora_ri_inj_nat'] === 'hem'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Hematoma
                                                </option>
                                                <option value="volume_plus" <?php
                                                if ($fisexam['ortop_tora_ri_inj_nat'] === 'volume_plus'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Aumento de volume de partes moles
                                                </option>
                                                <option value="pain_moviment" <?php
                                                if ($fisexam['ortop_tora_ri_inj_nat'] === 'pain_moviment'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Dor à movimentação
                                                </option>
                                                <option value="crackling_bone" <?php
                                                if ($fisexam['ortop_tora_ri_inj_nat'] === 'crackling_bone'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Crepitação óssea
                                                </option>
                                                <option value="anormal_mob" <?php
                                                if ($fisexam['ortop_tora_ri_inj_nat'] === 'anormal_mob'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Mobilidade anormal
                                                </option>
                                                <option value="functional_imp" <?php
                                                if ($fisexam['ortop_tora_ri_inj_nat'] === 'functional_imp'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Impotência funcional
                                                </option>
                                                <option value="def" <?php
                                                if ($fisexam['ortop_tora_ri_inj_nat'] === 'def'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Deformidade
                                                </option>
                                                <option value="rotation" <?php
                                                if ($fisexam['ortop_tora_ri_inj_nat'] === 'rotation'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Rotação
                                                </option>
                                                <option value="laceration" <?php
                                                if ($fisexam['ortop_tora_ri_inj_nat'] === 'laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração
                                                </option>
                                                <option value="bleeding" <?php
                                                if ($fisexam['ortop_tora_ri_inj_nat'] === 'bleeding'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Sangramento
                                                </option>
                                                <option value="mot_change" <?php
                                                if ($fisexam['ortop_tora_ri_inj_nat'] === 'mot_change'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Alteração da motricidade
                                                </option>
                                                <option value="infusion_change" <?php
                                                if ($fisexam['ortop_tora_ri_inj_nat'] === 'infusion_change'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Alteração da perfusão
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h4> Pé </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="ortop_pea_ri" name="ortop_pe_ri" <?php
                                            if ($fisexam['ortop_pea_ri_inj_nat'] && isset($fisexam['ortop_pea_ri_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#ortop_pea_ri_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="ortop_pen_ri" name="ortop_pe_ri" <?php
                                            if ($fisexam['ortop_pea_ri_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#ortop_pea_ri_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="ortop_pea_ri_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium  mb-md" id="ortop_pea_ri_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="contus" <?php
                                                if ($fisexam['ortop_pea_ri_inj_nat'] === 'contus'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Contusão
                                                </option>
                                                <option value="hem" <?php
                                                if ($fisexam['ortop_pea_ri_inj_nat'] === 'hem'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Hematoma
                                                </option>
                                                <option value="volume_plus" <?php
                                                if ($fisexam['ortop_pea_ri_inj_nat'] === 'volume_plus'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Aumento de volume de partes moles
                                                </option>
                                                <option value="pain_moviment" <?php
                                                if ($fisexam['ortop_pea_ri_inj_nat'] === 'pain_moviment'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Dor à movimentação
                                                </option>
                                                <option value="crackling_bone" <?php
                                                if ($fisexam['ortop_pea_ri_inj_nat'] === 'crackling_bone'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Crepitação óssea
                                                </option>
                                                <option value="anormal_mob" <?php
                                                if ($fisexam['ortop_pea_ri_inj_nat'] === 'anormal_mob'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Mobilidade anormal
                                                </option>
                                                <option value="functional_imp" <?php
                                                if ($fisexam['ortop_pea_ri_inj_nat'] === 'functional_imp'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Impotência funcional
                                                </option>
                                                <option value="def" <?php
                                                if ($fisexam['ortop_pea_ri_inj_nat'] === 'def'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Deformidade
                                                </option>
                                                <option value="rotation" <?php
                                                if ($fisexam['ortop_pea_ri_inj_nat'] === 'rotation'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Rotação
                                                </option>
                                                <option value="laceration" <?php
                                                if ($fisexam['ortop_pea_ri_inj_nat'] === 'laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração
                                                </option>
                                                <option value="bleeding" <?php
                                                if ($fisexam['ortop_pea_ri_inj_nat'] === 'bleeding'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Sangramento
                                                </option>
                                                <option value="mot_change" <?php
                                                if ($fisexam['ortop_pea_ri_inj_nat'] === 'mot_change'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Alteração da motricidade
                                                </option>
                                                <option value="infusion_change" <?php
                                                if ($fisexam['ortop_pea_ri_inj_nat'] === 'infusion_change'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Alteração da perfusão
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-1"></div>

                                <div class="col-sm-5 well">
                                    <h4 class="text-center"> Lado esquerdo (extremidades inferiores)</h4>
                                    <div class="row-fluid">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" id="normal_ortop_evaluation_li"
                                                <?php
                                                if ($fisexam['normal_ortop_evaluation_li'] === 1): echo 'checked';
                                                endif;
                                                ?>>
                                                Avaliação ortopédica do lado esquerdo sem alterações (normal)
                                            </label>
                                        </div>
                                    </div>
                                    <h4> Quadril </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="ortop_quada_li" name="ortop_quad_li" <?php
                                            if ($fisexam['ortop_quada_li_inj_nat'] && isset($fisexam['ortop_quada_li_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#ortop_quada_li_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="ortop_quadn_li" name="ortop_quad_li" <?php
                                            if ($fisexam['ortop_quada_li_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#ortop_quada_li_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="ortop_quada_li_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium  mb-md" id="ortop_quada_li_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="contus" <?php
                                                if ($fisexam['ortop_quada_li_inj_nat'] === 'contus'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Contusão
                                                </option>
                                                <option value="hem" <?php
                                                if ($fisexam['ortop_quada_li_inj_nat'] === 'hem'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Hematoma
                                                </option>
                                                <option value="volume_plus" <?php
                                                if ($fisexam['ortop_quada_li_inj_nat'] === 'volume_plus'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Aumento de volume de partes moles
                                                </option>
                                                <option value="pain_moviment" <?php
                                                if ($fisexam['ortop_quada_li_inj_nat'] === 'pain_moviment'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Dor à movimentação
                                                </option>
                                                <option value="crackling_bone" <?php
                                                if ($fisexam['ortop_quada_li_inj_nat'] === 'crackling_bone'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Crepitação óssea
                                                </option>
                                                <option value="anormal_mob" <?php
                                                if ($fisexam['ortop_quada_li_inj_nat'] === 'anormal_mob'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Mobilidade anormal
                                                </option>
                                                <option value="functional_imp" <?php
                                                if ($fisexam['ortop_quada_li_inj_nat'] === 'functional_imp'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Impotência funcional
                                                </option>
                                                <option value="def" <?php
                                                if ($fisexam['ortop_quada_li_inj_nat'] === 'def'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Deformidade
                                                </option>
                                                <option value="rotation" <?php
                                                if ($fisexam['ortop_quada_li_inj_nat'] === 'rotation'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Rotação
                                                </option>
                                                <option value="laceration" <?php
                                                if ($fisexam['ortop_quada_li_inj_nat'] === 'laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração
                                                </option>
                                                <option value="bleeding" <?php
                                                if ($fisexam['ortop_quada_li_inj_nat'] === 'bleeding'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Sangramento
                                                </option>
                                                <option value="mot_change" <?php
                                                if ($fisexam['ortop_quada_li_inj_nat'] === 'mot_change'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Alteração da motricidade
                                                </option>
                                                <option value="infusion_change" <?php
                                                if ($fisexam['ortop_quada_li_inj_nat'] === 'infusion_change'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Alteração da perfusão
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h4> Coxa </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="ortop_coxa_li" name="ortop_cox_li" <?php
                                            if ($fisexam['ortop_coxa_li_inj_nat'] && isset($fisexam['ortop_coxa_li_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#ortop_coxa_li_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="ortop_coxn_li" name="ortop_cox_li" <?php
                                            if ($fisexam['ortop_coxa_li_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#ortop_coxa_li_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="ortop_coxa_li_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium  mb-md" id="ortop_coxa_li_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="contus" <?php
                                                if ($fisexam['ortop_coxa_li_inj_nat'] === 'contus'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Contusão
                                                </option>
                                                <option value="hem" <?php
                                                if ($fisexam['ortop_coxa_li_inj_nat'] === 'hem'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Hematoma
                                                </option>
                                                <option value="volume_plus" <?php
                                                if ($fisexam['ortop_coxa_li_inj_nat'] === 'volume_plus'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Aumento de volume de partes moles
                                                </option>
                                                <option value="pain_moviment" <?php
                                                if ($fisexam['ortop_coxa_li_inj_nat'] === 'pain_moviment'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Dor à movimentação
                                                </option>
                                                <option value="crackling_bone" <?php
                                                if ($fisexam['ortop_coxa_li_inj_nat'] === 'crackling_bone'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Crepitação óssea
                                                </option>
                                                <option value="anormal_mob" <?php
                                                if ($fisexam['ortop_coxa_li_inj_nat'] === 'anormal_mob'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Mobilidade anormal
                                                </option>
                                                <option value="functional_imp" <?php
                                                if ($fisexam['ortop_coxa_li_inj_nat'] === 'functional_imp'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Impotência funcional
                                                </option>
                                                <option value="def" <?php
                                                if ($fisexam['ortop_coxa_li_inj_nat'] === 'def'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Deformidade
                                                </option>
                                                <option value="rotation" <?php
                                                if ($fisexam['ortop_coxa_li_inj_nat'] === 'rotation'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Rotação
                                                </option>
                                                <option value="laceration" <?php
                                                if ($fisexam['ortop_coxa_li_inj_nat'] === 'laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração
                                                </option>
                                                <option value="bleeding" <?php
                                                if ($fisexam['ortop_coxa_li_inj_nat'] === 'bleeding'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Sangramento
                                                </option>
                                                <option value="mot_change" <?php
                                                if ($fisexam['ortop_coxa_li_inj_nat'] === 'mot_change'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Alteração da motricidade
                                                </option>
                                                <option value="infusion_change" <?php
                                                if ($fisexam['ortop_coxa_li_inj_nat'] === 'infusion_change'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Alteração da perfusão
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h4> Joelho </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="ortop_joea_li" name="ortop_joe_li" <?php
                                            if ($fisexam['ortop_joea_li_inj_nat'] && isset($fisexam['ortop_joea_li_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#ortop_joea_li_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="ortop_joen_li" name="ortop_joe_li" <?php
                                            if ($fisexam['ortop_joea_li_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#ortop_joea_li_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="ortop_joea_li_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium  mb-md" id="ortop_joea_li_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="contus" <?php
                                                if ($fisexam['ortop_joea_li_inj_nat'] === 'contus'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Contusão
                                                </option>
                                                <option value="hem" <?php
                                                if ($fisexam['ortop_joea_li_inj_nat'] === 'hem'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Hematoma
                                                </option>
                                                <option value="volume_plus" <?php
                                                if ($fisexam['ortop_joea_li_inj_nat'] === 'volume_plus'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Aumento de volume de partes moles
                                                </option>
                                                <option value="pain_moviment" <?php
                                                if ($fisexam['ortop_joea_li_inj_nat'] === 'pain_moviment'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Dor à movimentação
                                                </option>
                                                <option value="crackling_bone" <?php
                                                if ($fisexam['ortop_joea_li_inj_nat'] === 'crackling_bone'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Crepitação óssea
                                                </option>
                                                <option value="anormal_mob" <?php
                                                if ($fisexam['ortop_joea_li_inj_nat'] === 'anormal_mob'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Mobilidade anormal
                                                </option>
                                                <option value="functional_imp" <?php
                                                if ($fisexam['ortop_joea_li_inj_nat'] === 'functional_imp'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Impotência funcional
                                                </option>
                                                <option value="def" <?php
                                                if ($fisexam['ortop_joea_li_inj_nat'] === 'def'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Deformidade
                                                </option>
                                                <option value="rotation" <?php
                                                if ($fisexam['ortop_joea_li_inj_nat'] === 'rotation'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Rotação
                                                </option>
                                                <option value="laceration" <?php
                                                if ($fisexam['ortop_joea_li_inj_nat'] === 'laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração
                                                </option>
                                                <option value="bleeding" <?php
                                                if ($fisexam['ortop_joea_li_inj_nat'] === 'bleeding'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Sangramento
                                                </option>
                                                <option value="mot_change" <?php
                                                if ($fisexam['ortop_joea_li_inj_nat'] === 'mot_change'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Alteração da motricidade
                                                </option>
                                                <option value="infusion_change" <?php
                                                if ($fisexam['ortop_joea_li_inj_nat'] === 'infusion_change'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Alteração da perfusão
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h4> Perna </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="ortop_pera_li" name="ortop_per_li" <?php
                                            if ($fisexam['ortop_pera_li_inj_nat'] && isset($fisexam['ortop_pera_li_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#ortop_pera_li_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="ortop_pern_li" name="ortop_per_li" <?php
                                            if ($fisexam['ortop_pera_li_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#ortop_pera_li_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="ortop_pera_li_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium  mb-md" id="ortop_pera_li_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="contus" <?php
                                                if ($fisexam['ortop_pera_li_inj_nat'] === 'contus'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Contusão
                                                </option>
                                                <option value="hem" <?php
                                                if ($fisexam['ortop_pera_li_inj_nat'] === 'hem'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Hematoma
                                                </option>
                                                <option value="volume_plus" <?php
                                                if ($fisexam['ortop_pera_li_inj_nat'] === 'volume_plus'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Aumento de volume de partes moles
                                                </option>
                                                <option value="pain_moviment" <?php
                                                if ($fisexam['ortop_pera_li_inj_nat'] === 'pain_moviment'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Dor à movimentação
                                                </option>
                                                <option value="crackling_bone" <?php
                                                if ($fisexam['ortop_pera_li_inj_nat'] === 'crackling_bone'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Crepitação óssea
                                                </option>
                                                <option value="anormal_mob" <?php
                                                if ($fisexam['ortop_pera_li_inj_nat'] === 'anormal_mob'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Mobilidade anormal
                                                </option>
                                                <option value="functional_imp" <?php
                                                if ($fisexam['ortop_pera_li_inj_nat'] === 'functional_imp'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Impotência funcional
                                                </option>
                                                <option value="def" <?php
                                                if ($fisexam['ortop_pera_li_inj_nat'] === 'def'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Deformidade
                                                </option>
                                                <option value="rotation" <?php
                                                if ($fisexam['ortop_pera_li_inj_nat'] === 'rotation'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Rotação
                                                </option>
                                                <option value="laceration" <?php
                                                if ($fisexam['ortop_pera_li_inj_nat'] === 'laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração
                                                </option>
                                                <option value="bleeding" <?php
                                                if ($fisexam['ortop_pera_li_inj_nat'] === 'bleeding'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Sangramento
                                                </option>
                                                <option value="mot_change" <?php
                                                if ($fisexam['ortop_pera_li_inj_nat'] === 'mot_change'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Alteração da motricidade
                                                </option>
                                                <option value="infusion_change" <?php
                                                if ($fisexam['ortop_pera_li_inj_nat'] === 'infusion_change'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Alteração da perfusão
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h4> Tornozelo </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="ortop_tora_li" name="ortop_tor_li" <?php
                                            if ($fisexam['ortop_tora_li_inj_nat'] && isset($fisexam['ortop_tora_li_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#ortop_tora_li_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="ortop_torn_li" name="ortop_tor_li" <?php
                                            if ($fisexam['ortop_tora_li_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#ortop_tora_li_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="ortop_tora_li_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium  mb-md" id="ortop_tora_li_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="contus" <?php
                                                if ($fisexam['ortop_tora_li_inj_nat'] === 'contus'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Contusão
                                                </option>
                                                <option value="hem" <?php
                                                if ($fisexam['ortop_tora_li_inj_nat'] === 'hem'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Hematoma
                                                </option>
                                                <option value="volume_plus" <?php
                                                if ($fisexam['ortop_tora_li_inj_nat'] === 'volume_plus'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Aumento de volume de partes moles
                                                </option>
                                                <option value="pain_moviment" <?php
                                                if ($fisexam['ortop_tora_li_inj_nat'] === 'pain_moviment'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Dor à movimentação
                                                </option>
                                                <option value="crackling_bone" <?php
                                                if ($fisexam['ortop_tora_li_inj_nat'] === 'crackling_bone'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Crepitação óssea
                                                </option>
                                                <option value="anormal_mob" <?php
                                                if ($fisexam['ortop_tora_li_inj_nat'] === 'anormal_mob'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Mobilidade anormal
                                                </option>
                                                <option value="functional_imp" <?php
                                                if ($fisexam['ortop_tora_li_inj_nat'] === 'functional_imp'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Impotência funcional
                                                </option>
                                                <option value="def" <?php
                                                if ($fisexam['ortop_tora_li_inj_nat'] === 'def'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Deformidade
                                                </option>
                                                <option value="rotation" <?php
                                                if ($fisexam['ortop_tora_li_inj_nat'] === 'rotation'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Rotação
                                                </option>
                                                <option value="laceration" <?php
                                                if ($fisexam['ortop_tora_li_inj_nat'] === 'laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração
                                                </option>
                                                <option value="bleeding" <?php
                                                if ($fisexam['ortop_tora_li_inj_nat'] === 'bleeding'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Sangramento
                                                </option>
                                                <option value="mot_change" <?php
                                                if ($fisexam['ortop_tora_li_inj_nat'] === 'mot_change'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Alteração da motricidade
                                                </option>
                                                <option value="infusion_change" <?php
                                                if ($fisexam['ortop_tora_li_inj_nat'] === 'infusion_change'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Alteração da perfusão
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h4> Pé </h4>
                                    <div class="row-fluid">
                                        <label class="radio-inline"><input type="radio" id="ortop_pea_li" name="ortop_pe_li" <?php
                                            if ($fisexam['ortop_pea_li_inj_nat'] && isset($fisexam['ortop_pea_li_inj_nat'])): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#ortop_pea_li_detail').show();">Exame alterado</label>
                                        <label class="radio-inline"><input type="radio" id="ortop_pen_li" name="ortop_pe_li" <?php
                                            if ($fisexam['ortop_pea_li_inj_nat'] === 'none'): echo 'checked';
                                            endif;
                                            ?> onclick="jQuery('#ortop_pea_li_detail').hide();">Exame normal</label>
                                    </div>
                                    <div id="ortop_pea_li_detail" class="row-fluid" style="display:none;">
                                        <div class="col-sm-8">
                                            <h5><label class="control-label">Natureza da lesão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium  mb-md" id="ortop_pea_li_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="contus" <?php
                                                if ($fisexam['ortop_pea_li_inj_nat'] === 'contus'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Contusão
                                                </option>
                                                <option value="hem" <?php
                                                if ($fisexam['ortop_pea_li_inj_nat'] === 'hem'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Hematoma
                                                </option>
                                                <option value="volume_plus" <?php
                                                if ($fisexam['ortop_pea_li_inj_nat'] === 'volume_plus'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Aumento de volume de partes moles
                                                </option>
                                                <option value="pain_moviment" <?php
                                                if ($fisexam['ortop_pea_li_inj_nat'] === 'pain_moviment'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Dor à movimentação
                                                </option>
                                                <option value="crackling_bone" <?php
                                                if ($fisexam['ortop_pea_li_inj_nat'] === 'crackling_bone'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Crepitação óssea
                                                </option>
                                                <option value="anormal_mob" <?php
                                                if ($fisexam['ortop_pea_li_inj_nat'] === 'anormal_mob'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Mobilidade anormal
                                                </option>
                                                <option value="functional_imp" <?php
                                                if ($fisexam['ortop_pea_li_inj_nat'] === 'functional_imp'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Impotência funcional
                                                </option>
                                                <option value="def" <?php
                                                if ($fisexam['ortop_pea_li_inj_nat'] === 'def'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Deformidade
                                                </option>
                                                <option value="rotation" <?php
                                                if ($fisexam['ortop_pea_li_inj_nat'] === 'rotation'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Rotação
                                                </option>
                                                <option value="laceration" <?php
                                                if ($fisexam['ortop_pea_li_inj_nat'] === 'laceration'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Laceração
                                                </option>
                                                <option value="bleeding" <?php
                                                if ($fisexam['ortop_pea_li_inj_nat'] === 'bleeding'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Sangramento
                                                </option>
                                                <option value="mot_change" <?php
                                                if ($fisexam['ortop_pea_li_inj_nat'] === 'mot_change'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Alteração da motricidade
                                                </option>
                                                <option value="infusion_change" <?php
                                                if ($fisexam['ortop_pea_li_inj_nat'] === 'infusion_change'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Alteração da perfusão
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- PULSOS/PERFUSAO -->
                    <div class="well" style="margin-left:25px;">
                        <h3 class="panel-title pull-left">
                            <a href="javascript:void(0)" onclick="togglePanels('#pulperfu_evaluation', '#pulperfu_evaluation_options');">Avaliação de pulsos/perfusão</a>
                        </h3>
                        <div class="row-fluid">
                            <div id="pulperfu_evaluation_chev">
                                <a href="javascript:void(0)" onclick="togglePanels('#pulperfu_evaluation', '#pulperfu_evaluation_options');"><i class="fa fa-plus pull-right"></i></a>
                            </div>
                            <div id="pulperfu_evaluation_chevback" style="display:none;">
                                <a href="javascript:void(0)" onclick="togglePanels('#pulperfu_evaluation', '#pulperfu_evaluation_options');"><i class="fa fa-minus pull-right"></i></a>
                            </div>
                        </div>
                        <div id="pulperfu_evaluation" style="display:none;">
                            <hr>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" id="normal_pulperfu_evaluation"
                                    <?php
                                    if ($fisexam['normal_pulperfu_evaluation'] === 1): echo 'checked';
                                    endif;
                                    ?>>
                                    Avaliação de pulsos/perfusão sem alterações (normal)
                                </label>
                            </div>
                            <div class="row-fluid">
                                <div class="col-sm-5 well">
                                    <h4 class="text-center"> Lado direito </h4>
                                    <div class="row-fluid">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" id="normal_pulperfu_evaluation_r"
                                                <?php
                                                if ($fisexam['normal_pulperfu_evaluation_r'] === 1): echo 'checked';
                                                endif;
                                                ?>>
                                                Avaliação de pulsos/perfusão do lado direito sem alterações (normal)
                                            </label>
                                        </div>
                                    </div>
                                    <h4> Membros superiores </h4>
                                    <div id="pulperfu_r_detail" class="row-fluid">
                                        <div class="col-sm-3">
                                            <h5><label class="control-label">Braquial</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="pulperfu_braq_r_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="normal" <?php
                                                if ($fisexam['pulperfu_braq_r_inj_nat'] === 'normal'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Normal
                                                </option>
                                                <option value="dimin" <?php
                                                if ($fisexam['pulperfu_braq_r_inj_nat'] === 'dimin'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Diminuído
                                                </option>
                                                <option value="inex" <?php
                                                if ($fisexam['pulperfu_braq_r_inj_nat'] === 'inex'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Ausente
                                                </option>
                                            </select>
                                        </div>
                                        <div class="col-sm-3">
                                            <h5><label class="control-label">Radial</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="pulperfu_rad_r_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="normal" <?php
                                                if ($fisexam['pulperfu_rad_r_inj_nat'] === 'normal'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Normal
                                                </option>
                                                <option value="dimin" <?php
                                                if ($fisexam['pulperfu_rad_r_inj_nat'] === 'dimin'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Diminuído
                                                </option>
                                                <option value="inex" <?php
                                                if ($fisexam['pulperfu_rad_r_inj_nat'] === 'inex'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Ausente
                                                </option>
                                            </select>
                                        </div>
                                        <div class="col-sm-3">
                                            <h5><label class="control-label">Ulnar</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="pulperfu_uln_r_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="normal" <?php
                                                if ($fisexam['pulperfu_uln_r_inj_nat'] === 'normal'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Normal
                                                </option>
                                                <option value="dimin" <?php
                                                if ($fisexam['pulperfu_uln_r_inj_nat'] === 'dimin'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Diminuído
                                                </option>
                                                <option value="inex" <?php
                                                if ($fisexam['pulperfu_uln_r_inj_nat'] === 'inex'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Ausente
                                                </option>
                                            </select>
                                        </div>
                                        <div class="col-sm-4">
                                            <h5><label class="control-label">Perfusão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="pulperfu_perfu_r_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="normal" <?php
                                                if ($fisexam['pulperfu_perfu_r_inj_nat'] === 'normal'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Normal
                                                </option>
                                                <option value="lent" <?php
                                                if ($fisexam['pulperfu_perfu_r_inj_nat'] === 'lent'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Lentificada
                                                </option>
                                                <option value="inex" <?php
                                                if ($fisexam['pulperfu_perfu_r_inj_nat'] === 'inex'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Ausente
                                                </option>
                                                <option value="cianose" <?php
                                                if ($fisexam['pulperfu_perfu_r_inj_nat'] === 'cianose'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Cianose
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h4> Membros inferiores </h4>
                                    <div id="pulperfu_r_detail" class="row-fluid">
                                        <div class="col-sm-3">
                                            <h5><label class="control-label">Femural</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="pulperfu_fem_r_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="normal" <?php
                                                if ($fisexam['pulperfu_fem_r_inj_nat'] === 'normal'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Normal
                                                </option>
                                                <option value="dimin" <?php
                                                if ($fisexam['pulperfu_fem_r_inj_nat'] === 'dimin'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Diminuído
                                                </option>
                                                <option value="inex" <?php
                                                if ($fisexam['pulperfu_fem_r_inj_nat'] === 'inex'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Ausente
                                                </option>
                                            </select>
                                        </div>
                                        <div class="col-sm-3">
                                            <h5><label class="control-label">Poplíteo</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="pulperfu_pop_r_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="normal" <?php
                                                if ($fisexam['pulperfu_pop_r_inj_nat'] === 'normal'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Normal
                                                </option>
                                                <option value="dimin" <?php
                                                if ($fisexam['pulperfu_pop_r_inj_nat'] === 'dimin'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Diminuído
                                                </option>
                                                <option value="inex" <?php
                                                if ($fisexam['pulperfu_pop_r_inj_nat'] === 'inex'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Ausente
                                                </option>
                                            </select>
                                        </div>
                                        <div class="col-sm-3">
                                            <h5><label class="control-label">Tibial posterior</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="pulperfu_tib_r_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="normal" <?php
                                                if ($fisexam['pulperfu_tib_r_inj_nat'] === 'normal'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Normal
                                                </option>
                                                <option value="dimin" <?php
                                                if ($fisexam['pulperfu_tib_r_inj_nat'] === 'dimin'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Diminuído
                                                </option>
                                                <option value="inex" <?php
                                                if ($fisexam['pulperfu_tib_r_inj_nat'] === 'inex'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Ausente
                                                </option>
                                            </select>
                                        </div>
                                        <div class="col-sm-3">
                                            <h5><label class="control-label">Pedioso</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="pulperfu_ped_r_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="normal" <?php
                                                if ($fisexam['pulperfu_ped_r_inj_nat'] === 'normal'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Normal
                                                </option>
                                                <option value="dimin" <?php
                                                if ($fisexam['pulperfu_ped_r_inj_nat'] === 'dimin'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Diminuído
                                                </option>
                                                <option value="inex" <?php
                                                if ($fisexam['pulperfu_ped_r_inj_nat'] === 'inex'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Ausente
                                                </option>
                                            </select>
                                        </div>
                                        <div class="col-sm-4">
                                            <h5><label class="control-label">Perfusão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="pulperfu_perfuinf_r_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="normal" <?php
                                                if ($fisexam['pulperfu_perfuinf_r_inj_nat'] === 'normal'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Normal
                                                </option>
                                                <option value="lent" <?php
                                                if ($fisexam['pulperfu_perfuinf_r_inj_nat'] === 'lent'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Lentificada
                                                </option>
                                                <option value="inex" <?php
                                                if ($fisexam['pulperfu_perfuinf_r_inj_nat'] === 'inex'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Ausente
                                                </option>
                                                <option value="cianose" <?php
                                                if ($fisexam['pulperfu_perfuinf_r_inj_nat'] === 'cianose'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Cianose
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-1"></div>

                                <div class="col-sm-5 well">
                                    <h4 class="text-center"> Lado esquerdo </h4>
                                    <div class="row-fluid">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" id="normal_pulperfu_evaluation_l"
                                                <?php
                                                if ($fisexam['normal_pulperfu_evaluation_l'] === 1): echo 'checked';
                                                endif;
                                                ?>>
                                                Avaliação da pelve do lado esquerdo sem alterações (normal)
                                            </label>
                                        </div>
                                    </div>
                                    <h4> Membros superiores </h4>
                                    <div id="pulperfu_l_detail" class="row-fluid">
                                        <div class="col-sm-3">
                                            <h5><label class="control-label">Braquial</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="pulperfu_braq_l_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="normal" <?php
                                                if ($fisexam['pulperfu_braq_l_inj_nat'] === 'normal'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Normal
                                                </option>
                                                <option value="dimin" <?php
                                                if ($fisexam['pulperfu_braq_l_inj_nat'] === 'dimin'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Diminuído
                                                </option>
                                                <option value="inex" <?php
                                                if ($fisexam['pulperfu_braq_l_inj_nat'] === 'inex'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Ausente
                                                </option>
                                            </select>
                                        </div>
                                        <div class="col-sm-3">
                                            <h5><label class="control-label">Radial</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="pulperfu_rad_l_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="normal" <?php
                                                if ($fisexam['pulperfu_rad_l_inj_nat'] === 'normal'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Normal
                                                </option>
                                                <option value="dimin" <?php
                                                if ($fisexam['pulperfu_rad_l_inj_nat'] === 'dimin'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Diminuído
                                                </option>
                                                <option value="inex" <?php
                                                if ($fisexam['pulperfu_rad_l_inj_nat'] === 'inex'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Ausente
                                                </option>
                                            </select>
                                        </div>
                                        <div class="col-sm-3">
                                            <h5><label class="control-label">Ulnar</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="pulperfu_uln_l_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="normal" <?php
                                                if ($fisexam['pulperfu_uln_l_inj_nat'] === 'normal'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Normal
                                                </option>
                                                <option value="dimin" <?php
                                                if ($fisexam['pulperfu_uln_l_inj_nat'] === 'dimin'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Diminuído
                                                </option>
                                                <option value="inex" <?php
                                                if ($fisexam['pulperfu_uln_l_inj_nat'] === 'inex'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Ausente
                                                </option>
                                            </select>
                                        </div>
                                        <div class="col-sm-4">
                                            <h5><label class="control-label">Perfusão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="pulperfu_perfu_l_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="normal" <?php
                                                if ($fisexam['pulperfu_perfu_l_inj_nat'] === 'normal'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Normal
                                                </option>
                                                <option value="lent" <?php
                                                if ($fisexam['pulperfu_perfu_l_inj_nat'] === 'lent'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Lentificada
                                                </option>
                                                <option value="inex" <?php
                                                if ($fisexam['pulperfu_perfu_l_inj_nat'] === 'inex'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Ausente
                                                </option>
                                                <option value="cianose" <?php
                                                if ($fisexam['pulperfu_perfu_l_inj_nat'] === 'cianose'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Cianose
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h4> Membros inferiores </h4>
                                    <div id="pulperfu_l_detail" class="row-fluid">
                                        <div class="col-sm-3">
                                            <h5><label class="control-label">Femural</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="pulperfu_fem_l_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="normal" <?php
                                                if ($fisexam['pulperfu_fem_l_inj_nat'] === 'normal'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Normal
                                                </option>
                                                <option value="dimin" <?php
                                                if ($fisexam['pulperfu_fem_l_inj_nat'] === 'dimin'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Diminuído
                                                </option>
                                                <option value="inex" <?php
                                                if ($fisexam['pulperfu_fem_l_inj_nat'] === 'inex'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Ausente
                                                </option>
                                            </select>
                                        </div>
                                        <div class="col-sm-3">
                                            <h5><label class="control-label">Poplíteo</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="pulperfu_pop_l_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="normal" <?php
                                                if ($fisexam['pulperfu_pop_l_inj_nat'] === 'normal'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Normal
                                                </option>
                                                <option value="dimin" <?php
                                                if ($fisexam['pulperfu_pop_l_inj_nat'] === 'dimin'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Diminuído
                                                </option>
                                                <option value="inex" <?php
                                                if ($fisexam['pulperfu_pop_l_inj_nat'] === 'inex'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Ausente
                                                </option>
                                            </select>
                                        </div>
                                        <div class="col-sm-3">
                                            <h5><label class="control-label">Tibial posterior</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="pulperfu_tib_l_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="normal" <?php
                                                if ($fisexam['pulperfu_tib_l_inj_nat'] === 'normal'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Normal
                                                </option>
                                                <option value="dimin" <?php
                                                if ($fisexam['pulperfu_tib_l_inj_nat'] === 'dimin'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Diminuído
                                                </option>
                                                <option value="inex" <?php
                                                if ($fisexam['pulperfu_tib_l_inj_nat'] === 'inex'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Ausente
                                                </option>
                                            </select>
                                        </div>
                                        <div class="col-sm-3">
                                            <h5><label class="control-label">Pedioso</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="pulperfu_ped_l_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="normal" <?php
                                                if ($fisexam['pulperfu_ped_l_inj_nat'] === 'normal'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Normal
                                                </option>
                                                <option value="dimin" <?php
                                                if ($fisexam['pulperfu_ped_l_inj_nat'] === 'dimin'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Diminuído
                                                </option>
                                                <option value="inex" <?php
                                                if ($fisexam['pulperfu_ped_l_inj_nat'] === 'inex'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Ausente
                                                </option>
                                            </select>
                                        </div>
                                        <div class="col-sm-4">
                                            <h5><label class="control-label">Perfusão</label></h5>
                                            <select data-plugin-selectTwo class="form-control populate input-medium mb-md" id="pulperfu_perfuinf_l_inj_nat">
                                                <option value="none"> - </option>
                                                <option value="normal" <?php
                                                if ($fisexam['pulperfu_perfuinf_l_inj_nat'] === 'normal'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Normal
                                                </option>
                                                <option value="lent" <?php
                                                if ($fisexam['pulperfu_perfuinf_l_inj_nat'] === 'lent'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Lentificada
                                                </option>
                                                <option value="inex" <?php
                                                if ($fisexam['pulperfu_perfuinf_l_inj_nat'] === 'inex'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Ausente
                                                </option>
                                                <option value="cianose" <?php
                                                if ($fisexam['pulperfu_perfuinf_l_inj_nat'] === 'cianose'): echo 'selected';
                                                endif;
                                                ?>>
                                                    Cianose
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <legend> 
                <a href="javascript:void(0)" onclick="togglePanels('#assist_team', '#assist_team_options');">Equipe Assistencial</a>
                <div id="assist_team_chev" class="pull-right">
                    <a href="javascript:void(0)" onclick="togglePanels('#assist_team', '#assist_team_options');"><i class="fa fa-plus-square-o"></i></a>
                </div>
                <div id="assist_team_chevback" style="display:none;" class="pull-right">
                    <a href="javascript:void(0)" onclick="togglePanels('#assist_team', '#assist_team_options');"><i class="fa fa-minus-square-o"></i></a>
                </div>
            </legend>

            <div id="assist_team" style="display:none;">
                <div class="well" id="assistencial_team">
                    <!-- supervisores -->
                    <div class="row-fluid">
                        <label for=""><b style="font-size: 14pt;">Supervisores</b></label>
                        <div class="row-fluid">
                            <div class="span4">
                                <select name="" id="supervisor_level" onchange="load_select_professionals('supervisor_spec', 'supervisor_level', 'supervisor_name')" class="span12 form-control">
                                    <option value="teaching_medical">Médico Docente</option>
                                    <option value="attending_physician">Médico Assistente</option>
                                </select>					
                            </div>
                            <div class="span4">
                                <select id="supervisor_spec" onchange="load_select_professionals('supervisor_spec', 'supervisor_level', 'supervisor_name')" class="span12 form-control">
                                    <option value="" selected>-</option>
                                    <option value="neurology" selected><?= lang('neurology') ?></option>
                                    <option value="surgery" ><?= lang('surgery') ?></option>
                                    <option value="orthopedics" ><?= lang('orthopedics') ?></option>
                                    <option value="head_and_neck" ><?= lang('head_and_neck') ?></option>
                                    <option value="neurosurgery" ><?= lang('neurosurgery') ?></option>
                                    <option value="ophthalmology" ><?= lang('ophthalmology') ?></option>
                                    <option value="otorhinolaringology" ><?= lang('otorhinolaringology') ?></option>
                                    <option value="pediatrics" ><?= lang('pediatrics') ?></option>
                                    <option value="burned" ><?= lang('burned') ?></option>
                                    <option value="chest" ><?= lang('chest') ?></option>
                                    <option value="urology" ><?= lang('urology') ?></option>
                                    <option value="vascular" ><?= lang('vascular') ?></option>
                                </select>									
                            </div>
                            <div class="span4">
                                <div class="control-group">
                                    <div class="controls">
                                        <select class="span12 form-control" id="supervisor_name">
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </br>
                        <div class="row-fluid ">
                            <div class=" span12">
                                <a id="add_supervisor" style="width:100%" class="btn btn-primary btn-small"><i class="fa fa-plus"></i></a>						
                            </div>
                        </div>
                        <div class="row-fluid">
                            <div class="span12">
                                <table class="table" id="supervisor_list">
                                    <thead>
                                        <tr>
                                            <th width="400">Nome</th>
                                            <th width="300">Cargo</th>
                                            <th width="300">Especialidade</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (isset($supervisor_list)): ?>
                                            <?php foreach ($supervisor_list as $a): ?>
                                                <tr id="team_member_<?= $a['id'] ?>">
                                                    <th><?= $a['name'] ?></th>
                                                    <th><?= lang($a['hierarchy']) ?></th>
                                                    <th><?= lang($a['specialty']) ?></th>
                                                    <th><a href="javascript:void(0)" onclick="remove_assist_team_member(<?= $a['id'] ?>)">Remover</a></th>
                                                </tr>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    </br>
                    <!-- residentes -->
                    <div class="row-fluid">
                        <label for=""><b style="font-size: 14pt;">Residentes</b></label>
                        <div class="row-fluid">
                            <div class="span4">
                                <select name="" id="resident_level" onchange="load_select_professionals('resident_spec', 'resident_level', 'resident_name')" class="span12 form-control">
                                    <option value="r1">R1</option>
                                    <option value="r2">R2</option>
                                    <option value="r3">R3</option>
                                </select>					
                            </div>
                            <div class="span4">
                                <select id="resident_spec" onchange="load_select_professionals('resident_spec', 'resident_level', 'resident_name')" class="span12 form-control">
                                    <option value="" selected>-</option>
                                    <option value="neurology" selected><?= lang('neurology') ?></option>
                                    <option value="surgery" ><?= lang('surgery') ?></option>
                                    <option value="orthopedics" ><?= lang('orthopedics') ?></option>
                                    <option value="head_and_neck" ><?= lang('head_and_neck') ?></option>
                                    <option value="neurosurgery" ><?= lang('neurosurgery') ?></option>
                                    <option value="ophthalmology" ><?= lang('ophthalmology') ?></option>
                                    <option value="otorhinolaringology" ><?= lang('otorhinolaringology') ?></option>
                                    <option value="pediatrics" ><?= lang('pediatrics') ?></option>
                                    <option value="burned" ><?= lang('burned') ?></option>
                                    <option value="chest" ><?= lang('chest') ?></option>
                                    <option value="urology" ><?= lang('urology') ?></option>
                                    <option value="vascular" ><?= lang('vascular') ?></option>
                                </select>									
                            </div>
                            <div class="span4">
                                <div class="control-group">
                                    <div class="controls">
                                        <select class="span12 form-control" id="resident_name">
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </br>
                        <div class="row-fluid ">
                            <div class=" span12">
                                <a id="add_resident" style="width:100%" class="btn btn-primary btn-small"><i class="fa fa-plus"></i></a>						
                            </div>
                        </div>
                        <div class="row-fluid">
                            <div class="span12">
                                <table class="table" id="resident_list">
                                    <thead>
                                        <tr>
                                            <th width="300">Especialidade</th>
                                            <th width="300">Cargo</th>
                                            <th width="400">Nome</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (isset($resident_list)): ?>
                                            <?php foreach ($resident_list as $a): ?>
                                                <tr id="team_member_<?= $a['id'] ?>">
                                                    <th><?= $a['name'] ?></th>
                                                    <th><?= lang($a['hierarchy']) ?></th>
                                                    <th><?= lang($a['specialty']) ?></th>
                                                    <th><a href="javascript:void(0)" onclick="remove_assist_team_member(<?= $a['id'] ?>)">Remover</a></th>
                                                </tr>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    </br>
                    <!-- enfermaria -->
                    <div class="row-fluid">
                        <label for=""><b style="font-size: 14pt;">Enfermeiros</b></label>
                        <div class="row-fluid">
                            <div class="span4">
                                <select name="" id="enf_level" onchange="load_select_professionals(null, 'enf_level', 'enf_name')" class="span12 form-control">
                                    <option value="nurse">Enfermeiro</option>
                                </select>					
                            </div>

                            <div class="span4">
                                <div class="control-group">
                                    <div class="controls">
                                        <select class="span12 form-control" id="enf_name">
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </br>
                        <div class="row-fluid">
                            <div class=" span12">
                                <a id="add_enf" style="width:100%" class="btn btn-primary btn-small"><i class="fa fa-plus"></i></a>						
                            </div>
                        </div>
                        <div class="row-fluid">
                            <div class=" span12">
                                <table class="table" id="enf_list">
                                    <thead>
                                        <tr>
                                            <th width="300">Cargo</th>
                                            <th width="400">Nome</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (isset($enf_list)): ?>
                                            <?php foreach ($enf_list as $a): ?>
                                                <tr id="team_member_<?= $a['id'] ?>">
                                                    <th><?= $a['name'] ?></th>
                                                    <th><?= lang($a['hierarchy']) ?></th>
                                                    <th><a href="javascript:void(0)" onclick="remove_assist_team_member(<?= $a['id'] ?>)">Remover</a></th>
                                                </tr>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="row-fluid">
                        <label for=""><b style="font-size: 14pt;">Técnicos em Enfermagem</b></label>
                        <div class="row-fluid">
                            <div class="span4">
                                <select name="" id="t_enf_level" onchange="load_select_professionals(null, 't_enf_level', 't_enf_name')" class="span12 form-control">
                                    <option value="nursing_technician">Técnico de Enfermagem</option>
                                </select>					
                            </div>
                            <div class="span4">
                                <div class="control-group">
                                    <div class="controls">
                                        <select class="span12 form-control" id="t_enf_name">
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </br>
                        <div class="row-fluid">
                            <div class=" span12">
                                <a id="add_t_enf" style="width:100%" class="btn btn-primary btn-small"><i class="fa fa-plus"></i></a>						
                            </div>
                        </div>
                        <div class="row-fluid">
                            <div class=" span12">
                                <table class="table" id="t_enf_list">
                                    <thead>
                                        <tr>
                                            <th width="300">Cargo</th>
                                            <th width="400">Nome</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (isset($t_enf_list)): ?>
                                            <?php foreach ($t_enf_list as $a): ?>
                                                <tr id="team_member_<?= $a['id'] ?>">
                                                    <th><?= $a['name'] ?></th>
                                                    <th><?= lang($a['hierarchy']) ?></th>
                                                    <th><a href="javascript:void(0)" onclick="remove_assist_team_member(<?= $a['id'] ?>)">Remover</a></th>
                                                </tr>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<a href="javascript:void(0);" class="btn btn-success btn-sm save_button" onclick="save_fixed_history(); save_phys_exam();"><?= lang('save') ?></a>

<script>
    function togglePanels(content, options) {
        jQuery(content).toggle();
        jQuery(content + '_chev').toggle();
        jQuery(content + '_chevback').toggle();
        jQuery(options).toggle();
    }

    function load_select_professionals(specialty_input, hierarchy_input, select_name) {
        if (specialty_input === null) {
            var specialty = null;
        } else {
            var specialty = jQuery('#' + specialty_input).val();
        }
        var hierarchy = jQuery('#' + hierarchy_input).val();
        jQuery.ajax({
            url: jQuery("body").data("baseurl") + "home/load_select_professionals",
            type: "post",
            dataType: 'json',
            data: {
                specialty: specialty,
                hierarchy: hierarchy
            },
            success: function (response) {
                jQuery('#' + select_name).html('');
                jQuery.each(response.professionals, function (index, value) {
                    jQuery('#' + select_name).append('<option value="' + value["id"] + '">' + value["name"] + '</option>');
                });
            }
        });
    }

    function remove_assist_team_member(id) {
        jQuery.ajax({
            url: jQuery("body").data("baseurl") + "attendance_hospital/remove_assist_team_member",
            type: "post",
            dataType: 'json',
            data: {
                id: id
            },
            success: function (response) {
                // remove da tabela
                jQuery('#team_member_' + id).remove();
            }
        });
    }

    // roda o script para enf e tecs em enf
    load_select_professionals(null, 't_enf_level', 't_enf_name');
    load_select_professionals(null, 'enf_level', 'enf_name');
    jQuery('#add_resident').click(function () {
        var especialidade = jQuery('#resident_spec').val();
        var cargo = jQuery('#resident_level').val();
        var nome = jQuery('#resident_name').text();
        var id = jQuery('#resident_name').val();
        if (nome !== '') {
            // adiciona no banco
            jQuery.ajax({
                url: jQuery("body").data("baseurl") + "attendance_hospital/add_assist_team",
                type: "post",
                dataType: 'json',
                data: {
                    team_member_id: id
                },
                success: function (response) {
                    // adiciona na tabela
                    jQuery('#resident_list').append('<tr id="team_member_' + response.id + '"><th>' + especialidade + '</th><th>' + cargo + '</th><th>' + nome + '</th> <th><a href="javascript:void(0)" onclick="remove_assist_team_member(' + response.id + ')">Remover</a></th></tr>');
                }
            });
        } else {
            alert('É preciso escolher o profissional');
        }
    });
    jQuery('#add_supervisor').click(function () {
        var especialidade = jQuery('#supervisor_spec').val();
        var cargo = jQuery('#supervisor_level').val();
        var nome = jQuery('#supervisor_name').text();
        var id = jQuery('#supervisor_name').val();
        if (nome !== '') {
            // adiciona no banco
            jQuery.ajax({
                url: jQuery("body").data("baseurl") + "attendance_hospital/add_assist_team",
                type: "post",
                dataType: 'json',
                data: {
                    team_member_id: id
                },
                success: function (response) {
                    // adiciona na tabela
                    jQuery('#supervisor_list').append('<tr id="team_member_' + response.id + '"><th>' + nome + '</th><th>' + cargo + '</th><th>' + especialidade + '</th> <th><a href="javascript:void(0)" onclick="remove_assist_team_member(' + response.id + ')">Remover</a></th></tr>');
                }
            });
        } else {
            alert('É preciso escolher o profissional');
        }
    });
    jQuery('#add_enf').click(function () {
        var cargo = jQuery('#enf_level').val();
        var nome = jQuery('#enf_name').text();
        var id = jQuery('#enf_name').val();
        if (nome !== '') {
            // adiciona no banco
            jQuery.ajax({
                url: jQuery("body").data("baseurl") + "attendance_hospital/add_assist_team",
                type: "post",
                dataType: 'json',
                data: {
                    team_member_id: id
                },
                success: function (response) {
                    // adiciona na tabela
                    jQuery('#enf_list').append('<tr id="team_member_' + response.id + '"><th>' + cargo + '</th><th>' + nome + '</th> <th><a href="javascript:void(0)" onclick="remove_assist_team_member(' + response.id + ')">Remover</a></th></tr>');
                }
            });
        } else {
            alert('É preciso escolher o profissional');
        }
    });
    jQuery('#add_t_enf').click(function () {
        var cargo = jQuery('#t_enf_level').val();
        var nome = jQuery('#t_enf_name').text();
        var id = jQuery('#t_enf_name').val();
        if (nome !== '') {
            // adiciona no banco
            jQuery.ajax({
                url: jQuery("body").data("baseurl") + "attendance_hospital/add_assist_team",
                type: "post",
                dataType: 'json',
                data: {
                    team_member_id: id
                },
                success: function (response) {
                    // adiciona na tabela
                    jQuery('#t_enf_list').append('<tr id="team_member_' + response.id + '"><th>' + cargo + '</th><th>' + nome + '</th> <th><a href="javascript:void(0)" onclick="remove_assist_team_member(' + response.id + ')">Remover</a></th></tr>');
                }
            });
        } else {
            alert('É preciso escolher o profissional');
        }
    });</script>

<script>
    function save_fixed_history() {
        visual = 0;
        jQuery.ajax({
            url: jQuery("body").data("baseurl") + "attendance_hospital/save_history",
            type: "post",
            dataType: 'json',
            data: {
                hiv: jQuery('#hiv').prop('checked'),
                hep: jQuery('#hep').prop('checked'),
                has: jQuery('#has ').prop('checked'),
                diab: jQuery('#diab').prop('checked'),
                dpoc: jQuery('#dpoc').prop('checked'),
                avc: jQuery('#avc').prop('checked'),
                sci: jQuery('#sci').prop('checked'),
                psic_transt: jQuery('#psic_transt').prop('checked'),
                elit: jQuery('#elit').prop('checked'),
                udi: jQuery('#udi').prop('checked'),
                ant_gestation: jQuery('#ant_gestation').prop('checked'),
                ant_tetanus_vaccination: jQuery('#ant_tetanus_vaccination').prop('checked'),
                ant_acute_intoxication: jQuery('#ant_acute_intoxication').prop('checked'),
                ant_fluid_intake: jQuery('#ant_fluid_intake').prop('checked'),
                physical_disability: jQuery('#physical_disability').prop('checked'),
                mental_disability: jQuery('#mental_disability').prop('checked'),
                hearing_deficiency: jQuery('#hearing_deficiency').prop('checked'),
                visual_impairment: jQuery('#visual_impairment').prop('checked'),
                context: 'hospital'
            },
            success: function (response) {
                if (response.status === 'OK') {
                    jQuery('#hospital_attendance_steps').load(jQuery("body").data("baseurl") + "attendance_hospital/secundary_evaluation");
                } else {
                    var notice = new PNotify({
                        title: 'Erro',
                        text: 'Tente novamente mais tarde',
                        type: 'error',
                        addclass: 'click-2-close',
                        hide: false,
                        buttons: {
                            closer: false,
                            sticker: false
                        }
                    });
                    notice.get().click(function () {
                        notice.remove();
                    });
                }
            }
        });
    }

    function add_history_item_cid(name) {
        if (!jQuery('[name=ec_category_cid_field]').val()) {
            var notice = new PNotify({
                title: 'Erro',
                text: 'É preciso escolher uma categoria',
                type: 'error',
                addclass: 'click-2-close',
                hide: false,
                buttons: {
                    closer: false,
                    sticker: false
                }
            });
            notice.get().click(function () {
                notice.remove();
            });
            return false;
        }

        if(jQuery('[name=ec_subcategory_cid_field]').val()){
            var subcategory = ' - '+jQuery('[name=ec_subcategory_cid_field]').val();
        }else{
            var subcategory = '';
        }

        jQuery.ajax({
            url: jQuery("body").data("baseurl") + "prehospital_fixed/add_history_item_cid/" + name,
            type: "post",
            dataType: 'json',
            data: {
                id: jQuery('[name=ec_category_cid_field] option:selected').text() + subcategory,
                context: 'hospital'
            },
            success: function (response) {
                if (response.status === 'OK') {
                    jQuery('#history_' + name + '_list').load(jQuery("body").data("baseurl") + "prehospital_fixed/load_history_" + name + "/hospital");
                } else {
                    var notice = new PNotify({
                        title: 'Erro',
                        text: 'Tente novamente mais tarde',
                        type: 'error',
                        addclass: 'click-2-close',
                        hide: false,
                        buttons: {
                            closer: false,
                            sticker: false
                        }
                    });
                    notice.get().click(function () {
                        notice.remove();
                    });
                }
            }
        });
    }

    function add_history_item(name) {
        jQuery.ajax({
            url: jQuery("body").data("baseurl") + "prehospital_fixed/add_history_item/" + name,
            type: "post",
            dataType: 'json',
            data: {
                id: jQuery('#history_' + name + '_c').val(),
                context: 'hospital'
            },
            success: function (response) {
                if (response.status === 'OK') {
                    jQuery('#history_' + name + '_list').load(jQuery("body").data("baseurl") + "prehospital_fixed/load_history_" + name + "/hospital");
                } else {
                    var notice = new PNotify({
                        title: 'Erro',
                        text: 'Tente novamente mais tarde',
                        type: 'error',
                        addclass: 'click-2-close',
                        hide: false,
                        buttons: {
                            closer: false,
                            sticker: false
                        }
                    });
                    notice.get().click(function () {
                        notice.remove();
                    });
                }
            }
        });
    }

    function delete_history_item(name, item_id) {
        jQuery.ajax({
            url: jQuery("body").data("baseurl") + "prehospital_fixed/delete_history_item",
            type: "post",
            dataType: 'json',
            data: {
                id: item_id,
                name: name
            },
            success: function (response) {
                if (response.status === 'OK') {
                    jQuery('#history_' + name + '_list').load(jQuery("body").data("baseurl") + "prehospital_fixed/load_history_" + name + "/hospital");
                } else {
                    var notice = new PNotify({
                        title: 'Erro',
                        text: 'Tente novamente mais tarde',
                        type: 'error',
                        addclass: 'click-2-close',
                        hide: false,
                        buttons: {
                            closer: false,
                            sticker: false
                        }
                    });
                    notice.get().click(function () {
                        notice.remove();
                    });
                }
            }
        });
    }

    function save_phys_exam() {
        jQuery.ajax({
            url: jQuery("body").data("baseurl") + "attendance_hospital/save_physical_exams",
            type: "post",
            dataType: 'json',
            data: {
                compfisevaluation: jQuery('#compfisevaluation').prop('checked'),
                normal_head_evaluation: jQuery('#normal_head_evaluation').prop('checked'),
                normal_head_evaluation_r: jQuery('#normal_head_evaluation_r').prop('checked'),
                normal_head_evaluation_l: jQuery('#normal_head_evaluation_l').prop('checked'),
                normal_face_evaluation: jQuery('#normal_face_evaluation').prop('checked'),
                normal_face_evaluation_r: jQuery('#normal_face_evaluation_r').prop('checked'),
                normal_face_evaluation_l: jQuery('#normal_face_evaluation_l').prop('checked'),
                normal_neck_evaluation: jQuery('#normal_neck_evaluation').prop('checked'),
                normal_neck_evaluation_r: jQuery('#normal_neck_evaluation_r').prop('checked'),
                normal_neck_evaluation_l: jQuery('#normal_neck_evaluation_l').prop('checked'),
                normal_spine_evaluation: jQuery('#normal_spine_evaluation').prop('checked'),
                normal_spine_evaluation_r: jQuery('#normal_spine_evaluation_r').prop('checked'),
                normal_spine_evaluation_l: jQuery('#normal_spine_evaluation_l').prop('checked'),
                normal_belly_evaluation: jQuery('#normal_belly_evaluation').prop('checked'),
                normal_belly_evaluation_r: jQuery('#normal_belly_evaluation_r').prop('checked'),
                normal_belly_evaluation_l: jQuery('#normal_belly_evaluation_l').prop('checked'),
                normal_chest_evaluation: jQuery('#normal_chest_evaluation').prop('checked'),
                normal_chest_evaluation_r: jQuery('#normal_chest_evaluation_r').prop('checked'),
                normal_chest_evaluation_l: jQuery('#normal_chest_evaluation_l').prop('checked'),
                normal_pelvis_evaluation: jQuery('#normal_pelvis_evaluation').prop('checked'),
                normal_pelvis_evaluation_r: jQuery('#normal_pelvis_evaluation_r').prop('checked'),
                normal_pelvis_evaluation_l: jQuery('#normal_pelvis_evaluation_l').prop('checked'),
                normal_prg_evaluation: jQuery('#normal_prg_evaluation').prop('checked'),
                normal_prg_evaluation_r: jQuery('#normal_prg_evaluation_r').prop('checked'),
                normal_prg_evaluation_l: jQuery('#normal_prg_evaluation_l').prop('checked'),
                normal_ortop_evaluation: jQuery('#normal_ortop_evaluation').prop('checked'),
                normal_ortop_evaluation_rs: jQuery('#normal_ortop_evaluation_rs').prop('checked'),
                normal_ortop_evaluation_ri: jQuery('#normal_ortop_evaluation_ri').prop('checked'),
                normal_ortop_evaluation_ls: jQuery('#normal_ortop_evaluation_ls').prop('checked'),
                normal_ortop_evaluation_li: jQuery('#normal_ortop_evaluation_li').prop('checked'),
                head_cba_r_inj_nat: jQuery('#head_cba_r_inj_nat').val(),
                head_cca_r_inj_nat: jQuery('#head_cca_r_inj_nat').val(),
                head_bca_r_inj_nat: jQuery('#head_bca_r_inj_nat').val(),
                head_cba_l_inj_nat: jQuery('#head_cba_l_inj_nat').val(),
                head_cca_l_inj_nat: jQuery('#head_cca_l_inj_nat').val(),
                head_bca_l_inj_nat: jQuery('#head_bca_l_inj_nat').val(),
                face_pela_r_inj_nat: jQuery('#face_pela_r_inj_nat').val(),
                face_olha_r_inj_nat: jQuery('#face_olha_r_inj_nat').val(),
                face_orea_r_inj_nat: jQuery('#face_orea_r_inj_nat').val(),
                face_bgla_r_inj_nat: jQuery('#face_bgla_r_inj_nat').val(),
                face_manda_r_inj_nat: jQuery('#face_manda_r_inj_nat').val(),
                face_maxia_r_inj_nat: jQuery('#face_maxia_r_inj_nat').val(),
                face_nara_r_inj_nat: jQuery('#face_nara_r_inj_nat').val(),
                face_orba_r_inj_nat: jQuery('#face_orba_r_inj_nat').val(),
                face_ziga_r_inj_nat: jQuery('#face_ziga_r_inj_nat').val(),
                face_pela_l_inj_nat: jQuery('#face_pela_l_inj_nat').val(),
                face_olha_l_inj_nat: jQuery('#face_olha_l_inj_nat').val(),
                face_orea_l_inj_nat: jQuery('#face_orea_l_inj_nat').val(),
                face_blga_l_inj_nat: jQuery('#face_blga_l_inj_nat').val(),
                face_manda_l_inj_nat: jQuery('#face_manda_l_inj_nat').val(),
                face_maxia_l_inj_nat: jQuery('#face_maxia_l_inj_nat').val(),
                face_nara_l_inj_nat: jQuery('#face_nara_l_inj_nat').val(),
                face_orba_l_inj_nat: jQuery('#face_orba_l_inj_nat').val(),
                face_ziga_l_inj_nat: jQuery('#face_ziga_l_inj_nat').val(),
                neck_pela_r_inj_nat: jQuery('#neck_pela_r_inj_nat').val(),
                neck_pca_r_inj_nat: jQuery('#neck_pca_r_inj_nat').val(),
                neck_pela_l_inj_nat: jQuery('#neck_pela_l_inj_nat').val(),
                neck_pca_l_inj_nat: jQuery('#neck_pca_l_inj_nat').val(),
                spine_cca_r_inj_nat: jQuery('#spine_cca_r_inj_nat').val(),
                spine_cta_r_inj_nat: jQuery('#spine_cta_r_inj_nat').val(),
                spine_clsa_r_inj_nat: jQuery('#spine_clsa_r_inj_nat').val(),
                spine_cca_l_inj_nat: jQuery('#spine_cca_l_inj_nat').val(),
                spine_cta_l_inj_nat: jQuery('#spine_cta_l_inj_nat').val(),
                spine_clsa_l_inj_nat: jQuery('#spine_clsa_l_inj_nat').val(),
                chest_ieda_r_inj_nat: jQuery('#chest_ieda_r_inj_nat').val(),
                chest_ieda_l_inj_nat: jQuery('#chest_ieda_l_inj_nat').val(),
                chest_perca_r_inj_nat: jQuery('#chest_perca_r_inj_nat').val(),
                chest_perca_l_inj_nat: jQuery('#chest_perca_l_inj_nat').val(),
                chest_ausca_r_inj_nat: jQuery('#chest_ausca_r_inj_nat').val(),
                chest_ausca_l_inj_nat: jQuery('#chest_ausca_l_inj_nat').val(),
                belly_ieda_r_inj_nat: jQuery('#belly_ieda_r_inj_nat').val(),
                belly_ieda_l_inj_nat: jQuery('#belly_ieda_l_inj_nat').val(),
                belly_perca_r_inj_nat: jQuery('#belly_perca_r_inj_nat').val(),
                belly_perca_l_inj_nat: jQuery('#belly_perca_l_inj_nat').val(),
                belly_ausca_r_inj_nat: jQuery('#belly_ausca_r_inj_nat').val(),
                belly_ausca_l_inj_nat: jQuery('#belly_ausca_l_inj_nat').val(),
                pelvis_pela_r_inj_nat: jQuery('#pelvis_pela_r_inj_nat').val(),
                pelvis_pela_l_inj_nat: jQuery('#pelvis_pela_l_inj_nat').val(),
                pelvis_palpa_r_inj_nat: jQuery('#pelvis_palpa_r_inj_nat').val(),
                pelvis_palpa_l_inj_nat: jQuery('#pelvis_palpa_l_inj_nat').val(),
                pelvis_pera_r_inj_nat: jQuery('#pelvis_pera_r_inj_nat').val(),
                pelvis_pera_l_inj_nat: jQuery('#pelvis_pera_l_inj_nat').val(),
                pelvis_toqret_r_inj_nat: jQuery('#pelvis_toqret_r_inj_nat').val(),
                pelvis_toqret_l_inj_nat: jQuery('#pelvis_toqret_l_inj_nat').val(),
                pelvis_pena_l_inj_nat: jQuery('#pelvis_pena_l_inj_nat').val(),
                pelvis_pena_r_inj_nat: jQuery('#pelvis_pena_r_inj_nat').val(),
                pelvis_testa_r_inj_nat: jQuery('#pelvis_testa_r_inj_nat').val(),
                pelvis_testa_l_inj_nat: jQuery('#pelvis_testa_l_inj_nat').val(),
                pelvis_vulva_r_inj_nat: jQuery('#pelvis_vulva_r_inj_nat').val(),
                pelvis_vulva_l_inj_nat: jQuery('#pelvis_vulva_l_inj_nat').val(),
                ortop_clava_rs_inj_nat: jQuery('#ortop_clava_rs_inj_nat').val(),
                ortop_omba_rs_inj_nat: jQuery('#ortop_omba_rs_inj_nat').val(),
                ortop_brca_rs_inj_nat: jQuery('#ortop_brca_rs_inj_nat').val(),
                ortop_cota_rs_inj_nat: jQuery('#ortop_cota_rs_inj_nat').val(),
                ortop_anta_rs_inj_nat: jQuery('#ortop_anta_rs_inj_nat').val(),
                ortop_puna_rs_inj_nat: jQuery('#ortop_puna_rs_inj_nat').val(),
                ortop_maoa_rs_inj_nat: jQuery('#ortop_maoa_rs_inj_nat').val(),
                ortop_clava_ls_inj_nat: jQuery('#ortop_clava_ls_inj_nat').val(),
                ortop_omba_ls_inj_nat: jQuery('#ortop_omba_ls_inj_nat').val(),
                ortop_brca_ls_inj_nat: jQuery('#ortop_brca_ls_inj_nat').val(),
                ortop_cota_ls_inj_nat: jQuery('#ortop_cota_ls_inj_nat').val(),
                ortop_anta_ls_inj_nat: jQuery('#ortop_anta_ls_inj_nat').val(),
                ortop_puna_ls_inj_nat: jQuery('#ortop_puna_ls_inj_nat').val(),
                ortop_maoa_ls_inj_nat: jQuery('#ortop_maoa_ls_inj_nat').val(),
                ortop_quada_ri_inj_nat: jQuery('#ortop_quada_ri_inj_nat').val(),
                ortop_quada_li_inj_nat: jQuery('#ortop_quada_li_inj_nat').val(),
                ortop_coxa_ri_inj_nat: jQuery('#ortop_coxa_ri_inj_nat').val(),
                ortop_coxa_li_inj_nat: jQuery('#ortop_coxa_li_inj_nat').val(),
                ortop_joea_ri_inj_nat: jQuery('#ortop_joea_ri_inj_nat').val(),
                ortop_joea_li_inj_nat: jQuery('#ortop_joea_li_inj_nat').val(),
                ortop_pera_ri_inj_nat: jQuery('#ortop_pera_ri_inj_nat').val(),
                ortop_pera_li_inj_nat: jQuery('#ortop_pera_li_inj_nat').val(),
                ortop_tora_ri_inj_nat: jQuery('#ortop_tora_ri_inj_nat').val(),
                ortop_tora_li_inj_nat: jQuery('#ortop_tora_li_inj_nat').val(),
                ortop_pea_ri_inj_nat: jQuery('#ortop_pea_ri_inj_nat').val(),
                ortop_pea_li_inj_nat: jQuery('#ortop_pea_li_inj_nat').val(),
                normal_pulperfu_evaluation: jQuery('#normal_pulperfu_evaluation').prop('checked'),
                normal_pulperfu_evaluation_r: jQuery('#normal_pulperfu_evaluation_r').prop('checked'),
                normal_pulperfu_evaluation_l: jQuery('#normal_pulperfu_evaluation_l').prop('checked'),
                pulperfu_braq_r_inj_nat: jQuery('#pulperfu_braq_r_inj_nat').val(),
                pulperfu_rad_r_inj_nat: jQuery('#pulperfu_rad_r_inj_nat').val(),
                pulperfu_uln_r_inj_nat: jQuery('#pulperfu_uln_r_inj_nat').val(),
                pulperfu_perfu_r_inj_nat: jQuery('#pulperfu_perfu_r_inj_nat').val(),
                pulperfu_fem_r_inj_nat: jQuery('#pulperfu_fem_r_inj_nat').val(),
                pulperfu_pop_r_inj_nat: jQuery('#pulperfu_pop_r_inj_nat').val(),
                pulperfu_tib_r_inj_nat: jQuery('#pulperfu_tib_r_inj_nat').val(),
                pulperfu_ped_r_inj_nat: jQuery('#pulperfu_ped_r_inj_nat').val(),
                pulperfu_perfuinf_r_inj_nat: jQuery('#pulperfu_perfuinf_r_inj_nat').val(),
                pulperfu_braq_l_inj_nat: jQuery('#pulperfu_braq_l_inj_nat').val(),
                pulperfu_rad_l_inj_nat: jQuery('#pulperfu_rad_l_inj_nat').val(),
                pulperfu_uln_l_inj_nat: jQuery('#pulperfu_uln_l_inj_nat').val(),
                pulperfu_perfu_l_inj_nat: jQuery('#pulperfu_perfu_l_inj_nat').val(),
                pulperfu_fem_l_inj_nat: jQuery('#pulperfu_fem_l_inj_nat').val(),
                pulperfu_pop_l_inj_nat: jQuery('#pulperfu_pop_l_inj_nat').val(),
                pulperfu_tib_l_inj_nat: jQuery('#pulperfu_tib_l_inj_nat').val(),
                pulperfu_ped_l_inj_nat: jQuery('#pulperfu_ped_l_inj_nat').val(),
                pulperfu_perfuinf_l_inj_nat: jQuery('#pulperfu_perfuinf_l_inj_nat').val(),
                context: 'hospital'
            },
            success: function (response) {
                if (response.status === 'OK') {
                    jQuery('#hospital_attendance_steps').load(jQuery("body").data("baseurl") + "attendance_hospital/secundary_evaluation");
                } else {
                    var notice = new PNotify({
                        title: 'Erro',
                        text: 'Tente novamente mais tarde',
                        type: 'error',
                        addclass: 'click-2-close',
                        hide: false,
                        buttons: {
                            closer: false,
                            sticker: false
                        }
                    });
                    notice.get().click(function () {
                        notice.remove();
                    });
                }
            }
        });
    }

    $("#compfisevaluation").change(function () {
        if (this.checked) {
            $('#normal_head_evaluation').prop('checked', true);
            $('#normal_head_evaluation_r').prop('checked', true);
            $('#normal_head_evaluation_l').prop('checked', true);
            headEvaluation('n', 'l');
            headEvaluation('n', 'r');
            $('#normal_face_evaluation').prop('checked', true);
            $('#normal_face_evaluation_r').prop('checked', true);
            $('#normal_face_evaluation_l').prop('checked', true);
            faceEvaluation('n', 'l');
            faceEvaluation('n', 'r');
            $('#normal_neck_evaluation').prop('checked', true);
            $('#normal_neck_evaluation_r').prop('checked', true);
            $('#normal_neck_evaluation_l').prop('checked', true);
            neckEvaluation('n', 'l');
            neckEvaluation('n', 'r');
            $('#normal_spine_evaluation').prop('checked', true);
            $('#normal_spine_evaluation_r').prop('checked', true);
            $('#normal_spine_evaluation_l').prop('checked', true);
            spineEvaluation('n', 'l');
            spineEvaluation('n', 'r');
            $('#normal_belly_evaluation').prop('checked', true);
            $('#normal_belly_evaluation_r').prop('checked', true);
            $('#normal_belly_evaluation_l').prop('checked', true);
            chestEvaluation('n', 'l');
            chestEvaluation('n', 'r');
            $('#normal_chest_evaluation').prop('checked', true);
            $('#normal_chest_evaluation_r').prop('checked', true);
            $('#normal_chest_evaluation_l').prop('checked', true);
            bellyEvaluation('n', 'l');
            bellyEvaluation('n', 'r');
            $('#normal_pelvis_evaluation').prop('checked', true);
            $('#normal_pelvis_evaluation_r').prop('checked', true);
            $('#normal_pelvis_evaluation_l').prop('checked', true);
            pelvisEvaluation('n', 'l');
            pelvisEvaluation('n', 'r');
            $('#normal_prg_evaluation').prop('checked', true);
            $('#normal_prg_evaluation_r').prop('checked', true);
            $('#normal_prg_evaluation_l').prop('checked', true);
            prgEvaluation('n', 'l');
            prgEvaluation('n', 'r');
            $('#normal_ortop_evaluation').prop('checked', true);
            $('#normal_ortop_evaluation_rs').prop('checked', true);
            $('#normal_ortop_evaluation_ri').prop('checked', true);
            $('#normal_ortop_evaluation_ls').prop('checked', true);
            $('#normal_ortop_evaluation_li').prop('checked', true);
            ortopEvaluation('n', 'ls');
            ortopEvaluation('n', 'li');
            ortopEvaluation('n', 'rs');
            ortopEvaluation('n', 'ri');
        } else {
            $('#normal_head_evaluation').prop('checked', false);
            $('#normal_head_evaluation_r').prop('checked', false);
            $('#normal_head_evaluation_l').prop('checked', false);
            headEvaluation('a', 'l');
            headEvaluation('a', 'r');
            $('#normal_face_evaluation').prop('checked', false);
            $('#normal_face_evaluation_r').prop('checked', false);
            $('#normal_face_evaluation_l').prop('checked', false);
            faceEvaluation('a', 'l');
            faceEvaluation('a', 'r');
            $('#normal_neck_evaluation').prop('checked', false);
            $('#normal_neck_evaluation_r').prop('checked', false);
            $('#normal_neck_evaluation_l').prop('checked', false);
            neckEvaluation('a', 'l');
            neckEvaluation('a', 'r');
            $('#normal_spine_evaluation').prop('checked', false);
            $('#normal_spine_evaluation_r').prop('checked', false);
            $('#normal_spine_evaluation_l').prop('checked', false);
            spineEvaluation('a', 'l');
            spineEvaluation('a', 'r');
            $('#normal_belly_evaluation').prop('checked', false);
            $('#normal_belly_evaluation_r').prop('checked', false);
            $('#normal_belly_evaluation_l').prop('checked', false);
            chestEvaluation('a', 'l');
            chestEvaluation('a', 'r');
            $('#normal_chest_evaluation').prop('checked', false);
            $('#normal_chest_evaluation_r').prop('checked', false);
            $('#normal_chest_evaluation_l').prop('checked', false);
            bellyEvaluation('a', 'l');
            bellyEvaluation('a', 'r');
            $('#normal_pelvis_evaluation').prop('checked', false);
            $('#normal_pelvis_evaluation_r').prop('checked', false);
            $('#normal_pelvis_evaluation_l').prop('checked', false);
            pelvisEvaluation('a', 'l');
            pelvisEvaluation('a', 'r');
            $('#normal_prg_evaluation').prop('checked', false);
            $('#normal_prg_evaluation_r').prop('checked', false);
            $('#normal_prg_evaluation_l').prop('checked', false);
            prgEvaluation('a', 'l');
            prgEvaluation('a', 'r');
            $('#normal_ortop_evaluation').prop('checked', false);
            $('#normal_ortop_evaluation_rs').prop('checked', false);
            $('#normal_ortop_evaluation_ri').prop('checked', false);
            $('#normal_ortop_evaluation_ls').prop('checked', false);
            $('#normal_ortop_evaluation_li').prop('checked', false);
            ortopEvaluation('a', 'ls');
            ortopEvaluation('a', 'li');
            ortopEvaluation('a', 'rs');
            ortopEvaluation('a', 'ri');
        }
    });
    $("#normal_head_evaluation").change(function () {
        if (this.checked) {
            $('#normal_head_evaluation_r').prop('checked', true);
            $('#normal_head_evaluation_l').prop('checked', true);
            headEvaluation('n', 'l');
            headEvaluation('n', 'r');
        } else {
            $('#normal_head_evaluation_r').prop('checked', false);
            $('#normal_head_evaluation_l').prop('checked', false);
            headEvaluation('a', 'l');
            headEvaluation('a', 'r');
        }
    });
    $("#normal_head_evaluation_r").change(function () {
        if (this.checked) {
            headEvaluation('n', 'r');
        } else {
            headEvaluation('a', 'r');
        }
    });
    $("#normal_head_evaluation_l").change(function () {
        if (this.checked) {
            headEvaluation('n', 'l');
        } else {
            headEvaluation('a', 'l');
        }
    });
    function headEvaluation(status, side) {
        if (side === 'l' && status === 'n') {
            $('#head_cbn_l').prop('checked', true);
            $('#head_ccn_l').prop('checked', true);
            $('#head_bcn_l').prop('checked', true);
        } else if (side === 'r' && status === 'n') {
            $('#head_cbn_r').prop('checked', true);
            $('#head_ccn_r').prop('checked', true);
            $('#head_bcn_r').prop('checked', true);
        } else if (side === 'l' && status === 'a') {
            $('#head_cbn_l').prop('checked', false);
            $('#head_ccn_l').prop('checked', false);
            $('#head_bcn_l').prop('checked', false);
        } else if (side === 'r' && status === 'a') {
            $('#head_cbn_r').prop('checked', false);
            $('#head_ccn_r').prop('checked', false);
            $('#head_bcn_r').prop('checked', false);
        }
    }

    $("#normal_face_evaluation").change(function () {
        if (this.checked) {
            $('#normal_face_evaluation_r').prop('checked', true);
            $('#normal_face_evaluation_l').prop('checked', true);
            faceEvaluation('n', 'l');
            faceEvaluation('n', 'r');
        } else {
            $('#normal_face_evaluation_r').prop('checked', false);
            $('#normal_face_evaluation_l').prop('checked', false);
            faceEvaluation('a', 'l');
            faceEvaluation('a', 'r');
        }
    });
    $("#normal_face_evaluation_r").change(function () {
        if (this.checked) {
            faceEvaluation('n', 'r');
        } else {
            faceEvaluation('a', 'r');
        }
    });
    $("#normal_face_evaluation_l").change(function () {
        if (this.checked) {
            faceEvaluation('n', 'l');
        } else {
            faceEvaluation('a', 'l');
        }
    });
    function faceEvaluation(status, side) {
        if (side === 'l' && status === 'n') {
            $('#face_peln_l').prop('checked', true);
            $('#face_olhn_l').prop('checked', true);
            $('#face_oren_l').prop('checked', true);
            $('#face_bgln_l').prop('checked', true);
            $('#face_mandn_l').prop('checked', true);
            $('#face_maxin_l').prop('checked', true);
            $('#face_narn_l').prop('checked', true);
            $('#face_orbn_l').prop('checked', true);
            $('#face_zign_l').prop('checked', true);
        } else if (side === 'r' && status === 'n') {
            $('#face_peln_r').prop('checked', true);
            $('#face_olhn_r').prop('checked', true);
            $('#face_oren_r').prop('checked', true);
            $('#face_bgln_r').prop('checked', true);
            $('#face_mandn_r').prop('checked', true);
            $('#face_maxin_r').prop('checked', true);
            $('#face_narn_r').prop('checked', true);
            $('#face_orbn_r').prop('checked', true);
            $('#face_zign_r').prop('checked', true);
        } else if (side === 'l' && status === 'a') {
            $('#face_peln_l').prop('checked', false);
            $('#face_olhn_l').prop('checked', false);
            $('#face_oren_l').prop('checked', false);
            $('#face_bgln_l').prop('checked', false);
            $('#face_mandn_l').prop('checked', false);
            $('#face_maxin_l').prop('checked', false);
            $('#face_narn_l').prop('checked', false);
            $('#face_orbn_l').prop('checked', false);
            $('#face_zign_l').prop('checked', false);
        } else if (side === 'r' && status === 'a') {
            $('#face_peln_r').prop('checked', false);
            $('#face_olhn_r').prop('checked', false);
            $('#face_oren_r').prop('checked', false);
            $('#face_bgln_r').prop('checked', false);
            $('#face_mandn_r').prop('checked', false);
            $('#face_maxin_r').prop('checked', false);
            $('#face_narn_r').prop('checked', false);
            $('#face_orbn_r').prop('checked', false);
            $('#face_zign_r').prop('checked', false);
        }
    }

    $("#normal_neck_evaluation").change(function () {
        if (this.checked) {
            $('#normal_neck_evaluation_r').prop('checked', true);
            $('#normal_neck_evaluation_l').prop('checked', true);
            neckEvaluation('n', 'l');
            neckEvaluation('n', 'r');
        } else {
            $('#normal_neck_evaluation_r').prop('checked', false);
            $('#normal_neck_evaluation_l').prop('checked', false);
            neckEvaluation('a', 'l');
            neckEvaluation('a', 'r');
        }
    });
    $("#normal_neck_evaluation_r").change(function () {
        if (this.checked) {
            neckEvaluation('n', 'r');
        } else {
            neckEvaluation('a', 'r');
        }
    });
    $("#normal_neck_evaluation_l").change(function () {
        if (this.checked) {
            neckEvaluation('n', 'l');
        } else {
            neckEvaluation('a', 'l');
        }
    });
    function neckEvaluation(status, side) {
        if (side === 'l' && status === 'n') {
            $('#neck_peln_l').prop('checked', true);
            $('#neck_pcn_l').prop('checked', true);
        } else if (side === 'r' && status === 'n') {
            $('#neck_peln_r').prop('checked', true);
            $('#neck_pcn_r').prop('checked', true);
        } else if (side === 'l' && status === 'a') {
            $('#neck_peln_l').prop('checked', false);
            $('#neck_pcn_l').prop('checked', false);
        } else if (side === 'r' && status === 'a') {
            $('#neck_peln_r').prop('checked', false);
            $('#neck_pcn_r').prop('checked', false);
        }
    }

    $("#normal_spine_evaluation").change(function () {
        if (this.checked) {
            $('#normal_spine_evaluation_r').prop('checked', true);
            $('#normal_spine_evaluation_l').prop('checked', true);
            spineEvaluation('n', 'l');
            spineEvaluation('n', 'r');
        } else {
            $('#normal_spine_evaluation_r').prop('checked', false);
            $('#normal_spine_evaluation_l').prop('checked', false);
            spineEvaluation('a', 'l');
            spineEvaluation('a', 'r');
        }
    });
    $("#normal_spine_evaluation_r").change(function () {
        if (this.checked) {
            spineEvaluation('n', 'r');
        } else {
            spineEvaluation('a', 'r');
        }
    });
    $("#normal_spine_evaluation_l").change(function () {
        if (this.checked) {
            spineEvaluation('n', 'l');
        } else {
            spineEvaluation('a', 'l');
        }
    });
    function spineEvaluation(status, side) {
        if (side === 'l' && status === 'n') {
            $('#spine_ccn_l').prop('checked', true);
            $('#spine_ctn_l').prop('checked', true);
        } else if (side === 'r' && status === 'n') {
            $('#spine_ccn_r').prop('checked', true);
            $('#spine_ctn_r').prop('checked', true);
        } else if (side === 'l' && status === 'a') {
            $('#spine_ccn_l').prop('checked', false);
            $('#spine_ctn_l').prop('checked', false);
        } else if (side === 'r' && status === 'a') {
            $('#spine_ccn_r').prop('checked', false);
            $('#spine_ctn_r').prop('checked', false);
        }
    }

    $("#normal_chest_evaluation").change(function () {
        if (this.checked) {
            $('#normal_chest_evaluation_r').prop('checked', true);
            $('#normal_chest_evaluation_l').prop('checked', true);
            chestEvaluation('n', 'l');
            chestEvaluation('n', 'r');
        } else {
            $('#normal_chest_evaluation_r').prop('checked', false);
            $('#normal_chest_evaluation_l').prop('checked', false);
            chestEvaluation('a', 'l');
            chestEvaluation('a', 'r');
        }
    });
    $("#normal_chest_evaluation_r").change(function () {
        if (this.checked) {
            chestEvaluation('n', 'r');
        } else {
            chestEvaluation('a', 'r');
        }
    });
    $("#normal_chest_evaluation_l").change(function () {
        if (this.checked) {
            chestEvaluation('n', 'l');
        } else {
            chestEvaluation('a', 'l');
        }
    });
    function chestEvaluation(status, side) {
        if (side === 'l' && status === 'n') {
            $('#chest_iedn_l').prop('checked', true);
            $('#chest_percn_l').prop('checked', true);
            $('#chest_auscn_l').prop('checked', true);
        } else if (side === 'r' && status === 'n') {
            $('#chest_iedn_r').prop('checked', true);
            $('#chest_percn_r').prop('checked', true);
            $('#chest_auscn_r').prop('checked', true);
        } else if (side === 'l' && status === 'a') {
            $('#chest_iedn_l').prop('checked', false);
            $('#chest_percn_l').prop('checked', false);
            $('#chest_auscn_l').prop('checked', false);
        } else if (side === 'r' && status === 'a') {
            $('#chest_iedn_r').prop('checked', false);
            $('#chest_percn_r').prop('checked', false);
            $('#chest_auscn_r').prop('checked', false);
        }
    }

    $("#normal_belly_evaluation").change(function () {
        if (this.checked) {
            $('#normal_belly_evaluation_r').prop('checked', true);
            $('#normal_belly_evaluation_l').prop('checked', true);
            bellyEvaluation('n', 'l');
            bellyEvaluation('n', 'r');
        } else {
            $('#normal_belly_evaluation_r').prop('checked', false);
            $('#normal_belly_evaluation_l').prop('checked', false);
            bellyEvaluation('a', 'l');
            bellyEvaluation('a', 'r');
        }
    });
    $("#normal_belly_evaluation_r").change(function () {
        if (this.checked) {
            bellyEvaluation('n', 'r');
        } else {
            bellyEvaluation('a', 'r');
        }
    });
    $("#normal_belly_evaluation_l").change(function () {
        if (this.checked) {
            bellyEvaluation('n', 'l');
        } else {
            bellyEvaluation('a', 'l');
        }
    });
    function bellyEvaluation(status, side) {
        if (side === 'l' && status === 'n') {
            $('#belly_iedn_l').prop('checked', true);
            $('#belly_percn_l').prop('checked', true);
            $('#belly_auscn_l').prop('checked', true);
        } else if (side === 'r' && status === 'n') {
            $('#belly_iedn_r').prop('checked', true);
            $('#belly_percn_r').prop('checked', true);
            $('#belly_auscn_r').prop('checked', true);
        } else if (side === 'l' && status === 'a') {
            $('#belly_iedn_l').prop('checked', false);
            $('#belly_percn_l').prop('checked', false);
            $('#belly_auscn_l').prop('checked', false);
        } else if (side === 'r' && status === 'a') {
            $('#belly_iedn_r').prop('checked', false);
            $('#belly_percn_r').prop('checked', false);
            $('#belly_auscn_r').prop('checked', false);
        }
    }

    $("#normal_pelvis_evaluation").change(function () {
        if (this.checked) {
            $('#normal_pelvis_evaluation_r').prop('checked', true);
            $('#normal_pelvis_evaluation_l').prop('checked', true);
            pelvisEvaluation('n', 'l');
            pelvisEvaluation('n', 'r');
        } else {
            $('#normal_pelvis_evaluation_r').prop('checked', false);
            $('#normal_pelvis_evaluation_l').prop('checked', false);
            pelvisEvaluation('a', 'l');
            pelvisEvaluation('a', 'r');
        }
    });
    $("#normal_pelvis_evaluation_r").change(function () {
        if (this.checked) {
            pelvisEvaluation('n', 'r');
        } else {
            pelvisEvaluation('a', 'r');
        }
    });
    $("#normal_pelvis_evaluation_l").change(function () {
        if (this.checked) {
            pelvisEvaluation('n', 'l');
        } else {
            pelvisEvaluation('a', 'l');
        }
    });
    function pelvisEvaluation(status, side) {
        if (side === 'l' && status === 'n') {
            $('#pelvis_peln_l').prop('checked', true);
            $('#pelvis_palpn_l').prop('checked', true);
        } else if (side === 'r' && status === 'n') {
            $('#pelvis_peln_r').prop('checked', true);
            $('#pelvis_palpn_r').prop('checked', true);
        } else if (side === 'l' && status === 'a') {
            $('#pelvis_peln_l').prop('checked', false);
            $('#pelvis_palpn_l').prop('checked', false);
        } else if (side === 'r' && status === 'a') {
            $('#pelvis_peln_r').prop('checked', false);
            $('#pelvis_palpn_r').prop('checked', false);
        }
    }

    $("#normal_prg_evaluation").change(function () {
        if (this.checked) {
            $('#normal_prg_evaluation_r').prop('checked', true);
            $('#normal_prg_evaluation_l').prop('checked', true);
            prgEvaluation('n', 'l');
            prgEvaluation('n', 'r');
        } else {
            $('#normal_prg_evaluation_r').prop('checked', false);
            $('#normal_prg_evaluation_l').prop('checked', false);
            prgEvaluation('a', 'l');
            prgEvaluation('a', 'r');
        }
    });
    $("#normal_prg_evaluation_r").change(function () {
        if (this.checked) {
            prgEvaluation('n', 'r');
        } else {
            prgEvaluation('a', 'r');
        }
    });
    $("#normal_prg_evaluation_l").change(function () {
        if (this.checked) {
            prgEvaluation('n', 'l');
        } else {
            prgEvaluation('a', 'l');
        }
    });
    function prgEvaluation(status, side) {
        if (side === 'l' && status === 'n') {
            $('#prg_pern_l').prop('checked', true);
            $('#prg_penn_l').prop('checked', true);
            $('#prg_testn_l').prop('checked', true);
            $('#prg_vulvn_l').prop('checked', true);
        } else if (side === 'r' && status === 'n') {
            $('#prg_pern_r').prop('checked', true);
            $('#prg_penn_r').prop('checked', true);
            $('#prg_testn_r').prop('checked', true);
            $('#prg_vulvn_r').prop('checked', true);
        } else if (side === 'l' && status === 'a') {
            $('#prg_pern_l').prop('checked', false);
            $('#prg_penn_l').prop('checked', false);
            $('#prg_testn_l').prop('checked', false);
            $('#prg_vulvn_l').prop('checked', false);
        } else if (side === 'r' && status === 'a') {
            $('#prg_pern_r').prop('checked', false);
            $('#prg_penn_r').prop('checked', false);
            $('#prg_testn_r').prop('checked', false);
            $('#prg_vulvn_r').prop('checked', false);
        }
    }

    $("#normal_ortop_evaluation").change(function () {
        if (this.checked) {
            $('#normal_ortop_evaluation_rs').prop('checked', true);
            $('#normal_ortop_evaluation_ri').prop('checked', true);
            $('#normal_ortop_evaluation_ls').prop('checked', true);
            $('#normal_ortop_evaluation_li').prop('checked', true);
            ortopEvaluation('n', 'ls');
            ortopEvaluation('n', 'li');
            ortopEvaluation('n', 'rs');
            ortopEvaluation('n', 'ri');
        } else {
            $('#normal_ortop_evaluation_rs').prop('checked', false);
            $('#normal_ortop_evaluation_ri').prop('checked', false);
            $('#normal_ortop_evaluation_ls').prop('checked', false);
            $('#normal_ortop_evaluation_li').prop('checked', false);
            ortopEvaluation('a', 'ls');
            ortopEvaluation('a', 'li');
            ortopEvaluation('a', 'rs');
            ortopEvaluation('a', 'ri');
        }
    });
    $("#normal_ortop_evaluation_rs").change(function () {
        if (this.checked) {
            ortopEvaluation('n', 'rs');
        } else {
            ortopEvaluation('a', 'rs');
        }
    });
    $("#normal_ortop_evaluation_ri").change(function () {
        if (this.checked) {
            ortopEvaluation('n', 'ri');
        } else {
            ortopEvaluation('a', 'ri');
        }
    });
    $("#normal_ortop_evaluation_ls").change(function () {
        if (this.checked) {
            ortopEvaluation('n', 'ls');
        } else {
            ortopEvaluation('a', 'ls');
        }
    });
    $("#normal_ortop_evaluation_li").change(function () {
        if (this.checked) {
            ortopEvaluation('n', 'li');
        } else {
            ortopEvaluation('a', 'li');
        }
    });
    function ortopEvaluation(status, side) {
        if (side === 'ls' && status === 'n') {
            $('#ortop_clavn_ls').prop('checked', true);
            $('#ortop_ombn_ls').prop('checked', true);
            $('#ortop_brcn_ls').prop('checked', true);
            $('#ortop_cotn_ls').prop('checked', true);
            $('#ortop_antn_ls').prop('checked', true);
            $('#ortop_punn_ls').prop('checked', true);
            $('#ortop_maon_ls').prop('checked', true);
        } else if (side === 'li' && status === 'n') {
            $('#ortop_quadn_li').prop('checked', true); //
            $('#ortop_coxn_li').prop('checked', true); //
            $('#ortop_joen_li').prop('checked', true); //
            $('#ortop_pern_li').prop('checked', true);
            $('#ortop_torn_li').prop('checked', true);
            $('#ortop_pen_li').prop('checked', true);
        } else if (side === 'rs' && status === 'n') {
            $('#ortop_clavn_rs').prop('checked', true);
            $('#ortop_ombn_rs').prop('checked', true);
            $('#ortop_brcn_rs').prop('checked', true);
            $('#ortop_cotn_rs').prop('checked', true);
            $('#ortop_antn_rs').prop('checked', true);
            $('#ortop_punn_rs').prop('checked', true);
            $('#ortop_maon_rs').prop('checked', true);
        } else if (side === 'ri' && status === 'n') {
            $('#ortop_quadn_ri').prop('checked', true); //
            $('#ortop_coxn_ri').prop('checked', true); //
            $('#ortop_joen_ri').prop('checked', true); //
            $('#ortop_pern_ri').prop('checked', true);
            $('#ortop_torn_ri').prop('checked', true);
            $('#ortop_pen_ri').prop('checked', true);
        } else if (side === 'li' && status === 'a') {
            $('#ortop_quadn_li').prop('checked', false);
            $('#ortop_coxn_li').prop('checked', false);
            $('#ortop_joen_li').prop('checked', false);
            $('#ortop_pern_li').prop('checked', false);
            $('#ortop_torn_li').prop('checked', false);
            $('#ortop_pen_li').prop('checked', false);
        } else if (side === 'ls' && status === 'a') {
            $('#ortop_clavn_ls').prop('checked', false);
            $('#ortop_ombn_ls').prop('checked', false);
            $('#ortop_brcn_ls').prop('checked', false);
            $('#ortop_cotn_ls').prop('checked', false);
            $('#ortop_antn_ls').prop('checked', false);
            $('#ortop_punn_ls').prop('checked', false);
            $('#ortop_maon_ls').prop('checked', false);
        } else if (side === 'ri' && status === 'a') {
            $('#ortop_quadn_ri').prop('checked', false);
            $('#ortop_coxn_ri').prop('checked', false);
            $('#ortop_joen_ri').prop('checked', false);
            $('#ortop_pern_ri').prop('checked', false);
            $('#ortop_torn_ri').prop('checked', false);
            $('#ortop_pen_ri').prop('checked', false);
        } else if (side === 'rs' && status === 'a') {
            $('#ortop_clavn_rs').prop('checked', false);
            $('#ortop_ombn_rs').prop('checked', false);
            $('#ortop_brcn_rs').prop('checked', false);
            $('#ortop_cotn_rs').prop('checked', false);
            $('#ortop_antn_rs').prop('checked', false);
            $('#ortop_punn_rs').prop('checked', false);
            $('#ortop_maon_rs').prop('checked', false);
        }
    }
</script>