<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    function __construct() {
        parent::__construct();
        date_default_timezone_set('America/Sao_Paulo');
        $this->user = $this->session->userdata('user');
        $this->load->model('Config_Model');
        $this->load->model('People_Model');
        $this->load->model('Faq_Model');
    }

    function encode($data) {
        print_r($this->encrypt->encode($data));
        exit;
    }

    function index() {
        if (isset($this->user['logged']) && $this->user['logged']) {
            $this->patients();
        } else {
            $data['title'] = 'Login';
            $data['content'] = 'home/login';
            $this->load->view('layouts/none', $data);
        }
    }

    function patients() {
        if ($this->user['logged']) {
            $this->load->model('People_Model');
            $data['patients'] = $this->People_Model->get_patients();
            foreach ($data['patients'] as $key => $p) {
                $data['patients'][$key]['use_services'] = $this->People_Model->get_patient_passages($p['id']);
            }
            $data['title'] = 'Pacientes';
            $data['content'] = 'home/patients';
            $this->load->view('layouts/default', $data);
        } else {
            $data['title'] = 'Login';
            $data['content'] = 'home/login';
            $this->load->view('layouts/none', $data);
        }
    }

    function load_select_professionals() {
        $data = $this->input->post();
        $professionals = $this->Config_Model->get_especific_professionals($data);
        echo json_encode(array('professionals' => $professionals));
        exit;
    }

    function filter_patients() {
        $page = $this->input->post('page');
        $filter = $this->input->post('filter');
        if ($this->user['logged']) {
            $this->load->model('People_Model');
            $data['patients'] = $this->People_Model->get_patients_filtered($filter, $page);
            foreach ($data['patients'] as $key => $p) {
                $data['patients'][$key]['use_services'] = $this->People_Model->get_patient_passages($p['id']);
            }
            $data['num_rows'] = $this->People_Model->get_count_patients_filtered($filter)->num_rows() / 10;

            foreach ($data['patients'] as $key => $p) {
                if (isset($p['forms'])) {
                    $data['patients'][$key]['forms'] = explode('||||', $p['forms']);
                }
            }
            $data['title'] = '';
            $data['content'] = 'home/patients';
            $this->load->view('layouts/default', $data);
        } else {
            $data['title'] = 'Login';
            $data['content'] = 'home/login';
            $this->load->view('layouts/none', $data);
        }
    }

    // INSTITUTIONS

    function institutions() {
        if ($this->user['logged']) {
            $data['institutions'] = $this->Config_Model->get_institutions();
            $data['title'] = '';
            $data['content'] = 'config/institutions';
            $this->load->view('layouts/default', $data);
        } else {
            $data['title'] = 'Login';
            $data['content'] = 'home/login';
            $this->load->view('layouts/none', $data);
        }
    }

    function delete_institution() {
        $id = $this->input->post('id');
        $status = $this->Config_Model->delete_institution($id);
        echo json_encode(array('status' => $status));
        exit;
    }

    function view_institution() {
        $id = $this->input->post('id');
        $institution = $this->Config_Model->get_institution($id);
        if (isset($institution)) {
            $status = 'OK';
        } else {
            $status = 'NOK';
        }
        echo json_encode(array('status' => $status, 'institution' => $institution));
        exit;
    }

    function save_institution() {
        $institution = $this->input->post();
        if ($institution['id'] == "") {
            unset($institution['id']);
            $status = $this->Config_Model->save_new_institution($institution);
        } else {
            $institution_id = $institution['id'];
            unset($institution['id']);
            $status = $this->Config_Model->edit_institution($institution, $institution_id);
        }
        echo json_encode(array('status' => $status));
        exit;
    }

    // SECTORS

    function sectors() {
        if ($this->user['logged']) {
            $data['sectors'] = $this->Config_Model->get_sectors();
            $data['institutions'] = $this->Config_Model->get_institutions();
            $data['title'] = '';
            $data['content'] = 'config/sectors';
            $this->load->view('layouts/default', $data);
        } else {
            $data['title'] = 'Login';
            $data['content'] = 'home/login';
            $this->load->view('layouts/none', $data);
        }
    }

    function delete_sector() {
        $id = $this->input->post('id');
        $status = $this->Config_Model->delete_sector($id);
        echo json_encode(array('status' => $status));
        exit;
    }

    function view_sector() {
        $id = $this->input->post('id');
        $sector = $this->Config_Model->get_sector($id);
        if (isset($sector)) {
            $status = 'OK';
        } else {
            $status = 'NOK';
        }
        echo json_encode(array('status' => $status, 'sector' => $sector));
        exit;
    }

    function save_sector() {
        $sector = $this->input->post();
        if ($sector['id'] == "") {
            unset($sector['id']);
            $status = $this->Config_Model->save_new_sector($sector);
        } else {
            $sector_id = $sector['id'];
            unset($sector['id']);
            $status = $this->Config_Model->edit_sector($sector, $sector_id);
        }
        echo json_encode(array('status' => $status));
        exit;
    }

    // BEDROOMS

    function bedrooms() {
        if ($this->user['logged']) {
            $data['bedrooms'] = $this->Config_Model->get_bedrooms();
            $data['sectors'] = $this->Config_Model->get_sectors();
            $data['title'] = '';
            $data['content'] = 'config/bedrooms';
            $this->load->view('layouts/default', $data);
        } else {
            $data['title'] = 'Login';
            $data['content'] = 'home/login';
            $this->load->view('layouts/none', $data);
        }
    }

    function delete_bedroom() {
        $id = $this->input->post('id');
        $status = $this->Config_Model->delete_bedroom($id);
        echo json_encode(array('status' => $status));
        exit;
    }

    function view_bedroom() {
        $id = $this->input->post('id');
        $bedroom = $this->Config_Model->get_bedroom($id);
        if (isset($bedroom)) {
            $status = 'OK';
        } else {
            $status = 'NOK';
        }
        echo json_encode(array('status' => $status, 'bedroom' => $bedroom));
        exit;
    }

    function save_bedroom() {
        $bedroom = $this->input->post();
        if ($bedroom['id'] == "") {
            unset($bedroom['id']);
            $status = $this->Config_Model->save_new_bedroom($bedroom);
        } else {
            $bedroom_id = $bedroom['id'];
            unset($bedroom['id']);
            $status = $this->Config_Model->edit_bedroom($bedroom, $bedroom_id);
        }
        echo json_encode(array('status' => $status));
        exit;
    }

    // BEDROOMS

    function beds() {
        if ($this->user['logged']) {
            $data['beds'] = $this->Config_Model->get_beds();
            $data['bedrooms'] = $this->Config_Model->get_bedrooms();
            $data['title'] = '';
            $data['content'] = 'config/beds';
            $this->load->view('layouts/default', $data);
        } else {
            $data['title'] = 'Login';
            $data['content'] = 'home/login';
            $this->load->view('layouts/none', $data);
        }
    }

    function delete_bed() {
        $id = $this->input->post('id');
        $status = $this->Config_Model->delete_bed($id);
        echo json_encode(array('status' => $status));
        exit;
    }

    function view_bed() {
        $id = $this->input->post('id');
        $bed = $this->Config_Model->get_bed($id);
        if (isset($bed)) {
            $status = 'OK';
        } else {
            $status = 'NOK';
        }
        echo json_encode(array('status' => $status, 'bed' => $bed));
        exit;
    }

    function save_bed() {
        $bed = $this->input->post();
        if ($bed['id'] == "") {
            unset($bed['id']);
            $status = $this->Config_Model->save_new_bed($bed);
        } else {
            $bed_id = $bed['id'];
            unset($bed['id']);
            $status = $this->Config_Model->edit_bed($bed, $bed_id);
        }
        echo json_encode(array('status' => $status));
        exit;
    }

    // TEAMS

    function teams() {
        if ($this->user['logged']) {
            $data['teams'] = $this->Config_Model->get_teams();
            $data['institutions'] = $this->Config_Model->get_institutions();
            $data['title'] = '';
            $data['content'] = 'config/teams';
            $this->load->view('layouts/default', $data);
        } else {
            $data['title'] = 'Login';
            $data['content'] = 'home/login';
            $this->load->view('layouts/none', $data);
        }
    }

    function delete_team() {
        $id = $this->input->post('id');
        $status = $this->Config_Model->delete_team($id);
        echo json_encode(array('status' => $status));
        exit;
    }

    function view_team() {
        $id = $this->input->post('id');
        $team = $this->Config_Model->get_team($id);
        if (isset($team)) {
            $status = 'OK';
        } else {
            $status = 'NOK';
        }
        echo json_encode(array('status' => $status, 'team' => $team));
        exit;
    }

    function save_team() {
        $team = $this->input->post();
        if ($team['id'] == "") {
            unset($team['id']);
            $status = $this->Config_Model->save_new_team($team);
        } else {
            $team_id = $team['id'];
            unset($team['id']);
            $status = $this->Config_Model->edit_team($team, $team_id);
        }
        echo json_encode(array('status' => $status));
        exit;
    }

    // PROFESSIONALS

    function professionals() {
        if ($this->user['logged']) {
            $data['professionals'] = $this->Config_Model->get_professionals();
            $data['teams'] = $this->Config_Model->get_teams();
            $data['title'] = '';
            $data['content'] = 'config/professionals';
            $this->load->view('layouts/default', $data);
        } else {
            $data['title'] = 'Login';
            $data['content'] = 'home/login';
            $this->load->view('layouts/none', $data);
        }
    }

    function delete_professional() {
        $id = $this->input->post('id');
        $status = $this->Config_Model->delete_professional($id);
        echo json_encode(array('status' => $status));
        exit;
    }

    function view_professional() {
        $id = $this->input->post('id');
        $professional = $this->Config_Model->get_professional($id);
        if (isset($professional)) {
            $status = 'OK';
        } else {
            $status = 'NOK';
        }
        echo json_encode(array('status' => $status, 'professional' => $professional));
        exit;
    }

    function save_professional() {
        $professional = $this->input->post();

        if ($professional['id'] == "") {

            $person['status'] = $professional['status'];
            $person['name'] = $professional['name'];
            $person['sus_card'] = $professional['sus_card'];
            $person['birthdate'] = $professional['birthdate'];
            $person['gender'] = $professional['gender'];
            $person['skin_color'] = $professional['skin_color'];
            $person['marital_status'] = $professional['marital_status'];
            $person['mother_name'] = $professional['mother_name'];
            $person['nationality'] = $professional['nationality'];
            $person['home_town'] = $professional['home_town'];
            $person['home_town_state'] = $professional['home_town_state'];
            $person_id = $this->People_Model->save_new_patient($person);

            $user['status'] = $professional['status'];
            $user['email'] = $professional['email'];
            $user['password'] = $professional['password'];
            $user['username'] = $professional['username'];
            $user['person_id'] = $person_id;
            $user_id = $this->People_Model->save_new_user($user);

            $team_member['status'] = $professional['status'];
            $team_member['team_id'] = $professional['team_id'];
            $team_member['person_id'] = $person_id;
            $team_member['hierarchy'] = $professional['hierarchy'];
            $team_member['specialty'] = $professional['specialty'];
            $team_member['class_document_type'] = $professional['class_document_type'];
            $team_member['class_document_number'] = $professional['class_document_number'];
            $status = $this->Config_Model->save_new_professional($team_member);
        } else {

            $team_member['status'] = $professional['status'];
            $team_member['team_id'] = $professional['team_id'];
            $team_member['person_id'] = $professional['person_id'];
            $team_member['hierarchy'] = $professional['hierarchy'];
            $team_member['specialty'] = $professional['specialty'];
            $team_member['class_document_type'] = $professional['class_document_type'];
            $team_member['class_document_number'] = $professional['class_document_number'];
            $team_member_id = $professional['id'];
            $status = $this->Config_Model->edit_professional($team_member, $team_member_id);

            $person['status'] = $professional['status'];
            $person['name'] = $professional['name'];
            $person['sus_card'] = $professional['sus_card'];
            $person['birthdate'] = $professional['birthdate'];
            $person['gender'] = $professional['gender'];
            $person['skin_color'] = $professional['skin_color'];
            $person['marital_status'] = $professional['marital_status'];
            $person['mother_name'] = $professional['mother_name'];
            $person['nationality'] = $professional['nationality'];
            $person['home_town'] = $professional['home_town'];
            $person['home_town_state'] = $professional['home_town_state'];
            $person_id = $professional['person_id'];
            $status = $this->People_Model->edit_patient($person, $person_id);

            $user['status'] = $professional['status'];
            $user['email'] = $professional['email'];
            $user['password'] = $professional['password'];
            $user['username'] = $professional['username'];
            $user['person_id'] = $professional['person_id'];
            $user_id = $professional['user_id'];
            $status = $this->People_Model->edit_user($user, $user_id);
        }
        echo json_encode(array('status' => $status));
        exit;
    }

    // IN OUT

    function login() {
        $data['title'] = 'Login';
        $data['content'] = 'home/login';
        $this->load->view('layouts/none', $data);
    }

    function logout() {
        $this->session->sess_destroy();
        $data['title'] = '';
        $data['content'] = 'home/login';
        $this->load->view('layouts/none', $data);
    }

    // PACIENTES

    function delete_patient() {
        $id = $this->encrypt->decode($this->input->post('id'));
        $status = $this->People_Model->delete_patient($id);
        echo json_encode(array('status' => $status));
        exit;
    }

    function view_patient() {
        $id = $this->encrypt->decode($this->input->post('id'));
        $people = $this->People_Model->get_person($id);
        if (isset($people)) {
            $status = 'OK';
        } else {
            $status = 'NOK';
        }
        echo json_encode(array('status' => $status, 'people' => $people));
        exit;
    }

    function save_patient() {
        $patient = $this->input->post();

        if (empty($patient['id'])) {
            unset($patient['id']);
            $status = $this->People_Model->save_new_patient($patient);
            if ($status !== 'NOK') {
                $status = 'OK';
            }
        } else {
            $patient_id = $this->encrypt->decode($patient['id']);
            unset($patient['id']);
            $status = $this->People_Model->edit_patient($patient, $patient_id);
        }
        echo json_encode(array('status' => $status));
        exit;
    }

//
//    public function load_addresses($id_encoded = NULL) {
//        if ($id_encoded == NULL) {
//            $id_encoded = $this->session->userdata('patient')['id'];
//        }
//        $id = $this->encrypt->decode($id_encoded);
//        $data['addresses'] = $this->People_Model->get_addresses($id);
//        $data['content'] = 'home/addresses';
//        $this->load->view('layouts/none', $data);
//    }
//
//    public function load_contacts($id_encoded = NULL) {
//        if ($id_encoded == NULL) {
//            $id_encoded = $this->session->userdata('patient')['id'];
//        }
//        $id = $this->encrypt->decode($id_encoded);
//        $data['contacts'] = $this->People_Model->get_contacts($id);
//        $data['content'] = 'home/contacts';
//        $this->load->view('layouts/none', $data);
//    }
    //FAQ

    public function faq() {
        if ($this->user['logged']) {
            $data['perguntas'] = $this->Faq_Model->get_questions();
            $data['content'] = 'home/faq';
            $this->load->view('layouts/default', $data);
        } else {
            $this->index();
        }
    }

    public function faq_response($msg, $aux) {
        if ($this->user['logged']) {
            $data['perguntas'] = $this->Faq_Model->get_questions();
            $data['content'] = 'home/faq';
            $data['msg'] = $msg;
            $data['aux'] = $aux;

            $this->load->view('layouts/default', $data);
        } else {
            $this->index();
        }
    }

    public function new_question() {
        $question = $this->input->post('form-question');
        $answer = $this->input->post('form-answer');

        if ($question !== '' && $answer !== '') {

            $data = array(
                'question' => $question,
                'answer' => $answer,
            );
            if ($this->Faq_Model->verify_question($data['question']) == 0) {
                $this->Faq_Model->set_questions($data);
                $this->faq_response('Pergunta adcionada com sucesso', 'alert-success');
            } else
                $this->faq_response('Ja existe esta pergunta, favor verifique as perguntasa abaixo', 'alert-warning');
        }
        else {
            $this->faq();
        }
    }

    public function edit_question() {
        $question = $this->input->post('form-question');
        $answer = $this->input->post('form-answer');

        if ($question !== '' && $answer !== '') {
            $id = $this->input->post('form-id_question');
            $data = array(
                'question' => $question,
                'answer' => $answer,
            );

            $result = $this->Faq_Model->edit_questions($data, $id);
            if ($result > 0) {
                $this->faq_response('Pergunta editada com sucesso', 'alert-success');
            } else {
                $this->faq_response('Erro ao editar pergunta', 'alert-danger');
            }
        } else {
            $this->faq();
        }
    }

    public function rem_question() {
        $id = $this->input->post('form-id_question');

        $result = $this->Faq_Model->remove_question($id);
        if ($result > 0) {
            $this->faq_response('Pergunta deletada com sucesso', 'alert-success');
        } else {
            $this->faq_response('Erro ao apagar pergunta', 'alert-danger');
        }
    }

}
