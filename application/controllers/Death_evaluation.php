<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Death_evaluation extends CI_Controller {

    function __construct() {
        parent::__construct();
        date_default_timezone_set('America/Sao_Paulo');
        $this->user = $this->session->userdata('user');
        $this->patient = $this->session->userdata('patient');
        $this->load->model('Death_evaluation_Model');
        $this->load->model('Hospitalization_Model');
        $this->load->model('Attendance_Model');
        $this->load->model('Config_Model');
        $this->load->model('Attendance_Hospital_Model');
        $this->load->model('People_Model');
    }

    function index() {
        if ($this->user['logged']) {
            $data['title'] = lang('death_evaluation');
            $use_service_id = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);

            // checa se tem avaliacao de morte
            $data['de'] = $this->Death_evaluation_Model->get_death_evaluation($use_service_id);
            if (isset($data['de'])) {
                $data['content'] = 'death_evaluation/resume';
            } else {
                $data['content'] = 'death_evaluation/add_edit';
            }
            // checa a condicao do paciente
            $has_hospitalization = $this->Attendance_Hospital_Model->get_patient_condition_like($use_service_id, 'internado');
            if ($has_hospitalization == 'OK') {
                $data['hospitalized'] = 1;
            } else {
                $data['hospitalized'] = 0;
            }



            //Pegando dados dos mecanismos de trauma ---PREHOSPITALAR----
            $data['mechanisms'] = $this->Death_evaluation_Model->get_prehospital_mechanisms($use_service_id);

            //Pegando dados de sinais vitais ---PREHOSPITALAR---
            $data['vital_signs'] = $this->Attendance_Model->get_attendance_vital_signs($use_service_id, 'scene');

            //Pegando dados da regulacao ---PREHOSPITALAR---
            $data['regulation'] = $this->Attendance_Model->get_attendance_regulation($use_service_id, 'scene');

            //Pegando dados de procedimentos realizados no ---PREHOSPITAlAR---
            $data['procedures_scene'] = $this->Attendance_Model->get_attendance_trauma_procedures($use_service_id, 'scene');

            //Pegando dados de sinais vitais ---ADMISSAO---
            $data['vital_signs_admissao'] = $this->Attendance_Model->get_attendance_vital_signs($use_service_id, 'hospital');

            //Pegando dados de procedimentos realizados no ---ADMISSAo---
            $data['procedures_hospital'] = $this->Attendance_Model->get_attendance_trauma_procedures($use_service_id, 'scene'); // TROCAR DEPOIS O SCENE POR HOSPITAL

            //Pegando dados de sinais vitais ---ADMISSAO---
            $data['vital_signs_hospitalization'] = $this->Attendance_Model->get_attendance_vital_signs($use_service_id, 'hospitalization');

            //pegando dados da condicao do paciente para blocode obito
            $data['patient_condition_list'] = $this->Attendance_Hospital_Model->get_patient_condition_list($use_service_id);
            
            $data['admission_date'] = $this->Death_evaluation_Model->get_patient_passages($use_service_id);
            
            foreach ($data['patient_condition_list'] as $pcl):
                
                if($pcl['exit_type'] == 'obito'){
                    $data['local_obito'] = $pcl['sent_to'];
                    $data['data_obito'] = $pcl['moment_admission_f'];
                    $data['data_obito_u'] = $pcl['moment_admission'];
                }
                
            endforeach;
            //calcula classificacao do obito
            $data_inicial = $data['admission_date']['created_sf'];
            $data_final = $data['data_obito_u'];

            $diferenca = strtotime($data_final) - strtotime($data_inicial);

            $horas = floor($diferenca / (60 * 60));
            $data['total_dias'] = floor($horas/24);
                if($horas <= 0)
                    $data['classification'] = "Morte ao chegar";
                else if($horas > 0 && $horas < 24)
                    $data['classification'] = "Morte antes de 24 horas";
                else if($horas > 24 && $horas <= 72)
                    $data['classification'] = "Morte entre 24 a 72 horas";
                else if($horas > 72 && $horas <= 168){
                    $data['classification'] = "Morte entre o 3ª e o 7ª dia";
                }
                else if($horas > 168 && $horas <= 360)
                    $data['classification'] = "Morte entre 7ª e o 15ª dia";
                else if($horas > 360)
                    $data['classification'] = "Morte após 15 dias";
            

            $data['medicines'] = $this->Attendance_Model->get_attendance_history_medicines($use_service_id, 'hospital');
            $data['neoplasms'] = $this->Attendance_Model->get_attendance_history_neoplasms($use_service_id, 'hospital');
            $data['allergies'] = $this->Attendance_Model->get_attendance_history_allergies($use_service_id, 'hospital');
            $data['surgeries'] = $this->Attendance_Model->get_attendance_history_surgeries($use_service_id, 'hospital');

            $data['cfg_allergies'] = $this->Config_Model->get_history_allergies('secondary_assessment');
            $data['cfg_medicines'] = $this->Config_Model->get_history_medicines('secondary_assessment');
            $data['cfg_surgeries'] = $this->Config_Model->get_history_surgeries('secondary_assessment');
            $data['history_complements'] = $this->Attendance_Model->get_attendance_history($use_service_id, 'hospital');





            $data['content'] = 'death_evaluation/add_edit';
            $this->load->view('layouts/default_patient', $data);




            // pegamos os dados para calcular os indicadores (TRISS, RTS, ISS)
            // $patient = $this->People_Model->get_person_by_use_service($use_service_id);
            // $date1 = date_create(date("Y-m-d"));
            // $date2 = date_create(date($patient['birthdate']));
            // $patient['age'] = date_diff($date2, $date1);
            // $patient['age'] = $patient['age']->format('%y');

            // $signals = $this->Attendance_Model->get_last_vital_signs($use_service_id);
            
            // $head = $this->Attendance_Hospital_Model->get_ais_diagnosys_region($use_service_id, 'cabeca');
            // $face = $this->Attendance_Hospital_Model->get_ais_diagnosys_region($use_service_id, 'face');
            // $ches = $this->Attendance_Hospital_Model->get_ais_diagnosys_region($use_service_id, 'torax');
            // $abdo = $this->Attendance_Hospital_Model->get_ais_diagnosys_region($use_service_id, 'abdome');
            // $limb = $this->Attendance_Hospital_Model->get_ais_diagnosys_region($use_service_id, 'extremidade');
            // $skin = $this->Attendance_Hospital_Model->get_ais_diagnosys_region($use_service_id, 'pele');
            
            // $data['trisscalc']['age'] = $patient['age'];
            // $data['trisscalc']['resprate'] = $signals['fr'];
            // $data['trisscalc']['sysbp'] = $signals['pa'];
            // $data['trisscalc']['coma'] = $signals['glasgow_score'];
            // $data['trisscalc']['head'] = $head ? $head['fci'] : 0;
            // $data['trisscalc']['face'] = $face ? $face['fci'] : 0;
            // $data['trisscalc']['ches'] = $ches ? $ches['fci'] : 0;
            // $data['trisscalc']['abdo'] = $abdo ? $abdo['fci'] : 0;
            // $data['trisscalc']['limb'] = $limb ? $limb['fci'] : 0;
            // $data['trisscalc']['skin'] = $skin ? $skin['fci'] : 0;

        } else {
            $data['title'] = 'Login';
            $data['content'] = 'home/login';
            $this->load->view('layouts/none', $data);
        }
    }

    function save_death_evaluation() {
        $data = $this->input->post();

        if ($data['autopsy'] == 'true') {
            $data['autopsy'] = 1;
        } else {
            $data['autopsy'] = 0;
        }
        if ($data['conclusive_iml'] == 'true') {
            $data['conclusive_iml'] = 1;
        } else {
            $data['conclusive_iml'] = 0;
        }
        if ($data['found_gravity'] == 'true') {
            $data['found_gravity'] = 1;
        } else {
            $data['found_gravity'] = 0;
        }
        if ($data['bac'] == 'true') {
            $data['bac'] = 1;
        } else {
            $data['bac'] = 0;
        }
        if ($data['conclusive_death_evaluation'] == 'true') {
            $data['conclusive_death_evaluation'] = 1;
        } else {
            $data['conclusive_death_evaluation'] = 0;
        }
        if ($data['deficiencies_none'] == 'true') {
            $data['deficiencies_none'] = 1;
        } else {
            $data['deficiencies_none'] = 0;
        }
        if ($data['approach_airways'] == 'true') {
            $data['approach_airways'] = 1;
        } else {
            $data['approach_airways'] = 0;
        }
        if ($data['bleeding_control'] == 'true') {
            $data['bleeding_control'] = 1;
        } else {
            $data['bleeding_control'] = 0;
        }
        if ($data['volemic_reposition'] == 'true') {
            $data['volemic_reposition'] = 1;
        } else {
            $data['volemic_reposition'] = 0;
        }
        if ($data['trauma_approach_system'] == 'true') {
            $data['trauma_approach_system'] = 1;
        } else {
            $data['trauma_approach_system'] = 0;
        }
        if ($data['cti_approach'] == 'true') {
            $data['cti_approach'] = 1;
        } else {
            $data['cti_approach'] = 0;
        }
        if ($data['delay_treatment'] == 'true') {
            $data['delay_treatment'] = 1;
        } else {
            $data['delay_treatment'] = 0;
        }
        if ($data['infection_related_problems'] == 'true') {
            $data['infection_related_problems'] = 1;
        } else {
            $data['infection_related_problems'] = 0;
        }
        if ($data['deficiencies_documentation'] == 'true') {
            $data['deficiencies_documentation'] = 1;
        } else {
            $data['deficiencies_documentation'] = 0;
        }
        if ($data['other_deficiencies'] == 'true') {
            $data['other_deficiencies'] = 1;
        } else {
            $data['other_deficiencies'] = 0;
        }
        if ($data['delay_decision'] == 'true') {
            $data['delay_decision'] = 1;
        } else {
            $data['delay_decision'] = 0;
        }
        if ($data['technical_difficulties'] == 'true') {
            $data['technical_difficulties'] = 1;
        } else {
            $data['technical_difficulties'] = 0;
        }
        if ($data['equipments'] == 'true') {
            $data['equipments'] = 1;
        } else {
            $data['equipments'] = 0;
        }
        if ($data['diagnosis_st'] == 'true') {
            $data['diagnosis_st'] = 1;
        } else {
            $data['diagnosis_st'] = 0;
        }
        if ($data['insufficient_surgical_technique'] == 'true') {
            $data['insufficient_surgical_technique'] = 1;
        } else {
            $data['insufficient_surgical_technique'] = 0;
        }
        if ($data['excess_crystalloid'] == 'true') {
            $data['excess_crystalloid'] = 1;
        } else {
            $data['excess_crystalloid'] = 0;
        }
        if ($data['severe_acidosis'] == 'true') {
            $data['severe_acidosis'] = 1;
        } else {
            $data['severe_acidosis'] = 0;
        }
        if ($data['delay_blood_transfusion'] == 'true') {
            $data['delay_blood_transfusion'] = 1;
        } else {
            $data['delay_blood_transfusion'] = 0;
        }
        if ($data['skull'] == 'true') {
            $data['skull'] = 1;
        } else {
            $data['skull'] = 0;
        }
        if ($data['column'] == 'true') {
            $data['column'] = 1;
        } else {
            $data['column'] = 0;
        }
        if ($data['torax'] == 'true') {
            $data['torax'] = 1;
        } else {
            $data['torax'] = 0;
        }
        if ($data['abdome'] == 'true') {
            $data['abdome'] = 1;
        } else {
            $data['abdome'] = 0;
        }
        if ($data['pelve'] == 'true') {
            $data['pelve'] = 1;
        } else {
            $data['pelve'] = 0;
        }
        if ($data['members'] == 'true') {
            $data['members'] = 1;
        } else {
            $data['members'] = 0;
        }
        if ($data['monitoring'] == 'true') {
            $data['monitoring'] = 1;
        } else {
            $data['monitoring'] = 0;
        }
        if ($data['mechanical_ventilation_cti'] == 'true') {
            $data['mechanical_ventilation_cti'] = 1;
        } else {
            $data['mechanical_ventilation_cti'] = 0;
        }
        if ($data['shock_reversal'] == 'true') {
            $data['shock_reversal'] = 1;
        } else {
            $data['shock_reversal'] = 0;
        }
        if ($data['drug_use'] == 'true') {
            $data['drug_use'] = 1;
        } else {
            $data['drug_use'] = 0;
        }
        if ($data['surgical_decision'] == 'true') {
            $data['surgical_decision'] = 1;
        } else {
            $data['surgical_decision'] = 0;
        }
        if ($data['cti_hospitalization'] == 'true') {
            $data['cti_hospitalization'] = 1;
        } else {
            $data['cti_hospitalization'] = 0;
        }
        if ($data['delay_diagnosis_infectious_focus'] == 'true') {
            $data['delay_diagnosis_infectious_focus'] = 1;
        } else {
            $data['delay_diagnosis_infectious_focus'] = 0;
        }
        if ($data['delay_surgical_management_infectious_source'] == 'true') {
            $data['delay_surgical_management_infectious_source'] = 1;
        } else {
            $data['delay_surgical_management_infectious_source'] = 0;
        }
        if ($data['delayed_antibiotic_therapy'] == 'true') {
            $data['delayed_antibiotic_therapy'] = 1;
        } else {
            $data['delayed_antibiotic_therapy'] = 0;
        }
        if ($data['Incorrect_empirical_antibiotic_therapy'] == 'true') {
            $data['Incorrect_empirical_antibiotic_therapy'] = 1;
        } else {
            $data['Incorrect_empirical_antibiotic_therapy'] = 0;
        }
        if ($data['lack_microbiological_examination'] == 'true') {
            $data['lack_microbiological_examination'] = 1;
        } else {
            $data['lack_microbiological_examination'] = 0;
        }
        if ($data['vital_signs'] == 'true') {
            $data['vital_signs'] = 1;
        } else {
            $data['vital_signs'] = 0;
        }
        if ($data['exams'] == 'true') {
            $data['exams'] = 1;
        } else {
            $data['exams'] = 0;
        }
        if ($data['conducts'] == 'true') {
            $data['conducts'] = 1;
        } else {
            $data['conducts'] = 0;
        }
        if ($data['procedures'] == 'true') {
            $data['procedures'] = 1;
        } else {
            $data['procedures'] = 0;
        }
        if ($data['pre_hospital'] == 'true') {
            $data['pre_hospital'] = 1;
        } else {
            $data['pre_hospital'] = 0;
        }
        if ($data['trauma_room'] == 'true') {
            $data['trauma_room'] = 1;
        } else {
            $data['trauma_room'] = 0;
        }
        if ($data['operating_room'] == 'true') {
            $data['operating_room'] = 1;
        } else {
            $data['operating_room'] = 0;
        }
        if ($data['recovery'] == 'true') {
            $data['recovery'] = 1;
        } else {
            $data['recovery'] = 0;
        }
        if ($data['cti'] == 'true') {
            $data['cti'] = 1;
        } else {
            $data['cti'] = 0;
        }
        if ($data['semi_intensive'] == 'true') {
            $data['semi_intensive'] = 1;
        } else {
            $data['semi_intensive'] = 0;
        }
        if ($data['ward'] == 'true') {
            $data['ward'] = 1;
        } else {
            $data['ward'] = 0;
        }
        if ($data['inadequacy_system'] == 'true') {
            $data['inadequacy_system'] = 1;
        } else {
            $data['inadequacy_system'] = 0;
        }

        $data['use_service_id'] = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);

        $status = $this->Death_evaluation_Model->save_death_evaluation($data);
        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    function image_exams($type = 'edit') {
        if ($this->user['logged']) {
            $use_service_id = $this->encrypt->decode($this->patient['use_service_id']);
            $data['image_exams'] = $this->Hospitalization_Model->get_image_exams($use_service_id);
            $data['title'] = lang('attendance');
            $data['type'] = $type;
            $data['content'] = 'hospitalization/image_exams';
            $this->load->view('layouts/none', $data);
        } else {
            $data['title'] = 'Login';
            $data['content'] = 'home/login';
            $this->load->view('layouts/none', $data);
        }
    }

    function anatomical_pathological_exams($type = 'edit') {
        if ($this->user['logged']) {
            $use_service_id = $this->encrypt->decode($this->patient['use_service_id']);
            $data['anatomical_pathological_exams'] = $this->Hospitalization_Model->get_anatomical_pathological_exams($use_service_id);
            $data['title'] = lang('attendance');
            $data['type'] = $type;
            $data['content'] = 'hospitalization/anatomical_pathological_exams';
            $this->load->view('layouts/none', $data);
        } else {
            $data['title'] = 'Login';
            $data['content'] = 'home/login';
            $this->load->view('layouts/none', $data);
        }
    }

    function drugs($type = 'edit') {
        if ($this->user['logged']) {
            $use_service_id = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);
            $attendance_type = $this->session->userdata('patient')['is_trauma'];
            $data['admindrugs'] = $this->Attendance_Model->get_attendance_administered_drugs($use_service_id, 'all');
            $data['drugs'] = $this->Config_Model->get_drugs('intensive_care_unit');
            $data['title'] = lang('attendance');
            $data['type'] = $type;
            $data['content'] = 'hospitalization/drugs';
            $this->load->view('layouts/none', $data);
        } else {
            $data['title'] = 'Login';
            $data['content'] = 'home/login';
            $this->load->view('layouts/none', $data);
        }
    }

    function surgery_anesthesia($type = 'edit') {
        if ($this->user['logged']) {
            $use_service_id = $this->encrypt->decode($this->patient['use_service_id']);
            $data['title'] = lang('attendance');
            $data['c_surgeries'] = $this->Config_Model->get_surgeries('intensive_care_unit');
            $data['surgeries'] = $this->Hospitalization_Model->get_surgeries($use_service_id, 'all');
            foreach ($data['surgeries'] as $key => $p) {
                $data['surgeries'][$key]['team_members'] = $this->Hospitalization_Model->get_surgery_team($p['id']);
            }
            $data['professionals'] = $this->Config_Model->get_professionals();
            $data['type'] = $type;
            $data['content'] = 'hospitalization/surgery_anesthesia';
            $this->load->view('layouts/none', $data);
        } else {
            $data['title'] = 'Login';
            $data['content'] = 'home/login';
            $this->load->view('layouts/none', $data);
        }
    }

    function procedures($type = 'edit') {
        if ($this->user['logged']) {
            $use_service_id = $this->encrypt->decode($this->patient['use_service_id']);
            $data['title'] = lang('attendance');
            $data['attendance_type'] = $this->session->userdata('patient')['is_trauma'];
            $data['type'] = $type;


            $last_condition = $this->Attendance_Hospital_Model->get_last_patient_condition($use_service_id);
            if ($last_condition['exit_type'] == 'internado') {
                $data['procedures'] = $this->Hospitalization_Model->get_procedures($use_service_id, 'hospitalization');
                $data['hemos'] = $this->Hospitalization_Model->get_hemos($use_service_id, 'hospitalization');
                $data['content'] = 'hospitalization/procedures';
            } else {
                $data['procedures'] = $this->Attendance_Model->get_attendance_trauma_procedures($use_service_id, 'all');
                $data['content'] = 'death_evaluation/procedures_old';
            }
            $data['type'] = $type;
            $this->load->view('layouts/none', $data);
        } else {
            $data['title'] = 'Login';
            $data['content'] = 'home/login';
            $this->load->view('layouts/none', $data);
        }
    }

        function hospital_biochemistry_exams($local) {
        if ($this->user['logged']) {
            $use_service_id = $this->encrypt->decode($this->patient['use_service_id']);
            $data['biochemistry_exams'] = $this->Attendance_Hospital_Model->get_biochemistry_exams($use_service_id, $local);
            $data['title'] = lang('attendance');
            $data['content'] = 'hospitalization/biochemistry_exams';
            $this->load->view('layouts/none', $data);
        } else {
            $data['title'] = 'Login';
            $data['content'] = 'home/login';
            $this->load->view('layouts/none', $data);
        }
    }

    function hospitalization_biochemistry_exams() {
        if ($this->user['logged']) {
            $use_service_id = $this->encrypt->decode($this->patient['use_service_id']);
            $data['biochemistry_exams'] = $this->Attendance_Hospital_Model->get_biochemistry_exams($use_service_id, 'hospitalization');
            $data['title'] = lang('attendance');
            $data['content'] = 'hospitalization/biochemistry_exams';
            $this->load->view('layouts/none', $data);
        } else {
            $data['title'] = 'Login';
            $data['content'] = 'home/login';
            $this->load->view('layouts/none', $data);
        }
    }

    

}

?>