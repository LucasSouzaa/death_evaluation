<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Prehospital_fixed extends CI_Controller {

    function __construct() {
        parent::__construct();
        date_default_timezone_set('America/Sao_Paulo');
        $this->user = $this->session->userdata('user');
        $this->patient = $this->session->userdata('patient');
        $this->load->model('Attendance_Model');
        $this->load->model('Config_Model');
    }

    function view($context = 'unit1') {
        $data['context'] = $context;
        $data['content'] = 'attendance_prehospital/fixed/resume';
        $this->load->view('layouts/default_patient', $data);
    }

    function edit($context = 'unit1') {
        $data['context'] = $context;
        $data['content'] = 'attendance_prehospital/fixed/add_edit';
        $this->load->view('layouts/default_patient', $data);
    }

    function regulation($type = 'edit') {
        $use_service_id = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);
        $data['regulation'] = $this->Attendance_Model->get_attendance_regulation($use_service_id, 'scene');

        if ($type == 'edit') {
            $data['content'] = 'attendance_prehospital/fixed/regulation';
        } else {
            $data['content'] = 'attendance_prehospital/fixed/regulation_view';
        }

        $this->load->view('layouts/none', $data);
    }

    function clinical($type = 'edit') {
        $use_service_id = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);
        $data['clinical'] = $this->Attendance_Model->get_attendance_clinical($use_service_id, 'scene');

        if ($type == 'edit') {
            $data['content'] = 'attendance_prehospital/fixed/clinical';
        } else {
            $data['content'] = 'attendance_prehospital/fixed/clinical_view';
        }
        $this->load->view('layouts/none', $data);
    }

    function mechanisms($type = 'edit') {
        $use_service_id = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);
        $data['mechanisms'] = $this->Attendance_Model->get_attendance_mechanisms($use_service_id, 'scene');

        if ($type == 'edit') {
            $data['content'] = 'attendance_prehospital/fixed/mechanisms';
        } else {
            $data['content'] = 'attendance_prehospital/fixed/mechanisms_view';
        }
        $this->load->view('layouts/none', $data);
    }

    function vital_signs($type = 'edit') {
        $use_service_id = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);
        $data['vital_signs'] = $this->Attendance_Model->get_attendance_vital_signs($use_service_id, 'hospital');

        if ($type == 'edit') {
            $data['content'] = 'attendance_prehospital/fixed/vital_signs';
        } else {
            $data['content'] = 'attendance_prehospital/fixed/vital_signs_view';
        }
        $this->load->view('layouts/none', $data);
    }

    function procedures($type = 'edit') {
        $use_service_id = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);
        $attendance_type = $this->encrypt->decode($this->session->userdata('patient')['is_trauma']);
        $data['procedures'] = $this->Attendance_Model->get_attendance_clinical_procedures($use_service_id, $attendance_type, 'scene');

        if ($type == 'edit') {
            $data['content'] = 'attendance_prehospital/fixed/procedures';
        } else {
            $data['content'] = 'attendance_prehospital/fixed/procedures_view';
        }

        $this->load->view('layouts/none', $data);
    }

    function complementary_exams($type = 'edit') {
        $use_service_id = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);

        if ($type == 'edit') {
            $data['content'] = 'attendance_prehospital/fixed/complementary_exams';
        } else {
            $data['content'] = 'attendance_prehospital/fixed/complementary_exams_view';
        }

        $this->load->view('layouts/none', $data);
    }

    function physical_exam($type = 'edit') {
        $use_service_id = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);

        if ($type == 'edit') {
            $data['content'] = 'attendance_prehospital/fixed/physical_exam';
        } else {
            $data['content'] = 'attendance_prehospital/fixed/physical_exam_view';
        }

        $this->load->view('layouts/none', $data);
    }

    function history($type = 'edit', $context = 'unit1') {
        $use_service_id = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);
        if ($type == 'edit') {
            $data['cfg_allergies'] = $this->Config_Model->get_history_allergies($context);
            $data['cfg_medicines'] = $this->Config_Model->get_history_medicines($context);
            $data['cfg_surgeries'] = $this->Config_Model->get_history_surgeries($context);
            $data['neoplasms'] = $this->Attendance_Model->get_attendance_history_neoplasms($use_service_id, $context);
            $data['allergies'] = $this->Attendance_Model->get_attendance_history_allergies($use_service_id, $context);
            $data['medicines'] = $this->Attendance_Model->get_attendance_history_medicines($use_service_id, $context);
            $data['surgeries'] = $this->Attendance_Model->get_attendance_history_surgeries($use_service_id, $context);
            $data['content'] = 'attendance_prehospital/fixed/history';
        } else {
            $data['content'] = 'attendance_prehospital/fixed/history_view';
        }

        $this->load->view('layouts/none', $data);
    }

    function drugs($type = 'edit') {
        $use_service_id = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);
        $data['cfg_drugs'] = $this->Config_Model->get_drugs('scene');
        $data['drugs'] = $this->Attendance_Model->get_attendance_administered_drugs($use_service_id, 'scene');

        if ($type == 'edit') {
            $data['content'] = 'attendance_prehospital/fixed/drugs';
        } else {
            $data['content'] = 'attendance_prehospital/fixed/drugs_view';
        }
        $this->load->view('layouts/none', $data);
    }

    function diagnostics($type = 'edit') {
        $use_service_id = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);
        $data['drugs'] = $this->Attendance_Model->get_attendance_regulation($use_service_id, 'scene');

        if ($type == 'edit') {
            $data['content'] = 'attendance_prehospital/fixed/diagnostics';
        } else {
            $data['content'] = 'attendance_prehospital/fixed/diagnostics_view';
        }
        $this->load->view('layouts/none', $data);
    }

    function route($type = 'edit') {
        $use_service_id = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);
        $data['routing'] = $this->Attendance_Model->get_attendance_routing($use_service_id, 'scene');
        if ($type == 'edit') {
            $data['content'] = 'attendance_prehospital/fixed/route';
        } else {
            $data['content'] = 'attendance_prehospital/fixed/route_view';
        }
        $this->load->view('layouts/none', $data);
    }

    //ok - checado
    function save_regulation() {
        $data = $this->input->post();
        $data['use_service_id'] = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);

        $status = $this->Attendance_Model->save_attendance_regulation($data);

        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    //ok - checado
    function save_vital_signs() {
        $data = $this->input->post();
        $data['use_service_id'] = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);

        foreach ($data as $key => $value) {
            if ($value == 'true') {
                $data[$key] = 1;
            } else if ($value == 'false') {
                $data[$key] = 0;
            }
        }
        $status = $this->Attendance_Model->save_attendance_vital_signs($data);
        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    //ok - checado
    function save_mechanisms() {
        $data = $this->input->post();
        $data['use_service_id'] = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);

        foreach ($data as $key => $value) {
            if ($value == 'true') {
                $data[$key] = 1;
            } else if ($value == 'false') {
                $data[$key] = 0;
            }
        }

        $status = $this->Attendance_Model->save_attendance_mechanisms($data);

        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    //ok - checado
    function save_clinical() {
        $data = $this->input->post();
        $data['use_service_id'] = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);

        $status = $this->Attendance_Model->save_attendance_clinical($data);

        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    //ok - checado
    function add_administered_drug() {
        $data = $this->input->post();
        $data['use_service_id'] = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);

        $status = $this->Attendance_Model->save_attendance_administered_drugs($data);

        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    function load_administered_drugs($context) {
        $use_service_id = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);
        $data['drugs'] = $this->Attendance_Model->get_attendance_administered_drugs($use_service_id, $context);
        $data['content'] = 'prehospital/administered_drugs_list';
        $this->load->view('layouts/none', $data);
    }

    function delete_administered_drug() {
        $data = $this->input->post();

        $status = $this->Attendance_Model->remove_attendance_administered_drugs($data['id']);

        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    function save_routing() {
        $data = $this->input->post();
        $data['use_service_id'] = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);

        $status = $this->Attendance_Model->save_routing($data);

        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    function add_history_item($name) {
        $data = $this->input->post();
        $data['use_service_id'] = $this->encrypt->decode($this->patient['use_service_id']);
        $status = $this->Attendance_Model->add_history_item($name, $data);
        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    function add_history_item_cid($name) {
        $data = $this->input->post();
        $data['use_service_id'] = $this->encrypt->decode($this->patient['use_service_id']);
        $status = $this->Attendance_Model->add_history_item_cid($name, $data);
        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    function load_history_neoplasms($context) {
        $use_service_id = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);
        $data['neoplasms'] = $this->Attendance_Model->get_attendance_history_neoplasms($use_service_id, $context);
        $data['content'] = 'attendance_hospital/history/history_neoplasms_list';
        $this->load->view('layouts/none', $data);
    }

    function load_history_allergies($context) {
        $use_service_id = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);
        $data['allergies'] = $this->Attendance_Model->get_attendance_history_allergies($use_service_id, $context);
        $data['content'] = 'attendance_hospital/history/history_allergies_list';
        $this->load->view('layouts/none', $data);
    }

    function load_history_medicines($context) {
        $use_service_id = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);
        $data['medicines'] = $this->Attendance_Model->get_attendance_history_medicines($use_service_id, $context);
        $data['content'] = 'attendance_hospital/history/history_medicines_list';
        $this->load->view('layouts/none', $data);
    }

    function load_history_surgeries($context) {
        $use_service_id = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);
        $data['surgeries'] = $this->Attendance_Model->get_attendance_history_surgeries($use_service_id, $context);
        $data['content'] = 'attendance_hospital/history/history_surgeries_list';
        $this->load->view('layouts/none', $data);
    }

    //ok - checado
    function delete_history_item() {
        $data = $this->input->post();
        $status = $this->Attendance_Model->delete_history_item($data);
        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    function save_complementary_exams() {
        $data = $this->input->post();
        $data['use_service_id'] = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);

        foreach ($data as $key => $value) {
            if ($value == 'true') {
                $data[$key] = 1;
            } else if ($value == 'false') {
                $data[$key] = 0;
            }
        }
        $status = $this->Attendance_Model->save_attendance_complementary_exams($data);

        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

}
