<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Outpatients extends CI_Controller {

    function __construct() {
        parent::__construct();
        date_default_timezone_set('America/Sao_Paulo');
        $this->user = $this->session->userdata('user');
        $this->patient = $this->session->userdata('patient');
        $this->load->model('Outpatients_Model');
        $this->load->model('Hospitalization_Model');
        $this->load->model('Attendance_Hospital_Model');
    }

    function index() {
        if ($this->user['logged']) {
            $use_service_id = $this->encrypt->decode($this->patient['use_service_id']);
            $data['returns'] = $this->Outpatients_Model->get_returns($use_service_id);
            $data['final_condition'] = $this->Attendance_Hospital_Model->get_last_patient_condition($use_service_id);
            $data['title'] = lang('outpatients');
            $data['content'] = 'outpatients/add_edit';
            $this->load->view('layouts/default_patient', $data);
        } else {
            $data['title'] = 'Login';
            $data['content'] = 'home/login';
            $this->load->view('layouts/none', $data);
        }
    }

    function save_return() {
        $data = $this->input->post();
        $data['use_service_id'] = $data1['use_service_id'] = $this->encrypt->decode($this->patient['use_service_id']);
        // pega ultima condicao do paciente
        $last_condition = $this->Attendance_Hospital_Model->get_last_patient_condition($data['use_service_id']);
        // se tiver uma condicao anterior ele da um update no momento de saida
        if (!empty($last_condition)) {
            $status = $this->Attendance_Hospital_Model->update_patient_condition($last_condition['id']);
        }
        if (!$data['return_date']) {
            $data1['moment_admission'] = date("Y-m-d H:i:s");
        }else{
            $data1['moment_admission'] = $data['return_date'];
        }
        $data1['exit_type'] = $data['conduct'];
        
        $status = $this->Attendance_Hospital_Model->save_add_patient_condition($data1);
        if ($data['conduct'] === 'reinternado') {
            $session = $this->session->userdata();
            $session['patient']['rehospitalization'] = TRUE;
            $this->session->set_userdata($session);
        }
        $status = $this->Outpatients_Model->save_return($data);
        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

}

?>