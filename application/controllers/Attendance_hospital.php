<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Attendance_hospital extends CI_Controller {

    function __construct() {
        parent::__construct();
        date_default_timezone_set('America/Sao_Paulo');
        $this->user = $this->session->userdata('user');
        $this->patient = $this->session->userdata('patient');
        $this->load->model('Attendance_Hospital_Model');
        $this->load->model('Attendance_Model');
        $this->load->model('Attendance_Model');
        $this->load->model('Config_Model');
    }

//function index() {
//        if ($this->user['logged']) {
//            $data['title'] = lang('attendance');
//            $use_service_id = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);
//            $first_condition = $this->Attendance_Hospital_Model->get_first_patient_condition($use_service_id);
//
//            if (empty($first_condition)) {
//                $data['first_condition_empty'] = TRUE;
//            } else {
//                $data['first_condition_empty'] = FALSE;
//            }
//
//            $data['content'] = 'attendance_hospital/resume';
//            $this->load->view('layouts/default_patient', $data);
//        } else {
//            $data['title'] = 'Login';
//            $data['content'] = 'home/login';
//            $this->load->view('layouts/none', $data);
//        }
//    }

    function index() {
        if ($this->user['logged']) {

            // atualizamos a sessao de internacao com datas para puxar tudo
            $session = $this->session->userdata();
            $session['hospitalization']['hosp_id'] = '';
            $session['hospitalization']['moment_admission'] = '1900-01-01 00:00:00';
            $session['hospitalization']['moment_exit'] = '';
            $this->session->set_userdata($session);
            // sessao atualizada

            $data['title'] = lang('attendance');
            // checa a condicao inicial do paciente
            // se nao houver da opcao do usuario preencher
            $use_service_id = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);
            $first_condition = $this->Attendance_Hospital_Model->get_first_patient_condition($use_service_id);

            if (empty($first_condition)) {
                $data['first_condition_empty'] = TRUE;
            } else {
                $data['first_condition_empty'] = FALSE;
            }

            $data['content'] = 'attendance_hospital/add_edit';
            $this->load->view('layouts/default_patient', $data);
        } else {
            $data['title'] = 'Login';
            $data['content'] = 'home/login';
            $this->load->view('layouts/none', $data);
        }
    }

    function primary_evaluation() {
        if ($this->user['logged']) {
            $data['title'] = lang('attendance');
            $data['attendance_type'] = $this->session->userdata('patient')['is_trauma'];
            $use_service_id = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);
            $data['eval'] = $this->Attendance_Hospital_Model->get_primary_evaluation($use_service_id, 'hospital');

            $data['procedures'] = $this->Attendance_Model->get_attendance_trauma_procedures($use_service_id, 'hospital');
            $data['procedures_unit1'] = $this->Attendance_Model->get_attendance_trauma_procedures($use_service_id, 'unit1');
            $data['procedures_scene'] = $this->Attendance_Model->get_attendance_trauma_procedures($use_service_id, 'scene');
            $data['vit_sig'] = $this->Attendance_Model->get_attendance_vital_signs($use_service_id, 'hospital');

            $data['content'] = 'attendance_hospital/primary_evaluation';
            $this->load->view('layouts/none', $data);
        } else {
            $data['title'] = 'Login';
            $data['content'] = 'home/login';
            $this->load->view('layouts/none', $data);
        }
    }

    function save_primary_evaluation() {
        $data = $this->input->post();

        foreach ($data as $key => $value) {
            if ($value == 'true') {
                $data[$key] = 1;
            } else if ($value == 'false') {
                $data[$key] = 0;
            }
        }

        $vit_sig['context'] = 'hospital';
        $vit_sig['pa'] = $data['pa'];
        $vit_sig['fc'] = $data['fc'];
        $vit_sig['fr'] = $data['fr'];
        $vit_sig['sato2'] = $data['sato2'];
        $vit_sig['axillary_temperature'] = $data['axillary_temperature'];
        $vit_sig['esophageal_temperature'] = $data['esophageal_temperature'];
        $vit_sig['glasgow_motor_response'] = $data['glasgow_motor_response'];
        $vit_sig['glasgow_visual_response'] = $data['glasgow_visual_response'];
        $vit_sig['glasgow_verbal_response'] = $data['glasgow_verbal_response'];
        $vit_sig['glasgow_score'] = $data['glasgow_score'];
        $vit_sig['pupillary_reflex_right'] = $data['pupillary_reflex_right'];
        $vit_sig['pupillary_reflex_left'] = $data['pupillary_reflex_left'];
        $vit_sig['right_anisocoria'] = $data['right_anisocoria'];
        $vit_sig['left_anisocoria'] = $data['left_anisocoria'];
        unset($data['pa']);
        unset($data['fc']);
        unset($data['fr']);
        unset($data['sato2']);
        unset($data['axillary_temperature']);
        unset($data['esophageal_temperature']);
        unset($data['glasgow_motor_response']);
        unset($data['glasgow_visual_response']);
        unset($data['glasgow_verbal_response']);
        unset($data['glasgow_score']);
        unset($data['pupillary_reflex_right']);
        unset($data['pupillary_reflex_left']);
        unset($data['right_anisocoria']);
        unset($data['left_anisocoria']);

        $vit_sig['context'] = $data['local_type'] = 'hospital';
        $data['use_service_id'] = $vit_sig['use_service_id'] = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);
        $status = $this->Attendance_Hospital_Model->save_primary_evaluation($data);
        $status = $this->Attendance_Model->save_attendance_vital_signs($vit_sig);
        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    function save_trauma_realized_procedures() {
        $data = $this->input->post();
        $data['use_service_id'] = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);
        foreach ($data as $key => $value) {
            if ($value == 'true') {
                $data[$key] = 1;
            } else if ($value == 'false') {
                $data[$key] = 0;
            }
        }

        $status = $this->Attendance_Model->save_attendance_trauma_procedures($data);
        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    function save_history() {
        $data = $this->input->post();
        $data['use_service_id'] = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);

        foreach ($data as $key => $value) {
            if ($value == 'true') {
                $data[$key] = 1;
            } else if ($value == 'false') {
                $data[$key] = 0;
            }
        }
        $status = $this->Attendance_Model->save_attendance_history($data);

        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    function add_assist_team() {
        $data = $this->input->post();
        $data['use_service_id'] = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);

        // checa se ja existe primeiro
        $check = $this->Attendance_Hospital_Model->select_assist_team($data);
        if (!empty($check)) {
            $id = $check['id'];
            $this->Attendance_Hospital_Model->update_assist_team($data);
        } else {
            $id = $this->Attendance_Hospital_Model->add_assist_team($data);
        }
        echo json_encode(array(
            'id' => $id
        ));
        exit;
    }

    function remove_assist_team_member() {
        $data = $this->input->post();
        $status = $this->Attendance_Hospital_Model->remove_assist_team_member($data);
        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    function secundary_evaluation() {
        if ($this->user['logged']) {
            $use_service_id = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);
            $data['medicines'] = $this->Attendance_Model->get_attendance_history_medicines($use_service_id, 'hospital');
            $data['neoplasms'] = $this->Attendance_Model->get_attendance_history_neoplasms($use_service_id, 'hospital');
            $data['allergies'] = $this->Attendance_Model->get_attendance_history_allergies($use_service_id, 'hospital');
            $data['surgeries'] = $this->Attendance_Model->get_attendance_history_surgeries($use_service_id, 'hospital');

            $data['cfg_allergies'] = $this->Config_Model->get_history_allergies('secondary_assessment');
            $data['cfg_medicines'] = $this->Config_Model->get_history_medicines('secondary_assessment');
            $data['cfg_surgeries'] = $this->Config_Model->get_history_surgeries('secondary_assessment');

            $data['history_complements'] = $this->Attendance_Model->get_attendance_history($use_service_id, 'hospital');

            $data['fisexam'] = $this->Attendance_Model->get_attendance_physical_exams($use_service_id, 'hospital');

            //pr($data['fisexam']);exit;
            // profissionais 
            $data['teaching_medical'] = $this->Config_Model->get_professionals_type('teaching_medical');
            $data['attending_physician'] = $this->Config_Model->get_professionals_type('attending_physician');
            $data['nurse'] = $this->Config_Model->get_professionals_type('nurse');
            $data['nursing_technician'] = $this->Config_Model->get_professionals_type('nursing_technician');
            $data['r1'] = $this->Config_Model->get_professionals_type('r1');
            $data['r2'] = $this->Config_Model->get_professionals_type('r2');
            $data['r3'] = $this->Config_Model->get_professionals_type('r3');

            // dados para equipe assistencial
            $data['supervisor_list'] = $this->Attendance_Hospital_Model->get_passage_assist_team($use_service_id, 'supervisor');
            $data['resident_list'] = $this->Attendance_Hospital_Model->get_passage_assist_team($use_service_id, 'resident');
            $data['enf_list'] = $this->Attendance_Hospital_Model->get_passage_assist_team($use_service_id, 'enf');
            $data['t_enf_list'] = $this->Attendance_Hospital_Model->get_passage_assist_team($use_service_id, 't_enf');

            //surgery_attending_physician
            $data['title'] = lang('attendance');
            $data['content'] = 'attendance_hospital/secundary_evaluation';
            $this->load->view('layouts/none', $data);
        } else {
            $data['title'] = 'Login';
            $data['content'] = 'home/login';
            $this->load->view('layouts/none', $data);
        }
    }

    function biochemistry_exams() {
        if ($this->user['logged']) {
            $use_service_id = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);
            $data['biochemistry_exams'] = $this->Attendance_Hospital_Model->get_biochemistry_exams($use_service_id, 'hospital');
            $data['title'] = lang('attendance');
            $data['content'] = 'attendance_hospital/biochemistry_exams';
            $this->load->view('layouts/none', $data);
        } else {
            $data['title'] = 'Login';
            $data['content'] = 'home/login';
            $this->load->view('layouts/none', $data);
        }
    }

    function save_biochemistry_exam() {
        $exams = $this->input->post();

        $data['use_service_id'] = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);
        $data['local_type'] = 'hospital';

        foreach ($exams['exams'] as $e) {
            $data['c_laboratory_exams_id'] = $e;
            $status = $this->Attendance_Hospital_Model->save_biochemistry_exams($data);
        }

        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    function delete_biochemistry_exam() {
        $id = $this->encrypt->decode($this->input->post('id'));
        $status = $this->Attendance_Hospital_Model->delete_biochemistry_exam($id);
        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    function save_bio_exam_result() {
        $result = $this->input->post();
        $status = $this->Attendance_Hospital_Model->save_bio_exam_result($result);
        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    function save_image_exam_result() {
        $result = $this->input->post();
        $status = $this->Attendance_Hospital_Model->save_image_exam_result($result);
        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    function image_exams() {
        if ($this->user['logged']) {
            $use_service_id = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);
            $data['image_exams'] = $this->Attendance_Hospital_Model->get_image_exams($use_service_id);
            $data['title'] = lang('attendance');
            $data['content'] = 'attendance_hospital/image_exams';
            $this->load->view('layouts/none', $data);
        } else {
            $data['title'] = 'Login';
            $data['content'] = 'home/login';
            $this->load->view('layouts/none', $data);
        }
    }

    function save_image_exam() {
        $exams = $this->input->post();
        $data['use_service_id'] = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);
        $data['local_type'] = 'hospital';

        foreach ($exams['exams'] as $e) {
            $data['c_radiological_exams_id'] = $e;
            $status = $this->Attendance_Hospital_Model->save_image_exams($data);
        }

        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    function delete_image_exam() {
        $id = $this->encrypt->decode($this->input->post('id'));
        $status = $this->Attendance_Hospital_Model->delete_image_exam($id);
        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    function diagnosys() {
        if ($this->user['logged']) {
            $data['aiss'] = $this->Config_Model->get_ais();
            $data['title'] = lang('attendance');
            $data['content'] = 'attendance_hospital/diagnosys/diagnosys';
            $this->load->view('layouts/none', $data);
        } else {
            $data['title'] = 'Login';
            $data['content'] = 'home/login';
            $this->load->view('layouts/none', $data);
        }
    }

    function conduct() {
        if ($this->user['logged']) {
            $use_service_id = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);
            $data['water_balance'] = $this->Attendance_Hospital_Model->get_water_balance($use_service_id, 'hospital');
            $data['evolution'] = $this->Attendance_Hospital_Model->get_evolution($use_service_id, 'hospital');
            $data['content'] = 'attendance_hospital/conduct/conduct';
            $data['title'] = lang('attendance');
            $this->load->view('layouts/none', $data);
        } else {
            $data['title'] = 'Login';
            $data['content'] = 'home/login';
            $this->load->view('layouts/none', $data);
        }
    }

    function get_ec_diagnosys_list() {
        if ($this->user['logged']) {
            $use_service_id = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);
            $data['ec_diagnosys_list'] = $this->Attendance_Hospital_Model->get_ec_diagnosys_list($use_service_id);
            $data['content'] = 'attendance_hospital/diagnosys/ec_diagnosys_list';
            $this->load->view('layouts/none', $data);
        } else {
            $data['title'] = 'Login';
            $data['content'] = 'home/login';
            $this->load->view('layouts/none', $data);
        }
    }

    function get_ais_diagnosys_list() {
        if ($this->user['logged']) {
            $use_service_id = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);
            $data['ais_diagnosys_list'] = $this->Attendance_Hospital_Model->get_ais_diagnosys_list($use_service_id);
            $data['content'] = 'attendance_hospital/diagnosys/ais_diagnosys_list';
            $this->load->view('layouts/none', $data);
        } else {
            $data['title'] = 'Login';
            $data['content'] = 'home/login';
            $this->load->view('layouts/none', $data);
        }
    }

    function get_i_diagnosys_list() {
        if ($this->user['logged']) {
            $use_service_id = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);
            $data['i_diagnosys_list'] = $this->Attendance_Hospital_Model->get_i_diagnosys_list($use_service_id);

            // monta regiao
            foreach ($data['i_diagnosys_list'] as $key => $c) {
                $category_cid_field = explode('S', $c['subcategory_cid_field_code']);

                if (isset($category_cid_field[1])) {

                    $category_cid_field = $category_cid_field[1];
                    $data['i_diagnosys_list'][$key]['region'] = '';

                    if ($category_cid_field >= 00 && $category_cid_field <= 010 || $category_cid_field >= 016 && $category_cid_field <= 021 || $category_cid_field >= 036 && $category_cid_field <= 049 || $category_cid_field >= 060 && $category_cid_field <= 099) {
                        //cabeca
                        $data['i_diagnosys_list'][$key]['region'] = $data['i_diagnosys_list'][$key]['region'] . ' - ' . 'Cabeça';
                    } else if ($category_cid_field >= 011 && $category_cid_field <= 015 || $category_cid_field >= 022 && $category_cid_field <= 035 || $category_cid_field >= 050 && $category_cid_field <= 059) {
                        //face
                        $data['i_diagnosys_list'][$key]['region'] = $data['i_diagnosys_list'][$key]['region'] . ' - ' . 'Face';
                    } else if ($category_cid_field >= 10 && $category_cid_field <= 179 || $category_cid_field >= 19 && $category_cid_field <= 199) {
                        //pescoco
                        $data['i_diagnosys_list'][$key]['region'] = $data['i_diagnosys_list'][$key]['region'] . ' - ' . 'Pescoço';
                    } else if ($category_cid_field >= 20 && $category_cid_field <= 239 || $category_cid_field >= 25 && $category_cid_field <= 299) {
                        //torax
                        $data['i_diagnosys_list'][$key]['region'] = $data['i_diagnosys_list'][$key]['region'] . ' - ' . 'Torax';
                    } else if ($category_cid_field >= 30 && $category_cid_field <= 399) {
                        //abdome
                        $data['i_diagnosys_list'][$key]['region'] = $data['i_diagnosys_list'][$key]['region'] . ' - ' . 'Abdome';
                    } else if ($category_cid_field >= 40 && $category_cid_field <= 499) {
                        //extremidade superior ombro braço
                        $data['i_diagnosys_list'][$key]['region'] = $data['i_diagnosys_list'][$key]['region'] . ' - ' . 'Ext Superior Ombro/Braço';
                    } else if ($category_cid_field >= 50 && $category_cid_field <= 599) {
                        //extremidade superior cotovelo antebraço
                        $data['i_diagnosys_list'][$key]['region'] = $data['i_diagnosys_list'][$key]['region'] . ' - ' . 'Ext Superior Cotovelo/Antebraço';
                    } else if ($category_cid_field >= 60 && $category_cid_field <= 699) {
                        //extremidade superior punho mao
                        $data['i_diagnosys_list'][$key]['region'] = $data['i_diagnosys_list'][$key]['region'] . ' - ' . 'Ext Superior Punho/Mão';
                    } else if ($category_cid_field >= 322 && $category_cid_field <= 325 || $category_cid_field >= 332 && $category_cid_field <= 337 || $category_cid_field >= 70 && $category_cid_field <= 71 || $category_cid_field >= 73 && $category_cid_field <= 731) {
                        //extremidade inferior quadril
                        $data['i_diagnosys_list'][$key]['region'] = $data['i_diagnosys_list'][$key]['region'] . ' - ' . 'Ext Inferior Quadril';
                    } else if ($category_cid_field >= 711 && $category_cid_field <= 729) {
                        //extremidade superior Coxa
                        $data['i_diagnosys_list'][$key]['region'] = $data['i_diagnosys_list'][$key]['region'] . ' - ' . 'Ext Inferior Coxa';
                    } else if ($category_cid_field >= 80 && $category_cid_field <= 899) {
                        //extremidade superior Perna joelho
                        $data['i_diagnosys_list'][$key]['region'] = $data['i_diagnosys_list'][$key]['region'] . ' - ' . 'Ext Inferior Perna/Joelho';
                    } else if ($category_cid_field >= 90 && $category_cid_field <= 999) {
                        //extremidade superior Coxa
                        $data['i_diagnosys_list'][$key]['region'] = $data['i_diagnosys_list'][$key]['region'] . ' - ' . 'Ext Inferior Tornozelo/Pé';
                    } else if ($category_cid_field >= 12 && $category_cid_field <= 136) {
                        //coluna cervical
                        $data['i_diagnosys_list'][$key]['region'] = $data['i_diagnosys_list'][$key]['region'] . ' - ' . 'Coluna cervical';
                    } else if ($category_cid_field >= 22 && $category_cid_field <= 221 || $category_cid_field >= 23 && $category_cid_field <= 233) {
                        //coluna toracica
                        $data['i_diagnosys_list'][$key]['region'] = $data['i_diagnosys_list'][$key]['region'] . ' - ' . 'Coluna toracica';
                    } else if ($category_cid_field >= 32 && $category_cid_field <= 321 || $category_cid_field >= 327 && $category_cid_field <= 331) {
                        //coluna lombar sacral
                        $data['i_diagnosys_list'][$key]['region'] = $data['i_diagnosys_list'][$key]['region'] . ' - ' . 'Coluna lombar sacral';
                    } else if ($category_cid_field >= 14 && $category_cid_field <= 146) {
                        //medula cervical
                        $data['i_diagnosys_list'][$key]['region'] = $data['i_diagnosys_list'][$key]['region'] . ' - ' . 'Medula cervical';
                    } else if ($category_cid_field >= 24 && $category_cid_field <= 242) {
                        //medula toracica
                        $data['i_diagnosys_list'][$key]['region'] = $data['i_diagnosys_list'][$key]['region'] . ' - ' . 'Medula toracica';
                    } else if ($category_cid_field >= 34 && $category_cid_field <= 346) {
                        //medula lombar sacral
                        $data['i_diagnosys_list'][$key]['region'] = $data['i_diagnosys_list'][$key]['region'] . ' - ' . 'Medula lombar sacral';
                    }
                } else {
                    $data['i_diagnosys_list'][$key]['region'] = 'Não definida';
                }
            }

            $data['content'] = 'attendance_hospital/diagnosys/i_diagnosys_list';
            $this->load->view('layouts/none', $data);
        } else {
            $data['title'] = 'Login';
            $data['content'] = 'home/login';
            $this->load->view('layouts/none', $data);
        }
    }

    function save_add_ec_cid() {
        $diagnosys = $this->input->post();
        $diagnosys['use_service_id'] = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);
        $status = $this->Attendance_Hospital_Model->save_add_ec_cid($diagnosys);
        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    function save_add_ais() {
        $diagnosys = $this->input->post();
        $diagnosys['use_service_id'] = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);
        $status = $this->Attendance_Hospital_Model->save_add_ais($diagnosys);
        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    function save_add_i_cid() {
        $diagnosys = $this->input->post();
        $diagnosys['use_service_id'] = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);
        $status = $this->Attendance_Hospital_Model->save_add_i_cid($diagnosys);
        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    function delete_ec_cid() {
        $id = $this->encrypt->decode($this->input->post('id'));
        $status = $this->Attendance_Hospital_Model->delete_ec_cid($id);
        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    function delete_ais() {
        $id = $this->encrypt->decode($this->input->post('id'));
        $status = $this->Attendance_Hospital_Model->delete_ais($id);
        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    function delete_i_cid() {
        $id = $this->encrypt->decode($this->input->post('id'));
        $status = $this->Attendance_Hospital_Model->delete_i_cid($id);
        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    function add_volemic_reposition() {
        $volemic_reposition = $this->input->post();
        $volemic_reposition['use_service_id'] = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);
        $volemic_reposition['local_type'] = 'hospital';
        if ($volemic_reposition['aptm'] == 'true') {
            $volemic_reposition['aptm'] = 1;
        } else {
            $volemic_reposition['aptm'] = 0;
        }
        $status = $this->Attendance_Hospital_Model->add_volemic_reposition($volemic_reposition);
        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    function get_volemic_reposition_list($volemic_reposition_type) {
        if ($this->user['logged']) {
            $use_service_id = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);
            $data['volemic_reposition_list'] = $this->Attendance_Hospital_Model->get_volemic_reposition_list($use_service_id, $volemic_reposition_type);
            $data['volemic_reposition_type'] = $volemic_reposition_type;
            $total = 0;
            foreach ($data['volemic_reposition_list'] as $value) {
                $total = $total + $value['solution_detail'];
            }
            if ($volemic_reposition_type == 'hemo' || $volemic_reposition_type == 'cristaloides') {
                $data['volemic_reposition_sum'] = $total;
            } else {
                $data['volemic_reposition_sum'] = null;
            }
            $data['content'] = 'attendance_hospital/conduct/volemic_reposition_list';
            $this->load->view('layouts/none', $data);
        } else {
            $data['title'] = 'Login';
            $data['content'] = 'home/login';
            $this->load->view('layouts/none', $data);
        }
    }

    function get_patient_condition_list() {
        if ($this->user['logged']) {
            $use_service_id = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);
            $data['patient_condition_list'] = $this->Attendance_Hospital_Model->get_patient_condition_list($use_service_id);
            $data['content'] = 'attendance_hospital/conduct/patient_condition_list';
            $this->load->view('layouts/none', $data);
        } else {
            $data['title'] = 'Login';
            $data['content'] = 'home/login';
            $this->load->view('layouts/none', $data);
        }
    }

    function delete_volemic_reposition() {
        $id = $this->encrypt->decode($this->input->post('id'));
        $status = $this->Attendance_Hospital_Model->delete_volemic_reposition($id);
        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    function save_evolution() {
        $data = $this->input->post();
        $data['use_service_id'] = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);
        $data['local_type'] = 'hospital';
        $status = $this->Attendance_Hospital_Model->save_evolution($data);
        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    function save_water_balance() {
        $data = $this->input->post();
        $data['use_service_id'] = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);
        $data['local_type'] = 'hospital';
        $status = $this->Attendance_Hospital_Model->save_water_balance($data);
        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    function save_physical_exams() {
        $data = $this->input->post();
        $data['use_service_id'] = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);

        foreach ($data as $key => $value) {
            if ($value == 'true') {
                $data[$key] = 1;
            } else if ($value == 'false') {
                $data[$key] = 0;
            }
        }
        $status = $this->Attendance_Model->save_attendance_physical_exams($data);

        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    function save_add_patient_condition() {
        $data = $this->input->post();
        $data['use_service_id'] = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);

        // pega ultima condicao do paciente
        $last_condition = $this->Attendance_Hospital_Model->get_last_patient_condition($data['use_service_id']);
        // se tiver uma condicao anterior ele da um update no momento de saida
        if (!empty($last_condition)) {
            $status = $this->Attendance_Hospital_Model->update_patient_condition($last_condition['id']);
        }
        // adiciona condicao nova
        $status = $this->Attendance_Hospital_Model->save_add_patient_condition($data);

        /*
         * Atualiza a session para as abas aparecerem
         */
        $session = $this->session->userdata();
        if ($data['exit_type'] == 'internado') {
            $session['patient']['death'] = FALSE;
            $session['patient']['hospitalization'] = TRUE;
            $session['patient']['rehospitalization'] = FALSE;
            $session['patient']['out'] = FALSE;
        } else if ($data['exit_type'] == 'obito') {
            $session['patient']['death'] = TRUE;
            $session['patient']['hospitalization'] = FALSE;
            $session['patient']['rehospitalization'] = FALSE;
            $session['patient']['out'] = FALSE;
        } else if ($data['exit_type'] == 'alta' || $data['exit_type'] == 'alta_a_pedido') {
            $session['patient']['death'] = FALSE;
            $session['patient']['hospitalization'] = TRUE;
            $session['patient']['rehospitalization'] = FALSE;
            $session['patient']['out'] = TRUE;
        } else if ($data['exit_type'] == 're-internado') {
            $session['patient']['death'] = FALSE;
            $session['patient']['hospitalization'] = TRUE;
            $session['patient']['rehospitalization'] = TRUE;
            $session['patient']['out'] = TRUE;
        } else {
            $session['patient']['death'] = FALSE;
            $session['patient']['hospitalization'] = FALSE;
            $session['patient']['rehospitalization'] = FALSE;
            $session['patient']['out'] = FALSE;
        }
        $this->session->set_userdata($session);

        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    function delete_patient_condition() {
        $id = $this->encrypt->decode($this->input->post('id'));
        $status = $this->Attendance_Hospital_Model->delete_patient_condition($id);
        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

}

?>