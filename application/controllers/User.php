<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    function __construct() {
        parent::__construct();
        date_default_timezone_set('America/Sao_Paulo');
        $this->user = $this->session->userdata('user');
//        $this->load->model('Faq_Model');
    }
    
    function profile() {
        if ($this->user['logged']) {
            $data['title'] = '';
            $this->load->model('User_model');
            $data['users'] = $this->User_model->get_user($this->user['id']);
            $data['content'] = 'user/profile';
            $this->load->view('layouts/default', $data);
            
        } else {
            $data['title'] = 'Login';
            $data['content'] = 'home/login';
            $this->load->view('layouts/none', $data);
        }
    }
    
}

?>
