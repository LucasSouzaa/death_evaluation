<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Prehospital_mobile extends CI_Controller {

    function __construct() {
        parent::__construct();
        date_default_timezone_set('America/Sao_Paulo');
        $this->user = $this->session->userdata('user');
        $this->patient = $this->session->userdata('patient');
        $this->load->model('Attendance_Model');
        $this->load->model('Config_Model');
    }

    function view() {
        $data['content'] = 'attendance_prehospital/mobile/resume';
        $this->load->view('layouts/default_patient', $data);
    }

    function edit() {
        $data['content'] = 'attendance_prehospital/mobile/add_edit';
        $this->load->view('layouts/default_patient', $data);
    }

    function regulation($type = 'edit') {
        $use_service_id = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);
        $data['regulation'] = $this->Attendance_Model->get_attendance_regulation($use_service_id, 'scene');

        if ($type == 'edit') {
            $data['content'] = 'attendance_prehospital/mobile/regulation';
        } else {
            $data['content'] = 'attendance_prehospital/mobile/regulation_view';
        }

        $this->load->view('layouts/none', $data);
    }

    function clinical($type = 'edit') {
        $use_service_id = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);
        $data['clinical'] = $this->Attendance_Model->get_attendance_clinical($use_service_id, 'scene');

        if ($type == 'edit') {
            $data['content'] = 'attendance_prehospital/mobile/clinical';
        } else {
            $data['content'] = 'attendance_prehospital/mobile/clinical_view';
        }

        $this->load->view('layouts/none', $data);
    }

    function mechanisms($type = 'edit') {
        $use_service_id = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);
        $data['mechanisms'] = $this->Attendance_Model->get_attendance_mechanisms($use_service_id, 'scene');

        if ($type == 'edit') {
            $data['content'] = 'attendance_prehospital/mobile/mechanisms';
        } else {
            $data['content'] = 'attendance_prehospital/mobile/mechanisms_view';
        }

        $this->load->view('layouts/none', $data);
    }

    function vital_signs($type = 'edit') {
        $use_service_id = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);
        $data['vital_signs'] = $this->Attendance_Model->get_attendance_vital_signs($use_service_id, 'hospital');

        if ($type == 'edit') {
            $data['content'] = 'attendance_prehospital/mobile/vital_signs';
        } else {
            $data['content'] = 'attendance_prehospital/mobile/vital_signs_view';
        }

        $this->load->view('layouts/none', $data);
    }

    function procedures($type = 'edit') {
        $use_service_id = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);
        $attendance_type = $this->encrypt->decode($this->session->userdata('patient')['is_trauma']);
        $data['procedures'] = $this->Attendance_Model->get_attendance_clinical_procedures($use_service_id, $attendance_type, 'scene');

        if ($type == 'edit') {
            $data['content'] = 'attendance_prehospital/mobile/procedures';
        } else {
            $data['content'] = 'attendance_prehospital/mobile/procedures_view';
        }

        $this->load->view('layouts/none', $data);
    }

    function drugs($type = 'edit') {
        $use_service_id = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);
        $data['cfg_drugs'] = $this->Config_Model->get_drugs('scene');
        $data['drugs'] = $this->Attendance_Model->get_attendance_administered_drugs($use_service_id, 'scene');

        if ($type == 'edit') {
            $data['content'] = 'attendance_prehospital/mobile/drugs';
        } else {
            $data['content'] = 'attendance_prehospital/mobile/drugs_view';
        }

        $this->load->view('layouts/none', $data);
    }

    function diagnostics($type = 'edit') {
        $use_service_id = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);
        $data['drugs'] = $this->Attendance_Model->get_attendance_regulation($use_service_id, 'scene');

        if ($type == 'edit') {
            $data['content'] = 'attendance_prehospital/mobile/diagnostics';
        } else {
            $data['content'] = 'attendance_prehospital/mobile/diagnostics_view';
        }

        $this->load->view('layouts/none', $data);
    }

    function route($type = 'edit') {
        $use_service_id = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);
        $data['routing'] = $this->Attendance_Model->get_attendance_routing($use_service_id, 'scene');

        if ($type == 'edit') {
            $data['content'] = 'attendance_prehospital/mobile/route';
        } else {
            $data['content'] = 'attendance_prehospital/mobile/route_view';
        }

        $this->load->view('layouts/none', $data);
    }

    //ok - checado
    function save_regulation() {
        $data = $this->input->post();
        $data['use_service_id'] = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);

        $status = $this->Attendance_Model->save_attendance_regulation($data);

        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    //ok - checado
    function save_vital_signs() {
        $data = $this->input->post();
        $data['use_service_id'] = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);

        if ($data['pupillary_reflex'] == 'true') {
            $data['pupillary_reflex'] = 1;
            if ($data['isocoria'] == 'true') {
                $data['isocoria'] = 1;
            } else {
                $data['isocoria'] = 0;
            }
            if ($data['anisocoria'] == 'true') {
                $data['anisocoria'] = 1;
                if ($data['right_anisocoria'] == 'true') {
                    $data['right_anisocoria'] = 1;
                } else {
                    $data['right_anisocoria'] = 0;
                }
                if ($data['left_anisocoria'] == 'true') {
                    $data['left_anisocoria'] = 1;
                } else {
                    $data['left_anisocoria'] = 0;
                }
            } else {
                $data['anisocoria'] = 0;
            }
        } else {
            $data['pupillary_reflex'] = 0;
        }

        $status = $this->Attendance_Model->save_attendance_vital_signs($data);

        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    //ok - checado
    function save_mechanisms() {
        $data = $this->input->post();
        $data['use_service_id'] = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);
        foreach ($data as $key => $value) {
            if ($value == 'true') {
                $data[$key] = 1;
            } else if ($value == 'false') {
                $data[$key] = 0;
            }
        }

        $status = $this->Attendance_Model->save_attendance_mechanisms($data);

        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    //ok - checado
    function save_clinical() {
        $data = $this->input->post();
        $data['use_service_id'] = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);

        $status = $this->Attendance_Model->save_attendance_clinical($data);

        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }


    //ok - checado
    function save_routing() {
        $data = $this->input->post();
        $data['use_service_id'] = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);

        $status = $this->Attendance_Model->save_routing($data);

        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    //ok - checado
    function save_clinical_realized_procedures() {
        $data = $this->input->post();
        $data['use_service_id'] = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);
        foreach ($data as $key => $value) {
            if ($value == 'true') {
                $data[$key] = 1;
            } else if ($value == 'false') {
                $data[$key] = 0;
            }
        }
        $status = $this->Attendance_Model->save_attendance_clinical_procedures($data);

        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

}
