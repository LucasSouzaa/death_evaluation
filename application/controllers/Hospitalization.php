<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Hospitalization extends CI_Controller {

    function __construct() {
        parent::__construct();
        date_default_timezone_set('America/Sao_Paulo');
        $this->user = $this->session->userdata('user');
        $this->patient = $this->session->userdata('patient');
        $this->load->model('Hospitalization_Model');
        $this->load->model('Attendance_Model');
        $this->load->model('Config_Model');
        $this->load->model('Attendance_Hospital_Model');
    }

    function index() {
        if ($this->user['logged']) {
            $data['title'] = lang('attendance');
            $data['content'] = 'hospitalization/add_edit';
            $this->load->view('layouts/default_patient', $data);
        } else {
            $data['title'] = 'Login';
            $data['content'] = 'home/login';
            $this->load->view('layouts/none', $data);
        }
    }

    function all_hospitalizations() {
        if ($this->user['logged']) {
            $data['title'] = lang('attendance');
            $use_service_id = $this->encrypt->decode($this->patient['use_service_id']);
            $data['hospitalizations'] = $this->Hospitalization_Model->get_hospitalizations($use_service_id);
            $data['content'] = 'hospitalization/all_hospitalizations';
            $this->load->view('layouts/default_patient', $data);
        } else {
            $data['title'] = 'Login';
            $data['content'] = 'home/login';
            $this->load->view('layouts/none', $data);
        }
    }

    function open_hospitalization() {
        $session = $this->session->userdata();
        $moment_admission = date_create($this->input->post('moment_admission'));
        $moment_admission = date_format($moment_admission, 'Y-m-d H:i:s');
        $session['hospitalization']['hosp_id'] = $this->input->post('hosp_id');
        $session['hospitalization']['moment_admission'] = $moment_admission;
        if ($this->input->post('moment_exit') == '') {
            $moment_exit = '';
        } else {
            $moment_exit = date_create($this->input->post('moment_exit'));
            $moment_exit = date_format($moment_exit, 'Y-m-d H:i:s');
        }
        $session['hospitalization']['moment_exit'] = $moment_exit;
        $this->session->set_userdata($session);
        echo json_encode(array(
            'status' => 'OK'
        ));
        exit;
    }

    function visits_resume() {
        $use_service_id = $this->encrypt->decode($this->patient['use_service_id']);
        $data = $this->Hospitalization_Model->get_visits_resume($use_service_id);
        $data['title'] = lang('attendance');
        $data['content'] = 'hospitalization/visits_resume';
        $this->load->view('layouts/none', $data);
    }

    function clinical_evolution() {
        if ($this->user['logged']) {
            $data['title'] = lang('attendance');
            $data['content'] = 'hospitalization/clinical_evolution';
            $this->load->view('layouts/none', $data);
        } else {
            $data['title'] = 'Login';
            $data['content'] = 'home/login';
            $this->load->view('layouts/none', $data);
        }
    }

    function save_visit_resume() {
        $data = $this->input->post();
        $data['use_service_id'] = $this->encrypt->decode($this->patient['use_service_id']);
        $status = $this->Hospitalization_Model->save_visit_resume($data);
        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    function use_service_beds() {
        if ($this->user['logged']) {
            $data['title'] = lang('attendance');
            $use_service_id = $this->encrypt->decode($this->patient['use_service_id']);
            $data['beds'] = $this->Config_Model->get_beds();
            $data['passages_beds'] = $this->Hospitalization_Model->get_use_service_beds($use_service_id);
            $data['content'] = 'hospitalization/use_service_beds';
            $this->load->view('layouts/none', $data);
        } else {
            $data['title'] = 'Login';
            $data['content'] = 'home/login';
            $this->load->view('layouts/none', $data);
        }
    }

    function save_use_service_bed() {
        $data = $this->input->post();
        $data['use_service_id'] = $this->encrypt->decode($this->patient['use_service_id']);

        // se tiver leito ele da update no momento de saida dele
        //if (!empty($last_bed)) {
        // $this->Hospitalization_Model->update_use_service($data['use_service_id']);
        //}

        $status = $this->Hospitalization_Model->save_use_service_bed($data);
        // pegamos o ultimo leito que ele estava
        $last_bed = $this->Hospitalization_Model->get_last_use_service_bed($data['use_service_id']);
        //atualiza sessão
        $session = $this->session->userdata();
        $session['patient']['bed'] = $last_bed['name'] . '(Quarto:' . $last_bed['bedroom'] . ') (Setor:' . $last_bed['sector'] . ')';
        $this->session->set_userdata($session);

        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    function visits() {
        if ($this->user['logged']) {
            $use_service_id = $this->encrypt->decode($this->patient['use_service_id']);
            $data = $this->Hospitalization_Model->get_visits_resume($use_service_id);
            $data['title'] = lang('attendance');
            $data['passages'] = $this->Hospitalization_Model->get_passages($use_service_id);
            foreach ($data['passages'] as $key => $p) {
                $data['passages'][$key]['responsables'] = $this->Hospitalization_Model->get_passages_responsables($p['id']);
            }
            $data['professionals'] = $this->Config_Model->get_professionals();
            $data['content'] = 'hospitalization/visits';
            $this->load->view('layouts/none', $data);
        } else {
            $data['title'] = 'Login';
            $data['content'] = 'home/login';
            $this->load->view('layouts/none', $data);
        }
    }

    function intercurrences() {
        if ($this->user['logged']) {
            $use_service_id = $this->encrypt->decode($this->patient['use_service_id']);
            $data = $this->Hospitalization_Model->get_intercurrences_resume($use_service_id);
            $data['title'] = lang('attendance');
            $data['passages'] = $this->Hospitalization_Model->get_passages_intercurrences($use_service_id);
            foreach ($data['passages'] as $key => $p) {
                $data['passages'][$key]['responsables'] = $this->Hospitalization_Model->get_passages_responsables($p['id']);
            }
            $data['professionals'] = $this->Config_Model->get_professionals();
            $data['content'] = 'hospitalization/intercurrences';
            $this->load->view('layouts/none', $data);
        } else {
            $data['title'] = 'Login';
            $data['content'] = 'home/login';
            $this->load->view('layouts/none', $data);
        }
    }

    function save_procedure() {
        $procedure = $this->input->post();
        $procedure['use_service_id'] = $this->encrypt->decode($this->patient['use_service_id']);
        $procedure['hemo'] = 0;
        $procedure['context'] = 'hospitalization';
        $status = $this->Hospitalization_Model->save_procedure_hemo($procedure);
        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    function save_hemo() {
        $hemo = $this->input->post();
        $hemo['use_service_id'] = $this->encrypt->decode($this->patient['use_service_id']);
        $hemo['hemo'] = 1;
        $hemo['context'] = 'hospitalization';
        $status = $this->Hospitalization_Model->save_procedure_hemo($hemo);
        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    function save_visit() {
        $visit = $this->input->post();
        $visit['use_service_id'] = $this->encrypt->decode($this->patient['use_service_id']);
        $visit['author_id'] = $this->user['id'];
        $status = $this->Hospitalization_Model->save_visit($visit);
        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    function insert_responsable_in_visit() {
        $data = $this->input->post();
        $status = $this->Hospitalization_Model->insert_responsable_in_visit($data);
        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    function save_intercurrence() {
        $intercurrence = $this->input->post();
        $intercurrence['use_service_id'] = $this->encrypt->decode($this->patient['use_service_id']);
        $intercurrence['author_id'] = $this->user['id'];
        $status = $this->Hospitalization_Model->save_intercurrence($intercurrence);
        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    function vital_signs() {
        if ($this->user['logged']) {
            $data['title'] = lang('attendance');
            $use_service_id = $this->encrypt->decode($this->patient['use_service_id']);
            $data['vital_signs'] = $this->Attendance_Model->get_all_attendance_vital_signs($use_service_id, 'hospitalization');
            $data['content'] = 'hospitalization/vital_signs';
            $this->load->view('layouts/none', $data);
        } else {
            $data['title'] = 'Login';
            $data['content'] = 'home/login';
            $this->load->view('layouts/none', $data);
        }
    }

    function save_vital_signs() {
        $data = $this->input->post();
        $data['use_service_id'] = $this->encrypt->decode($this->patient['use_service_id']);
        $data['context'] = 'hospitalization';
        $status = $this->Attendance_Model->save_attendance_vital_signs($data);
        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    function microbiology_exams() {
        if ($this->user['logged']) {
            $data['title'] = lang('attendance');
            $data['content'] = 'hospitalization/microbiology_exams';
            $this->load->view('layouts/none', $data);
        } else {
            $data['title'] = 'Login';
            $data['content'] = 'home/login';
            $this->load->view('layouts/none', $data);
        }
    }

    function anatomical_pathological_exams() {
        if ($this->user['logged']) {
            $use_service_id = $this->encrypt->decode($this->patient['use_service_id']);
            $data['anatomical_pathological_exams'] = $this->Hospitalization_Model->get_anatomical_pathological_exams($use_service_id);
            $data['title'] = lang('attendance');
            $data['content'] = 'hospitalization/anatomical_pathological_exams';
            $this->load->view('layouts/none', $data);
        } else {
            $data['title'] = 'Login';
            $data['content'] = 'home/login';
            $this->load->view('layouts/none', $data);
        }
    }

    function save_anatomical_pathological_exam() {
        $data = $this->input->post();
        $data['use_service_id'] = $this->encrypt->decode($this->patient['use_service_id']);
        $data['local_type'] = 'hospitalization';
        $status = $this->Hospitalization_Model->save_anatomical_pathological_exam($data);
        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    function delete_anatomical_pathological_exam() {
        $id = $this->encrypt->decode($this->input->post('id'));
        $status = $this->Hospitalization_Model->delete_anatomical_pathological_exam($id);
        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    function delete_procedure() {
        $id = $this->encrypt->decode($this->input->post('id'));
        $status = $this->Hospitalization_Model->delete_procedure($id);
        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    function drugs() {
        if ($this->user['logged']) {
            $use_service_id = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);
            $data['admindrugs'] = $this->Attendance_Model->get_attendance_administered_drugs($use_service_id, 'hospitalization');
            $date1 = date_create(date("Y-m-d H:i"));

            foreach ($data['admindrugs'] as $key => $drug) {
                $date3 = date_create(date($drug['created']));
                $data['admindrugs'][$key]['days'] = date_diff($date3, $date1);
            }

            $data['drugs'] = $this->Config_Model->get_drugs('intensive_care_unit');
            $data['title'] = lang('attendance');
            $data['content'] = 'hospitalization/drugs';
            $this->load->view('layouts/none', $data);
        } else {
            $data['title'] = 'Login';
            $data['content'] = 'home/login';
            $this->load->view('layouts/none', $data);
        }
    }

    function surgery_anesthesia() {
        if ($this->user['logged']) {
            $use_service_id = $this->encrypt->decode($this->patient['use_service_id']);
            $data['title'] = lang('attendance');
            $data['c_surgeries'] = $this->Config_Model->get_surgeries('intensive_care_unit');
            $data['surgeries'] = $this->Hospitalization_Model->get_surgeries($use_service_id, 'hospitalization');
            foreach ($data['surgeries'] as $key => $p) {
                $data['surgeries'][$key]['team_members'] = $this->Hospitalization_Model->get_surgery_team($p['id']);
            }
            $data['professionals'] = $this->Config_Model->get_professionals();
            $data['content'] = 'hospitalization/surgery_anesthesia';
            $this->load->view('layouts/none', $data);
        } else {
            $data['title'] = 'Login';
            $data['content'] = 'home/login';
            $this->load->view('layouts/none', $data);
        }
    }

    function save_surgery() {
        $data = $this->input->post();
        $data['use_service_id'] = $this->encrypt->decode($this->patient['use_service_id']);
        $data['local_type'] = 'hospitalization';
        $team = $data['team'];
        unset($data['team']);
        $surgery_id = $this->Hospitalization_Model->save_surgery($data);
        $status = 'NOK';
        foreach ($team as $t) {
            $status = $this->Hospitalization_Model->save_surgery_team_member($surgery_id, $t);
        }
        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    function procedures() {
        if ($this->user['logged']) {
            $use_service_id = $this->encrypt->decode($this->patient['use_service_id']);
            $data['title'] = lang('attendance');
            $data['attendance_type'] = $this->session->userdata('patient')['is_trauma'];
            $data['c_hemos'] = $this->Config_Model->get_hemos('intensive_care_unit');
            $data['c_procedures'] = $this->Config_Model->get_procedures('intensive_care_unit');
            $data['c_procedures'] = $this->Config_Model->get_procedures('intensive_care_unit');
            $data['responsibles'] = $this->Config_Model->get_professionals();
            $data['procedures'] = $this->Hospitalization_Model->get_procedures($use_service_id, 'hospitalization');
            $data['hemos'] = $this->Hospitalization_Model->get_hemos($use_service_id, 'hospitalization');
            $data['content'] = 'hospitalization/procedures';
            $this->load->view('layouts/none', $data);
        } else {
            $data['title'] = 'Login';
            $data['content'] = 'home/login';
            $this->load->view('layouts/none', $data);
        }
    }

    function use_of_blood() {
        if ($this->user['logged']) {
            $use_service_id = $this->encrypt->decode($this->patient['use_service_id']);
            $data['use_of_blood'] = $this->Hospitalization_Model->get_use_of_blood($use_service_id);
            $data['sectors'] = $this->Config_Model->get_sectors();
            $data['title'] = lang('attendance');
            $data['content'] = 'hospitalization/use_of_blood';
            $this->load->view('layouts/none', $data);
        } else {
            $data['title'] = 'Login';
            $data['content'] = 'home/login';
            $this->load->view('layouts/none', $data);
        }
    }

    function save_use_of_blood() {
        $data = $this->input->post();
        $data['use_service_id'] = $this->encrypt->decode($this->patient['use_service_id']);
        $data['sector_id'] = $this->encrypt->decode($data['sector_id']);
        $data['local_type'] = 'hospitalization';
        $status = $this->Hospitalization_Model->save_use_of_blood($data);
        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    function delete_use_of_blood() {
        $id = $this->encrypt->decode($this->input->post('id'));
        $status = $this->Hospitalization_Model->delete_use_of_blood($id);
        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    function medical_management() {
        if ($this->user['logged']) {
            $use_service_id = $this->encrypt->decode($this->patient['use_service_id']);
            $data['albumina'] = $this->Hospitalization_Model->get_biochemistry_exams_c_id($use_service_id, 31);
            $data['amilase'] = $this->Hospitalization_Model->get_biochemistry_exams_c_id($use_service_id, 12);
            $data['bilirrubina'] = $this->Hospitalization_Model->get_biochemistry_exams_c_id($use_service_id, 14);
            $data['calcio_total'] = $this->Hospitalization_Model->get_biochemistry_exams_c_id($use_service_id, 33);
            $data['calcio_ionico'] = $this->Hospitalization_Model->get_biochemistry_exams_c_id($use_service_id, 34);
            $data['creatinina'] = $this->Hospitalization_Model->get_biochemistry_exams_c_id($use_service_id, 11);
            $data['glicose'] = $this->Hospitalization_Model->get_biochemistry_exams_c_id($use_service_id, 35);
            $data['sodio'] = $this->Hospitalization_Model->get_biochemistry_exams_c_id($use_service_id, 36);
            $data['potassio'] = $this->Hospitalization_Model->get_biochemistry_exams_c_id($use_service_id, 37);
            $data['lactato'] = $this->Hospitalization_Model->get_biochemistry_exams_c_id($use_service_id, 5);
            $data['ph_arterial'] = $this->Hospitalization_Model->get_biochemistry_exams_c_id($use_service_id, 38);
            $data['po2'] = $this->Hospitalization_Model->get_biochemistry_exams_c_id($use_service_id, 39);
            $data['pco2'] = $this->Hospitalization_Model->get_biochemistry_exams_c_id($use_service_id, 40);
            $data['hco3'] = $this->Hospitalization_Model->get_biochemistry_exams_c_id($use_service_id, 41);
            $data['be'] = $this->Hospitalization_Model->get_biochemistry_exams_c_id($use_service_id, 42);
            $data['ttpa'] = $this->Hospitalization_Model->get_biochemistry_exams_c_id($use_service_id, 7);
            $data['tpinr'] = $this->Hospitalization_Model->get_biochemistry_exams_c_id($use_service_id, 6);
            $data['fibrinogenio'] = $this->Hospitalization_Model->get_biochemistry_exams_c_id($use_service_id, 8);
            $data['bastonetes'] = $this->Hospitalization_Model->get_biochemistry_exams_c_id($use_service_id, 30);
            $data['hemoglobina'] = $this->Hospitalization_Model->get_biochemistry_exams_c_id($use_service_id, 43);
            $data['plaquetas'] = $this->Hospitalization_Model->get_biochemistry_exams_c_id($use_service_id, 17);
            $data['glob_branco'] = $this->Hospitalization_Model->get_biochemistry_exams_c_id($use_service_id, 44);
            $data['pcr'] = $this->Hospitalization_Model->get_biochemistry_exams_c_id($use_service_id, 45);
            $data['troponinai'] = $this->Hospitalization_Model->get_biochemistry_exams_c_id($use_service_id, 46);
            $data['ckmb'] = $this->Hospitalization_Model->get_biochemistry_exams_c_id($use_service_id, 47);
            $data['signals'] = $this->Attendance_Model->get_all_vital_signs($use_service_id);

            $data['title'] = lang('attendance');
            $data['content'] = 'hospitalization/medical_management';
            $this->load->view('layouts/none', $data);
        } else {
            $data['title'] = 'Login';
            $data['content'] = 'home/login';
            $this->load->view('layouts/none', $data);
        }
    }

    function prognostic_indicators() {
        if ($this->user['logged']) {
            $data['title'] = lang('attendance');
            $data['content'] = 'hospitalization/prognostic_indicators';
            $use_service_id = $this->encrypt->decode($this->patient['use_service_id']);
            $data['indices'] = $this->Hospitalization_Model->get_all_indices($use_service_id);
            $this->load->view('layouts/none', $data);
        } else {
            $data['title'] = 'Login';
            $data['content'] = 'home/login';
            $this->load->view('layouts/none', $data);
        }
    }

    function imagery() {
        if ($this->user['logged']) {
            $use_service_id = $this->encrypt->decode($this->patient['use_service_id']);
            $data['images'] = $this->Hospitalization_Model->get_imagerys($use_service_id);
            $data['title'] = lang('attendance');
            $data['content'] = 'hospitalization/imagery';
            $this->load->view('layouts/none', $data);
        } else {
            $data['title'] = 'Login';
            $data['content'] = 'home/login';
            $this->load->view('layouts/none', $data);
        }
    }

    function outpatient_condition() {
        if ($this->user['logged']) {
            $use_service_id = $this->encrypt->decode($this->patient['use_service_id']);
            $data['institutions'] = $this->Config_Model->get_institutions();
            $data['final_condition'] = $this->Attendance_Hospital_Model->get_last_patient_condition($use_service_id);
            $data['title'] = lang('attendance');
            $data['content'] = 'hospitalization/outpatient_condition';
            $this->load->view('layouts/none', $data);
        } else {
            $data['title'] = 'Login';
            $data['content'] = 'home/login';
            $this->load->view('layouts/none', $data);
        }
    }

    function add_patient_condition_final() {
        $data = $this->input->post();
        $data['use_service_id'] = $this->encrypt->decode($this->patient['use_service_id']);
        // pega ultima condicao do paciente
        $last_condition = $this->Attendance_Hospital_Model->get_last_patient_condition($data['use_service_id']);
        // se tiver uma condicao anterior ele da um update no momento de saida
        if (!empty($last_condition)) {
            $status = $this->Attendance_Hospital_Model->update_patient_condition($last_condition['id']);
        }
        if (!$data['moment_admission']) {
            $data['moment_admission'] = date("Y-m-d H:i:s");
        }
        $data['institution_id'] = $this->encrypt->decode($data['institution_id']);
        $status = $this->Attendance_Hospital_Model->save_add_patient_condition($data);

        /*
         * Atualiza a session para as abas aparecerem
         */
        $session = $this->session->userdata();
        if ($data['exit_type'] == 'obito') {
            $session['patient']['death'] = TRUE;
            $session['patient']['out'] = FALSE;
        } else if ($data['exit_type'] == 'alta' || $data['exit_type'] == 'alta_a_pedido') {
            $session['patient']['death'] = FALSE;
            $session['patient']['out'] = TRUE;
        }
        $this->session->set_userdata($session);

        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    function biochemistry_exams() {
        if ($this->user['logged']) {
            $use_service_id = $this->encrypt->decode($this->patient['use_service_id']);
            $data['biochemistry_exams'] = $this->Attendance_Hospital_Model->get_biochemistry_exams($use_service_id, 'hospitalization');
            $data['title'] = lang('attendance');
            $data['content'] = 'hospitalization/biochemistry_exams';
            $this->load->view('layouts/none', $data);
        } else {
            $data['title'] = 'Login';
            $data['content'] = 'home/login';
            $this->load->view('layouts/none', $data);
        }
    }

    function all_biochemistry_exams() {
        if ($this->user['logged']) {
            $use_service_id = $this->encrypt->decode($this->patient['use_service_id']);
            $data['biochemistry_exams'] = $this->Attendance_Hospital_Model->get_biochemistry_exams($use_service_id, 'all');
            $data['title'] = lang('attendance');
            $data['content'] = 'hospitalization/biochemistry_exams';
            $this->load->view('layouts/none', $data);
        } else {
            $data['title'] = 'Login';
            $data['content'] = 'home/login';
            $this->load->view('layouts/none', $data);
        }
    }


    function save_biochemistry_exam() {
        $exams = $this->input->post();
        $data['use_service_id'] = $this->encrypt->decode($this->patient['use_service_id']);
        $data['local_type'] = 'hospitalization';
        foreach ($exams['exams'] as $e) {
            $data['c_laboratory_exams_id'] = $e;
            $status = $this->Hospitalization_Model->save_biochemistry_exams($data);
        }
        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    function delete_biochemistry_exam() {
        $id = $this->encrypt->decode($this->input->post('id'));
        $status = $this->Hospitalization_Model->delete_biochemistry_exam($id);
        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    function image_exams() {
        if ($this->user['logged']) {
            $use_service_id = $this->encrypt->decode($this->patient['use_service_id']);
            $data['image_exams'] = $this->Hospitalization_Model->get_image_exams($use_service_id);
            $data['title'] = lang('attendance');
            $data['content'] = 'hospitalization/image_exams';
            $this->load->view('layouts/none', $data);
        } else {
            $data['title'] = 'Login';
            $data['content'] = 'home/login';
            $this->load->view('layouts/none', $data);
        }
    }

    function save_image_exam() {
        $exams = $this->input->post();

        $data['use_service_id'] = $this->encrypt->decode($this->patient['use_service_id']);
        $data['local_type'] = 'hospitalization';

        foreach ($exams['exams'] as $e) {
            $data['c_radiological_exams_id'] = $e;
            $status = $this->Hospitalization_Model->save_image_exams($data);
        }

        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    function delete_image_exam() {
        $id = $this->encrypt->decode($this->input->post('id'));
        $status = $this->Hospitalization_Model->delete_image_exam($id);
        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    public function receive_imagery() {
        $ds = DIRECTORY_SEPARATOR;

        if (!empty($_FILES)) {
            $tempFile = $_FILES['file']['tmp_name'];
            $targetFile = $this->config->item('store_folder') . 'patients' . $ds . 'images' . $ds . $_FILES['file']['name'];
            $additional = '1';
            while (file_exists($targetFile)) {
                $info = pathinfo($targetFile);
                $targetFile = $info['dirname'] . '/'
                        . $info['filename'] . $additional
                        . '.' . $info['extension'];
            }
            move_uploaded_file($tempFile, $targetFile);
            $content['use_service_id'] = $this->encrypt->decode($this->patient['use_service_id']);
            $content['path'] = $targetFile;
            $content['description'] = $_FILES['file']['name'];
            $content = $this->Hospitalization_Model->insert_imagery($content);
        }
    }

    function delete_imagery() {
        $id = $this->input->post('id');
        $return = $this->Hospitalization_Model->delete_imagery($id);
        echo json_encode(array('status' => $return));
        exit;
    }

    function view_imagery($id) {
        $this->load->helper('file');
        $image = $this->Hospitalization_Model->view_imagery($id);
        $extension = get_mime_by_extension($image['path']);
        header("Cache-Control: maxage=1");
        header("Pragma: public");
        header("Content-type: " . $extension);
        header('Content-Length:' . filesize($image['path']));
        readfile($image['path']);
    }

    function add_administered_drug() {
        $data = $this->input->post();
        $data['use_service_id'] = $this->encrypt->decode($this->session->userdata('patient')['use_service_id']);
        $status = $this->Attendance_Model->save_attendance_administered_drugs($data);
        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    function delete_administered_drug() {
        $data = $this->input->post();
        $status = $this->Attendance_Model->remove_attendance_administered_drugs($data['id']);
        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

}

?>
