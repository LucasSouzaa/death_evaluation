<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Attendance extends CI_Controller {

    function __construct() {
        parent::__construct();
        date_default_timezone_set('America/Sao_Paulo');
        $this->user = $this->session->userdata('user');
        $this->patient = $this->session->userdata('patient');
        $this->load->model('People_Model');
        $this->load->model('Attendance_Model');
        $this->load->model('Attendance_Model');
        $this->load->model('Attendance_Hospital_Model');
        $this->load->model('Hospitalization_Model');
        if (!$this->user['logged']) {
            $data['title'] = 'Login';
            $data['content'] = 'home/login';
            $this->load->view('layouts/none', $data);
        }
    }

    function index() {
        $data['attendance'] = $this->Attendance_Model->get_attendances();
        foreach ($data['attendance'] as $key => $d) {
            $first_regulation = $this->Attendance_Model->get_first_attendance_regulation($d['id']);
            if (isset($first_regulation['moment_admission'])) {
                $data['attendance'][$key]['moment_admission'] = $first_regulation['moment_admission'];
            } else {
                $first_condition = $this->Attendance_Hospital_Model->get_first_patient_condition($d['id']);
                if (isset($first_condition['moment_admission'])) {
                    $data['attendance'][$key]['moment_admission'] = $first_condition['moment_admission'];
                } else {
                    $data['attendance'][$key]['moment_admission'] = $d['created'];
                }
            }

            /*
             * Checa se paciente ja tem leito associado
             */
            $bed = $this->Attendance_Model->get_patient_bed($d['id']);

            if (!empty($bed)) {
                $data['attendance'][$key]['bed'] = $bed['name'];
            } else {
                $data['attendance'][$key]['bed'] = 'Nenhum leito associado ao paciente';
            }

            //checa se ja deu saida e tira do array se ja teve saida
            $last_condition = $this->Attendance_Hospital_Model->get_last_patient_condition($d['id']);
            if ($last_condition['exit_type'] == 'alta' || $last_condition['exit_type'] == 'alta_a_pedido' || $last_condition['exit_type'] == 'obito' || $last_condition['exit_type'] == 'transferencia') {
                unset($data['attendance'][$key]);
            }
        }

        $data['title'] = 'Atendimentos';
        $data['content'] = 'home/attendance';
        $this->load->view('layouts/default', $data);
    }

    function open_attendance($use_service_id_encoded) {
        // atualizamos a sessao de internacao com datas para puxar tudo
        $session = $this->session->userdata();
        $session['hospitalization']['hosp_id'] = '';
        $session['hospitalization']['moment_admission'] = '1900-01-01 00:00:00';
        $session['hospitalization']['moment_exit'] = '';
        $this->session->set_userdata($session);
        // sessao atualizada

        $use_service_id = $this->encrypt->decode($use_service_id_encoded);
        $patient = $this->People_Model->get_person_by_use_service($use_service_id);
        $date1 = date_create(date("Y-m-d"));
        $date2 = date_create(date($patient['birthdate']));
        $patient['age'] = date_diff($date2, $date1);
        if ($patient['age']->format('%y') == 0) {
            $patient['age'] = $patient['age']->format('%m') . ' meses';
        } else {
            $patient['age'] = $patient['age']->format('%y') . ' anos';
        }

        /*
         * checa ultima condicao do paciente
         *  - se for internação pega a data para calcular os dias de internacao
         * checa tambem a data de entrada no intra_hospital
         *  - coloca o moment_admission no horario de admissao do hospital
         */

        $first_condition = $this->Attendance_Hospital_Model->get_first_patient_condition($use_service_id);
        $last_condition = $this->Attendance_Hospital_Model->get_last_patient_condition($use_service_id);
        $date3 = date_create(date($last_condition['moment_exit_calcule']));

        if (!empty($date3) && $last_condition['exit_type'] == 'internado') {
            $date4 = date_create(date($last_condition['moment_admission_calcule']));
            $date5 = date_create(date($last_condition['moment_exit_calcule']));
            $hospitalization_days = date_diff($date5, $date4);
            $hospitalization_days = $hospitalization_days->format('%d');
        } else {
            $hospitalization_days = 0;
        }

        /*
         * Checa se paciente ja tem leito associado
         */
        $bed = $this->Attendance_Model->get_patient_bed($use_service_id);

        if (!empty($bed)) {
            $patient_bed = $bed['name'];
        } else {
            $patient_bed = 'Nenhum leito associado ao paciente';
        }

        $session = $this->session->userdata();
        $session['patient']['death'] = FALSE;
        $session['patient']['hospitalization'] = FALSE;
        $session['patient']['rehospitalization'] = FALSE;
        $session['patient']['out'] = FALSE;
        $condition_list = $this->Attendance_Hospital_Model->get_patient_condition_list($use_service_id);

        //pr($condition_list);exit;

        foreach ($condition_list as $cl) {
            if ($cl['exit_type'] == 'internado') {
                $session['patient']['hospitalization'] = TRUE;
            } else if ($cl['exit_type'] == 'reinternado') {
                $session['patient']['rehospitalization'] = TRUE;
            } else if ($cl['exit_type'] == 'obito') {
                $session['patient']['death'] = TRUE;
            } else if ($cl['exit_type'] == 'alta' || $cl['exit_type'] == 'alta_a_pedido') {
                $session['patient']['out'] = TRUE;
            }
        }

        $session['patient']['logged'] = TRUE;
        $session['patient']['id'] = $this->encrypt->encode($patient['id']);
        $session['patient']['use_service_id'] = $this->encrypt->encode($use_service_id);
        $session['patient']['name'] = $patient['name'];
        $session['patient']['bed'] = $patient_bed;
        $session['patient']['hospitalization_days'] = $hospitalization_days;
        $session['patient']['admission_date'] = $first_condition['moment_admission'];
        $session['patient']['regulation'] = 'Sem informação';
        $session['patient']['is_trauma'] = $patient['type'];
        $session['patient']['gender'] = lang($patient['gender']);
        $session['patient']['home_town'] = $patient['home_town'];
        $session['patient']['age'] = $patient['age'];
        $this->session->set_userdata($session);

        $data['title'] = lang('attendance');
        $data['person'] = $patient;
        $data['content'] = 'demo_data/resume';
        $this->load->view('layouts/default_patient', $data);
    }

    function new_attendance($id_encoded, $type) {
        $id = $this->encrypt->decode($id_encoded);
        $patient = $this->People_Model->get_person($id);
        $date1 = date_create(date("Y-m-d"));
        $date2 = date_create(date($patient['birthdate']));
        $patient['age'] = date_diff($date2, $date1);
        if ($patient['age']->format('%y') == 0) {
            $patient['age'] = $patient['age']->format('%m') . ' meses';
        } else {
            $patient['age'] = $patient['age']->format('%y') . ' anos';
        }

        // checa se ja tem a sessao iniciada
        if ($patient['id'] !== $this->encrypt->decode($this->session->userdata('patient')['id'])) {
            // generate new passage
            $use_service_id = $this->Attendance_Model->new_use_service($patient['id'], $type);
            $session = $this->session->userdata();

            $session['patient']['logged'] = TRUE;
            $session['patient']['id'] = $this->encrypt->encode($patient['id']);
            $session['patient']['use_service_id'] = $this->encrypt->encode($use_service_id);
            $session['patient']['name'] = $patient['name'];
            $session['patient']['bed'] = 'Nenhum leito associado ao paciente';
            $session['patient']['hospitalization_days'] = 0;
            $session['patient']['admission_date'] = date('d-m-Y');
            $session['patient']['regulation'] = 'Sem informação';
            $session['patient']['is_trauma'] = $type;
            $session['patient']['gender'] = $patient['gender'];
            $session['patient']['home_town'] = $patient['home_town'];
            $session['patient']['age'] = $patient['age'];
            $session['patient']['death'] = FALSE;
            $session['patient']['hospitalization'] = FALSE;
            $session['patient']['out'] = FALSE;
            $this->session->set_userdata($session);
        }

        $data['title'] = lang('attendance');
        $data['person'] = $patient;
        $data['content'] = 'demo_data/resume';
        $this->load->view('layouts/default_patient', $data);
    }

    function demo_data() {
        $id = $this->encrypt->decode($this->session->userdata('patient')['id']);
        $patient = $this->People_Model->get_person($id);
        $date1 = date_create(date("Y-m-d"));
        $date2 = date_create(date($patient['birthdate']));
        $patient['age'] = date_diff($date2, $date1);
        if ($patient['age']->format('%y') == 0) {
            $patient['age'] = $patient['age']->format('%m') . ' meses';
        } else {
            $patient['age'] = $patient['age']->format('%y') . ' anos';
        }

        $data['title'] = lang('attendance');
        $data['person'] = $patient;
        $data['content'] = 'demo_data/resume';
        $this->load->view('layouts/default_patient', $data);
    }

    function save_patient_data() {
        $patient = $this->input->post();
        $patient_id = $this->encrypt->decode($this->patient['id']);
        $status = $this->People_Model->edit_patient($patient, $patient_id);
        echo json_encode(array('status' => $status));
        exit;
    }

}

?>
