<?php

Class Outpatients_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->user = $this->session->userdata('user');
        $this->patient = $this->session->userdata('patient');
        date_default_timezone_set('America/Sao_Paulo');
    }

    function save_return($data) {
        $this->db->set($data)
                ->set('created', date("Y-m-d H:i:s"))
                ->set('created_by', $this->user['id'])
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->insert('medical_return');
        $id = $this->db->insert_id();
        if ($id) {
            return 'OK';
        } else {
            return "NOK";
        }
    }

    function get_returns($use_service_id) {
        return $this->db->select('mr.*, pe.name')
                        ->select("date_format(mr.created,'%d/%m/%Y %H:%i') created", false)
                        ->from('medical_return mr')
                        ->join('people pe', 'pe.id=mr.created_by')
                        ->where('mr.status', 1)
                        ->where('mr.use_service_id', $use_service_id)
                        ->get()->result_array();
    }

}

?>
