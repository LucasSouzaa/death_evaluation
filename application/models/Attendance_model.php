<?php

Class Attendance_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->user = $this->session->userdata('user');
        $this->patient = $this->session->userdata('patient');
        date_default_timezone_set('America/Sao_Paulo');
        $this->hospitalization = $this->session->userdata('hospitalization');
        if ($this->hospitalization['moment_exit'] == '') {
            $this->moment_exit = date("Y-m-d H:i:s");
        } else {
            $this->moment_exit = $this->hospitalization['moment_exit'];
        }
    }

    function get_attendance_vital_signs($use_service_id, $context) {
        return $this->db->select('*')
                        ->select("date_format(created,'%d/%m/%Y %H:%i') created", false)
                        ->from('vital_signs')
                        ->where('context', $context)
                        ->where('use_service_id', $use_service_id)
                        ->where('status', 1)
                        ->get()->row_array();
    }

    function get_all_attendance_vital_signs($use_service_id, $context) {
        return $this->db->select('*')
                        ->select("date_format(created,'%d/%m/%Y %H:%i') created", false)
                        ->from('vital_signs')
                        ->where('context', $context)
                        ->where('use_service_id', $use_service_id)
                        ->where('status', 1)
                        ->get()->result_array();
    }

    function get_all_vital_signs($use_service_id) {
        return $this->db->select('*')
                        ->from('vital_signs')
                        ->where('use_service_id', $use_service_id)
                        ->where('status', 1)
                        ->get()->result_array();
    }

    function get_attendance_administered_drugs($use_service_id, $context) {
        $query = $this->db->select('t.id, d.id as drug_id, d.drug_name, t.dose, t.unit')
                ->select("date_format(t.created,'%m/%d/%Y %H:%i') created", false)
                ->from('drugs t')
                ->join('c_drugs d', 't.drug_id = d.id')
                ->where('t.use_service_id', $use_service_id)
                //condicao para trazer os dados da internacao aberta
                ->where('t.created >=', $this->hospitalization['moment_admission'])
                ->where('t.created <=', $this->moment_exit)
                ->where('t.status', 1);
        if ($context !== 'all') {
            return $query->where('context', $context)
                            ->get()->result_array();
        } else {
            return $query->get()->result_array();
        }
    }

    function get_attendance_history_neoplasms($use_service_id, $context) {
        return $this->db->select('hn.*')
                        ->from('history_neoplasms hn')
                        ->where('hn.context', $context)
                        ->where('hn.use_service_id', $use_service_id)
                        ->get()->result_array();
    }

    function get_attendance_history_allergies($use_service_id, $context) {
        return $this->db->select('ha.*, cha.name')
                        ->from('history_allergies ha')
                        ->join('c_history_allergies cha', 'cha.id=ha.c_allergies_id')
                        ->where('ha.context', $context)
                        ->where('ha.use_service_id', $use_service_id)
                        ->get()->result_array();
    }

    function get_attendance_history_medicines($use_service_id, $context) {
        return $this->db->select('hm.*, chm.name')
                        ->from('history_medicines hm')
                        ->join('c_history_medicines chm', 'chm.id=hm.c_medicines_id')
                        ->where('hm.context', $context)
                        ->where('hm.use_service_id', $use_service_id)
                        ->get()->result_array();
    }

    function get_attendance_history_surgeries($use_service_id, $context) {
        return $this->db->select('hs.*, chs.name')
                        ->from('history_surgeries hs')
                        ->join('c_history_surgeries chs', 'chs.id=hs.c_surgeries_id')
                        ->where('hs.context', $context)
                        ->where('hs.use_service_id', $use_service_id)
                        ->get()->result_array();
    }

    function get_patient_bed($use_service_id) {
        return $this->db->select('p.*, b.name')
                        ->select("date_format(p.created,'%d/%m/%Y %H:%i') created", false)
                        ->from('passages_beds p')
                        ->join('beds b', 'b.id=p.bed_id')
                        ->where('p.use_service_id', $use_service_id)
                        ->order_by('p.created', 'desc')
                        ->limit(1)
                        ->get()->row_array();
    }

    function get_attendances() {
        return $this->db->select('us.*, p.name, p.sus_card')
                        ->select("date_format(p.birthdate,'%d/%m/%Y') birthdate", false)
                        ->select("date_format(us.created,'%d/%m/%Y %H:%i') created", false)
                        ->from('use_service us')
                        ->join('people p', 'p.id=us.person_id')
                        ->where('p.status', 1)
                        ->get()->result_array();
    }

    function get_attendance($use_service_id) {
        return $this->db->select('*, p.id as patient_id')
                        ->from('use_service us')
                        ->join('people p', 'p.id=us.person_id')
                        ->where('p.id', 1)
                        ->where('us.id', $use_service_id)
                        ->get()->result_array();
    }

    function get_attendance_history($use_service_id, $context) {
        return $this->db->select('*')
                        ->from('history_complements')
                        ->where('use_service_id', $use_service_id)
                        ->where('context', $context)
                        ->where('status', 1)
                        ->get()->row_array();
    }

    function get_attendance_trauma_procedures($use_service_id, $context) {
        $query = $this->db->select('*')
                ->from('trauma_procedures')
                ->where('use_service_id', $use_service_id)
                ->where('status', 1);
        if ($context !== 'all') {
            return $query->where('context', $context)
                            ->get()->row_array();
        } else {
            return $query->get()->row_array();
        }
    }

    function get_attendance_physical_exams($use_service_id, $context) {
        return $this->db->select('*')
                        ->from('phys_exams')
                        ->where('context', $context)
                        ->where('use_service_id', $use_service_id)
                        ->where('status', 1)
                        ->get()->row_array();
    }

    function get_attendance_regulation($use_service_id, $context) {
        return $this->db->select('*')
                        ->from('regulation')
                        ->where('context', $context)
                        ->where('use_service_id', $use_service_id)
                        ->get()->row_array();
    }

    function get_first_attendance_regulation($use_service_id) {
        return $this->db->select('*')
                        ->select("date_format(reg_datetime,'%d/%m/%Y %H:%i') moment_admission", false)
                        ->from('regulation')
                        ->where('use_service_id', $use_service_id)
                        ->order_by('reg_datetime', 'asc')
                        ->limit(1)
                        ->get()->row_array();
    }

    function get_last_vital_signs($use_service_id) {
        return $this->db->select('*')
                        ->from('vital_signs')
                        ->where('use_service_id', $use_service_id)
                        ->where('status', 1)
                        ->order_by('moment', 'desc')
                        ->limit(1)
                        ->get()->row_array();
    }

    function get_attendance_routing($use_service_id, $context) {
        return $this->db->select('*')
                        ->from('routing')
                        ->where('context', $context)
                        ->where('use_service_id', $use_service_id)
                        ->get()->row_array();
    }

    function get_attendance_clinical($use_service_id, $context) {
        return $this->db->select('*')
                        ->from('clinical')
                        ->where('context', $context)
                        ->where('use_service_id', $use_service_id)
                        ->get()->row_array();
    }

    function get_attendance_mechanisms($use_service_id, $context) {
        return $this->db->select('*')
                        ->from('trauma_mechanism')
                        ->where('context', $context)
                        ->where('use_service_id', $use_service_id)
                        ->get()->row_array();
    }

    function get_attendance_clinical_procedures($use_service_id, $context) {
        return $this->db->select('*')
                        ->from('clinical_procedures')
                        ->where('context', $context)
                        ->where('use_service_id', $use_service_id)
                        ->get()->row_array();
    }

    /*
     * **********************************************************************************************************************************
     * **********************************************************************************************************************************
     * functions to save content
     * **********************************************************************************************************************************
     * **********************************************************************************************************************************
     */

    function new_use_service($person_id, $attendance_type) {
        $this->db->set('person_id', $person_id)
                ->set('type', $attendance_type)
                ->set('created', date("Y-m-d H:i:s"))
                ->insert('use_service');
        $id = $this->db->insert_id();

        if ($id) {
            return $id;
        } else {
            return "NOK";
        }
    }

    function save_attendance_vital_signs($data) {
        $this->db->set('status', 0)
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->where('context', $data['context'])
                ->where('use_service_id', $data['use_service_id'])
                ->where('status', 1)
                ->update('vital_signs');

        $this->db->set($data)
                ->set('created', date("Y-m-d H:i:s"))
                ->set('created_by', $this->user['id'])
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->insert('vital_signs');
        if ($this->db->insert_id()) {
            return "OK";
        } else {
            return 'NOK';
        }
    }

    function save_attendance_history($data) {
        $this->db->set('status', 0)
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->where('context', $data['context'])
                ->where('use_service_id', $data['use_service_id'])
                ->where('status', 1)
                ->update('history_complements');

        $this->db->set($data)
                ->set('created', date("Y-m-d H:i:s"))
                ->set('created_by', $this->user['id'])
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->insert('history_complements');
        if ($this->db->insert_id()) {
            return 'OK';
        } else {
            return "NOK";
        }
    }

    function save_attendance_regulation($data) {
        $this->db->set($data)
                ->set('created', date("Y-m-d H:i:s"))
                ->set('created_by', $this->user['id'])
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->insert('regulation');
        if ($this->db->insert_id()) {
            return "OK";
        } else {
            return 'NOK';
        }
    }

    function save_attendance_clinical($data) {
        $this->db->set($data)
                ->set('created', date("Y-m-d H:i:s"))
                ->set('created_by', $this->user['id'])
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->insert('clinical');
        if ($this->db->insert_id()) {
            return "OK";
        } else {
            return 'NOK';
        }
    }

    function save_attendance_mechanisms($data) {
        $this->db->set($data)
                ->set('created', date("Y-m-d H:i:s"))
                ->set('created_by', $this->user['id'])
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->insert('trauma_mechanism');
        if ($this->db->insert_id()) {
            return "OK";
        } else {
            return 'NOK';
        }
    }

    function save_attendance_administered_drugs($data) {
        $this->db->set($data)
                ->set('created', date("Y-m-d H:i:s"))
                ->set('created_by', $this->user['id'])
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->insert('drugs');
        if ($this->db->insert_id()) {
            return 'OK';
        } else {
            return 'NOK';
        }
    }

    function remove_attendance_administered_drugs($id) {
        $this->db->set('status', 0)
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->where('id', $id)
                ->update('drugs');
    }

    function save_attendance_trauma_procedures($data) {
        $this->db->set('status', 0)
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->where('context', $data['context'])
                ->where('use_service_id', $data['use_service_id'])
                ->where('status', 1)
                ->update('trauma_procedures');

        $this->db->set($data)
                ->set('created', date("Y-m-d H:i:s"))
                ->set('created_by', $this->user['id'])
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->insert('trauma_procedures');
        if ($this->db->insert_id()) {
            return 'OK';
        } else {
            return 'NOK';
        }
    }

    function save_attendance_clinical_procedures($data) {
        $this->db->set($data)
                ->set('created', date("Y-m-d H:i:s"))
                ->set('created_by', $this->user['id'])
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->insert('clinical_procedures');
        if ($this->db->insert_id()) {
            return 'OK';
        } else {
            return "NOK";
        }
    }

    function save_attendance_physical_exams($data) {
        $this->db->set('status', 0)
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->where('context', $data['context'])
                ->where('use_service_id', $data['use_service_id'])
                ->where('status', 1)
                ->update('phys_exams');

        $this->db->set($data)
                ->set('created', date("Y-m-d H:i:s"))
                ->set('created_by', $this->user['id'])
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->insert('phys_exams');
        $id = $this->db->insert_id();
        if ($id) {
            return 'OK';
        } else {
            return "NOK";
        }
    }

    function save_attendance_complementary_exams($data) {
        $this->db->set($data)
                ->set('created', date("Y-m-d H:i:s"))
                ->set('created_by', $this->user['id'])
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->insert('complementary_exams');
        $id = $this->db->insert_id();
        if ($id) {
            return 'OK';
        } else {
            return "NOK";
        }
    }

    function add_history_item($name, $data) {
//            $select = $this->db->select('*')
//                            ->from('history_' . $name)
//                            ->where('c_' . $name . '_id', $data['id'])
//                            ->where('context', $data['context'])
//                            ->where('use_service_id', $data['use_service_id'])
//                            ->get()->num_rows();
//            if ($select > 0) {
//                $query = $this->db->set('status', 1)
//                        ->set('modified', date("Y-m-d H:i:s"))
//                        ->set('modified_by', $this->user['id'])
//                        ->where('c_' . $name . '_id', $data['id'])
//                        ->where('context', $data['context'])
//                        ->where('use_service_id', $data['use_service_id'])
//                        ->update('history_' . $name);
//
//                if ($query) {
//                    return 'OK';
//                } else {
//                    return 'NOK';
//                }
//            } else {
        $this->db->set('c_' . $name . '_id', $data['id'])
                ->set('use_service_id', $data['use_service_id'])
                ->set('created', date("Y-m-d H:i:s"))
                ->set('created_by', $this->user['id'])
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->set('context', $data['context'])
                ->insert('history_' . $name);
        $id = $this->db->insert_id();
        if ($id) {
            return 'OK';
        } else {
            return "NOK";
        }
        //}
    }

    function add_history_item_cid($name, $data) {
        $this->db->set('name', $data['id'])
                ->set('use_service_id', $data['use_service_id'])
                ->set('created', date("Y-m-d H:i:s"))
                ->set('created_by', $this->user['id'])
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->set('context', $data['context'])
                ->insert('history_' . $name);
        $id = $this->db->insert_id();
        if ($id) {
            return 'OK';
        } else {
            return "NOK";
        }
    }

    function delete_history_item($data) {
        $this->db->where('id', $data['id']);
        $this->db->delete('history_' . $data['name']);
        return "OK";
    }

}

?>
