<?php

Class Death_evaluation_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->user = $this->session->userdata('user');
    }

    function save_death_evaluation($data) {
        $this->db->set($data)
                ->set('created', date("Y-m-d H:i:s"))
                ->set('created_by', $this->user['id'])
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->insert('death_evaluation');
        $id = $this->db->insert_id();
        if ($id) {
            return 'OK';
        } else {
            return "NOK";
        }
    }

    function get_death_evaluation($use_service_id) {
        return $this->db->select('*')
                        ->from('death_evaluation')
                        ->where('use_service_id', $use_service_id)
                        ->get()->result_array();
    }

    function get_image_exams($use_service_id) {
        return $this->db->select('pre.*, cre.name')
                        ->select("date_format(pre.report_date,'%d/%m/%Y %H:%i') report_date", false)
                        ->select("date_format(pre.created,'%d/%m/%Y %H:%i') created", false)
                        ->from('passages_radiological_exams pre')
                        ->join('c_radiological_exams cre', 'cre.id=pre.c_radiological_exams_id')
                        ->where('pre.use_service_id', $use_service_id)
                        ->where('pre.status', 1)
                        ->get()->result_array();
    }

    function get_anatomical_pathological_exams($use_service_id) {
        return $this->db->select('*')
                        ->select("date_format(reception_date,'%d/%m/%Y %H:%i') reception_date", false)
                        ->select("date_format(liberation_date,'%d/%m/%Y %H:%i') liberation_date", false)
                        ->from('passages_anatomical_pathological_exams')
                        ->where('use_service_id', $use_service_id)
                        ->where('status', 1)
                        ->get()->result_array();
    }

    function get_prehospital_mechanisms($use_service_id) {
        return $this->db->select('*')
                        ->from('trauma_mechanism')
                        ->where('use_service_id', $use_service_id)
                        ->get()->row_array();
    }

    function get_patient_passages($usi) {
        return $this->db->select("date_format(created,'%d/%m/%Y %H:%i') created", false)
                        ->select("created as created_sf")
                        ->from('use_service')
                        ->where('id', $usi)
                        ->get()->row_array();
    }

}

?>
