<?php

Class User_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function get_user($id) {

        return $this->db->select('p.*, u.username, u.email, u.password')
                        ->from('users u')
                        ->join('people p', 'p.id=u.person_id')
                        ->where('u.status', 1)
                        ->where('u.id', $id)
                        ->get()->row_array();
    }

}

?>