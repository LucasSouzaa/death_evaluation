<?php

Class Attendance_Hospital_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->user = $this->session->userdata('user');
        date_default_timezone_set('America/Sao_Paulo');
        $this->hospitalization = $this->session->userdata('hospitalization');
        if ($this->hospitalization['moment_exit'] == '') {
            $this->moment_exit = date("Y-m-d H:i:s");
        } else {
            $this->moment_exit = $this->hospitalization['moment_exit'];
        }
    }

    function save_biochemistry_exams($data) {
        $this->db->set('c_laboratory_exams_id', $data['c_laboratory_exams_id'])
                ->set('use_service_id', $data['use_service_id'])
                ->set('created', date("Y-m-d H:i:s"))
                ->set('created_by', $this->user['id'])
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->set('local_type', $data['local_type'])
                ->insert('passages_laboratory_exams');
        $id = $this->db->insert_id();
        if ($id) {
            return 'OK';
        } else {
            return "NOK";
        }
    }

    function add_assist_team($data) {
        $this->db->set($data)
                ->set('created', date("Y-m-d H:i:s"))
                ->set('created_by', $this->user['id'])
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->insert('assist_team');
        $id = $this->db->insert_id();
        if ($id) {
            return $id;
        } else {
            return "NOK";
        }
    }

    function select_assist_team($data) {
        return $this->db->select('*')
                        ->from('assist_team')
                        ->where('use_service_id', $data['use_service_id'])
                        ->where('team_member_id', $data['team_member_id'])
                        ->get()->row_array();
    }

    function update_assist_team($data) {
        $this->db->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->set('status', 1)
                ->where('use_service_id', $data['use_service_id'])
                ->where('team_member_id', $data['team_member_id'])
                ->update('assist_team');
    }

    function remove_assist_team_member($data) {
        $query = $this->db->set('status', 0)
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->where('id', $data['id'])
                ->where('status', 1)
                ->update('assist_team');
        if ($query) {
            return 'OK';
        } else {
            return 'NOK';
        }
    }

    function get_passage_assist_team($use_service_id, $type) {
        $this->db->select('at.id, tm.hierarchy, tm.specialty, t.name as team, p.name as name')
                ->from('team_members tm')
                ->join('team t', 't.id=tm.team_id')
                ->join('assist_team at', 'tm.id = at.team_member_id')
                ->join('people p', 'p.id=tm.person_id')
                ->where('at.use_service_id', $use_service_id)
                ->where('tm.status', 1)
                ->where('at.status', 1);
        if ($type == 'supervisor') {
            $this->db->where('hierarchy', 'teaching_medical');
            $this->db->or_where('hierarchy', 'attending_physician');
        } else if ($type == 'resident') {
            $this->db->where('hierarchy', 'r1');
            $this->db->or_where('hierarchy', 'r2');
            $this->db->or_where('hierarchy', 'r3');
        } else if ($type == 'enf') {
            $this->db->where('hierarchy', 'nurse');
        } else if ($type == 't_enf') {
            $this->db->where('hierarchy', 'nursing_technician');
        }
        return $this->db->get()->result_array();
    }

    function save_bio_exam_result($result) {
        $this->db->set('result', $result['result'])
                ->set('result_date', $result['result_date'])
                ->set('passages_laboratory_exams_id', $result['exam_id'])
                ->set('created', date("Y-m-d H:i:s"))
                ->set('created_by', $this->user['id'])
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->insert('passages_laboratory_exams_result');
        $id = $this->db->insert_id();
        if ($id) {
            return 'OK';
        } else {
            return "NOK";
        }
    }

    function save_image_exam_result($result) {
        $query = $this->db->set('report', $result['report'])
                ->set('report_date', $result['report_date'])
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->where('id', $result['exam_id'])
                ->update('passages_radiological_exams');

        if ($query) {
            return 'OK';
        } else {
            return 'NOK';
        }
    }

    function save_image_exams($data) {
        $this->db->set('c_radiological_exams_id', $data['c_radiological_exams_id'])
                ->set('use_service_id', $data['use_service_id'])
                ->set('created', date("Y-m-d H:i:s"))
                ->set('created_by', $this->user['id'])
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->set('local_type', $data['local_type'])
                ->insert('passages_radiological_exams');
        $id = $this->db->insert_id();
        if ($id) {
            return 'OK';
        } else {
            return "NOK";
        }
    }

    function get_biochemistry_exams($use_service_id, $local_type) {
        $query = $this->db->select('ple.*, cle.name, pler.result')
                ->select("date_format(pler.result_date,'%d/%m/%Y %H:%i') result_date", false)
                ->select("date_format(ple.created,'%d/%m/%Y %H:%i') created", false)
                ->from('passages_laboratory_exams ple')
                ->join('c_laboratorial_exams cle', 'cle.id=ple.c_laboratory_exams_id')
                ->join('passages_laboratory_exams_result pler', 'pler.passages_laboratory_exams_id=ple.id', 'left')
                ->where('ple.use_service_id', $use_service_id)
                //condicao para trazer os dados da internacao aberta
                ->where('ple.created >=', $this->hospitalization['moment_admission'])
                ->where('ple.created <=', $this->moment_exit)
                ->where('ple.status', 1);
        if ($local_type !== 'all') {
            $query->where('ple.local_type', $local_type);
        }
        return $query->get()->result_array();
    }

    function get_image_exams($use_service_id) {
        return $this->db->select('pre.*, cre.name')
                        ->select("date_format(pre.created,'%d/%m/%Y %H:%i') created", false)
                        ->from('passages_radiological_exams pre')
                        ->join('c_radiological_exams cre', 'cre.id=pre.c_radiological_exams_id')
                        ->where('pre.use_service_id', $use_service_id)
                        ->where('pre.status', 1)
                        ->where('pre.local_type', 'hospital')
                        ->get()->result_array();
    }

    function delete_image_exam($id) {
        $query = $this->db->set('status', 0)
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->where('id', $id)
                ->update('passages_radiological_exams');

        if ($query) {
            return 'OK';
        } else {
            return 'NOK';
        }
    }

    function delete_biochemistry_exam($id) {
        $query = $this->db->set('status', 0)
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->where('id', $id)
                ->update('passages_laboratory_exams');

        if ($query) {
            return 'OK';
        } else {
            return 'NOK';
        }
    }

    function get_ec_diagnosys_list($use_service_id) {
        return $this->db->select('*')
                        ->from('diagnosys_ec')
                        ->where('use_service_id', $use_service_id)
                        ->where('status', 1)
                        //condicao para trazer os dados da internacao aberta
                        ->where('created >=', $this->hospitalization['moment_admission'])
                        ->where('created <=', $this->moment_exit)
                        ->get()->result_array();
    }

    function get_ais_diagnosys_region($use_service_id, $region) {
        return $this->db->select('ais.*, ais.id as ais_id, p.id')
                        ->from('passage_ais p')
                        ->join('ais', 'ais.id=p.ais_id')
                        ->where('p.use_service_id', $use_service_id)
                        ->like('ais.regiao_corpo', $region)
                        ->where('p.status', 1)
                        ->limit(1)
                        ->get()->row_array();
    }

    function get_ais_diagnosys_list($use_service_id) {
        return $this->db->select('ais.*, ais.id as ais_id, p.id')
                        ->from('passage_ais p')
                        ->join('ais', 'ais.id=p.ais_id')
                        ->where('p.use_service_id', $use_service_id)
                        ->where('p.status', 1)
                        //condicao para trazer os dados da internacao aberta
                        ->where('p.created >=', $this->hospitalization['moment_admission'])
                        ->where('p.created <=', $this->moment_exit)
                        ->get()->result_array();
    }

    function get_i_diagnosys_list($use_service_id) {
        return $this->db->select('*')
                        ->from('diagnosys_i')
                        ->where('use_service_id', $use_service_id)
                        ->where('status', 1)
                        //condicao para trazer os dados da internacao aberta
                        ->where('created >=', $this->hospitalization['moment_admission'])
                        ->where('created <=', $this->moment_exit)
                        ->get()->result_array();
    }

    function save_add_ec_cid($diagnosys) {
        $select = $this->db->select('*')
                        ->from('diagnosys_ec')
                        ->where('chapter_cid_field', $diagnosys['chapter_cid_field'])
                        ->where('group_cid_field', $diagnosys['group_cid_field'])
                        ->where('category_cid_field', $diagnosys['category_cid_field'])
                        ->where('subcategory_cid_field', $diagnosys['subcategory_cid_field'])
                        ->where('use_service_id', $diagnosys['use_service_id'])
                        ->get()->num_rows();

        if ($select > 0) {
            $query = $this->db->set('status', 1)
                    ->set('modified', date("Y-m-d H:i:s"))
                    ->set('modified_by', $this->user['id'])
                    ->where('chapter_cid_field', $diagnosys['chapter_cid_field'])
                    ->where('group_cid_field', $diagnosys['group_cid_field'])
                    ->where('category_cid_field', $diagnosys['category_cid_field'])
                    ->where('subcategory_cid_field', $diagnosys['subcategory_cid_field'])
                    ->where('use_service_id', $diagnosys['use_service_id'])
                    ->update('diagnosys_ec');

            if ($query) {
                return 'OK';
            } else {
                return 'NOK';
            }
        } else {
            $this->db->set('group_cid_field', $diagnosys['group_cid_field'])
                    ->set('chapter_cid_field', $diagnosys['chapter_cid_field'])
                    ->set('category_cid_field', $diagnosys['category_cid_field'])
                    ->set('subcategory_cid_field', $diagnosys['subcategory_cid_field'])
                    ->set('use_service_id', $diagnosys['use_service_id'])
                    ->set('created', date("Y-m-d H:i:s"))
                    ->set('created_by', $this->user['id'])
                    ->set('modified', date("Y-m-d H:i:s"))
                    ->set('modified_by', $this->user['id'])
                    ->insert('diagnosys_ec');
            $id = $this->db->insert_id();
            if ($id) {
                return 'OK';
            } else {
                return "NOK";
            }
        }
    }

    function save_add_ais($diagnosys) {
        $select = $this->db->select('*')
                        ->from('passage_ais')
                        ->where('use_service_id', $diagnosys['use_service_id'])
                        ->where('ais_id', $diagnosys['ais_id'])
                        ->get()->num_rows();

        if ($select > 0) {
            $query = $this->db->set('status', 1)
                    ->set('modified', date("Y-m-d H:i:s"))
                    ->set('modified_by', $this->user['id'])
                    ->where('ais_id', $diagnosys['ais_id'])
                    ->where('use_service_id', $diagnosys['use_service_id'])
                    ->update('passage_ais');

            if ($query) {
                return 'OK';
            } else {
                return 'NOK';
            }
        } else {
            $this->db->set('ais_id', $diagnosys['ais_id'])
                    ->set('use_service_id', $diagnosys['use_service_id'])
                    ->set('created', date("Y-m-d H:i:s"))
                    ->set('created_by', $this->user['id'])
                    ->set('modified', date("Y-m-d H:i:s"))
                    ->set('modified_by', $this->user['id'])
                    ->insert('passage_ais');
            $id = $this->db->insert_id();
            if ($id) {
                return 'OK';
            } else {
                return "NOK";
            }
        }
    }

    function save_add_i_cid($diagnosys) {
        $select = $this->db->select('*')
                        ->from('diagnosys_i')
                        ->where('chapter_cid_field', $diagnosys['chapter_cid_field'])
                        ->where('group_cid_field', $diagnosys['group_cid_field'])
                        ->where('category_cid_field', $diagnosys['category_cid_field'])
                        ->where('subcategory_cid_field', $diagnosys['subcategory_cid_field'])
                        ->where('subcategory_cid_field_code', $diagnosys['subcategory_cid_field_code'])
                        ->where('use_service_id', $diagnosys['use_service_id'])
                        ->get()->num_rows();

        if ($select > 0) {
            $query = $this->db->set('status', 1)
                    ->set('modified', date("Y-m-d H:i:s"))
                    ->set('modified_by', $this->user['id'])
                    ->where('chapter_cid_field', $diagnosys['chapter_cid_field'])
                    ->where('group_cid_field', $diagnosys['group_cid_field'])
                    ->where('category_cid_field', $diagnosys['category_cid_field'])
                    ->where('subcategory_cid_field', $diagnosys['subcategory_cid_field'])
                    ->where('subcategory_cid_field_code', $diagnosys['subcategory_cid_field_code'])
                    ->where('use_service_id', $diagnosys['use_service_id'])
                    ->update('diagnosys_i');

            if ($query) {
                return 'OK';
            } else {
                return 'NOK';
            }
        } else {
            $this->db->set('group_cid_field', $diagnosys['group_cid_field'])
                    ->set('chapter_cid_field', $diagnosys['chapter_cid_field'])
                    ->set('category_cid_field', $diagnosys['category_cid_field'])
                    ->set('subcategory_cid_field', $diagnosys['subcategory_cid_field'])
                    ->set('subcategory_cid_field_code', $diagnosys['subcategory_cid_field_code'])
                    ->set('use_service_id', $diagnosys['use_service_id'])
                    ->set('created', date("Y-m-d H:i:s"))
                    ->set('created_by', $this->user['id'])
                    ->set('modified', date("Y-m-d H:i:s"))
                    ->set('modified_by', $this->user['id'])
                    ->insert('diagnosys_i');
            $id = $this->db->insert_id();
            if ($id) {
                return 'OK';
            } else {
                return "NOK";
            }
        }
    }

    function delete_i_cid($id) {
        $query = $this->db->set('status', 0)
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->where('id', $id)
                ->update('diagnosys_i');
        if ($query) {
            return 'OK';
        } else {
            return 'NOK';
        }
    }

    function delete_ec_cid($id) {
        $query = $this->db->set('status', 0)
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->where('id', $id)
                ->update('diagnosys_ec');
        if ($query) {
            return 'OK';
        } else {
            return 'NOK';
        }
    }

    function delete_ais($id) {
        $query = $this->db->set('status', 0)
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->where('id', $id)
                ->update('passage_ais');
        if ($query) {
            return 'OK';
        } else {
            return 'NOK';
        }
    }

    function add_volemic_reposition($volemic_reposition) {
        $this->db->set('solution', $volemic_reposition['solution'])
                ->set('solution_type', $volemic_reposition['solution_type'])
                ->set('solution_detail', $volemic_reposition['solution_detail'])
                ->set('moment', $volemic_reposition['moment'])
                ->set('local_type', $volemic_reposition['local_type'])
                ->set('aptm', $volemic_reposition['aptm'])
                ->set('use_service_id', $volemic_reposition['use_service_id'])
                ->set('created', date("Y-m-d H:i:s"))
                ->set('created_by', $this->user['id'])
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->insert('volemic_reposition');
        $id = $this->db->insert_id();
        if ($id) {
            return 'OK';
        } else {
            return "NOK";
        }
    }

    function get_volemic_reposition_list($use_service_id, $volemic_reposition_type) {
        return $this->db->select('*')
                        ->from('volemic_reposition')
                        ->where('use_service_id', $use_service_id)
                        ->where('solution', $volemic_reposition_type)
                        ->where('status', 1)
                        ->get()->result_array();
    }

    function get_patient_condition_list($use_service_id) {
        return $this->db->select('*')
                        ->select("date_format(moment_admission,'%d/%m/%Y %H:%i') moment_admission_f", false)
                        ->from('intra_hospital')
                        ->where('use_service_id', $use_service_id)
                        ->where('status', 1)
                        ->get()->result_array();
    }

    function get_last_patient_condition($use_service_id) {
        return $this->db->select('ih.*, i.name')
                        ->select("date_format(ih.moment_exit,'%d/%m/%Y %H:%i') moment_exit", false)
                        ->select("date_format(ih.moment_exit,'%m/%d/%Y %H:%i') moment_exit_calcule", false)
                        ->select("date_format(ih.moment_admission,'%d/%m/%Y %H:%i') moment_admission", false)
                        ->select("date_format(ih.moment_admission,'%m/%d/%Y %H:%i') moment_admission_calcule", false)
                        ->from('intra_hospital ih')
                        ->join('institutions i', 'ih.institution_id=i.id', 'left')
                        ->where('ih.use_service_id', $use_service_id)
                        ->where('ih.status', 1)
                        ->order_by('ih.id', 'desc')
                        ->limit(1)
                        ->get()->row_array();
    }

    function get_patient_condition_like($use_service_id, $type) {
        $select = $this->db->select('*')
                        ->from('intra_hospital')
                        ->where('exit_type', $type)
                        ->where('status', 1)
                        ->where('use_service_id', $use_service_id)
                        ->get()->num_rows();
        if ($select > 0) {
            return 'OK';
        } else {
            return 'NOK';
        }
    }

    function get_first_patient_condition($use_service_id) {
        return $this->db->select('*')
                        ->select("date_format(moment_admission,'%d/%m/%Y %H:%i') moment_admission", false)
                        ->from('intra_hospital')
                        ->where('use_service_id', $use_service_id)
                        ->where('status', 1)
                        ->order_by('moment_admission', 'asc')
                        ->limit(1)
                        ->get()->row_array();
    }

    function update_patient_condition($id) {
        $query = $this->db->set('moment_exit', date("Y-m-d H:i:s"))
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->where('id', $id)
                ->update('intra_hospital');
        if ($query) {
            return 'OK';
        } else {
            return 'NOK';
        }
    }

    function delete_volemic_reposition($id) {
        $query = $this->db->set('status', 0)
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->where('id', $id)
                ->update('volemic_reposition');
        if ($query) {
            return 'OK';
        } else {
            return 'NOK';
        }
    }

    function save_evolution($data) {
        $this->db->set('status', 1)
                ->set('evolution', $data['evolution'])
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->where('local_type', $data['local_type'])
                ->where('use_service_id', $data['use_service_id'])
                ->where('status', 1)
                ->update('evolutions');

        $this->db->set('evolution', $data['evolution'])
                ->set('use_service_id', $data['use_service_id'])
                ->set('created', date("Y-m-d H:i:s"))
                ->set('created_by', $this->user['id'])
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->set('local_type', $data['local_type'])
                ->insert('evolutions');
        $id = $this->db->insert_id();
        if ($id) {
            return 'OK';
        } else {
            return "NOK";
        }
    }

    function save_water_balance($data) {
        $this->db->set('status', 0)
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->where('local_type', $data['local_type'])
                ->where('use_service_id', $data['use_service_id'])
                ->where('status', 1)
                ->update('water_balance');

        $this->db->set('diurese_out', $data['diurese_out'])
                ->set('sonda_gast_out', $data['sonda_gast_out'])
                ->set('dreno_dir_out', $data['dreno_dir_out'])
                ->set('dreno_esq_out', $data['dreno_esq_out'])
                ->set('use_service_id', $data['use_service_id'])
                ->set('created', date("Y-m-d H:i:s"))
                ->set('created_by', $this->user['id'])
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->set('local_type', $data['local_type'])
                ->insert('water_balance');
        $id = $this->db->insert_id();
        if ($id) {
            return 'OK';
        } else {
            return "NOK";
        }
    }

    function get_water_balance($use_service_id, $local_type) {
        return $this->db->select('*')
                        ->from('water_balance')
                        ->where('local_type', $local_type)
                        ->where('use_service_id', $use_service_id)
                        ->get()->row_array();
    }

    function get_evolution($use_service_id, $local_type) {
        return $this->db->select('*')
                        ->from('evolutions')
                        ->where('local_type', $local_type)
                        ->where('use_service_id', $use_service_id)
                        ->get()->row_array();
    }

    function save_add_patient_condition($data) {
        $this->db->set($data)
                ->set('created', date("Y-m-d H:i:s"))
                ->set('created_by', $this->user['id'])
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->insert('intra_hospital');
        $id = $this->db->insert_id();
        if ($id) {
            return 'OK';
        } else {
            return "NOK";
        }
    }

    function delete_patient_condition($id) {
        $query = $this->db->set('status', 0)
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->where('id', $id)
                ->update('intra_hospital');
        if ($query) {
            return 'OK';
        } else {
            return 'NOK';
        }
    }

    function save_primary_evaluation($data) {
        $query = $this->db->set('status', 0)
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->where('local_type', $data['local_type'])
                ->where('use_service_id', $data['use_service_id'])
                ->where('status', 1)
                ->update('primary_assessment');
        if ($query) {
            $this->db->set($data)
                    ->set('created', date("Y-m-d H:i:s"))
                    ->set('created_by', $this->user['id'])
                    ->set('modified', date("Y-m-d H:i:s"))
                    ->set('modified_by', $this->user['id'])
                    ->insert('primary_assessment');
            $id = $this->db->insert_id();
            if ($id) {
                return 'OK';
            } else {
                return "NOK";
            }
        } else {
            return 'NOK';
        }
    }

    function get_primary_evaluation($use_service_id, $local_type) {
        return $this->db->select('*')
                        ->from('primary_assessment')
                        ->where('local_type', $local_type)
                        ->where('use_service_id', $use_service_id)
                        ->where('status', 1)
                        ->get()->row_array();
    }

}

?>
