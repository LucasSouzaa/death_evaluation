<?php

Class Config_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->user = $this->session->userdata('user');
    }

    function get_ais() {
        return $this->db->select('*')
                        ->from('ais')
                        ->get()->result_array();
    }

    // INSTITUTIONS
    function get_institutions() {
        return $this->db->select('*')
                        ->from('institutions')
                        ->where('status', 1)
                        ->get()->result_array();
    }

    function get_institution($id) {
        return $this->db->select('*')
                        ->from('institutions')
                        ->where('id', $id)
                        ->get()->row_array();
    }

    function delete_institution($id) {
        $query = $this->db->set('status', 2)
                ->set('modified_by', $this->user['id'])
                ->set('modified', date("Y-m-d H:i:s"))
                ->where('id', $id)
                ->update('institutions');
        if ($query) {
            return 'OK';
        } else {
            return 'NOK';
        }
    }

    function save_new_institution($institution) {
        $this->db->set($institution)
                ->set('created', date("Y-m-d H:i:s"))
                ->set('created_by', $this->user['id'])
                ->set('modified_by', $this->user['id'])
                ->set('modified', date("Y-m-d H:i:s"))
                ->insert('institutions');
        $id = $this->db->insert_id();

        if ($id) {
            return 'OK';
        } else {
            return "NOK";
        }
    }

    function edit_institution($institution, $institution_id) {
        $query = $this->db->set($institution)
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->where('id', $institution_id)
                ->update('institutions');

        if ($query) {
            return 'OK';
        } else {
            return 'NOK';
        }
    }

    // SECTORS

    function get_sectors() {
        return $this->db->select('s.*, i.name as institution')
                        ->from('sectors s')
                        ->join('institutions i', 'i.id=s.institution_id')
                        ->where('s.status', 1)
                        ->get()->result_array();
    }

    function get_sector($id) {
        return $this->db->select('*')
                        ->from('sectors')
                        ->where('id', $id)
                        ->get()->row_array();
    }

    function delete_sector($id) {
        $query = $this->db->set('status', 2)
                ->set('modified_by', $this->user['id'])
                ->set('modified', date("Y-m-d H:i:s"))
                ->where('id', $id)
                ->update('sectors');
        if ($query) {
            return 'OK';
        } else {
            return 'NOK';
        }
    }

    function save_new_sector($sector) {
        $this->db->set($sector)
                ->set('created', date("Y-m-d H:i:s"))
                ->set('created_by', $this->user['id'])
                ->set('modified_by', $this->user['id'])
                ->set('modified', date("Y-m-d H:i:s"))
                ->insert('sectors');
        $id = $this->db->insert_id();

        if ($id) {
            return 'OK';
        } else {
            return "NOK";
        }
    }

    function edit_sector($sector, $sector_id) {
        $query = $this->db->set($sector)
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->where('id', $sector_id)
                ->update('sectors');

        if ($query) {
            return 'OK';
        } else {
            return 'NOK';
        }
    }

    // BEDROOMS

    function get_bedrooms() {
        return $this->db->select('b.*, s.name as sector')
                        ->from('bedrooms b')
                        ->join('sectors s', 's.id=b.sector_id')
                        ->where('b.status', 1)
                        ->get()->result_array();
    }

    function get_bedroom($id) {
        return $this->db->select('*')
                        ->from('bedrooms')
                        ->where('id', $id)
                        ->get()->row_array();
    }

    function delete_bedrooms($id) {
        $query = $this->db->set('status', 2)
                ->set('modified_by', $this->user['id'])
                ->set('modified', date("Y-m-d H:i:s"))
                ->where('id', $id)
                ->update('bedrooms');
        if ($query) {
            return 'OK';
        } else {
            return 'NOK';
        }
    }

    function save_new_bedroom($bedroom) {
        $this->db->set($bedroom)
                ->set('created', date("Y-m-d H:i:s"))
                ->set('created_by', $this->user['id'])
                ->set('modified_by', $this->user['id'])
                ->set('modified', date("Y-m-d H:i:s"))
                ->insert('bedrooms');
        $id = $this->db->insert_id();

        if ($id) {
            return 'OK';
        } else {
            return "NOK";
        }
    }

    function edit_bedroom($bedroom, $bedroom_id) {
        $query = $this->db->set($bedroom)
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->where('id', $bedroom_id)
                ->update('bedrooms');

        if ($query) {
            return 'OK';
        } else {
            return 'NOK';
        }
    }

    // BEDS

    function get_beds() {
        return $this->db->select('b.*, s.name as bedroom, sc.name as sector')
                        ->from('beds b')
                        ->join('bedrooms s', 's.id=b.bedroom_id')
                        ->join('sectors sc', 'sc.id=s.sector_id')
                        ->where('b.status', 1)
                        ->get()->result_array();
    }

    function get_bed($id) {
        return $this->db->select('*')
                        ->from('beds')
                        ->where('id', $id)
                        ->get()->row_array();
    }

    function delete_beds($id) {
        $query = $this->db->set('status', 2)
                ->set('modified_by', $this->user['id'])
                ->set('modified', date("Y-m-d H:i:s"))
                ->where('id', $id)
                ->update('beds');
        if ($query) {
            return 'OK';
        } else {
            return 'NOK';
        }
    }

    function save_new_bed($bed) {
        $this->db->set($bed)
                ->set('created', date("Y-m-d H:i:s"))
                ->set('created_by', $this->user['id'])
                ->set('modified_by', $this->user['id'])
                ->set('modified', date("Y-m-d H:i:s"))
                ->insert('beds');
        $id = $this->db->insert_id();

        if ($id) {
            return 'OK';
        } else {
            return "NOK";
        }
    }

    function edit_bed($bed, $bed_id) {
        $query = $this->db->set($bed)
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->where('id', $bed_id)
                ->update('beds');

        if ($query) {
            return 'OK';
        } else {
            return 'NOK';
        }
    }

    // TEAMS

    function get_teams() {
        return $this->db->select('s.*, i.name as institution')
                        ->from('team s')
                        ->join('institutions i', 'i.id=s.institution_id')
                        ->where('s.status', 1)
                        ->get()->result_array();
    }

    function get_team($id) {
        return $this->db->select('*')
                        ->from('team')
                        ->where('id', $id)
                        ->get()->row_array();
    }

    function delete_team($id) {
        $query = $this->db->set('status', 2)
                ->set('modified_by', $this->user['id'])
                ->set('modified', date("Y-m-d H:i:s"))
                ->where('id', $id)
                ->update('team');
        if ($query) {
            return 'OK';
        } else {
            return 'NOK';
        }
    }

    function save_new_team($team) {
        $this->db->set($team)
                ->set('created', date("Y-m-d H:i:s"))
                ->set('created_by', $this->user['id'])
                ->set('modified_by', $this->user['id'])
                ->set('modified', date("Y-m-d H:i:s"))
                ->insert('team');
        $id = $this->db->insert_id();

        if ($id) {
            return 'OK';
        } else {
            return "NOK";
        }
    }

    function edit_team($team, $team_id) {
        $query = $this->db->set($team)
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->where('id', $team_id)
                ->update('team');

        if ($query) {
            return 'OK';
        } else {
            return 'NOK';
        }
    }

    // PROFESSIONALS
    function get_especific_professionals($data) {
        $this->db->select('tm.*, t.name as team, p.name as name')
                ->from('team_members tm')
                ->join('team t', 't.id=tm.team_id')
                ->join('people p', 'p.id=tm.person_id')
                ->where('tm.status', 1);
        if ($data['specialty'] !== null) {
            $this->db->where('specialty', $data['specialty']);
        }
        $this->db->where('hierarchy', $data['hierarchy']);
        return $this->db->get()->result_array();
    }

    function get_professionals() {
        return $this->db->select('tm.*, t.name as team, p.name as name')
                        ->from('team_members tm')
                        ->join('team t', 't.id=tm.team_id')
                        ->join('people p', 'p.id=tm.person_id')
                        ->where('tm.status', 1)
                        ->get()->result_array();
    }

    function get_professionals_type($type) {
        return $this->db->select('tm.*, t.name as team, p.name as name')
                        ->from('team_members tm')
                        ->join('team t', 't.id=tm.team_id')
                        ->join('people p', 'p.id=tm.person_id')
                        ->where('tm.status', 1)
                        ->where('tm.hierarchy', $type)
                        ->get()->result_array();
    }

    function get_professional($id) {
        return $this->db->select('*, u.id as user_id')
                        ->from('team_members tm')
                        ->join('people p', 'p.id=tm.person_id')
                        ->join('users u', 'u.person_id=tm.person_id')
                        ->where('tm.id', $id)
                        ->get()->row_array();
    }

    function delete_professional($id) {
        $query = $this->db->set('status', 2)
                ->set('modified_by', $this->user['id'])
                ->set('modified', date("Y-m-d H:i:s"))
                ->where('id', $id)
                ->update('team_members');
        if ($query) {
            return 'OK';
        } else {
            return 'NOK';
        }
    }

    function save_new_professional($professional) {
        $this->db->set($professional)
                ->set('created', date("Y-m-d H:i:s"))
                ->set('created_by', $this->user['id'])
                ->set('modified_by', $this->user['id'])
                ->set('modified', date("Y-m-d H:i:s"))
                ->insert('team_members');
        $id = $this->db->insert_id();

        if ($id) {
            return 'OK';
        } else {
            return "NOK";
        }
    }

    function edit_professional($professional, $professional_id) {
        $query = $this->db->set($professional)
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->where('id', $professional_id)
                ->update('team_members');

        if ($query) {
            return 'OK';
        } else {
            return 'NOK';
        }
    }

    // tabelas configuradas diretamente pelo banco
    function get_drugs($local) {
        $var = '';
        if ($local == 'unit1') {
            $var = 'primary_assessment';
        } else if ($local == 'unit2') {
            $var = 'secondary_assessment';
        }

        if ($var) {
            return $this->db->select('d.id, d.drug_name, d.description')
                            ->from('c_drugs d')
                            ->where('d.' . $var, 1)
                            ->where('d.status', 1)
                            ->get()->result_array();
        } else {
            return $this->db->select('d.id, d.drug_name, d.description')
                            ->from('c_drugs d')
                            ->where('d.' . $local, 1)
                            ->where('d.status', 1)
                            ->get()->result_array();
        }
    }

    function get_procedures($local) {
        return $this->db->select('d.id, d.procedure_name')
                        ->from('c_procedures d')
                        ->where('d.' . $local, 1)
                        ->where('d.status', 1)
                        ->get()->result_array();
    }

    function get_hemos($local) {
        return $this->db->select('d.id, d.procedure_name')
                        ->from('c_procedures d')
                        ->where('d.' . $local, 1)
                        ->where('d.status', 1)
                        ->get()->result_array();
    }

    function get_laboratorial_exams($local) {
        return $this->db->select('d.id, d.name')
                        ->from('c_laboratorial_exams d')
                        ->where('d.' . $local, 1)
                        ->where('d.status', 1)
                        ->get()->result_array();
    }

    function get_radiological_exams($local) {
        return $this->db->select('d.id, d.name')
                        ->from('c_radiological_exams d')
                        ->where('d.' . $local, 1)
                        ->where('d.status', 1)
                        ->get()->result_array();
    }

    // SURGERIES
    function get_surgeries($local) {
        return $this->db->select('*, surgery_name as name')
                        ->from('c_surgeries')
                        ->where($local, 1)
                        ->where('status', 1)
                        ->get()->result_array();
    }

    function get_history_allergies($context) {
        return $this->db->select('*')
                        ->from('c_history_allergies')
                        ->where($context, 1)
                        ->get()->result_array();
    }

    function get_history_medicines($context) {
        return $this->db->select('*')
                        ->from('c_history_medicines')
                        ->where($context, 1)
                        ->get()->result_array();
    }

    function get_history_surgeries($context) {
        return $this->db->select('*')
                        ->from('c_history_surgeries')
                        ->where($context, 1)
                        ->get()->result_array();
    }

}

?>
