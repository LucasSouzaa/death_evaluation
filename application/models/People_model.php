<?php

Class People_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->user = $this->session->userdata('user');
        date_default_timezone_set('America/Sao_Paulo');
    }

    function get_person_by_use_service($usi) {
        return $this->db->select('p.*, usi.type')
                        ->from('use_service usi')
                        ->join('people p', 'p.id=usi.person_id')
                        ->where('usi.id', $usi)
                        ->get()->row_array();
    }

    function get_person($id) {
        return $this->db->select('*')
                        ->from('people')
                        ->where('id', $id)
                        ->get()->row_array();
    }

    function get_patient_passages($id) {
        return $this->db->select('*')
                        ->from('use_service')
                        ->where('person_id', $id)
                        ->get()->result_array();
    }

    function get_patients() {
        return $this->db->select('p.*')
                        ->select("date_format(birthdate,'%d/%m/%Y') birthdate", false)
                        ->from('people p')
                        ->where('p.status', 1)
                        ->limit(10)
                        ->get()->result_array();
    }

    function get_patients_filtered($filter, $page) {
        return $this->db->select('p.*')
                        ->from('people p')
                        ->where("p.status = 1 and (sus_card like '%$filter%' or name like '%$filter%')")
                        ->limit(10, $page - 1)
                        ->get()->result_array();
    }

    function get_count_patients_filtered($filter) {
        $query = $this->db->query("select id from people where status = 1 and (sus_card like '%$filter%' or name like '%$filter%')");
        return $query;
    }

    function delete_patient($id) {
        $query = $this->db->set('status', 2)
                ->set('modified_by', $this->user['id'])
                ->set('modified', date("Y-m-d H:i:s"))
                ->where('id', $id)
                ->update('people');
        if ($query) {
            return 'OK';
        } else {
            return 'NOK';
        }
    }

    function save_new_patient($patient) {
        $this->db->set($patient)
                ->set('created', date("Y-m-d H:i:s"))
                ->set('created_by', $this->user['id'])
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->insert('people');
        $id = $this->db->insert_id();

        if ($id) {
            return $id;
        } else {
            return "NOK";
        }
    }

    function save_new_user($user) {
        $this->db->set($user)
                ->set('created', date("Y-m-d H:i:s"))
                ->set('created_by', $this->user['id'])
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->insert('users');
        $id = $this->db->insert_id();

        if ($id) {
            return $id;
        } else {
            return "NOK";
        }
    }

    function edit_patient($patient, $patient_id) {
        $query = $this->db->set($patient)
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->where('id', $patient_id)
                ->update('people');
        
        if ($query) {
            return 'OK';
        } else {
            return 'NOK';
        }
    }

    function edit_user($user, $user_id) {
        $query = $this->db->set($user)
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->where('id', $user_id)
                ->update('users');

        if ($query) {
            return 'OK';
        } else {
            return 'NOK';
        }
    }

}

?>
