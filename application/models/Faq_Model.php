<?php

Class Faq_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
    
    public function get_questions(){
        return $this->db->select('id_question, question, answer')
                        ->from('faq')
                        ->order_by('id_question')
                        ->get()->result_array();
    }
    
    public function verify_question($question){
        $query = $this->db->query('SELECT question FROM faq where question = "'.$question.'"');
        
        return $query->num_rows();
    }
    
    public function set_questions($data){
        $this->db->insert('faq', $data);
        return $this->db->affected_rows();
    }
    
    public function edit_questions($data, $id){
        $this->db->where('id_question', $id);
        $this->db->update('faq', $data);
        return $this->db->affected_rows();
    }
    
    public function remove_question($id){
        $this->db->delete('faq', array('id_question' => $id));
        return $this->db->affected_rows();
    }
    
}
