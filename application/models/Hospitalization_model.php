<?php

Class Hospitalization_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
        date_default_timezone_set('America/Sao_Paulo');
        $this->user = $this->session->userdata('user');
        $this->hospitalization = $this->session->userdata('hospitalization');
        if ($this->hospitalization['moment_exit'] == '') {
            $this->moment_exit = date("Y-m-d H:i:s");
        } else {
            $this->moment_exit = $this->hospitalization['moment_exit'];
        }
    }

    function get_use_service_beds($use_service_id) {
        return $this->db->select('b.name as bed, br.name as bedroom, s.name as sector')
                        ->select("date_format(pb.created,'%d/%m/%Y %H:%i') created", false)
                        ->from('passages_beds pb')
                        ->join('beds b', 'b.id=pb.bed_id')
                        ->join('bedrooms br', 'br.id=b.bedroom_id')
                        ->join('sectors s', 's.id=br.sector_id')
                        ->where('pb.use_service_id', $use_service_id)
                        //condicao para trazer os dados da internacao aberta
                        ->where('pb.created >=', $this->hospitalization['moment_admission'])
                        ->where('pb.created <=', $this->moment_exit)
                        ->get()->result_array();
    }

    function get_all_indices($use_service_id) {
        return $this->db->select('*')
                        ->from('indices')
                        ->where('passagem_id', $use_service_id)
                        ->limit(1)
                        ->get()->result_array();
    }

    function save_visit_resume($data) {
        $query = $this->db->set('resume', $data['resume'])
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->where('id', $data['id'])
                ->update('visits');

        if ($query) {
            return 'OK';
        } else {
            return 'NOK';
        }
    }

    function get_biochemistry_exams_c_id($use_service_id, $c_id) {
        return $this->db->select('ple.*, cle.name, pler.result')
                        ->select("date_format(pler.result_date,'%d/%m/%Y %H:%i') result_date", false)
                        ->select("date_format(ple.created,'%d/%m/%Y %H:%i') created", false)
                        ->from('passages_laboratory_exams ple')
                        ->join('c_laboratorial_exams cle', 'cle.id=ple.c_laboratory_exams_id')
                        ->join('passages_laboratory_exams_result pler', 'pler.passages_laboratory_exams_id=ple.id', 'left')
                        ->where('ple.use_service_id', $use_service_id)
                        //->where('ple.status', 1)
                        ->where('cle.id', $c_id)
                        ->order_by('ple.created', 'asc')
                        ->order_by('ple.id', 'asc')
                        ->get()->result_array();
    }

    function get_hospitalizations($usi) {
        return $this->db->select('*')
                        ->select("date_format(moment_admission,'%d/%m/%Y %H:%i') moment_admission", false)
                        ->select("date_format(moment_exit,'%d/%m/%Y %H:%i') moment_exit", false)
                        ->from('intra_hospital')
                        ->where('(exit_type = "internado" OR exit_type = "reinternado")')
                        ->where('status', 1)
                        ->where('use_service_id', $usi)
                        ->order_by('id', 'desc')
                        ->get()->result_array();
    }

    function get_visits_resume($use_service_id) {
        return $this->db->select('*')
                        ->from('visits')
                        ->where('use_service_id', $use_service_id)
                        //condicao para trazer os dados da internacao aberta
                        ->where('created >=', $this->hospitalization['moment_admission'])
                        ->where('created <=', $this->moment_exit)
                        ->order_by('modified', 'desc')
                        ->limit(1)
                        ->get()->row_array();
    }

    function get_intercurrences_resume($use_service_id) {
        return $this->db->select('*')
                        ->from('intercurrences')
                        ->where('use_service_id', $use_service_id)
                        //condicao para trazer os dados da internacao aberta
                        ->where('created >=', $this->hospitalization['moment_admission'])
                        ->where('created <=', $this->moment_exit)
                        ->order_by('modified', 'desc')
                        ->limit(1)
                        ->get()->row_array();
    }

    function get_procedures($use_service_id, $context) {
        return $this->db->select('p.*, cp.procedure_name as name, pe.name as responsible')
                        ->from('passages_procedures p')
                        ->join('c_procedures cp', 'p.c_procedures_id=cp.id')
                        ->join('people pe', 'pe.id=p.responsible_id')
                        ->where('p.use_service_id', $use_service_id)
                        ->where('p.context', $context)
                        ->where('p.hemo', 0)
                        ->where('p.status', 1)
                        //condicao para trazer os dados da internacao aberta
                        ->where('p.created >=', $this->hospitalization['moment_admission'])
                        ->where('p.created <=', $this->moment_exit)
                        ->order_by('p.modified', 'desc')
                        ->get()->result_array();
    }

    function get_hemos($use_service_id, $context) {
        return $this->db->select('p.*, cp.procedure_name as name, pe.name as responsible')
                        ->from('passages_procedures p')
                        ->join('c_procedures cp', 'p.c_procedures_id=cp.id')
                        ->join('people pe', 'pe.id=p.responsible_id')
                        ->where('p.use_service_id', $use_service_id)
                        ->where('p.context', $context)
                        ->where('p.hemo', 1)
                        ->where('p.status', 1)
                        //condicao para trazer os dados da internacao aberta
                        ->where('p.created >=', $this->hospitalization['moment_admission'])
                        ->where('p.created <=', $this->moment_exit)
                        ->order_by('p.modified', 'desc')
                        ->get()->result_array();
    }

    function save_use_service_bed($data) {
        $this->db->set($data)
                ->set('moment_exit', NULL)
                ->set('created', date("Y-m-d H:i:s"))
                ->set('created_by', $this->user['id'])
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->insert('passages_beds');
        $id = $this->db->insert_id();
        if ($id) {
            return 'OK';
        } else {
            return "NOK";
        }
    }

    function get_last_use_service_bed($use_service_id) {
        return $this->db->select('pb.*, b.*, s.name as bedroom, sc.name as sector')
                        ->select("date_format(pb.moment_exit,'%d/%m/%Y %H:%i') moment_exit", false)
                        ->from('passages_beds pb')
                        ->join('beds b', 'b.id=pb.bed_id')
                        ->join('bedrooms s', 's.id=b.bedroom_id')
                        ->join('sectors sc', 'sc.id=s.sector_id')
                        ->where('pb.use_service_id', $use_service_id)
                        ->order_by('pb.moment_exit', 'desc')
                        ->limit(1)
                        ->get()->row_array();
    }

    function update_use_service_($id) {
        $query = $this->db->set('moment_exit', date("Y-m-d H:i:s"))
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->where('id', $id)
                ->update('passages_beds');

        if ($query) {
            return 'OK';
        } else {
            return 'NOK';
        }
    }

    // ABA USO DE SANGUE DA INTERNACAO
    function get_use_of_blood($use_service_id) {
        return $this->db->select('uob.*, s.name')
                        ->select("date_format(uob.created,'%d/%m/%Y %H:%i') created", false)
                        ->from('passages_use_of_blood uob')
                        ->join('sectors s', 's.id=uob.sector_id')
                        ->where('uob.use_service_id', $use_service_id)
                        ->where('uob.status', 1)
                        ->where('uob.local_type', 'hospitalization')
                        //condicao para trazer os dados da internacao aberta
                        ->where('uob.created >=', $this->hospitalization['moment_admission'])
                        ->where('uob.created <=', $this->moment_exit)
                        ->get()->result_array();
    }

    function save_use_of_blood($data) {

        $this->db->set($data)
                ->set('created', date("Y-m-d H:i:s"))
                ->set('created_by', $this->user['id'])
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->insert('passages_use_of_blood');
        $id = $this->db->insert_id();
        if ($id) {
            return 'OK';
        } else {
            return "NOK";
        }
    }

    function delete_use_of_blood($id) {
        $query = $this->db->set('status', 0)
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->where('id', $id)
                ->update('passages_use_of_blood');

        if ($query) {
            return 'OK';
        } else {
            return 'NOK';
        }
    }

    // ABA EXAMES LABORATORIAIS DA INTERNACAO

    function save_biochemistry_exams($data) {

//        $select = $this->db->select('*')
//                        ->from('passages_laboratory_exams')
//                        ->where('c_laboratory_exams_id', $data['c_laboratory_exams_id'])
//                        ->where('local_type', $data['local_type'])
//                        ->where('use_service_id', $data['use_service_id'])
//                        ->get()->num_rows();
//
//        if ($select > 0) {
//            $query = $this->db->set('status', 1)
//                    ->set('modified', date("Y-m-d H:i:s"))
//                    ->set('modified_by', $this->user['id'])
//                    ->where('c_laboratory_exams_id', $data['c_laboratory_exams_id'])
//                    ->where('local_type', $data['local_type'])
//                    ->where('use_service_id', $data['use_service_id'])
//                    ->update('passages_laboratory_exams');
//
//            if ($query) {
//                return 'OK';
//            } else {
//                return 'NOK';
//            }
//        } else {
        $this->db->set('c_laboratory_exams_id', $data['c_laboratory_exams_id'])
                ->set('use_service_id', $data['use_service_id'])
                ->set('created', date("Y-m-d H:i:s"))
                ->set('created_by', $this->user['id'])
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->set('local_type', $data['local_type'])
                ->insert('passages_laboratory_exams');
        $id = $this->db->insert_id();
        if ($id) {
            return 'OK';
        } else {
            return "NOK";
        }
        // }
    }

    function delete_biochemistry_exam($id) {
        $query = $this->db->set('status', 0)
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->where('id', $id)
                ->update('passages_laboratory_exams');

        if ($query) {
            return 'OK';
        } else {
            return 'NOK';
        }
    }

    // ABA EXAMES ANATOMO PATOLOGICOS DA INTERNACAO
    function get_anatomical_pathological_exams($use_service_id) {
        return $this->db->select('*')
                        ->select("date_format(reception_date,'%d/%m/%Y %H:%i') reception_date", false)
                        ->select("date_format(liberation_date,'%d/%m/%Y %H:%i') liberation_date", false)
                        ->from('passages_anatomical_pathological_exams')
                        ->where('use_service_id', $use_service_id)
                        ->where('status', 1)
                        ->where('local_type', 'hospitalization')
                        //condicao para trazer os dados da internacao aberta
                        ->where('created >=', $this->hospitalization['moment_admission'])
                        ->where('created <=', $this->moment_exit)
                        ->get()->result_array();
    }

    function save_anatomical_pathological_exam($data) {

        $this->db->set($data)
                ->set('created', date("Y-m-d H:i:s"))
                ->set('created_by', $this->user['id'])
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->insert('passages_anatomical_pathological_exams');
        $id = $this->db->insert_id();
        if ($id) {
            return 'OK';
        } else {
            return "NOK";
        }
    }

    function delete_anatomical_pathological_exam($id) {
        $query = $this->db->set('status', 0)
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->where('id', $id)
                ->update('passages_anatomical_pathological_exams');

        if ($query) {
            return 'OK';
        } else {
            return 'NOK';
        }
    }

    function delete_procedure($id) {
        $query = $this->db->set('status', 0)
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->where('id', $id)
                ->update('passages_procedures');

        if ($query) {
            return 'OK';
        } else {
            return 'NOK';
        }
    }

    // ABA EXAMES DE IMAGEM DA INTERNACAO
    function get_image_exams($use_service_id) {
        return $this->db->select('pre.*, cre.name')
                        ->select("date_format(pre.report_date,'%d/%m/%Y %H:%i') report_date", false)
                        ->select("date_format(pre.created,'%d/%m/%Y %H:%i') created", false)
                        ->from('passages_radiological_exams pre')
                        ->join('c_radiological_exams cre', 'cre.id=pre.c_radiological_exams_id')
                        ->where('pre.use_service_id', $use_service_id)
                        ->where('pre.status', 1)
                        ->where('pre.local_type', 'hospitalization')
                        //condicao para trazer os dados da internacao aberta
                        ->where('pre.created >=', $this->hospitalization['moment_admission'])
                        ->where('pre.created <=', $this->moment_exit)
                        ->get()->result_array();
    }

    function save_image_exams($data) {

//        $select = $this->db->select('*')
//                        ->from('passages_radiological_exams')
//                        ->where('c_radiological_exams_id', $data['c_radiological_exams_id'])
//                        ->where('local_type', $data['local_type'])
//                        ->where('use_service_id', $data['use_service_id'])
//                        ->get()->num_rows();
//
//        if ($select > 0) {
//            $query = $this->db->set('status', 1)
//                    ->set('modified', date("Y-m-d H:i:s"))
//                    ->set('modified_by', $this->user['id'])
//                    ->where('c_radiological_exams_id', $data['c_radiological_exams_id'])
//                    ->where('local_type', $data['local_type'])
//                    ->where('use_service_id', $data['use_service_id'])
//                    ->update('passages_radiological_exams');
//
//            if ($query) {
//                return 'OK';
//            } else {
//                return 'NOK';
//            }
//        } else {
        $this->db->set('c_radiological_exams_id', $data['c_radiological_exams_id'])
                ->set('use_service_id', $data['use_service_id'])
                ->set('created', date("Y-m-d H:i:s"))
                ->set('created_by', $this->user['id'])
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->set('local_type', $data['local_type'])
                ->insert('passages_radiological_exams');
        $id = $this->db->insert_id();
        if ($id) {
            return 'OK';
        } else {
            return "NOK";
        }
        //}
    }

    function delete_image_exam($id) {
        $query = $this->db->set('status', 0)
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->where('id', $id)
                ->update('passages_radiological_exams');

        if ($query) {
            return 'OK';
        } else {
            return 'NOK';
        }
    }

    // ABA IMAGERY DA INTERNACAO
    public function insert_imagery($content) {
        $this->db->set($content)
                ->set('created', date("Y-m-d H:i:s"))
                ->set('created_by', $this->user['id'])
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->insert('user_service_images');
        $id = $this->db->insert_id();
        if ($id) {
            return $id;
        } else {
            return "NOK";
        }
    }

    function get_imagerys($use_service_id) {
        return $this->db->select('*')
                        ->from('user_service_images')
                        ->where('use_service_id', $use_service_id)
                        ->where('status', 1)
                        //condicao para trazer os dados da internacao aberta
                        ->where('created >=', $this->hospitalization['moment_admission'])
                        ->where('created <=', $this->moment_exit)
                        ->get()->result_array();
    }

    function delete_imagery($id) {
        $query = $this->db->set('status', 0)
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->where('id', $id)
                ->update('user_service_images');
        if ($query) {
            return 'OK';
        } else {
            return 'NOK';
        }
    }

    function view_imagery($id) {
        return $this->db->select('*')
                        ->from('user_service_images')
                        ->where('id', $id)
                        ->get()->row_array();
    }

    // ABA DE CIRURGIAS
    function get_surgeries($use_service_id, $local_type) {
        $query = $this->db->select('p.*, c.surgery_name as name')
                ->from('passages_surgeries p')
                ->join('c_surgeries c', 'c.id=p.c_surgeries_id', 'left')
                ->where('p.use_service_id', $use_service_id)
                //condicao para trazer os dados da internacao aberta
                ->where('p.created >=', $this->hospitalization['moment_admission'])
                ->where('p.created <=', $this->moment_exit)
                ->where('p.status', 1);
        if ($local_type !== 'all') {
            return $query->where('p.local_type', $local_type)
                            ->get()->result_array();
        } else {
            return $query->get()->result_array();
        }
    }

    function get_surgery_team($passage_surgery_id) {
        return $this->db->select('pr.*, pe.name')
                        ->from('passages_surgeries_team pr')
                        ->join('team_members tm', 'tm.id=pr.team_member_id')
                        ->join('people pe', 'pe.id=tm.person_id')
                        ->where('pr.passage_surgery_id', $passage_surgery_id)
                        ->where('pr.status', 1)
                        ->get()->result_array();
    }

    // FUNCOES DE VISITAS
    function get_passages($use_service_id) {
        return $this->db->select('pa.*, pe.name')
                        ->select("date_format(pa.created,'%d/%m/%Y %H:%i') created", false)
                        ->from('visits pa')
                        ->join('people pe', 'pe.id=pa.author_id')
                        ->where('pa.use_service_id', $use_service_id)
                        ->where('pa.status', 1)
                        //condicao para trazer os dados da internacao aberta
                        ->where('pa.created >=', $this->hospitalization['moment_admission'])
                        ->where('pa.created <=', $this->moment_exit)
                        ->get()->result_array();
    }

    function get_passages_intercurrences($use_service_id) {
        return $this->db->select('pa.*, pe.name')
                        ->select("date_format(pa.created,'%d/%m/%Y %H:%i') created", false)
                        ->from('intercurrences pa')
                        ->join('people pe', 'pe.id=pa.author_id')
                        ->where('pa.use_service_id', $use_service_id)
                        ->where('pa.status', 1)
                        //condicao para trazer os dados da internacao aberta
                        ->where('pa.created >=', $this->hospitalization['moment_admission'])
                        ->where('pa.created <=', $this->moment_exit)
                        ->get()->result_array();
    }

    function get_passages_responsables($passage_id) {
        return $this->db->select('pr.*, pe.name')
                        ->from('passages_responsables pr')
                        ->join('people pe', 'pe.id=pr.person_id')
                        ->where('pr.passage_id', $passage_id)
                        ->where('pr.status', 1)
                        ->get()->result_array();
    }

    function save_procedure_hemo($procedure) {
        $this->db->set($procedure)
                ->set('created', date("Y-m-d H:i:s"))
                ->set('created_by', $this->user['id'])
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->insert('passages_procedures');
        $id = $this->db->insert_id();
        if ($id) {
            return 'OK';
        } else {
            return "NOK";
        }
    }

    function save_visit($visit) {
        $this->db->set($visit)
                ->set('created', date("Y-m-d H:i:s"))
                ->set('created_by', $this->user['id'])
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->insert('visits');
        $id = $this->db->insert_id();
        if ($id) {
            return 'OK';
        } else {
            return "NOK";
        }
    }

    function save_intercurrence($intercurrence) {
        $this->db->set($intercurrence)
                ->set('created', date("Y-m-d H:i:s"))
                ->set('created_by', $this->user['id'])
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->insert('intercurrences');
        $id = $this->db->insert_id();
        if ($id) {
            return 'OK';
        } else {
            return "NOK";
        }
    }

    // ADICIONANDO RESPONSAVEIS AS VISITAS
    function insert_responsable_in_visit($data) {
        $select = $this->db->select('*')
                        ->from('passages_responsables')
                        ->where('passage_id', $data['passage_id'])
                        ->where('person_id', $data['person_id'])
                        ->get()->num_rows();

        if ($select > 0) {
            $query = $this->db->set('status', 1)
                    ->set('modified', date("Y-m-d H:i:s"))
                    ->set('modified_by', $this->user['id'])
                    ->where('passage_id', $data['passage_id'])
                    ->update('passages_responsables');

            if ($query) {
                return 'OK';
            } else {
                return 'NOK';
            }
        } else {
            $this->db->set($data)
                    ->set('created', date("Y-m-d H:i:s"))
                    ->set('created_by', $this->user['id'])
                    ->set('modified', date("Y-m-d H:i:s"))
                    ->set('modified_by', $this->user['id'])
                    ->insert('passages_responsables');
            $id = $this->db->insert_id();
            if ($id) {
                return 'OK';
            } else {
                return "NOK";
            }
        }
    }

    function save_surgery($surgery) {
        $this->db->set($surgery)
                ->set('created', date("Y-m-d H:i:s"))
                ->set('created_by', $this->user['id'])
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->insert('passages_surgeries');
        $id = $this->db->insert_id();
        if ($id) {
            return $id;
        } else {
            return "NOK";
        }
    }

    function save_surgery_team_member($surgery_id, $team_member_id) {
        $this->db->set('passage_surgery_id', $surgery_id)
                ->set('team_member_id', $team_member_id)
                ->set('created', date("Y-m-d H:i:s"))
                ->set('created_by', $this->user['id'])
                ->set('modified', date("Y-m-d H:i:s"))
                ->set('modified_by', $this->user['id'])
                ->insert('passages_surgeries_team');
        $id = $this->db->insert_id();
        if ($id) {
            return 'OK';
        } else {
            return "NOK";
        }
    }

}

?>